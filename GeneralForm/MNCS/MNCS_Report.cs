﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.MNCS
{
    public partial class MNCS_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

        readonly string _pTypeReport;
        //0 - รายงานรายการสินค้าตรวจนับสต๊อกมินิมาร์ท (ใหม่)
        //1 - รายงานเปรียบเทียบยอดขายกับจำนวนสต็อกคงเหลือ(มินิมาร์ท)
        //2 - รายงานจำนวนรายการเช็คสต๊อค(ตามวันที่)
        //3 - รายงานจำนวนรายการเช็คสต๊อค (รายชั่วโมง)
        //4 - รายงานสินค้าทั้งหมด ภายในสาขา ดึงสต็อกขายยอนหลัง/ปริมาณคงเหลือ/ประวัติสต็อก
        //5 - รายงานจำนวนรายการเช็คสต๊อค(ตามปี)

        //6 - รายงานสินค้าทั้งหมด ภายในสาขา ประกบการนับย้อนหลัง 5 ครั้ง
        //7 - ค่าคอมของพนักงาน เช็คสต็อก
        //8 - รายงานรายละเอียดการนับสต๊อก[พนักงาน]
        //9 -  รายการเช็คสต๊อกที่ไม่ลงรายการบัญชี
        readonly string _pPermission; //0 ฝั่งมินิมาร์ท 1 ฝั่งซุปเปอร์ชีป ดูแลบิลขาเข้า //2 ฝั่งซุปเปอร์ชีป แผนกเบิกถายใน

        string caseBchID;

        //Load
        public MNCS_Report(string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
        }
        //Load
        private void MNCS_Report_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            RadButton_Search.ButtonElement.ShowBorder = true;
            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_2.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_2);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadDropDownList_1.Visible = false; RadCheckBox_1.Visible = false;
            RadDropDownList_2.Visible = false; RadCheckBox_2.Visible = false;

            radLabel_Date.Visible = false; radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;

            radLabel_3.Visible = false; radTextBox_3.Visible = false;

            switch (_pTypeReport)
            {
                case "0":
                    radLabel_Detail.Text = "สีเหลือง >> สรุปสต็อกเกิน | สีเขียว >> สรุปสต็อกขาด";

                    RadCheckBox_1.Text = "ระบุคลังสินค้า"; RadCheckBox_2.Text = "วันที่ตรวจนับ";
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    RadDropDownList_2.Visible = true; RadCheckBox_2.Visible = true; RadCheckBox_2.Checked = true; RadCheckBox_2.Enabled = false;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WH", "คลัง", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WH_NAME", "ชื่อคลัง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JOURNALID", "เลขที่เอกสาร", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPT", "แผนกนับ", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPTNAME", "ชื่อแผนกนับ", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMID", "รหัสสินค้า", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODE", "บาร์โค้ด", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 330)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("INVENTONHAND", "คงคลังคงเหลือ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTED", "จำนวนตรวจนับ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("AMOUNT", "ยอดขายเกิน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SPC_PriceGroup3", "ราคาต่อหน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("pricetotal", "ยอดหัก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERID", "จัดซื้อ", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BUYERNAME", "ชื่อจัดซื้อ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PRIMARYVENDORID", "ผู้จำหน่าย", 70)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENNAME", "ชื่อผู้จำหน่าย", 280)));

                    DatagridClass.SetCellBackClolorByExpression("AMOUNT", "AMOUNT > 0 ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("AMOUNT", "AMOUNT < 0 ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);

                    break;

                case "1":   //1- รายงานเปรียบเทียบยอดขายกับจำนวนสต็อกคงเหลือ(มินิมาร์ท)
                    radLabel_Detail.Text = "สีแดง ยอดขายรวม  >> ไม่มีประวัติการขาย | สีเทา ใช้เวลาขาย >> กรณีใช้เวลาขายมากกว่า 3 เดือน | สีแดง ใช้เวลาขาย >> ติดสต็อก 100%";

                    RadCheckBox_1.Text = "ระบุคลังสินค้า"; RadCheckBox_2.Text = "เลขที่ตรวจนับสต็อก";
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    RadDropDownList_2.Visible = true; RadCheckBox_2.Visible = true; RadCheckBox_2.Checked = true; RadCheckBox_2.Enabled = false;
                    RadDropDownList_2.DropDownListElement.DropDownWidth = 350;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่ขายย้อนหลัง";
                    break;
                case "2": //2 รายงานจำนวนรายการเช็คสต๊อค (ตามวันที่) 
                    radLabel_Detail.Text = "สีแดง >> ไม่มีรายการนับ | [จำนวนรายการนับ มาจากจำนวนบาร์โค้ดที่นับ]";

                    RadCheckBox_1.Text = "ระบุแผนกนับสต็อก"; RadCheckBox_2.Visible = false;
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่นับสต็อก"; radLabel_Date.Visible = true;
                    RadDropDownList_2.DropDownListElement.DropDownWidth = 350;
                    RadDropDownList_2.Visible = true; RadCheckBox_2.Visible = true; RadCheckBox_2.Checked = false; RadCheckBox_2.Enabled = true;

                    break;
                case "3": //3 รายงานจำนวนรายการเช็คสต๊อค (รายชั่วโมง)
                    radLabel_Detail.Text = "สีแดง >> ไม่มีรายการนับ | [จำนวนรายการนับ มาจากจำนวนบาร์โค้ดที่นับ]";

                    RadCheckBox_1.Text = "ระบุแผนกนับสต็อก"; RadCheckBox_2.Visible = false;
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่นับสต็อก"; radLabel_Date.Visible = true;
                    RadDropDownList_2.DropDownListElement.DropDownWidth = 350;
                    RadDropDownList_2.Visible = true; RadCheckBox_2.Visible = true; RadCheckBox_2.Checked = false; RadCheckBox_2.Enabled = true;
                    break;

                case "4":
                    radLabel_Detail.Text = "สีแดง จำนวนวันที่ไม่ได้นับสต็อก >> ไม่เคยนับสต็อก | สีส้ม จำนวนวันที่ไม่ได้นับสต็อก >> นับสต็อกเกิน 3 เดือน | สีแดง ใช้เวลาขาย >> สินค้าไม่มีประวัติขายย้อนหลัง | สีส้ม ใช้เวลาขาย >> สินค้าใช้เวลาขายมากกว่า 3 เดือน | สินค้าตั้งต้นจากประวัติขาย-เบิกย้อนหลังตามที่ระบุ ";
                    radLabel_3.Visible = true; radTextBox_3.Visible = true; radTextBox_3.Text = "3";
                    radLabel_3.Text = "จำนวนเดือน ย้อนหลัง";
                    RadCheckBox_1.Text = "ระบุสาขา"; RadCheckBox_2.Visible = false; RadCheckBox_1.Enabled = true;
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    RadCheckBox_2.Text = "ระบุจัดซื้อ";
                    RadDropDownList_2.Visible = true; RadCheckBox_2.Visible = true; RadCheckBox_2.Checked = false; RadCheckBox_2.Enabled = true;


                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 330)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("COUNTDATE", "นับสต็อกล่าสุด", 105)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTEDQTY", "จำนวนนับล่าสุด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTDAY", "จำนวนวันที่ไม่ได้" + Environment.NewLine + "นับสต็อก", 130)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("AVAILPHYSICAL", "คงคลังเหลือ", 100)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTYSALE", "จำนวนขาย" + Environment.NewLine + "ย้อนหลัง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("AVGM", "เฉลี่ยยอดขาย" + Environment.NewLine + "เดือน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SALEEND", "ใช้เวลาขาย" + Environment.NewLine + "เดือน", 80)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPT_ID", "จัดซื้อ", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPT_NAME", "ชื่อจัดซื้อ", 250)));

                    DatagridClass.SetCellBackClolorByExpression("COUNTDAY", "COUNTDAY = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("COUNTDAY", "COUNTDAY > 90 ", Color.Orange, RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SALEEND", "SALEEND > 3 ", Color.Orange, RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SALEEND", "SALEEND = 100 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("COUNTDATE", "COUNTDATE = '1900-01-01' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    break;
                case "5": //5 รายงานจำนวนรายการเช็คสต๊อค (ตามปี) 
                    radLabel_Detail.Text = "สีแดง >> ไม่มีรายการนับ | [จำนวนรายการนับ มาจากจำนวนบาร์โค้ดที่นับ]";

                    RadCheckBox_1.Text = "ระบุแผนกนับสต็อก";
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true; RadDropDownList_1.Enabled = true;
                    RadCheckBox_2.Text = "ปีที่ทำรายการ";
                    RadCheckBox_2.Visible = true; RadCheckBox_2.Checked = true; RadCheckBox_2.Enabled = false;
                    radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
                    radLabel_Date.Text = "ระบุวันที่นับสต็อก"; radLabel_Date.Visible = false;
                    RadDropDownList_2.Visible = true;
                    RadGridView_ShowHD.TableElement.TableHeaderHeight = 100;

                    break;

                case "6": //5 รายงานประวัติการนับสต็อก 5 ครั้งย้อนหลัง
                    radLabel_Detail.Text = "สีแดง >> %ขาด | สีฟ้า >> %เกิน | สินค้าแสดงบาร์โค้ดหน่วยย่อยเท่านั้น | ครั้งที่ 1 คือ ครั้งล่าสุด | สินค้าตั้งต้นจากประวัติขาย-เบิกย้อนหลัง 3 เดือน ";
                    radLabel_3.Visible = false; radTextBox_3.Visible = false;
                    RadCheckBox_1.Text = "ระบุสาขา"; RadCheckBox_2.Visible = false; RadCheckBox_1.Enabled = true;
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    RadCheckBox_2.Text = "ระบุจัดซื้อ";
                    RadDropDownList_2.Visible = true; RadCheckBox_2.Visible = true; RadCheckBox_2.Checked = false; RadCheckBox_2.Enabled = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 330)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

                    for (int i = 0; i < 5; i++)
                    {
                        string valueF = (i + 1).ToString();
                        RadGridView_ShowHD.MasterTemplate.Columns.Add(
                            (DatagridClass.AddTextBoxColumn_AddManualSetRight("D_" + valueF, "ครั้งที่ " + valueF + Environment.NewLine + "วันที่นับสต็อก", 100)));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add(
                            (DatagridClass.AddTextBoxColumn_AddManualSetRight("P_" + valueF, "ครั้งที่ " + valueF + Environment.NewLine + "% ขาด-เกิน", 80)));
                    }

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPT_ID", "จัดซื้อ", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPT_NAME", "ชื่อจัดซื้อ", 300)));

                    break;
                case "7":
                    radLabel_Detail.Text = "แสดงเฉพาะพนักงานตำแหน่งเช็คสต็อกเท่านั้น | พนักงานจะแสดงรายการเฉพาะที่มีรายการนับสต็อกภายในวันที่ระบุเท่านั้น";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMLINE", "จำนวนรายการรวม", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMGRAND", "ยอดเงินค่าคอม", 120)));

                    RadCheckBox_1.Text = "ระบุแผนกนับสต็อก";
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่นับสต็อก"; radLabel_Date.Visible = true;

                    break;
                case "8":
                    radLabel_Detail.Text = "รายละเอียดการนับสต็อกสินค้า";

                    RadCheckBox_1.Visible = false; RadCheckBox_2.Visible = false;
                    RadDropDownList_1.Visible = false; RadDropDownList_2.Visible = false;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่นับสต็อก"; radLabel_Date.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "คลัง", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STOCKDescription", "ชื่อคลัง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัสผู้บันทึก", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อผู้บันทึก", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("StockID", "เลขที่บิล", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTYCounting", "จำนวนตรวจนับ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Unit", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CountType", "ประเภทนับสต๊อก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CountDesc", "อธิบายการนับ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATECounting", "วันที่นับ", 150)));
                    break;
                case "9":
                    radLabel_Detail.Text = "รายละเอียดการเช็คสต๊อกที่ไม่ลงรายการบัญชี";

                    RadCheckBox_1.Visible = false; RadCheckBox_2.Visible = false;
                    RadDropDownList_1.Visible = false; RadDropDownList_2.Visible = false;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Date.Text = "ระบุวันที่นับสต็อก"; radLabel_Date.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "คลัง", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STOCKDescription", "ชื่อคลัง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STOCKID", "เลขที่บิล", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Count_front", "จำนวนหน้า", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Count_back", "จำนวนหลัง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Balance", "คงเหลือ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Sum", "สรุปขาด-เกิน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCountDate", "วันที่นับล่าสุด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLChecker", "รหัสผู้นับ", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEChecker", "ชื่อผู้นับ", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYCounting", "จำนวนตรวจนับ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CountType", "ประเภทนับสต๊อก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CountDesc", "อธิบายการนับ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCheckChar", "วันที่เช็คก่อนหน้า", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("LastQTY", "จำนวนเช็คก่อนหน้า", 120)));
                    break;
                default:
                    break;
            }


            DataTable dtBch;

            if (SystemClass.SystemBranchID == "MN000")
            {
                if (_pPermission == "2")//แผนกเบิกถายใน
                {
                    if ((_pTypeReport == "2") | (_pTypeReport == "5"))
                    {
                        if (SystemClass.SystemDptID == "D194") { caseBchID = "1"; }
                        else { caseBchID = "2"; }

                        dtBch = Models.DptClass.GetDpt_AllD();
                        RadDropDownList_1.DataSource = dtBch;
                        RadDropDownList_1.DisplayMember = "DESCRIPTION_SHOW";
                        RadDropDownList_1.ValueMember = "NUM";
                        RadCheckBox_1.Checked = true; RadCheckBox_1.Enabled = true;
                        RadDropDownList_1.SelectedValue = SystemClass.SystemDptID;
                    }
                    else if (_pTypeReport == "3")
                    {
                        if (SystemClass.SystemDptID == "D194") { caseBchID = "1"; }
                        else { caseBchID = "2"; }

                        dtBch = Models.DptClass.GetDpt_AllD();
                        RadDropDownList_1.DataSource = dtBch;
                        RadDropDownList_1.DisplayMember = "DESCRIPTION_SHOW";
                        RadDropDownList_1.ValueMember = "NUM";
                        RadCheckBox_1.Checked = true; RadCheckBox_1.Enabled = true; RadDropDownList_1.Enabled = true;
                        RadDropDownList_1.SelectedValue = SystemClass.SystemDptID;
                    }
                    else
                    {
                        caseBchID = "2";
                        RadCheckBox_1.Checked = false; RadCheckBox_1.Enabled = true; RadDropDownList_1.Enabled = false;
                        dtBch = Models.InventClass.Find_InventSupc("");
                        RadDropDownList_1.DataSource = dtBch;
                        RadDropDownList_1.DisplayMember = "INVENTLOCATIONNAME";
                        RadDropDownList_1.ValueMember = "INVENTLOCATIONID";
                    }
                }
                else
                {
                    caseBchID = "1";
                    RadCheckBox_1.Checked = false; RadCheckBox_1.Enabled = true; RadDropDownList_1.Enabled = false;
                    dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                    RadDropDownList_1.DataSource = dtBch;
                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";
                    if (_pTypeReport == "4") { RadCheckBox_1.Enabled = false; RadCheckBox_1.Checked = true; RadDropDownList_1.Enabled = true; }
                }
            }
            else
            {
                caseBchID = "0";
                RadCheckBox_1.Checked = true; RadCheckBox_1.Enabled = false; RadDropDownList_1.Enabled = true;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadDropDownList_1.DataSource = dtBch;
                RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                RadDropDownList_1.ValueMember = "BRANCH_ID";
            }

            ClearTxt();

            if ((_pTypeReport == "2") || (_pTypeReport == "3"))
            {
                string caseSend = "";

                if (SystemClass.SystemBranchID != "MN000")
                {
                    caseSend = " AND SPC_InventLocationIdFrom = '" + SystemClass.SystemBranchID + "' ";
                }
                else
                {
                    switch (caseBchID)
                    {
                        case "0": caseSend = " AND SPC_InventLocationIdFrom = '" + RadDropDownList_1.SelectedValue.ToString() + "' "; break;
                        case "1": caseSend = " AND SPC_InventLocationIdFrom LIKE 'MN%'"; break;
                        case "2": caseSend = " AND SPC_InventLocationIdFrom NOT LIKE 'MN%'"; break;
                        default:
                            break;
                    }
                }

                RadDropDownList_2.DataSource = CheckStockClass.MNCS_JournalByWH("1", caseSend); ;
                RadDropDownList_2.DisplayMember = "DisplayMember";
                RadDropDownList_2.ValueMember = "ValueMember";
            }

            if (_pTypeReport == "4")
            {
                RadDropDownList_2.DataSource = Models.DptClass.GetDpt_Purchase();
                RadDropDownList_2.DisplayMember = "DESCRIPTION";
                RadDropDownList_2.ValueMember = "NUM";
            }

            if (_pTypeReport == "5")
            {
                RadDropDownList_2.DataSource = DateTimeSettingClass.Config_YY();
                RadDropDownList_2.DisplayMember = "Y1";
                RadDropDownList_2.ValueMember = "Y2";
            }

            if (_pTypeReport == "6")
            {
                RadCheckBox_1.Enabled = false; RadCheckBox_1.Checked = true;
                RadDropDownList_2.DataSource = Models.DptClass.GetDpt_Purchase();
                RadDropDownList_2.DisplayMember = "DESCRIPTION";
                RadDropDownList_2.ValueMember = "NUM";
            }
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            switch (_pTypeReport)
            {
                case "0":// รายงานรายการสินค้าตรวจนับสต๊อก
                    //string bchID = "";
                    //if (RadCheckBox_1.Checked == true)
                    //{
                    //    bchID = $@" AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom = '{RadDropDownList_1.SelectedValue}' ";
                    //}
                    //else
                    //{
                    //    switch (caseBchID)
                    //    {
                    //        case "0":
                    //            bchID = $@" AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom = '{SystemClass.SystemBranchID}' ";
                    //            break;
                    //        case "1":
                    //            bchID = $@" AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom LIKE 'MN%' ";
                    //            break;
                    //        case "2":
                    //            bchID = $@" AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom NOT LIKE 'MN%' AND INVENTJOURNALTABLE.SPC_InventLocationIdFrom NOT LIKE 'V%' ";
                    //            break;
                    //        default:
                    //            break;
                    //    }
                    //}
                    //string transDate = $@" AND CONVERT(VARCHAR,SPC_TRANSDATE,23) = '{RadDropDownList_2.SelectedValue}' ";
                    string bchID = "";
                    if (RadCheckBox_1.Checked == true) bchID =RadDropDownList_1.SelectedValue.ToString();
                    dt_Data = CheckStockClass.MNCS_ReportItemByJournalID(bchID, RadDropDownList_2.SelectedValue.ToString(), caseBchID);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;

                case "1":
                    RptCase1();

                    break;
                case "2":
                    RptCase2();
                    break;

                case "3":
                    RptCase3();
                    break;

                case "4":
                    RptCase4();
                    break;

                case "5":
                    RptCase5();
                    break;

                case "6":
                    RptCase6();
                    break;

                case "7":
                    RptCase7();
                    break;
                case "8":
                    dt_Data = CheckStockClass.MNCS_RC(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                    if (dt_Data.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("การนับสต็อก");
                    }
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "9":
                    string dateSql = $@" WHERE CONVERT(VARCHAR,LastCountDate,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' ";
                    dt_Data = CheckStock_Class.MNCS_DEL(dateSql);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }
        //Report Case 1
        void RptCase1()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0) this.RadGridView_ShowHD.Columns.Clear();

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WH", "คลัง", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WH_NAME", "ชื่อคลัง", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JOURNALID", "เลขที่เอกสาร", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPT", "แผนกนับ", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DPTNAME", "ชื่อแผนกนับ", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMID", "รหัสสินค้า", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CONFIGID", "โครงแบบ")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTSIZEID", "ขนาด")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTCOLORID", "สี")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODE", "บาร์โค้ด", 140)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 330)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODEUNIT", "หน่วย", 80)));

            DataTable dtMonth = Controllers.DateTimeSettingClass.GetMonthYearByDate(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));

            foreach (DataRow item in dtMonth.Rows)
            {
                string headF = item["MonthNumber"].ToString() + item["MonthYear"].ToString();
                string valueF = item["ThaiMonth"].ToString() + Environment.NewLine + item["MonthYear"].ToString();
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(headF, valueF, 100)));
            }
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMALL", "ยอดขายรวม", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMMONTH", "จำนวนเดือน", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("AVG", "ยอดขายเฉลี่ย", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNTED", "ปริมาณคงเหลือ", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SaleMonth", "ใช้เวลาขาย" + Environment.NewLine + "เดือน", 80)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "จัดซื้อ", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION_NAME", "ชื่อจัดซื้อ", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDID", "ผู้จำหน่าย", 70)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VENDNAME", "ชื่อผู้จำหน่าย", 280)));

            DatagridClass.SetCellBackClolorByExpression("SaleMonth", "SaleMonth > 3 ", ConfigClass.SetColor_GrayPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("SaleMonth", "SaleMonth = 100 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("SUMALL", "SUMALL = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);


            dt_Data = CheckStockClass.MNCS_SPCN_SumInventCountingMNByVoucherByDate_FindJournalId(RadDropDownList_2.SelectedValue.ToString());
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                double count_M = 0;
                double sumAll = 0;

                DataTable dtSale = PosSaleClass.GetQtySale_ByBchByConfigGroupMMYY(
                    dt_Data.Rows[i]["CONFIGID"].ToString(), dt_Data.Rows[i]["INVENTSIZEID"].ToString(),
                    dt_Data.Rows[i]["INVENTCOLORID"].ToString(), dt_Data.Rows[i]["ITEMID"].ToString(),
                    dt_Data.Rows[i]["WH"].ToString(),
                    radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
                foreach (DataRow item in dtMonth.Rows)
                {
                    string headF = item["MonthNumber"].ToString() + item["MonthYear"].ToString();
                    DataRow[] result = dtSale.Select($@"SALE_MM = '{item["MonthNumber"]}' AND SALE_YY  = '{item["MonthYear"]}' ");
                    if (result.Length > 0)
                    {
                        RadGridView_ShowHD.Rows[i].Cells[headF].Value = result[0]["SALEQTY"].ToString();
                        sumAll += Convert.ToDouble(result[0]["SALEQTY"].ToString());
                        count_M++;
                    }
                }

                double avg = (sumAll) / (count_M);
                if (sumAll == 0) avg = 0;

                double saleMM = (Convert.ToDouble(RadGridView_ShowHD.Rows[i].Cells["COUNTED"].Value.ToString())) / avg;
                if ((Convert.ToDouble(RadGridView_ShowHD.Rows[i].Cells["COUNTED"].Value.ToString()) > 0) && (sumAll == 0)) saleMM = 100;

                RadGridView_ShowHD.Rows[i].Cells["SUMALL"].Value = sumAll;
                RadGridView_ShowHD.Rows[i].Cells["SUMMONTH"].Value = count_M;
                RadGridView_ShowHD.Rows[i].Cells["AVG"].Value = avg;

                RadGridView_ShowHD.Rows[i].Cells["SaleMonth"].Value = saleMM;
            }

            //dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //Report Case 2
        void RptCase2()
        {
            this.Cursor = Cursors.WaitCursor;

            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                this.RadGridView_ShowHD.Columns.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                summaryRowItem.Clear();
            }
            //string dateSql = $@" AND CONVERT(VARCHAR,DATECOUNTING,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' ";
            //string dpt = "";
            //if (RadCheckBox_1.Checked == true) dpt = $@" AND EMPLTABLE.DIMENSION = '{RadDropDownList_1.SelectedValue}' ";
            //string stockID = "";
            //if (RadCheckBox_2.Checked == true)
            //{
            //    stockID = $@" AND SHOP_COUNTING_RC.STOCKID LIKE '{RadDropDownList_2.SelectedValue}%' ";
            //}

            string dateSql = $@" AND CONVERT(VARCHAR,DATECOUNTING,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' ";
            string dptID = "";
            if (RadCheckBox_1.Checked == true) dptID = RadDropDownList_1.SelectedValue.ToString();

            string stockID = "";
            if (RadCheckBox_2.Checked == true) stockID = $@" AND SHOP_COUNTING_RC.STOCKID LIKE '{RadDropDownList_2.SelectedValue}%' ";

            dt_Data = CheckStockClass.MNCS_ReportCountItemByWhoID("0", dptID, dateSql, stockID, "");

            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("การนับสต็อก");
                this.Cursor = Cursors.Default;
                return;
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 200)));
            RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
            RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;

            DataTable dtDay = Controllers.DateTimeSettingClass.GetDayByDate(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));

            foreach (DataRow item in dtDay.Rows)
            {
                string valueF = item["Day"].ToString();
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(valueF, valueF, 100)));

                //sum col
                GridViewSummaryItem summaryItem = new GridViewSummaryItem(valueF, "{0:n2}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItem);
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMALL", "รายการรวม", 120)));
            GridViewSummaryItem summaryItemS = new GridViewSummaryItem("SUMALL", "{0:n2}", GridAggregateFunction.Sum);
            summaryRowItem.Add(summaryItemS);

            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            DataTable dtCount = CheckStockClass.MNCS_ReportCountItemByWhoID("1", dptID, dateSql, stockID, "");

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                double sumAll = 0;
                foreach (DataRow item in dtDay.Rows)
                {
                    string headF = item["Day"].ToString();
                    double countCheck = 0;
                    DataRow[] result = dtCount.Select($@"EMPLID = '{dt_Data.Rows[i]["EMPLID"]}' AND DATECOUNTING  = '{headF}' ");
                    if (result.Length > 0) countCheck = Convert.ToDouble(result[0]["COUNTCHECK"].ToString());

                    RadGridView_ShowHD.Rows[i].Cells[headF].Value = countCheck;
                    if (countCheck == 0)
                    {
                        RadGridView_ShowHD.Rows[i].Cells[headF].Style.ForeColor = ConfigClass.SetColor_Red();
                    }

                    sumAll += countCheck;
                }
                RadGridView_ShowHD.Rows[i].Cells["SUMALL"].Value = sumAll;
            }

            this.Cursor = Cursors.Default;
        }
        //Report Case 3
        void RptCase3()
        {
            this.Cursor = Cursors.WaitCursor;

            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                this.RadGridView_ShowHD.Columns.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                summaryRowItem.Clear();
            }

            string dateSql = $@" AND CONVERT(VARCHAR,DATECOUNTING,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' ";
            string dptID = "";
            //if (RadCheckBox_1.Checked == true) dpt = $@" AND EMPLTABLE.DIMENSION = '{RadDropDownList_1.SelectedValue}' ";
            if (RadCheckBox_1.Checked == true) dptID = RadDropDownList_1.SelectedValue.ToString();
            string stockID = "";
            if (RadCheckBox_2.Checked == true) stockID = $@" AND SHOP_COUNTING_RC.STOCKID LIKE '{RadDropDownList_2.SelectedValue}%' ";

            dt_Data = CheckStockClass.MNCS_ReportCountItemByWhoID("0", dptID, dateSql, stockID, "");

            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("การนับสต็อก");
                this.Cursor = Cursors.Default;
                return;
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 200)));

            DataTable dtHour = Controllers.DateTimeSettingClass.GetHour();

            foreach (DataRow item in dtHour.Rows)
            {
                string valueF = item["MyHour"].ToString();
                string display = item["MyHourName"].ToString();

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(valueF, display, 100)));

                //sum col
                GridViewSummaryItem summaryItem = new GridViewSummaryItem(valueF, "{0:n2}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItem);
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMALL", "รายการรวม", 120)));
            GridViewSummaryItem summaryItemS = new GridViewSummaryItem("SUMALL", "{0:n2}", GridAggregateFunction.Sum);
            summaryRowItem.Add(summaryItemS);

            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            DataTable dtCount = CheckStockClass.MNCS_ReportCountItemByWhoID("2", dptID, dateSql, stockID, "");

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                double sumAll = 0;
                foreach (DataRow item in dtHour.Rows)
                {
                    string headF = item["MyHour"].ToString();
                    double countCheck = 0;
                    DataRow[] result = dtCount.Select($@"EMPLID = '{dt_Data.Rows[i]["EMPLID"]}' AND TIMEHOUR  = '{headF}' ");
                    if (result.Length > 0) countCheck = Convert.ToDouble(result[0]["COUNTCHECK"].ToString());

                    RadGridView_ShowHD.Rows[i].Cells[headF].Value = countCheck;
                    if (countCheck == 0)
                    {
                        RadGridView_ShowHD.Rows[i].Cells[headF].Style.ForeColor = ConfigClass.SetColor_Red();
                    }

                    sumAll += countCheck;
                }
                RadGridView_ShowHD.Rows[i].Cells["SUMALL"].Value = sumAll;
            }

            this.Cursor = Cursors.Default;
        }

        //Report Case 4
        void RptCase4()
        {
            this.Cursor = Cursors.WaitCursor;

            string whID = "";
            if (RadCheckBox_1.Checked == true) whID = RadDropDownList_1.SelectedValue.ToString();

            string buyer = "";
            //if (RadCheckBox_2.Checked == true) { buyer = $@" AND SPC_ITEMBUYERGROUPID = '{RadDropDownList_2.SelectedValue}' "; }
            if (RadCheckBox_2.Checked == true) buyer = RadDropDownList_2.SelectedValue.ToString();

            int mm = 3;
            try { mm = Convert.ToInt32(radTextBox_3.Text); }
            catch (Exception) { radTextBox_3.Text = "3"; }


            dt_Data = CheckStockClass.MNCS_FindItemInWH(mm, whID, "MIN", buyer);

            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("สินค้าในคลัง " + whID);
                this.Cursor = Cursors.Default;
                return;
            }

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            DataTable dtQtyCheckStock = PosSaleClass.MNCS_FindLastCount(whID);
            DataTable dtQtySale = PosSaleClass.MNCS_FindQtySale(mm, whID);
            int rowS = 0;
            foreach (DataRow item in dt_Data.Rows)
            {
                double saleEnd = 0;

                double qty_Sale = 0, qty_avg = 0;

                if (dtQtySale.Rows.Count > 0)
                {
                    DataRow[] resultQtySale = dtQtySale.Select($@"ITEMID = '{item["ITEMID"]}' AND CONFIGID  = '{item["CONFIGID"]}' 
                                                AND INVENTSIZEID  = '{item["INVENTSIZEID"]}' AND INVENTCOLORID  = '{item["INVENTCOLORID"]}' ");
                    if (resultQtySale.Length > 0)
                    {
                        qty_Sale = Convert.ToDouble(resultQtySale[0]["INVENTQTY"]);
                        qty_avg = qty_Sale / mm;
                    }
                }

                string checkStk_Date = "1900-01-01";
                double checkStk_Count = 0, checkStk_Stock = 0;
                if (dtQtyCheckStock.Rows.Count > 0)
                {
                    DataRow[] resultCheckStk = dtQtyCheckStock.Select($@"ITEMID = '{item["ITEMID"]}' AND CONFIGID  = '{item["CONFIGID"]}' 
                                                AND INVENTSIZEID  = '{item["INVENTSIZEID"]}' AND INVENTCOLORID  = '{item["INVENTCOLORID"]}' ");

                    if (resultCheckStk.Length > 0)
                    {
                        checkStk_Date = resultCheckStk[0]["COUNTDATE"].ToString();
                        checkStk_Count = Convert.ToDouble(resultCheckStk[0]["COUNTEDQTY"].ToString());
                        checkStk_Stock = Convert.ToDouble(resultCheckStk[0]["AVAILPHYSICAL"].ToString());
                    }
                }


                if (checkStk_Stock > 0)
                {
                    if (qty_avg == 0) { saleEnd = 100; }
                    else { saleEnd = checkStk_Stock / qty_avg; }
                }
                double ddd = 0;
                if (checkStk_Date != "1900-01-01") ddd = (DateTime.Now - Convert.ToDateTime(checkStk_Date)).TotalDays;

                RadGridView_ShowHD.Rows[rowS].Cells["COUNTDATE"].Value = checkStk_Date;
                RadGridView_ShowHD.Rows[rowS].Cells["COUNTEDQTY"].Value = checkStk_Count;
                RadGridView_ShowHD.Rows[rowS].Cells["COUNTDAY"].Value = ddd;//จำนวนวันที่ไม่ได้

                RadGridView_ShowHD.Rows[rowS].Cells["AVAILPHYSICAL"].Value = checkStk_Stock;

                RadGridView_ShowHD.Rows[rowS].Cells["QTYSALE"].Value = qty_Sale;//ยอดขายย้อนหลัง
                RadGridView_ShowHD.Rows[rowS].Cells["AVGM"].Value = qty_avg;//เฉลี่ยยอดขาย
                RadGridView_ShowHD.Rows[rowS].Cells["SALEEND"].Value = saleEnd;//ใช้เวลาขาย

                rowS++;
            }

            this.Cursor = Cursors.Default;
        }

        //Report Case 5
        void RptCase5()
        {
            this.Cursor = Cursors.WaitCursor;

            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                this.RadGridView_ShowHD.Columns.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                summaryRowItem.Clear();
            }
            string Y = RadDropDownList_2.SelectedValue.ToString();
            string dateSql = $@" AND YEAR(DATECOUNTING) = '{Y}' ";
            string dptsql = " AND EMPLTABLE.DIMENSION LIKE 'MN%' ";
            if (RadCheckBox_1.Checked == true)
            {
                dptsql = $@" AND EMPLTABLE.DIMENSION = '{RadDropDownList_1.SelectedValue}' ";
            }
            else
            {
                if (_pPermission == "2") dptsql = $@" AND EMPLTABLE.DIMENSION LIKE 'D%' ";
            }

            dt_Data = CheckStockClass.MNCS_ReportCountItemByWhoID("4", "", dateSql, "", dptsql);

            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("การนับสต็อก");
                this.Cursor = Cursors.Default;
                return;
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัส", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM_DESC", "แผนก", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSITION", "ตำแหน่ง", 250)));
            RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
            RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;
            
            DataTable dtM = DateTimeSettingClass.GetMonthYearByDate(Y + @"-01-01", Y + @"-12-31");
            for (int i = 0; i < dtM.Rows.Count; i++)
            {
                string valueF = dtM.Rows[i]["MonthNumber2"].ToString();
                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                    (DatagridClass.AddNumberColumn_AddManualSetCenter(valueF + "_C", dtM.Rows[i]["ThaiMonth"].ToString() + Environment.NewLine + Y + Environment.NewLine + "จำนวนรายการ", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                    (DatagridClass.AddNumberColumn_AddManualSetCenter(valueF + "_D", dtM.Rows[i]["ThaiMonth"].ToString() + Environment.NewLine + Y + Environment.NewLine + "วันทำงาน", 120)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                    (DatagridClass.AddNumberColumn_AddManualSetCenter(valueF + "_V", dtM.Rows[i]["ThaiMonth"].ToString() + Environment.NewLine + Y + Environment.NewLine + "เฉลี่ย/วัน", 100)));

                GridViewSummaryItem summaryItemC = new GridViewSummaryItem(valueF + "_C", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemD = new GridViewSummaryItem(valueF + "_D", "{0:n2}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItemC); summaryRowItem.Add(summaryItemD);
            }


            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMALL", "รายการรวม", 120)));
            GridViewSummaryItem summaryItemS = new GridViewSummaryItem("SUMALL", "{0:n2}", GridAggregateFunction.Sum);
            summaryRowItem.Add(summaryItemS);

            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            DataTable dtCountItem = CheckStockClass.MNCS_ReportCountItemByWhoID("3", "", dateSql, "", dptsql);
            DataTable dtCountWork = TimeKeeper.TimeKeeperClass.GetDayOfWorkByMouth($@" AND YEAR(PW_DATE_IN) = '{Y}' ");

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                double sumAll = 0;
                string emplID = dt_Data.Rows[i]["EMPLID"].ToString();

                for (int iC = 0; iC < dtM.Rows.Count; iC++)
                {
                    string MM = dtM.Rows[iC]["MonthNumber2"].ToString();
                    double cItem = 0, cWork = 0, avg = 0;

                    DataRow[] result_Item = dtCountItem.Select($@"EMPLID = '{emplID}' AND M_MONTH  = '{MM}' ");
                    DataRow[] result_Work = dtCountWork.Select($@"EMPLID = '{emplID}' AND M_MONTH  = '{MM}' ");

                    if (result_Item.Length > 0) cItem = Convert.ToDouble(result_Item[0]["COUNTCHECK"].ToString());
                    if (result_Work.Length > 0) cWork = Convert.ToDouble(result_Work[0]["C_DAY"].ToString());

                    if (cWork > 0) avg = cItem / cWork;

                    RadGridView_ShowHD.Rows[i].Cells[MM + "_C"].Value = cItem;
                    RadGridView_ShowHD.Rows[i].Cells[MM + "_D"].Value = cWork;
                    RadGridView_ShowHD.Rows[i].Cells[MM + "_V"].Value = avg;

                    if (cItem == 0) RadGridView_ShowHD.Rows[i].Cells[MM + "_C"].Style.ForeColor = ConfigClass.SetColor_Red();

                    sumAll += cItem;
                }

                RadGridView_ShowHD.Rows[i].Cells["SUMALL"].Value = sumAll;
            }

            this.Cursor = Cursors.Default;
        }

        //Report Case 6
        void RptCase6()
        {
            this.Cursor = Cursors.WaitCursor;

            string whID = "";
            if (RadCheckBox_1.Checked == true) whID = RadDropDownList_1.SelectedValue.ToString();

            string buyer = "", buyer24 = "";
            if (RadCheckBox_2.Checked == true) { buyer = $@" AND SPC_ITEMBUYERGROUPID = '{RadDropDownList_2.SelectedValue}' "; buyer24 = $@" AND DIMENSION = '{RadDropDownList_2.SelectedValue}' "; }


            dt_Data = CheckStockClass.MNCS_FindItemInWH(3, whID, "MIN", buyer);

            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("สินค้าในคลัง " + whID);
                this.Cursor = Cursors.Default;
                return;
            }

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            DataTable checkStockHostory = CheckStockClass.MNCS_FindHistory(whID, buyer24);

            int rowS = 0;
            foreach (DataRow item in dt_Data.Rows)
            {
                DataRow[] resultHis = checkStockHostory.Select($@"ITEMID = '{item["ITEMID"]}' AND CONFIGID  = '{item["CONFIGID"].ToString().Replace("'", "")}' 
                                                AND INVENTSIZEID  = '{item["INVENTSIZEID"]}' AND INVENTCOLORID  = '{item["INVENTCOLORID"]}' ");
                int iR = resultHis.Length;
                if (iR > 5) iR = 5;
                if (iR > 0)
                {
                    for (int iC = 0; iC < iR; iC++)
                    {
                        string vF = (iC + 1).ToString();


                        double pp = Convert.ToDouble(resultHis[iC]["CPercent"].ToString());
                        //string dd = resultHis[iC]["TRANSDATE"].ToString();
                        RadGridView_ShowHD.Rows[rowS].Cells["D_" + vF].Value = resultHis[iC]["TRANSDATE"].ToString();
                        RadGridView_ShowHD.Rows[rowS].Cells["P_" + vF].Value = pp.ToString("#,###.00");

                        if (pp > 0) RadGridView_ShowHD.Rows[rowS].Cells["P_" + vF].Style.ForeColor = ConfigClass.SetColor_Blue();
                        if (pp < 0) RadGridView_ShowHD.Rows[rowS].Cells["P_" + vF].Style.ForeColor = ConfigClass.SetColor_Red();
                    }
                }

                rowS++;
            }

            this.Cursor = Cursors.Default;
        }

        //Report Case 7
        void RptCase7()
        {
            this.Cursor = Cursors.WaitCursor;

            //if (RadGridView_ShowHD.Columns.Count > 0)
            //{
            //    this.RadGridView_ShowHD.Columns.Clear();
            //    this.RadGridView_ShowHD.SummaryRowsTop.Clear();
            //    summaryRowItem.Clear();
            //}

            string dateSql = $@" AND CONVERT(VARCHAR,DATECOUNTING,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' ";
            string dptID = "";
            //string dpt = "";//
            //if (RadCheckBox_1.Checked == true) dpt = $@" AND EMPLTABLE.DIMENSION = '{RadDropDownList_1.SelectedValue}' ";
            if (RadCheckBox_1.Checked == true) dptID = RadDropDownList_1.SelectedValue.ToString();

            dt_Data = CheckStockClass.MNCS_ReportCountItemByWhoID("7", dptID, dateSql, "", "");

            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("การนับสต็อก");
                this.Cursor = Cursors.Default;
                return;
            }

            //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMALL", "รายการรวม", 120)));
            //GridViewSummaryItem summaryItemS = new GridViewSummaryItem("SUMALL", "{0:n2}", GridAggregateFunction.Sum);
            //summaryRowItem.Add(summaryItemS);

            //this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            //DataTable dtCount = MNCS_Class.MNCS_ReportCountItemByWhoID("7_2", bchStock, dateSql, "");

            //for (int i = 0; i < dt_Data.Rows.Count; i++)
            //{
            //    double countCheck = 0;
            //    DataRow[] result = dtCount.Select($@"EMPLID = '{dt_Data.Rows[i]["EMPLID"]}'");
            //    if (result.Length > 0) countCheck = Convert.ToDouble(result[0]["COUNTCHECK"].ToString());

            //    RadGridView_ShowHD.Rows[i].Cells["SUMLINE"].Value = countCheck.ToString("#,###.00"); ;
            //    RadGridView_ShowHD.Rows[i].Cells["SUMGRAND"].Value = (countCheck * 0.4).ToString("#,###.00"); ;
            //}

            this.Cursor = Cursors.Default;
        }
        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            switch (_pTypeReport)
            {
                case "0":
                    radDateTimePicker_D1.Value = DateTime.Now;
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "1":
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-90);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "2":
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-30);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        this.RadGridView_ShowHD.Columns.Clear();
                        this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                        summaryRowItem.Clear();
                    }
                    break;
                case "3":
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-7);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        this.RadGridView_ShowHD.Columns.Clear();
                        this.RadGridView_ShowHD.SummaryRowsTop.Clear();
                        summaryRowItem.Clear();
                    }
                    break;
                case "7":
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-45);
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "8":
                    radDateTimePicker_D1.Value = DateTime.Now;
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                case "9":
                    radDateTimePicker_D1.Value = DateTime.Now;
                    radDateTimePicker_D2.Value = DateTime.Now;
                    break;
                default:
                    break;
            }

            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = "NoSelect";
            switch (_pTypeReport)
            {
                case "0":
                    T = DatagridClass.ExportExcelGridView("รายงานตรวจนับสต๊อกสินค้า " + RadDropDownList_2.SelectedValue.ToString(), RadGridView_ShowHD, "1");
                    break;
                case "1":
                    T = DatagridClass.ExportExcelGridView("รายงานเปรียบเทียบยอดขายกับจำนวนสต็อกคงเหลือ " + RadDropDownList_2.SelectedValue.ToString(), RadGridView_ShowHD, "1");
                    break;
                case "2":
                    T = DatagridClass.ExportExcelGridView("รายงานจำนวนรายการเช็คสต๊อก " + RadDropDownList_1.SelectedValue.ToString(), RadGridView_ShowHD, "1");
                    break;
                case "3":
                    T = DatagridClass.ExportExcelGridView("รายงานจำนวนรายการเช็คสต๊อก[รายชั่วโมง] " + RadDropDownList_1.SelectedValue.ToString(), RadGridView_ShowHD, "1");
                    break;
                case "4":
                    T = DatagridClass.ExportExcelGridView("ประวัติการเช็คสต็อกสินค้า[หน่วยย่อย] " + RadDropDownList_1.SelectedValue.ToString(), RadGridView_ShowHD, "1");
                    break;
                case "5":
                    T = DatagridClass.ExportExcelGridView("รายงานจำนวนรายการเช็คสต๊อก[รายปี] " + RadDropDownList_2.SelectedValue.ToString(), RadGridView_ShowHD, "1");
                    break;
                case "6":
                    T = DatagridClass.ExportExcelGridView("รายงานประวัติการเช็คสต็อกสินค้า [สรุป] " + RadDropDownList_1.SelectedValue.ToString(), RadGridView_ShowHD, "1");
                    break;
                case "7":
                    T = DatagridClass.ExportExcelGridView("รายงานค่าคอมของพนักงานเเช็คสต๊อก", RadGridView_ShowHD, "1");
                    break;
                case "8":
                    T = DatagridClass.ExportExcelGridView("รายงานรายละเอียดการนับสต๊อก[พนักงาน]", RadGridView_ShowHD, "1");
                    break;
                case "9":
                    T = DatagridClass.ExportExcelGridView("รายการเช็คสต๊อกที่ไม่ลงรายการบัญชี", RadGridView_ShowHD, "1");
                    break;
                default:
                    break;
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            return;

        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            switch (_pTypeReport)
            {
                case "0":
                    if (RadCheckBox_1.Checked == true)
                    { RadDropDownList_1.Enabled = true; SetReport0_TarnsDate("0"); }
                    else { RadDropDownList_1.Enabled = false; SetReport0_TarnsDate("0"); }
                    break;
                case "1":
                    if (RadCheckBox_1.Checked == true)
                    { RadDropDownList_1.Enabled = true; SetReport0_TarnsDate("1"); }
                    else { RadDropDownList_1.Enabled = false; SetReport0_TarnsDate("1"); }
                    break;
                case "2":
                    if (RadCheckBox_1.Checked == true)
                    { RadDropDownList_1.Enabled = true; }
                    else { RadDropDownList_1.Enabled = false; }
                    break;
                case "3":
                    if (RadCheckBox_1.Checked == true)
                    { RadDropDownList_1.Enabled = true; }
                    else { RadDropDownList_1.Enabled = false; }
                    break;
                case "5":
                    if (RadCheckBox_1.Checked == true)
                    { RadDropDownList_1.Enabled = true; }
                    else { RadDropDownList_1.Enabled = false; }
                    break;
                case "6":
                    if (RadCheckBox_1.Checked == true)
                    { RadDropDownList_1.Enabled = true; }
                    else { RadDropDownList_1.Enabled = false; }
                    break;
                default:
                    break;
            }

        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

        }
        //pdf
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
        //กรณีเปลี่ยนแปลงค่า
        private void RadDropDownList_1_SelectedValueChanged(object sender, EventArgs e)
        {
            if ((RadDropDownList_1.SelectedValue is null)) return;

            switch (_pTypeReport)
            {
                case "0":
                    SetReport0_TarnsDate("0");
                    break;
                case "1":
                    SetReport0_TarnsDate("1");
                    break;
                default:
                    break;
            }
        }
        //DropSelectChage
        void SetReport0_TarnsDate(string caseID)
        {
            this.Cursor = Cursors.WaitCursor;
            string caseSend = "";

            if (SystemClass.SystemBranchID != "MN000")
            {
                caseSend = " AND SPC_InventLocationIdFrom = '" + SystemClass.SystemBranchID + "' ";
            }
            else
            {
                try
                {
                    if (RadCheckBox_1.Checked == true)//ในกรณีที่ระบุคลัง
                    {
                        caseSend = " AND SPC_InventLocationIdFrom = '" + RadDropDownList_1.SelectedValue.ToString() + "' ";
                    }
                    else
                    {
                        switch (caseBchID)
                        {
                            case "0": caseSend = " AND SPC_InventLocationIdFrom = '" + RadDropDownList_1.SelectedValue.ToString() + "' "; break;
                            case "1": caseSend = " AND SPC_InventLocationIdFrom LIKE 'MN%'"; break;
                            case "2": caseSend = " AND SPC_InventLocationIdFrom NOT LIKE 'MN%'"; break;
                            default:
                                break;
                        }
                    }
                }
                catch (Exception) { this.Cursor = Cursors.Default; }
            }

            RadDropDownList_2.DataSource = CheckStockClass.MNCS_JournalByWH(caseID, caseSend); 
            RadDropDownList_2.DisplayMember = "DisplayMember";
            RadDropDownList_2.ValueMember = "ValueMember";
            this.Cursor = Cursors.Default;
        }

        //Check INput
        private void RadTextBox_3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_pTypeReport == "4")
            {
                e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            }
        }
    }
}
