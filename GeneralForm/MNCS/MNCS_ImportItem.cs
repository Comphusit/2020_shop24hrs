﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.MNCS
{
    public partial class MNCS_ImportItem : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable DtSetGrid = new DataTable();
        readonly string _pTypeImport; //1 แผนกดูแลบิลขาเข้า 2 แผนกเบิกภายใน
        //Load
        public MNCS_ImportItem(string pTypeImport)
        {
            InitializeComponent();
            _pTypeImport = pTypeImport;
        }
        //Load
        private void MNCS_ImportItem_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement_pdf.ShowBorder = true;
            radButtonElement_Templete.ToolTipText = "Template Excel"; radButtonElement_Templete.ShowBorder = true;
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Save.ButtonElement.ToolTipText = "บันทึก"; RadButton_Save.Visible = true;

            RadButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_Apv.ButtonElement.ShowBorder = true;

            RadDropDownList_Branch.Enabled = true; radBrowseEditor_choose.Enabled = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            radLabel_Detail.Visible = true;
            radLabel_Detail.Text = "ระบุคลัง >> เลือก File Excel | กดปุ่มบันทึก > บันทึกข้อมูล | ไม่รองรับกับสินค้าที่มี S/N และ Batch | สีแดง >> สินคัายังไม่เคยนับสต็อก | สีเหลือง >> สรุปสต็อกเกิน | สีเขียว >> สรุปสต็อกขาด";

            DtSetGrid.Columns.Add("INVENTLOCATIONID");
            DtSetGrid.Columns.Add("ITEMID");
            DtSetGrid.Columns.Add("CONFIGID");
            DtSetGrid.Columns.Add("INVENTSIZEID");
            DtSetGrid.Columns.Add("INVENTCOLORID");
            DtSetGrid.Columns.Add("INVENTDIMID");
            DtSetGrid.Columns.Add("ITEMBARCODE");
            DtSetGrid.Columns.Add("SPC_ITEMNAME");
            DtSetGrid.Columns.Add("QTY_Count");
            DtSetGrid.Columns.Add("QTY_Balance");
            DtSetGrid.Columns.Add("QTY_Sum", typeof(Double));
            DtSetGrid.Columns.Add("UNITID");
            DtSetGrid.Columns.Add("QTY");
            DtSetGrid.Columns.Add("INVENTBATCHID");
            DtSetGrid.Columns.Add("INVENTSERIALID");
            DtSetGrid.Columns.Add("LastCheckDate");
            DtSetGrid.Columns.Add("LastQTY");
            DtSetGrid.Columns.Add("LastCheckEmpl");

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "คลัง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CONFIGID", "โครงแบบ")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTSIZEID", "ขนาด")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTCOLORID", "สี")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Count", "จำนวนนับ", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Balance", "คงหลือ", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Sum", "สรุปขาด-เกิน", 100)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("QTY", "อัตราส่วน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTBATCHID", "ชุดงาน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTSERIALID", "S/N")));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCheckDate", "วันที่เช็คล่าสุด", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("LastQTY", "จำนวนเช็คล่าสุด", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCheckEmpl", "ผู้ที่เช็คล่าสุด", 150)));


            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("LastCheckEmpl", "LastCheckEmpl = '?' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_ShowHD.Columns["LastCheckDate"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["LastQTY"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_ShowHD.Columns["LastCheckEmpl"].ConditionalFormattingObjectList.Add(obj2);

            DatagridClass.SetCellBackClolorByExpression("QTY_Sum", "QTY_Sum > 0 ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("QTY_Sum", "QTY_Sum < 0 ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);

            switch (_pTypeImport)
            {
                case "1":
                    radLabel_Bch.Text = "สาขา";
                    RadDropDownList_Branch.DataSource = BranchClass.GetBranchAll("'1','4'", "'1'"); ;
                    RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_Branch.ValueMember = "BRANCH_ID";
                    break;
                case "2":
                    radLabel_Bch.Text = "คลังสินค้า";
                    RadDropDownList_Branch.DataSource = Models.InventClass.Find_InventSupc("");
                    RadDropDownList_Branch.DisplayMember = "INVENTLOCATIONNAME";
                    RadDropDownList_Branch.ValueMember = "INVENTLOCATIONID";
                    break;
                default:
                    break;
            }

            ClearTxt();
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            radLabel_Docno.Text = "";
            radLabel_StatusBill.Text = "";
            if (DtSetGrid.Rows.Count > 0) DtSetGrid.Rows.Clear();

            radBrowseEditor_choose.Enabled = true;
            RadDropDownList_Branch.Enabled = true;

            RadButton_Save.Enabled = false;
            RadButton_Cancel.Enabled = false;
            RadButton_Apv.Enabled = false;

            if (!(radBrowseEditor_choose is null))
            { radBrowseEditor_choose.Value = null; }

            if (DtSetGrid.Rows.Count > 0)
            {
                DtSetGrid.Rows.Clear();
                RadGridView_ShowHD.DataSource = DtSetGrid;
                DtSetGrid.AcceptChanges();
            }
        }

        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("ปรับสต็อกสินค้าจาก Excel", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

        }
        //Document
        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeImport);
        }
        //choose sheet
        private void RadBrowseEditor_choose_ValueChanged(object sender, EventArgs e)
        {
            //Check ค่าว่างใน File ที่เลือก
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == "")) return;

            //Check File Excel
            if ((radBrowseEditor_choose.Value.Contains(".xls")) == false)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("File ที่ระบุไม่ใช่ File Excel เช็คใหม่อีกครั้ง. " + Environment.NewLine +
                    "[" + radBrowseEditor_choose.Value + @"]");
                ClearTxt();
                return;
            }

            this.Cursor = Cursors.WaitCursor;
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook excelBook = xlApp.Workbooks.Open(radBrowseEditor_choose.Value);

            DataTable DtSet = new DataTable();
            if (DtSetGrid.Rows.Count > 0) DtSetGrid.Rows.Clear();
            //ใช้ excel แค่ 2 คอลัม คือ ช่องที่ 1 [บาร์โค้ด] และ 2 [จำนวนที่ต้องการปรับสต็อก] //ชื่อจะต้องเป็น Sheet1$
            try
            {
                System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
             ($@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='{radBrowseEditor_choose.Value}';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

                System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection);
                MyCommand.TableMappings.Add("Table", "Net-informations.com");
                MyCommand.Fill(DtSet);

                foreach (DataRow row_Excel in DtSet.Rows)
                {
                    string _invent = RadDropDownList_Branch.SelectedValue.ToString();
                    string _itemBarcode = row_Excel[0].ToString();
                    double _QtyStock;

                    try
                    { _QtyStock = Convert.ToDouble(row_Excel[1].ToString()); }
                    catch (Exception) { _QtyStock = 0; }

                    //DataTable dtbarcode = ConnectionClass.SelectSQL_Main($@" CheckStock_FindData '0','{_itemBarcode}' ");
                    DataTable dtbarcode = ConnectionClass.SelectSQL_Main($@" ItembarcodeClass_GetItembarcode_ALLDeatil  '{_itemBarcode}' ");
                    if (dtbarcode.Rows.Count > 0)
                    {
                        //if ((_QtyStock == 0) && (dtbarcode.Rows[0]["SPC_ITEMACTIVE"].ToString() == "0"))
                        //{

                        //}
                        //else
                        //{
                        DataTable dtCheckLast = CheckStock_Class.MNCS_CheckLast(dtbarcode.Rows[0]["ITEMID"].ToString(), dtbarcode.Rows[0]["INVENTDIMID"].ToString(),
                       dtbarcode.Rows[0]["CONFIGID"].ToString(), dtbarcode.Rows[0]["INVENTSIZEID"].ToString(), dtbarcode.Rows[0]["INVENTCOLORID"].ToString(),
                       _invent, dtbarcode.Rows[0]["UNITID"].ToString());
                        string lastCheckDate = "", lastCheckEmp = "?";
                        double lasQty = 0;
                        if (dtCheckLast.Rows.Count > 0)
                        {
                            lastCheckDate = dtCheckLast.Rows[0]["LastCountDateTime"].ToString();
                            lasQty = Convert.ToDouble(dtCheckLast.Rows[0]["QTYCOUNT"].ToString());
                            lastCheckEmp = dtCheckLast.Rows[0]["EMPLChecker"].ToString();
                        }

                        double stock;
                        string str = string.Format(@"
                            ItembarcodeClass_FindStockByDIM '" + _invent + @"',
                                '" + dtbarcode.Rows[0]["ITEMID"].ToString() + @"',
                                '" + dtbarcode.Rows[0]["CONFIGID"].ToString() + @"',
                                '" + dtbarcode.Rows[0]["INVENTSIZEID"].ToString() + @"',
                                '" + dtbarcode.Rows[0]["INVENTCOLORID"].ToString() + @"',
                                ''    ");
                        //" + dtbarcode.Rows[0]["BatchNumGroupId"].ToString() + @"
                        DataTable dt = ConnectionClass.SelectSQL_Main(str);
                        if (dt.Rows.Count == 0) { stock = 0; }
                        else { stock = Convert.ToDouble(dt.Rows[0]["AVAILPHYSICAL"].ToString()); }

                        //double qty_Sum = Convert.ToDouble((stock - _QtyStock).ToString("#,#0.00"));
                        double qty_Sum = Convert.ToDouble((_QtyStock - stock).ToString("#,#0.00"));

                        DtSetGrid.Rows.Add(
                            _invent, dtbarcode.Rows[0]["ITEMID"].ToString(),
                            dtbarcode.Rows[0]["CONFIGID"].ToString(),
                            dtbarcode.Rows[0]["INVENTSIZEID"].ToString(),
                            dtbarcode.Rows[0]["INVENTCOLORID"].ToString(),
                            dtbarcode.Rows[0]["INVENTDIMID"].ToString(),
                            dtbarcode.Rows[0]["ITEMBARCODE"].ToString(),
                            dtbarcode.Rows[0]["SPC_ITEMNAME"].ToString(),
                            _QtyStock.ToString("#,#0.00"),
                            stock.ToString("#,#0.00"),
                            qty_Sum.ToString("#,#0.00"),
                            dtbarcode.Rows[0]["UNITID"].ToString(),
                            dtbarcode.Rows[0]["QTY"].ToString(),
                            "",
                            dtbarcode.Rows[0]["SerialNumGroupId"].ToString(),
                            lastCheckDate, lasQty.ToString("#,#0.00"),
                            lastCheckEmp
                        );
                        //}
                    }


                }

                MyConnection.Close();
                RadGridView_ShowHD.DataSource = DtSetGrid;
                DtSetGrid.AcceptChanges();

                if (RadGridView_ShowHD.Rows.Count > 0)
                {
                    RadButton_Save.Enabled = true;
                    RadDropDownList_Branch.Enabled = false;
                    radBrowseEditor_choose.Enabled = false;
                }

                excelBook.Close();
                xlApp.Quit();

                this.Cursor = Cursors.Default;
                return;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถดึงข้อมูลจาก File Excel ได้" + Environment.NewLine +
                    "ลองใหม่อีกครั้ง" + Environment.NewLine + ex.Message.ToString());
                excelBook.Close();
                xlApp.Quit();
                return;
            }

        }

        //SaveData
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกข้อมูล ?") == DialogResult.No) return;

            string invent = RadDropDownList_Branch.SelectedValue.ToString();
            string maxDocno = CheckStock_Class.MNCS_MaxDocno();

            ArrayList sqlIn24 = new ArrayList
            {
                $@"
                    INSERT INTO SHOP_COUNTING_HD
                                (STOCKID, STOCKDescription, INVENTLOCATIONID, EMPLChecker, JOURNALTYPE,
                                NAMEChecker, checkstockDate, remark, dataareaid, ModifiedUser, ModifiedName, ModifiedDate)
                    VALUES      ('{maxDocno}', '{"ปรับสต็อกสินค้าจาก Excel " + invent}', '{invent}', '{SystemClass.SystemUserID_M}', {"4"}, 
                                '{SystemClass.SystemUserName}',CONVERT(VARCHAR,GETDATE(),23), '', 'SPC', '', '', '')"
            };

            foreach (GridViewRowInfo item in RadGridView_ShowHD.Rows)
            {
                if (Convert.ToDouble(item.Cells["QTY_Count"].Value) > 10000)
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการระบุจำนวนสต็อก > 10,000 ?") == DialogResult.No) continue;
                }

                string dd = "";
                if (item.Cells["LastCheckDate"].Value.ToString() != "")
                {
                    dd = Convert.ToDateTime(item.Cells["LastCheckDate"].Value.ToString()).ToString("yyyy-MM-dd");
                }
                sqlIn24.Add($@"
                    INSERT INTO SHOP_COUNTING_DT 
                                (STOCKID, ITEMID, ITEMBARCODE, 
                                SPC_ITEMNAME, INVENTDIMID, 
                                CONFIGID, INVENTSIZEID, INVENTCOLORID, 
                                QTY_Count_front, QTY_Count_back, 
                                QTY_Balance, QTY_Sale, 
                                QTY_Sum, QTY, 
                                UNITID,INVENTBATCHID,
                                LastCheckDate,LastQTY,LastCheckEmpl) 
                    VALUES      ('{maxDocno}', '{item.Cells["ITEMID"].Value}', '{item.Cells["ITEMBARCODE"].Value}', 
                                 '{item.Cells["SPC_ITEMNAME"].Value.ToString().Replace("'", "")}', '{item.Cells["INVENTDIMID"].Value}', 
                                '{item.Cells["CONFIGID"].Value.ToString().Replace("'", "")}', '{item.Cells["INVENTSIZEID"].Value.ToString().Replace("'", "")}', '{item.Cells["INVENTCOLORID"].Value.ToString().Replace("'", "")}', 
                                '{Convert.ToDouble(item.Cells["QTY_Count"].Value)}', '0', 
                                '{Convert.ToDouble(item.Cells["QTY_Balance"].Value)}','0',
                                '{Convert.ToDouble(item.Cells["QTY_Sum"].Value)}','{Convert.ToDouble(item.Cells["QTY"].Value)}',
                                '{item.Cells["UNITID"].Value}', '{item.Cells["INVENTBATCHID"].Value}',
                                '{dd}','{Convert.ToDouble(item.Cells["LastQTY"].Value)}','{item.Cells["LastCheckEmpl"].Value}')"
                );

                sqlIn24.Add($@"
                    INSERT INTO SHOP_COUNTING_RC (EmplID, StockID, Barcode, QTYCounting, Unit ,INVENTSERIALID  ) 
                    VALUES                       ('{SystemClass.SystemUserID_M}','{maxDocno}','{item.Cells["ITEMBARCODE"].Value}',
                                                '{Convert.ToDouble(item.Cells["QTY_Count"].Value)}','{item.Cells["UNITID"].Value}','')
                ");
            }

            String T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn24);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T != "") return;

            radLabel_Docno.Text = maxDocno;
            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยันลงรายการบัญชี"; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
            RadButton_Save.Enabled = false;
            RadButton_Apv.Enabled = true;
            RadButton_Cancel.Enabled = true;

        }
        //ยกเลิกบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (radLabel_Docno.Text == "") return;

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการยกเลิกบิล {radLabel_Docno.Text} ?") == DialogResult.No) return;

            string result = CheckStock_Class.MNCS_CancleBill(radLabel_Docno.Text, SystemClass.SystemUserID_M, SystemClass.SystemUserName);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result != "") return;

            radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก";
            radLabel_StatusBill.ForeColor = Color.Red; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel();
            RadButton_Apv.Enabled = false;
            RadButton_Cancel.Enabled = false;
        }
        //ลงรายการบัญชี
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {
            if (radLabel_Docno.Text == "") return;

            DataRow rowTable = CheckStock_Class.MNCS_FindDetailByDocno(radLabel_Docno.Text).Rows[0];
            if (rowTable["status"].ToString() != "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("บิลเลขที่ " + radLabel_Docno.Text + " ได้ถูกลงรายการบัญชีไปแล้วหรือถูกยกเลิกเรียบร้อยแล้ว" + Environment.NewLine + "ไม่สามารถลงรายการบัญชีซ้ำได้อีก");
                return;
            }

            if (rowTable["STAFORAPV"].ToString() == "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("เลขที่บิลที่ระบุ ได้นับสต็อกเกิน 2-5 วันแล้ว" + Environment.NewLine +
                    "ไม่สามารถลงรายการบัญชีได้ ให้นับใหม่เท่านั้น" + Environment.NewLine +
                    "[เมื่อนับสต็อกเรียบร้อยแล้วควรลงรายการบัญชีทันทีเท่านั้น]");
                return;
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลงรายการบัญชีบิล {radLabel_Docno.Text} ? " + Environment.NewLine +
              "ไม่สามารถแก้ไขได้") == DialogResult.No) return;

            this.Cursor = Cursors.WaitCursor;
            string result = CheckStockClass.MNCS_ApvBill("0", radLabel_Docno.Text, SystemClass.SystemUserID_M, SystemClass.SystemUserName, rowTable);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ลงรายการบัญชีแล้ว";
                radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                RadButton_Apv.Enabled = false;
                RadButton_Cancel.Enabled = false;
            }
            this.Cursor = Cursors.Default;
        }

        //Template Excel
        private void RadButtonElement_Templete_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "0");
        }
    }

}
