﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.MNCS
{
    public partial class MNCS_Main : Telerik.WinControls.UI.RadForm
    {
        //string pApv;  // 0 ยังไม่ลงรายการบัญชี 1  ลงรายการบัญชีแล้ว 3 บิลยกเลิก
        DataTable dtData = new DataTable("dtData");
        readonly string _pPerminssion;//0 ลงรายการบัญชีเฉพาะแผนกตัวเองเท่านั้น 1 ลงรายการบัญชีทุกแผนก 2 ลงรายการบัญชีได้เฉพาะสาขาเท่านั้น 3 เฉพาะสาขา MN098/MN998
        string _pDocID;
        int percentCheck = 50;
        string statusLock = "1";
        int countpercen = 0;
        public MNCS_Main(string pPerminssion)
        {
            InitializeComponent();
            _pPerminssion = pPerminssion;
        }
        //Load Main
        private void MNCS_Main_Load(object sender, EventArgs e)
        {
            DataTable dtpercent = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("51", "", " ORDER BY SHOW_ID,SHOW_NAME", "0,1");
            statusLock = dtpercent.Rows[0]["STA"].ToString();
            percentCheck = Int32.Parse(dtpercent.Rows[0]["SHOW_NAME"].ToString());

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า")));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า")));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTBATCHID", "INVENTBATCHID")));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("LastCountDate", "LastCountDate")));

            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));

            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Count_front", "จำนวนนับหน้า", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Count_back", "จำนวนนับหลัง", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Balance", "คงเหลือ[AX]", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Sum", "สรุปขาด-เกิน", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PERCENT", $@"เปอร์เซ็น{Environment.NewLine}ผิดพลาด", 80)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTSERIALID", "S/N", 80)));

            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCheckDate", "วันที่เช็คล่าสุด", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("LastQTY", "จำนวนเช็คล่าสุด", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCheckEmpl", "ผู้ที่เช็คล่าสุด", 100)));

            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CN_Qty", "จำนวนเรียกคืน", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CN_Sta", "CN_Sta")));
            //RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคาเงินสด",100)));
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMPRICEALL", $@"ยอดเงินรวม{Environment.NewLine}สต็อกคงเหลือ", 130)));

            DatagridClass.SetCellBackClolorByExpression("CN_Qty", "CN_Sta = '1' ", ConfigClass.SetColor_Red(), RadGridView_Show);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("LastCheckEmpl", "LastCheckEmpl = '?' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_Show.Columns["LastCheckDate"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_Show.Columns["LastQTY"].ConditionalFormattingObjectList.Add(obj2);
            RadGridView_Show.Columns["LastCheckEmpl"].ConditionalFormattingObjectList.Add(obj2);

            this.RadGridView_Show.SummaryRowsTop.Clear();

            GridViewSummaryItem summaryItem = new GridViewSummaryItem("SUMPRICEALL", "{0:n2}", GridAggregateFunction.Sum);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem };

            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem);



            DatagridClass.SetCellBackClolorByExpression("QTY_Sum", "QTY_Sum > 0 ", ConfigClass.SetColor_YellowPastel(), RadGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("QTY_Sum", "QTY_Sum < 0 ", ConfigClass.SetColor_GreenPastel(), RadGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("PERCENT", $@"PERCENT >= {percentCheck} ", ConfigClass.SetColor_Red(), RadGridView_Show);
            DatagridClass.SetCellBackClolorByExpression("PERCENT", $@"PERCENT <= {percentCheck * (-1)} ", ConfigClass.SetColor_Red(), RadGridView_Show);

            DataTable dtBch;
            if (SystemClass.SystemBranchID == "MN000") dtBch = BranchClass.GetBranchAll("'1'", "'1'");
            else
            {
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadGridView_Show.Columns["QTY_Balance"].IsVisible = false;
                RadGridView_Show.Columns["QTY_Sum"].IsVisible = false;
            }

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เพิ่มข้อมูล";
            RadButtonElement_Find.ShowBorder = true; RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Save.ButtonElement.ToolTipText = "อนุมัติเอกสาร";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";

            RadButton_Cancel.ButtonElement.ShowBorder = true;
            radLabel_F2.Text = "ค้นหาเลขที่เอกสาร >> เลือกเลขที่เอกสาร | กดลงรายการบัญชีหรือยกเลิก | สีแดง เช็คล่าสุด >> สินคัายังไม่เคยนับสต็อก | สีแดง เรียกคืน >> ควรคืนสินค้า | สีเหลือง >> สรุปสต็อกเกิน | สีเขียว >> สรุปสต็อกขาด";
            ClearData();
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        #endregion

        //ClearData
        void ClearData()
        {
            //pApv = "0";
            radLabel_Docno.Text = "";
            radLabel_StatusBill.Text = ""; label_Sta.Text = "";
            if (dtData.Rows.Count > 0)
            {
                dtData.Rows.Clear();
            }
            RadGridView_Show.FilterDescriptors.Clear();
            RadButton_Cancel.Enabled = false;
            RadButton_Save.Enabled = false;
        }
        //Find
        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            MNCS_FindData frm = new MNCS_FindData(_pPerminssion);
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                SetDGV_Load(frm.pDocno);
                _pDocID = frm.pDocno;
            }
        }
        //set DGV
        void SetDGV_Load(string _pDocno)
        {
            this.Cursor = Cursors.WaitCursor;
            dtData = CheckStock_Class.MNCS_FindDetailByDocno(_pDocno);  //MNCS_FindDetailByDocno

            radLabel_Docno.Text = dtData.Rows[0]["STOCKID"].ToString();
            label_Sta.Text = dtData.Rows[0]["STA_COUNT"].ToString();
            if (dtData.Rows[0]["STADOC"].ToString() == "0") //ยดเลิก
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก" + " | " + dtData.Rows[0]["NUM"].ToString() + "-" + dtData.Rows[0]["DESCRIPTION"].ToString() + " | " + dtData.Rows[0]["checkstockDate"].ToString();
                radLabel_StatusBill.ForeColor = Color.Red; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); //pApv = "3";
                RadButton_Save.Enabled = false;
                RadButton_Cancel.Enabled = false;
                //pApv = "3";
            }
            else
            {
                if (dtData.Rows[0]["STAPRCDOC"].ToString() == "0")//ยังไม่ลงรายการบัญชี
                {
                    //pApv = "0";
                    radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยันลงรายการบัญชี" + " | " + dtData.Rows[0]["NUM"].ToString() + "-" + dtData.Rows[0]["DESCRIPTION"].ToString() + " | " + dtData.Rows[0]["checkstockDate"].ToString();
                    radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();

                    if (dtData.Rows[0]["STAFORAPV"].ToString() == "0")//ในกรณีที่นับสต็อกไว้เกิน 3 วัน ไม่ให้ลงรายการบัญชีแล้ว
                    {
                        radLabel_StatusBill.ForeColor = Color.Red;
                        RadButton_Save.Enabled = false;
                        RadButton_Cancel.Enabled = true;
                    }
                    else//พร้อมลงรายการบัญชี
                    {
                        countpercen = 0;
                        if ((_pPerminssion == "1") || (_pPerminssion == "2"))
                        {
                            radLabel_StatusBill.ForeColor = Color.Black;
                            RadButton_Save.Enabled = true;
                            RadButton_Cancel.Enabled = true;
                        }
                        else
                        {
                            if (dtData.Rows[0]["NUM"].ToString() == SystemClass.SystemDptID)
                            {
                                radLabel_StatusBill.ForeColor = Color.Black;
                                RadButton_Save.Enabled = true;
                                RadButton_Cancel.Enabled = true;
                            }
                            else
                            {
                                radLabel_StatusBill.ForeColor = Color.Red;
                                RadButton_Save.Enabled = false;
                                RadButton_Cancel.Enabled = false;
                            }
                        }

                        if (_pPerminssion == "2")
                        {
                            for (int i = 0; i < dtData.Rows.Count; i++)
                            {
                                if ((float.Parse(dtData.Rows[i]["PERCENT"].ToString()) >= percentCheck) || (float.Parse(dtData.Rows[i]["PERCENT"].ToString()) <= (-1 * percentCheck))) countpercen++;
                            }

                            if ((countpercen > 0) && (statusLock == "1"))
                            {
                                RadButton_Save.Enabled = false;
                                RadButton_Cancel.Enabled = false;
                            }
                        }

                    }
                }
                else//ลงรายการบัญชีแล้ว
                {
                    // pApv = "1";
                    radLabel_StatusBill.Text = "สถานะบิล : ลงรายการบัญชีแล้ว" + " | " + dtData.Rows[0]["NUM"].ToString() + "-" + dtData.Rows[0]["DESCRIPTION"].ToString() + " | " + dtData.Rows[0]["checkstockDate"].ToString();
                    radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                    RadButton_Save.Enabled = false;
                    RadButton_Cancel.Enabled = false;
                }
            }

            RadGridView_Show.DataSource = dtData;
            dtData.AcceptChanges();
            this.Cursor = Cursors.Default;
        }

        //Cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (radLabel_Docno.Text == "") return;

            string EmpID = SystemClass.SystemUserID_M;
            string EmpName = SystemClass.SystemUserName;

            if (_pPerminssion == "0")
            {
                if (SystemClass.SystemUserLevelMinimart == "0")
                {
                    TimeKeeper.ConfirmCheckTimekeep frmConfirm = new TimeKeeper.ConfirmCheckTimekeep("1");
                    if (frmConfirm.ShowDialog(this) == DialogResult.OK)
                    {
                        EmpID = frmConfirm.EmpID;
                        EmpName = frmConfirm.EmpName;
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"รหัสผู้ใช้นี้ไม่มีสิทธิ์ในการลงรายการบัญชี{ Environment.NewLine}สามารถติดต่อขอสิทธิ์เพิ่มเติมได้ที่ Centershop Tel.1022");
                        return;
                    }
                }
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการยกเลิกเลขที่บิล " + radLabel_Docno.Text + " ?.") == DialogResult.No) return;
            string result = CheckStock_Class.MNCS_CancleBill(radLabel_Docno.Text, EmpID, EmpName);// ConnectionClass.ExecuteSQL_SentServer(sqlUp, IpServerConnectClass.ConMP);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก" + " | " + dtData.Rows[0]["NUM"].ToString() + "-" + dtData.Rows[0]["DESCRIPTION"].ToString();
                radLabel_StatusBill.ForeColor = Color.Red; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); //pApv = "3";
                RadButton_Save.Enabled = false;
                RadButton_Cancel.Enabled = false;
            }

        }
        //Apv
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radLabel_Docno.Text == "") return;

            if (countpercen > 0)
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลงรายการบัญชี {Environment.NewLine} สำหรับการนับสต็อกที่มีรายการขาดเกินมากกว่าที่ตั้งค่าไว้ ?") == DialogResult.No) return;
            }

            string EmpID = SystemClass.SystemUserID_M;
            string EmpName = SystemClass.SystemUserName;

            DataRow rowTable = CheckStock_Class.MNCS_FindDetailByDocno(radLabel_Docno.Text).Rows[0];
            if (rowTable["status"].ToString() != "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"บิลเลขที่  { radLabel_Docno.Text }  ได้ถูกลงรายการบัญชีไปแล้วหรือถูกยกเลิกเรียบร้อยแล้ว { Environment.NewLine } ไม่สามารถลงรายการบัญชีซ้ำได้อีก");
                return;
            }

            if (rowTable["STAFORAPV"].ToString() == "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"เลขที่บิลที่ระบุ ได้นับสต็อกเกิน 2-5 วันแล้ว { Environment.NewLine }
                    ไม่สามารถลงรายการบัญชีได้ ให้นับใหม่เท่านั้น { Environment.NewLine }
                    [เมื่อนับสต็อกเรียบร้อยแล้วควรลงรายการบัญชีทันทีเท่านั้น]");
                return;
            }

            if (_pPerminssion == "0")
            {
                if (SystemClass.SystemUserLevelMinimart == "0")
                {
                    TimeKeeper.ConfirmCheckTimekeep frmConfirm = new TimeKeeper.ConfirmCheckTimekeep("1");
                    if (frmConfirm.ShowDialog(this) == DialogResult.OK)
                    {
                        EmpID = frmConfirm.EmpID;
                        EmpName = frmConfirm.EmpName;
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error($@"รหัสผู้ใช้นี้ไม่มีสิทธิ์ในการลงรายการบัญชี { Environment.NewLine } สามารถติดต่อขอสิทธิ์เพิ่มเติมได้ที่ Centershop Tel.1022");
                        return;
                    }
                }
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลงรายการบัญชีเลขที่บิล  { radLabel_Docno.Text }  ?.") == DialogResult.No) return;

            this.Cursor = Cursors.WaitCursor;

            string result = CheckStockClass.MNCS_ApvBill(label_Sta.Text, radLabel_Docno.Text, EmpID, EmpName, rowTable); //ConnectionClass.Execute_SameTime_2Server(sqlMP, IpServerConnectClass.ConMP, sql708, IpServerConnectClass.ConMainAX);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ลงรายการบัญชีแล้ว" + " | " + dtData.Rows[0]["NUM"].ToString() + "-" + dtData.Rows[0]["DESCRIPTION"].ToString();
                radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                RadButton_Save.Enabled = false;
                RadButton_Cancel.Enabled = false;
            }
            this.Cursor = Cursors.Default;
        }

        //refresh
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Export
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูลการนับสต็อก " + radLabel_Docno.Text, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _pPerminssion);
        }

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (_pPerminssion != "2") return; //เฉพาะ นับสต็อก
            if (dtData.Rows[0]["STAPRCDOC"].ToString() != "0") return; //ยังไม่ลงรายการบัญชี
            if (dtData.Rows[0]["STADOC"].ToString() == "0") return; //ยกเลิกแล้ว
            if (RadGridView_Show.Rows.Count == 0) return;

            switch (RadGridView_Show.CurrentColumn.Name.ToString())
            {
                case "PERCENT":
                    if (float.Parse(RadGridView_Show.CurrentRow.Cells["PERCENT"].Value.ToString()) >= percentCheck || float.Parse(RadGridView_Show.CurrentRow.Cells["PERCENT"].Value.ToString()) <= (-1 * percentCheck))
                    {
                        string itemid = RadGridView_Show.CurrentRow.Cells["ITEMID"].Value.ToString();
                        string inventdimid = RadGridView_Show.CurrentRow.Cells["INVENTDIMID"].Value.ToString();
                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการ ClearLog Error {Environment.NewLine} { RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value} {Environment.NewLine} { RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value} ?") == DialogResult.No) return;
                        MsgBoxClass.MsgBoxShow_Bill_SaveStatus(CheckStockClass.MNCS_InsertDel(_pDocID, itemid, inventdimid), _pDocID, "ClearLog Error ");
                        SetDGV_Load(_pDocID);

                    }
                    break;
                default:
                    break;
            }
        }
    }
}


