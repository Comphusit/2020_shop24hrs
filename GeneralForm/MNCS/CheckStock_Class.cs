﻿using System;
using System.Data;
using PC_Shop24Hrs.Controllers;


namespace PC_Shop24Hrs.GeneralForm.MNCS
{
    class CheckStock_Class
    {
        //OK ค้นหาข้อมูลรายละเอียดการนับสต็อกตามเลขที่บิล
        public static DataTable MNCS_FindDetailLineByDocno(string _pDocno)
        {
            return ConnectionClass.SelectSQL_Main($@" Counting_FindData '2','{_pDocno}' ");
        }
        //OK ค้นหาข้อมูลรายละเอียดการนับสต็อกตามเลขที่บิล
        public static DataTable MNCS_FindDetailByDocno(string _pDocno)
        {
            return ConnectionClass.SelectSQL_Main($@" Counting_FindData '3','{_pDocno}' ");
        }
        //OK ค้นหาข้อมูลเช็คสต็อกก่อนลงรายการบัญชี
        public static DataTable MNCS_FindDetailSendAXByDocno(string _pDocno)
        {
            return ConnectionClass.SelectSQL_Main($@" Counting_FindData '1','{_pDocno}' ");
        }
        //OK ค้นหาสต็อก Onhand ล่าสุดในการนับของตัวนั้นๆ ในเลขที่เอกสารเดวกัน
        public static double MNCS_FindOnHandByDocno(string _stockId, string _itemId, string _dimId, string _serialId, string _batch)
        {
            DataTable dt = ConnectionClass.SelectSQL_Main
                ($@" Counting_OnHand '{_stockId}','{_itemId}','{_dimId}','{_serialId}','{_batch}' ");
            double ret = 0;
            if (dt.Rows.Count > 0) ret = Convert.ToDouble(dt.Rows[0]["QTY_BALANCE"].ToString());
            return ret;
        }
        //OK เลขที่เอกสาร
        public static string MNCS_MaxDocno()
        {
            DataTable dtMaxDocno = ConnectionClass.SelectSQL_Main(" config_SHOP_GENARATE_BILLAUTO  'MNCS','-','MNCS','10'  ");
            return dtMaxDocno.Rows[0]["ID"].ToString();
        }
        //OK ยกเลิกบิล
        public static string MNCS_CancleBill(string pDocno, string pEmpID, string pEmpName)
        {
            string sqlUp = $@"
                UPDATE  SHOP_COUNTING_HD 
                SET     status = '2',ModifiedUser = '{pEmpID}',ModifiedName = '{pEmpName}',ModifiedDate = GETDATE(),REMARK = REMARK + ' ยกเลิกรายการ SHOP24HRS PC'
                WHERE   STOCKID = '{pDocno}' ";
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }
        
        //OK CheckStock ล่าสุด ใช้ฟังก์ชั่นเดียวกันกับ PDA
        public static DataTable MNCS_CheckLast(string pID, string pDim, string pConFig, string pSize, string pColor, string pBch, string pUnit)
        {
            return ConnectionClass.SelectSQL_Main($@" PDA_COUNTING_FindData '0','SPC','{pID}','{pDim}','{pConFig}','{pSize}','{pColor}','','','{pBch}','{pUnit}','','0' ");
        }
        //รายการเช็คสต๊อกที่ไม่ลงรายการบัญชี
        public static DataTable MNCS_DEL(string date1)
        {
            string sql = $@"
                SELECT  *,CONVERT(VARCHAR,LastCheckDate,23)AS LastCheckChar
                FROM    [SHOP_COUNTING_DEL]
                {date1} ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
     
    }
}
