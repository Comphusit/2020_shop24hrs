﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNCS
{
    public partial class MNCS_FindData : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();

        public string pDocno;
        readonly string _pPermiossion;

        public MNCS_FindData(string pPermiossion)
        {
            InitializeComponent();
            _pPermiossion = pPermiossion;
        }
        //Load
        private void MNCS_FindData_Load(object sender, EventArgs e)
        {

            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(-2), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STAPRCDOC", "ยืนยัน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ยกเลิก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "สาขา", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STOCKDescription", "ชื่อสาขา", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STOCKID", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("checkstockDate", "วันที่บิล", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLChecker", "ผู้บันทึก", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEChecker", "ชื่อผู้บันทึก", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ModifiedUser", "ผู้ยืนยัน/ยกเลิก", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ModifiedName", "ชื่อยืนยัน/ยกเลิก", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ModifiedDate", "วันที่ยืนยัน/ยกเลิก", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_AddManual("STA_COUNT", "คำนวณสต็อก", 100)));

            RadGridView_ShowHD.Columns["STAPRCDOC"].IsPinned = true;
            RadGridView_ShowHD.Columns["STADOC"].IsPinned = true;
            RadGridView_ShowHD.Columns["INVENTLOCATIONID"].IsPinned = true;

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า")));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า")));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTBATCHID", "INVENTBATCHID")));
            //RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTSERIALID", "INVENTSERIALID")));

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Count_front", "จำนวนหน้า", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Count_back", "จำนวนหลัง", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Balance", "คงหลือ", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_Sum", "สรุปขาด-เกิน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCheckDate", "วันที่เช็คล่าสุด", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("LastQTY", "จำนวนเช็คล่าสุด", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCheckEmpl", "ผู้ที่เช็คล่าสุด", 100)));

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CN_Qty", "จำนวนเรียกคืน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CN_Sta", "CN_Sta")));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTSERIALID", "S/N", 250)));

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastCountDate", "เวลานับ", 150)));


            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            DataTable dtBch;
            if (SystemClass.SystemBranchID == "MN000")
            {
                switch (_pPermiossion)
                {
                    case "1"://centeshop MN%
                        RadCheckBox_Branch.Checked = false;
                        dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                        RadCheckBox_Branch.Enabled = true;
                        radCheckBox_Apv.Checked = false;
                        RadDropDownList_Branch.DataSource = dtBch;
                        RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
                        RadDropDownList_Branch.ValueMember = "BRANCH_ID";
                        break;
                    case "2"://แผนกเบิกถายใน D%
                        RadCheckBox_Branch.Checked = false;
                        RadCheckBox_Branch.Text = "คลัง";
                        dtBch = Models.InventClass.Find_InventSupc("");
                        RadCheckBox_Branch.Enabled = true;
                        radCheckBox_Apv.Checked = false;
                        RadDropDownList_Branch.DataSource = dtBch;
                        RadDropDownList_Branch.DisplayMember = "INVENTLOCATIONNAME";
                        RadDropDownList_Branch.ValueMember = "INVENTLOCATIONID";
                        break;
                    case "3"://แผนกเบิกถายใน BRANCH_STA = 4
                        RadCheckBox_Branch.Checked = false;
                        RadCheckBox_Branch.Text = "คลัง";
                        dtBch = BranchClass.GetBranchAll("'4'", "'1'");
                        RadCheckBox_Branch.Enabled = true;
                        radCheckBox_Apv.Checked = false;
                        RadDropDownList_Branch.DataSource = dtBch;
                        RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
                        RadDropDownList_Branch.ValueMember = "BRANCH_ID";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                RadCheckBox_Branch.Checked = true;
                dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                RadCheckBox_Branch.Enabled = false;
                radCheckBox_Apv.Checked = false;
                RadDropDownList_Branch.DataSource = dtBch;
                RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
                RadDropDownList_Branch.ValueMember = "BRANCH_ID";

                RadGridView_ShowDT.Columns["QTY_Balance"].IsVisible = false;
                RadGridView_ShowDT.Columns["QTY_Sum"].IsVisible = false;
            }



            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-2);
            radDateTimePicker_End.Value = DateTime.Now;
            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }
            //string sqlSelect = string.Format(@"
            //    SELECT  STOCKID, SUBSTRING(STOCKDescription,0,60) as STOCKDescription, INVENTLOCATIONID, EMPLChecker, JOURNALTYPE, NAMEChecker, 
            //      CONVERT(VARCHAR,checkstockDate,23) AS checkstockDate, CONVERT(VARCHAR,createDate,25) AS createDate, SHOP_COUNTING_HD.status,  remark ,
            //      CASE SHOP_COUNTING_HD.status WHEN '2' THEN '1' ELSE '0' END AS STADOC,
            //      CASE SHOP_COUNTING_HD.status WHEN '1' THEN '1' ELSE '0' END AS STAPRCDOC,
            //            DIMENSIONS.NUM,DIMENSIONS.DESCRIPTION,[ModifiedUser],[ModifiedName],CONVERT(VARCHAR,ModifiedDate,25) AS [ModifiedDate],REMARK ,
            //            CASE ISNULL(STA_COUNT,'0') WHEN '0' THEN '1' ELSE '0' END AS STA_COUNT 
            //    FROM	SHOP_COUNTING_HD WITH(NOLOCK) 
            //            INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH (NOLOCK) ON SHOP_COUNTING_HD.EMPLChecker = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
            //      INNER JOIN SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON EMPLTABLE.DIMENSION = DIMENSIONS.NUM AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0' 
            //    WHERE	CONVERT(VARCHAR,checkstockDate,23) BETWEEN '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' 
            //            AND '" + radDateTimePicker_End.Value.ToString("yyyy-MM-dd") + @"' 
            //      AND SHOP_COUNTING_HD.DATAAREAID = N'SPC'

            //");
            ////ในกรณีของสาขา
            //if (RadCheckBox_Branch.Checked == true)
            //{
            //    sqlSelect += " AND INVENTLOCATIONID = '" + RadDropDownList_Branch.SelectedValue.ToString() + @"' ";
            //}
            //else
            //{
            //    switch (_pPermiossion)
            //    {
            //        case "1": //สำหรับแผนก centershop
            //            sqlSelect += " AND INVENTLOCATIONID LIKE 'MN%' ";
            //            break;//เบิกภายใน D
            //        case "2":
            //            sqlSelect += " AND INVENTLOCATIONID NOT LIKE 'MN%' ";
            //            break;
            //        case "3"://เบิกภายใน MN
            //            sqlSelect += " AND INVENTLOCATIONID ( SELECT	BRANCH_ID FROM	SHOP_BRANCH WITH (NOLOCK) WHERE	BRANCH_STA IN ('4') ) ";
            //            break;
            //        default:
            //            break;
            //    }
            //}
            //if (radCheckBox_Apv.Checked == true)
            //{
            //    sqlSelect += @" AND CASE SHOP_COUNTING_HD.status WHEN '2' THEN '0' ELSE '1' END = '1' 
            //                    AND CASE SHOP_COUNTING_HD.status WHEN '1' THEN '1' ELSE '0' END = '0'  ";
            //}
            //sqlSelect += " ORDER BY createDate DESC ";

            string bchID = "";
            if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();
            string staApv = "";
            if (radCheckBox_Apv.Checked == true) staApv = "1";

            dtDataHD = CheckStockClass.CheckStock_FindDataHD(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
               bchID, staApv, _pPermiossion);  //ConnectionClass.SelectSQL_Main(sqlSelect);
            RadGridView_ShowHD.DataSource = dtDataHD;
            dtDataHD.AcceptChanges();

            if (dtDataHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
            }
        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            dtDataDT = CheckStock_Class.MNCS_FindDetailLineByDocno(_pDocno);
            RadGridView_ShowDT.DataSource = dtDataDT;
            dtDataDT.AcceptChanges();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if ((RadGridView_ShowHD.CurrentRow.Index == -1) || (RadGridView_ShowHD.CurrentRow.Cells["STOCKID"].Value.ToString() == "")) return;

            if (RadGridView_ShowHD.Rows.Count == 0) { if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); } return; }

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["STOCKID"].Value.ToString());
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["STOCKID"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
