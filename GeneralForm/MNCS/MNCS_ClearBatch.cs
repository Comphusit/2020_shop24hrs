﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.MNCS
{
    public partial class MNCS_ClearBatch : Telerik.WinControls.UI.RadForm
    {
        DataTable dt = new DataTable();
        //Load
        public MNCS_ClearBatch()
        {
            InitializeComponent();
        }
        //Load
        private void MNCS_ClearBatch_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement_pdf.ShowBorder = true;

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;
            RadButton_Apv.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDropDown(RadDropDownListDpt);
            DatagridClass.SetDefaultFontDropDown(RadDropDownListInvent);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);


            RadDropDownListInvent.DataSource = Models.InventClass.Find_InventSupc("");
            RadDropDownListInvent.ValueMember = "INVENTLOCATIONID";
            RadDropDownListInvent.DisplayMember = "INVENTLOCATIONNAME";

            RadDropDownListDpt.DataSource = Models.DptClass.GetDpt_AllD();
            RadDropDownListDpt.ValueMember = "NUM";
            RadDropDownListDpt.DisplayMember = "DESCRIPTION";
            RadDropDownListDpt.SelectedValue = "D182";

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "คลัง", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMID", "รหัสสินค้า", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CONFIGID", "โครงแบบ", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTSIZEID", "ขนาด", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTCOLORID", "สี", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTDIMID", "มิติสินค้า", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("AVAILPHYSICAL", "จำนวน", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("QTY", "อัตราส่วน")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTBATCHID", "ชุดงาน", 120)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTSERIALID", "S/N")));

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            string dpt = RadDropDownListDpt.SelectedValue.ToString();
            string invent = RadDropDownListInvent.SelectedValue.ToString();

            dt = ConnectionClass.SelectSQL_Main($@" CheckStock_Batch '{invent}','{dpt}' ");
            RadGridView_ShowHD.DataSource = dt;
            dt.AcceptChanges();

            RadButton_Search.Enabled = false;
            RadDropDownListDpt.Enabled = false; RadDropDownListInvent.Enabled = false;

            if (RadGridView_ShowHD.Rows.Count > 0) RadButton_Save.Enabled = true;

            this.Cursor = Cursors.Default;
        }



        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            RadButton_Search.Enabled = true;
            RadDropDownListDpt.Enabled = true; RadDropDownListInvent.Enabled = true;

            RadButton_Save.Enabled = false;
            RadButton_Cancel.Enabled = false;
            RadButton_Apv.Enabled = false;

            radLabel_StatusBill.Text = ""; radLabel_StatusBill.BackColor = Color.Transparent;
            radLabel_Docno.Text = "";

            if (dt.Rows.Count > 0)
            {
                dt.Rows.Clear();
                RadGridView_ShowHD.DataSource = dt;
                dt.AcceptChanges();
            }

            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("สินค้าที่เคลียร์ชุดงาน", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
        }
        //Document
        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        //สร้างเอกสาร MNCS
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกข้อมูล ?") == DialogResult.No) return;


            string dpt = RadDropDownListDpt.SelectedValue.ToString();
            string invent = RadDropDownListInvent.SelectedValue.ToString();
            string maxDocno = CheckStock_Class.MNCS_MaxDocno();

            ArrayList sqlIn24 = new ArrayList
            {
                $@"
                    INSERT INTO SHOP_COUNTING_HD
                                (STOCKID, STOCKDescription, INVENTLOCATIONID, EMPLChecker, JOURNALTYPE,
                                NAMEChecker, checkstockDate, remark, dataareaid, ModifiedUser, ModifiedName, ModifiedDate)
                    VALUES      ('{maxDocno}', '{"เคลียร์สต๊อกสินค้าชุดมิติ " + dpt}', '{invent}', '{SystemClass.SystemUserID_M}', {"4"}, 
                                '{SystemClass.SystemUserName}',CONVERT(VARCHAR,GETDATE(),23), '', 'SPC', '', '', '')"
            };

            foreach (GridViewRowInfo item in RadGridView_ShowHD.Rows)
            {
                sqlIn24.Add($@"
                    INSERT INTO SHOP_COUNTING_DT 
                                (STOCKID, ITEMID, ITEMBARCODE, 
                                SPC_ITEMNAME, INVENTDIMID, 
                                CONFIGID, INVENTSIZEID, INVENTCOLORID, 
                                QTY_Count_front, QTY_Count_back, 
                                QTY_Balance, QTY_Sale, QTY_Sum, QTY, UNITID,INVENTBATCHID) 
                    VALUES      ('{maxDocno}', '{item.Cells["ITEMID"].Value}', '{item.Cells["ITEMBARCODE"].Value}', 
                                 '{item.Cells["SPC_ITEMNAME"].Value.ToString().Replace("'", "")}', '{item.Cells["INVENTDIMID"].Value}', 
                                '{item.Cells["CONFIGID"].Value.ToString().Replace("'", "")}', '{item.Cells["INVENTSIZEID"].Value.ToString().Replace("'", "")}', '{item.Cells["INVENTCOLORID"].Value.ToString().Replace("'", "")}', 
                                '0', '0', 
                                '{item.Cells["AVAILPHYSICAL"].Value}','0','0','1','{item.Cells["UNITID"].Value}', '{item.Cells["INVENTBATCHID"].Value}')"
                );
            }

            String T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn24);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T != "") return;

            radLabel_Docno.Text = maxDocno;
            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยันลงรายการบัญชี"; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
            RadButton_Save.Enabled = false;
            RadButton_Apv.Enabled = true;
            RadButton_Cancel.Enabled = true;


        }
        //ยกเลิกบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (radLabel_Docno.Text == "") return;

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการยกเลิกบิล {radLabel_Docno.Text} ?") == DialogResult.No) return;

            string result = CheckStock_Class.MNCS_CancleBill(radLabel_Docno.Text, SystemClass.SystemUserID_M, SystemClass.SystemUserName);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result != "") return;

            radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก";
            radLabel_StatusBill.ForeColor = Color.Red; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel();
            RadButton_Apv.Enabled = false;
            RadButton_Cancel.Enabled = false;
        }
        //ลงรายการบัญชี
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {
            if (radLabel_Docno.Text == "") return;

            DataRow rowTable = CheckStock_Class.MNCS_FindDetailByDocno(radLabel_Docno.Text).Rows[0];
            if (rowTable["status"].ToString() != "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("บิลเลขที่ " + radLabel_Docno.Text + " ได้ถูกลงรายการบัญชีไปแล้วหรือถูกยกเลิกเรียบร้อยแล้ว" + Environment.NewLine + "ไม่สามารถลงรายการบัญชีซ้ำได้อีก");
                return;
            }

            if (rowTable["STAFORAPV"].ToString() == "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("เลขที่บิลที่ระบุ ได้นับสต็อกเกิน 2-5 วันแล้ว" + Environment.NewLine +
                    "ไม่สามารถลงรายการบัญชีได้ ให้นับใหม่เท่านั้น" + Environment.NewLine +
                    "[เมื่อนับสต็อกเรียบร้อยแล้วควรลงรายการบัญชีทันทีเท่านั้น]");
                return;
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลงรายการบัญชีบิล {radLabel_Docno.Text} ? " + Environment.NewLine +
             "ไม่สามารถแก้ไขได้") == DialogResult.No) return;

            this.Cursor = Cursors.WaitCursor;
            string result = CheckStockClass.MNCS_ApvBill("0", radLabel_Docno.Text, SystemClass.SystemUserID_M, SystemClass.SystemUserName, rowTable);
            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ลงรายการบัญชีแล้ว";
                radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                RadButton_Apv.Enabled = false;
                RadButton_Cancel.Enabled = false;
            }
            this.Cursor = Cursors.Default;
        }
    }
}
