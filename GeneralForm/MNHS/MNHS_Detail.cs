﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.GeneralForm.MNHS
{
    public partial class MNHS_Detail : Telerik.WinControls.UI.RadForm
    {
        public string pChange;
        DataTable dtRecive = new DataTable();

        readonly string _pCstID;//รหัสลูกค้า
        readonly string _pCstName;//ชื่อลูกค้า
        readonly string _pCstSta;//สัญญา
        readonly string _pCstActive;//หยุดรับ
        readonly string _pPrice;//ระดับราคา
        readonly string _pCstRmk;//หมายเหตุ
        readonly string _pOpenType;//0 เพิ่มหรือ 1 แก้ไข 
        readonly double _pMaxLimit;//วงเงิน/วัน

        string sta_SaveIDCard;
        string sta_SaveIDCst;

        Data_CUSTOMER cust;
        public MNHS_Detail(string pCstID, string pCstName, string pCstSta, string pCstActive, string pPrice, string pCstRmk, string pOpenType, double pMaxLimit)
        {
            InitializeComponent();

            _pCstID = pCstID;
            _pOpenType = pOpenType;
            _pCstName = pCstName;
            _pCstSta = pCstSta;
            _pCstActive = pCstActive;
            _pPrice = pPrice;
            _pCstRmk = pCstRmk;
            _pMaxLimit = pMaxLimit;
        }
        //Clear
        void ClearData()
        {
            pChange = "0";
            radLabel_IdCard.Text = ""; radLabel_Name.Text = "";
            radTextBox_IdCard.Text = "";
            radTextBox_CstID.Text = "";
            radTextBox_IdCard.Text = "";
            radTextBox_Name.Text = "";
            radTextBox_Remark.Text = ""; radLabel_Price.Text = "";
            radButton_Delete.Enabled = false;
            radTextBox_CstID.Focus();
        }
        //load
        private void MNHS_Detail_Load(object sender, EventArgs e)
        {
            radCheckBox_CustSta.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Active.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radButton_SaveCst.ButtonElement.ShowBorder = true;
            radButton_Add.ButtonElement.ShowBorder = true; radButton_Add.ButtonElement.ToolTipText = "เพิ่มผู้รับสินค้า";
            RadButton_FindCst.ButtonElement.ShowBorder = true; RadButton_FindCst.ButtonElement.ToolTipText = "ค้นหาลูกค้า";
            radButton_Delete.ButtonElement.ShowBorder = true; radButton_Delete.ButtonElement.ToolTipText = "ลบรายการ";
            RadButton_pdt.ButtonElement.ShowBorder = true; RadButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CUST_PIDCARD", "รหัส", 160));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CUST_PNAME", "ชื่อผู้รับ", 240));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("CUST_STATUS", "ใช้งาน"));

            radGridView_Show.MasterTemplate.EnableFiltering = false;

            ClearData();

            if (_pOpenType == "0")
            {
                radLabel_CstID.Text = "รหัสลูกค้า [Enter]";
                radButton_SaveCst.ButtonElement.ToolTipText = "บันทึกข้อมูลลูกค้า";
                radTextBox_IdCard.Enabled = false; RadButton_FindCst.Enabled = true;
                radTextBox_Remark.Enabled = false;
                radCheckBox_CustSta.Enabled = false;
                radCheckBox_Active.Enabled = false;
                radButton_SaveCst.Enabled = false;
                radLabel_CstName.Text = "";
                radTextBox_Limit.Text = "5000";

                if (dtRecive.Rows.Count > 0)
                {
                    dtRecive.Rows.Clear();
                    radGridView_Show.DataSource = dtRecive;
                    dtRecive.AcceptChanges();
                }
                sta_SaveIDCst = "1";
                radTextBox_IdCard.Enabled = false;
                radTextBox_Name.Enabled = false;
                radButton_Add.Enabled = false;
            }
            else
            {
                sta_SaveIDCst = "2";
                radButton_SaveCst.ButtonElement.ToolTipText = "แก้ไขข้อมูลลูกค้า";
                radLabel_CstID.Text = "รหัสลูกค้า"; radTextBox_CstID.Text = _pCstID; radTextBox_CstID.Enabled = false; RadButton_FindCst.Enabled = false;
                radTextBox_IdCard.Enabled = false; radTextBox_Name.Enabled = false;
                radTextBox_Remark.Enabled = true; radTextBox_Remark.Text = _pCstRmk;
                radCheckBox_CustSta.Enabled = true;
                radCheckBox_CustSta.Enabled = true; radCheckBox_Active.Enabled = true;

                if (_pCstSta == "0") radCheckBox_CustSta.CheckState = CheckState.Unchecked; else radCheckBox_CustSta.CheckState = CheckState.Checked;
                radCheckBox_Active.Enabled = true;
                if (_pCstActive == "0") radCheckBox_Active.CheckState = CheckState.Unchecked; else radCheckBox_Active.CheckState = CheckState.Checked;

                radButton_SaveCst.Enabled = true; radButton_SaveCst.Focus();
                radLabel_CstName.Text = _pCstName;
                radLabel_Price.Text = _pPrice;
                radTextBox_Limit.Text = _pMaxLimit.ToString();
                radButton_Add.Enabled = true;
                LoadRecive(_pCstID);
            }
        }
        //ดึงข้อมูล
        void LoadRecive(string _pCstID)
        {
            string str = $@"
                SELECT	CUST_ID,CUST_PNAME,CUST_PIDCARD,CUST_STATUS 
                FROM    SHOP_MNHS_CUST_RECIVE WITH (NOLOCK) 
                WHERE	CUST_ID = '{_pCstID}'  ";
            dtRecive = ConnectionClass.SelectSQL_Main(str);
            radGridView_Show.DataSource = dtRecive;
            dtRecive.AcceptChanges();
        }

        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //Save Cst
        private void RadButton_SaveCst_Click(object sender, EventArgs e)
        {
            if (sta_SaveIDCst == "1")
            {
                if (radTextBox_CstID.Text == "") return;
                if (radCheckBox_CustSta.Checked == false)
                {
                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ยืนยันลูกค้ายังจัดการสัญญาไม่เรียบร้อย") == DialogResult.No) return;
                }
                if (radLabel_Price.Text == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่พบระดับราคาขายส่ง ให้จัดการระดับราคาใน AX ให้เรียบร้อยก่อน{Environment.NewLine}[ลองใหม่อีกครั้ง]");
                    return;
                }
                string ckSta = "0";
                if (radCheckBox_CustSta.Checked == true) ckSta = "1";
              
                string InStr = string.Format(@"
                    INSERT INTO SHOP_MNHS_CUST (RouteId,CustID,CustName,CustSta,Remark,WHOIN,WHOINNAME,MAXLIMIT) 
                            values('CREDIT_HT', '" + radTextBox_CstID.Text + @"', '" + radLabel_CstName.Text.Replace("'", " ") + @"','" + ckSta + @"',
                    '" + radTextBox_Remark.Text + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','" + Convert.ToDouble(radTextBox_Limit.Text) + @"')
                ");
                string T_IN = ConnectionClass.ExecuteSQL_Main(InStr);
                if (T_IN == "")
                {
                    pChange = "1";
                    radButton_Add.Enabled = true; radButton_Add.Focus();
                    radButton_SaveCst.Enabled = true; radButton_SaveCst.Focus();
                    radLabel_CstName.Text = _pCstName;
                    radLabel_Price.Text = _pPrice;
                    radButton_Add.Enabled = true;
                    LoadRecive(radTextBox_CstID.Text);
                }
                MsgBoxClass.MsgBoxShow_SaveStatus(T_IN);
            }
            else
            {
                string ckStaUp = "0";
                if (radCheckBox_CustSta.Checked == true) ckStaUp = "1";
               
                string ckActive = "1";
                if (radCheckBox_Active.Checked == true) ckActive = "0";
               
                string Upstr = string.Format(@"
                    UPDATE  SHOP_MNHS_CUST  
                    SET     CustSta = '" + ckStaUp + @"',CustActive = '" + ckActive + @"',
                            Remark = '" + radTextBox_Remark.Text + @"',
                            DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                            WHOUPD = '" + SystemClass.SystemUserID + @"',WHOUPDNAME = '" + SystemClass.SystemUserName + @"',
                            MAXLIMIT = '" + Convert.ToDouble(radTextBox_Limit.Text) + @"'
                    WHERE   CUSTID = '" + _pCstID + @"'
                ");
                string T_UP = ConnectionClass.ExecuteSQL_Main(Upstr);
                if (T_UP == "") pChange = "1";
                MsgBoxClass.MsgBoxShow_SaveStatus(T_UP);
            }
        }
        //add Recive
        private void RadButton_Add_Click(object sender, EventArgs e)
        {
            if (radTextBox_CstID.Text == "") return;
            
            sta_SaveIDCard = "1";
            radLabel_IdCard.Text = "ระบุ บัตร ปชช | Passport [Enter]";
            radTextBox_IdCard.Text = ""; radTextBox_IdCard.Enabled = true;
            radLabel_Name.Text = "ระบุชื่อผู้รับ [Enter เพื่อบันทึก]";
            radTextBox_Name.Text = "";
            radButton_Delete.Enabled = true;
            radTextBox_IdCard.Focus();
        }
        //Save Recive
        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if ((radTextBox_IdCard.Text == "") | (radTextBox_CstID.Text == "")) return;

                if (radTextBox_Name.Text == "") return;
                string sql = "";
                switch (sta_SaveIDCard)
                {
                    case "1":
                        if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลของผู้รับสินค้า " + radTextBox_CstID.Text + "-" + radTextBox_Name.Text) == DialogResult.No)
                        {
                            radTextBox_Name.SelectAll();
                            radTextBox_Name.Focus();
                            return;
                        }
                        sql = string.Format(@"
                            INSERT INTO SHOP_MNHS_CUST_RECIVE 
                                (CUST_ID,CUST_PNAME,CUST_PIDCARD,CUST_WHOIN)
                            VALUES ('" + radTextBox_CstID.Text + @"','" + radTextBox_Name.Text + @"','" + radTextBox_IdCard.Text + @"',
                                '" + SystemClass.SystemUserID + @"')
                        ");

                        break;
                    case "2":
                        sql = string.Format(@"
                            UPDATE  SHOP_MNHS_CUST_RECIVE
                            SET     CUST_PNAME = '" + radTextBox_Name.Text + @"',
                                    CUST_DATEUP = CONVERT(VARCHAR, GETDATE(), 25),CUST_WHOUP = '" + SystemClass.SystemUserID + @"'
                            WHERE   CUST_ID = '" + radTextBox_CstID.Text + @"' AND CUST_PIDCARD = '" + radTextBox_IdCard.Text + @"'
                        ");
                        break;
                    default:
                        break;
                }

                string T = ConnectionClass.ExecuteSQL_Main(sql);
                if (T == "")
                {
                    radLabel_IdCard.Text = ""; radLabel_Name.Text = "";
                    radTextBox_IdCard.Text = ""; radTextBox_IdCard.Enabled = false;
                    radTextBox_Name.Text = ""; radTextBox_Name.Enabled = false;
                    radButton_Delete.Enabled = false;
                    LoadRecive(radTextBox_CstID.Text);
                }
                MsgBoxClass.MsgBoxShow_SaveStatus(T);

            }
        }
        //enter Cst ID
        private void RadTextBox_CstID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_CstID.Text == "") return;

                Data_CUSTOMER dtCst = new Data_CUSTOMER(radTextBox_CstID.Text);
                this.cust = dtCst;
                if ((this.cust.Customer_ACCOUNTNUM == "") || (this.cust.Customer_ACCOUNTNUM is null))
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสลูกค้า");
                    radTextBox_CstID.Text = "";
                    radTextBox_CstID.Focus();
                }
                else
                {
                    SetAddCst();
                }
            }
        }
        //ค้นหาลูกค้า
        private void RadButton_FindCst_Click(object sender, EventArgs e)
        {

            using (ShowDataDGV_Customer ShowDataDGV_Customer = new ShowDataDGV_Customer(""))
            {
                DialogResult dr = ShowDataDGV_Customer.ShowDialog();

                if (dr == DialogResult.Yes)
                {
                    this.cust = ShowDataDGV_Customer.cust;
                    SetAddCst();
                }
            }

        }
        //SetAddCst
        void SetAddCst()
        {
            string chkCst = $@"
                SELECT  *   FROM    SHOP_MNHS_CUST  WITH (NOLOCK)
                WHERE   CustID = '{this.cust.Customer_ACCOUNTNUM}' " ;
            DataTable dtFind = ConnectionClass.SelectSQL_Main(chkCst);
            if (dtFind.Rows.Count > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("รหัสลูกค้า " + this.cust.Customer_ACCOUNTNUM + " - " + this.cust.Customer_NAME +
                    Environment.NewLine + "มีอยู่แล้วในระบบไม่สามารถเพิ่มซ้ำได้");
                radTextBox_CstID.Text = "";
                radTextBox_CstID.Focus();
                return;
            }
            radCheckBox_CustSta.Enabled = true; radCheckBox_Active.Enabled = true;
            radTextBox_CstID.Text = this.cust.Customer_ACCOUNTNUM; radTextBox_CstID.Enabled = false;
            radLabel_CstName.Text = this.cust.Customer_NAME;
            radLabel_Price.Text = this.cust.Customer_PriceGroup;
            radTextBox_Remark.Enabled = true;
            radButton_SaveCst.Enabled = true;
            radTextBox_Remark.Focus();
        }

        private void RadTextBox_IdCard_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_IdCard.Text == "")
                {
                    return;
                }

                string sql = string.Format(@"
                            SELECT  *
                            FROM    SHOP_MNHS_CUST_RECIVE WITH (NOLOCK)
                            WHERE   CUST_ID = '" + radTextBox_CstID.Text + @"' AND CUST_PIDCARD = '" + radTextBox_IdCard.Text + @"'
                        ");
                if (ConnectionClass.SelectSQL_Main(sql).Rows.Count > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ข้อมูลรหัสบัตรที่ระบุ มีอยู่แล้วไม่สามารถเพิ่มซ้ำได้" + Environment.NewLine + "ลองใหม่อีกครั้ง.");
                    radTextBox_IdCard.SelectAll();
                    radTextBox_IdCard.Focus();
                    return;
                }

                radTextBox_IdCard.Enabled = false;
                radTextBox_Name.Enabled = true;
                radTextBox_Name.Focus();
            }
        }

        //Edit Name
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;

            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            if (e.Column.Name != "CUST_STATUS")
            {
                radTextBox_IdCard.Text = radGridView_Show.CurrentRow.Cells["CUST_PIDCARD"].Value.ToString();
                radTextBox_Name.Text = radGridView_Show.CurrentRow.Cells["CUST_PNAME"].Value.ToString();

                radLabel_IdCard.Text = "บัตร ปชช | Passport";
                radLabel_Name.Text = "แก้ไขชื่อผู้รับ [Enter เพื่อบันทึก]";
                radTextBox_Name.Enabled = true;
                sta_SaveIDCard = "2";
                return;
            }

        }

        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {

            if (radGridView_Show.Rows.Count == 0) return;

            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            if (e.Column.Name == "CUST_STATUS")
            {
                string staCust, desc; ;
                if (radGridView_Show.CurrentRow.Cells["CUST_STATUS"].Value.ToString() == "1")
                {
                    staCust = "0";
                    desc = " เปลี่ยนสถานะผู้รับเป็น ไม่ใช้งาน";
                }
                else
                {
                    staCust = "1";
                    desc = " เปลี่ยนสถานะผู้รับเป็น ใช้งาน";
                }

                if (MsgBoxClass.MsgBoxShow_ConfirmEdit(desc) == DialogResult.No)
                {
                    return;
                }

                string sql = string.Format(@"
                            UPDATE  SHOP_MNHS_CUST_RECIVE  
                            SET     CUST_STATUS = '" + staCust + @"',
                                    CUST_DATEUP = CONVERT(VARCHAR, GETDATE(), 25),CUST_WHOUP = '" + SystemClass.SystemUserID + @"'
                            WHERE   CUST_ID = '" + radTextBox_CstID.Text + @"' AND CUST_PIDCARD = '" + radGridView_Show.CurrentRow.Cells["CUST_PIDCARD"].Value.ToString() + @"'
                        ");

                string T = ConnectionClass.ExecuteSQL_Main(sql);
                if (T == "")
                {
                    radGridView_Show.CurrentRow.Cells["CUST_STATUS"].Value = staCust;
                }

                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }


        }

        private void RadButton_Delete_Click(object sender, EventArgs e)
        {
            radButton_Delete.Enabled = false;
            radTextBox_IdCard.Text = ""; radTextBox_IdCard.Enabled = false;
            radTextBox_Name.Text = ""; radTextBox_Name.Enabled = false;
            radButton_Add.Focus();

        }

        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        //Check Number
        private void RadTextBox_Limit_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
