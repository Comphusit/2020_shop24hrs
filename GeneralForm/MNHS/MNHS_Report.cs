﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;

namespace PC_Shop24Hrs.GeneralForm.MNHS
{
    public partial class MNHS_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport; //  0 รายงานบิลเครดิต
        readonly string _pTypeOpen;//SUPC - SHOP

        //Load
        public MNHS_Report(string pTypeReport, string pTypeOpen)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pTypeOpen = pTypeOpen;
        }
        //Load
        private void MNHS_Report_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;


            RadDropDownList_1.DropDownListElement.Font = SystemClass.SetFontGernaral_Bold; RadDropDownList_1.Enabled = false;
            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);


            RadDropDownList_1.Visible = true;
            RadCheckBox_1.Visible = true;
            DataTable dtBch = new DataTable();
            switch (_pTypeOpen)
            {
                case "SHOP":
                    RadCheckBox_1.Enabled = true; RadCheckBox_1.CheckState = CheckState.Checked;
                    dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    break;
                case "SUPC":
                    dtBch = BranchClass.GetBranchAll("'1','2','4'", "'1'");
                    break;
                default: break;
            }

            RadDropDownList_1.DataSource = dtBch;
            RadDropDownList_1.ValueMember = "BRANCH_ID";
            RadDropDownList_1.DisplayMember = "NAME_BRANCH";

            switch (_pTypeReport)
            {
                case "0":

                    radLabel_Detail.Text = "สีแดง >> ยังไม่มีรายการใน AX";
                    radLabel_Date.Text = "ระบุวันที่บิล";


                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEACCOUNT", "รหัสลูกค้า", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CSTNAME", "ชื่อลูกค้า", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEDATE", "วันที่บิล", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PERSONNELID", "รหัสผู้รับ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PERSONNELNAME", "ชื่อผู้รับ", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SALESID", "แคชเชียร์", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อแคชเชียร์", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SALESUNIT", "หน่วย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("BARCODEAX", "AX")));

                    RadGridView_ShowHD.MasterTemplate.Columns[0].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns[1].IsPinned = true;

                    ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "BARCODEAX = '0000' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["QTY"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["SALESUNIT"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["INVOICEID"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["INVOICEACCOUNT"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["CSTNAME"].ConditionalFormattingObjectList.Add(obj2);
                    RadGridView_ShowHD.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj2);

                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add("INVOICEID", System.ComponentModel.ListSortDirection.Ascending);
                    descriptor.GroupNames.Add("BRANCH_NAME", System.ComponentModel.ListSortDirection.Ascending);
                    RadGridView_ShowHD.GroupDescriptors.Add(descriptor);

                    break;

                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();
             
            switch (_pTypeReport)
            {
                case "0":
                    //    string pConBch = "";
                    //    if (RadCheckBox_1.Checked == true)
                    //    {
                    //        pConBch = " AND POSGROUP = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
                    //    }
                    //    string sqlSelect0 = string.Format(@"
                    //    SELECT	SHOP_MNHS_HD.INVOICEID,SHOP_MNHS_HD.INVOICEACCOUNT,SHOP_MNHS_HD.NAME AS CSTNAME,  
                    //      CONVERT(VARCHAR,SHOP_MNHS_HD.INVOICEDATE,23) AS INVOICEDATE,SHOP_MNHS_HD.POSGROUP AS BRANCH_ID,BRANCH_NAME,   
                    //      SHOP_MNHS_HD.PERSONNELID, SHOP_MNHS_HD.PERSONNELNAME, SHOP_MNHS_HD.SALESID,WHONAME  AS SPC_NAME,
                    //      ISNULL(SALEAX.BARCODE,'0000') AS BARCODEAX,
                    //      ITEMBARCODE ,SHOP_MNHS_DT.NAME AS SPC_ITEMNAME,QTY,SALESUNIT

                    //    FROM	SHOP_MNHS_HD WITH (NOLOCK)   
                    //      INNER JOIN SHOP_MNHS_DT WITH (NOLOCK) ON SHOP_MNHS_HD.INVOICEID = SHOP_MNHS_DT.INVOICEID AND SHOP_MNHS_DT.DOCUTYPE = '1' 
                    //      LEFT OUTER JOIN   
                    //           (  
                    //                 SELECT	SUBSTRING(SALESTABLE.SalesID,CHARINDEX('M',SALESTABLE.SalesId),16) AS DOCNO,INVENTLOCATIONID,BARCODE,
                    //                            SUBSTRING(INVENTTRANSID,18,LEN(INVENTTRANSID)-17) AS LINENUM
                    //                 FROM	SHOP2013TMP.dbo.SALESTABLE WITH (NOLOCK) INNER JOIN SHOP2013TMP.dbo.SALESLINE WITH (NOLOCK)  
                    //                   ON SALESTABLE.SalesId = SALESLINE.SalesId  
                    //                 WHERE	SALESTABLE.SalesId LIKE '%MNHS%' AND DELIVERYDATE  BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"'     
                    //                  AND SALESTABLE.DATAAREAID = N'SPC' AND SALESLINE.DATAAREAID = N'SPC'  
                    //          )SALEAX ON SHOP_MNHS_HD.INVOICEID = SALEAX.DOCNO   AND SHOP_MNHS_DT.ITEMBARCODE = SALEAX.BARCODE
                    //                            AND SHOP_MNHS_DT.LINENUM =  SALEAX.LINENUM

                    //    WHERE	SHOP_MNHS_HD.DOCUTYPE = '1'   
                    //      AND CONVERT(VARCHAR,SHOP_MNHS_HD.INVOICEDATE,23) BETWEEN '" + pDate1 + @"' AND '" + pDate2 + @"' 
                    //      " + pConBch + @"

                    //    ORDER BY  SHOP_MNHS_HD.INVOICEID   

                    //");
                    string pConBch = "";
                    if (RadCheckBox_1.Checked == true) pConBch = RadDropDownList_1.SelectedValue.ToString();
                    dt_Data = Models.CustomerClass.GetReport_MNHSDetail(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pConBch); //ConnectionClass.SelectSQL_Main(sqlSelect0);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }


        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
        }

        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
    }
}
