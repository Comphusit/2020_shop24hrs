﻿namespace PC_Shop24Hrs.GeneralForm.MNHS
{
    partial class MNHS_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNHS_Detail));
            this.radLabel_Desc = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_IdCard = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_IdCard = new Telerik.WinControls.UI.RadLabel();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_CstID = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_CstID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_CstName = new Telerik.WinControls.UI.RadLabel();
            this.radCheckBox_CustSta = new Telerik.WinControls.UI.RadCheckBox();
            this.radButton_Add = new Telerik.WinControls.UI.RadButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radButton_Delete = new Telerik.WinControls.UI.RadButton();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Name = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_SaveCst = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox_Active = new Telerik.WinControls.UI.RadCheckBox();
            this.RadButton_FindCst = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Price = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Limit = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_IdCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_IdCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CstID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_CustSta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Delete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SaveCst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Active)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindCst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Price)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Limit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel_Desc
            // 
            this.radLabel_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Desc.Location = new System.Drawing.Point(14, 92);
            this.radLabel_Desc.Name = "radLabel_Desc";
            this.radLabel_Desc.Size = new System.Drawing.Size(65, 19);
            this.radLabel_Desc.TabIndex = 26;
            this.radLabel_Desc.Text = "หมายเหตุ";
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(14, 115);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(497, 41);
            this.radTextBox_Remark.TabIndex = 4;
            // 
            // radTextBox_IdCard
            // 
            this.radTextBox_IdCard.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_IdCard.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_IdCard.Location = new System.Drawing.Point(41, 48);
            this.radTextBox_IdCard.MaxLength = 13;
            this.radTextBox_IdCard.Name = "radTextBox_IdCard";
            this.radTextBox_IdCard.Size = new System.Drawing.Size(184, 25);
            this.radTextBox_IdCard.TabIndex = 2;
            this.radTextBox_IdCard.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_IdCard_KeyDown);
            // 
            // radLabel_IdCard
            // 
            this.radLabel_IdCard.AutoSize = false;
            this.radLabel_IdCard.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_IdCard.Location = new System.Drawing.Point(9, 21);
            this.radLabel_IdCard.Name = "radLabel_IdCard";
            this.radLabel_IdCard.Size = new System.Drawing.Size(226, 21);
            this.radLabel_IdCard.TabIndex = 30;
            this.radLabel_IdCard.Text = "ระบุ บัตร ปชช | Passport [Enter]";
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(7, 106);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(560, 296);
            this.radGridView_Show.TabIndex = 41;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellClick);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            // 
            // radLabel_CstID
            // 
            this.radLabel_CstID.AutoSize = false;
            this.radLabel_CstID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstID.Location = new System.Drawing.Point(14, 4);
            this.radLabel_CstID.Name = "radLabel_CstID";
            this.radLabel_CstID.Size = new System.Drawing.Size(148, 21);
            this.radLabel_CstID.TabIndex = 42;
            this.radLabel_CstID.Text = "รหัสลูกค้า [Enter]";
            // 
            // radTextBox_CstID
            // 
            this.radTextBox_CstID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_CstID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CstID.Location = new System.Drawing.Point(15, 26);
            this.radTextBox_CstID.MaxLength = 7;
            this.radTextBox_CstID.Name = "radTextBox_CstID";
            this.radTextBox_CstID.Size = new System.Drawing.Size(147, 25);
            this.radTextBox_CstID.TabIndex = 0;
            this.radTextBox_CstID.Text = "C001000";
            this.radTextBox_CstID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CstID_KeyDown);
            // 
            // radLabel_CstName
            // 
            this.radLabel_CstName.AutoSize = false;
            this.radLabel_CstName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_CstName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CstName.Location = new System.Drawing.Point(14, 57);
            this.radLabel_CstName.Name = "radLabel_CstName";
            this.radLabel_CstName.Size = new System.Drawing.Size(450, 23);
            this.radLabel_CstName.TabIndex = 45;
            this.radLabel_CstName.Text = "ชื่อลูกค้า";
            // 
            // radCheckBox_CustSta
            // 
            this.radCheckBox_CustSta.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_CustSta.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_CustSta.Location = new System.Drawing.Point(204, 29);
            this.radCheckBox_CustSta.Name = "radCheckBox_CustSta";
            this.radCheckBox_CustSta.Size = new System.Drawing.Size(71, 19);
            this.radCheckBox_CustSta.TabIndex = 46;
            this.radCheckBox_CustSta.Text = "มีสัญญา";
            // 
            // radButton_Add
            // 
            this.radButton_Add.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Add.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButton_Add.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Add.Location = new System.Drawing.Point(8, 76);
            this.radButton_Add.Name = "radButton_Add";
            this.radButton_Add.Size = new System.Drawing.Size(26, 26);
            this.radButton_Add.TabIndex = 52;
            this.radButton_Add.Text = "radButton3";
            this.radButton_Add.Click += new System.EventHandler(this.RadButton_Add_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radButton_Delete);
            this.groupBox1.Controls.Add(this.radLabel_F2);
            this.groupBox1.Controls.Add(this.radLabel_Name);
            this.groupBox1.Controls.Add(this.radTextBox_Name);
            this.groupBox1.Controls.Add(this.radButton_Add);
            this.groupBox1.Controls.Add(this.radGridView_Show);
            this.groupBox1.Controls.Add(this.radLabel_IdCard);
            this.groupBox1.Controls.Add(this.radTextBox_IdCard);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(4, 162);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(575, 408);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ผู้รับสินค้า";
            // 
            // radButton_Delete
            // 
            this.radButton_Delete.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Delete.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Delete.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButton_Delete.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Delete.Location = new System.Drawing.Point(7, 47);
            this.radButton_Delete.Name = "radButton_Delete";
            this.radButton_Delete.Size = new System.Drawing.Size(26, 26);
            this.radButton_Delete.TabIndex = 58;
            this.radButton_Delete.Text = "radButton3";
            this.radButton_Delete.Click += new System.EventHandler(this.RadButton_Delete_Click);
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.BorderVisible = true;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(142, 81);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(424, 19);
            this.radLabel_F2.TabIndex = 57;
            this.radLabel_F2.Text = "DoubleClick เพื่อแก้ไข ชื่อผู้รับ | กด ใช้งาน เพื่อปรับสถานะ ใช้งาน";
            this.radLabel_F2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Name.Location = new System.Drawing.Point(234, 21);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(302, 21);
            this.radLabel_Name.TabIndex = 56;
            this.radLabel_Name.Text = "ระบุชื่อผู้รับ [Enter เพื่อบันทึก]";
            // 
            // radTextBox_Name
            // 
            this.radTextBox_Name.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Name.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Name.Location = new System.Drawing.Point(234, 48);
            this.radTextBox_Name.MaxLength = 300;
            this.radTextBox_Name.Name = "radTextBox_Name";
            this.radTextBox_Name.Size = new System.Drawing.Size(332, 25);
            this.radTextBox_Name.TabIndex = 54;
            this.radTextBox_Name.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Name_KeyDown);
            // 
            // radButton_SaveCst
            // 
            this.radButton_SaveCst.BackColor = System.Drawing.Color.Transparent;
            this.radButton_SaveCst.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_SaveCst.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButton_SaveCst.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_SaveCst.Location = new System.Drawing.Point(517, 115);
            this.radButton_SaveCst.Name = "radButton_SaveCst";
            this.radButton_SaveCst.Size = new System.Drawing.Size(33, 41);
            this.radButton_SaveCst.TabIndex = 55;
            this.radButton_SaveCst.Text = "radButton3";
            this.radButton_SaveCst.Click += new System.EventHandler(this.RadButton_SaveCst_Click);
            // 
            // radCheckBox_Active
            // 
            this.radCheckBox_Active.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Active.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_Active.Location = new System.Drawing.Point(283, 29);
            this.radCheckBox_Active.Name = "radCheckBox_Active";
            this.radCheckBox_Active.Size = new System.Drawing.Size(65, 19);
            this.radCheckBox_Active.TabIndex = 56;
            this.radCheckBox_Active.Text = "หยุดรับ";
            // 
            // RadButton_FindCst
            // 
            this.RadButton_FindCst.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_FindCst.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_FindCst.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_FindCst.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButton_FindCst.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_FindCst.Location = new System.Drawing.Point(167, 27);
            this.RadButton_FindCst.Name = "RadButton_FindCst";
            this.RadButton_FindCst.Size = new System.Drawing.Size(26, 26);
            this.RadButton_FindCst.TabIndex = 64;
            this.RadButton_FindCst.Text = "radButton3";
            this.RadButton_FindCst.Click += new System.EventHandler(this.RadButton_FindCst_Click);
            // 
            // radLabel_Price
            // 
            this.radLabel_Price.AutoSize = false;
            this.radLabel_Price.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Price.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Price.Location = new System.Drawing.Point(501, 57);
            this.radLabel_Price.Name = "radLabel_Price";
            this.radLabel_Price.Size = new System.Drawing.Size(69, 23);
            this.radLabel_Price.TabIndex = 65;
            this.radLabel_Price.Text = "ระดับราคา";
            // 
            // RadButton_pdt
            // 
            this.RadButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_pdt.Location = new System.Drawing.Point(553, 4);
            this.RadButton_pdt.Name = "RadButton_pdt";
            this.RadButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.RadButton_pdt.TabIndex = 87;
            this.RadButton_pdt.Text = "radButton3";
            this.RadButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radTextBox_Limit
            // 
            this.radTextBox_Limit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Limit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Limit.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Limit.Location = new System.Drawing.Point(428, 26);
            this.radTextBox_Limit.MaxLength = 13;
            this.radTextBox_Limit.Name = "radTextBox_Limit";
            this.radTextBox_Limit.Size = new System.Drawing.Size(112, 25);
            this.radTextBox_Limit.TabIndex = 88;
            this.radTextBox_Limit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Limit_KeyPress);
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(428, 6);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(66, 19);
            this.radLabel1.TabIndex = 89;
            this.radLabel1.Text = "วงเงิน/วัน";
            // 
            // MNHS_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 572);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radTextBox_Limit);
            this.Controls.Add(this.RadButton_pdt);
            this.Controls.Add(this.radLabel_Price);
            this.Controls.Add(this.RadButton_FindCst);
            this.Controls.Add(this.radCheckBox_Active);
            this.Controls.Add(this.radButton_SaveCst);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.radCheckBox_CustSta);
            this.Controls.Add(this.radLabel_CstName);
            this.Controls.Add(this.radTextBox_CstID);
            this.Controls.Add(this.radLabel_Desc);
            this.Controls.Add(this.radTextBox_Remark);
            this.Controls.Add(this.radLabel_CstID);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MNHS_Detail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "รายละเอียดลูกค้า ระบบเครดิตมินิมาร์ท";
            this.Load += new System.EventHandler(this.MNHS_Detail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_IdCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_IdCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CstID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_CustSta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Add)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Delete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_SaveCst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Active)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_FindCst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Price)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Limit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel_Desc;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadTextBox radTextBox_IdCard;
        private Telerik.WinControls.UI.RadLabel radLabel_IdCard;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel_CstID;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CstID;
        private Telerik.WinControls.UI.RadLabel radLabel_CstName;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_CustSta;
        private Telerik.WinControls.UI.RadButton radButton_Add;
        private System.Windows.Forms.GroupBox groupBox1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Name;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private Telerik.WinControls.UI.RadButton radButton_SaveCst;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Active;
        private Telerik.WinControls.UI.RadButton RadButton_FindCst;
        private Telerik.WinControls.UI.RadLabel radLabel_Price;
        private Telerik.WinControls.UI.RadButton radButton_Delete;
        private Telerik.WinControls.UI.RadButton RadButton_pdt;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Limit;
        private Telerik.WinControls.UI.RadLabel radLabel1;
    }
}
