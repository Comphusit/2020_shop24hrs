﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.MNHS
{
    public partial class MNHS_Main : Telerik.WinControls.UI.RadForm
    {
        private DataTable dt = new DataTable();
        //Load
        public MNHS_Main()
        {
            InitializeComponent();

            RadButtonElement_pdt.ShowBorder = true;RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "เพิ่มลูกค้า";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขลูกค้า";
            radStatusStrip1.SizingGrip = false;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("Blocked", "หยุด[AX]"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("CustSta", "สัญญา"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("CustActive", "หยุดรับ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUM", "ลูกค้า", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อลูกค้า", 350));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("CreditMax", "วงเงินรวม", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("MAXLIMIT", "วงเงิน/วัน", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PriceGroup", "ระดับราคา", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PaymTermId", "วันเครดิต", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Remark", "หมายเหตุ", 350));

            //RadGridView_Show.Columns["CreditMax"].FormatString = "{0:#,##0.00}";

            radButtonElement_add.ShowBorder = true;
            radButtonElement_Edit.ShowBorder = true;

        }
        //Load Main
        private void MNHS_Main_Load(object sender, EventArgs e)
        {
            Set_DGV();
        }

        //Set DGV
        void Set_DGV()
        {
            this.Cursor = Cursors.WaitCursor;
            string sql = $@" [CashDesktop_SmartShop_Select] '8','','','' ";
            dt = ConnectionClass.SelectSQL_Main(sql);
            RadGridView_Show.DataSource = dt;
            dt.AcceptChanges();
            this.Cursor = Cursors.Default;
        }

        //SetEdit
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            SetEditAmount();
        }

        void SetEditAmount()
        {

            if ((RadGridView_Show.CurrentRow.Cells["ACCOUNTNUM"].Value.ToString() == "") || (RadGridView_Show.Rows.Count == 0)) return;

            MNHS_Detail _mnhsDetail = new MNHS_Detail(
                RadGridView_Show.CurrentRow.Cells["ACCOUNTNUM"].Value.ToString(),
                RadGridView_Show.CurrentRow.Cells["NAME"].Value.ToString(),
                RadGridView_Show.CurrentRow.Cells["CustSta"].Value.ToString(),
                RadGridView_Show.CurrentRow.Cells["CustActive"].Value.ToString(),
                RadGridView_Show.CurrentRow.Cells["PriceGroup"].Value.ToString(),
                RadGridView_Show.CurrentRow.Cells["Remark"].Value.ToString(), "1",
                Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["MAXLIMIT"].Value.ToString()));
            if (_mnhsDetail.ShowDialog(this) == DialogResult.Yes)
            {
                if (_mnhsDetail.pChange == "1") Set_DGV();
            }
            else
            {
                if (_mnhsDetail.pChange == "1") Set_DGV();
            }
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //double Click
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            SetEditAmount();
        }

        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {

            MNHS_Detail _mnhsDetail = new MNHS_Detail(
                "", "", "", "", "", "", "0", 5000);
            if (_mnhsDetail.ShowDialog(this) == DialogResult.Yes)
            {
                if (_mnhsDetail.pChange == "1")
                {
                    Set_DGV();
                }
            }
            else
            {
                if (_mnhsDetail.pChange == "1")
                {
                    Set_DGV();
                }
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }
    }
}
