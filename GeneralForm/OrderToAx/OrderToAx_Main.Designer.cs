﻿namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    partial class OrderToAx_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderToAx_Main));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radButton_ItemRound2 = new Telerik.WinControls.UI.RadButton();
            this.RadButton_OrderCustRound2 = new Telerik.WinControls.UI.RadButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButton_Round2 = new System.Windows.Forms.RadioButton();
            this.radioButton_Round1 = new System.Windows.Forms.RadioButton();
            this.RadButton_Item = new Telerik.WinControls.UI.RadButton();
            this.RadDateTimePicker_SaleB = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadDropDownList_Dept = new Telerik.WinControls.UI.RadDropDownList();
            this.RadCheckBox_Dept = new Telerik.WinControls.UI.RadCheckBox();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator8 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_ExcelGroup = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_ExcelOrder = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator7 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadioButton_Sale = new System.Windows.Forms.RadioButton();
            this.RadioButton_DateItem = new System.Windows.Forms.RadioButton();
            this.RadButton_OrderCust = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.RadDropDownList_GroupItem = new Telerik.WinControls.UI.RadDropDownList();
            this.RadCheckBox_GroupItem = new Telerik.WinControls.UI.RadCheckBox();
            this.RadDateTimePicker_SaleE = new Telerik.WinControls.UI.RadDateTimePicker();
            this.RadDateTimePicker_SendItemDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radStatusStrip2 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_DetailPdf = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_DetailMange = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.panel3 = new System.Windows.Forms.Panel();
            this.radioButton_Wha = new System.Windows.Forms.RadioButton();
            this.radioButton_Retail = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_ItemRound2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_OrderCustRound2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Item)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_SaleB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_OrderCust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_GroupItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_GroupItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_SaleE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_SendItemDate)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip2)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.radButton_ItemRound2);
            this.panel1.Controls.Add(this.RadButton_OrderCustRound2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.RadButton_Item);
            this.panel1.Controls.Add(this.RadDateTimePicker_SaleB);
            this.panel1.Controls.Add(this.RadDropDownList_Dept);
            this.panel1.Controls.Add(this.RadCheckBox_Dept);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadioButton_Sale);
            this.panel1.Controls.Add(this.RadioButton_DateItem);
            this.panel1.Controls.Add(this.RadButton_OrderCust);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Controls.Add(this.RadDropDownList_GroupItem);
            this.panel1.Controls.Add(this.RadCheckBox_GroupItem);
            this.panel1.Controls.Add(this.RadDateTimePicker_SaleE);
            this.panel1.Controls.Add(this.RadDateTimePicker_SendItemDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 1;
            // 
            // radButton_ItemRound2
            // 
            this.radButton_ItemRound2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.radButton_ItemRound2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radButton_ItemRound2.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_ItemRound2.Location = new System.Drawing.Point(10, 597);
            this.radButton_ItemRound2.Name = "radButton_ItemRound2";
            this.radButton_ItemRound2.Size = new System.Drawing.Size(175, 32);
            this.radButton_ItemRound2.TabIndex = 71;
            this.radButton_ItemRound2.Text = "บันทึกรอบ [2]";
            this.radButton_ItemRound2.ThemeName = "Fluent";
            this.radButton_ItemRound2.Click += new System.EventHandler(this.RadButton_ItemRound2_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_ItemRound2.GetChildAt(0))).Text = "บันทึกรอบ [2]";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_ItemRound2.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_ItemRound2.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ItemRound2.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ItemRound2.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ItemRound2.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ItemRound2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_ItemRound2.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_OrderCustRound2
            // 
            this.RadButton_OrderCustRound2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_OrderCustRound2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_OrderCustRound2.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_OrderCustRound2.Location = new System.Drawing.Point(10, 559);
            this.RadButton_OrderCustRound2.Name = "RadButton_OrderCustRound2";
            this.RadButton_OrderCustRound2.Size = new System.Drawing.Size(175, 32);
            this.RadButton_OrderCustRound2.TabIndex = 70;
            this.RadButton_OrderCustRound2.Text = "ออร์เดอร์ลูกค้า [2]";
            this.RadButton_OrderCustRound2.ThemeName = "Fluent";
            this.RadButton_OrderCustRound2.Click += new System.EventHandler(this.RadButton_OrderCustRound2_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_OrderCustRound2.GetChildAt(0))).Text = "ออร์เดอร์ลูกค้า [2]";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_OrderCustRound2.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_OrderCustRound2.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCustRound2.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCustRound2.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCustRound2.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCustRound2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCustRound2.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButton_Round2);
            this.panel2.Controls.Add(this.radioButton_Round1);
            this.panel2.Location = new System.Drawing.Point(10, 277);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(175, 27);
            this.panel2.TabIndex = 69;
            // 
            // radioButton_Round2
            // 
            this.radioButton_Round2.AutoSize = true;
            this.radioButton_Round2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Round2.Location = new System.Drawing.Point(100, 3);
            this.radioButton_Round2.Name = "radioButton_Round2";
            this.radioButton_Round2.Size = new System.Drawing.Size(62, 20);
            this.radioButton_Round2.TabIndex = 71;
            this.radioButton_Round2.Text = "รอบ 2";
            this.radioButton_Round2.UseVisualStyleBackColor = true;
            this.radioButton_Round2.CheckedChanged += new System.EventHandler(this.RadioButton_Round2_CheckedChanged);
            // 
            // radioButton_Round1
            // 
            this.radioButton_Round1.AutoSize = true;
            this.radioButton_Round1.Checked = true;
            this.radioButton_Round1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Round1.Location = new System.Drawing.Point(6, 3);
            this.radioButton_Round1.Name = "radioButton_Round1";
            this.radioButton_Round1.Size = new System.Drawing.Size(62, 20);
            this.radioButton_Round1.TabIndex = 70;
            this.radioButton_Round1.TabStop = true;
            this.radioButton_Round1.Text = "รอบ 1";
            this.radioButton_Round1.UseVisualStyleBackColor = true;
            this.radioButton_Round1.CheckedChanged += new System.EventHandler(this.RadioButton_Round1_CheckedChanged);
            // 
            // RadButton_Item
            // 
            this.RadButton_Item.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_Item.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Item.Location = new System.Drawing.Point(10, 521);
            this.RadButton_Item.Name = "RadButton_Item";
            this.RadButton_Item.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Item.TabIndex = 31;
            this.RadButton_Item.Text = "จัดการสินค้า";
            this.RadButton_Item.ThemeName = "Fluent";
            this.RadButton_Item.Click += new System.EventHandler(this.RadButton_Item_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Item.GetChildAt(0))).Text = "จัดการสินค้า";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Item.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Item.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Item.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Item.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Item.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Item.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Item.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadDateTimePicker_SaleB
            // 
            this.RadDateTimePicker_SaleB.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_SaleB.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_SaleB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_SaleB.Location = new System.Drawing.Point(10, 223);
            this.RadDateTimePicker_SaleB.Name = "RadDateTimePicker_SaleB";
            this.RadDateTimePicker_SaleB.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_SaleB.TabIndex = 68;
            this.RadDateTimePicker_SaleB.TabStop = false;
            this.RadDateTimePicker_SaleB.Value = new System.DateTime(((long)(0)));
            this.RadDateTimePicker_SaleB.ValueChanged += new System.EventHandler(this.RadDateTimePicker_SaleEnd_ValueChanged);
            // 
            // RadDropDownList_Dept
            // 
            this.RadDropDownList_Dept.DropDownAnimationEnabled = false;
            this.RadDropDownList_Dept.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Dept.Enabled = false;
            this.RadDropDownList_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Dept.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Dept.Location = new System.Drawing.Point(10, 66);
            this.RadDropDownList_Dept.Name = "RadDropDownList_Dept";
            this.RadDropDownList_Dept.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Dept.TabIndex = 67;
            this.RadDropDownList_Dept.Text = "radDropDownList1";
            this.RadDropDownList_Dept.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Dept_SelectedValueChanged);
            // 
            // RadCheckBox_Dept
            // 
            this.RadCheckBox_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Dept.Location = new System.Drawing.Point(10, 43);
            this.RadCheckBox_Dept.Name = "RadCheckBox_Dept";
            this.RadCheckBox_Dept.Size = new System.Drawing.Size(57, 19);
            this.RadCheckBox_Dept.TabIndex = 66;
            this.RadCheckBox_Dept.Text = "แผนก";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator8,
            this.radButtonElement_Add,
            this.commandBarSeparator3,
            this.RadButtonElement_pdt,
            this.commandBarSeparator2,
            this.radButtonElement_ExcelGroup,
            this.commandBarSeparator5,
            this.radButtonElement_ExcelOrder,
            this.commandBarSeparator7});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 65;
            // 
            // commandBarSeparator8
            // 
            this.commandBarSeparator8.Name = "commandBarSeparator8";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator8, false);
            this.commandBarSeparator8.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.UseCompatibleTextRendering = false;
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_ExcelGroup
            // 
            this.radButtonElement_ExcelGroup.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_ExcelGroup.Name = "radButtonElement_ExcelGroup";
            this.radStatusStrip1.SetSpring(this.radButtonElement_ExcelGroup, false);
            this.radButtonElement_ExcelGroup.Text = "";
            this.radButtonElement_ExcelGroup.Click += new System.EventHandler(this.RadButtonElement_ExcelGroup_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_ExcelOrder
            // 
            this.radButtonElement_ExcelOrder.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_ExcelOrder.Name = "radButtonElement_ExcelOrder";
            this.radStatusStrip1.SetSpring(this.radButtonElement_ExcelOrder, false);
            this.radButtonElement_ExcelOrder.Text = "";
            this.radButtonElement_ExcelOrder.Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            this.radButtonElement_ExcelOrder.Click += new System.EventHandler(this.RadButtonElement_ExcelOrder_Click);
            // 
            // commandBarSeparator7
            // 
            this.commandBarSeparator7.Name = "commandBarSeparator7";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator7, false);
            this.commandBarSeparator7.VisibleInOverflowMenu = false;
            // 
            // RadioButton_Sale
            // 
            this.RadioButton_Sale.AutoSize = true;
            this.RadioButton_Sale.Enabled = false;
            this.RadioButton_Sale.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadioButton_Sale.Location = new System.Drawing.Point(10, 197);
            this.RadioButton_Sale.Name = "RadioButton_Sale";
            this.RadioButton_Sale.Size = new System.Drawing.Size(76, 20);
            this.RadioButton_Sale.TabIndex = 61;
            this.RadioButton_Sale.Text = "วันที่ขาย";
            this.RadioButton_Sale.UseVisualStyleBackColor = true;
            // 
            // RadioButton_DateItem
            // 
            this.RadioButton_DateItem.AutoSize = true;
            this.RadioButton_DateItem.Checked = true;
            this.RadioButton_DateItem.Enabled = false;
            this.RadioButton_DateItem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadioButton_DateItem.Location = new System.Drawing.Point(10, 147);
            this.RadioButton_DateItem.Name = "RadioButton_DateItem";
            this.RadioButton_DateItem.Size = new System.Drawing.Size(99, 20);
            this.RadioButton_DateItem.TabIndex = 60;
            this.RadioButton_DateItem.TabStop = true;
            this.RadioButton_DateItem.Text = "วันที่ส่งสินค้า";
            this.RadioButton_DateItem.UseVisualStyleBackColor = true;
            // 
            // RadButton_OrderCust
            // 
            this.RadButton_OrderCust.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_OrderCust.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_OrderCust.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_OrderCust.Location = new System.Drawing.Point(10, 406);
            this.RadButton_OrderCust.Name = "RadButton_OrderCust";
            this.RadButton_OrderCust.Size = new System.Drawing.Size(175, 32);
            this.RadButton_OrderCust.TabIndex = 31;
            this.RadButton_OrderCust.Text = "เช็คออร์เดอร์ลูกค้า";
            this.RadButton_OrderCust.ThemeName = "Fluent";
            this.RadButton_OrderCust.Visible = false;
            this.RadButton_OrderCust.Click += new System.EventHandler(this.RadButton_OrderCust_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_OrderCust.GetChildAt(0))).Text = "เช็คออร์เดอร์ลูกค้า";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_OrderCust.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_OrderCust.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCust.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCust.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCust.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCust.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_OrderCust.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(10, 483);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 30;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadDropDownList_GroupItem
            // 
            this.RadDropDownList_GroupItem.DropDownAnimationEnabled = false;
            this.RadDropDownList_GroupItem.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_GroupItem.Enabled = false;
            this.RadDropDownList_GroupItem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_GroupItem.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_GroupItem.Location = new System.Drawing.Point(10, 116);
            this.RadDropDownList_GroupItem.Name = "RadDropDownList_GroupItem";
            this.RadDropDownList_GroupItem.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_GroupItem.TabIndex = 29;
            this.RadDropDownList_GroupItem.Text = "radDropDownList1";
            this.RadDropDownList_GroupItem.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_GroupItem_SelectedValueChanged);
            // 
            // RadCheckBox_GroupItem
            // 
            this.RadCheckBox_GroupItem.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_GroupItem.Location = new System.Drawing.Point(10, 93);
            this.RadCheckBox_GroupItem.Name = "RadCheckBox_GroupItem";
            this.RadCheckBox_GroupItem.Size = new System.Drawing.Size(82, 19);
            this.RadCheckBox_GroupItem.TabIndex = 28;
            this.RadCheckBox_GroupItem.Text = "กลุ่มสินค้า";
            this.RadCheckBox_GroupItem.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_GroupItem_ToggleStateChanged);
            // 
            // RadDateTimePicker_SaleE
            // 
            this.RadDateTimePicker_SaleE.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_SaleE.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_SaleE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_SaleE.Location = new System.Drawing.Point(10, 249);
            this.RadDateTimePicker_SaleE.Name = "RadDateTimePicker_SaleE";
            this.RadDateTimePicker_SaleE.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_SaleE.TabIndex = 27;
            this.RadDateTimePicker_SaleE.TabStop = false;
            this.RadDateTimePicker_SaleE.Value = new System.DateTime(((long)(0)));
            this.RadDateTimePicker_SaleE.ValueChanged += new System.EventHandler(this.RadDateTimePicker_SaleBegin_ValueChanged);
            // 
            // RadDateTimePicker_SendItemDate
            // 
            this.RadDateTimePicker_SendItemDate.CustomFormat = "dd/MM/yyyy";
            this.RadDateTimePicker_SendItemDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadDateTimePicker_SendItemDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.RadDateTimePicker_SendItemDate.Location = new System.Drawing.Point(10, 170);
            this.RadDateTimePicker_SendItemDate.Name = "RadDateTimePicker_SendItemDate";
            this.RadDateTimePicker_SendItemDate.Size = new System.Drawing.Size(175, 21);
            this.RadDateTimePicker_SendItemDate.TabIndex = 26;
            this.RadDateTimePicker_SendItemDate.TabStop = false;
            this.RadDateTimePicker_SendItemDate.Value = new System.DateTime(((long)(0)));
            this.RadDateTimePicker_SendItemDate.ValueChanged += new System.EventHandler(this.RadDateTimePicker_SendItemDate_ValueChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_F2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.radStatusStrip2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(3, 614);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(662, 19);
            this.radLabel_F2.TabIndex = 68;
            this.radLabel_F2.Text = "<html>สีแดง ช่อง จำนวนเอกสาร MNOR &gt;&gt; สาขายังไม่สั่ง | สีแดง ช่อง จำนวนเอกสา" +
    "ร MRT &gt;&gt; ยังไม่สร้างใบจัด MRT | สีแดง ช่อง จำนวนรายการ MRT &gt;&gt; มีใบจั" +
    "ด MRT แต่ทุกรายการเป็น 0 หมด</html>";
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 53);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(662, 555);
            this.RadGridView_Show.TabIndex = 67;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            // 
            // radStatusStrip2
            // 
            this.radStatusStrip2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator1,
            this.radButtonElement_DetailPdf,
            this.commandBarSeparator4,
            this.RadButtonElement_DetailMange,
            this.commandBarSeparator6});
            this.radStatusStrip2.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip2.Name = "radStatusStrip2";
            this.radStatusStrip2.Size = new System.Drawing.Size(662, 34);
            this.radStatusStrip2.TabIndex = 66;
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip2.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_DetailPdf
            // 
            this.radButtonElement_DetailPdf.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_DetailPdf.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButtonElement_DetailPdf.Name = "radButtonElement_DetailPdf";
            this.radStatusStrip2.SetSpring(this.radButtonElement_DetailPdf, false);
            this.radButtonElement_DetailPdf.Text = "radButtonElement1";
            this.radButtonElement_DetailPdf.UseCompatibleTextRendering = false;
            this.radButtonElement_DetailPdf.Click += new System.EventHandler(this.RadButtonElement_DetailPdf_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip2.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator4.UseCompatibleTextRendering = false;
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_DetailMange
            // 
            this.RadButtonElement_DetailMange.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.RadButtonElement_DetailMange.Name = "RadButtonElement_DetailMange";
            this.radStatusStrip2.SetSpring(this.RadButtonElement_DetailMange, false);
            this.RadButtonElement_DetailMange.Text = "";
            this.RadButtonElement_DetailMange.Click += new System.EventHandler(this.RadButtonElement_DetailMange_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip2.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Location = new System.Drawing.Point(10, 310);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(175, 52);
            this.panel3.TabIndex = 72;
            // 
            // radioButton_Wha
            // 
            this.radioButton_Wha.AutoSize = true;
            this.radioButton_Wha.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Wha.Location = new System.Drawing.Point(100, 20);
            this.radioButton_Wha.Name = "radioButton_Wha";
            this.radioButton_Wha.Size = new System.Drawing.Size(64, 20);
            this.radioButton_Wha.TabIndex = 71;
            this.radioButton_Wha.Text = "WH-A";
            this.radioButton_Wha.UseVisualStyleBackColor = true;
            // 
            // radioButton_Retail
            // 
            this.radioButton_Retail.AutoSize = true;
            this.radioButton_Retail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Retail.Location = new System.Drawing.Point(6, 19);
            this.radioButton_Retail.Name = "radioButton_Retail";
            this.radioButton_Retail.Size = new System.Drawing.Size(71, 20);
            this.radioButton_Retail.TabIndex = 70;
            this.radioButton_Retail.Text = "RETAIL";
            this.radioButton_Retail.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton_Wha);
            this.groupBox1.Controls.Add(this.radioButton_Retail);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 52);
            this.groupBox1.TabIndex = 73;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "สต็อกคลังหลัก";
            // 
            // OrderToAx_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "OrderToAx_Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "จัดการสินค้า";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OrderToAx_Main_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_ItemRound2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_OrderCustRound2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Item)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_SaleB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_OrderCust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_GroupItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_GroupItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_SaleE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDateTimePicker_SendItemDate)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_SaleE;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        protected Telerik.WinControls.UI.RadButton RadButton_OrderCust;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_SendItemDate;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_GroupItem;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_GroupItem;
        private System.Windows.Forms.RadioButton RadioButton_Sale;
        private System.Windows.Forms.RadioButton RadioButton_DateItem;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Dept;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Dept;
        private Telerik.WinControls.UI.RadDateTimePicker RadDateTimePicker_SaleB;
        protected Telerik.WinControls.UI.RadButton RadButton_Item;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButton_Round2;
        private System.Windows.Forms.RadioButton radioButton_Round1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_DetailPdf;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_DetailMange;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        protected Telerik.WinControls.UI.RadButton RadButton_OrderCustRound2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_ExcelGroup;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_ExcelOrder;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator7;
        protected Telerik.WinControls.UI.RadButton radButton_ItemRound2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator8;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton radioButton_Wha;
        private System.Windows.Forms.RadioButton radioButton_Retail;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
