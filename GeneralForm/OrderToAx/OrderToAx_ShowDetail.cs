﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_ShowDetail : Telerik.WinControls.UI.RadForm
    {
        readonly int _TypePage;
        //private string pConfigDB;
        readonly DataTable _dtRound2;
        readonly string _DateDocno, _Dept, _GroupItem;
        private enum TypePage
        {
            TypePage_OrderCustDetail = 1,
            TypePage_OrderCustAddMRTO = 2,
            TypePage_MRTODetailRound2 = 3,
            TypePage_MRTOMange = 4
        }

        //main
        public OrderToAx_ShowDetail(int TypePage, string DateDocno, string Dept, string GroupItem, DataTable dtRound2 = null)
        {
            InitializeComponent();
            _TypePage = TypePage;
            _DateDocno = DateDocno;
            _Dept = Dept;
            _GroupItem = GroupItem;
            _dtRound2 = dtRound2;
        }

        //Load
        private void OrderToAx_ShowDetail_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            radButtonElement_Add.ToolTipText = "เพิ่ม"; radButtonElement_Add.ShowBorder = true;
            RadButtonElement_Excel.ToolTipText = "Excel"; RadButtonElement_Excel.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            this.Cursor = Cursors.WaitCursor;

            switch (_TypePage)
            {
                case (int)TypePage.TypePage_OrderCustDetail:
                    this.Text = "เช็คออร์เดอร์ลูกค้า";
                    radButtonElement_Add.ToolTipText = "บันทึกจัดออร์เดอร์ลูกค้า";
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัสสาขา", 60)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNOGBranch", "สาขา", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("OrderSeq", "ลำดับ", 60)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOItemID", "รหัสสินค้า", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPODimid", "มิติสินค้า", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BARCODE", "บาร์โค้ด", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOName", "ชื่อสินค้า", 300)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("total", "จำนวน", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOUnitID", "หน่วย", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคา", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POAX", "PX", 150)));

                    RadGridView_Show.Columns["BRANCH_ID"].IsPinned = true;
                    RadGridView_Show.Columns["MNOGBranch"].IsPinned = true;

                    RadGridView_Show.Columns["MNPOItemID"].IsVisible = false;
                    RadGridView_Show.Columns["MNPODimid"].IsVisible = false;
                    RadGridView_Show.Columns["SPC_PRICEGROUP3"].IsVisible = false;

                    ConditionalFormattingObject cPOAX1 = new ConditionalFormattingObject("POAX", ConditionTypes.NotEqual, "", "", true)
                    {
                        RowBackColor = ConfigClass.SetColor_PinkPastel(),
                        CellBackColor = ConfigClass.SetColor_PinkPastel()
                    };
                    RadGridView_Show.Columns["POAX"].ConditionalFormattingObjectList.Add(cPOAX1);


                    RadGridView_Show.DataSource = MRTClass.GetOrderCust(_Dept, _DateDocno);
                    break;
                case (int)TypePage.TypePage_OrderCustAddMRTO:
                    this.Text = "บันทึกจัดออร์เดอร์ลูกค้า";
                    radButtonElement_Add.ToolTipText = "บันทึกจัดออร์เดอร์ลูกค้า";
                    radLabel_F2.Text = "สีแดง ช่องสินค้า >> ยังไม่บันทึกสินค้าเป็นใบจัด MRT | กด + >> เพื่อบันทึกออเดอร์เข้าระบบเป็นรอบ 2";
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัสสาขา", 60)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNOGBranch", "สาขา", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("OrderSeq", "ลำดับ")));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNPOItemID", "รหัสสินค้า")));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNPODimid", "มิติสินค้า")));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BARCODE", "บาร์โค๊ด", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOName", "ชื่อสินค้า", 300)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("total", "จำนวน", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNPOUnitID", "หน่วย", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคาขายปลีก", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POAX", "สถานะประมวลผล", 450)));

                    ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "OrderSeq <> 0  ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    this.RadGridView_Show.Columns["BARCODE"].ConditionalFormattingObjectList.Add(obj1);
                    this.RadGridView_Show.Columns["MNPOName"].ConditionalFormattingObjectList.Add(obj1);

                    RadGridView_Show.DataSource = _dtRound2;

                    break;

                case (int)TypePage.TypePage_MRTODetailRound2:
                    this.Text = "บันทึกสินค้ารอบ 2";
                    radButtonElement_Add.ToolTipText = "บันทึกจัดออร์เดอร์รอบ 2";

                    radLabel_F2.Text = "รายการที่แสดง >> ยังไม่บันทึกสินค้าเป็นใบจัด MRT | กด + >> เพื่อบันทึกออเดอร์เข้าระบบเป็นรอบ 2";
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("docno", "เลขที่เอกสาร", 170)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัสสาขา", 60)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 150)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("OrderSeq", "ลำดับ", 60)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 120)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ItemID", "รหัสสินค้า")));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ItemDim", "มิติสินค้า")));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Name", "ชื่อสินค้า", 300)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("total", "จำนวน", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 100)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("Price", "ราคา")));

                    RadGridView_Show.DataSource = _dtRound2;
                    break;
            }

            this.Cursor = Cursors.Default;
        }

        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            string MRTODocno, BranchId, BranchName;
            int Seq;
            switch (_TypePage)
            {
                case (int)TypePage.TypePage_OrderCustDetail: // ดูเฉยๆ
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                    break;

                case (int)TypePage.TypePage_OrderCustAddMRTO: //กรณี บันทึกออเดอร์ลูกค้าเป็นรอบ 2
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทุกการจัดออเดอร์ลูกค้าที่ยังค้างทั้งหมด ?") == DialogResult.No) return;

                    ArrayList InStrAddMRT = new ArrayList();
                    MRTODocno = string.Empty;
                    BranchId = String.Empty;

                    Seq = 1;
                    for (int iRow = 0; iRow < RadGridView_Show.RowCount; iRow++)
                    {
                        string SeqGrid = RadGridView_Show.Rows[iRow].Cells["OrderSeq"].Value.ToString();
                        if (SeqGrid == "0") continue;
                        if (SeqGrid.Equals("1"))
                        {
                            string StatusGroupItem = "0";
                            if (!(_GroupItem.Equals(""))) StatusGroupItem = "1";

                            MRTODocno = Class.ConfigClass.GetMaxINVOICEID("MRT", _Dept.Substring(1, 3), DateTime.Parse(_DateDocno).ToString("yyMMdd"), "8");

                            BranchId = RadGridView_Show.Rows[iRow].Cells["BRANCH_ID"].Value.ToString();
                            BranchName = RadGridView_Show.Rows[iRow].Cells["MNOGBranch"].Value.ToString();

                            InStrAddMRT.Add(string.Format($@"
                            INSERT INTO Android_OrderTOHD(
                                DocNo, Date, Time, 
                                UserCode, Branch,BranchName, Depart, 
                                StaDoc, StaPrcDoc, StaApvDoc, 
                                StaAx, StaRound2, Box, Remark,
                                DateUp, TimeUp, WhoUp,WhoUpName,
                                DateIn, TimeIn, WhoIn,WhoInName,
                                DateApv, TimeApv, WhoApv,
                                DateAx, TimeAx, WhoAx, 
                                Round, StaGroupItem, GroupItem, 
                                DeptGroup, StaOrderCust, StatusStock)
                             VALUES (
                                '{MRTODocno}', '{_DateDocno}', convert(varchar, getdate(), 24),
                                '{SystemClass.SystemUserID_M}', '{BranchId}','{BranchName}', '{_Dept}', 
                                '1', '0', '0', 
                                '0', '0', 0, 'ออเดอร์ลูกค้า',
                                convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{ SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                '', '', '', 
                                '', '', '',
                                '2', '{StatusGroupItem}', '{_GroupItem}',
                                '{_Dept}', '0', '0')"));
                            Seq = 1;
                        }

                        InStrAddMRT.Add(string.Format(@"INSERT INTO Android_OrderTODT(
                            DocNo, SeqNo, ItemID, 
                            ItemDim, Barcode, Name, 
                            Qty, UnitID, Price, 
                            DateIn, TimeIn, WhoIn, 
                            DateUp, TimeUp, WhoUp,
                            StatusItem,Round,StatusCheck,qtyround) 
                            VALUES('{0}', '{1}', '{2}', 
                            '{3}', '{4}', '{5}', 
                            '{6}', '{7}', '{8}',
                            convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{9}',
                            convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{9}',
                            '0', '2', '0', '{10}')",
                            MRTODocno, Seq,
                            RadGridView_Show.Rows[iRow].Cells["MNPOItemID"].Value.ToString(),
                            RadGridView_Show.Rows[iRow].Cells["MNPODimid"].Value.ToString(),
                            RadGridView_Show.Rows[iRow].Cells["BARCODE"].Value.ToString(),
                            RadGridView_Show.Rows[iRow].Cells["MNPOName"].Value.ToString(),
                            RadGridView_Show.Rows[iRow].Cells["total"].Value.ToString(),
                            RadGridView_Show.Rows[iRow].Cells["MNPOUnitID"].Value.ToString(),
                            RadGridView_Show.Rows[iRow].Cells["SPC_PRICEGROUP3"].Value.ToString(),
                            SystemClass.SystemUserID_M, '0'));
                        Seq += 1;


                        InStrAddMRT.Add(string.Format($@"
                        update  SHOP_MNOG_DT   
                        set     PROCESS_ORDER = '{SystemClass.SystemUserID_M},' + convert(varchar, getdate(), 23) + ' ' + convert(varchar, getdate(), 24) + ' ' + '{MRTODocno}'
                        from    SHOP_MNOG_HD WITH (NOLOCK)
                                INNER JOIN SHOP_MNOG_DT WITH (NOLOCK)     ON SHOP_MNOG_HD.DOCNO = SHOP_MNOG_DT.DOCNO 
                        where   SHOP_MNOG_HD.BRANCH_ID = '{BranchId}'
                                and   RECIVE_DATE='{_DateDocno}' and ITEMBARCODE='{ RadGridView_Show.Rows[iRow].Cells["BARCODE"].Value}'"
                         ));
                    }

                    if (InStrAddMRT.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"รายการออเดอร์ลูค้าบันทึกจัดสินค้าครบแล้วทุกรายการ{Environment.NewLine}ไม่ต้องบันทึกซ้ำ");
                        this.DialogResult = DialogResult.No;
                        return;
                    }
                    else
                    {
                        string T = ConnectionClass.ExecuteSQL_ArrayMain(InStrAddMRT);
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        if (T == "")
                        {
                            this.DialogResult = DialogResult.Yes;
                        }
                        else
                        {
                            this.DialogResult = DialogResult.No;
                        }
                    }



                    this.Close();
                    break;

                case (int)TypePage.TypePage_MRTODetailRound2://ประมวลผล รอบ 2
                    if (RadGridView_Show.Rows.Count == 0) return;

                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการบันทึกสินค้าทั้งหมดเป็น รอบ2 ?") == DialogResult.No) return;

                    ArrayList InStrAddNew2 = new ArrayList();
                    MRTODocno = string.Empty;
                    //BranchId = "";

                    Seq = 1;

                    for (int iRow = 0; iRow < RadGridView_Show.RowCount; iRow++)
                    {
                        string SeqGrid = RadGridView_Show.Rows[iRow].Cells["OrderSeq"].Value.ToString();

                        if (SeqGrid.Equals("1"))
                        {
                            string StatusGroupItem = "0";
                            if (!(_GroupItem.Equals(""))) StatusGroupItem = "1";

                            MRTODocno = Class.ConfigClass.GetMaxINVOICEID("MRT", _Dept.Substring(1, 3), DateTime.Parse(_DateDocno).ToString("yyMMdd"), "8");
                            BranchId = RadGridView_Show.Rows[iRow].Cells["BRANCH_ID"].Value.ToString();
                            BranchName = RadGridView_Show.Rows[iRow].Cells["BRANCH_NAME"].Value.ToString();

                            InStrAddNew2.Add(string.Format($@"
                            INSERT INTO Android_OrderTOHD(
                                DocNo, Date, Time, 
                                UserCode, Branch,BranchName, Depart, 
                                StaDoc, StaPrcDoc, StaApvDoc, 
                                StaAx, StaRound2, Box, Remark,
                                DateUp, TimeUp, WhoUp,WhoUpName,
                                DateIn, TimeIn, WhoIn,WhoInName,
                                DateApv, TimeApv, WhoApv,
                                DateAx, TimeAx, WhoAx, 
                                Round, StaGroupItem, GroupItem, 
                                DeptGroup, StaOrderCust, StatusStock)
                            VALUES('{MRTODocno}', '{_DateDocno}', convert(varchar, getdate(), 24),
                                '{SystemClass.SystemUserID_M}', '{BranchId}','{BranchName}', '{_Dept}', 
                                '1', '0', '0', 
                                '0', '0', 0, 'ประมวลผลสินค้าจัด รอบ 2',
                                convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                '', '', '', 
                                '', '', '', 
                                '2', '{StatusGroupItem}', '{_GroupItem}', 
                                '{_Dept}', '0', '0')"));
                            Seq = 1;

                            InStrAddNew2.Add($@"
                                UPDATE  Android_OrderTOHD 
                                SET     StaRound2='1',
                                        DateUp = convert(varchar,getdate(),23), TimeUp = convert(varchar,getdate(),24),
                                        WhoUp = '{SystemClass.SystemUserID_M}'
                                WHERE   DocNo = '{RadGridView_Show.Rows[iRow].Cells["docno"].Value}' ");
                        }

                        InStrAddNew2.Add(string.Format($@"
                        INSERT INTO Android_OrderTODT(
                            DocNo, SeqNo, ItemID, 
                            ItemDim, Barcode, 
                            Name, Qty, UnitID, Price, 
                            DateIn, TimeIn, WhoIn, 
                            DateUp, TimeUp, WhoUp,
                            StatusItem,Round,StatusCheck,QtyOrderCust) 
                        VALUES('{MRTODocno}', '{Seq}', '{RadGridView_Show.Rows[iRow].Cells["ItemID"].Value}', 
                            '{RadGridView_Show.Rows[iRow].Cells["ItemDim"].Value}', '{RadGridView_Show.Rows[iRow].Cells["Barcode"].Value}',
                            '{RadGridView_Show.Rows[iRow].Cells["Name"].Value}', 
                            '{RadGridView_Show.Rows[iRow].Cells["total"].Value}', '{RadGridView_Show.Rows[iRow].Cells["UnitID"].Value}', '{RadGridView_Show.Rows[iRow].Cells["Price"].Value}',
                            convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}',
                            convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}', 
                            '0', '2', '0', '0')"));

                        Seq += 1;
                    }

                    string Tf = ConnectionClass.ExecuteSQL_ArrayMain(InStrAddNew2);
                    MsgBoxClass.MsgBoxShow_SaveStatus(Tf);
                    if (Tf == "")
                    {
                        this.DialogResult = DialogResult.Yes;
                    }
                    else
                    {
                        this.DialogResult = DialogResult.No;
                    }
                    this.Close();
                    break;
            }
        }

        //Excel
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (_TypePage == (int)TypePage.TypePage_OrderCustAddMRTO)
            {
                if (RadGridView_Show.Rows.Count == 0) return;
                string T = DatagridClass.ExportExcelGridView("ออเดอร์ลูกค้า", RadGridView_Show, "1");
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }

        }
 
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
    }
}
