﻿namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    partial class OrderToAx_PrintT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderToAx_PrintT));
            this.RadGridView_ShowHD = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_PrintTag = new Telerik.WinControls.UI.RadDropDownList();
            this.radCheckBox3 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.btnInQuery = new Telerik.WinControls.UI.RadButton();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radDateTimePicker_d1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDropDownList_Route = new Telerik.WinControls.UI.RadDropDownList();
            this.radDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Dept = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_Printer = new Telerik.WinControls.UI.RadDropDownList();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_print = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_PrintBill = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_PrintTag = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_PrintTag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnInQuery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_d1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Route)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Printer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_ShowHD
            // 
            this.RadGridView_ShowHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_ShowHD.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowHD.Name = "RadGridView_ShowHD";
            // 
            // 
            // 
            this.RadGridView_ShowHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowHD.Size = new System.Drawing.Size(662, 605);
            this.RadGridView_ShowHD.TabIndex = 16;
            this.RadGridView_ShowHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_ShowHD.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_BillTran_CellClick);
            this.RadGridView_ShowHD.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_ShowHD.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_ShowHD.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 642F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowHD, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 614);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(662, 19);
            this.radLabel_Detail.TabIndex = 53;
            this.radLabel_Detail.Text = "<html>สีชมพู &gt;&gt; มีการพิมพ์เอกสารแล้ว || สีแดง &gt;&gt; เป็นรายการในบิลเดียว" +
    "กัน ไม่จำเป็นต้องเลือก</html>";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radDropDownList_PrintTag);
            this.panel1.Controls.Add(this.radCheckBox3);
            this.panel1.Controls.Add(this.radCheckBox2);
            this.panel1.Controls.Add(this.btnInQuery);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radDateTimePicker_d1);
            this.panel1.Controls.Add(this.radDropDownList_Route);
            this.panel1.Controls.Add(this.radDropDownList_Branch);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radDropDownList_Dept);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radDropDownList_Printer);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 1;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(13, 106);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(132, 19);
            this.radLabel3.TabIndex = 55;
            this.radLabel3.Text = "เลือกเครื่องพิมพ์แท็ก";
            // 
            // radDropDownList_PrintTag
            // 
            this.radDropDownList_PrintTag.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_PrintTag.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_PrintTag.Location = new System.Drawing.Point(13, 131);
            this.radDropDownList_PrintTag.Name = "radDropDownList_PrintTag";
            this.radDropDownList_PrintTag.Size = new System.Drawing.Size(169, 21);
            this.radDropDownList_PrintTag.TabIndex = 54;
            // 
            // radCheckBox3
            // 
            this.radCheckBox3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox3.Location = new System.Drawing.Point(13, 279);
            this.radCheckBox3.Name = "radCheckBox3";
            this.radCheckBox3.Size = new System.Drawing.Size(62, 19);
            this.radCheckBox3.TabIndex = 62;
            this.radCheckBox3.Text = "สายรถ";
            this.radCheckBox3.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Route_ToggleStateChanged);
            // 
            // radCheckBox2
            // 
            this.radCheckBox2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox2.Location = new System.Drawing.Point(13, 220);
            this.radCheckBox2.Name = "radCheckBox2";
            this.radCheckBox2.Size = new System.Drawing.Size(53, 19);
            this.radCheckBox2.TabIndex = 61;
            this.radCheckBox2.Text = "สาขา";
            this.radCheckBox2.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Branch_ToggleStateChanged);
            // 
            // btnInQuery
            // 
            this.btnInQuery.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnInQuery.Font = new System.Drawing.Font("Tahoma", 16.25F);
            this.btnInQuery.Location = new System.Drawing.Point(9, 401);
            this.btnInQuery.Name = "btnInQuery";
            this.btnInQuery.Size = new System.Drawing.Size(175, 32);
            this.btnInQuery.TabIndex = 60;
            this.btnInQuery.Text = "ค้นหา";
            this.btnInQuery.Click += new System.EventHandler(this.BtnInQuery_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnInQuery.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnInQuery.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnInQuery.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnInQuery.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnInQuery.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnInQuery.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnInQuery.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.btnInQuery.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(13, 335);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(33, 19);
            this.radLabel5.TabIndex = 59;
            this.radLabel5.Text = "วันที่";
            // 
            // radDateTimePicker_d1
            // 
            this.radDateTimePicker_d1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_d1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_d1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_d1.Location = new System.Drawing.Point(13, 360);
            this.radDateTimePicker_d1.Name = "radDateTimePicker_d1";
            this.radDateTimePicker_d1.Size = new System.Drawing.Size(169, 21);
            this.radDateTimePicker_d1.TabIndex = 58;
            this.radDateTimePicker_d1.TabStop = false;
            this.radDateTimePicker_d1.Text = "01/01/2022";
            this.radDateTimePicker_d1.Value = new System.DateTime(2022, 1, 1, 0, 0, 0, 0);
            // 
            // radDropDownList_Route
            // 
            this.radDropDownList_Route.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Route.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Route.Location = new System.Drawing.Point(13, 304);
            this.radDropDownList_Route.Name = "radDropDownList_Route";
            this.radDropDownList_Route.Size = new System.Drawing.Size(169, 21);
            this.radDropDownList_Route.TabIndex = 57;
            // 
            // radDropDownList_Branch
            // 
            this.radDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Branch.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Branch.Location = new System.Drawing.Point(13, 245);
            this.radDropDownList_Branch.Name = "radDropDownList_Branch";
            this.radDropDownList_Branch.Size = new System.Drawing.Size(169, 21);
            this.radDropDownList_Branch.TabIndex = 56;
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(13, 158);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(43, 19);
            this.radLabel2.TabIndex = 55;
            this.radLabel2.Text = "แผนก";
            // 
            // radDropDownList_Dept
            // 
            this.radDropDownList_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Dept.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Dept.Location = new System.Drawing.Point(13, 183);
            this.radDropDownList_Dept.Name = "radDropDownList_Dept";
            this.radDropDownList_Dept.Size = new System.Drawing.Size(169, 21);
            this.radDropDownList_Dept.TabIndex = 54;
            this.radDropDownList_Dept.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Dept_SelectedValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(13, 49);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(105, 19);
            this.radLabel1.TabIndex = 53;
            this.radLabel1.Text = "เลือกเครื่องพิมพ์";
            // 
            // radDropDownList_Printer
            // 
            this.radDropDownList_Printer.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radDropDownList_Printer.ForeColor = System.Drawing.Color.Blue;
            this.radDropDownList_Printer.Location = new System.Drawing.Point(13, 74);
            this.radDropDownList_Printer.Name = "radDropDownList_Printer";
            this.radDropDownList_Printer.Size = new System.Drawing.Size(169, 21);
            this.radDropDownList_Printer.TabIndex = 52;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.radButtonElement_print,
            this.commandBarSeparator1,
            this.radButtonElement_PrintBill,
            this.commandBarSeparator4,
            this.radButtonElement_PrintTag,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator5});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 36);
            this.radStatusStrip1.TabIndex = 51;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_print
            // 
            this.radButtonElement_print.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_print.Image = global::PC_Shop24Hrs.Properties.Resources.print;
            this.radButtonElement_print.Name = "radButtonElement_print";
            this.radStatusStrip1.SetSpring(this.radButtonElement_print, false);
            this.radButtonElement_print.Text = "radButtonElement1";
            this.radButtonElement_print.ToolTipText = "Export To Excel";
            this.radButtonElement_print.UseCompatibleTextRendering = false;
            this.radButtonElement_print.Click += new System.EventHandler(this.RadButtonElement_print_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_PrintBill
            // 
            this.radButtonElement_PrintBill.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_PrintBill.Image = global::PC_Shop24Hrs.Properties.Resources.check;
            this.radButtonElement_PrintBill.Name = "radButtonElement_PrintBill";
            this.radStatusStrip1.SetSpring(this.radButtonElement_PrintBill, false);
            this.radButtonElement_PrintBill.Text = "radButtonElement1";
            this.radButtonElement_PrintBill.Click += new System.EventHandler(this.RadButtonElement_PrintBill_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_PrintTag
            // 
            this.radButtonElement_PrintTag.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_PrintTag.Image = global::PC_Shop24Hrs.Properties.Resources.TagRefresh;
            this.radButtonElement_PrintTag.Name = "radButtonElement_PrintTag";
            this.radStatusStrip1.SetSpring(this.radButtonElement_PrintTag, false);
            this.radButtonElement_PrintTag.Text = "radButtonElement1";
            this.radButtonElement_PrintTag.Click += new System.EventHandler(this.RadButtonElement_PrintTag_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument1_PrintPage);
            // 
            // OrderToAx_PrintT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "OrderToAx_PrintT";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "พิมพ์บิลจัดส่งสินค้า";
            this.Load += new System.EventHandler(this.OrderToAx_PrintT_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_PrintTag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnInQuery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_d1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Route)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Printer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowHD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_print;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox3;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox2;
        private Telerik.WinControls.UI.RadButton btnInQuery;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_d1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Route;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Branch;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Printer;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_PrintTag;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_PrintBill;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_PrintTag;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
    }
}
