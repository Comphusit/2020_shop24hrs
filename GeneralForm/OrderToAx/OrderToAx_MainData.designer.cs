﻿namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    partial class OrderToAx_MainData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderToAx_MainData));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadLabel_Status = new Telerik.WinControls.UI.RadLabel();
            this.radioButton_In = new System.Windows.Forms.RadioButton();
            this.radioButton_Out = new System.Windows.Forms.RadioButton();
            this.RadioButton_Cencel = new System.Windows.Forms.RadioButton();
            this.RadioButton_Choose = new System.Windows.Forms.RadioButton();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.DataGridView_ShowBranch = new System.Windows.Forms.DataGridView();
            this.Choose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Branch_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Branch_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BRANCH_OUTPHUKET = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Export = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_AddValueExcel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.DataGridView_Show = new System.Windows.Forms.DataGridView();
            this.radCheckBox_Border = new Telerik.WinControls.UI.RadCheckBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel_Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_ShowBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Border)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radCheckBox_Border);
            this.panel1.Controls.Add(this.RadLabel_Status);
            this.panel1.Controls.Add(this.radioButton_In);
            this.panel1.Controls.Add(this.radioButton_Out);
            this.panel1.Controls.Add(this.RadioButton_Cencel);
            this.panel1.Controls.Add(this.RadioButton_Choose);
            this.panel1.Controls.Add(this.radLabel_Name);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadButton_Save);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(627, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(244, 636);
            this.panel1.TabIndex = 1;
            // 
            // RadLabel_Status
            // 
            this.RadLabel_Status.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadLabel_Status.AutoSize = false;
            this.RadLabel_Status.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.RadLabel_Status.ForeColor = System.Drawing.Color.Blue;
            this.RadLabel_Status.Location = new System.Drawing.Point(208, 40);
            this.RadLabel_Status.Name = "RadLabel_Status";
            this.RadLabel_Status.Size = new System.Drawing.Size(31, 20);
            this.RadLabel_Status.TabIndex = 68;
            this.RadLabel_Status.Text = "สถานะ";
            this.RadLabel_Status.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadLabel_Status.Visible = false;
            // 
            // radioButton_In
            // 
            this.radioButton_In.AutoSize = true;
            this.radioButton_In.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_In.Location = new System.Drawing.Point(117, 120);
            this.radioButton_In.Name = "radioButton_In";
            this.radioButton_In.Size = new System.Drawing.Size(81, 20);
            this.radioButton_In.TabIndex = 74;
            this.radioButton_In.Text = "ในจังหวัด";
            this.radioButton_In.UseVisualStyleBackColor = true;
            this.radioButton_In.CheckedChanged += new System.EventHandler(this.RadioButton_In_CheckedChanged);
            // 
            // radioButton_Out
            // 
            this.radioButton_Out.AutoSize = true;
            this.radioButton_Out.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Out.Location = new System.Drawing.Point(13, 120);
            this.radioButton_Out.Name = "radioButton_Out";
            this.radioButton_Out.Size = new System.Drawing.Size(89, 20);
            this.radioButton_Out.TabIndex = 73;
            this.radioButton_Out.Text = "ต่างจังหวัด";
            this.radioButton_Out.UseVisualStyleBackColor = true;
            this.radioButton_Out.CheckedChanged += new System.EventHandler(this.RadioButton_Out_CheckedChanged);
            // 
            // RadioButton_Cencel
            // 
            this.RadioButton_Cencel.AutoSize = true;
            this.RadioButton_Cencel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadioButton_Cencel.Location = new System.Drawing.Point(117, 97);
            this.RadioButton_Cencel.Name = "RadioButton_Cencel";
            this.RadioButton_Cencel.Size = new System.Drawing.Size(108, 20);
            this.RadioButton_Cencel.TabIndex = 70;
            this.RadioButton_Cencel.Text = "ยกเลิกทั้งหมด";
            this.RadioButton_Cencel.UseVisualStyleBackColor = true;
            this.RadioButton_Cencel.CheckedChanged += new System.EventHandler(this.RadioButton_Cencel_CheckedChanged);
            // 
            // RadioButton_Choose
            // 
            this.RadioButton_Choose.AutoSize = true;
            this.RadioButton_Choose.Checked = true;
            this.RadioButton_Choose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadioButton_Choose.Location = new System.Drawing.Point(13, 97);
            this.RadioButton_Choose.Name = "RadioButton_Choose";
            this.RadioButton_Choose.Size = new System.Drawing.Size(98, 20);
            this.RadioButton_Choose.TabIndex = 69;
            this.RadioButton_Choose.TabStop = true;
            this.RadioButton_Choose.Text = "เลือกทั้งหมด";
            this.RadioButton_Choose.UseVisualStyleBackColor = true;
            this.RadioButton_Choose.CheckedChanged += new System.EventHandler(this.RadioButton_Choose_CheckedChanged);
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(7, 40);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(232, 19);
            this.radLabel_Name.TabIndex = 67;
            this.radLabel_Name.Text = "แผนก";
            this.radLabel_Name.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.DataGridView_ShowBranch);
            this.panel2.Location = new System.Drawing.Point(3, 174);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(238, 459);
            this.panel2.TabIndex = 66;
            // 
            // DataGridView_ShowBranch
            // 
            this.DataGridView_ShowBranch.AllowUserToAddRows = false;
            this.DataGridView_ShowBranch.AllowUserToDeleteRows = false;
            this.DataGridView_ShowBranch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_ShowBranch.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Choose,
            this.Branch_ID,
            this.Branch_Name,
            this.BRANCH_OUTPHUKET});
            this.DataGridView_ShowBranch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridView_ShowBranch.Location = new System.Drawing.Point(0, 0);
            this.DataGridView_ShowBranch.Name = "DataGridView_ShowBranch";
            this.DataGridView_ShowBranch.ReadOnly = true;
            this.DataGridView_ShowBranch.RowHeadersVisible = false;
            this.DataGridView_ShowBranch.Size = new System.Drawing.Size(238, 459);
            this.DataGridView_ShowBranch.TabIndex = 0;
            this.DataGridView_ShowBranch.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_ShowBranch_CellClick);
            // 
            // Choose
            // 
            this.Choose.DataPropertyName = "Choose";
            this.Choose.FalseValue = "0";
            this.Choose.HeaderText = "เลือก";
            this.Choose.Name = "Choose";
            this.Choose.ReadOnly = true;
            this.Choose.TrueValue = "1";
            this.Choose.Width = 60;
            // 
            // Branch_ID
            // 
            this.Branch_ID.DataPropertyName = "Branch_ID";
            this.Branch_ID.HeaderText = "รหัส";
            this.Branch_ID.Name = "Branch_ID";
            this.Branch_ID.ReadOnly = true;
            this.Branch_ID.Width = 80;
            // 
            // Branch_Name
            // 
            this.Branch_Name.DataPropertyName = "Branch_Name";
            this.Branch_Name.HeaderText = "ชื่อ";
            this.Branch_Name.Name = "Branch_Name";
            this.Branch_Name.ReadOnly = true;
            // 
            // BRANCH_OUTPHUKET
            // 
            this.BRANCH_OUTPHUKET.HeaderText = "BRANCH_OUTPHUKET";
            this.BRANCH_OUTPHUKET.Name = "BRANCH_OUTPHUKET";
            this.BRANCH_OUTPHUKET.ReadOnly = true;
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.RadButtonElement_pdt,
            this.commandBarSeparator2,
            this.radButtonElement_Export,
            this.commandBarSeparator1,
            this.radButtonElement_AddValueExcel,
            this.commandBarSeparator4,
            this.RadButtonElement_excel,
            this.commandBarSeparator5});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(244, 34);
            this.radStatusStrip1.TabIndex = 65;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.UseCompatibleTextRendering = false;
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Export
            // 
            this.radButtonElement_Export.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Export.Image = global::PC_Shop24Hrs.Properties.Resources.check;
            this.radButtonElement_Export.Name = "radButtonElement_Export";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Export, false);
            this.radButtonElement_Export.Text = "radButtonElement1";
            this.radButtonElement_Export.Click += new System.EventHandler(this.RadButtonElement_Export_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_AddValueExcel
            // 
            this.radButtonElement_AddValueExcel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_AddValueExcel.Image = global::PC_Shop24Hrs.Properties.Resources.FileSave;
            this.radButtonElement_AddValueExcel.Name = "radButtonElement_AddValueExcel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_AddValueExcel, false);
            this.radButtonElement_AddValueExcel.Text = "radButtonElement1";
            this.radButtonElement_AddValueExcel.Click += new System.EventHandler(this.RadButtonElement_AddValueExcel_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_excel
            // 
            this.RadButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.RadButtonElement_excel.Name = "RadButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_excel, false);
            this.RadButtonElement_excel.Text = "";
            this.RadButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(7, 62);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(232, 32);
            this.RadButton_Save.TabIndex = 30;
            this.RadButton_Save.Text = "บันทึก";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.DataGridView_Show, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(618, 636);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 614);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(612, 19);
            this.radLabel_Detail.TabIndex = 54;
            this.radLabel_Detail.Text = "<html>OR &gt; สาขาสั่ง | SALE &gt; เฉลี่ยขายย้อนหลัง 3 วัน | TF &gt; ส่งเมื่อวาน " +
    "| CN &gt; คืนเฉลี่ยย้อนหลัง 2 วัน [จำนวนจะแสดงตามหน่วยแต่จะรวมจำนวนทุกมิติสินค้า" +
    "]</html>";
            // 
            // DataGridView_Show
            // 
            this.DataGridView_Show.AllowUserToAddRows = false;
            this.DataGridView_Show.AllowUserToDeleteRows = false;
            this.DataGridView_Show.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataGridView_Show.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DataGridView_Show.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridView_Show.Location = new System.Drawing.Point(3, 3);
            this.DataGridView_Show.Name = "DataGridView_Show";
            this.DataGridView_Show.Size = new System.Drawing.Size(612, 605);
            this.DataGridView_Show.TabIndex = 1;
            this.DataGridView_Show.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DataGridView_Show_CellBeginEdit);
            this.DataGridView_Show.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_Show_CellClick);
            this.DataGridView_Show.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_Show_CellDoubleClick);
            this.DataGridView_Show.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_Show_CellEndEdit);
            this.DataGridView_Show.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DataGridView_Show_EditingControlShowing);
            this.DataGridView_Show.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.DataGridView_Show_RowPostPaint);
            this.DataGridView_Show.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DataGridView_Show_KeyPress);
            // 
            // radCheckBox_Border
            // 
            this.radCheckBox_Border.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Border.Location = new System.Drawing.Point(13, 146);
            this.radCheckBox_Border.Name = "radCheckBox_Border";
            this.radCheckBox_Border.Size = new System.Drawing.Size(114, 19);
            this.radCheckBox_Border.TabIndex = 75;
            this.radCheckBox_Border.Text = "แสดงเส้นตาราง";
            this.radCheckBox_Border.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Border_CheckStateChanged);
            // 
            // OrderToAx_MainData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "OrderToAx_MainData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "จัดการสินค้าส่ง";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OrderToAx_MainData_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadLabel_Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_ShowBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Border)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private System.Windows.Forms.Panel panel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadLabel RadLabel_Status;
        private System.Windows.Forms.RadioButton RadioButton_Cencel;
        private System.Windows.Forms.RadioButton RadioButton_Choose;
        private System.Windows.Forms.DataGridView DataGridView_ShowBranch;
        private System.Windows.Forms.DataGridView DataGridView_Show;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Choose;
        private System.Windows.Forms.DataGridViewTextBoxColumn Branch_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Branch_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn BRANCH_OUTPHUKET;
        private System.Windows.Forms.RadioButton radioButton_Out;
        private System.Windows.Forms.RadioButton radioButton_In;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_AddValueExcel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Export;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Border;
    }
}
