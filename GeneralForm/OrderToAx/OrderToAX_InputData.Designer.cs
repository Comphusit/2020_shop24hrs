﻿namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    partial class OrderToAX_InputData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderToAX_InputData));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.RadioButton_CopyTable = new System.Windows.Forms.RadioButton();
            this.RadioButton_Copy0 = new System.Windows.Forms.RadioButton();
            this.RadioButton_CopyPack = new System.Windows.Forms.RadioButton();
            this.radTextBox_Qty = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Unit = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(109, 210);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 4;
            this.radButton_Save.Text = "ตกลง";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "ตกลง";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(236, 210);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 2;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadioButton_CopyTable
            // 
            this.RadioButton_CopyTable.AutoSize = true;
            this.RadioButton_CopyTable.Enabled = false;
            this.RadioButton_CopyTable.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.RadioButton_CopyTable.Location = new System.Drawing.Point(213, 28);
            this.RadioButton_CopyTable.Name = "RadioButton_CopyTable";
            this.RadioButton_CopyTable.Size = new System.Drawing.Size(202, 21);
            this.RadioButton_CopyTable.TabIndex = 1;
            this.RadioButton_CopyTable.Text = "คัดลอกจำนวนตามค่าที่ตั้งไว้";
            this.RadioButton_CopyTable.UseVisualStyleBackColor = true;
            this.RadioButton_CopyTable.Visible = false;
            // 
            // RadioButton_Copy0
            // 
            this.RadioButton_Copy0.AutoSize = true;
            this.RadioButton_Copy0.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.RadioButton_Copy0.Location = new System.Drawing.Point(30, 85);
            this.RadioButton_Copy0.Name = "RadioButton_Copy0";
            this.RadioButton_Copy0.Size = new System.Drawing.Size(315, 21);
            this.RadioButton_Copy0.TabIndex = 2;
            this.RadioButton_Copy0.Text = "ปรับจำนวนทุกสาขาเป็น [รวมสาขาที่ไม่สั่งด้วย]";
            this.RadioButton_Copy0.UseVisualStyleBackColor = true;
            this.RadioButton_Copy0.CheckedChanged += new System.EventHandler(this.RadioButton_Copy0_CheckedChanged);
            // 
            // RadioButton_CopyPack
            // 
            this.RadioButton_CopyPack.AutoSize = true;
            this.RadioButton_CopyPack.Checked = true;
            this.RadioButton_CopyPack.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.RadioButton_CopyPack.Location = new System.Drawing.Point(30, 122);
            this.RadioButton_CopyPack.Name = "RadioButton_CopyPack";
            this.RadioButton_CopyPack.Size = new System.Drawing.Size(370, 21);
            this.RadioButton_CopyPack.TabIndex = 3;
            this.RadioButton_CopyPack.TabStop = true;
            this.RadioButton_CopyPack.Text = "ปรับจำนวนทุกสาขาให้ลงแพ็ค [เฉพาะสาขาที่สั่งเท่านั้น]";
            this.RadioButton_CopyPack.UseVisualStyleBackColor = true;
            this.RadioButton_CopyPack.CheckedChanged += new System.EventHandler(this.RadioButton_CopyPack_CheckedChanged);
            // 
            // radTextBox_Qty
            // 
            this.radTextBox_Qty.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Qty.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Qty.Location = new System.Drawing.Point(45, 161);
            this.radTextBox_Qty.Name = "radTextBox_Qty";
            this.radTextBox_Qty.Size = new System.Drawing.Size(351, 25);
            this.radTextBox_Qty.TabIndex = 0;
            this.radTextBox_Qty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Qty_KeyPress);
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_Name.AutoSize = false;
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_Name.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Name.Location = new System.Drawing.Point(12, 12);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(403, 22);
            this.radLabel_Name.TabIndex = 69;
            this.radLabel_Name.Text = "Name";
            // 
            // radLabel_Unit
            // 
            this.radLabel_Unit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_Unit.AutoSize = false;
            this.radLabel_Unit.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_Unit.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Unit.Location = new System.Drawing.Point(12, 46);
            this.radLabel_Unit.Name = "radLabel_Unit";
            this.radLabel_Unit.Size = new System.Drawing.Size(403, 22);
            this.radLabel_Unit.TabIndex = 70;
            this.radLabel_Unit.Text = "Unit";
            // 
            // OrderToAX_InputData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(427, 262);
            this.Controls.Add(this.RadioButton_CopyTable);
            this.Controls.Add(this.radLabel_Unit);
            this.Controls.Add(this.radLabel_Name);
            this.Controls.Add(this.radTextBox_Qty);
            this.Controls.Add(this.RadioButton_CopyPack);
            this.Controls.Add(this.RadioButton_Copy0);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderToAX_InputData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบุ ประเภทข้อมูลสำหรับการคัดลอก";
            this.Load += new System.EventHandler(this.OrderToAX_InputData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private System.Windows.Forms.RadioButton RadioButton_CopyTable;
        private System.Windows.Forms.RadioButton RadioButton_Copy0;
        private System.Windows.Forms.RadioButton RadioButton_CopyPack;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Qty;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private Telerik.WinControls.UI.RadLabel radLabel_Unit;
    }
}
