﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAX_InputData : Telerik.WinControls.UI.RadForm
    {
        public double pQty;
        public string pCase;
        readonly string _itemName, _itemUnit;

        public OrderToAX_InputData(string itemName, string itemUnit)
        {
            InitializeComponent();
            _itemName = itemName;
            _itemUnit = itemUnit;
        }
        //Main Load
        private void OrderToAX_InputData_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radTextBox_Qty.Enabled = false;
            RadioButton_Copy0.Checked = false;
            RadioButton_CopyPack.Checked = false;
            RadioButton_CopyTable.Checked = false;

            radLabel_Name.Text = _itemName; radLabel_Name.Visible = true;
            radLabel_Unit.Text = "หน่วย : " + _itemUnit; radLabel_Unit.Visible = true;

        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {

            if (RadioButton_CopyTable.Checked == true)
            {
                pCase = "1";//คัดลอดตามค่าที่ตั้ง
                pQty = 1;
            }
            else if (RadioButton_Copy0.Checked == true)
            {
                if (radTextBox_Qty.Text == "")
                {
                    radTextBox_Qty.Focus(); return;
                }
                pCase = "2";//คัดลอดตามจำนวนที่ระบุ 
                pQty = Convert.ToDouble(radTextBox_Qty.Text);
            }
            else if (RadioButton_CopyPack.Checked == true)
            {
                if (radTextBox_Qty.Text == "")
                {
                    radTextBox_Qty.Focus(); return;
                }
                pCase = "3";//คัดลอกให้ลงจำนวนแพ็ค
                pQty = Convert.ToDouble(radTextBox_Qty.Text);
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ระบุ ประเภทข้อมูลสำหรับการคัดลอก{Environment.NewLine}ก่อนการกด ตกลง");
                return;
            }

            if (pQty < 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"จะต้องระบุจำนวนการคัดลอกมากกว่า 0 เท่านั้น");
                return;
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadTextBox_Qty_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
        //เลือกการ Copy ทุกรายการ
        private void RadioButton_Copy0_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButton_Copy0.Checked == true)
            {
                radTextBox_Qty.Text = "";
                radTextBox_Qty.Enabled = true;
                radTextBox_Qty.Focus();
            }
            else
            {
                radTextBox_Qty.Enabled = false;
            }
        }
        //เลือกการ Copy ให้ลงหน่วย ที่กำหนด
        private void RadioButton_CopyPack_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButton_CopyPack.Checked == true)
            {
                radTextBox_Qty.Text = "";
                radTextBox_Qty.Enabled = true;
                radTextBox_Qty.Focus();
            }
            else
            {
                radTextBox_Qty.Enabled = false;

            }
        }
    }
}

