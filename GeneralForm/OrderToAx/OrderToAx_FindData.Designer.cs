﻿namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    partial class OrderToAx_FindData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderToAx_FindData));
            this.RadGridView_ShowHD = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.RadGridView_ShowDT = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadDropDownList_Dept = new Telerik.WinControls.UI.RadDropDownList();
            this.RadCheckBox_Dept = new Telerik.WinControls.UI.RadCheckBox();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radioButton_APV = new System.Windows.Forms.RadioButton();
            this.radioButton_CONF = new System.Windows.Forms.RadioButton();
            this.radCheckBox_Status = new Telerik.WinControls.UI.RadCheckBox();
            this.radioButton_CENCEL = new System.Windows.Forms.RadioButton();
            this.radioButton_SAVE = new System.Windows.Forms.RadioButton();
            this.RadButton_Choose = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Search = new Telerik.WinControls.UI.RadButton();
            this.RadDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.RadCheckBox_Branch = new Telerik.WinControls.UI.RadCheckBox();
            this.radDateTimePicker_End = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker_Begin = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Choose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_End)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Begin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_ShowHD
            // 
            this.RadGridView_ShowHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowHD.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowHD.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_ShowHD.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ShowHD.Name = "RadGridView_ShowHD";
            // 
            // 
            // 
            this.RadGridView_ShowHD.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowHD.Size = new System.Drawing.Size(662, 312);
            this.RadGridView_ShowHD.TabIndex = 16;
            this.RadGridView_ShowHD.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_ShowHD.SelectionChanged += new System.EventHandler(this.RadGridView_ShowHD_SelectionChanged);
            this.RadGridView_ShowHD.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_ShowHD.FilterChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.RadGridView_ShowHD_FilterChanged);
            this.RadGridView_ShowHD.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_ShowHD.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowHD, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_ShowDT, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(668, 636);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // RadGridView_ShowDT
            // 
            this.RadGridView_ShowDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowDT.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowDT.Location = new System.Drawing.Point(3, 321);
            // 
            // 
            // 
            this.RadGridView_ShowDT.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.RadGridView_ShowDT.Name = "RadGridView_ShowDT";
            // 
            // 
            // 
            this.RadGridView_ShowDT.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowDT.Size = new System.Drawing.Size(662, 312);
            this.RadGridView_ShowDT.TabIndex = 17;
            this.RadGridView_ShowDT.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowDT_ViewCellFormatting);
            this.RadGridView_ShowDT.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_ShowDT_ConditionalFormattingFormShown);
            this.RadGridView_ShowDT.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_ShowDT_FilterPopupRequired);
            this.RadGridView_ShowDT.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_ShowDT_FilterPopupInitialized);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadDropDownList_Dept);
            this.panel1.Controls.Add(this.RadCheckBox_Dept);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.radioButton_APV);
            this.panel1.Controls.Add(this.radioButton_CONF);
            this.panel1.Controls.Add(this.radCheckBox_Status);
            this.panel1.Controls.Add(this.radioButton_CENCEL);
            this.panel1.Controls.Add(this.radioButton_SAVE);
            this.panel1.Controls.Add(this.RadButton_Choose);
            this.panel1.Controls.Add(this.RadButton_Search);
            this.panel1.Controls.Add(this.RadDropDownList_Branch);
            this.panel1.Controls.Add(this.RadCheckBox_Branch);
            this.panel1.Controls.Add(this.radDateTimePicker_End);
            this.panel1.Controls.Add(this.radDateTimePicker_Begin);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(677, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 636);
            this.panel1.TabIndex = 1;
            // 
            // RadDropDownList_Dept
            // 
            this.RadDropDownList_Dept.DropDownAnimationEnabled = false;
            this.RadDropDownList_Dept.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Dept.Enabled = false;
            this.RadDropDownList_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Dept.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Dept.Location = new System.Drawing.Point(10, 120);
            this.RadDropDownList_Dept.Name = "RadDropDownList_Dept";
            this.RadDropDownList_Dept.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Dept.TabIndex = 67;
            this.RadDropDownList_Dept.Text = "radDropDownList1";
            this.RadDropDownList_Dept.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Dept_SelectedValueChanged);
            // 
            // RadCheckBox_Dept
            // 
            this.RadCheckBox_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Dept.Location = new System.Drawing.Point(10, 97);
            this.RadCheckBox_Dept.Name = "RadCheckBox_Dept";
            this.RadCheckBox_Dept.Size = new System.Drawing.Size(57, 19);
            this.RadCheckBox_Dept.TabIndex = 66;
            this.RadCheckBox_Dept.Text = "แผนก";
            this.RadCheckBox_Dept.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Dept_ToggleStateChanged);
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.commandBarSeparator3,
            this.RadButtonElement_pdt,
            this.commandBarSeparator2});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(194, 34);
            this.radStatusStrip1.TabIndex = 65;
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.UseCompatibleTextRendering = false;
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radioButton_APV
            // 
            this.radioButton_APV.AutoSize = true;
            this.radioButton_APV.Enabled = false;
            this.radioButton_APV.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_APV.Location = new System.Drawing.Point(24, 399);
            this.radioButton_APV.Name = "radioButton_APV";
            this.radioButton_APV.Size = new System.Drawing.Size(61, 20);
            this.radioButton_APV.TabIndex = 64;
            this.radioButton_APV.Text = "อนุมัติ";
            this.radioButton_APV.UseVisualStyleBackColor = true;
            this.radioButton_APV.Visible = false;
            this.radioButton_APV.CheckedChanged += new System.EventHandler(this.RadioButton_APV_CheckedChanged);
            // 
            // radioButton_CONF
            // 
            this.radioButton_CONF.AutoSize = true;
            this.radioButton_CONF.Enabled = false;
            this.radioButton_CONF.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_CONF.Location = new System.Drawing.Point(24, 373);
            this.radioButton_CONF.Name = "radioButton_CONF";
            this.radioButton_CONF.Size = new System.Drawing.Size(62, 20);
            this.radioButton_CONF.TabIndex = 63;
            this.radioButton_CONF.Text = "ยืนยัน";
            this.radioButton_CONF.UseVisualStyleBackColor = true;
            this.radioButton_CONF.Visible = false;
            this.radioButton_CONF.CheckedChanged += new System.EventHandler(this.RadioButton_CONF_CheckedChanged);
            // 
            // radCheckBox_Status
            // 
            this.radCheckBox_Status.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Status.Location = new System.Drawing.Point(10, 296);
            this.radCheckBox_Status.Name = "radCheckBox_Status";
            this.radCheckBox_Status.Size = new System.Drawing.Size(106, 19);
            this.radCheckBox_Status.TabIndex = 62;
            this.radCheckBox_Status.Text = "สถานะเอกสาร";
            this.radCheckBox_Status.Visible = false;
            this.radCheckBox_Status.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Status_ToggleStateChanged);
            // 
            // radioButton_CENCEL
            // 
            this.radioButton_CENCEL.AutoSize = true;
            this.radioButton_CENCEL.Enabled = false;
            this.radioButton_CENCEL.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_CENCEL.Location = new System.Drawing.Point(24, 347);
            this.radioButton_CENCEL.Name = "radioButton_CENCEL";
            this.radioButton_CENCEL.Size = new System.Drawing.Size(65, 20);
            this.radioButton_CENCEL.TabIndex = 61;
            this.radioButton_CENCEL.Text = "ยกเลิก";
            this.radioButton_CENCEL.UseVisualStyleBackColor = true;
            this.radioButton_CENCEL.Visible = false;
            this.radioButton_CENCEL.CheckedChanged += new System.EventHandler(this.RadioButton_CENCEL_CheckedChanged);
            // 
            // radioButton_SAVE
            // 
            this.radioButton_SAVE.AutoSize = true;
            this.radioButton_SAVE.Checked = true;
            this.radioButton_SAVE.Enabled = false;
            this.radioButton_SAVE.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_SAVE.Location = new System.Drawing.Point(24, 321);
            this.radioButton_SAVE.Name = "radioButton_SAVE";
            this.radioButton_SAVE.Size = new System.Drawing.Size(62, 20);
            this.radioButton_SAVE.TabIndex = 60;
            this.radioButton_SAVE.TabStop = true;
            this.radioButton_SAVE.Text = "บันทึก";
            this.radioButton_SAVE.UseVisualStyleBackColor = true;
            this.radioButton_SAVE.Visible = false;
            this.radioButton_SAVE.CheckedChanged += new System.EventHandler(this.RadioButton_SAVE_CheckedChanged);
            // 
            // RadButton_Choose
            // 
            this.RadButton_Choose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Choose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_Choose.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Choose.Location = new System.Drawing.Point(10, 595);
            this.RadButton_Choose.Name = "RadButton_Choose";
            this.RadButton_Choose.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Choose.TabIndex = 31;
            this.RadButton_Choose.Text = "เลือก";
            this.RadButton_Choose.ThemeName = "Fluent";
            this.RadButton_Choose.Click += new System.EventHandler(this.RadButton_Choose_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Choose.GetChildAt(0))).Text = "เลือก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Choose.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Choose.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Choose.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Search
            // 
            this.RadButton_Search.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Search.Location = new System.Drawing.Point(10, 234);
            this.RadButton_Search.Name = "RadButton_Search";
            this.RadButton_Search.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Search.TabIndex = 30;
            this.RadButton_Search.Text = "ค้นหา";
            this.RadButton_Search.ThemeName = "Fluent";
            this.RadButton_Search.Click += new System.EventHandler(this.RadButton_Search_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Text = "ค้นหา";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Search.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Search.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadDropDownList_Branch
            // 
            this.RadDropDownList_Branch.DropDownAnimationEnabled = false;
            this.RadDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Branch.Enabled = false;
            this.RadDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Branch.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Branch.Location = new System.Drawing.Point(10, 70);
            this.RadDropDownList_Branch.Name = "RadDropDownList_Branch";
            this.RadDropDownList_Branch.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Branch.TabIndex = 29;
            this.RadDropDownList_Branch.Text = "radDropDownList1";
            this.RadDropDownList_Branch.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Branch_SelectedValueChanged);
            // 
            // RadCheckBox_Branch
            // 
            this.RadCheckBox_Branch.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadCheckBox_Branch.Location = new System.Drawing.Point(10, 47);
            this.RadCheckBox_Branch.Name = "RadCheckBox_Branch";
            this.RadCheckBox_Branch.Size = new System.Drawing.Size(53, 19);
            this.RadCheckBox_Branch.TabIndex = 28;
            this.RadCheckBox_Branch.Text = "สาขา";
            this.RadCheckBox_Branch.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.RadCheckBox_Branch_ToggleStateChanged);
            this.RadCheckBox_Branch.CheckStateChanged += new System.EventHandler(this.RadCheckBox_Branch_CheckStateChanged);
            // 
            // radDateTimePicker_End
            // 
            this.radDateTimePicker_End.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_End.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_End.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_End.Location = new System.Drawing.Point(10, 195);
            this.radDateTimePicker_End.Name = "radDateTimePicker_End";
            this.radDateTimePicker_End.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_End.TabIndex = 27;
            this.radDateTimePicker_End.TabStop = false;
            this.radDateTimePicker_End.Value = new System.DateTime(((long)(0)));
            this.radDateTimePicker_End.ValueChanged += new System.EventHandler(this.RadDateTimePicker_End_ValueChanged);
            // 
            // radDateTimePicker_Begin
            // 
            this.radDateTimePicker_Begin.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_Begin.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radDateTimePicker_Begin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_Begin.Location = new System.Drawing.Point(10, 168);
            this.radDateTimePicker_Begin.Name = "radDateTimePicker_Begin";
            this.radDateTimePicker_Begin.Size = new System.Drawing.Size(175, 21);
            this.radDateTimePicker_Begin.TabIndex = 26;
            this.radDateTimePicker_Begin.TabStop = false;
            this.radDateTimePicker_Begin.Value = new System.DateTime(((long)(0)));
            this.radDateTimePicker_Begin.ValueChanged += new System.EventHandler(this.RadDateTimePicker_Begin_ValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(10, 148);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(83, 19);
            this.radLabel1.TabIndex = 25;
            this.radLabel1.Text = "วันที่ส่งสินค้า";
            // 
            // OrderToAx_FindData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "OrderToAx_FindData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ค้นหาข้อมูลการส่งสินค้า";
            this.Load += new System.EventHandler(this.OrderToAx_FindData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowHD)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Choose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadCheckBox_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_End)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_Begin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowHD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowDT;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_End;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Search;
        protected Telerik.WinControls.UI.RadButton RadButton_Choose;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_Begin;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Branch;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Branch;
        private System.Windows.Forms.RadioButton radioButton_APV;
        private System.Windows.Forms.RadioButton radioButton_CONF;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Status;
        private System.Windows.Forms.RadioButton radioButton_CENCEL;
        private System.Windows.Forms.RadioButton radioButton_SAVE;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Dept;
        private Telerik.WinControls.UI.RadCheckBox RadCheckBox_Dept;
    }
}
