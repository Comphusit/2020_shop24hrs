﻿namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    partial class OrderToAx_CopyDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderToAx_CopyDocument));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Input = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadTextBox_Dept = new Telerik.WinControls.UI.RadTextBox();
            this.RadTextBox_Docno = new Telerik.WinControls.UI.RadTextBox();
            this.radioButton_Barcode = new System.Windows.Forms.RadioButton();
            this.radioButton_Docno = new System.Windows.Forms.RadioButton();
            this.RadTextBox_Date = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(79, 140);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 4;
            this.radButton_Save.Text = "คัดลอก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "คัดลอก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(175, 140);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 5;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radLabel_Input
            // 
            this.radLabel_Input.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Input.Location = new System.Drawing.Point(26, 44);
            this.radLabel_Input.Name = "radLabel_Input";
            this.radLabel_Input.Size = new System.Drawing.Size(37, 19);
            this.radLabel_Input.TabIndex = 24;
            this.radLabel_Input.Text = "MRT";
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Barcode.Location = new System.Drawing.Point(79, 38);
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(201, 25);
            this.radTextBox_Barcode.TabIndex = 0;
            this.radTextBox_Barcode.Tag = "";
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            this.radTextBox_Barcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Barcode_KeyPress);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(26, 104);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(37, 19);
            this.radLabel1.TabIndex = 25;
            this.radLabel1.Text = "MRT";
            // 
            // RadTextBox_Dept
            // 
            this.RadTextBox_Dept.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_Dept.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Dept.Location = new System.Drawing.Point(76, 98);
            this.RadTextBox_Dept.MaxLength = 3;
            this.RadTextBox_Dept.Name = "RadTextBox_Dept";
            this.RadTextBox_Dept.Size = new System.Drawing.Size(38, 25);
            this.RadTextBox_Dept.TabIndex = 1;
            this.RadTextBox_Dept.Tag = "";
            this.RadTextBox_Dept.Text = "164";
            this.RadTextBox_Dept.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RadTextBox_Dept.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Dept_KeyDown);
            this.RadTextBox_Dept.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Dept_KeyPress);
            // 
            // RadTextBox_Docno
            // 
            this.RadTextBox_Docno.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_Docno.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Docno.Location = new System.Drawing.Point(200, 98);
            this.RadTextBox_Docno.MaxLength = 6;
            this.RadTextBox_Docno.Name = "RadTextBox_Docno";
            this.RadTextBox_Docno.Size = new System.Drawing.Size(80, 25);
            this.RadTextBox_Docno.TabIndex = 3;
            this.RadTextBox_Docno.Tag = "";
            this.RadTextBox_Docno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Docno_KeyDown);
            this.RadTextBox_Docno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Docno_KeyPress);
            // 
            // radioButton_Barcode
            // 
            this.radioButton_Barcode.AutoSize = true;
            this.radioButton_Barcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Barcode.Location = new System.Drawing.Point(26, 12);
            this.radioButton_Barcode.Name = "radioButton_Barcode";
            this.radioButton_Barcode.Size = new System.Drawing.Size(73, 20);
            this.radioButton_Barcode.TabIndex = 26;
            this.radioButton_Barcode.Text = "บาร์โค้ด";
            this.radioButton_Barcode.UseVisualStyleBackColor = true;
            this.radioButton_Barcode.CheckedChanged += new System.EventHandler(this.RadioButton_Barcode_CheckedChanged);
            // 
            // radioButton_Docno
            // 
            this.radioButton_Docno.AutoSize = true;
            this.radioButton_Docno.Checked = true;
            this.radioButton_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_Docno.Location = new System.Drawing.Point(26, 72);
            this.radioButton_Docno.Name = "radioButton_Docno";
            this.radioButton_Docno.Size = new System.Drawing.Size(99, 20);
            this.radioButton_Docno.TabIndex = 27;
            this.radioButton_Docno.TabStop = true;
            this.radioButton_Docno.Text = "เลขที่เอกสาร";
            this.radioButton_Docno.UseVisualStyleBackColor = true;
            this.radioButton_Docno.CheckedChanged += new System.EventHandler(this.RadioButton_Docno_CheckedChanged);
            // 
            // RadTextBox_Date
            // 
            this.RadTextBox_Date.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.RadTextBox_Date.ForeColor = System.Drawing.Color.Blue;
            this.RadTextBox_Date.Location = new System.Drawing.Point(115, 98);
            this.RadTextBox_Date.MaxLength = 6;
            this.RadTextBox_Date.Name = "RadTextBox_Date";
            this.RadTextBox_Date.Size = new System.Drawing.Size(84, 25);
            this.RadTextBox_Date.TabIndex = 2;
            this.RadTextBox_Date.Tag = "";
            this.RadTextBox_Date.Text = "201012";
            this.RadTextBox_Date.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RadTextBox_Date.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Date_KeyDown);
            this.RadTextBox_Date.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Date_KeyPress);
            // 
            // OrderToAx_CopyDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(327, 190);
            this.Controls.Add(this.RadTextBox_Date);
            this.Controls.Add(this.radioButton_Docno);
            this.Controls.Add(this.radioButton_Barcode);
            this.Controls.Add(this.RadTextBox_Docno);
            this.Controls.Add(this.RadTextBox_Dept);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radTextBox_Barcode);
            this.Controls.Add(this.radLabel_Input);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderToAx_CopyDocument";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "เอกสารตั้งต้น";
            this.Load += new System.EventHandler(this.OrderToAx_CopyDocument_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadTextBox_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel_Input;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Dept;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Docno;
        private System.Windows.Forms.RadioButton radioButton_Barcode;
        private System.Windows.Forms.RadioButton radioButton_Docno;
        private Telerik.WinControls.UI.RadTextBox RadTextBox_Date;
    }
}
