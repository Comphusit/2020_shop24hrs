﻿using System;
using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{

    static class MRT_Class
    {
        //ค้นหาข้อมูล HD
        public static DataTable GetFindDocument(string Dept, string Branch, string DateBagin, string DateEnd)
        {
            string SqlBch = "";
            if (Branch != "" && Branch != null) SqlBch = string.Format($@" AND Branch='{Branch}' ");

            string sql = string.Format($@"
            SELECT  DocNo ,convert(varchar,Date,23)  DocnoDate,  
                    Branch,BRANCHNAME, 
                    CASE StaPrcDoc WHEN '3' THEN 'X' ELSE '/' END AS StaPrcDoc , 
                    CASE StaPrcDoc WHEN '3' THEN 'X' ELSE 
                        CASE StaDoc WHEN '1' THEN '/' ELSE 'X' END END AS StaDoc , 
                    CASE StaPrcDoc WHEN '3' THEN 'X' ELSE 
                        CASE StaApvDoc WHEN '1' THEN '/' ELSE 'X' END  END AS StaApvDoc ,  
                    CASE StaPrcDoc WHEN '3' THEN 'X' ELSE 
                        CASE StaAx WHEN '1' THEN '/' ELSE 'X' END END AS StaAx , 
                    ROUND,Remark  
            FROM    Android_OrderTOHD  with (nolock) 
            where   convert(varchar,date,23)  Between '{DateBagin}' and '{DateEnd}' 
                    AND Depart = '{Dept}'  {SqlBch}
            ORDER BY convert(varchar,Date,23) desc,DocNo desc,Branch desc"
            );
            return ConnectionClass.SelectSQL_Main(sql);
        }
 
        //ค้นหาเอกสาร HD / MRT
        public static DataTable GetMRTDocnoHD(String DocNo)
        {
            string sql = string.Format($@"
                SELECT  DocNo ,convert(varchar,Date,23) as DocDate ,Time ,convert(varchar,DateUp,23) as DateUp ,TimeUp,
		                UserCode ,WhoUpName,WhoInName, Branch ,BranchName,Depart ,
		                StaDoc ,StaPrcDoc ,StaApvDoc ,StaAx ,StaRound2 ,Box ,Remark , Round, 
		                case StaAx when '1' then 'AX' else 
			                case StaApvDoc  when '1' then 'APV' else 
				                case StaPrcDoc when '3' then 'CENCEL' else 
					                case StaDoc when '1' then 'SAVE' else 'DEFAULT' 
					                end 
				                end 
			                end 
		                end as StatusDoc, StaGroupItem, GroupItem, DeptGroup
                FROM	Android_OrderTOHD with (nolock)
                WHERE	DocNo =  '{DocNo}'
            ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Detail MRT
        public static DataTable GetMRTDocnoDT(String DocNo)
        {
            string sql = string.Format($@"
                SELECT	Android_OrderTODT.SeqNo, ItemID, ItemDim, Barcode, Name,
		                Qty,UnitID,CAST(Price AS DECIMAL(10,2)) AS Price,     
                        CAST((Qty+QtyOrderCust+ISNULL(qtyround,0))*Price AS DECIMAL(10,2)) AS	NET, 
                        CAST(QtyOrderCust AS DECIMAL(10,2)) AS QtyOrderCust,
                        ISNULL(CAST(qtyround AS DECIMAL(10,2)),0) AS qtyround,
                        Android_OrderTODT.Round
                FROM	Android_OrderTOHD with (nolock)
		                INNER JOIN Android_OrderTODT with (nolock) ON Android_OrderTOHD.DocNo= Android_OrderTODT.DocNo
                WHERE	Android_OrderTOHD.DocNo = '{DocNo}'
                        AND  CAST((Qty+QtyOrderCust+ISNULL(qtyround,0))*Price AS DECIMAL(10,2)) > 0 
                ORDER BY SeqNo
            ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
    static class MRTMainClass
    {
        //กลุ่มย่อยของแต่ละแผนก
        public static DataTable GetGroupItem_ByDept(string Dept)
        {
            string sql = string.Format($@" 
            SELECT  ITEMGROUPID, ITEMGROUPID+' '+ITEMGROUPNAME As ITEMGROUPNAME
            FROM    SHOP_ITEMGROUP WITH (NOLOCK)
            WHERE   ITEMGROUPDEPT= '{Dept}' AND ITEMGROUPSTATUS3 = '1' 
            ORDER BY ITEMGROUPID ");
            return ConnectionClass.SelectSQL_Main(sql);
        }

        //ค้นหาเอกสาร MNOR และ เอกสาร MRT
        public static DataTable GetMNOR(String Dept, String GroupItem, String Date, String RoundDocno)
        {
            string ConditionsMNOR = "", ConditionsMRTO = "  ";
            if (!GroupItem.Equals(""))
            {
                ConditionsMNOR = string.Format($@" AND GROUPID = '{GroupItem}' ");
                ConditionsMRTO = string.Format($@" AND GroupItem = '{GroupItem}' ");
            }

            string sql = string.Format($@" 
                SELECT  BRANCH_ID,BRANCH_NAME,isnull(CMNORDOCNO,0) as CMNORDOCNO,isnull(CMRTODocNo,0) as CMRTODocNo ,
                        CASE BRANCH_OUTPHUKET  WHEN '1' THEN 'ในจังหวัด' ELSE 'ต่างจังหวัด' END AS OUTPHUKET,ISNULL(SUMQTY,0) AS SUMQTY

                FROM    SHOP_BRANCH WITH (NOLOCK)  
		                LEFT OUTER JOIN   
	                    (SELECT BRANCH_ID AS BRANCH_MNOR,ISNULL(count(DOCNO),0) as CMNORDOCNO
	                     FROM	SHOP_MNOR_HD WITH (NOLOCK)    
		                 WHERE	STA_DOC != '3' AND STA_PRCDOC = '1' AND STA_APVDOC = '1'     
				                AND DPTID = '{Dept}'  {ConditionsMNOR}
				                AND DATE_RECIVE = '{Date}'    
		                 GROUP BY BRANCH_ID )Shop_MNOR_OrderHD  ON Shop_Branch.BRANCH_ID = Shop_MNOR_OrderHD.BRANCH_MNOR 
		                LEFT OUTER JOIN   
	                    (SELECT Branch AS BRANCH_MNT,count(DocNo) as CMRTODocNo 
		                 FROM	ANDROID_ORDERTOHD  WITH (NOLOCK)   
		                 WHERE	Depart = '{Dept}'  {ConditionsMRTO} 
				                AND convert(varchar,Date,23) = '{Date}'
				                AND Round = '{RoundDocno}' AND StaPrcDoc != '3' 
                         GROUP BY Branch )ANDROID_ORDERTOHD   ON Shop_Branch.BRANCH_ID = ANDROID_ORDERTOHD.BRANCH_MNT  
                         LEFT OUTER JOIN 
						 (SELECT	Branch AS BRANCH_MNT,COUNT(ANDROID_ORDERTODT.DocNo) as SUMQTY 
							FROM	ANDROID_ORDERTOHD WITH (NOLOCK) INNER JOIN ANDROID_ORDERTODT WITH (NOLOCK) ON ANDROID_ORDERTOHD.DocNo = ANDROID_ORDERTODT.DocNo
							WHERE	Depart = '{Dept}'   {ConditionsMRTO} 
									AND convert(varchar,Date,23) = '{Date}'
									AND ANDROID_ORDERTOHD.Round = '{RoundDocno}' AND StaPrcDoc != '3' 
                                    AND ANDROID_ORDERTODT.Qty > 0 
							GROUP BY Branch)ORDERDT ON ANDROID_ORDERTOHD.BRANCH_MNT = ORDERDT.BRANCH_MNT

                WHERE   BRANCH_STA='1'	AND BRANCH_ORDERTOAX='1'

                ORDER BY BRANCH_ID 
            ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Check สต็อกที่ส่งมาทั้งหมด
        public static DataTable GetItemSendStockAll(string Date, string pCase = "0")
        {
            DateTime daySelect = DateTime.Parse(Date);
            if (pCase == "0") daySelect = Convert.ToDateTime(Date).AddDays(-1);

            string sql = $@"
                DECLARE @Date NVARCHAR(12) = '{daySelect:yyyy-MM-dd}';

                SELECT	DISTINCT MNOSBarcode AS BARCODE,MNOSQtyStock AS QTYSTOCK,MNOSBranch AS BRANCH_ID
                FROM	Shop_MNOS_DT  with (nolock) 
		                INNER JOIN  Shop_MNOS_HD with (nolock)  on Shop_MNOS_HD.MNOSDocNo=Shop_MNOS_DT.MNOSDocNo    
                WHERE   Shop_MNOS_HD.MNOSDate = @Date     ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
 

    }
    static class MRTShowDetail
    {
      
        //ประมวลผลรอบ 2
        public static DataTable GetDtRound2Detail(string Date, string Dept, string GroupItem)
        {
            string ConditionsMRTO = $@" AND GroupItem LIKE '{GroupItem}%' ";
            if (!GroupItem.Equals(""))
            {
                ConditionsMRTO = string.Format("AND GroupItem = '{0}'", GroupItem);
            }
            string sql = string.Format(@" 
            SELECT  android_ordertohd.docno, BRANCH AS BRANCH_ID,BRANCHNAME AS BRANCH_NAME ,
                    ROW_NUMBER( ) OVER(PARTITION BY BRANCH ORDER BY BRANCH ASC) AS OrderSeq,
                    Barcode, ItemID, ItemDim, Name, sum(isnull(qtyround, 0)) as total, UnitID,Price
            FrOM    android_ordertohd WITH(NOLOCK)
                    inner join Android_OrderTODT WITH(NOLOCK)  ON android_ordertohd.DocNo=android_ordertodt.docno
            where   date = '{0}' 
                    AND android_ordertohd.round= '1'
                    and StaRound2 = '0'
                    AND android_ordertohd.Depart='{1}'   {2}
            group by android_ordertohd.docno, BRANCH , BRANCHNAME, 
                    Barcode , ItemID, ItemDim, Name, UnitID,Price 
            HAVING  sum(isnull(qtyround, 0)) > 0 ",
                Date, Dept, ConditionsMRTO);
            return ConnectionClass.SelectSQL_Main(sql);
        }


    }
    static class MainData
    {
         
        //ค้นหานวนการสั่ง
        public static DataTable GetItemBarcodeMNOR_ByDept(string DateReceive, string Dept)
        {
            string sql = string.Format($@" 
            SELECT  *
            FROM    (
                    SELECT  Shop_MNOR_DT.ITEMBARCODE,BRANCH_ID,isnull(sum(Shop_MNOR_DT.QTYORDER),0) as totalOR,
                            Shop_MNOR_DT.ITEMBARCODE AS Barcode,BRANCH_ID AS Branch,isnull(sum(Shop_MNOR_DT.QTYSTOCK),0) as QTY 
                    FROM    Shop_MNOR_HD WITH (NOLOCK)
                            INNER JOIN  Shop_MNOR_DT ON Shop_MNOR_HD.DOCNO = Shop_MNOR_DT.DOCNO 
                    WHERE   STA_DOC !=3 
                            AND STA_PRCDOC = 1 
                            AND STA_APVDOC = 1 
                            AND DATE_RECIVE = '{DateReceive}' 
                            AND DPTID = '{Dept}'
                    GROUP BY Shop_MNOR_DT.ITEMBARCODE,BRANCH_ID
                    )TMP
            WHERE   totalOR > 0 OR QTY > 0
            ORDER BY ITEMBARCODE,BRANCH_ID");
            return ConnectionClass.SelectSQL_Main(sql);
        }
     
        public static bool CheckDocument(string Dept, string Date, string round, string DeptGroup)
        {
            if (!(DeptGroup.Equals("")))
            {
                DeptGroup = string.Format(@" AND GroupItem='{0}'", DeptGroup);
            }
            string sql = string.Format(@"select * from ANDROID_ORDERTOHD  where StaPrcDoc!='3' AND Depart='{0}'
                and convert(varchar,Date,23)='{1}' and Round='{2}' {3}", Dept, Date, round, DeptGroup);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            bool Rsu = false;
            if (dt.Rows.Count > 0)
            {
                Rsu = true;
            }
            return Rsu;
        }
        //public static DataTable GetItemCopy(string Barcode)
        //{
        //    string sql = $@"
        //        SELECT	BRANCH_ID,BRANCH_NAME,ISNULL(QTY,0) AS QTY
        //        FROM	SHOP_BRANCH WITH (NOLOCK)
		      //          LEFT OUTER JOIN
		      //          (
		      //          SELECT	*	FROM	ANDROID_ITEMS WITH (NoLOCK)
		      //          WHERE	ITEMBARCODE = '{Barcode}' 
		      //          )TMP_QTY ON SHOP_BRANCH.BRANCH_ID = TMP_QTY.BRANCHID
        //        WHERE	BRANCH_STA = '1' AND BRANCH_ORDERTOAX = '1'
        //        ";
        //    return ConnectionClass.SelectSQL_Main(sql);
        //    // return dt;
        //}
 
        //ค้นหาจำนวน คืน CN วันนี้และย้อนหลัง 1 วัน
        public static DataTable GetCN(string pCase_0Barcode_1ItemID, string pAvg_0Avg_1Not, int Day)
        {
            string sql;
            int dayDiff = Day; if (Day == 0) dayDiff = 1;
            if (pCase_0Barcode_1ItemID == "0")
            {
                string fName = $@" (SUM(QTY)/{dayDiff}) ";
                if (pAvg_0Avg_1Not == "1") fName = " SUM(QTY) ";

                sql = $@"
                SELECT  SHOP_MNPC_HD.Branch ,SHOP_MNPC_DT.Barcode,
                        {fName} AS QTY,
		                sum(QTY) AS sumQty , sum(QTY)/{dayDiff} AS QtyAVG
                FROM    SHOP_MNPC_HD WITH (NOLOCK) 
                        INNER JOIN SHOP_MNPC_DT WITH (NOLOCK)  on SHOP_MNPC_HD.DocNo = SHOP_MNPC_DT.DocNo 
                WHERE   DocDate  between convert(varchar,getdate()-{Day},23) and convert(varchar,getdate(),23)
                        and StaDoc ='1' and StaPrcDoc ='1' 
                GROUP BY SHOP_MNPC_HD.Branch , SHOP_MNPC_DT.Barcode  ";
            }
            else
            {
                string fName = $@" (SUM(QtySum)/{dayDiff})  ";
                if (pAvg_0Avg_1Not == "1") fName = " SUM(QtySum)  ";

                sql = $@"
                SELECT  SHOP_MNPC_HD.Branch , SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid,
                        {fName} AS QTY,
		                sum(QtySum) AS sumQty , sum(QtySum)/{dayDiff} AS QtyAVG
                FROM    SHOP_MNPC_HD WITH (NOLOCK) 
                        INNER JOIN SHOP_MNPC_DT WITH (NOLOCK)  on SHOP_MNPC_HD.DocNo = SHOP_MNPC_DT.DocNo 
                WHERE   DocDate  between convert(varchar,getdate()-{Day},23) and convert(varchar,getdate(),23)
                        and StaDoc ='1' and StaPrcDoc ='1' 
                GROUP BY SHOP_MNPC_HD.Branch , SHOP_MNPC_DT.ItemID,SHOP_MNPC_DT.Dimid  ";
            }

            return ConnectionClass.SelectSQL_Main(sql);
        }

    }
    static class MRTMainDocBranch
    {
        //check เลขที่เอกสารจากเงื่อนไข
        public static DataTable CheckDocumentMRTO(string Dept, string Date, string round, string Branch, string groupitem)
        {
            string sqlcondition = "";
            if (!groupitem.Equals("")) sqlcondition = string.Format($@" AND GroupItem='{groupitem}' ");
            string sql = string.Format($@"
                    select  DocNo ,StaApvDoc 
                    from    ANDROID_ORDERTOHD   WITH (NOLOCK)
                    where   StaPrcDoc != '3' AND Depart = '{Dept}'
                            and convert(varchar,Date,23)='{Date}' and Round='{round}' and Branch='{Branch}'
				            {sqlcondition}"
                    );
            return ConnectionClass.SelectSQL_Main(sql);
        }
        
        //ค้นหาบาร์โค้ดในบิล
        public static int CheckMRTOBarcode(string DocNo, string Barcode)
        {
            string sql = string.Format($@"
                SELECT  Barcode
                FROM    ANDROID_ORDERTODT  
                WHERE   DocNo = '{DocNo}'
                        AND Barcode = '{Barcode}' ");
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            //            Boolean MRTO = false;
            if (dt.Rows.Count > 0)
            {
                return 0;
            }
            else
            {
                DataTable dA = ConnectionClass.SelectSQL_Main($@"
                    SELECT	COUNT(DocNo)+1 AS SEQ	
                    FROM	Android_OrderTODT WITH (NOLOCK)
                    WHERE	DocNo = '{DocNo}'
                ");
                return Convert.ToInt32(dA.Rows[0]["SEQ"].ToString());
            }
        }

    }
  
 

}