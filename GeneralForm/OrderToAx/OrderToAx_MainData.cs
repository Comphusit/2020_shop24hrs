﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Drawing;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_MainData : Telerik.WinControls.UI.RadForm
    {
        readonly int fixColumn = 11;
        readonly DataTable dtDataHD = new DataTable();
        readonly DataTable dtDataDT = new DataTable();
        string pBarcodeAll, pItemID;
        readonly string _TypeOpenOR;//0 = MNOR 1 = Sale
        readonly string _Dept;
        string _StatusDocument;
        readonly string _DateReceive;
        readonly string _GroupItem;
        readonly string _DateSaleBegin;
        readonly string _DateSaleEnd;
        readonly string _Invent;
        public object DirInfo { get; private set; }
        double QtyCell;
        private class Qty
        {
            public double QtyBeforeEdit { get; set; }
            public double QtyEndEdit { get; set; }
        }
        //ok get การ insert ข้อมูลสำหรับการแสดงผลค่าเฉลี่ยต่างๆ
        private class QtyDataShow
        {
            public string Barcode { get; set; }
            public string Branch { get; set; }
            public double QtyOR { get; set; }
            public double QtySale { get; set; }
            public double QtyTF { get; set; }
            public double QtyCN { get; set; }
        }
        private class QtyUpdate
        {
            public string Branch { get; set; }
            public string Barcode { get; set; }
            public double Qty { get; set; }
        }
        public OrderToAx_MainData(string statusDocument, string TypeOpenOR, string Dept, string DateReceive, string GroupItem, string DateSaleBegin, string DateSaleEnd, string Invent)
        {
            InitializeComponent();
            _StatusDocument = statusDocument;
            _TypeOpenOR = TypeOpenOR;
            _Dept = Dept;
            _DateReceive = DateReceive;
            _GroupItem = GroupItem;
            _DateSaleBegin = DateSaleBegin;
            _DateSaleEnd = DateSaleEnd;
            _Invent = Invent;
        }
        //ok get Set Enable
        void SetEnable(int status)
        {
            if (status.Equals(0))
            {
                RadButton_Save.Enabled = true;
                DataGridView_ShowBranch.Enabled = false;
                RadioButton_Choose.Enabled = false;
                RadioButton_Cencel.Enabled = false;
                radioButton_In.Enabled = false;
                radioButton_Out.Enabled = false;
                radButtonElement_AddValueExcel.Enabled = false;
                radButtonElement_Export.Enabled = false;
            }
            else
            {
                RadButton_Save.Enabled = false;
                DataGridView_ShowBranch.Enabled = true;
                RadioButton_Choose.Enabled = true;
                RadioButton_Cencel.Enabled = true;
                radioButton_In.Enabled = true;
                radioButton_Out.Enabled = true;
                radButtonElement_AddValueExcel.Enabled = true;
                radButtonElement_Export.Enabled = true;
            }
        }

        //ok get Set Grid สำหรับของการสร้าง HeadColumn
        private DataGridViewTextBoxColumn DataGridViewTextBoxColumn(string Name, string HeaderText, int Width, string Format, bool Frozen, bool ReadOnly, bool Visible)
        {
            DataGridViewTextBoxColumn Column = new DataGridViewTextBoxColumn()
            {
                Name = Name,
                HeaderText = HeaderText,
                Width = Width
            };
            switch (Format)
            {
                case "Number":
                    Column.DefaultCellStyle.Format = "N2";
                    Column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    break;
                case "Right":
                    Column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    break;
                case "Center":
                    Column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    break;
                default:
                    Column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    break;
            }

            Column.Frozen = Frozen;
            Column.ReadOnly = ReadOnly;
            Column.Visible = Visible;
            Column.DefaultCellStyle.BackColor = Color.White;
            return Column;
        }
        //ok get Load
        private void OrderToAx_MainData_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radCheckBox_Border.Checked = false; radCheckBox_Border.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_excel.ToolTipText = "Export ออเดอร์รวมทั้งหมด"; RadButtonElement_excel.ShowBorder = true;
            radButtonElement_AddValueExcel.ToolTipText = "ระบุออเดอร์ผ่าน Excel"; radButtonElement_AddValueExcel.ShowBorder = true;
            radButtonElement_Export.ToolTipText = "Export เพื่อทำออเดอร์"; radButtonElement_Export.ShowBorder = true;
            DataGridView_Show.ReadOnly = false;

            RadButton_Save.ButtonElement.ShowBorder = true;
            switch (_Dept)
            {
                case "D032":
                    //radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายวันนี้[บาร์โค้ด] | ST > สต็อกรอบ 21.00-00.00 เมื่อวานสาขาส่ง | CN > สาขาส่งคืนเมื่อวาน[บาร์โค้ด] | DoubleClick ช่อง ST >> เพื่อดูรูปส่งสต็อก";
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE1 > ขายเมื่อวาน[บาร์โค้ด] | SALE2 > ขายวันนี้[บาร์โค้ด] | MRT > ส่งล่าสุด[บาร์โค้ด] | ST > สต็อกสาขาส่งวันนี้[บาร์โค้ด]";
                    break;
                case "D053":
                    if (_TypeOpenOR == "1") radLabel_Detail.Text = "OR > จำนวนขายตามวันที่ระบุ[ทุกมิติ] | SALE > ย้อนหลัง 7 วันแบบเฉลี่ย[ทุกมิติ] | MRT > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ] | CN > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ]";
                    else radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ย้อนหลัง 7 วันแบบเฉลี่ย[ทุกมิติ] | MRT > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ] | CN > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ]";
                    break;
                case "D067"://โอเค
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > เฉลี่ยย้อนหลัง 3 วัน[บาร์โค้ด] | TF > เฉลี่ยย้อนหลัง 2 วัน[ทุกมิติ] | CN > เฉลี่ยย้อนหลัง 2 วัน[ทุกมิติ]";
                    break;
                case "D106":
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ย้อนหลัง 10 วันแบบไม่เฉลี่ย[ทุกมิติ] | MRT > ย้อนหลัง 10 วัน ไม่เฉลี่ย[ทุกมิติ] | CN > ย้อนหลัง 10 วัน ไม่เฉลี่ย[ทุกมิติ]";
                    break;
                case "D110":
                    //radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายวันนี้[บาร์โค้ด] | MRT > ส่งเมื่อวาน[บาร์โค้ด] | CN > สาขาส่งคืนเมื่อวาน[บาร์โค้ด]";
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE1 > ขายเมื่อวาน[บาร์โค้ด] | SALE2 > ขายวันนี้[บาร์โค้ด] | MRT > ส่งล่าสุด[บาร์โค้ด] | ST > สต็อกสาขาส่งวันนี้[บาร์โค้ด]";
                    break;
                case "D118":
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายย้อนหลัง 1 วันแบบไม่เฉลี่ย[ทุกมิติ] | MRT > ส่งเมื่อวาน[ทุกมิติ] | ST > สต็อก ณ ตอนสาขาสั่ง ";
                    break;
                case "D144":
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ย้อนหลัง 3 วันแบบเฉลี่ย[บาร์โค้ด] | MRT > ย้อนหลัง 3 วัน ไม่เฉลี่ย[บาร์โค้ด] | CN > ย้อนหลัง 3 วัน ไม่เฉลี่ย[บาร์โค้ด]";
                    break;
                case "D159":
                    if (_TypeOpenOR == "1") radLabel_Detail.Text = "OR > จำนวนขายตามวันที่ระบุ[ทุกมิติ] | SALE > ย้อนหลัง 7 วันแบบเฉลี่ย[ทุกมิติ] | MRT > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ] | CN > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ]";
                    else radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ย้อนหลัง 7 วันแบบเฉลี่ย[ทุกมิติ] | MRT > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ] | CN > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ]";
                    break;
                case "D164":
                    //radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายวันนี้[ทุกมิติ] | MRT > ส่งเมื่อวาน[ทุกมิติ] | ST > สต็อกรอบ 16.00-19.00 วันนี้สาขาส่ง | DoubleClick ช่อง ST >> เพื่อดูรูปส่งสต็อก ";
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE1 > ขายเมื่อวาน[ทุกมิติ] | SALE2 > ขายวันนี้[ทุกมิติ] | MRT > ส่งล่าสุด[ทุกมิติ] | ST > สต็อกสาขาส่งวันนี้[บาร์โค้ด]";
                    break;
                case "D207":
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายย้อนหลัง 2 วันแบบไม่เฉลี่ย[ทุกมิติ] | MRT > ส่งเมื่อวาน[ทุกมิติ] | ST > สต็อก ณ ตอนสาขาสั่ง ";
                    break;
                default:
                    radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > เฉลี่ยขายย้อนหลัง 3 วัน[ทุกมิติ] | MRT > ส่งเมื่อวาน[ทุกมิติ] | CN > คืนเฉลี่ยย้อนหลัง 2 วัน[ทุกมิติ]";
                    break;
            }

            this.Cursor = Cursors.WaitCursor;
            radLabel_Name.Text = "จัดซื้อ " + _Dept + " : " + _DateReceive;

            //สร้าง columns สินค้า  และ ในรายการสินค้า
            CreateColumn_Datagridview();
            //สร้าง columns มินิมาร์ท
            SetRadGridViewColumnName_Show();

            if (_StatusDocument.Equals("SAVE"))
            {
                RadLabel_Status.Text = "บันทึก";
                RadLabel_Status.ForeColor = Color.Blue;
                ShowQtyMRTO_ByDept();
                SetEnable(1);
            }
            else
            {
                RadLabel_Status.Text = "กรุณาบันทึก";
                RadLabel_Status.ForeColor = Color.Red;
                ShowQtyMNOR_ByDept();
                SetEnable(0);
            }

            DataGridViewcolor(DataGridView_Show);

            this.Cursor = Cursors.Default;

        }
        #region "Function for Show Detail "

        //OK get สร้าง columns สินค้า  และ ในรายการสินค้า
        private void CreateColumn_Datagridview()
        {
            DataGridViewCheckBoxColumn ck = new DataGridViewCheckBoxColumn()
            {
                Name = "CC",
                HeaderText = "เลือก",
                Width = 50,
                Frozen = true,
                ReadOnly = false,
                FalseValue = "0",
                TrueValue = "1"
            };
            DataGridView_Show.Columns.Add(ck);

            DataGridViewButtonColumn Column_Copy = new DataGridViewButtonColumn()
            {
                Name = "Copy",
                HeaderText = "ปรับ จน.",
                Width = 60,
                Frozen = true,
                ReadOnly = true,
                Visible = true
            };
            DataGridView_Show.Columns.Add(Column_Copy);

            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("ITEMID", "รหัสสินค้า", 60, "string", true, true, false));
            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("INVENTDIMID", "มิติสินค้า", 120, "string", true, true, false));
            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("ITEMBARCODE", "บาร์โค้ด", 100, "string", true, true, true));
            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("SPC_ITEMNAME", "ชื่อสินค้า", 150, "string", true, true, true));
            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("Retail", "สต็อก " + _Invent, 90, "Number", true, true, true));
            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("UNITID", "หน่วย", 60, "Right", true, true, true));

            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("E1", "ยอดรวม", 90, "Number", true, true, true));
            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("E2", "รวมแก้", 90, "Number", true, true, true));
            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("QTY", "ปริมาณ", 80, "Number", false, true, true));
            DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("SPC_PRICEGROUP3", "ราคาเงินสด", 90, "Number", false, true, true));

            this.Cursor = Cursors.WaitCursor;
            //Add Value In Grid
            DataTable DtItems = MRTClass.GetItemBarcode_ByDeptItemGroup(_Dept, _GroupItem);
            pBarcodeAll = "'";
            pItemID = "'";
            if (DtItems.Rows.Count > 0)
            {
                string ITEMID, INVENTDIMID, ITEMBARCODE, SPC_ITEMNAME, UNITID, QTY, SPC_PRICEGROUP3, stkID = "0.00";

                foreach (DataRow rowItembarcode in DtItems.Rows)
                {
                    ITEMID = rowItembarcode["ITEMID"].ToString();
                    INVENTDIMID = rowItembarcode["INVENTDIMID"].ToString();
                    ITEMBARCODE = rowItembarcode["ITEMBARCODE"].ToString();
                    SPC_ITEMNAME = rowItembarcode["SPC_ITEMNAME"].ToString();
                    UNITID = rowItembarcode["UNITID"].ToString();
                    QTY = Double.Parse(rowItembarcode["QTY"].ToString()).ToString("N2");
                    SPC_PRICEGROUP3 = Double.Parse(rowItembarcode["SPC_PRICEGROUP3"].ToString()).ToString("N2");
                    pBarcodeAll += ITEMBARCODE + "','";
                    pItemID += ITEMID + "','";
                    if (_Invent != "") stkID = (ItembarcodeClass.FindStock_ByBarcode(ITEMBARCODE, _Invent) / Double.Parse(rowItembarcode["QTY"].ToString())).ToString("N2");
                    DataGridView_Show.Rows.Add(new object[] { "0", "แก้ไข", ITEMID, INVENTDIMID, ITEMBARCODE, SPC_ITEMNAME, stkID, UNITID, "0", "0", QTY, SPC_PRICEGROUP3 });
                }
                pBarcodeAll = pBarcodeAll.Substring(0, pBarcodeAll.Length - 2);
                pItemID = pItemID.Substring(0, pItemID.Length - 2);
                this.Cursor = Cursors.Default;
            }
        }

        //OK get เพิ่ม Columns Branch  แสดงรายละเอียดสินค้าสำหรับส่ง 
        void SetRadGridViewColumnName_Show()
        {
            DataTable DtBranch = BranchClass.GetBranchAll_ByConditions(" '1' ", " '1' ", " AND BRANCH_ORDERTOAX = '1' "); //ConfigBranchClass.GetBranchORDERTOAX();
            if (DtBranch.Rows.Count > 0)
            {
                string Branch_ID, Branch_Name, Branch_OutPhuket;
                foreach (DataRow row in DtBranch.Rows)
                {
                    Branch_ID = row["BRANCH_ID"].ToString();
                    Branch_Name = row["BRANCH_NAME"].ToString();
                    Branch_OutPhuket = row["BRANCH_OUTPHUKET"].ToString();

                    string ColumnsName = "OR-SALE | MRT-CN";
                    switch (_Dept)
                    {
                        case "D032":
                            //ColumnsName = "OR-SALE | ST-CN";
                            ColumnsName = "OR-SALE1-SALE2 | MRT-ST";
                            break;
                        case "D110":
                            ColumnsName = "OR-SALE1-SALE2 | MRT-ST";
                            break;
                        case "D118":
                            ColumnsName = "OR-SALE | MRT-ST";
                            break;
                        case "D164":
                            ColumnsName = "OR-SALE1-SALE2 | MRT-ST";
                            break;
                        case "D207":
                            ColumnsName = "OR-SALE | MRT-ST";
                            break;
                    }
                    int iW = 140;
                    if ((_Dept == "D032") || (_Dept == "D110") || (_Dept == "D164")) iW = 200;
                    DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn("AGV" + Branch_ID,
                        Branch_ID + System.Environment.NewLine + ColumnsName, iW, "Center", false, true, true));
                    DataGridView_Show.Columns.Add(DataGridViewTextBoxColumn(Branch_ID, Branch_ID +
                        System.Environment.NewLine + Branch_Name, 60, "Center", false, false, true));

                    DataGridView_ShowBranch.Rows.Add(new object[] { "1", Branch_ID, Branch_Name, Branch_OutPhuket });
                }
            }
        }

        //OK get โหลดข้อมูลรายการสินค้าสั่งตาม MNOR เอกสารเริ่มต้นใหม่
        private void ShowQtyMNOR_ByDept()
        {
            //ในกรณี แผนกหล่าวนี้ ให้ส่งตามยอดขาย D027/D19/D159/D053 ส่วนที่เหลือจากการสั่ง MNOR
            DataTable DtMNORBarcode;
            if (_TypeOpenOR == "1")
            { DtMNORBarcode = MRTClass.GetQtySale_ByDptOrder(_Dept, _GroupItem, _DateSaleBegin, _DateSaleEnd); }
            else
            { DtMNORBarcode = MainData.GetItemBarcodeMNOR_ByDept(_DateReceive, _Dept); }
            //แสดงข้อมูลเฉลี่ยต่างๆ

            List<QtyDataShow> QtyDetailList = SetDataGeneral(DtMNORBarcode);
            for (int iRow = 0; iRow < DataGridView_Show.Rows.Count; iRow++)
            {
                double SumQtyItemBarcodeAllBranch = 0;
                for (int iColumn = fixColumn; iColumn < DataGridView_Show.Columns.Count; iColumn++)
                {
                    if (CheckColumnsBranch(DataGridView_Show.Columns[iColumn].Name.ToString()))
                    {
                        //Set ค่า MNOR
                        double QtyMNOR = 0;
                        if (DtMNORBarcode.Rows.Count > 0)
                        {
                            DataRow[] rowSelect;
                            //ในกรณี แผนกหล่าวนี้ ให้ส่งตามยอดขาย แต่ ต้องหาร Qty อีกที เพราะค่าที่ได้คือ inventQty ปัดเศษขึ้นหมด
                            if (_TypeOpenOR == "1")//Dept.Equals("D027") || Dept.Equals("D019") || Dept.Equals("D159")|| Dept.Equals("D053")
                            {
                                rowSelect = DtMNORBarcode.Select($@" BRANCH_ID='{DataGridView_Show.Columns[iColumn].Name}' AND ITEMID = '{DataGridView_Show.Rows[iRow].Cells["ITEMID"].Value}' AND INVENTDIMID = '{DataGridView_Show.Rows[iRow].Cells["INVENTDIMID"].Value}' ");
                                if (rowSelect.Length > 0) QtyMNOR = Math.Round(double.Parse(rowSelect[0]["totalOR"].ToString()) / double.Parse(DataGridView_Show.Rows[iRow].Cells["QTY"].Value.ToString()), 0);
                            }
                            else
                            {//ส่งตาม mnor
                                rowSelect = DtMNORBarcode.Select(string.Format(@"ITEMBARCODE='{0}' AND BRANCH_ID='{1}'", DataGridView_Show.Rows[iRow].Cells["ITEMBARCODE"].Value, DataGridView_Show.Columns[iColumn].Name));
                                if (rowSelect.Length > 0) QtyMNOR = double.Parse(rowSelect[0]["totalOR"].ToString());
                            }
                        }

                        DataGridView_Show.Rows[iRow].Cells[iColumn].Value = QtyMNOR;
                        SumQtyItemBarcodeAllBranch += QtyMNOR;

                        //ค่าเฉลี่ยต่างๆ In grid
                        if (QtyDetailList.Count > 0)
                        {
                            var correctData = QtyDetailList.First(qty => qty.Barcode.Equals(DataGridView_Show.Rows[iRow].Cells["ITEMBARCODE"].Value.ToString()) && qty.Branch.Equals(DataGridView_Show.Columns[iColumn].Name.ToString()));
                            //string Data = GetDataAvgSale(correctData);
                            string or = "";
                            if ((_Dept == "D032") || (_Dept == "D110") || (_Dept == "D164")) or = QtyMNOR.ToString() + "-";

                            DataGridView_Show.Rows[iRow].Cells[iColumn - 1].Value = $@"{or}{correctData.QtyOR:N2}-{correctData.QtySale:#,#0.00}-{correctData.QtyTF:N2}-{correctData.QtyCN}";
                        }
                    }
                    //Sum จำนวน รวม/รวมแก้
                    DataGridView_Show.Rows[iRow].Cells["E1"].Value = SumQtyItemBarcodeAllBranch;
                    DataGridView_Show.Rows[iRow].Cells["E2"].Value = SumQtyItemBarcodeAllBranch;
                }
            }
        }

        //ok get โหลดข้อมูลรายการสินค้าสั่งตาม MRTO เอกสารบันทึกแล้ว
        private void ShowQtyMRTO_ByDept()
        {
            DataTable DtMNORBarcode = MRTClass.GetItemBarcodeMRTO_ByDept(_DateReceive, _Dept);
            for (int iRow = 0; iRow < DataGridView_Show.Rows.Count; iRow++)
            {
                double SumQtyItemBarcodeAllBranch = 0, SumQtyORItemBarcodeAllBranch = 0;
                for (int iColumn = fixColumn; iColumn < DataGridView_Show.Columns.Count; iColumn++)
                {
                    if (CheckColumnsBranch(DataGridView_Show.Columns[iColumn].Name.ToString()))
                    {
                        double Qty = 0, QtyMNOR = 0, QtyMNSale, QtyMNTF, QtyMNCN;
                        string QtyData = "0.00-0.00 | 0.00-0";
                        if ((_Dept == "D032") || (_Dept == "D110") || (_Dept == "D164")) QtyData = "0.00-0.00-0.00 | 0.00-0";
                        if (DtMNORBarcode.Rows.Count > 0)
                        {
                            DataRow[] rowSelect = DtMNORBarcode.Select(string.Format(@"Barcode='{0}' AND BRANCH_ID='{1}'", DataGridView_Show.Rows[iRow].Cells["ITEMBARCODE"].Value, DataGridView_Show.Columns[iColumn].Name));
                            if (rowSelect.Length > 0)
                            {
                                string Sale110_164 = "";
                                if ((_Dept == "D032") || (_Dept == "D110") || (_Dept == "D164")) Sale110_164 = double.Parse(rowSelect[0]["Sale1"].ToString()).ToString("N2") + "-";

                                Qty = double.Parse(rowSelect[0]["totalQty"].ToString());
                                QtyMNOR = double.Parse(rowSelect[0]["totalOR"].ToString());
                                QtyMNSale = double.Parse(rowSelect[0]["totalSale"].ToString());
                                QtyMNTF = double.Parse(rowSelect[0]["totalTF"].ToString());
                                QtyMNCN = double.Parse(rowSelect[0]["totalCN"].ToString());
                                QtyData = QtyMNOR.ToString("N2") + "-" + Sale110_164 + QtyMNSale.ToString("N2") + " | " + QtyMNTF.ToString("N2") + "-" + QtyMNCN.ToString();
                            }
                        }
                        DataGridView_Show.Rows[iRow].Cells[iColumn].Value = Qty;
                        DataGridView_Show.Rows[iRow].Cells[iColumn - 1].Value = QtyData;

                        SumQtyORItemBarcodeAllBranch += QtyMNOR;
                        SumQtyItemBarcodeAllBranch += Qty;
                    }
                    DataGridView_Show.Rows[iRow].Cells["E1"].Value = SumQtyORItemBarcodeAllBranch;
                    DataGridView_Show.Rows[iRow].Cells["E2"].Value = SumQtyItemBarcodeAllBranch;
                }
            }
        }

        //ok get Check คอลัมนี้ ว่าเป็นคอลัม สาขา มั้ย
        private bool CheckColumnsBranch(string ColumnName)
        {
            bool rsu = false;
            if (ColumnName.Substring(0, 2).Equals("MN")) rsu = true;
            return rsu;
        }
        //คำนวนยอดแก้
        private Double SumQtyItembarcodeAllBranch(string SumQty, List<Qty> Qty)
        {
            if (SumQty.Equals("")) SumQty = "0";
            Double Ans = double.Parse(SumQty) + (Qty[0].QtyEndEdit - Qty[0].QtyBeforeEdit);
            return Ans;
        }
        //OK get ค้นหาข้อมูลเริ่มต้น สำหรับเอกสาร OR-SALE-TF-CN เอาเก็บไว้ใน List ก่อนนำไปแสดงผล
        private List<QtyDataShow> SetDataGeneral(DataTable DtMNORBarcode)//DataGridView DataGridBarcode, สำหรับ ค่า OR จะต้องเลือกว่าเอามาจากยอดขายหรือยอดส่ง
        {
            DataTable DtSale, DtTF, DtCN;
            DataTable DtSaleYesterday = new DataTable();
            string pTypeSale, pTypeTF;//0 Barcode // 1 Dim  // 2 ST D032 R3
            string pTypeCN_ST;//0 STOCK D164 1 CN_Barcode 2 CN_DIM
            switch (_Dept)
            {
                case "D032"://OK radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายวันนี้[บาร์โค้ด] | ST > สต็อกรอบ 21.00-00.00 เมื่อวานสาขาส่ง | CN > สาขาส่งคืนเมื่อวาน[บาร์โค้ด]";
                    DtSaleYesterday = PosSaleClass.MRT_GetSaleXXX("0", "1", pBarcodeAll, 1, 1);
                    DtSale = PosSaleClass.MRT_GetSaleXXX("0", "1", pBarcodeAll, 0); pTypeSale = "0";
                    //DtTF = MainData.GetQtyMRT("0", "1", pBarcodeAll, 1); pTypeTF = "0";
                    DtTF = MRTClass.GetQtyMRT("4", "1", pBarcodeAll, 1); pTypeTF = "0";
                    //DtCN = MainData.GetCN("0", "1", 1); pTypeCN_ST = "1";
                    DtCN = MRTMainClass.GetItemSendStockAll(_DateReceive); pTypeCN_ST = "0";
                    //DtSale = MainData.GetSaleXXX("0", "1", pBarcodeAll, 0); pTypeSale = "0";//
                    //DtTF = SendStock.SendStockClass.GetItemSendStockAll(DateTime.Parse(_DateReceive).AddDays(-1).ToString("yyyy-MM-dd")); pTypeTF = "2";
                    //DtCN = MainData.GetCN("0", "1", 1); pTypeCN_ST = "1";
                    break;
                case "D053":
                    DtSale = PosSaleClass.MRT_GetSaleXXX("1", "0", pItemID, 7); pTypeSale = "1";
                    DtTF = MRTClass.GetQtyMRT("1", "1", pItemID, 7); pTypeTF = "1";
                    DtCN = MainData.GetCN("1", "1", 7); pTypeCN_ST = "2";
                    break;
                case "D067"://OK radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > เฉลี่ยย้อนหลัง 3 วัน[บาร์โค้ด] | TF > เฉลี่ยย้อนหลัง 2 วัน[ทุกมิติ] | CN > เฉลี่ยย้อนหลัง 2 วัน[ทุกมิติ]";
                    DtSale = PosSaleClass.MRT_GetSaleXXX("0", "0", pBarcodeAll, 3); pTypeSale = "0";//SALE > เฉลี่ยย้อนหลัง 3 วัน [บาร์โค้ด]
                    DtTF = MRTClass.GetQtyMRT("1", "0", pItemID, 2); pTypeTF = "1";// TF > เฉลี่ยย้อนหลัง 2 วัน[มิติสินค้า] | "
                    DtCN = MainData.GetCN("1", "0", 2); pTypeCN_ST = "2";//CN > เฉลี่ยย้อนหลัง 2 วัน [มิติสินค้า]
                    break;
                case "D106":// OK radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ย้อนหลัง 10 วันแบบไม่เฉลี่ย[ทุกมิติ] | MRT > ย้อนหลัง 10 วัน ไม่เฉลี่ย[ทุกมิติ] | CN > ย้อนหลัง 10 วัน ไม่เฉลี่ย[ทุกมิติ]";
                    DtSale = PosSaleClass.MRT_GetSaleXXX("1", "1", pItemID, 10); pTypeSale = "1";
                    DtTF = MRTClass.GetQtyMRT("1", "1", pItemID, 7); pTypeTF = "1";
                    DtCN = MainData.GetCN("1", "1", 10); pTypeCN_ST = "2";
                    break;
                case "D110":// OK radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายวันนี้[บาร์โค้ด] | MRT > ส่งเมื่อวาน[บาร์โค้ด] | CN > สาขาส่งคืนเมื่อวาน[บาร์โค้ด]";
                    DtSaleYesterday = PosSaleClass.MRT_GetSaleXXX("0", "1", pBarcodeAll, 1, 1);
                    DtSale = PosSaleClass.MRT_GetSaleXXX("0", "1", pBarcodeAll, 0); pTypeSale = "0";
                    //DtTF = MainData.GetQtyMRT("0", "1", pBarcodeAll, 1); pTypeTF = "0";
                    DtTF = MRTClass.GetQtyMRT("4", "1", pBarcodeAll, 1); pTypeTF = "0";
                    //DtCN = MainData.GetCN("0", "1", 1); pTypeCN_ST = "1";
                    DtCN = MRTMainClass.GetItemSendStockAll(_DateReceive); pTypeCN_ST = "0";
                    break;
                case "D118":// ครัวมาลินี แช่แข็ง  radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายย้อนหลัง 1 วันแบบไม่เฉลี่ย[ทุกมิติ] | MRT > ส่งเมื่อวาน[ทุกมิติ] | ST > สต็อก ณ ตอนสาขาสั่ง ";
                    DtSale = PosSaleClass.MRT_GetSaleXXX("1", "1", pItemID, 1); pTypeSale = "1";
                    DtTF = MRTClass.GetQtyMRT("1", "1", pItemID, 1); pTypeTF = "1";
                    DtCN = DtMNORBarcode; pTypeCN_ST = "1";
                    break;
                case "D144"://OK radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ย้อนหลัง 3 วันแบบเฉลี่ย[บาร์โค้ด] | MRT > ย้อนหลัง 3 วัน ไม่เฉลี่ย[บาร์โค้ด] | CN > ย้อนหลัง 3 วัน ไม่เฉลี่ย[บาร์โค้ด]";
                    DtSale = PosSaleClass.MRT_GetSaleXXX("0", "0", pBarcodeAll, 3); pTypeSale = "0";
                    DtTF = MRTClass.GetQtyMRT("0", "1", pBarcodeAll, 3); pTypeTF = "0";
                    DtCN = MainData.GetCN("0", "1", 3); pTypeCN_ST = "1";
                    break;
                case "D159":
                    //radLabel_Detail.Text = "OR > จำนวนขายตามวันที่ระบุ[ทุกมิติ] | SALE > ย้อนหลัง 7 วันแบบเฉลี่ย[ทุกมิติ] | MRT > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ] | CN > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ]";
                    //radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ย้อนหลัง 7 วันแบบเฉลี่ย[ทุกมิติ] | MRT > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ] | CN > ย้อนหลัง 7 วัน ไม่เฉลี่ย[ทุกมิติ]";
                    DtSale = PosSaleClass.MRT_GetSaleXXX("1", "0", pItemID, 7); pTypeSale = "1";
                    DtTF = MRTClass.GetQtyMRT("1", "1", pItemID, 7); pTypeTF = "1";
                    DtCN = MainData.GetCN("1", "1", 7); pTypeCN_ST = "2";
                    break;
                case "D164":
                    //OK  radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายวันนี้[ทุกมิติ] | MRT > ส่งเมื่อวาน[ทุกมิติ] | ST > สต็อกรอบ 16.00-19.00 วันนี้สาขาส่ง | DoubleClick ช่อง ST >> เพื่อดูรูปส่งสต็อก ";
                    //radLabel_Detail.Text = "SALE > ขายเมื่อวาน[ทุกมิติ] | SALE > ขายวันนี้[ทุกมิติ] | MRT > ส่งล่าสุด[ทุกมิติ] | ST > สต็อกสาขาส่งวันนี้";
                    DtSaleYesterday = PosSaleClass.MRT_GetSaleXXX("1", "1", pItemID, 1, 1);
                    DtSale = PosSaleClass.MRT_GetSaleXXX("1", "1", pItemID, 0); pTypeSale = "1";
                    //DtTF = MainData.GetQtyMRT("1", "1", pItemID, 1); pTypeTF = "1";
                    DtTF = MRTClass.GetQtyMRT("3", "1", pItemID, 1); pTypeTF = "1";
                    //DtCN = SendStock.SendStockClass.GetItemSendStockAll(_DateReceive, "2"); pTypeCN_ST = "0";
                    DtCN = MRTMainClass.GetItemSendStockAll(_DateReceive); pTypeCN_ST = "0";
                    break;
                case "D207":// ครัวชญานัน แช่แข็ง   radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > ขายย้อนหลัง 3 วันแบบไม่เฉลี่ย[ทุกมิติ] | MRT > ส่งเมื่อวาน[ทุกมิติ] | ST > สต็อก ณ ตอนสาขาสั่ง ";
                    DtSale = PosSaleClass.MRT_GetSaleXXX("1", "1", pItemID, 2); pTypeSale = "1";
                    DtTF = MRTClass.GetQtyMRT("1", "1", pItemID, 1); pTypeTF = "1";
                    DtCN = DtMNORBarcode; pTypeCN_ST = "1";
                    break;
                default://  radLabel_Detail.Text = "OR > สาขาสั่ง | SALE > เฉลี่ยขายย้อนหลัง 3 วัน[ทุกมิติ] | MRT > ส่งเมื่อวาน[ทุกมิติ] | CN > คืนเฉลี่ยย้อนหลัง 2 วัน[ทุกมิติ]";
                    DtSale = PosSaleClass.MRT_GetSaleXXX("1", "0", pItemID, 3); pTypeSale = "1"; //SALE > เฉลี่ยขายย้อนหลัง 3 วัน
                    DtTF = MRTClass.GetQtyMRT("1", "0", pItemID, 2); pTypeTF = "1";
                    DtCN = MainData.GetCN("1", "0", 2); pTypeCN_ST = "2";
                    break;
            }


            List<QtyDataShow> qtyDataShows = new List<QtyDataShow>();
            if (DataGridView_Show.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in DataGridView_Show.Rows)//วน Barcode
                {
                    foreach (DataGridViewColumn column in DataGridView_Show.Columns)//วน Branch
                    {
                        if (!CheckColumnsBranch(column.Name)) continue;//ในกรณีที่ คอลัมไม่ใช่ สาขา

                        //DataRow[] rowSelectOR = null, rowSelectCN = null, rowSelectSale = null, rowSelectTF = null;
                        double qtyOr = 0, qtySale = 0, qtyTF = 0, qtyCN = 0;

                        //Order OR [qtyOr]
                        if (_Dept == "D164")
                        {
                            if (DtSaleYesterday.Rows.Count > 0)
                            {
                                DataRow[] rowSaleYesterday = DtSaleYesterday.Select($@" POSGROUP='{column.Name}' AND ITEMID = '{row.Cells["ITEMID"].Value}' AND INVENTDIMID = '{row.Cells["INVENTDIMID"].Value}' ");
                                if (rowSaleYesterday.Length > 0)
                                    qtyOr = (double.Parse(rowSaleYesterday[0]["QTY"].ToString()) / double.Parse(row.Cells["QTY"].Value.ToString()));

                            }
                        }
                        else if (_Dept == "D110")
                        {
                            if (DtSaleYesterday.Rows.Count > 0)
                            {
                                DataRow[] rowSaleYesterday = DtSaleYesterday.Select($@" POSGROUP='{column.Name}' AND ITEMBARCODE = '{row.Cells["ITEMBARCODE"].Value}' ");
                                if (rowSaleYesterday.Length > 0)
                                    qtyOr = double.Parse(rowSaleYesterday[0]["QTY"].ToString());
                            }

                        }
                        else if (_Dept == "D032")
                        {
                            if (DtSaleYesterday.Rows.Count > 0)
                            {
                                DataRow[] rowSaleYesterday = DtSaleYesterday.Select($@" POSGROUP='{column.Name}' AND ITEMBARCODE = '{row.Cells["ITEMBARCODE"].Value}' ");
                                if (rowSaleYesterday.Length > 0)
                                    qtyOr = double.Parse(rowSaleYesterday[0]["QTY"].ToString());
                            }
                        }
                        else
                        {
                            if (DtMNORBarcode.Rows.Count > 0)
                            {
                                DataRow[] rowSelectOR;
                                if (_TypeOpenOR == "1")// Dept.Equals("D159") || Dept.Equals("D027") || Dept.Equals("D019") || Dept.Equals("D053") || 
                                {
                                    rowSelectOR = DtMNORBarcode.Select($@" BRANCH_ID='{column.Name}' AND ITEMID = '{row.Cells["ITEMID"].Value}' AND INVENTDIMID = '{row.Cells["INVENTDIMID"].Value}' ");
                                    if (rowSelectOR.Length > 0)
                                    {
                                        //QTYTotalSale
                                        qtyOr = Math.Ceiling(double.Parse(rowSelectOR[0]["totalOR"].ToString()) / double.Parse(row.Cells["QTY"].Value.ToString()));
                                    }
                                }
                                else
                                {
                                    rowSelectOR = DtMNORBarcode.Select(string.Format(@"BRANCH_ID='{0}' AND ITEMBARCODE='{1}'", column.Name, row.Cells["ITEMBARCODE"].Value));
                                    if (!(rowSelectOR is null)) qtyOr = rowSelectOR.Length > 0 ? double.Parse(rowSelectOR[0]["totalOR"].ToString()) : 0;
                                }
                            }
                        }

                        //CN ในกรณีของแผนกผลไม้จะเป็น Stock ที่สาขาส่งมา //0 STOCK D164 1 CN_Barcode 2 CN_DIM
                        if (DtCN.Rows.Count > 0)
                        {
                            if (pTypeCN_ST == "0")
                            {
                                DataRow[] rowSelectCN = DtCN.Select(string.Format($@"BRANCH_ID='{ column.Name}' AND BARCODE='{row.Cells["ITEMBARCODE"].Value}' "));
                                if (rowSelectCN.Length > 0) qtyCN = double.Parse(rowSelectCN[0]["QTYSTOCK"].ToString());
                            }
                            else if (pTypeCN_ST == "1")// Barcode
                            {
                                DataRow[] rowSelectCN = DtCN.Select(string.Format($@"Branch='{ column.Name}' AND Barcode='{row.Cells["ITEMBARCODE"].Value}'   "));
                                if (rowSelectCN.Length > 0) qtyCN = double.Parse(rowSelectCN[0]["Qty"].ToString());
                            }
                            else//pTypeCN_ST= "2"
                            {
                                //rowSelectCN = DtCN.Select(string.Format(@"Branch='{0}' AND Barcode='{1}'", column.Name, row.Cells["ITEMBARCODE"].Value));
                                DataRow[] rowSelectCN = DtCN.Select(string.Format($@"Branch='{ column.Name}' AND ItemID='{row.Cells["ITEMID"].Value}' AND Dimid = '{row.Cells["INVENTDIMID"].Value}' "));
                                if (rowSelectCN.Length > 0) qtyCN = Math.Ceiling(double.Parse(rowSelectCN[0]["Qty"].ToString()) / double.Parse(row.Cells["QTY"].Value.ToString()));
                            }
                        }

                        //Sale
                        if (DtSale.Rows.Count > 0)
                        {
                            if (pTypeSale == "0")//ตามบาร์โค้ด
                            {
                                DataRow[] rowSelectSale = DtSale.Select($@" POSGROUP='{column.Name}' AND ITEMBARCODE = '{row.Cells["ITEMBARCODE"].Value}' ");
                                if (rowSelectSale.Length > 0) qtySale = double.Parse(rowSelectSale[0]["QTY"].ToString());
                            }
                            else
                            {

                                DataRow[] rowSelectSale = DtSale.Select($@" POSGROUP='{column.Name}' AND ITEMID = '{row.Cells["ITEMID"].Value}' AND INVENTDIMID = '{row.Cells["INVENTDIMID"].Value}' ");
                                if (rowSelectSale.Length > 0) qtySale = (double.Parse(rowSelectSale[0]["QTY"].ToString()) / double.Parse(row.Cells["QTY"].Value.ToString()));
                            }

                        }

                        //TF
                        if (DtTF.Rows.Count > 0)
                        {
                            if (pTypeTF == "0")//Barcode
                            {
                                DataRow[] rowSelectTF = DtTF.Select($@"  BRANCH_ID='{column.Name}' AND ITEMBARCODE = '{row.Cells["ITEMBARCODE"].Value}'   ");
                                if (rowSelectTF.Length > 0) qtyTF = double.Parse(rowSelectTF[0]["QTY"].ToString());
                                //{ qtyTF = Math.Ceiling(double.Parse(rowSelectTF[0]["QTY"].ToString()) / double.Parse(row.Cells["QTY"].Value.ToString())); }
                            }
                            else if (pTypeTF == "1")
                            {
                                DataRow[] rowSelectTF = DtTF.Select($@"  BRANCH_ID='{column.Name}' AND ITEMID = '{row.Cells["ITEMID"].Value}' AND INVENTDIMID = '{row.Cells["INVENTDIMID"].Value}' ");
                                if (rowSelectTF.Length > 0) qtyTF = Math.Ceiling(double.Parse(rowSelectTF[0]["QTY"].ToString()) / double.Parse(row.Cells["QTY"].Value.ToString()));
                                //{ qtyTF = Math.Ceiling(double.Parse(rowSelectTF[0]["QRTTF"].ToString()) / double.Parse(row.Cells["QTY"].Value.ToString())); }
                            }
                            else
                            {
                                DataRow[] rowSelectTF = DtTF.Select(string.Format($@"BRANCH_ID='{ column.Name}' AND BARCODE='{row.Cells["ITEMBARCODE"].Value}' "));
                                if (rowSelectTF.Length > 0) qtyTF = double.Parse(rowSelectTF[0]["QTYSTOCK"].ToString());
                            }
                            //ส่วนนี้จาก AX
                            //DataRow[] rowSelectTF = DtTF.Select($@"  BRANCH_ID='{column.Name}' AND ITEMID = '{row.Cells["ITEMID"].Value}' AND INVENTDIMID = '{row.Cells["INVENTDIMID"].Value}' ");
                            //if (rowSelectTF.Length > 0)
                            //{ qtyTF = Math.Ceiling(double.Parse(rowSelectTF[0]["QRTTF"].ToString()) / double.Parse(row.Cells["QTY"].Value.ToString())); }
                        }

                        qtyDataShows.Add(new QtyDataShow()
                        {
                            Barcode = row.Cells["ITEMBARCODE"].Value.ToString(),
                            Branch = column.Name,
                            QtyOR = qtyOr,
                            QtySale = qtySale,
                            QtyTF = qtyTF,
                            QtyCN = qtyCN
                        });
                    }
                }
            }
            return qtyDataShows;
        }
        //OK get ค้นหาเลขคอมภัม
        private int GetIndexColumns(string ColumnsName, DataGridView DGVColumn)
        {
            int index = 0;
            for (int IColumn = 8; IColumn < DGVColumn.Columns.Count; IColumn++)
            {
                if (ColumnsName == DGVColumn.Columns[IColumn].Name)
                {
                    index = IColumn;
                    break;
                }
            }
            return index;
        }

        private string UpdateQty(List<QtyUpdate> _lists)
        {
            string SqlgroupItemCondition = "AND StaGroupItem = '0'";
            if (!(_GroupItem.Equals("0"))) SqlgroupItemCondition = string.Format(@"AND StaGroupItem = '1' AND GroupItem = '{0}'", _GroupItem);

            ArrayList arrayList = new ArrayList();
            foreach (var l in _lists)
            {
                arrayList.Add(($@"
                    UPDATE  Android_OrderTODT   
                    SET     Qty = '{l.Qty}' 
                    FROM    Android_OrderTODT  WITH (NOLOCK)
                            INNER JOIN Android_OrderTOHD WITH (NOLOCK) ON Android_OrderTOHD.Docno = Android_OrderTODT.DocNo 
                    WHERE   Branch='{l.Branch}'  
                            AND Barcode='{ l.Barcode}'  
                            AND Date='{_DateReceive}' 
                            AND Android_OrderTOHD.StaPrcDoc != '3' {SqlgroupItemCondition} 
                            AND Android_OrderTOHD.Remark = '' "));

            }
            return ConnectionClass.ExecuteSQL_ArrayMain(arrayList);
        }
        #endregion
        //ok get Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }
        //ok get set rows in grid
        private void DataGridView_Show_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            DataGridView grid = sender as DataGridView;
            var rowIdx = (e.RowIndex + 1).ToString();

            var centerFormat = new StringFormat()
            {
                // right alignment might actually make more sense for numbers
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            var headerBounds = new Rectangle(e.RowBounds.Left, e.RowBounds.Top, grid.RowHeadersWidth, e.RowBounds.Height);
            e.Graphics.DrawString(rowIdx, this.Font, SystemBrushes.ControlText, headerBounds, centerFormat);
        }
        //set color ตรงคอลัมหลัก
        private void DataGridViewcolor(DataGridView DataGridView_Show)
        {
            foreach (DataGridViewRow Myrow in DataGridView_Show.Rows)
            {
                if (Double.Parse(Myrow.Cells["E1"].Value.ToString()) > 0) Myrow.Cells["E1"].Style.BackColor = Color.Lime;

                if (Double.Parse(Myrow.Cells["E2"].Value.ToString()) > 0) Myrow.Cells["E2"].Style.BackColor = Color.GreenYellow;

                if (_Invent != "") { if (Double.Parse(Myrow.Cells["E2"].Value.ToString()) > Double.Parse(Myrow.Cells["Retail"].Value.ToString())) Myrow.Cells["Retail"].Style.BackColor = Color.Red; }

                for (int iColumns = fixColumn; iColumns < DataGridView_Show.Columns.Count; iColumns++)
                {
                    if (Myrow.Cells[iColumns].Value == null) continue;

                    if (CheckColumnsBranch(Myrow.Cells[iColumns].OwningColumn.Name))
                    {
                        if (double.Parse(Myrow.Cells[iColumns].Value.ToString()) > 0) Myrow.Cells[iColumns].Style.BackColor = Color.SkyBlue;
                        else Myrow.Cells[iColumns].Style.BackColor = Color.Thistle;
                    }
                    else Myrow.Cells[iColumns].Style.BackColor = Color.White;
                }
            }
        }
        //set color ตรงสาขา
        private void DataGridViewRowcolor()
        {
            DataGridViewRow Myrow = DataGridView_Show.CurrentRow;
            double sumqty = 0;
            for (int iColumns = fixColumn; iColumns < DataGridView_Show.Columns.Count; iColumns++)
            {
                if (Myrow.Cells[iColumns].Value == null) continue;

                if (CheckColumnsBranch(Myrow.Cells[iColumns].OwningColumn.Name))
                {
                    if (double.Parse(Myrow.Cells[iColumns].Value.ToString()) > 0) Myrow.Cells[iColumns].Style.BackColor = Color.SkyBlue;
                    else Myrow.Cells[iColumns].Style.BackColor = Color.Thistle;

                    sumqty += sumqty;
                }
                else Myrow.Cells[iColumns].Style.BackColor = Color.White;
            }
            if (sumqty == 0) Myrow.Cells[7].Style.BackColor = Color.White;
        }
        //ok get แก้ไขจำนวนในช่องคอลัมสาขา
        private void DataGridView_Show_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridView_Show.CurrentCell.ColumnIndex > fixColumn && DataGridView_Show.CurrentCell.ColumnIndex % 2 == 0) return;
            //if (!_StatusDocument.Equals("SAVE")) return;

            if (DataGridView_Show.CurrentCell.ColumnIndex > fixColumn
            && CheckColumnsBranch(DataGridView_Show.Columns[DataGridView_Show.CurrentCell.ColumnIndex].Name.ToString())) //Desired Column
            {
                string QtyInput = "0";
                if (!(DataGridView_Show.CurrentRow.Cells[DataGridView_Show.CurrentCell.ColumnIndex].Value is null)) QtyInput = DataGridView_Show.CurrentRow.Cells[DataGridView_Show.CurrentCell.ColumnIndex].Value.ToString();



                string Branch = DataGridView_Show.Columns[DataGridView_Show.CurrentCell.ColumnIndex].Name.ToString();
                string Barcode = DataGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();

                if (double.TryParse(QtyInput, out _))
                {
                    //แก้ไข+-จำนวนด้วย
                    List<Qty> ListQty = new List<Qty>();
                    Qty qty = new Qty()
                    {
                        QtyBeforeEdit = QtyCell,
                        QtyEndEdit = double.Parse(QtyInput)
                    };
                    ListQty.Add(qty);

                    List<QtyUpdate> listUp = new List<QtyUpdate>();
                    QtyUpdate Tmp = new QtyUpdate()
                    {
                        Branch = Branch,
                        Barcode = Barcode,
                        Qty = Convert.ToDouble(QtyInput)
                    };
                    listUp.Add(Tmp);

                    string Transction = UpdateQty(listUp); //ConnectionClass.ExecuteSQL_Main(upQty);
                    if (!(Transction.Equals("")))
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(Transction);
                        return;
                    }
                    //แก้ไขการแสดงผลของยอดรวม
                    DataGridView_Show.CurrentRow.Cells[DataGridView_Show.CurrentCell.ColumnIndex].Value = QtyInput;
                    DataGridView_Show.CurrentRow.Cells["E2"].Value =
                        SumQtyItembarcodeAllBranch(DataGridView_Show.CurrentRow.Cells["E2"].Value.ToString(), ListQty);
                }

                //set color ยอดรวม
                if (double.Parse(DataGridView_Show.CurrentRow.Cells["E1"].Value.ToString()) > 0) DataGridView_Show.CurrentRow.Cells["E1"].Style.BackColor = Color.Lime;
                else DataGridView_Show.CurrentRow.Cells["E1"].Style.BackColor = SystemColors.Control;

                //set color ยอดรวมแก้
                if (double.Parse(DataGridView_Show.CurrentRow.Cells["E2"].Value.ToString()) > 0) DataGridView_Show.CurrentRow.Cells["E2"].Style.BackColor = Color.GreenYellow;
                else DataGridView_Show.CurrentRow.Cells["E2"].Style.BackColor = SystemColors.Control;
                //set color ข่องจำนวนส่ง
                if (double.Parse(DataGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString()) > 0) DataGridView_Show.CurrentRow.Cells[e.ColumnIndex].Style.BackColor = Color.SkyBlue;
                else DataGridView_Show.CurrentRow.Cells[e.ColumnIndex].Style.BackColor = SystemColors.Control;

            }

        }
        //ไม่สามารคีย์ได้
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                 && !char.IsDigit(e.KeyChar)
                 && e.KeyChar != '.')
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }
        //เชคช่องที่เปิดให้คีย์ได้หรือเปล่า
        private void DataGridView_Show_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //TextBox tdb = (DataGridView_Show.EditingControl as TextBox); 
            e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
            if (DataGridView_Show.CurrentCell.ColumnIndex > fixColumn) //Desired Column
            {
                if (e.Control is TextBox)
                {
                    TextBox tb = e.Control as TextBox;
                    tb.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                }
            }
        }
        //ok get การแสดงข้อมูลของสาขาที่ระบุ
        private void DataGridView_ShowBranch_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                string AGVBranch, Branch;
                AGVBranch = "AGV" + DataGridView_ShowBranch.CurrentRow.Cells[1].Value.ToString();
                Branch = DataGridView_ShowBranch.CurrentRow.Cells[1].Value.ToString();
                switch (DataGridView_ShowBranch.CurrentRow.Cells[0].Value.ToString())
                {
                    case "1":
                        DataGridView_Show.Columns[AGVBranch].Visible = false;
                        DataGridView_Show.Columns[Branch].Visible = false;
                        DataGridView_ShowBranch.CurrentRow.Cells[0].Value = 0;
                        break;
                    case "0":
                        DataGridView_Show.Columns[AGVBranch].Visible = true;
                        DataGridView_Show.Columns[Branch].Visible = true;
                        DataGridView_ShowBranch.CurrentRow.Cells[0].Value = 1;
                        break;
                }
            }
        }
        //ข้อมูลจำนวนก่อนการแก้ไขยอด ?
        private void DataGridView_Show_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!_StatusDocument.Equals("SAVE"))
            {
                e.Cancel = true;
                return;
            }

            if (CheckColumnsBranch((DataGridView_Show.Columns[DataGridView_Show.CurrentCell.ColumnIndex].HeaderText)))
            {
                string Qty = DataGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString();
                if (Qty.Equals(""))
                {
                    QtyCell = 0;
                }
                QtyCell = double.Parse(Qty);
            }
            else
            {
                e.Cancel = true;
            }

        }
        //ok get Export to excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = _Dept + "_" + _DateReceive;

                try
                {
                    int Columns = 1;
                    worksheet.Columns[1].NumberFormat = "@";
                    for (int iColumn = 4; iColumn < 10; iColumn++)
                    {
                        worksheet.Cells[2, Columns].value = DataGridView_Show.Columns[iColumn].HeaderText;
                        worksheet.Cells[2, Columns].Orientation = 90;
                        for (int iRow = 0; iRow < DataGridView_Show.Rows.Count; iRow++)
                        {
                            worksheet.Cells[iRow + 3, Columns].value = DataGridView_Show.Rows[iRow].Cells[iColumn].Value.ToString();
                        }
                        Columns += 1;
                    }

                    DataTable DtRoute = LogisticClass.GetRouteAll_GroupBranch(" AND BRANCH_ORDERTOAX = '1' ");//Dept  GetRouteExcel_ByDept
                    if (DtRoute.Rows.Count == 0) return;
                    int ColumnsRoute = 7;
                    for (int iRoute = 0; iRoute < DtRoute.Rows.Count; iRoute++)
                    {
                        worksheet.Cells[1, ColumnsRoute].value = DtRoute.Rows[iRoute]["ROUTEID"].ToString();
                        worksheet.Cells[2, ColumnsRoute].value = DtRoute.Rows[iRoute]["BRANCH"].ToString();
                        worksheet.Cells[2, ColumnsRoute].Orientation = 90;

                        int index = GetIndexColumns(DtRoute.Rows[iRoute]["BRANCH"].ToString().Substring(0, 5), DataGridView_Show);
                        for (int iRow = 0; iRow < DataGridView_Show.Rows.Count; iRow++)
                        {
                            if (index == 0)
                            {
                                worksheet.Cells[iRow + 3, ColumnsRoute].value = "|-|";
                            }
                            else
                            {
                                if (DataGridView_Show.Rows[iRow].Cells[index].Value.ToString() == "0")
                                {
                                    worksheet.Cells[iRow + 3, ColumnsRoute].value = "-";
                                }
                                else
                                {
                                    worksheet.Cells[iRow + 3, ColumnsRoute].value = DataGridView_Show.Rows[iRow].Cells[index].Value.ToString();
                                }

                            }

                        }
                        ColumnsRoute += 1;
                    }
                    //Getting the location and file name of the excel to save from user. 
                    SaveFileDialog saveDialog = new SaveFileDialog
                    {
                        Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                        FilterIndex = 2
                    };

                    if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        workbook.SaveAs(saveDialog.FileName);
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกข้อมูลลง Excel เสร็จสมบูรณ์");
                    }
                }
                catch (System.Exception ex)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถบันทึกข้อมูลลง Excel ได้{Environment.NewLine}{ex.Message}");
                }

                finally
                {
                    app.Quit();
                    workbook = null;
                    worksheet = null;
                }
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถบันทึกข้อมูลลง Excel ได้{Environment.NewLine}{ex.Message}");
            }
        }

        //ปุ่มคัดลอก
        private void DataGridView_Show_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 0://เลือกเพื่อ Export Excel
                    if (RadLabel_Status.Text.Equals("กรุณาบันทึก"))
                    { MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาบันทึกเอกสารก่อนใช้งาน ฟังก์ชั่นนี้"); return; }
                    if (DataGridView_Show.CurrentRow.Cells["CC"].Value.ToString() == "0") DataGridView_Show.CurrentRow.Cells["CC"].Value = "1";
                    else DataGridView_Show.CurrentRow.Cells["CC"].Value = "0";
                    break;
                case 1://แก้ไขจำนวน
                    //เช็คก่อนว่าบันทึกเอกสารแล้วหรือเปล่า
                    if (RadLabel_Status.Text.Equals("กรุณาบันทึก"))
                    { MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาบันทึกเอกสารก่อนใช้งาน แก้ไขจำนวน"); return; }

                    string Barcode = DataGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();
                    string Itemname = DataGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString();
                    string unit = DataGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString();

                    OrderToAX_InputData _Choose = new OrderToAX_InputData(Itemname, unit);
                    string sta;
                    double qty;
                    if (_Choose.ShowDialog() != DialogResult.Yes) return;
                    else
                    {
                        sta = _Choose.pCase; qty = _Choose.pQty;
                    }

                    switch (sta)
                    {
                        //case "1":
                        //    Cursor = Cursors.WaitCursor;
                        //    DataTable DtItemCopy = MainData.GetItemCopy(Barcode);
                        //    if (DtItemCopy.Rows.Count == 0)
                        //    {
                        //        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีข้อมูลการตั้งค่าของสินค้าไว้{Environment.NewLine}ไม่สามารถคัดลอกเอกสารตามค่าที่ตั้งไว้ได้{Environment.NewLine}เช็คข้อมูลใหม่อีกครั้ง");
                        //        Cursor = Cursors.Default;
                        //        return;
                        //    }

                        //    List<QtyUpdate> listUpdateqty = new List<QtyUpdate>();
                        //    foreach (DataRow row in DtItemCopy.Rows)
                        //    {
                        //        string nameindex = string.Empty;
                        //        foreach (DataGridViewColumn column in DataGridView_Show.Columns)
                        //            if (column.Name.Equals(row.ItemArray[0].ToString(), StringComparison.InvariantCultureIgnoreCase))
                        //            {
                        //                nameindex = column.Name.ToString();
                        //                break;
                        //            }
                        //        if (nameindex.Equals("")) continue;

                        //        int Qty = int.Parse(row.ItemArray[2].ToString());
                        //        //+- จำนวนก่อนหน้าจะเปลี่ยน 
                        //        int QtyOld = int.Parse(DataGridView_Show.CurrentRow.Cells[nameindex].Value.ToString());
                        //        //int QtyOldd = int.Parse(DataGridView_Show.CurrentRow.Cells[nameindex].Value.ToString());
                        //        DataGridView_Show.CurrentRow.Cells[nameindex].Value = Qty;

                        //        DataGridView_Show.CurrentRow.Cells["E2"].Value = Convert.ToDouble(DataGridView_Show.CurrentRow.Cells["E2"].Value.ToString()) - (QtyOld - Qty);
                        //        //qty
                        //        QtyUpdate Tmp = new QtyUpdate()
                        //        {
                        //            Branch = row.ItemArray[0].ToString(),
                        //            Barcode = Barcode,
                        //            Qty = Qty
                        //        };
                        //        listUpdateqty.Add(Tmp);
                        //    }

                        //    string retTransction = UpdateQty(listUpdateqty);
                        //    if (!retTransction.Equals(""))
                        //    {
                        //        MsgBoxClass.MsgBoxShow_SaveStatus(retTransction);
                        //        Cursor = Cursors.Default;
                        //        return;
                        //    }
                        //    //set สี
                        //    DataGridViewRowcolor();
                        //    DataGridViewcolor(DataGridView_Show);
                        //    Cursor = Cursors.Default;
                        //    break;
                        case "2"://การแทนที่จำนวนด้วยตัวเลขเดวกัน

                            List<QtyUpdate> listUpQty0 = new List<QtyUpdate>();

                            foreach (DataGridViewColumn column in DataGridView_Show.Columns)
                            {
                                if (!CheckColumnsBranch(column.Name)) continue;

                                string nameindex = column.Name.ToString();
                                double QtyOld = double.Parse(DataGridView_Show.CurrentRow.Cells[nameindex].Value.ToString());
                                DataGridView_Show.CurrentRow.Cells[nameindex].Value = qty;
                                DataGridView_Show.CurrentRow.Cells["E2"].Value = Convert.ToDouble(DataGridView_Show.CurrentRow.Cells["E2"].Value.ToString()) - (QtyOld - qty);
                                //qty
                                QtyUpdate Tmp = new QtyUpdate()
                                {
                                    Branch = column.Name,
                                    Barcode = Barcode,
                                    Qty = qty
                                };
                                listUpQty0.Add(Tmp);
                            }

                            string retTransction0 = UpdateQty(listUpQty0);
                            if (!retTransction0.Equals(""))
                            {
                                MsgBoxClass.MsgBoxShow_SaveStatus(retTransction0);
                                Cursor = Cursors.Default;
                                return;
                            }
                            //set สี
                            DataGridViewRowcolor();
                            DataGridViewcolor(DataGridView_Show);
                            Cursor = Cursors.Default;

                            break;
                        case "3"://การหารให้ลงตัว

                            List<QtyUpdate> listUpQtyPack = new List<QtyUpdate>();

                            foreach (DataGridViewColumn column in DataGridView_Show.Columns)
                            {
                                if (!CheckColumnsBranch(column.Name)) continue;

                                string nameindex = column.Name.ToString();
                                double QtyOld = double.Parse(DataGridView_Show.CurrentRow.Cells[nameindex].Value.ToString());
                                if (QtyOld > 0)
                                {
                                    double iu;
                                    if (QtyOld % qty == 0) iu = QtyOld;
                                    else iu = ((Math.Floor(QtyOld / qty)) * qty) + qty;

                                    DataGridView_Show.CurrentRow.Cells[nameindex].Value = iu;
                                    DataGridView_Show.CurrentRow.Cells["E2"].Value = Convert.ToDouble(DataGridView_Show.CurrentRow.Cells["E2"].Value.ToString()) - (QtyOld - iu);
                                    //qty
                                    QtyUpdate Tmp = new QtyUpdate()
                                    {
                                        Branch = column.Name,
                                        Barcode = Barcode,
                                        Qty = iu
                                    };
                                    listUpQtyPack.Add(Tmp);
                                }

                            }

                            string retTransctionpack = UpdateQty(listUpQtyPack);
                            if (!retTransctionpack.Equals(""))
                            {
                                MsgBoxClass.MsgBoxShow_SaveStatus(retTransctionpack);
                                Cursor = Cursors.Default;
                                return;
                            }
                            //set สี
                            DataGridViewRowcolor();
                            DataGridViewcolor(DataGridView_Show);
                            Cursor = Cursors.Default;

                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

        }
        //ok get บันทึกข้อมูลทั้งหมดเป็นบิล MRT
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            ArrayList SqlOrder = new ArrayList();
            if (DataGridView_Show.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีรายการสินค้า ไม่สามารถบันทึกเอกสารได้"); return;
            }

            this.Cursor = Cursors.WaitCursor;
            foreach (DataGridViewColumn dataColumn in DataGridView_Show.Columns)
            {
                //เช็คแล้วไม่ใช่ Columns จำนวนที่ต้องการบันทึกเอกสารใหม่
                if (!CheckColumnsBranch(dataColumn.Name.ToString())) continue;

                string Docno = Class.ConfigClass.GetMaxINVOICEID("MRT", _Dept.Substring(1, 3), DateTime.Parse(_DateReceive).ToString("yyMMdd"), "8");
                string branchId = dataColumn.Name.ToString();
                string branchName = BranchClass.GetBranchNameByID(branchId);
                string StaGroupItem = "0"; if (!_GroupItem.Equals("0")) StaGroupItem = "1";

                string SqlHD = String.Format($@"
                    INSERT INTO ANDROID_ORDERTOHD(
                        DocNo, Date, Time, 
                        UserCode, Branch, BranchName , Depart, 
                        StaDoc, StaPrcDoc, StaApvDoc, 
                        StaAx, StaRound2, Box, Remark,
                        DateUp, TimeUp, WhoUp,WhoUpName,
                        DateIn, TimeIn, WhoIn,WhoInName,
                        DateApv, TimeApv, WhoApv,
                        DateAx, TimeAx, WhoAx, 
                        Round, StaGroupItem, GroupItem, 
                        DeptGroup, StaOrderCust, StatusStock)
                    VALUES      ('{Docno}', '{_DateReceive}', convert(varchar, getdate(), 24),
                        '{SystemClass.SystemUserID_M}', '{branchId}','{branchName}', '{_Dept}', 
                        '1', '0', '0', 
                        '0', '0', 0, '',
                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                        '', '', '', 
                        '', '', '', 
                        '1', '{StaGroupItem}', '{_GroupItem}', 
                        '{_Dept}', '0', '0')");
                SqlOrder.Add(SqlHD);

                for (int iRow = 0; iRow < DataGridView_Show.Rows.Count; iRow++)
                {
                    string ItemID = DataGridView_Show.Rows[iRow].Cells["ITEMID"].Value.ToString();
                    string ItemDim = DataGridView_Show.Rows[iRow].Cells["INVENTDIMID"].Value.ToString();
                    string Barcode = DataGridView_Show.Rows[iRow].Cells["ITEMBARCODE"].Value.ToString();
                    string Name = DataGridView_Show.Rows[iRow].Cells["SPC_ITEMNAME"].Value.ToString();
                    string Qty = DataGridView_Show.Rows[iRow].Cells[dataColumn.Index].Value.ToString();
                    string UnitID = DataGridView_Show.Rows[iRow].Cells["UNITID"].Value.ToString();
                    double Price = double.Parse(DataGridView_Show.Rows[iRow].Cells["SPC_PRICEGROUP3"].Value.ToString());

                    //string Sale = "0", TF = "0", CN = 0;qtyCustOR ใช้เฉพาะแผงผักเท่านั้น
                    double Sale = 0, TF = 0, CN = 0, qtyCustOR = 0;
                    string[] Data = DataGridView_Show.Rows[iRow].Cells[dataColumn.Index - 1].Value.ToString().Split('-');

                    if (Data.Length > 0)
                    {
                        if ((_Dept == "D032") || (_Dept == "D110") || (_Dept == "D164"))
                        {
                            qtyCustOR = Convert.ToDouble(Data[1].ToString());
                            Sale = Convert.ToDouble(Data[2].ToString());
                            TF = Convert.ToDouble(Data[3].ToString());
                            if (Data[4].ToString() == "0") CN = 0;
                            else CN = Convert.ToDouble(Data[4].ToString());
                        }
                        else
                        {
                            Sale = Convert.ToDouble(Data[1].ToString());
                            TF = Convert.ToDouble(Data[2].ToString());
                            if (Data[3].ToString() == "") CN = 0;
                            else CN = Convert.ToDouble(Data[3].ToString());
                        }

                    }
                    string SqlDT = string.Format($@"
                            INSERT INTO Android_OrderTODT(
                                DocNo, SeqNo, ItemID, 
                                ItemDim, Barcode, Name, 
                                QtyOrderOR, QtyOrderSaleAGV, QtyOrderTF, 
                                QtyOrderCN, Qty, QtyOrderCust, UnitID,Price,  
                                DateIn, TimeIn, WhoIn, 
                                DateUp, TimeUp, WhoUp,
                                StatusItem,Round,StatusCheck)  
                            VALUES      ('{Docno}', '{iRow + 1}','{ItemID}', 
                                '{ItemDim}', '{Barcode}', '{ConfigClass.ChecKStringForImport(Name)}', 
                                '{Qty}','{Sale}','{TF}',
                                '{CN}','{Qty}','{qtyCustOR}', '{UnitID}', '{Price}', 
                                convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}', 
                                convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}', 
                                '0', '1', '0')");
                    SqlOrder.Add(SqlDT);
                }
            }
            string Transction = ConnectionClass.ExecuteSQL_ArrayMain(SqlOrder);
            MsgBoxClass.MsgBoxShow_SaveStatus(Transction);
            if (Transction.Equals(""))
            {
                RadLabel_Status.Text = "บันทึก";
                _StatusDocument = "SAVE";
                RadLabel_Status.ForeColor = Color.Blue;
                SetEnable(1);
            }
            this.Cursor = Cursors.Default;
        }
        //MyJeeds Add Set Grid Bch 
        void SetGrid(string setCase, string valueSet)
        {
            for (int i = 0; i < DataGridView_ShowBranch.Rows.Count; i++)
            {
                if (setCase == "1")
                {
                    if (DataGridView_ShowBranch.Rows[i].Cells["BRANCH_OUTPHUKET"].Value.ToString() == valueSet)
                    {
                        DataGridView_ShowBranch.Rows[i].Cells["Choose"].Value = "1";

                        int index = GetIndexColumns(DataGridView_ShowBranch.Rows[i].Cells["Branch_ID"].Value.ToString(), DataGridView_Show);
                        DataGridView_Show.Columns[index].Visible = true;
                        DataGridView_Show.Columns[index - 1].Visible = true;
                    }
                    else
                    {
                        DataGridView_ShowBranch.Rows[i].Cells["Choose"].Value = "0";

                        int index = GetIndexColumns(DataGridView_ShowBranch.Rows[i].Cells["Branch_ID"].Value.ToString(), DataGridView_Show);
                        DataGridView_Show.Columns[index].Visible = false;
                        DataGridView_Show.Columns[index - 1].Visible = false;
                    }
                }
                else
                {
                    DataGridView_ShowBranch.Rows[i].Cells["Choose"].Value = "0";

                    int index = GetIndexColumns(DataGridView_ShowBranch.Rows[i].Cells["Branch_ID"].Value.ToString(), DataGridView_Show);
                    DataGridView_Show.Columns[index].Visible = false;
                    DataGridView_Show.Columns[index - 1].Visible = false;
                }

            }
        }
        //ok get เลือกทั้งหมด
        private void RadioButton_Choose_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in DataGridView_ShowBranch.Rows)
            {
                row.Cells["Choose"].Value = "1";
                int index = GetIndexColumns(row.Cells["Branch_ID"].Value.ToString(), DataGridView_Show);
                DataGridView_Show.Columns[index].Visible = true;
                DataGridView_Show.Columns[index - 1].Visible = true;
            }
        }
        //ok get ยกเลิกทั้งหมด
        private void RadioButton_Cencel_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in DataGridView_ShowBranch.Rows)
            {
                row.Cells["Choose"].Value = "0";
                int index = GetIndexColumns(row.Cells["Branch_ID"].Value.ToString(), DataGridView_Show);
                DataGridView_Show.Columns[index].Visible = false;
                DataGridView_Show.Columns[index - 1].Visible = false;
            }
        }
        //MyJeeds Add out bch
        private void RadioButton_Out_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Out.Checked == true) SetGrid("1", "0");
            else SetGrid("0", "0");
        }
        //MyJeeds Add In bch
        private void RadioButton_In_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_In.Checked == true) SetGrid("1", "1");
            else SetGrid("0", "0");
        }

        private void DataGridView_Show_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Image path Stk
        void FindImageStock(string bch, string barcode)
        {
            string dd = Convert.ToDateTime(_DateReceive).AddDays(-1).ToString("yyyy-MM-dd");
            if (_Dept == "D032") dd = Convert.ToDateTime(_DateReceive).AddDays(-2).ToString("yyyy-MM-dd");

            string pathF = PathImageClass.pPathSendStock + @"\" + dd + "";
            DirectoryInfo DirInfo = new DirectoryInfo(pathF);
            FileInfo[] FilesImage = DirInfo.GetFiles("StkMN-" + bch + @"-*#" + barcode + @".JPG", SearchOption.AllDirectories);

            if (FilesImage.Length > 0)
            {
                ImageClass.OpenShowImage("1", pathF + @"|" + FilesImage[FilesImage.Length - 1].Name);
                return;
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูลรูปส่งสต็อกจากสาขา");
                return;
            }
        }
        //Import Order
        private void RadButtonElement_AddValueExcel_Click(object sender, EventArgs e)
        {
            ArrayList B = new ArrayList();
            OrderToAx_ImportExcel docOpen = new OrderToAx_ImportExcel("0", _Dept, _GroupItem, _DateReceive, B, 0);
            if (docOpen.ShowDialog() == DialogResult.Yes)
            {
                ShowQtyMRTO_ByDept();
                DataGridViewcolor(DataGridView_Show);
            }
        }
        //Export เพื่อทำออเดอร์
        private void RadButtonElement_Export_Click(object sender, EventArgs e)
        {
            string itemID = "'";
            double iCC = 0;
            ArrayList barcodeExport = new ArrayList
            {
                ""
            };
            for (int i = 0; i < DataGridView_Show.Rows.Count; i++)
            {
                if (DataGridView_Show.Rows[i].Cells["CC"].Value.ToString() == "1")
                {
                    barcodeExport.Add(DataGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + "|" +
                        DataGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + "|" +
                        DataGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + "|" +
                        DataGridView_Show.Rows[i].Cells["QTY"].Value.ToString() + "|" +
                        DataGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString());
                    itemID += DataGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + "','";
                    iCC++;
                }
            }

            if (iCC == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("เลือกสินค้าให้เรียบร้อยก่อนการใช้งานฟังก์ชั่นนี้");
                return;
            }

            barcodeExport[0] = itemID.Substring(0, itemID.Length - 2);

            FormShare.InputData f = new FormShare.InputData("0", "จำนวนวันย้อนหลังที่ต้องการออกข้อมูล", "ระบุจำนวน.", "วัน")
            {
                pInputData = "5"
            };
            if (f.ShowDialog(this) == DialogResult.Yes)
            {
                int iD = Convert.ToInt16(f.pInputData);
                OrderToAx_ImportExcel docOpen = new OrderToAx_ImportExcel("1", _Dept, _GroupItem, _DateReceive, barcodeExport, iD);
                docOpen.Show();
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุจำนวนวันที่ต้องการดึงข้อมูลย้อนหลัง.");
                return;
            }

        }

        private void RadCheckBox_Border_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Border.Checked == true) DataGridView_Show.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            else DataGridView_Show.CellBorderStyle = DataGridViewCellBorderStyle.None;
        }

        //ดูรูปส่งสต็อกของแผนกผลไม้
        private void DataGridView_Show_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (_Dept != "D164") return;
            if (DataGridView_Show.CurrentCell.ColumnIndex > fixColumn && DataGridView_Show.CurrentCell.ColumnIndex % 2 == 0)
            { return; }

            switch (_Dept)
            {
                case "D032":
                    if (DataGridView_Show.CurrentCell.ColumnIndex > fixColumn && (CheckColumnsBranch(DataGridView_Show.Columns[DataGridView_Show.CurrentCell.ColumnIndex].Name.ToString()) == false)) //Desired Column
                    {
                        FindImageStock(DataGridView_Show.Columns[DataGridView_Show.CurrentCell.ColumnIndex + 1].Name.ToString(), DataGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString());
                    }
                    break;
                case "D164":
                    if (DataGridView_Show.CurrentCell.ColumnIndex > fixColumn && (CheckColumnsBranch(DataGridView_Show.Columns[DataGridView_Show.CurrentCell.ColumnIndex].Name.ToString()) == false)) //Desired Column
                    {
                        FindImageStock(DataGridView_Show.Columns[DataGridView_Show.CurrentCell.ColumnIndex + 1].Name.ToString(), DataGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString());
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
