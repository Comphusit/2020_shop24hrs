﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_Main : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dtDataHD = new DataTable();
        readonly DataTable dtDataDT = new DataTable();
        //readonly string pDept; // shop - supc  
        public string pDocno;
        private string Dept, GroupItem, DateItem, DateSaleBegin, DateSaleEnd, RoundDocno;
        string TypeOpenOR;//0 = MNOR 1 = Sale
        //  string statusGroupItem;
        public OrderToAx_Main()
        {
            InitializeComponent();
        }
        //set Dpt ที่ใช้งาน
        void Set_Dept()
        {
            RadDropDownList_Dept.DataSource = MRTClass.GetDept(SystemClass.SystemDptID);
            RadCheckBox_Dept.Checked = true;
            RadDropDownList_Dept.Enabled = true;
            RadDropDownList_Dept.ValueMember = "NUM";
            RadDropDownList_Dept.DisplayMember = "DeptName";
            RadDropDownList_Dept.SelectedIndex = 0;
        }
        //กลุ่มสินค้าตามแผนก
        void Set_GroupItem()
        {
            RadDropDownList_GroupItem.DataSource = MRTMainClass.GetGroupItem_ByDept(RadDropDownList_Dept.SelectedValue.ToString());
            RadDropDownList_GroupItem.ValueMember = "ITEMGROUPID";
            RadDropDownList_GroupItem.DisplayMember = "ITEMGROUPNAME";
        }
        //Load
        private void OrderToAx_Main_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            RadButtonElement_pdt.ShowBorder = true;
            radButtonElement_Add.ShowBorder = true;
            radButtonElement_Add.ToolTipText = "Clear";
       
            RadCheckBox_GroupItem.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            RadCheckBox_Dept.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GroupItem);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);

            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_SaleB, DateTime.Now.AddDays(-15), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(RadDateTimePicker_SaleE, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("OUTPHUKET", "ประเภทสาขา", 100)));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("CMNORDOCNO", "จำนวนเอกสาร MNOR", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("CMRTODocNo", "จำนวนเอกสาร MRT", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("SUMQTY", "จำนวนรายการ MRT", 150));

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Item.ButtonElement.ShowBorder = true;
            RadButton_OrderCust.ButtonElement.ShowBorder = true;
            RadButton_OrderCustRound2.ButtonElement.ShowBorder = true;
            radButton_ItemRound2.ButtonElement.ShowBorder = true;

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "CMNORDOCNO = 0 ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_Show.Columns["CMNORDOCNO"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "CMRTODocNo > 0", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            RadGridView_Show.Columns["CMRTODocNo"].ConditionalFormattingObjectList.Add(obj2);

            ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition1", "SUMQTY = 0", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_Show.Columns["SUMQTY"].ConditionalFormattingObjectList.Add(obj3);
            //RadGridView_Show.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj3);
            //RadGridView_Show.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj3);

            RadDateTimePicker_SendItemDate.Value = DateTime.Now.AddDays(0);
            RadDateTimePicker_SendItemDate.MaxDate = DateTime.Now.AddDays(5);

            RadDateTimePicker_SaleB.Value = DateTime.Now.AddDays(-7);
            RadDateTimePicker_SaleE.Value = DateTime.Now.AddDays(0);

            radButtonElement_DetailPdf.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_DetailPdf.ShowBorder = true;

            RadButtonElement_DetailMange.ToolTipText = "จัดการสินค้า";
            RadButtonElement_DetailMange.ShowBorder = true;

            radButtonElement_ExcelGroup.ToolTipText = "สินค้าส่งรวม";
            radButtonElement_ExcelGroup.ShowBorder = true;

            radButtonElement_ExcelOrder.ToolTipText = "สินค้าส่งแยกประเภท";
            radButtonElement_ExcelOrder.ShowBorder = true;

            Set_Dept();

            if (!(RadDropDownList_Dept.DataSource is null)) Set_GroupItem();

            ClearDept();

        }
        //Clear Data
        void ClearDept()
        {
            RadCheckBox_GroupItem.Checked = false;
            RadDropDownList_GroupItem.Enabled = false;
            RadButton_Item.Enabled = false;
            //Dept = string.Empty;// RadDropDownList_Dept.SelectedValue.ToString();
            GroupItem = "";
            //statusGroupItem = "0";
            if (RadCheckBox_GroupItem.Checked == true) GroupItem = RadDropDownList_GroupItem.SelectedValue.ToString();
            RoundDocno = "1";
            radioButton_Round1.Checked = true;
            radioButton_Retail.Checked = false;
            radioButton_Wha.Checked = false;
            switch (RadDropDownList_Dept.SelectedValue.ToString())
            {
                //case "D027":
                //case "D019":
                case "D053":
                    RadioButton_Sale.Enabled = true;
                    RadioButton_Sale.Checked = true;
                    RadDateTimePicker_SaleE.Enabled = true;
                    RadDateTimePicker_SaleB.Enabled = true;

                    RadioButton_DateItem.Enabled = true;
                    RadioButton_DateItem.Checked = false; TypeOpenOR = "1";
                    RadDateTimePicker_SendItemDate.Enabled = true;

                    DateItem = DateTime.Now.AddDays(0).ToString("yyyy-MM-dd");
                    break;
                case "D159":
                    RadioButton_Sale.Enabled = true;
                    RadioButton_Sale.Checked = true;
                    RadDateTimePicker_SaleE.Enabled = true;
                    RadDateTimePicker_SaleB.Enabled = true;

                    RadioButton_DateItem.Enabled = true;
                    RadioButton_DateItem.Checked = false; TypeOpenOR = "1";
                    RadDateTimePicker_SendItemDate.Enabled = true;

                    DateItem = DateTime.Now.AddDays(0).ToString("yyyy-MM-dd");
                    break;
                default:
                    RadioButton_Sale.Enabled = false;
                    RadioButton_Sale.Checked = false;
                    RadDateTimePicker_SaleE.Enabled = false;
                    RadDateTimePicker_SaleB.Enabled = false;

                    RadioButton_DateItem.Enabled = true;
                    RadioButton_DateItem.Checked = true; TypeOpenOR = "0";
                    RadDateTimePicker_SendItemDate.Enabled = true;

                    DateItem = RadDateTimePicker_SendItemDate.Value.ToString("yyyy-MM-dd");
                    break;
            }

        }
        //bool IsDeptOrder(string DeptId)
        //{
        //    string[] Depts = { "D147", "D159", "D067", "D019" };
        //    return -1 != Array.IndexOf(Depts, DeptId);
        //}

        //Find เอกสารว่าได้สร้าง MNT แล้วหรือไม่  และมีเอกสาร MNOR หรือเปล่า
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            RadGridView_Show.DataSource = MRTMainClass.GetMNOR(Dept, GroupItem, DateItem, RoundDocno);
            if (RadGridView_Show.Rows.Count > 0) RadButton_Item.Enabled = true;
            this.Cursor = Cursors.Default;
        }
        //เลือกเเผนก
        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.DataSource != null && RadCheckBox_Dept.Checked == true)
            {
                Dept = RadDropDownList_Dept.SelectedValue.ToString();
                ClearDept();
                Set_GroupItem();
            }
        }
        //เลือกรอบ 1
        private void RadioButton_Round1_CheckedChanged(object sender, EventArgs e)
        {
            RoundDocno = "1";
        }
        //เลือกรอบ 2
        private void RadioButton_Round2_CheckedChanged(object sender, EventArgs e)
        {
            RoundDocno = "2";
        }
        //sale begin
        private void RadDateTimePicker_SaleBegin_ValueChanged(object sender, EventArgs e)
        {
            RadDateTimePicker_SaleB.MaxDate = RadDateTimePicker_SaleE.Value;
            RadDateTimePicker_SaleE.MinDate = RadDateTimePicker_SaleB.Value;
            DateSaleBegin = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
        }
        //sale End
        private void RadDateTimePicker_SaleEnd_ValueChanged(object sender, EventArgs e)
        {
            RadDateTimePicker_SaleB.MaxDate = RadDateTimePicker_SaleE.Value;
            RadDateTimePicker_SaleE.MinDate = RadDateTimePicker_SaleB.Value;
            DateSaleEnd = RadDateTimePicker_SaleB.Value.ToString("yyyy-MM-dd");
        }
        //Check Order ลูกค้า
        private void RadButton_OrderCust_Click(object sender, EventArgs e)
        {
            //using (OrderToAx_ShowDetail OrderToAx_ShowDetail = new OrderToAx_ShowDetail(1))
            //{
            //    OrderToAx_ShowDetail.DateDocno = DateItem;
            //    OrderToAx_ShowDetail.Dept = Dept;
            //    DialogResult dr = OrderToAx_ShowDetail.ShowDialog();
            //    if (dr == DialogResult.Yes)
            //    {
            //        //SetGridAndData(MRTClass.GetMRTDocnoHD(OrderToAx_FindData.pDocno));
            //    }
            //}
        }
        //จัดการสินค้าทั้งหมด
        private void RadButton_Item_Click(object sender, EventArgs e)
        {
            //ตรวจสอบเอกสารก่อนจัดการสินค้า
            String StatusDocument = "NO";
            if (MainData.CheckDocument(Dept, DateItem, RoundDocno, GroupItem))
            {
                DialogResult rsu = MsgBoxClass.MsgBoxShowYesNoCencel_DialogResult($@"มีเอกสารจัดสินค้าวันที่  " + DateItem + System.Environment.NewLine + "แผนก  " + RadDropDownList_Dept.SelectedItem.ToString() + "  แล้ว " + Environment.NewLine + "ต้องการเริ่มต้นใหม่หรือไม่");
                if (rsu == DialogResult.Cancel)
                {
                    return;
                }
                else if (rsu == DialogResult.Yes)
                {
                    string SqlGroupItemCondition = string.Empty;
                    if (!GroupItem.Equals(""))
                    {
                        SqlGroupItemCondition = string.Format(@"AND GroupItem = '{0}' ", GroupItem);
                    }

                    String SqlUp = string.Format(@"
                            update  ANDROID_ORDERTOHD set StaPrcDoc='3' , 
                                    DateUp = convert(varchar, Date, 23), TimeUp = convert(varchar, Date, 24), WhoUp = '{0}'
                            where   convert(varchar, Date, 23) = '{1}'
                                    AND StaApvDoc='0' AND StaAx='0' AND Depart = '{2}' {3}  AND Remark='' "
                                    , SystemClass.SystemUserID_M, DateItem, Dept, SqlGroupItemCondition);

                    string SqlUpHDTranction = ConnectionClass.ExecuteSQL_Main(SqlUp);
                    if (SqlUpHDTranction != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpHDTranction);
                        return;
                    }

                    StatusDocument = "NO";
                }
                else if (rsu == DialogResult.No)
                {
                    StatusDocument = "SAVE";
                }
            }
            // String StatusDocument = "NO";
            string dateSend = DateItem, dateEnd = "", dateBegin = "";
            if (RadioButton_Sale.Checked == true)
            {
                dateBegin = RadDateTimePicker_SaleB.Value.ToString("yyyy-MM-dd");
                dateEnd = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
                dateSend = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
            }
            string invent = "";
            if (radioButton_Wha.Checked == true) invent = "WH-A";
            if (radioButton_Retail.Checked == true) invent = "RETAILAREA";

            OrderToAx_MainData OrderToAx_MainData = new OrderToAx_MainData(StatusDocument, TypeOpenOR, RadDropDownList_Dept.SelectedValue.ToString(), dateSend, GroupItem, dateBegin, dateEnd, invent) { };
            OrderToAx_MainData.Show();
            //if (RadDropDownList_Dept.SelectedValue.ToString().Equals("D164"))
            //{
            //    //ผลไม้
            //    using (OrderToAx_MainDataFruit OrderToAx_MainDataFruit = new OrderToAx_MainDataFruit())
            //    {
            //        OrderToAx_MainDataFruit.Dept = RadDropDownList_Dept.SelectedValue.ToString();
            //        if (RadDropDownList_GroupItem.SelectedValue != null && RadCheckBox_GroupItem.Checked == true)
            //        {
            //            OrderToAx_MainDataFruit.GroupItem = GroupItem;
            //        }
            //        else
            //        {
            //            OrderToAx_MainDataFruit.GroupItem = "0";
            //        }
            //        OrderToAx_MainDataFruit.DateReceive = string.Empty;
            //        OrderToAx_MainDataFruit.DateSaleBegin = string.Empty;
            //        OrderToAx_MainDataFruit.DateSaleEnd = string.Empty;
            //        OrderToAx_MainDataFruit.Round = RoundDocno;
            //        if (RadioButton_DateItem.Checked == true)
            //        {
            //            OrderToAx_MainDataFruit.DateReceive = DateItem;
            //        }
            //        if (RadioButton_Sale.Checked == true)
            //        {
            //            OrderToAx_MainDataFruit.DateSaleBegin = RadDateTimePicker_SaleB.Value.ToString("yyyy-MM-dd");
            //            OrderToAx_MainDataFruit.DateSaleEnd = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
            //            OrderToAx_MainDataFruit.DateReceive = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
            //        }
            //        OrderToAx_MainDataFruit.StatusDocument = StatusDocument;
            //        DialogResult dr = OrderToAx_MainDataFruit.ShowDialog();
            //        if (dr == DialogResult.Yes)
            //        {
            //            //SetGridAndData(MRTClass.GetMRTDocnoHD(OrderToAx_FindData.pDocno));
            //        }
            //    }
            //}
            //else
            //{
            //    OrderToAx_MainData OrderToAx_MainData = new OrderToAx_MainData(StatusDocument)
            //    {
            //        Dept = RadDropDownList_Dept.SelectedValue.ToString(),
            //    };
            //    if (RadDropDownList_GroupItem.SelectedValue != null && RadCheckBox_GroupItem.Checked == true)
            //    {
            //        OrderToAx_MainData.GroupItem = GroupItem;
            //    }
            //    else
            //    {
            //        OrderToAx_MainData.GroupItem = "0";
            //    }
            //    OrderToAx_MainData.DateReceive = string.Empty;
            //    OrderToAx_MainData.DateSaleBegin = string.Empty;
            //    OrderToAx_MainData.DateSaleEnd = string.Empty;
            //    if (RadioButton_DateItem.Checked == true)
            //    {
            //        OrderToAx_MainData.DateReceive = DateItem;
            //    }
            //    if (RadioButton_Sale.Checked == true)
            //    {
            //        OrderToAx_MainData.DateSaleBegin = RadDateTimePicker_SaleB.Value.ToString("yyyy-MM-dd");
            //        OrderToAx_MainData.DateSaleEnd = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
            //        OrderToAx_MainData.DateReceive = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
            //    }

            //    OrderToAx_MainData.Show();
            //    //using (OrderToAx_MainData OrderToAx_MainData = new OrderToAx_MainData(StatusDocument))
            //    //{
            //    //    OrderToAx_MainData.Dept = RadDropDownList_Dept.SelectedValue.ToString();
            //    //    if (RadDropDownList_GroupItem.SelectedValue != null && RadCheckBox_GroupItem.Checked == true)
            //    //    {
            //    //        OrderToAx_MainData.GroupItem = GroupItem;
            //    //    }
            //    //    else
            //    //    {
            //    //        OrderToAx_MainData.GroupItem = "0";
            //    //    }
            //    //    OrderToAx_MainData.DateReceive = string.Empty;
            //    //    OrderToAx_MainData.DateSaleBegin = string.Empty;
            //    //    OrderToAx_MainData.DateSaleEnd = string.Empty;
            //    //    if (RadioButton_DateItem.Checked == true)
            //    //    {
            //    //        OrderToAx_MainData.DateReceive = DateItem;
            //    //    }
            //    //    if (RadioButton_Sale.Checked == true)
            //    //    {
            //    //        OrderToAx_MainData.DateSaleBegin = RadDateTimePicker_SaleB.Value.ToString("yyyy-MM-dd");
            //    //        OrderToAx_MainData.DateSaleEnd = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
            //    //        OrderToAx_MainData.DateReceive = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
            //    //    }

            //    //    //OrderToAx_MainData.StatusDocument = StatusDocument;
            //    //    DialogResult dr = OrderToAx_MainData.ShowDialog();
            //    //    if (dr == DialogResult.Yes)
            //    //    {
            //    //        //SetGridAndData(MRTClass.GetMRTDocnoHD(OrderToAx_FindData.pDocno));
            //    //    }
            //    //}
            //}
        }

        private void RadButtonElement_ExcelGroup_Click(object sender, EventArgs e)
        {
            //try
            //{
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            app.Visible = true;
            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            worksheet.Name = "ออร์เดอร์" + DateItem;

            try
            {
                DataTable Dtitem = MRTClass.GetSumItemMRTO_ByDate(Dept, GroupItem, DateItem);
                if (Dtitem.Rows.Count == 0) return;

                worksheet.Columns[1].NumberFormat = "@"; worksheet.Columns[1].ColumnWidth = 15;
                worksheet.Columns[2].NumberFormat = "@"; worksheet.Columns[2].ColumnWidth = 30;
                worksheet.Columns[3].NumberFormat = "@"; worksheet.Columns[3].ColumnWidth = 15;
                worksheet.Columns[4].NumberFormat = "#,#0.00"; worksheet.Columns[4].ColumnWidth = 15;
                worksheet.Columns[5].NumberFormat = "#,#0.00"; worksheet.Columns[5].ColumnWidth = 15;
                worksheet.Columns[6].NumberFormat = "#,#0.00"; worksheet.Columns[5].ColumnWidth = 15;

                worksheet.Cells[1, 1].value = "บาร์โค๊ด";
                worksheet.Cells[1, 2].value = "ชื่อสินค้า";
                worksheet.Cells[1, 3].value = "หน่วย";
                worksheet.Cells[1, 4].value = "จำนวนส่งสาขา";
                worksheet.Cells[1, 5].value = "จำนวนส่งลูกค้า";
                worksheet.Cells[1, 6].value = "จำนวนส่งรวม";

                for (int iItem = 0; iItem < Dtitem.Rows.Count; iItem++)
                {
                    worksheet.Cells[iItem + 2, 1].value = Dtitem.Rows[iItem]["ITEMBARCODE"].ToString();
                    worksheet.Cells[iItem + 2, 2].value = Dtitem.Rows[iItem]["SPC_ITEMNAME"].ToString();
                    worksheet.Cells[iItem + 2, 3].value = Dtitem.Rows[iItem]["UNITID"].ToString();
                    worksheet.Cells[iItem + 2, 4].value = Dtitem.Rows[iItem]["QtyBranch"].ToString();
                    worksheet.Cells[iItem + 2, 5].value = Dtitem.Rows[iItem]["QtyCust"].ToString();
                    worksheet.Cells[iItem + 2, 6].value = Dtitem.Rows[iItem]["Qty"].ToString();
                }

                SaveFileDialog saveDialog = new SaveFileDialog
                {
                    Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                    FilterIndex = 2
                };

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    //MessageBox.Show("Export Successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกข้อมูลเรียบร้อย");
                }
            }
            catch (System.Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Export ข้อมูลเป็น Excel ได้ {Environment.NewLine}{ex.Message}");
            }

            finally
            {
                app.Quit();
                workbook = null;
                worksheet = null;
            }
        }

        private void RadButtonElement_ExcelOrder_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            //    Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            //    Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            //    app.Visible = true;
            //    worksheet = workbook.Sheets["Sheet1"];
            //    worksheet = workbook.ActiveSheet;
            //    worksheet.Name = "ออร์เดอร์" + DateItem;

            //    try
            //    {
            //        worksheet.Columns[1].NumberFormat = "@"; worksheet.Columns[1].ColumnWidth = 20;
            //        worksheet.Columns[2].NumberFormat = "@"; worksheet.Columns[2].ColumnWidth = 13;
            //        worksheet.Columns[3].NumberFormat = "@"; worksheet.Columns[3].ColumnWidth = 25;
            //        worksheet.Columns[4].NumberFormat = "@"; worksheet.Columns[4].ColumnWidth = 10;
            //        worksheet.Columns[5].NumberFormat = "#,#0.00";
            //        worksheet.Columns[6].NumberFormat = "#,#0.00";
            //        worksheet.Columns[7].NumberFormat = "#,#0.00";


            //        DataTable Dtitem = GetSumItemMRTO_ByDate(Dept, GroupItem, DateItem, RoundDocno);
            //        if (Dtitem.Rows.Count == 0) return;

            //        worksheet.Cells[1, 1].value = "กลุ่ม";
            //        worksheet.Cells[1, 2].value = "บาร์โค๊ด";
            //        worksheet.Cells[1, 3].value = "ชื่อสินค้า";
            //        worksheet.Cells[1, 4].value = "หนวย";
            //        worksheet.Cells[1, 5].value = "จำนวนสาขา";
            //        worksheet.Cells[1, 6].value = "จำนวนลูกค้า";
            //        worksheet.Cells[1, 7].value = "จำนวนรวม";


            //        for (int iItem = 0; iItem < Dtitem.Rows.Count; iItem++)
            //        {
            //            worksheet.Cells[iItem + 2, 1].value = Dtitem.Rows[iItem]["ITEMGROUP"].ToString();
            //            worksheet.Cells[iItem + 2, 2].value = Dtitem.Rows[iItem]["ITEMBARCODE"].ToString();
            //            worksheet.Cells[iItem + 2, 3].value = Dtitem.Rows[iItem]["SPC_ITEMNAME"].ToString();
            //            worksheet.Cells[iItem + 2, 4].value = Dtitem.Rows[iItem]["UNITID"].ToString();
            //            worksheet.Cells[iItem + 2, 5].value = Dtitem.Rows[iItem]["QtyBranch"].ToString();
            //            worksheet.Cells[iItem + 2, 6].value = Dtitem.Rows[iItem]["QtyCust"].ToString();
            //            worksheet.Cells[iItem + 2, 7].value = Dtitem.Rows[iItem]["QTY"].ToString();
            //        }

            //        //Getting the location and file name of the excel to save from user. 
            //        SaveFileDialog saveDialog = new SaveFileDialog
            //        {
            //            Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
            //            FilterIndex = 2
            //        };

            //        if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //        {
            //            workbook.SaveAs(saveDialog.FileName);
            //            MessageBox.Show("Export Successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        }
            //    }
            //    catch (System.Exception ex)
            //    {
            //        MessageBox.Show(ex.Message);
            //    }

            //    finally
            //    {
            //        app.Quit();
            //        workbook = null;
            //        worksheet = null;
            //    }
            //}
            //catch (Exception ex) { MessageBox.Show(ex.Message.ToString()); }
        }

        #region "FormatGrid"
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        #endregion

        //รายละเอียดบิล แต่ละรายการ
        private void RadButtonElement_DetailMange_Click(object sender, EventArgs e)
        {
            //เชคสิดแผนก
            if (!SystemClass.SystemDptID.Equals(RadDropDownList_Dept.SelectedValue.ToString()))
            {
                if (SystemClass.SystemComProgrammer != "1")
                {
                    if (!SystemClass.SystemDptID.Equals("D147") || !SystemClass.SystemDptID.Equals("D019"))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่สามารถจัดสินค้าแผนกอื่นได้.");
                        return;
                    }
                }

            }

            if (RadGridView_Show.RowCount == 0) return;
            string statusDoc = "NO";
            //เชคก่อนว่ามีเอกสาร MRT หรือ MMOR หรือไม่
            if (RadGridView_Show.CurrentRow.Cells["CMRTODocNo"].Value.ToString() == "0")
            {
                if (RadGridView_Show.CurrentRow.Cells["CMNORDOCNO"].Value.ToString() == "0")
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ไม่มีเอกสารสั่งสินค้า ยืนยันจัดสินค้าส่งหรือไม่") == DialogResult.No)
                    {
                        return;
                    }
                }
                else
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("มีเอกสารสั่งสินค้าจากสาขา ต้องการยืนยันจัดสินค้าตามจำนวนสั่งหรือไม่") == DialogResult.Yes)
                    {
                        statusDoc = "MNOR";
                    }
                }
            }
            else
            {
                statusDoc = "SAVE";
            }

            //ไปหน้าจอสั่ง
            string bchID = RadGridView_Show.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
            string bchName = RadGridView_Show.CurrentRow.Cells["BRANCH_NAME"].Value.ToString();
            string dateSend = DateItem;
            if (RadioButton_Sale.Checked == true)
            {
                dateSend = RadDateTimePicker_SaleE.Value.ToString("yyyy-MM-dd");
            }
            //string StatusGroupItem = "0";
            //if (RadCheckBox_GroupItem.Checked == true) StatusGroupItem = "1";

            using (OrderToAx_MainDocBranch OrderToAx_MainDocBranch = new OrderToAx_MainDocBranch(bchID, bchName, dateSend, RoundDocno, Dept, GroupItem, statusDoc))
            {
                //OrderToAx_MainDocBranch.StatusDocument = statusDoc;
                //OrderToAx_MainDocBranch.DocnoMRTO = "";
                //OrderToAx_MainDocBranch.StatusGroupItem = "0";
                //if (RadCheckBox_GroupItem.Checked == true) OrderToAx_MainDocBranch.StatusGroupItem = "1";
                //OrderToAx_MainDocBranch.GroupItem = GroupItem;

                DialogResult dr = OrderToAx_MainDocBranch.ShowDialog();
                if (dr == DialogResult.Yes)
                {

                }

                //refresh Grid
                RadGridView_Show.DataSource = MRTMainClass.GetMNOR(Dept, GroupItem, DateItem, RoundDocno);
            }
        }

        //ประมวลผลรอบ 2 เปนบิลใบใหม่
        private void RadButton_ItemRound2_Click(object sender, EventArgs e)
        {
            //DataTable Dtround2 = MRTClass.GetDtRound2(DateItem, Dept);
            DataTable Dtround2 = MRTShowDetail.GetDtRound2Detail(DateItem, Dept, GroupItem);
            if (Dtround2.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีรายการสินค้ารอบ 2 เช็คข้อมูลใหม่อีกคร้ง.");
                return;
            }
            using (OrderToAx_ShowDetail OrderToAx_ShowDetail = new OrderToAx_ShowDetail(3, DateItem, Dept, GroupItem, Dtround2))
            {
                //    OrderToAx_ShowDetail.DateDocno = DateItem;
                //    OrderToAx_ShowDetail.Dept = Dept;
                //    OrderToAx_ShowDetail.GroupItem = GroupItem;

                DialogResult dr = OrderToAx_ShowDetail.ShowDialog();
                if (dr == DialogResult.Yes)
                {
                    //SetGridAndData(MRTClass.GetMRTDocnoHD(OrderToAx_FindData.pDocno));
                }
            }
        }
        //Clear
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearDept();
            if (RadGridView_Show.Rows.Count > 0)
            {
                RadGridView_Show.DataSource = null;
            }
        }

        private void RadButtonElement_DetailPdf_Click(object sender, EventArgs e)
        {

        }

        //ออเดอร์ลูกค้า เพื่อบันทึก
        private void RadButton_OrderCustRound2_Click(object sender, EventArgs e)
        {
            DataTable dtOrderCst = MRTClass.GetOrderCust(Dept, DateItem);
            if (dtOrderCst.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีรายการสินค้าสำหรับออเดอร์ลูกค้า เช็คข้อมูลใหม่อีกคร้ง.");
                return;
            }
            using (OrderToAx_ShowDetail OrderToAx_ShowDetail = new OrderToAx_ShowDetail(2, DateItem, Dept, GroupItem, dtOrderCst))
            {
                //OrderToAx_ShowDetail.DateDocno = DateItem;
                //OrderToAx_ShowDetail.Dept = Dept;
                //OrderToAx_ShowDetail.GroupItem = GroupItem;

                DialogResult dr = OrderToAx_ShowDetail.ShowDialog();
                if (dr == DialogResult.Yes)
                {
                    //SetGridAndData(MRTClass.GetMRTDocnoHD(OrderToAx_FindData.pDocno));
                }
            }
        }

        //ระบุวันที่ส่งสินค้า
        private void RadDateTimePicker_SendItemDate_ValueChanged(object sender, EventArgs e)
        {
            DateItem = RadDateTimePicker_SendItemDate.Value.ToString("yyyy-MM-dd");
        }
        //เปลี่ยนกลุ่มสินค้า
        private void RadDropDownList_GroupItem_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_GroupItem.DataSource != null && RadCheckBox_GroupItem.Checked == true)
            {
                GroupItem = RadDropDownList_GroupItem.SelectedValue.ToString();
                //statusGroupItem = "1";
            }
        }
        //การเลือกกลุ่ม สินค้า
        private void RadCheckBox_GroupItem_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (RadCheckBox_GroupItem.Checked == true && RadDropDownList_GroupItem.SelectedValue != null)
            {
                RadDropDownList_GroupItem.Enabled = true;
                GroupItem = RadDropDownList_GroupItem.SelectedValue.ToString();
                //   statusGroupItem = "1";
            }
            else
            {
                RadDropDownList_GroupItem.Enabled = false;
                GroupItem = string.Empty;
                //    statusGroupItem = "0";
            }
        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
