﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_ImportExcel : Telerik.WinControls.UI.RadForm
    {
        readonly string _pCase, _ItemGroup, _DeptID, _DateSend;//pCase 0 Import 1 Export
        readonly int _DateSale;
        readonly ArrayList _Barcode;
        DataTable DtItem = new DataTable();
        DataTable DtBranch = new DataTable();
        //Load
        public OrderToAx_ImportExcel(string pCase, string DeptID, string ItemGroup, string DateSend, ArrayList Barcode, int DateSale)
        {
            InitializeComponent();
            _pCase = pCase;
            _DeptID = DeptID;
            _ItemGroup = ItemGroup;
            _DateSend = DateSend;
            _Barcode = Barcode;
            _DateSale = DateSale;
        }
        //Load
        private void OrderToAx_ImportExcel_Load(object sender, EventArgs e)
        {
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Cancel.ButtonElement.ShowBorder = true;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radLabel_Dpt.Text = _DeptID;
            radLabel_Date.Text = _DateSend;
            radLabel_Group.Text = _ItemGroup;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            RadGridView_ShowHD.MasterTemplate.EnableFiltering = false;
            DtBranch = BranchClass.GetBranchAll_ByConditions(" '1' ", " '1' ", " AND BRANCH_ORDERTOAX = '1' ");

            if (_pCase == "0")
            {
                radBrowseEditor_choose.Enabled = true;
                DtItem = MRTClass.GetItemBarcode_ByDeptItemGroup(_DeptID, _ItemGroup);
                ClearTxt();
                radLabel_Detail.Text = "ช่องที่เป็นสีแดง >> ไม่มีรายการสินค้าในกลุ่มที่ระบุ จะไม่ถูกนำเข้าทุกกรณี | File Excel จะต้องมี Sheet เดียวเท่านั้น | ยอดที่แสดง >> ตามมิติสินค้าทั้ง MRT และ SALE";
                this.Text = "นำเข้าออเดอร์จาก Excel";
                RadButton_Save.Text = "บันทึก";
            }
            else
            {
                radBrowseEditor_choose.Enabled = false;
                RadButton_Save.Enabled = true;
                LoadDataForExport();
                radLabel_Detail.Text = "ให้แก้ข้อมูลช่องสีม่วง >> เมื่อเสร็จแล้วให้ลบข้อมูลออกเหลือเฉพาะสาขาและช่องสีม่วงเพื่อนำไป Import";
                this.Text = "ออกข้อมูลเป็น Excel เพื่อระบุออเดอร์";
                RadButton_Save.Text = "Export Excel";
            }


        }
        //Load ข้อมูลทั้งหมด เพื่อออก Excel
        void LoadDataForExport()
        {
            this.Cursor = Cursors.WaitCursor;

            DataTable dtOR = MainData.GetItemBarcodeMNOR_ByDept(_DateSend, _DeptID);
            DataTable dtSale = PosSaleClass.MRT_GetSaleXXXDate(_Barcode[0].ToString(), _DateSale);//DataTable dtSale = MainData.GetSaleXXXDate(_Barcode[0].ToString(), 4);
            DataTable dtMRT = MRTClass.GetQtyMRTDate(_Barcode[0].ToString(), _DateSale);//DataTable dtMRT = MainData.GetQtyMRTDate(_Barcode[0].ToString(), 4);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
 
            for (int iC = 1; iC < _Barcode.Count; iC++)
            {
                string[] subBarcode = _Barcode[iC].ToString().Split('|');
                string barcode = subBarcode[2].ToString();

                for (int iDateSale = _DateSale; iDateSale > 0; iDateSale--)
                {
                    string DayString = DateTime.Parse(_DateSend).AddDays(iDateSale * -1).ToString("yyyy-MM-dd");
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(barcode + "_MRT" + iDateSale.ToString(), $@"MRT{Environment.NewLine}{DayString}", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(barcode + "_SALE" + iDateSale.ToString(), $@"SALE{Environment.NewLine}{DayString}", 100)));
                }


                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(barcode + "_OR", $@"ออเดอร์สาขา{Environment.NewLine}{_DateSend}", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(barcode + "_KK", $@"{barcode}:{Environment.NewLine}{subBarcode[4]}", 100)));

            }

            int iRow = 0;
            for (int iB = 0; iB < DtBranch.Rows.Count; iB++)
            {
                string bch = DtBranch.Rows[iB]["BRANCH_ID"].ToString();
                RadGridView_ShowHD.Rows.Add(bch, DtBranch.Rows[iB]["BRANCH_NAME"].ToString());

                for (int i = 1; i < _Barcode.Count; i++)
                {
                    string[] subBarcode = _Barcode[i].ToString().Split('|');
                    string item = subBarcode[0].ToString();
                    string dim = subBarcode[1].ToString();
                    string barcode = subBarcode[2].ToString();
                    double factor = double.Parse(subBarcode[3]);

                    double qrtOR = 0;//qtyOrder = 0, 
                    //double qtyMRT3 = 0, qtySale3 = 0, qtyMRT2 = 0, qtySale2 = 0, qtyMRT1 = 0, qtySale1 = 0;

                    if (dtOR.Rows.Count > 0)
                    {
                        DataRow[] rowOR = dtOR.Select($@"  BRANCH_ID= '{bch}' AND ITEMBARCODE = '{barcode}'   ");
                        if (rowOR.Length > 0) qrtOR = double.Parse(rowOR[0]["totalOR"].ToString());
                    }

                    for (int iDD = _DateSale; iDD > 0; iDD--)
                    {
                        string DD = DateTime.Parse(_DateSend).AddDays(iDD * -1).ToString("yyyy-MM-dd");
                        double qtySale = 0, qtyMRT = 0;
                        if (dtSale.Rows.Count > 0)
                        {
                            DataRow[] rowSale1 = dtSale.Select($@"  POSGROUP='{bch}' AND ITEMID = '{item}' AND INVENTDIMID = '{dim}' AND DATE = '{DD}' ");
                            if (rowSale1.Length > 0) qtySale = (double.Parse(rowSale1[0]["QTY"].ToString()) / factor);
                        }

                        if (dtMRT.Rows.Count > 0)
                        {
                            DataRow[] rowMRT1 = dtMRT.Select($@"  BRANCH_ID='{bch}' AND ITEMID = '{item}' AND INVENTDIMID = '{dim}' AND DATE = '{DD}' ");
                            if (rowMRT1.Length > 0) qtyMRT = (double.Parse(rowMRT1[0]["QTY"].ToString()) / factor);
                        }

                        RadGridView_ShowHD.Rows[iRow].Cells[barcode + "_MRT" + iDD.ToString()].Value = qtyMRT.ToString("N2");
                        RadGridView_ShowHD.Rows[iRow].Cells[barcode + "_SALE" + iDD.ToString()].Value = qtySale.ToString("N2");
                    }
                     
                    RadGridView_ShowHD.Rows[iRow].Cells[barcode + "_OR"].Value = qrtOR.ToString("N2");
                    RadGridView_ShowHD.Rows[iRow].Cells[barcode + "_KK"].Value = "0.00";


                }


                iRow++;
            }

            this.Cursor = Cursors.Default;
        }
        #region "ROWS DGV"
        private void RadGridView_ShowHD_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }


        #endregion

        //Clear
        void ClearTxt()
        {
            if (_pCase == "0")
            {
                if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();
                if (RadGridView_ShowHD.Columns.Count > 0) RadGridView_ShowHD.Columns.Clear();

                radBrowseEditor_choose.Enabled = true;
                RadButton_Save.Enabled = false;
                radBrowseEditor_choose.Value = "";
            }
        }

        //Clear
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Choose File
        private void RadBrowseEditor_choose_ValueChanged(object sender, EventArgs e)
        {
            //Check ค่าว่างใน File ที่เลือก
            if ((radBrowseEditor_choose.Value is null) || (radBrowseEditor_choose.Value == "")) return;
            //Clear Datatable
            if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();

            this.Cursor = Cursors.WaitCursor;
            //Check File Excel
            if ((radBrowseEditor_choose.Value.Contains(".xls")) == false)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("File ที่ระบุไม่ใช่ File Excel เช็คใหม่อีกครั้ง. " + Environment.NewLine +
                    "[" + radBrowseEditor_choose.Value + @"]");
                ClearTxt();
                this.Cursor = Cursors.Default;
                return;
            }

            DataTable DtSet = new DataTable();
            radBrowseEditor_choose.Enabled = false;

            try
            {
                System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
               ("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + radBrowseEditor_choose.Value +
               @"';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

                MyConnection.Open();
                DataTable dtExcelSheet = MyConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);

                if (dtExcelSheet == null)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลใน File Excel ที่ระบุ");
                    return;
                }

                System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter($@"select * from [{dtExcelSheet.Rows[0]["TABLE_NAME"]}] ", MyConnection);
                MyCommand.TableMappings.Add("Table", "Net-informations.com");
                MyCommand.Fill(DtSet);

                if (DtSet.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูลสำหรับ File Excel ที่เลือก."); return;
                }

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 90)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));

                for (int i = 2; i < DtSet.Columns.Count; i++)
                {
                    string HeadTextDesc;
                    string HeadColumn = DtSet.Columns[i].ColumnName.ToString();
                    if (HeadColumn.Contains(":"))
                    {
                        string[] SubColumn = DtSet.Columns[i].ColumnName.ToString().Split(':');
                        HeadColumn = SubColumn[0].ToString(); //DtSet.Columns[i].ColumnName.ToString();
                    }

                    DataRow[] dr = DtItem.Select($"ITEMBARCODE = '{HeadColumn}' ");
                    if (dr.Length > 0)
                    {
                        HeadTextDesc = $@"{HeadColumn}{Environment.NewLine}{dr[0]["SPC_ITEMNAME"]}";
                    }
                    else
                    {
                        HeadColumn += "_1";
                        HeadTextDesc = $@"{HeadColumn}{Environment.NewLine}ไม่มีสินค้าในกลุ่มที่ระบุ";
                    }
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(HeadColumn, HeadTextDesc, 150)));

                }

                int iR = 0;
                foreach (DataRow item in DtSet.Rows)
                {
                    DataRow[] drBch = DtBranch.Select($"BRANCH_ID = '{DtSet.Rows[iR][0]}' ");
                    if (drBch.Length > 0)
                    {
                        RadGridView_ShowHD.Rows.Add(DtSet.Rows[iR][0].ToString(), drBch[0]["BRANCH_NAME"]);
                        for (int i = 2; i < DtSet.Columns.Count; i++)
                        {
                            RadGridView_ShowHD.Rows[iR].Cells[i].Value = DtSet.Rows[iR][i].ToString();
                            if (RadGridView_ShowHD.Columns[i].HeaderText.Contains("_1"))
                            {
                                RadGridView_ShowHD.Rows[iR].Cells[i].Style.BackColor = Color.Red;
                            }
                        }
                        iR++;
                    }
                }

                MyConnection.Close();

                if (RadGridView_ShowHD.Rows.Count > 0)
                {
                    RadButton_Save.Enabled = true;
                    RadButton_Save.Focus();

                }
                else
                {
                    RadButton_Save.Enabled = false;
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีข้อมูลออเดอร์ที่จะนำเข้าได้{Environment.NewLine}เช็ค File Excel ใหม่อีกครั้ง");
                }

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถดำเนินการนำเข้าข้อมูลจาก Excel ได้ ลองใหม่อีกครั้ง." + Environment.NewLine +
                    "[ " + ex.Message + @" ]");
                ClearTxt();
                this.Cursor = Cursors.Default;
                return;
            }
        }

        //ยกเลิก
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (_pCase == "0")
            {
                ClearTxt();
            }
            else
            {
                this.Close();
            }

        }
        //Cell Color
        private void RadGridView_ShowHD_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (_pCase == "0")
            {
                if (e.CellElement.ColumnInfo.Name.Contains("_1"))
                {
                    e.CellElement.DrawFill = true;
                    e.CellElement.BackColor = ConfigClass.SetColor_Red();
                    e.CellElement.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local);
                    e.CellElement.ResetValue(LightVisualElement.ForeColorProperty, ValueResetFlags.Local);
                    e.CellElement.ResetValue(LightVisualElement.NumberOfColorsProperty, ValueResetFlags.Local);
                    e.CellElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
                }
            }
            else
            {
                if (e.CellElement.ColumnInfo is GridViewDataColumn column)
                {
                    if ((column.FieldName).Contains("_MRT"))
                    {
                        e.CellElement.DrawFill = true;
                        e.CellElement.NumberOfColors = 1;
                        e.CellElement.BackColor = Color.FromArgb(183, 222, 232);
                    }
                    else if ((column.FieldName).Contains("_SALE"))
                    {
                        e.CellElement.DrawFill = true;
                        e.CellElement.NumberOfColors = 1;
                        e.CellElement.BackColor = Color.FromArgb(230, 184, 183);
                    }
                    else if ((column.FieldName).Contains("_KK"))
                    {
                        e.CellElement.DrawFill = true;
                        e.CellElement.NumberOfColors = 1;
                        e.CellElement.BackColor = ConfigClass.SetColor_PurplePastel();
                    }
                    else
                    {
                        e.CellElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local);
                        e.CellElement.ResetValue(LightVisualElement.ForeColorProperty, ValueResetFlags.Local);
                        e.CellElement.ResetValue(LightVisualElement.NumberOfColorsProperty, ValueResetFlags.Local);
                        e.CellElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
                    }
                }

            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _pCase);
        }

        void ImportData()
        {
            if (RadGridView_ShowHD.Rows.Count < 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถบันทึกรายการได้ เนื่องจากไม่มีรายการสินค้า");
                return;
            }

            this.Cursor = Cursors.WaitCursor;
            ArrayList sqlUp = new ArrayList();

            string SqlgroupItemCondition = "AND StaGroupItem = '0'";
            if (!(_ItemGroup.Equals("0"))) SqlgroupItemCondition = string.Format(@"AND StaGroupItem = '1' AND GroupItem = '{0}'", _ItemGroup);

            for (int i = 2; i < RadGridView_ShowHD.Columns.Count; i++)
            {
                string barcode = RadGridView_ShowHD.Columns[i].Name.ToString();

                if (barcode.Contains("_1")) continue;

                foreach (GridViewRowInfo item in RadGridView_ShowHD.Rows)
                {
                    string bch = item.Cells[0].Value.ToString();

                    sqlUp.Add(($@"
                    UPDATE  Android_OrderTODT   
                    SET     Qty = '{item.Cells[i].Value}' ,QtyExcel = '{item.Cells[i].Value}'
                    FROM    Android_OrderTODT  WITH (NOLOCK)
                            INNER JOIN Android_OrderTOHD WITH (NOLOCK) ON Android_OrderTOHD.Docno = Android_OrderTODT.DocNo 
                    WHERE   Branch = '{bch}'  
                            AND Barcode = '{barcode}'  
                            AND Date = '{_DateSend}' 
                            AND Android_OrderTOHD.StaPrcDoc != '3' {SqlgroupItemCondition} 
                            AND Android_OrderTOHD.Remark = '' "));
                }
            }
            string resualt = ConnectionClass.ExecuteSQL_ArrayMain(sqlUp);
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "")
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }

            this.Cursor = Cursors.Default;
        }

        //Save To DB
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (_pCase == "0")
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการบันทึกข้อมูลที่เลือก ?") == DialogResult.No) return;

                ImportData();
            }
            else
            {
                if (RadGridView_ShowHD.Rows.Count == 0) { return; }
                string T = DatagridClass.ExportExcelGridView("OrderToAX", RadGridView_ShowHD, "1", _DateSend);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }

    }
}
