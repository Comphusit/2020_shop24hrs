﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using System.Drawing;
using System.Collections;
using PC_Shop24Hrs.FormShare.ShowData;
using Telerik.WinControls.UI;
using Telerik.WinControls;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_MainDocBranch : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dtDataHD = new DataTable();
        readonly DataTable dtDataDT = new DataTable();

        Data_ITEMBARCODE Data_ITEMBARCODE;

        readonly string _BranchID, _BranchName, _DateSend, _Round, _DptID, _GroupID, _sta;

        string DocnoMRT;

        //Main
        public OrderToAx_MainDocBranch(string bchID, string bchName, string dateSend, string Round, string DptID, string GroupID, string Sta)
        {
            InitializeComponent();
            _BranchID = bchID;
            _BranchName = bchName;
            _DateSend = dateSend;
            _Round = Round;
            _DptID = DptID;
            _GroupID = GroupID;
            _sta = Sta;

        }
        //SetForm
        void SetEnable(string StatusDocno)
        {
            RadLabel_BranchID.Text = _BranchID;
            radLabel_BranchName.Text = _BranchName;
            radLabel_dateSend.Text = _DateSend;

            RadLabel_Qty.Text = "";

            RadLabel_Status.Text = "บันทึก";
            RadLabel_Status.ForeColor = Color.Blue;
            this.Name = "รายละเอียดจัดสินค้า";

            //ในกรณียังไม่มีบิล
            if (StatusDocno.Equals("NO") || StatusDocno.Equals("MNOR"))
            {

                DataTable DtDocno = MRTClass.GetDocumentMNOR(_DptID, _DateSend, _GroupID, _BranchID);
                if (DtDocno.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่มีสินค้าในเงื่อนไขที่ระบุ{Environment.NewLine}เช็คเงื่อนไขใหม่อีกครั้ง.");
                    this.DialogResult = DialogResult.No;
                    return;
                }
                DocnoMRT = Class.ConfigClass.GetMaxINVOICEID("MRT", _DptID.Substring(1, 3), DateTime.Parse(_DateSend).ToString("yyMMdd"), "8");
                string StaGroupItem = "1"; if (_GroupID == "") StaGroupItem = "0";

                ArrayList sql = new ArrayList() {
                $@"
                    INSERT INTO Android_OrderTOHD(DocNo, Date, Time, UserCode, Branch,BranchName,
                                    Depart, StaDoc, StaPrcDoc, StaApvDoc, StaAx, StaRound2, Box, Remark,
                                    DateUp, TimeUp, WhoUp,WhoUpName,
                                    DateIn, TimeIn, WhoIn,WhoInName,
                                    DateApv, TimeApv, WhoApv,DateAx, TimeAx, WhoAx, 
                                    Round, StaGroupItem, GroupItem, DeptGroup)
                    VALUES  ('{DocnoMRT}','{_DateSend}',convert(varchar, getdate(), 24),'{SystemClass.SystemUserID_M}','{_BranchID}', '{_BranchName}',
                            '{_DptID}', '1', '0', '0', '0', '', 0, '',
                                    convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                    convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                    '', '', '', '', '', '', 
                    '{_Round}', '{StaGroupItem}', '{_GroupID}', '{_DptID}')
                "
                };

                string priceLevel = DtDocno.Rows[0]["PRICELAVEL"].ToString();
                for (int i = 0; i < DtDocno.Rows.Count - 1; i++)
                {
                    sql.Add($@"
                    INSERT INTO    Android_OrderTODT(
                                    DocNo, SeqNo, ItemID, ItemDim,
                                    Barcode, Name, 
                                    QTY, UnitID, price,
                                    DateIn, TimeIn, WhoIn,
                                    DateUp, TimeUp, WhoUp, StatusItem, Round)
                    VALUES          (
                                    '{DocnoMRT}', '{i + 1}', '{DtDocno.Rows[i]["ITEMID"]}','{DtDocno.Rows[i]["INVENTDIMID"]}',
                                    '{DtDocno.Rows[i]["ITEMBARCODE"]}', '{DtDocno.Rows[i]["SPC_ITEMNAME"]}',
                                    '{DtDocno.Rows[i]["QTYORDER"]}', '{DtDocno.Rows[i]["UNITID"]}', '{DtDocno.Rows[i][priceLevel]}',
                                    convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}',
                                    convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}', '0', '{_Round}')"
                );
                }

                string staSave = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                if (staSave != "")
                { MsgBoxClass.MsgBoxShow_SaveStatus(staSave); return; }
                else
                { RadLabel_Status.Text = "บันทึก"; }

            }
            else //ในกรณีที่มีบิลอยู่แล้ว
            {
                DataTable dtDocno = MRTMainDocBranch.CheckDocumentMRTO(_DptID, DateTime.Parse(_DateSend).ToString("yyyy-MM-dd"), _Round, _BranchID, _GroupID);
                DocnoMRT = dtDocno.Rows[0]["DocNo"].ToString();
                if (dtDocno.Rows[0]["StaApvDoc"].ToString() == "1")
                {
                    RadTextBox_Barcode.Enabled = false;
                    RadLabel_Status.Text = "อนุมัติแล้ว";
                    RadLabel_Status.ForeColor = Color.Red;
                    RadGridView_Show.ReadOnly = true;
                }
            }

            radLabel_Docno.Text = DocnoMRT;
            RadGridView_Show.DataSource = MRT_Class.GetMRTDocnoDT(DocnoMRT);
        }


        //Load
        private void OrderToAx_MainDocBranch_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.ReadOnly = false;
            radStatusStrip1.SizingGrip = false;

            radButtonElement_CopYMNOR.ToolTipText = "คัดลอกจำนวนสั่ง MNOR"; radButtonElement_CopYMNOR.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_excel.ToolTipText = "excel"; RadButtonElement_excel.ShowBorder = true;

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SeqNo", "ลำดับ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ItemID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ItemDim", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Name", "ชื่อสินค้า", 220));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Qty", "จำนวนต้องส่ง", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("qtyround", "จำนวนรอส่ง", 120));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Price", "ราคา:หน่วย", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NET", "ยอดรวม", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("Round", "รอบ"));

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "Qty > 0  ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            this.RadGridView_Show.Columns["Qty"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_Show.Columns["NET"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "qtyround > 0  ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            this.RadGridView_Show.Columns["qtyround"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["NET"].ConditionalFormattingObjectList.Add(obj2);

            RadGridView_Show.MasterTemplate.EnableFiltering = false;
            RadGridView_Show.MasterTemplate.AllowAddNewRow = false;

            SetEnable(_sta);

            this.Cursor = Cursors.Default;
        }
        //copy จำนวน MNOR มาใส่
        private void RadButtonElement_CopYMNOR_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการแก้ไขจำนวนรอบ 1 ทั้งหมด เท่ากับจำนวนสั่ง MNOR ?") == DialogResult.No) return;

            string sqlUp = $@"
            UPDATE	ANDROID_ORDERTODT	SET	Qty = TMPOR.totalOR
            FROM	ANDROID_ORDERTODT WITH (NOLOCK) 
		            INNER JOIN	
		            (
			            SELECT  Shop_MNOR_DT.ITEMBARCODE,BRANCH_ID , 
					            isnull(sum(Shop_MNOR_DT.QTYORDER),0) as totalOR
			            FROM    Shop_MNOR_HD WITH (NOLOCK)
					            INNER JOIN  Shop_MNOR_DT ON Shop_MNOR_HD.DOCNO = Shop_MNOR_DT.DOCNO 
			            WHERE   STA_DOC !=3 
					            AND STA_PRCDOC = 1 
					            AND STA_APVDOC = 1 
					            AND DATE_RECIVE = '{_DateSend}' 
					            AND DPTID = '{_DptID}' AND BRANCH_ID = '{_BranchID}'
			            GROUP BY Shop_MNOR_DT.ITEMBARCODE,BRANCH_ID)TMPOR ON ANDROID_ORDERTODT.Barcode = TMPOR.ITEMBARCODE 
            WHERE	ANDROID_ORDERTODT.DocNo = '{radLabel_Docno.Text}'
            ";
            string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "") RadGridView_Show.DataSource = MRT_Class.GetMRTDocnoDT(DocnoMRT);

        }

        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        //Formart
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //เชค คอลัมที่ต้องการให้แก้ไข
        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "Qty":
                    break;
                case "qtyround":
                    break;
                //case "QtyOrderCust":
                //    break;
                default:
                    e.Cancel = true;
                    break;
            }
        }
        //แก้ไขข้อมูล
        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {

            switch (e.Column.Name)
            {
                //แก้ไขจำนวนสั่งสาขา
                case "Qty":
                    string SqlUpQty = string.Format(@"
                    UPDATE  Android_OrderTODT   
                    SET     Qty = '{0}',
                            DATEUP = convert(varchar, getdate(), 23),
                            TIMEUP = convert(varchar, getdate(), 24),
                            WHOUP = '{1}'
                    WHERE   Docno = '{2}'
                            AND Barcode = '{3}'",
                    RadGridView_Show.CurrentRow.Cells["Qty"].Value.ToString(),
                    SystemClass.SystemUserID_M, DocnoMRT,
                    RadGridView_Show.CurrentRow.Cells["Barcode"].Value.ToString());

                    string SqlUpTranctionQty = ConnectionClass.ExecuteSQL_Main(SqlUpQty);
                    if (SqlUpTranctionQty != "")
                    { MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranctionQty); return; }
                    else
                    {
                        RadGridView_Show.CurrentRow.Cells["NET"].Value = (Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Qty"].Value) + Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["qtyround"].Value)) * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Price"].Value);
                    }

                    break;

                //แก้ไขจำนวนสั่งรอบ2
                case "qtyround":
                    string SqlUpQtyR2 = string.Format(@"
                    UPDATE  Android_OrderTODT
                    SET     qtyround = '{0}',
                            DATEUP = convert(varchar, getdate(), 23),
                            TIMEUP = convert(varchar, getdate(), 24),
                            WHOUP = '{1}'
                    WHERE   Docno = '{2}'
                            AND Barcode = '{3}'",
                        RadGridView_Show.CurrentRow.Cells["qtyround"].Value.ToString(),
                        SystemClass.SystemUserID_M, DocnoMRT,
                        RadGridView_Show.CurrentRow.Cells["Barcode"].Value.ToString());

                    string SqlUpTranctionQtyR2 = ConnectionClass.ExecuteSQL_Main(SqlUpQtyR2);
                    if (SqlUpTranctionQtyR2 != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranctionQtyR2);
                    }
                    else
                    {
                        RadGridView_Show.CurrentRow.Cells["NET"].Value = (Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Qty"].Value) + Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["qtyround"].Value)) * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Price"].Value);
                    }
                    break;
                //case "QtyOrderCust":
                //    string SqlUpQtyCst = string.Format(@"
                //    UPDATE  Android_OrderTODT
                //    SET     QtyOrderCust = '{0}',
                //            DATEUP = convert(varchar, getdate(), 23),
                //            TIMEUP = convert(varchar, getdate(), 24),
                //            WHOUP = '{1}'
                //    WHERE   Docno = '{2}'
                //            AND Barcode = '{3}'",
                //       RadGridView_Show.CurrentRow.Cells["QtyOrderCust"].Value.ToString(),
                //       SystemClass.SystemUserID_M, DocnoMRT,
                //       RadGridView_Show.CurrentRow.Cells["Barcode"].Value.ToString());

                //    string SqlUpTranctionQtyCst = ConnectionClass.ExecuteSQL_Main(SqlUpQtyCst);
                //    if (SqlUpTranctionQtyCst != "")
                //    {
                //        MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranctionQtyCst);
                //    }
                //    else
                //    {
                //        RadGridView_Show.CurrentRow.Cells["NET"].Value = (Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Qty"].Value) + Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["qtyround"].Value) + Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["QtyOrderCust"].Value)) * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Price"].Value);
                //    }
                //    break;
                default:
                    break;
            }
        }

        //ค้นหาราคาสินค้า ตามสาขา
        double CheckPrice(string pBchID)
        {
            double priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP14;
            //GetBranchSettingBranch
            DataTable dtBch = BranchClass.GetBranchSettingBranch(pBchID);
            if (dtBch.Rows[0]["BRANCH_PRICE"].ToString() == "SPC_PriceGroup13")
            {
                priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP13;
            }
            else if (dtBch.Rows[0]["BRANCH_PRICE"].ToString() == "SPC_PriceGroup15")
            {
                priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP15;
            }
            else if (dtBch.Rows[0]["BRANCH_PRICE"].ToString() == "SPC_PriceGroup17")
            {
                priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP17;
            }
            else if (dtBch.Rows[0]["BRANCH_PRICE"].ToString() == "SPC_PriceGroup18")
            {
                priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP18;
            }
            else if (dtBch.Rows[0]["BRANCH_PRICE"].ToString() == "SPC_PriceGroup19")
            {
                priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP19;
            }
            return priceMN;
        }

        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
            {
                //ค้นหาสินค้าตามชื่อสินค้า
                string SqlWhere = $@" AND SPC_ITEMBUYERGROUPID = '{_DptID}' ";
                if (RadTextBox_Barcode.Text != "")
                { SqlWhere += " AND SPC_ITEMNAME LIKE '%" + RadTextBox_Barcode.Text.Replace(" ", "%") + @"%' "; }
                //  else { SqlWhere = ""; }

                ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(SqlWhere);
                if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
                {
                    Data_ITEMBARCODE = ShowDataDGV_Itembarcode.items;
                    RadTextBox_Barcode.Text = Data_ITEMBARCODE.Itembarcode_ITEMBARCODE;
                }
                else
                {
                    RadTextBox_Barcode.Text = ""; RadTextBox_Barcode.Focus(); return;
                }
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                if (double.TryParse(RadTextBox_Barcode.Text, out _))
                {
                    RadLabel_Qty.Text = RadTextBox_Barcode.Text;
                    RadTextBox_Barcode.Text = string.Empty;
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่จำนวนสินค้าให้ถูกต้อง");
                    RadTextBox_Barcode.SelectAll();
                    RadTextBox_Barcode.Focus();
                    return;
                }
            }
            else if (e.KeyCode == Keys.Enter && RadTextBox_Barcode.Text.Length > 0)
            {
                Data_ITEMBARCODE = new Data_ITEMBARCODE(RadTextBox_Barcode.Text);
                if (Data_ITEMBARCODE.Itembarcode_ITEMBARCODE == null)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีรายการสินค้าที่ต้องการค้นหา กรุณาลองใหม่อีกครั้ง");
                    RadTextBox_Barcode.SelectAll();
                    RadTextBox_Barcode.Focus();
                    return;
                }

                if (Data_ITEMBARCODE.Itembarcode_SPC_ITEMACTIVE == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่สามารถจัดส่งสินค้าได้ เนื่องจากเป็นสินค้าสถานะ หยุด ใช้งาน");
                    RadTextBox_Barcode.SelectAll();
                    return;
                }

                //Check สิดแผนกที่สาขา คีย์ได้
                if (SystemClass.SystemComProgrammer != "1")
                {
                    if (SystemClass.SystemDptID != "D147")
                    {
                        if (Data_ITEMBARCODE.Itembarcode_DIMENSION != SystemClass.SystemDptID)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถจัดส่งสินค้าได้{Environment.NewLine}เนื่องจากเป็นสินค้าต่างแผนก");
                            RadTextBox_Barcode.SelectAll();
                            return;
                        }
                    }
                    else
                    {
                        if ((Data_ITEMBARCODE.Itembarcode_DIMENSION != "D032") || (Data_ITEMBARCODE.Itembarcode_DIMENSION != "D144"))
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถจัดส่งสินค้าได้{Environment.NewLine}เนื่องจากไม่มีสิทธ์สำหรับแผนก {Data_ITEMBARCODE.Itembarcode_DESCRIPTION} ");
                            RadTextBox_Barcode.SelectAll();
                            return;
                        }
                    }
                }
                //Check แผนกสินค้าและรหัสที่ต้องการเปิด
                if (_DptID != Data_ITEMBARCODE.Itembarcode_DIMENSION)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถจัดส่งสินค้าได้{Environment.NewLine}เนื่องจากรหัสแผนกที่ระบุและรหัสบาร์โค้ดไม่ตรงกัน{Environment.NewLine}เช็คข้อมูลสินค้าใหม่อีกครั้ง.");
                    RadTextBox_Barcode.SelectAll();
                    return;
                }
                ArrayList sql = new ArrayList();
                string staSave = "1";
                //  string BranchID = RadLabel_BranchID.Text, BranchName = radLabel_BranchName.Text;
                if (radLabel_Docno.Text.Equals("-"))
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($"ต้องการบันทึกเอกสารเพิ่มของสาขา {_BranchID} {_BranchName} หรือไม่ ") == DialogResult.No) return;

                    // DocnoMRTO = Class.ConfigClass.GetMaxINVOICEID("MRT", DateTime.Parse(DateDocno).ToString("yyMMdd"), Dept.Substring(1, 3), "8");
                    DocnoMRT = Class.ConfigClass.GetMaxINVOICEID("MRT", _DptID.Substring(1, 3), DateTime.Parse(_DateSend).ToString("yyMMdd"), "8");
                    string StaGroupItem = "1"; if (_GroupID == "") StaGroupItem = "0";
                    sql.Add(
                    $@"
                        INSERT INTO Android_OrderTOHD(DocNo, Date, Time, UserCode, Branch,BranchName,
                                        Depart, StaDoc, StaPrcDoc, StaApvDoc, StaAx, StaRound2, Box, Remark,
                                        DateUp, TimeUp, WhoUp,WhoUpName,
                                        DateIn, TimeIn, WhoIn,WhoInName,
                                        DateApv, TimeApv, WhoApv,DateAx, TimeAx, WhoAx, 
                                        Round, StaGroupItem, GroupItem, DeptGroup)
                        VALUES  ('{DocnoMRT}','{_DateSend}',convert(varchar, getdate(), 24),'{SystemClass.SystemUserID_M}','{_BranchID}', '{_BranchName}',
                                '{_DptID}', '1', '0', '0', '0', '', 0, '',
                                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                        '', '', '', '', '', '', 
                        '{_Round}', '{StaGroupItem}', '{_GroupID}', '{_DptID}')
                    ");
                    staSave = "0";
                }

                //                RadGridView_Show.Select();
                //Boolean CheckMRT_Barcode = (MRTMainDocBranch.CheckMRTOBarcode(DocnoMRT, Data_ITEMBARCODE.Itembarcode_ITEMBARCODE));
                int iRow = MRTMainDocBranch.CheckMRTOBarcode(DocnoMRT, Data_ITEMBARCODE.Itembarcode_ITEMBARCODE);
                double Qty;
                if (RadLabel_Qty.Text.Equals("")) RadLabel_Qty.Text = "1";
                Qty = Convert.ToDouble(RadLabel_Qty.Text);

                double priceMN = CheckPrice(_BranchID);

                if (iRow == 0)
                {
                    sql.Add(string.Format(@"
                    UPDATE  ANDROID_ORDERTODT 
                    SET     Qty = qty + {0}
                    WHERE   docno = '{1}'
                            and Barcode = '{2}'",
                        Qty, DocnoMRT, Data_ITEMBARCODE.Itembarcode_ITEMBARCODE));
                    staSave = "0";
                }
                else
                {
                    sql.Add($@"
                    insert into Android_OrderTODT(
                        DocNo, SeqNo, ItemID, ItemDim,
                        Barcode, Name, 
                        QTY, UnitID, price,
                        DateIn, TimeIn, WhoIn,
                        StatusItem, Round, qtyround,QtyOrderCust)
                    values
                        ('{DocnoMRT}', '{iRow}', '{Data_ITEMBARCODE.Itembarcode_ITEMID}','{Data_ITEMBARCODE.Itembarcode_INVENTDIMID}',
                        '{Data_ITEMBARCODE.Itembarcode_ITEMBARCODE}', '{Data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME}',
                        '{Qty}', '{Data_ITEMBARCODE.Itembarcode_UNITID}', '{priceMN}',
                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}',
                        '0', '{_Round}','0','0')");
                }


                string SqlInsDTTranction = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                if (SqlInsDTTranction != "") { MsgBoxClass.MsgBoxShow_SaveStatus(SqlInsDTTranction); return; }

                if (staSave == "0")
                {
                    RadGridView_Show.DataSource = MRT_Class.GetMRTDocnoDT(DocnoMRT);
                    radLabel_Docno.Text = DocnoMRT;
                    RadLabel_Status.Text = "บันทึก";

                }
                else
                {
                    RadGridView_Show.Rows.Add(RadGridView_Show.Rows.Count + 1,
                    Data_ITEMBARCODE.Itembarcode_ITEMID,
                      Data_ITEMBARCODE.Itembarcode_INVENTDIMID,
                      Data_ITEMBARCODE.Itembarcode_ITEMBARCODE,
                      Data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME,
                      String.Format("{0:0.##}", Qty),
                      "0",
                      Data_ITEMBARCODE.Itembarcode_UNITID,
                      priceMN.ToString("#,#0.00"),
                      String.Format("{0:0.##}", (Qty * priceMN)),
                      _Round);
                }


                RadLabel_Qty.Text = "";
                RadTextBox_Barcode.SelectAll();
                RadTextBox_Barcode.Focus();
                // Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP13)
                this.Cursor = Cursors.Default;
            }
            this.Cursor = Cursors.Default;
        }
    }
}
