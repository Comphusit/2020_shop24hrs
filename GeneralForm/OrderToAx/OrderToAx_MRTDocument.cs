﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.FormShare.ShowData;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_MRTDocument : Telerik.WinControls.UI.RadForm
    {
        string Round, MRTDOCNO;// ,MNPCDOCNO, Docno, MNPCDate;Branch, BranchName, 
        Double Qty;

        Data_ITEMBARCODE Data_ITEMBARCODE;

        public OrderToAx_MRTDocument()
        {
            InitializeComponent();
        }

        //Load Main
        private void OrderToAx_MRTDocument_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.ReadOnly = false;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SeqNo", "ลำดับ"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ItemID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ItemDim", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Name", "ชื่อสินค้า", 220));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Qty", "จำนวนต้องส่ง", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("qtyround", "จำนวนรอส่ง", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Price", "ราคา:หน่วย", 80));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NET", "ยอดรวม", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("Round", "รอบ", 80));

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "Qty > 0  ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            this.RadGridView_Show.Columns["Qty"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_Show.Columns["NET"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "qtyround > 0  ", false)
            { CellBackColor = ConfigClass.SetColor_SkyPastel() };
            this.RadGridView_Show.Columns["qtyround"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["NET"].ConditionalFormattingObjectList.Add(obj2);


            RadGridView_Show.MasterTemplate.EnableFiltering = false;

            radStatusStrip1.SizingGrip = false;
            RadButtonElement_Add.ShowBorder = true; RadButtonElement_Add.ToolTipText = "เปิดเอกสารใหม่";
            RadButtonElement_Find.ShowBorder = true; RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            RadButtonElement_Copy.ShowBorder = true; RadButtonElement_Copy.ToolTipText = "คัดลอก";
            RadButtonElement_Excel.ShowBorder = true; RadButtonElement_Excel.ToolTipText = "Export Excel";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือ";

            RadButton_Round1.ButtonElement.ShowBorder = true;
            RadButton_Round2.ButtonElement.ShowBorder = true;
            RadButton_Apv.ButtonElement.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;

            Set_Dept();
            ClearData("");
            Set_Branch();
        }

        #region "ClEAR AND FUNCION"

        //Clear
        void ClearData(string status)
        {
            switch (status)
            {
                case "SAVE":
                    RadLabel_StatusBill.Text = "สถานะบิล : บันทึก";
                    RadLabel_StatusBill.ForeColor = Color.Black;
                    RadLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();

                    RadTextBox_Barcode.Enabled = true;

                    RadTextBox_Box.Enabled = true;
                    RadButton_Round1.Enabled = true;
                    RadButton_Round2.Enabled = true;

                    RadButton_Cancel.Enabled = true;
                    RadButton_Apv.Enabled = true;
                    RadTextBox_Barcode.SelectAll();
                    break;

                case "APV":
                    RadLabel_StatusBill.Text = "สถานะบิล : รอส่งสินค้า";
                    RadLabel_StatusBill.ForeColor = Color.Black;
                    RadLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();

                    RadDropDownList_Branch.Enabled = false;

                    RadTextBox_Barcode.Enabled = false;

                    RadTextBox_Box.Enabled = false;
                    RadButton_Round1.Enabled = false;
                    RadButton_Round2.Enabled = false;

                    RadButton_Cancel.Enabled = false;
                    RadButton_Apv.Enabled = false;
                    break;

                case "AX":
                    RadLabel_StatusBill.Text = "สถานะบิล : ส่งสินค้า";
                    RadLabel_StatusBill.ForeColor = Color.Black;
                    RadLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel();

                    RadDropDownList_Branch.Enabled = false;

                    RadTextBox_Barcode.Enabled = false;

                    RadTextBox_Box.Enabled = false;
                    RadButton_Round1.Enabled = false;
                    RadButton_Round2.Enabled = false;

                    RadButton_Cancel.Enabled = false;
                    RadButton_Apv.Enabled = false;
                    break;

                case "CENCEL":
                    RadLabel_StatusBill.Text = "สถานะบิล : ยกเลิก";
                    RadLabel_StatusBill.ForeColor = Color.Black;
                    RadLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel();

                    RadDropDownList_Branch.Enabled = false;

                    RadTextBox_Barcode.Enabled = false;

                    RadTextBox_Box.Enabled = false;
                    RadButton_Round1.Enabled = false;
                    RadButton_Round2.Enabled = false;

                    RadButton_Cancel.Enabled = false;
                    RadButton_Apv.Enabled = false;
                    break;

                default:
                    //MNPCDOCNO = ""; Docno = ""; 
                    // Branch = ""; BranchName = "";
                    Round = "2";
                    //MNPCDate = "";
                    //BoxCount = 0; BoxSeq = 0; Seq = 0;
                    //TypeProduct = ""; Box = "";
                    //PriceItemNet = 0;
                    Qty = 1;
                    if (RadGridView_Show.Rows.Count > 0)
                    {
                        RadGridView_Show.DataSource = null;
                        RadGridView_Show.Rows.Clear();
                    }
                    RadLabel_Docno.Text = "MRT0000000000000";//"MRTO" + DateTime.Now.ToString("yyMMdd") + @"000000"; 
                                                             //radLabel_PriceNet.Text = string.Empty;

                    RadLabel_StatusBill.Text = string.Empty;
                    RadLabel_StatusBill.ForeColor = Color.Black;
                    RadLabel_StatusBill.BackColor = Color.Transparent;

                    RadLabel_Qty.Text = string.Empty;
                    RadLabel_Round.Text = string.Empty;
                    RadLabel_Round.BackColor = Color.Transparent;

                    RadDropDownList_Branch.Enabled = true;
                    RadDropDownList_Branch.Refresh();

                    RadDropDownList_Dept.Enabled = true;
                    RadDropDownList_Dept.Refresh();

                    RadTextBox_Barcode.Enabled = true;

                    RadTextBox_Box.Enabled = false;
                    RadButton_Round1.Enabled = false;
                    RadButton_Round2.Enabled = false;

                    RadButton_Cancel.Enabled = false;
                    RadButton_Apv.Enabled = false;

                    RadLabel_EmplUp.Text = string.Empty;
                    RadLabel_DateUp.Text = string.Empty;
                    break;

            }

            RadTextBox_Barcode.Focus();
        }
        //set สาขา
        void Set_Branch()
        {
            DataTable DtBranch;
            DtBranch = BranchClass.GetBranchAll(" '1','4' ", " '1' ");
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.DataSource = DtBranch;
        }
        //ค้นหาเอกสาร
        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            using (OrderToAx_FindData OrderToAx_FindData = new OrderToAx_FindData())
            {
                DialogResult dr = OrderToAx_FindData.ShowDialog();
                if (dr == DialogResult.Yes)
                {
                    SetGridAndData(MRT_Class.GetMRTDocnoHD(OrderToAx_FindData.pDocno));
                }
            }
        }

        #endregion

        //set rows in grid
        #region "SET GRIDVIEW"

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        //Set Data IN Grid
        void SetGridAndData(DataTable DtMRT)
        {
            if (DtMRT.Rows.Count > 0)
            {
                ClearData(DtMRT.Rows[0]["StatusDoc"].ToString());

                RadLabel_Docno.Text = DtMRT.Rows[0]["DocNo"].ToString();
                Round = DtMRT.Rows[0]["Round"].ToString();
                RadLabel_Round.Text = "รอบ : " + Round;
                RadLabel_Round.BackColor = ConfigClass.SetColor_Red();
                RadDropDownList_Branch.SelectedValue = DtMRT.Rows[0]["Branch"].ToString();
                RadDropDownList_Branch.Enabled = false;

                RadLabel_EmplUp.Text = "ผู้สร้าง : " + DtMRT.Rows[0]["WhoInName"].ToString();
                RadLabel_DateUp.Text = "สร้าง : " + DtMRT.Rows[0]["DocDate"].ToString() + " " + DtMRT.Rows[0]["Time"].ToString();

                RadGridView_Show.DataSource = null;
                RadGridView_Show.Rows.Clear();
                RadGridView_Show.Refresh();

                RadDropDownList_Dept.SelectedValue = DtMRT.Rows[0]["DeptGroup"].ToString();
                RadDropDownList_Dept.Enabled = false;

                RadGridView_Show.DataSource = MRT_Class.GetMRTDocnoDT(RadLabel_Docno.Text);

                if ((RadGridView_Show.Rows.Count == 0) && (DtMRT.Rows[0]["StatusDoc"].ToString() == "APV")) RadButton_Cancel.Enabled = true;

            }
            else
            {
                ClearData("");
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่มีเอกสารที่ต้องการค้นหา");
                return;

            }
        }
        //Set แผนก
        void Set_Dept()
        {
            RadDropDownList_Dept.DataSource = MRTClass.GetDept(SystemClass.SystemDptID);
            RadDropDownList_Dept.Enabled = true;
            RadDropDownList_Dept.ValueMember = "NUM";
            RadDropDownList_Dept.DisplayMember = "DeptName";
        }

        //Key Data
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F4)
            {
                string SqlWhere = $@" AND SPC_ITEMBUYERGROUPID = '{RadDropDownList_Branch.SelectedValue}' ";
                if (RadTextBox_Barcode.Text != "")
                { SqlWhere += "AND SPC_ITEMNAME LIKE '%" + RadTextBox_Barcode.Text.Replace(" ", "%") + @"%' "; }


                ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(SqlWhere);
                if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
                {
                    Data_ITEMBARCODE = ShowDataDGV_Itembarcode.items;
                    RadTextBox_Barcode.Text = Data_ITEMBARCODE.Itembarcode_ITEMBARCODE;
                }
                else
                {
                    RadTextBox_Barcode.Text = ""; RadTextBox_Barcode.Focus(); return;
                }
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                //var isNumeric = double.TryParse(RadTextBox_Barcode.Text, out double n);
                if (double.TryParse(RadTextBox_Barcode.Text, out _))
                {
                    RadLabel_Qty.Text = RadTextBox_Barcode.Text;
                    Qty = double.Parse(RadLabel_Qty.Text);
                    RadTextBox_Barcode.Text = string.Empty;
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่จำนวนสินค้าให้ถูกต้อง");
                    RadTextBox_Barcode.SelectAll();
                    RadTextBox_Barcode.Focus();
                    return;
                }
            }
            else if (e.KeyCode == Keys.Enter && RadTextBox_Barcode.Text.Length > 0)
            {
                Data_ITEMBARCODE = new Data_ITEMBARCODE(RadTextBox_Barcode.Text);
                if (Data_ITEMBARCODE.Itembarcode_ITEMBARCODE == null)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีรายการสินค้าที่ต้องการค้นหา กรุณาลองใหม่อีกครั้ง");
                    RadTextBox_Barcode.SelectAll();
                    RadTextBox_Barcode.Focus();
                    return;
                }

                if (Data_ITEMBARCODE.Itembarcode_SPC_ITEMACTIVE == "0")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่สามารถจัดส่งสินค้าได้ เนื่องจากเป็นสินค้าสถานะ หยุด ใช้งาน");
                    RadTextBox_Barcode.SelectAll();
                    return;
                }

                //Check สิดแผนกที่สาขา คีย์ได้
                if (SystemClass.SystemComProgrammer != "1")
                {
                    if (SystemClass.SystemDptID != "D147")
                    {
                        if (Data_ITEMBARCODE.Itembarcode_DIMENSION != SystemClass.SystemDptID)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถจัดส่งสินค้าได้{Environment.NewLine}เนื่องจากเป็นสินค้าต่างแผนก");
                            RadTextBox_Barcode.SelectAll();
                            return;
                        }
                    }
                    else
                    {
                        if ((RadDropDownList_Dept.SelectedValue.ToString() != "D032") || (RadDropDownList_Dept.SelectedValue.ToString() != "D144"))
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถจัดส่งสินค้าได้{Environment.NewLine}เนื่องจากไม่มีสิทธ์สำหรับแผนก {RadDropDownList_Dept.SelectedItem[1]} ");
                            RadTextBox_Barcode.SelectAll();
                            return;
                        }
                    }
                }

                //Check แผนกสินค้าและรหัสที่ต้องการเปิด
                if (RadDropDownList_Dept.SelectedValue.ToString() != Data_ITEMBARCODE.Itembarcode_DIMENSION)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่สามารถจัดส่งสินค้าได้{Environment.NewLine}เนื่องจากรหัสแผนกที่ระบุและรหัสบาร์โค้ดไม่ตรงกัน{Environment.NewLine}เช็คข้อมูลสินค้าใหม่อีกครั้ง.");
                    RadTextBox_Barcode.SelectAll();
                    return;
                }


                string staSave = "1";
                ArrayList sql = new ArrayList();

                MRTDOCNO = RadLabel_Docno.Text;
                if (RadLabel_Docno.Text == "MRT0000000000000")
                {
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ต้องการบันทึกเอกสารเพิ่มของสาขา " +
                        RadDropDownList_Branch.SelectedItem[1].ToString()) == DialogResult.No)
                    {
                        return;
                    }
                    //MRTDOCNO = Class.ConfigClass.GetMaxINVOICEID("MRT", SystemClass.SystemDptID.Substring(1, 3), "MRT", "8");
                    MRTDOCNO = Class.ConfigClass.GetMaxINVOICEID("MRT", SystemClass.SystemDptID.Substring(1, 3), DateTime.Now.ToString("yyMMdd"), "8");

                    staSave = "0"; Round = "2";
                    string bchName = BranchClass.GetBranchNameByID(RadDropDownList_Branch.SelectedValue.ToString());

                    sql.Add(string.Format($@"
                    INSERT INTO Android_OrderTOHD(
                                DocNo, Date, Time, 
                                UserCode, Branch,BranchName, Depart, 
                                StaDoc, StaPrcDoc, StaApvDoc, 
                                StaAx, StaRound2, Box, Remark,
                                DateUp, TimeUp, WhoUp,WhoUpName,
                                DateIn, TimeIn, WhoIn,WhoInName,
                                DateApv, TimeApv, WhoApv,
                                DateAx, TimeAx, WhoAx, 
                                Round, StaGroupItem, GroupItem, DeptGroup)
                    VALUES  (   '{MRTDOCNO}', convert(varchar, getdate(), 23), convert(varchar, getdate(), 24),
                                '{SystemClass.SystemUserID_M}', '{RadDropDownList_Branch.SelectedValue}','{bchName}','{RadDropDownList_Dept.SelectedValue}', 
                                '1', '0', '0', 
                                '0', '0', 0, 'บันทึกสินค้าส่งเพิ่มเติม',
                                convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                                '', '', '', 
                                '', '', '', 
                                '2', '0', '0', '{RadDropDownList_Dept.SelectedValue}')"
                                ));

                }

                //                RadGridView_Show.Select();
                //Boolean CheckMRT_Barcode = (MRTMainDocBranch.CheckMRTOBarcode(MRTDOCNO, Data_ITEMBARCODE.Itembarcode_ITEMBARCODE));
                int iRow = MRTMainDocBranch.CheckMRTOBarcode(MRTDOCNO, Data_ITEMBARCODE.Itembarcode_ITEMBARCODE);

                double priceMN = CheckPrice(RadDropDownList_Branch.SelectedValue.ToString());

                if (iRow == 0)
                {
                    sql.Add(string.Format(@"
                    UPDATE  ANDROID_ORDERTODT 
                    SET     Qty = qty + {0}
                    WHERE   docno = '{1}'
                            and Barcode = '{2}'",
                        Qty, MRTDOCNO, Data_ITEMBARCODE.Itembarcode_ITEMBARCODE));
                    staSave = "0";
                }
                else
                {
                    sql.Add($@"
                    insert into Android_OrderTODT(
                        DocNo, SeqNo, ItemID, ItemDim,
                        Barcode, Name, 
                        QTY, UnitID, price,
                        DateIn, TimeIn, WhoIn,
                        StatusItem, Round, qtyround,QtyOrderCust)
                    values
                        (
                        '{MRTDOCNO}', '{iRow}', '{Data_ITEMBARCODE.Itembarcode_ITEMID}','{Data_ITEMBARCODE.Itembarcode_INVENTDIMID}',
                        '{Data_ITEMBARCODE.Itembarcode_ITEMBARCODE}', '{Data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME}',
                        '{Qty}', '{Data_ITEMBARCODE.Itembarcode_UNITID}', '{priceMN}',
                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}',
                        '0', '{Round}','0','0')");
                }

                string SqlInsDTTranction = ConnectionClass.ExecuteSQL_ArrayMain(sql);
                if (SqlInsDTTranction != "") { MsgBoxClass.MsgBoxShow_SaveStatus(SqlInsDTTranction); return; }
 
                if (staSave == "0")
                {
                    SetGridAndData(MRT_Class.GetMRTDocnoHD(MRTDOCNO));

                }
                else
                {
                    RadGridView_Show.Rows.Add(RadGridView_Show.Rows.Count + 1,
                    Data_ITEMBARCODE.Itembarcode_ITEMID,
                      Data_ITEMBARCODE.Itembarcode_INVENTDIMID,
                      Data_ITEMBARCODE.Itembarcode_ITEMBARCODE,
                      Data_ITEMBARCODE.Itembarcode_SPC_ITEMNAME,
                      String.Format("{0:0.##}", Qty),
                      "0",
                      Data_ITEMBARCODE.Itembarcode_UNITID,
                      priceMN.ToString("#,#0.00"),
                      String.Format("{0:0.##}", (Qty * priceMN)),
                      Round);
                }
                RadLabel_Qty.Text = ""; Qty = 1;
                RadTextBox_Barcode.SelectAll();
                RadTextBox_Barcode.Focus();
                // Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP13)
                this.Cursor = Cursors.Default;
            }
        }

        //ค้นหาราคาสินค้า ตามสาขา
        double CheckPrice(string pBchID)
        {
            double priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP14;
            //GetBranchSettingBranch
            DataTable dtBch = BranchClass.GetBranchSettingBranch(pBchID);
            if (dtBch.Rows[0]["BRANCH_PRICE"].ToString() == "SPC_PriceGroup13")
            {
                priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP13;
            }
            else if (dtBch.Rows[0]["BRANCH_PRICE"].ToString() == "SPC_PriceGroup15")
            {
                priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP15;
            }
            else if (dtBch.Rows[0]["BRANCH_PRICE"].ToString() == "SPC_PriceGroup17")
            {
                priceMN = Data_ITEMBARCODE.Itembarcode_SPC_PRICEGROUP17;
            }
            return priceMN;
        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        //Cell Edit
        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                ////แก้ไขรอบ
                //case "Round":
                //    if (RadGridView_Show.CurrentRow.Cells["Round"].Value.ToString() != "1" &&
                //    RadGridView_Show.CurrentRow.Cells["Round"].Value.ToString() != "2")
                //    {
                //        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีสามารถบันทึกข้อมูลได้ นอกจาก 1 หรือ 2 เท่านั้น");
                //        RadGridView_Show.CurrentRow.Cells["Round"].Value = RadLabel_Round.Text.Replace("รอบ : ", "");
                //        return;
                //    }

                //    string SqlUpRound = string.Format(@"
                //    UPDATE  Android_OrderTODT   
                //    SET     Round = '{0}',
                //            DATEUP = convert(varchar, getdate(), 23),
                //            TIMEUP = convert(varchar, getdate(), 24),
                //            WHOUP = '{1}'
                //    WHERE   Docno = '{2}'
                //            AND Barcode = '{3}'",
                //    RadGridView_Show.CurrentRow.Cells["Round"].Value.ToString(),
                //    SystemClass.SystemUserID_M, RadLabel_Docno.Text,
                //    RadGridView_Show.CurrentRow.Cells["Barcode"].Value.ToString());

                //    string SqlUpTranctionRound = ConnectionClass.ExecuteSQL_Main(SqlUpRound);
                //    if (SqlUpTranctionRound != "") MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranctionRound);
                //    break;

                //แก้ไขจำนวนสั่งสาขา
                case "Qty":
                    string SqlUpQty = string.Format(@"
                    UPDATE  Android_OrderTODT   
                    SET     Qty = '{0}',
                            DATEUP = convert(varchar, getdate(), 23),
                            TIMEUP = convert(varchar, getdate(), 24),
                            WHOUP = '{1}'
                    WHERE   Docno = '{2}'
                            AND Barcode = '{3}'",
                    RadGridView_Show.CurrentRow.Cells["Qty"].Value.ToString(),
                    SystemClass.SystemUserID_M, RadLabel_Docno.Text,
                    RadGridView_Show.CurrentRow.Cells["Barcode"].Value.ToString());

                    string SqlUpTranctionQty = ConnectionClass.ExecuteSQL_Main(SqlUpQty);
                    if (SqlUpTranctionQty != "")
                    { MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranctionQty); return; }
                    else
                    {
                        RadGridView_Show.CurrentRow.Cells["NET"].Value = (Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Qty"].Value) + Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["qtyround"].Value)) * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Price"].Value);
                    }

                    break;

                //แก้ไขจำนวนสั่งรอบ 2
                case "qtyround":
                    string SqlUpQtyR2 = string.Format(@"
                    UPDATE  Android_OrderTODT
                    SET     qtyround = '{0}',
                            DATEUP = convert(varchar, getdate(), 23),
                            TIMEUP = convert(varchar, getdate(), 24),
                            WHOUP = '{1}'
                    WHERE   Docno = '{2}'
                            AND Barcode = '{3}'",
                        RadGridView_Show.CurrentRow.Cells["qtyround"].Value.ToString(),
                        SystemClass.SystemUserID_M, RadLabel_Docno.Text,
                        RadGridView_Show.CurrentRow.Cells["Barcode"].Value.ToString());

                    string SqlUpTranctionQtyR2 = ConnectionClass.ExecuteSQL_Main(SqlUpQtyR2);
                    if (SqlUpTranctionQtyR2 != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranctionQtyR2);
                    }
                    else
                    {
                        RadGridView_Show.CurrentRow.Cells["NET"].Value = (Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Qty"].Value) + Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["qtyround"].Value)) * Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["Price"].Value);
                    }
                    break;

                default:
                    break;
            }

        }
        //อนุมัติบิล
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันอนุมัติเอกสาร จำนวนลัง " + RadTextBox_Box.Text + " ลังหรือไม่") == DialogResult.No)
            {
                RadTextBox_Box.SelectAll();
                return;
            }

            if (RadTextBox_Box.Text.Trim() == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่จำนวนลังให้เรียบร้อยก่อนอนุมัติเอกสาร");
                RadTextBox_Box.SelectAll();
                return;
            }

            if (int.Parse(RadTextBox_Box.Text.Trim()) <= 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่จำนวนลังให้เรียบร้อยก่อนอนุมัติเอกสาร");
                RadTextBox_Box.SelectAll();
                return;
            }

            //อนุมัติเอกสาร ต้องใส่จำนวนลัง
            String SqlUpAX;
            SqlUpAX = string.Format($@"
                UPDATE  Android_OrderTOHD 
                SET     StaApvDoc = '1',BoxPrint = '{RadTextBox_Box.Text}',
                        DATEUP = convert(varchar, getdate(), 23),
                        TIMEUP = convert(varchar, getdate(), 24),
                        WHOUP = '{SystemClass.SystemUserID_M}',
                        DateApv = convert(varchar, getdate(), 23),TimeApv = convert(varchar, getdate(), 24),WhoApv = '{SystemClass.SystemUserID_M}'
                WHERE   DOCNO = '{RadLabel_Docno.Text}'");

            string SqlUpTranction = ConnectionClass.ExecuteSQL_Main(SqlUpAX);
            if (SqlUpTranction != "") MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranction);
            ClearData("APV");
        }
        //Copy เอกสาร
        private void RadButtonElement_Copy_Click(object sender, EventArgs e)
        {
            using (OrderToAx_CopyDocument OrderToAx_CopyDocument = new OrderToAx_CopyDocument())
            {
                DialogResult dr = OrderToAx_CopyDocument.ShowDialog();
                if (dr == DialogResult.Yes)
                {
                    SetGridAndData(MRT_Class.GetMRTDocnoHD(OrderToAx_CopyDocument.MRTNewDocno));
                }
            }

        }
        //รอบ 1
        private void RadButton_Round1_Click(object sender, EventArgs e)
        {
            if (Round == "1")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("เอกสารเป็นรอบ 1 อยู่แล้ว สามารถเปลี่ยนรอบที่สินค้าแทนได้");
                return;
            }
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ต้องการเปลี่ยนเอกสารเป็นรอบ 1 หรือไม่") == DialogResult.No) return;

            ArrayList sqlR1 = new ArrayList
            {
                //แก้เอกสารเป็นรอบ 1
                $@"
                UPDATE  Android_OrderTOHD 
                SET     Round = '1',
                        DATEUP = convert(varchar, getdate(), 23),
                        TIMEUP = convert(varchar, getdate(), 24),
                        WHOUP = '{SystemClass.SystemUserID_M}'
                WHERE   DOCNO = '{RadLabel_Docno.Text}'",
                //MyJeeds ปรับ DT เป็นรอบ 1 ด้วย เช่นกัน
                $@"
                    UPDATE  Android_OrderTODT
                    SET     qtyround = '1'
                    WHERE   Docno = '{RadLabel_Docno.Text}' "
            };

            string SqlUpTranction = ConnectionClass.ExecuteSQL_ArrayMain(sqlR1);
            if (SqlUpTranction != "") MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranction);
            SetGridAndData(MRT_Class.GetMRTDocnoHD(RadLabel_Docno.Text));
        }
        //รอบ2
        private void RadButton_Round2_Click(object sender, EventArgs e)
        {
            if (Round == "2")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("เอกสารเป็นรอบ 2 อยู่แล้ว สามารถเปลี่ยนรอบที่สินค้าแทนได้");
                return;
            }
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ต้องการเปลี่ยนเอกสารเป็นรอบ 2 หรือไม่") == DialogResult.No) return;

            //แก้เอกสารเป็นรอบ 2
            ArrayList sqlR2 = new ArrayList
            {
                $@"
                UPDATE  Android_OrderTOHD 
                SET     Round = '2',
                        DATEUP = convert(varchar, getdate(), 23),
                        TIMEUP = convert(varchar, getdate(), 24),
                        WHOUP = '{SystemClass.SystemUserID_M}'
                WHERE   DOCNO = '{RadLabel_Docno.Text}'",
              //MyJeeds ปรับ DT เป็นรอบ 1 ด้วย เช่นกัน
                $@"
                    UPDATE  Android_OrderTODT
                    SET     qtyround = '2'
                    WHERE   Docno = '{RadLabel_Docno.Text}' "
            };


            string SqlUpTranction = ConnectionClass.ExecuteSQL_ArrayMain(sqlR2);
            if (SqlUpTranction != "") MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranction);
            SetGridAndData(MRT_Class.GetMRTDocnoHD(RadLabel_Docno.Text));
        }
        //Check Key Number
        private void RadTextBox_Box_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        //Export Excel
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView(RadLabel_Docno.Text, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        //
        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {

            switch (e.Column.Name)
            {
                case "Qty":
                    break;
                case "qtyround":
                    if (Round == "2") e.Cancel = true;
                    break;
                //case "Round":
                //    break;
                default:
                    e.Cancel = true;
                    break;
            }

        }

        //เปืดบิลเพิ่ม
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData("");
        }
        //ยกเลิกเอกสาร
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            //ยกเลิกเอกสาร
            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันยกเลิกเอกสาร หรือไม่") == DialogResult.No)
            {
                RadTextBox_Barcode.SelectAll();
                return;
            }

            if (RadButton_Apv.Enabled != true)
            {
                if (RadGridView_Show.Rows.Count > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("เอกสารอนุมัติแล้วไม่สามารถยกเลิกได้");
                    return;
                }
            }

            string SqlUp3 = string.Format(@"
            UPDATE  Android_OrderTOHD 
            SET     staprcdoc = '3',StaDoc = '3',
                    DATEUP = convert(varchar, getdate(), 23),
                    TIMEUP = convert(varchar, getdate(), 24),
                    WHOUP = '{0}'
            WHERE   DOCNO = '{1}'",
                         SystemClass.SystemUserID_M, RadLabel_Docno.Text);

            string SqlUpTranction = ConnectionClass.ExecuteSQL_Main(SqlUp3);
            if (SqlUpTranction != "") MsgBoxClass.MsgBoxShow_SaveStatus(SqlUpTranction);
            ClearData("CENCEL");
        }
        //Check สาขา
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            RadTextBox_Barcode.Focus();
        }


    }
}

