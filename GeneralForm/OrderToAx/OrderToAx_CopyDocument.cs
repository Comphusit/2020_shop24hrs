﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using System.Data;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_CopyDocument : Telerik.WinControls.UI.RadForm
    {

        public String MRTFindDocno;
        public String MRTNewDocno;
        public OrderToAx_CopyDocument()
        {
            InitializeComponent();
        }

        //load
        private void OrderToAx_CopyDocument_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            ClearDefault("DOCNO");
        }

        #region FUNCTION

        void ClearDefault(String Type)
        {
            if (Type == "BARCODE")
            {
                radioButton_Barcode.Checked = true;
                radTextBox_Barcode.Enabled = true;

                radioButton_Docno.Checked = false;
                RadTextBox_Dept.Text = SystemClass.SystemDptID.Substring(1, 3);
                RadTextBox_Dept.Enabled = false;
                RadTextBox_Date.Text = DateTime.Today.ToString("yyMMdd");
                RadTextBox_Date.Enabled = false;
                RadTextBox_Docno.Enabled = false;

                radTextBox_Barcode.TabIndex = 0;
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
            }
            else if (Type == "DOCNO")
            {
                radioButton_Barcode.Checked = false;
                radTextBox_Barcode.Enabled = false;

                radioButton_Docno.Checked = true;
                RadTextBox_Dept.Text = SystemClass.SystemDptID.Substring(1, 3);
                RadTextBox_Dept.Enabled = true;
                RadTextBox_Date.Text = DateTime.Today.ToString("yyMMdd");
                RadTextBox_Date.Enabled = true;
                RadTextBox_Docno.Enabled = true;
                RadTextBox_Docno.SelectAll();
                RadTextBox_Docno.Focus();
            }

        }

        #endregion

        #region EVEN
        //Cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //OK
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (radioButton_Barcode.Checked == true) MRTFindDocno = "MRT" + radTextBox_Barcode.Text;
 
            //if (radioButton_Docno.Checked == true) MRTFindDocno = "MRT" + RadTextBox_Dept.Text + RadTextBox_Date.Text + "-" + MRTClass.CheckDocno(RadTextBox_Docno.Text);
            if (radioButton_Docno.Checked == true) MRTFindDocno = "MRT" + RadTextBox_Dept.Text + RadTextBox_Date.Text + "-" + String.Format("{0:0000}", Int16.Parse(RadTextBox_Docno.Text));
            
             
            DataTable DtDocnoHD = MRT_Class.GetMRTDocnoHD(MRTFindDocno);
            //String MRTDOCNO;
            if (DtDocnoHD.Rows.Count > 0)
            {
                DataTable DtDocnoDT = MRT_Class.GetMRTDocnoDT(MRTFindDocno);
                if (DtDocnoDT.Rows.Count > 0)
                {
                    ArrayList SqlMRT = new ArrayList();
                    string MRTDOCNO = Class.ConfigClass.GetMaxINVOICEID("MRT", SystemClass.SystemDptID.Substring(1, 3), DateTime.Now.ToString("yyMMdd"), "8");
                    SqlMRT.Add($@" 
                    INSERT INTO ANDROID_ORDERTOHD
                        (DocNo, Date, Time, 
                        UserCode, Branch, BranchName,
                        Depart, StaDoc, StaPrcDoc, StaApvDoc,StaAx,StaRound2,Box, Remark,   
                        DateIn, TimeIn, WhoIn, WhoInName,
                        Round, StaGroupItem, GroupItem, DeptGroup)  
                    VALUES('{MRTDOCNO}',convert(varchar, getdate(), 23), convert(varchar,getdate(),24), 
                        '{SystemClass.SystemUserID_M}','{DtDocnoHD.Rows[0]["Branch"]}','{DtDocnoHD.Rows[0]["BranchName"]}', 
                        '{DtDocnoHD.Rows[0]["Depart"]}','1', '0', '0', '0', '0', 0, 'คัดลอก {MRTFindDocno}',
                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',
                        '{DtDocnoHD.Rows[0]["Round"]}', '{DtDocnoHD.Rows[0]["StaGroupItem"]}', '{DtDocnoHD.Rows[0]["GroupItem"]}', '{DtDocnoHD.Rows[0]["DeptGroup"]}' )");


                    for (int iMRTDT = 0; iMRTDT < DtDocnoDT.Rows.Count; iMRTDT++)
                    {
                        SqlMRT.Add(String.Format(@" insert into Android_OrderTODT(  
                        DocNo, SeqNo, ItemID, ItemDim,   
                        Barcode, Name, QTY, UnitID, price,  
                        DateIn, TimeIn, WhoIn,  
                        DateUp, TimeUp, WhoUp, StatusItem,Round)  
                        values  
                        ( '{0}', '{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}',
                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{9}',
                        convert(varchar, getdate(), 23), convert(varchar, getdate(), 24), '{9}',
                        '0', '{10}')",
                        MRTDOCNO,
                        iMRTDT + 1,
                        DtDocnoDT.Rows[iMRTDT]["ItemID"].ToString(),
                        DtDocnoDT.Rows[iMRTDT]["ItemDim"].ToString(),
                        DtDocnoDT.Rows[iMRTDT]["Barcode"].ToString(),
                        DtDocnoDT.Rows[iMRTDT]["Name"].ToString(),
                        DtDocnoDT.Rows[iMRTDT]["Qty"].ToString(),
                        DtDocnoDT.Rows[iMRTDT]["UnitID"].ToString(),
                        DtDocnoDT.Rows[iMRTDT]["Price"].ToString(),
                        SystemClass.SystemUserID_M,
                        DtDocnoDT.Rows[iMRTDT]["Round"].ToString()));
                    }
                 //   String SqlMRTTransection =;
                    MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_ArrayMain(SqlMRT));
                    MRTNewDocno = MRTDOCNO;
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีรายการข้อมูล ไม่สามารถคัดลอกได้");
                    this.DialogResult = DialogResult.No;
                    this.Close();
                }
            }
            else
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่มีเอกสารที่ต้องการ ไม่สามารถคัดลอกได้");
                this.DialogResult = DialogResult.No;
                this.Close();
            }
        }

        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                MRTFindDocno = "MRT" + radTextBox_Barcode.Text;
            }
        }

        private void RadioButton_Barcode_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Barcode.Checked == true)
            {
                ClearDefault("BARCODE");
            }
        }

        private void RadioButton_Docno_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Docno.Checked == true)
            {
                ClearDefault("DOCNO");
            }
        }

        private void RadTextBox_Docno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radButton_Save.Focus();
                //MRTFindDocno = "MRTO" + radTextBox_Dept.Text + radTextBox_Date.Text + MRTClass.CheckDocno(radTextBox_Docno.Text);
                //this.DialogResult = DialogResult.Yes;
                //this.Close();
            }
            else if (e.KeyCode == Keys.Left)
            {
                RadTextBox_Date.SelectAll();
                RadTextBox_Date.Focus();
            }
        }

        private void RadTextBox_Date_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Enter)
            {
                RadTextBox_Docno.SelectAll();
                RadTextBox_Docno.Focus();
            }
            else if (e.KeyCode == Keys.Left)
            {
                RadTextBox_Dept.SelectAll();
                RadTextBox_Dept.Focus();
            }
        }

        private void RadTextBox_Dept_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Enter)
            {
                RadTextBox_Date.SelectAll();
                RadTextBox_Date.Focus();
            }
        }



        private void RadTextBox_Barcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void RadTextBox_Dept_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void RadTextBox_Date_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void RadTextBox_Docno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }
        #endregion

    }
}
