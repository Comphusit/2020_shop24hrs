﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_FindData : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dtDataHD = new DataTable();
        readonly DataTable dtDataDT = new DataTable();

        // readonly string pDept; // shop - supc  
        public string pDocno;
        string Branch, Dept;

        public OrderToAx_FindData()
        {
            InitializeComponent();
        }

        void Set_Branch()
        {
            RadDropDownList_Branch.DataSource = BranchClass.GetBranchAll(" '1','4' ", " '1' ");
            RadCheckBox_Branch.Checked = false;
            RadDropDownList_Branch.Enabled = false;
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
        }
        void Set_Dept()
        {
            RadDropDownList_Dept.DataSource = MRTClass.GetDept(SystemClass.SystemDptID);
            RadCheckBox_Dept.Checked = true;
            RadDropDownList_Dept.Enabled = true;
            RadDropDownList_Dept.ValueMember = "NUM";
            RadDropDownList_Dept.DisplayMember = "DeptName";
        }


        //Load
        private void OrderToAx_FindData_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            RadButtonElement_pdt.ShowBorder = true;

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            RadCheckBox_Dept.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Status.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(-2), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now.AddDays(+1));

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);


            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Branch", "สาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocNo", "เลขที่บิล", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DocnoDate", "วันที่บิล", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("StaPrcDoc", "ยกเลิก", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("StaDoc", "บันทึก", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("StaApvDoc", "รอส่งสินค้า", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("StaAx", "ส่งสินค้า", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("ROUND", "รอบส่ง", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Remark", "หมายเหตุ", 300)));

            RadGridView_ShowHD.Columns["Branch"].IsPinned = true;
            RadGridView_ShowHD.Columns["BranchName"].IsPinned = true;
            RadGridView_ShowHD.Columns["DocNo"].IsPinned = true;

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "StaPrcDoc = 'X'  ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.RadGridView_ShowHD.Columns["StaPrcDoc"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_ShowHD.Columns["DocNo"].ConditionalFormattingObjectList.Add(obj1);

            obj1 = new ExpressionFormattingObject("MyCondition1", "StaDoc = '/'  ", false)
            { CellBackColor = ConfigClass.SetColor_GreenPastel() };
            this.RadGridView_ShowHD.Columns["StaDoc"].ConditionalFormattingObjectList.Add(obj1);

            obj1 = new ExpressionFormattingObject("MyCondition1", "StaApvDoc = '/'  ", false)
            { CellBackColor = ConfigClass.SetColor_GreenPastel() };
            this.RadGridView_ShowHD.Columns["StaApvDoc"].ConditionalFormattingObjectList.Add(obj1);

            obj1 = new ExpressionFormattingObject("MyCondition1", "StaAx = '/'  ", false)
            { CellBackColor = ConfigClass.SetColor_GreenPastel() };
            this.RadGridView_ShowHD.Columns["StaAx"].ConditionalFormattingObjectList.Add(obj1);

            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Barcode", "บาร์โค้ด", 120));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Name", "ชื่อสินค้า", 220));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("Qty", "จำนวนต้องส่ง", 120));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("qtyround", "จำนวนรอส่ง", 120));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UnitID", "หน่วย", 70));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("price", "ราคา", 90));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("NET", "ราคารวม", 90));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("Round", "รอบสินค้า", 80));
 
            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_End.Value = DateTime.Now.AddDays(1);

            Set_Branch();
            Set_Dept();
            RadButton_Search_Click(sender, e);
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }


        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if (RadCheckBox_Branch.Checked == true)
            {
                Branch = RadDropDownList_Branch.SelectedValue.ToString();
            }
            if (RadCheckBox_Dept.Checked == true)
            {
                Dept = RadDropDownList_Dept.SelectedValue.ToString();
            }
            RadGridView_ShowHD.DataSource = MRT_Class.GetFindDocument(Dept, Branch,
                                                                radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"),
                                                                radDateTimePicker_End.Value.ToString("yyyy-MM-dd"));
            //if (RadGridView_ShowHD.RowCount == 0) RadGridView_ShowDT.DataSource = MRTClass.GetFindDocumentDetail("");
            if (RadGridView_ShowHD.RowCount == 0) RadGridView_ShowDT.DataSource = MRT_Class.GetMRTDocnoDT("");

            this.Cursor = Cursors.Default;
        }

        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {

            if ((RadGridView_ShowHD.CurrentRow.Index == -1) || (RadGridView_ShowHD.CurrentRow.Cells["DocNo"].Value.ToString() == "")) { return; }
            if (RadGridView_ShowHD.Rows.Count == 0) { if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); } return; }
            //RadGridView_ShowDT.DataSource = MRTClass.GetFindDocumentDetail(
            //    RadGridView_ShowHD.CurrentRow.Cells["DocNo"].Value.ToString());
            RadGridView_ShowDT.DataSource = MRT_Class.GetMRTDocnoDT(
                RadGridView_ShowHD.CurrentRow.Cells["DocNo"].Value.ToString());

        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["DocNo"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true)
            {
                RadDropDownList_Branch.Enabled = true;
            }
            else
            {
                RadDropDownList_Branch.Enabled = false;
            }
        }

        private void RadGridView_ShowHD_FilterChanged(object sender, GridViewCollectionChangedEventArgs e)
        {
            if (RadGridView_ShowHD.RowCount < 1) return;
        }

        private void RadCheckBox_Status_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_Status.Checked == true)
            {
                radioButton_SAVE.Enabled = true;
                radioButton_CENCEL.Enabled = true;
                radioButton_CONF.Enabled = true;
                radioButton_APV.Enabled = true;

                radioButton_SAVE.Checked = true;
                //Status = "SAVE";
            }
            else
            {
                //Status = "";
                radioButton_SAVE.Enabled = false;
                radioButton_CENCEL.Enabled = false;
                radioButton_CONF.Enabled = false;
                radioButton_APV.Enabled = false;
            }
        }

        private void RadioButton_SAVE_CheckedChanged(object sender, EventArgs e)
        {
            //Status = "SAVE";
        }

        private void RadioButton_CENCEL_CheckedChanged(object sender, EventArgs e)
        {
            //Status = "CENCEL";
        }

        private void RadioButton_CONF_CheckedChanged(object sender, EventArgs e)
        {
            //Status = "CONF";
        }

        private void RadioButton_APV_CheckedChanged(object sender, EventArgs e)
        {
            //Status = "APV";
        }

        private void RadCheckBox_Branch_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (RadCheckBox_Branch.Checked == true)
            {
                RadDropDownList_Branch.Enabled = true;
                Branch = RadDropDownList_Branch.SelectedValue.ToString();
            }
            else
            {
                RadDropDownList_Branch.Enabled = false;
                Branch = "";
            }
        }

        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Branch.DataSource != null && RadCheckBox_Branch.Checked == true)
            {
                Branch = RadDropDownList_Branch.SelectedValue.ToString();
            }
        }

        private void RadCheckBox_Dept_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (RadCheckBox_Dept.Checked == true)
            {
                RadDropDownList_Dept.Enabled = true;
                Dept = RadDropDownList_Dept.SelectedValue.ToString();
            }
            else
            {
                RadDropDownList_Dept.Enabled = false;
                Dept = "";
            }
        }

        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.DataSource != null && RadCheckBox_Dept.Checked == true)
            {
                Dept = RadDropDownList_Dept.SelectedValue.ToString();
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
