﻿//CheckOK
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PrintBarcode;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.OrderToAx
{
    public partial class OrderToAx_PrintT : Telerik.WinControls.UI.RadForm
    {
        readonly PrintController printController = new StandardPrintController();
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        BILLTRANS billTrans = new BILLTRANS();
        readonly BILLPRINT billPrint = new BILLPRINT();

        DataTable dtgrid = new DataTable("dtgrid");
        DataTable dtbox = new DataTable("dtbox");
        string countprint;

        //Load
        public OrderToAx_PrintT()
        {
            InitializeComponent();
        }
        //Load
        private void OrderToAx_PrintT_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            radButtonElement_print.ShowBorder = true; radButtonElement_print.ToolTipText = "พิมพ์บิลและแท็ก";
            radButtonElement_PrintBill.ShowBorder = true; radButtonElement_PrintBill.ToolTipText = "พิมพ์เฉพาะบิล";
            radButtonElement_PrintTag.ShowBorder = true; radButtonElement_PrintTag.ToolTipText = "พิมพ์เฉพาะแท็ก";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            btnInQuery.ButtonElement.ShowBorder = true;

            radDropDownList_Branch.Enabled = false;
            radDropDownList_Dept.Enabled = true;
            radDropDownList_Route.Enabled = false;
            radCheckBox2.Checked = false;
            radCheckBox3.Checked = false;

            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Printer);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_PrintTag);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dept);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Route);

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_d1, DateTime.Now, DateTime.Now);

            SetPrint();
            BindDropDownListDept();
            BindDropDownListBranch();
            BindDropDownListRoute();


            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);


            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_CheckBox("CHECK", "เลือก", 70)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VOUCHERID", "เลขที่เอกสาร", 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDFROM", "คลัง", 110)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEDOCNO", "วันที่เอกสาร", 110)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TIMEDOCNO", "เวลาบิล", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัสสาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHNAME", "ชื่อสาขา", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTEID", "เลขสายรถ", 85)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTENAME", "เส้นทาง", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STATUSDOCNO", "สถานะ", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BOXGROUP", "จำนวนลัง", 70)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("COUNTPRINT", "ครั้งที่พิมพ์", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LINENUM", "ลำดับ", 70)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "สินค้า", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 70)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODEUNIT", "หน่วย", 70)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CASHIERID", "รหัสแคชเชียร์")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CASHIERNAME", "ชื่อแคชเชียร์")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMBARCODE", "รหัสบาร์โค้ดสินค้า")));

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition", "COUNTPRINT <> '0' ", false)
            { CellBackColor = ConfigClass.SetColor_PinkPastel() };
            this.RadGridView_ShowHD.Columns["VOUCHERID"].ConditionalFormattingObjectList.Add(obj1);
            this.RadGridView_ShowHD.Columns["COUNTPRINT"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition", "LINENUM <> '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.RadGridView_ShowHD.Columns["CHECK"].ConditionalFormattingObjectList.Add(obj2);
        }

        //ปุ่มค้นหา
        private void BtnInQuery_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            billTrans = SetBillTran();
            dtgrid = MRTClass.PrintT_GetAndroidOrderTo(billTrans.DATEDOC, billTrans.DEPT, billTrans.BRANCHID, billTrans.ROUTE);

            if (dtgrid.Rows.Count == 0) MsgBoxClass.MsgBoxShow_FindRecordNoData("บิลจัดส่งสินค้า");

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dtgrid;
            dtgrid.AcceptChanges();

            this.Cursor = Cursors.Default;
        }

        private BILLTRANS SetBillTran()
        {
            BILLTRANS newBillTrans = new BILLTRANS()
            {
                DATEDOC = radDateTimePicker_d1.Value.ToString("yyyy-MM-dd"),
                DEPT = radDropDownList_Dept.SelectedIndex != -1 ? radDropDownList_Dept.SelectedValue.ToString() : "",
                BRANCHID = (radCheckBox2.Checked == true & radDropDownList_Branch.SelectedIndex != -1) ? radDropDownList_Branch.SelectedValue.ToString() : "",
                ROUTE = (radCheckBox3.Checked == true & radDropDownList_Route.SelectedIndex != -1) ? radDropDownList_Route.SelectedValue.ToString() : "",
            };
            return newBillTrans;
        }

        //ปุ่มพิมพ์บิลและแท็ก
        private void RadButtonElement_print_Click(object sender, EventArgs e)
        {
            int contcheck = 0;
            RadGridView_ShowHD.EndEdit();

            foreach (GridViewRowInfo row in RadGridView_ShowHD.MasterView.Rows)
            {
                var val = row.Cells["CHECK"].Value;
                if (val == null || val.ToString() == "0" || val.ToString().ToLower() == "false") continue;
                else if (val.ToString() == "1")
                {
                    contcheck++;

                    billPrint.VOUCHER = row.Cells["VOUCHERID"].Value.ToString();
                    billPrint.INVENTLOCATIONID = row.Cells["INVENTLOCATIONIDFROM"].Value.ToString();
                    billPrint.DATEDOC = row.Cells["DATEDOCNO"].Value.ToString();
                    billPrint.TIMEDOC = row.Cells["TIMEDOCNO"].Value.ToString();
                    billPrint.BRANCHID = row.Cells["BRANCH_ID"].Value.ToString();
                    billPrint.BRANCHNAME = row.Cells["BRANCHNAME"].Value.ToString();
                    billPrint.ROUTEID = row.Cells["ROUTEID"].Value.ToString();
                    billPrint.ROUTENAME = row.Cells["ROUTENAME"].Value.ToString();
                    billPrint.CASHIERID = row.Cells["CASHIERID"].Value.ToString();
                    billPrint.CASHIERNAME = row.Cells["CASHIERNAME"].Value.ToString();

                    int count = Convert.ToInt32(row.Cells["COUNTPRINT"].Value);
                    countprint = (count + 1).ToString();
                    dtbox = MRTClass.PrintT_BoxGroup(billPrint.VOUCHER);

                    DataTable dt_DetailSelect = new DataTable();
                    dt_DetailSelect.Columns.Add("VOUCHERID");
                    dt_DetailSelect.Columns.Add("NUMBERLABEL");
                    dt_DetailSelect.Columns.Add("BOXNAME");
                    dt_DetailSelect.Columns.Add("BOXGROUP");
                    dt_DetailSelect.Columns.Add("SHIPDATE");
                    dt_DetailSelect.Columns.Add("ROUTENAME");
                    dt_DetailSelect.Columns.Add("LOCATIONNAMETO");

                    for (int j = 0; j < dtbox.Rows.Count; j++)
                    {

                        dt_DetailSelect.Rows.Add(billPrint.VOUCHER, (j + 1).ToString() + "/" + dtbox.Rows.Count.ToString(),
                           "ลังสูง", dtbox.Rows[j]["BOXGROUP"].ToString(), billPrint.DATEDOC, billPrint.ROUTEID, billPrint.BRANCHNAME);
                    }

                    System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                    printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                    printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                    printDocument1.PrintController = printController;

                    if (InsertAndroid_CountPrint(billPrint.VOUCHER, count) == "")
                    {
                        printDocument1.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = radDropDownList_Printer.SelectedItem.Text;
                        printDocument1.Print();
                        PrintBoxFaceSheet30x40_GoDEX(radDropDownList_PrintTag.SelectedItem.Text, dt_DetailSelect);
                        row.Cells["COUNTPRINT"].Value = countprint;
                    }

                }
            }

            if (contcheck == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("เลือกเอกสารก่อนกดพิมพ์");
                return;
            }

            dtgrid.AcceptChanges();
        }

        //พิมพ์เฉพาะบิล
        private void RadButtonElement_PrintBill_Click(object sender, EventArgs e)
        {
            int contcheck = 0;
            RadGridView_ShowHD.EndEdit();

            foreach (GridViewRowInfo row in RadGridView_ShowHD.MasterView.Rows)
            {
                var val = row.Cells["CHECK"].Value;
                if (val == null || val.ToString() == "0" || val.ToString().ToLower() == "false") continue;
                else if (val.ToString() == "1")
                {
                    contcheck++;

                    billPrint.VOUCHER = row.Cells["VOUCHERID"].Value.ToString();
                    billPrint.INVENTLOCATIONID = row.Cells["INVENTLOCATIONIDFROM"].Value.ToString();
                    billPrint.DATEDOC = row.Cells["DATEDOCNO"].Value.ToString();
                    billPrint.TIMEDOC = row.Cells["TIMEDOCNO"].Value.ToString();
                    billPrint.BRANCHID = row.Cells["BRANCH_ID"].Value.ToString();
                    billPrint.BRANCHNAME = row.Cells["BRANCHNAME"].Value.ToString();
                    billPrint.ROUTEID = row.Cells["ROUTEID"].Value.ToString();
                    billPrint.ROUTENAME = row.Cells["ROUTENAME"].Value.ToString();
                    billPrint.CASHIERID = row.Cells["CASHIERID"].Value.ToString();
                    billPrint.CASHIERNAME = row.Cells["CASHIERNAME"].Value.ToString();

                    int count = Convert.ToInt32(row.Cells["COUNTPRINT"].Value);
                    countprint = (count + 1).ToString();
                    dtbox = MRTClass.PrintT_BoxGroup(billPrint.VOUCHER);

                    System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                    printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                    printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                    printDocument1.PrintController = printController;

                    if (InsertAndroid_CountPrint(billPrint.VOUCHER, count) == "")
                    {
                        printDocument1.PrinterSettings.DefaultPageSettings.PrinterSettings.PrinterName = radDropDownList_Printer.SelectedItem.Text;
                        printDocument1.Print();
                        row.Cells["COUNTPRINT"].Value = countprint;
                    }
                }
            }

            if (contcheck == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("เลือกเอกสารก่อนกดพิมพ์");
                return;
            }

            dtgrid.AcceptChanges();
        }

        private void RadButtonElement_PrintTag_Click(object sender, EventArgs e)
        {
            int contcheck = 0;
            RadGridView_ShowHD.EndEdit();

            foreach (GridViewRowInfo row in RadGridView_ShowHD.MasterView.Rows)
            {
                var val = row.Cells["CHECK"].Value;
                if (val == null || val.ToString() == "0" || val.ToString().ToLower() == "false") continue;
                else if (val.ToString() == "1")
                {
                    contcheck++;

                    billPrint.VOUCHER = row.Cells["VOUCHERID"].Value.ToString();
                    billPrint.INVENTLOCATIONID = row.Cells["INVENTLOCATIONIDFROM"].Value.ToString();
                    billPrint.DATEDOC = row.Cells["DATEDOCNO"].Value.ToString();
                    billPrint.TIMEDOC = row.Cells["TIMEDOCNO"].Value.ToString();
                    billPrint.BRANCHID = row.Cells["BRANCH_ID"].Value.ToString();
                    billPrint.BRANCHNAME = row.Cells["BRANCHNAME"].Value.ToString();
                    billPrint.ROUTEID = row.Cells["ROUTEID"].Value.ToString();
                    billPrint.ROUTENAME = row.Cells["ROUTENAME"].Value.ToString();
                    billPrint.CASHIERID = row.Cells["CASHIERID"].Value.ToString();
                    billPrint.CASHIERNAME = row.Cells["CASHIERNAME"].Value.ToString();

                    int count = Convert.ToInt32(row.Cells["COUNTPRINT"].Value);
                    countprint = (count + 1).ToString();
                    dtbox = MRTClass.PrintT_BoxGroup(billPrint.VOUCHER);

                    DataTable dt_DetailSelect = new DataTable();
                    dt_DetailSelect.Columns.Add("VOUCHERID");
                    dt_DetailSelect.Columns.Add("NUMBERLABEL");
                    dt_DetailSelect.Columns.Add("BOXNAME");
                    dt_DetailSelect.Columns.Add("BOXGROUP");
                    dt_DetailSelect.Columns.Add("SHIPDATE");
                    dt_DetailSelect.Columns.Add("ROUTENAME");
                    dt_DetailSelect.Columns.Add("LOCATIONNAMETO");

                    for (int j = 0; j < dtbox.Rows.Count; j++)
                    {

                        dt_DetailSelect.Rows.Add(billPrint.VOUCHER, (j + 1).ToString() + "/" + dtbox.Rows.Count.ToString(),
                           "ลังสูง", dtbox.Rows[j]["BOXGROUP"].ToString(), billPrint.DATEDOC, billPrint.ROUTEID, billPrint.BRANCHNAME);
                    }

                    if (InsertAndroid_CountPrint(billPrint.VOUCHER, count) == "")
                    {
                        PrintBoxFaceSheet30x40_GoDEX(radDropDownList_PrintTag.SelectedItem.Text, dt_DetailSelect);
                        row.Cells["COUNTPRINT"].Value = countprint;
                    }
                }
            }

            if (contcheck == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("เลือกเอกสารก่อนกดพิมพ์");
                return;
            }

            dtgrid.AcceptChanges();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }


        //Set และตั้งค่า Printer
        private void SetPrint()
        {
            try
            {
                PrinterSettings prt = new PrinterSettings();
                string pkInstalledPrinters;
                int iRow = 0;

                for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
                {
                    pkInstalledPrinters = PrinterSettings.InstalledPrinters[i];
                    radDropDownList_Printer.Items.Add(pkInstalledPrinters);
                    radDropDownList_PrintTag.Items.Add(pkInstalledPrinters);
                    if (prt.PrinterName == pkInstalledPrinters)
                    {
                        iRow = i;
                    }
                }

                radDropDownList_Printer.SelectedIndex = iRow;
                radDropDownList_PrintTag.SelectedIndex = iRow;
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถเลือกเครื่องปริ้นได้ ลองใหม่อีกครั้ง{Environment.NewLine}{ex.Message}");
            }
        }

        //PrinTag GoDEX
        void PrintBoxFaceSheet30x40_GoDEX(string _printerName, DataTable dt_DetailTag)
        {
            if (dt_DetailTag == null || dt_DetailTag.Rows.Count == 0) return;

            StringBuilder rowData = new StringBuilder();
            int iBox = 1;
            foreach (DataRow row in dt_DetailTag.Rows)
            {
                string Dept = string.Empty;
                Dept = row["VOUCHERID"].ToString().Substring(9, 3);
                string NumLabel = row["NUMBERLABEL"].ToString();
                if (NumLabel == "") NumLabel = "[" + iBox.ToString() + "/" + dt_DetailTag.Rows.Count.ToString() + "]"; else NumLabel = "[" + NumLabel + "]";

                rowData.Append("^Q38,3\n");
                rowData.Append("^W28\n");
                rowData.Append("^H0\n");
                rowData.Append("^P1\n");
                rowData.Append("^S6\n");
                rowData.Append("^AD\n");
                rowData.Append("^C1\n");
                rowData.Append("^R0\n");
                rowData.Append("~Q+0\n");
                rowData.Append("^O0\n");
                rowData.Append("^D0\n");
                rowData.Append("^E16\n");
                rowData.Append("~R200\n");
                rowData.Append("^XSET,ROTATION,0\n");
                rowData.Append("^L\n");
                rowData.Append("Dy2-me-dd\n");
                rowData.Append("Th:m:s\n");
                rowData.Append("Dy2-me-dd\n");
                rowData.Append("Th:m:s\n");

                // typeItem, row("NUMBERLABEL")
                rowData.AppendFormat("ATA,226,0,28,28,0,1,A,0,{0}\n", row["BOXNAME"]);
                rowData.AppendFormat("BQ,185,16,2,6,50,1,0,{0}\n", row["BOXGROUP"]);
                rowData.AppendFormat("ATA,135,2,30,30,0,1,A,0,{0}    {1}\n", row["BOXGROUP"], DateTime.Parse(row["SHIPDATE"].ToString()).ToString("dd.MM.yy"));
                rowData.AppendFormat("ATA,110,0,30,30,0,1,A,0,{0} \n", row["VOUCHERID"]);//, NumLabel
                rowData.AppendFormat("ATA,85,0,30,30,0,1,A,0,{0}\n", row["ROUTENAME"]);
                rowData.AppendFormat("ATA,56,0,42,42,0,1,A,0,{0}\n", row["LOCATIONNAMETO"]);
                rowData.AppendFormat("ATA,108,240,28,28,0,1,A,0,{0}\n", NumLabel);
                rowData.Append("E\n");
                iBox++;
            }
            RawPrinterHelper.SendStringToPrinter(_printerName, rowData.ToString());
        }


        //Set detail on paper
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //Font printFont = new Font("Tahoma", 9, System.Drawing.FontStyle.Regular);
            barcode.Data = billPrint.VOUCHER; Bitmap barcodeBill = new Bitmap(barcode.drawBarcode());
            //int countbox = 0;

            int Y = 20;
            e.Graphics.DrawString(billPrint.BRANCHID + " - " + billPrint.BRANCHNAME, SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 25;
            e.Graphics.DrawString("ใบสั่งโอนย้าย-การจัดส่ง", SystemClass.printFont, Brushes.Black, 25, Y);
            Y += 25;
            e.Graphics.DrawString("วันที่บิล : " + billPrint.DATEDOC + " ครั้งที่ " + Convert.ToString(countprint), SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawString("สายรถ : " + billPrint.ROUTEID + " - " + billPrint.ROUTENAME, SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawString("CH : " + billPrint.CASHIERID + " - " + billPrint.CASHIERNAME, SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawImage(barcodeBill, 13, Y);
            Y += 75;
            e.Graphics.DrawString("วันที่พิมพ์ : " + DateTime.Now.ToString("dd-MM-yyyy HH:MM:ss"), SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            DataRow[] rowSelect = dtgrid.Select($@"VOUCHERID='{billPrint.VOUCHER}' ");

            for (int i = 0; i < rowSelect.Length; i++)
            {
                Y += 25;
                e.Graphics.DrawString(rowSelect[i]["LINENUM"].ToString() + "." + "(" + rowSelect[i]["QTY"].ToString() + " x " + rowSelect[i]["SPC_ITEMBARCODEUNIT"].ToString() + ") " + rowSelect[i]["ITEMBARCODE"].ToString(), SystemClass.printFont, Brushes.Black, 15, Y);
                Y += 25;
                e.Graphics.DrawString(rowSelect[i]["SPC_ITEMNAME"].ToString(), SystemClass.printFont, Brushes.Black, 15, Y);
            }
            Y += 25;
            e.Graphics.DrawString("ผู้รับ..................................................", SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawString("ผู้ส่ง..................................................", SystemClass.printFont, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);


        }

        #region DropDownList
        private void BindDropDownListDept()
        {
            radDropDownList_Dept.DataSource = MRTClass.GetDept("D204");
            radDropDownList_Dept.ValueMember = "NUM";
            radDropDownList_Dept.DisplayMember = "DESCRIPTION";
        }
        private void BindDropDownListBranch()
        {
            radCheckBox2.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radDropDownList_Branch.DataSource = BranchClass.GetBranchAll_ByConditions(" '1' ", " '1' ", " AND BRANCH_ORDERTOAX = '1' ");
            radDropDownList_Branch.ValueMember = "BRANCH_ID";
            radDropDownList_Branch.DisplayMember = "NAME_BRANCH";
        }
        private void BindDropDownListRoute()//string _Dept
        {
            radCheckBox3.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radDropDownList_Route.DataSource = LogisticClass.GetRouteAll_MainRoute();// GetRoute(_Dept);
            radDropDownList_Route.ValueMember = "ROUTEID";
            radDropDownList_Route.DisplayMember = "ROUTENAME";
        }
        #endregion

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        } //row number datagrid

        private void RadGridView_BillTran_CellClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "CHECK":
                    if (RadGridView_ShowHD.CurrentRow.Cells["LINENUM"].Value.ToString().Equals("1"))
                    {
                        if (RadGridView_ShowHD.CurrentRow.Cells[e.ColumnIndex].Value.ToString().Equals("0"))
                        {
                            RadGridView_ShowHD.CurrentRow.Cells[e.ColumnIndex].Value = 1;
                        }
                        else
                        {
                            RadGridView_ShowHD.CurrentRow.Cells[e.ColumnIndex].Value = 0;
                        }
                    }
                    else
                    {
                        RadGridView_ShowHD.CurrentRow.Cells[e.ColumnIndex].Value = 0;
                    }
                    break;
                default:
                    break;
            }
        } //click check on row

        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            //if (radDropDownList_Dept.SelectedIndex != -1)
            //{
            //    BindDropDownListRoute(radDropDownList_Dept.SelectedValue.ToString());
            //}
        }

        private void RadCheckBox_Branch_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox2.CheckState == CheckState.Checked)
            {
                radDropDownList_Branch.Enabled = true;
            }
            else
            {
                radDropDownList_Branch.Enabled = false;
            }
        } // การทำงาน checkbox สาขา

        private void RadCheckBox_Route_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox3.CheckState == CheckState.Checked)
            {
                radDropDownList_Route.Enabled = true;
            }
            else
            {
                radDropDownList_Route.Enabled = false;
            }
        } // การทำงาน checkbox สายรถ

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        #region GetRoute
        //DataTable GetRoute(string _Dept)
        //{
        //    string sql = string.Format($@"
        //            SELECT  DISTINCT SPC_ROUTETABLE.ROUTEID,SPC_ROUTETABLE.ROUTEID + ' : ' + SPC_ROUTETABLE.NAME AS  [NAME] 
        //            FROM    ANDROID_ROUTE  WITH (NOLOCK) INNER JOIN 
        //                 SHOP2013TMP.dbo.SPC_ROUTETABLE  WITH (NOLOCK) ON ANDROID_ROUTE.ROUTEID = SPC_ROUTETABLE.ROUTEID 
        //            WHERE   DEPT = '{_Dept}' 
        //            ORDER BY SPC_ROUTETABLE.ROUTEID
        //            ");
        //    DataTable dt = Controllers.ConnectionClass.SelectSQL_Main(sql);
        //    return dt;
        //}
        #endregion

        #region AndroidOrder
        //    internal static DataTable AGetAndroidOrderTo(string _DateDoc, string _Dept, string _Branch, string _Route)
        //    {

        //        if (!string.IsNullOrEmpty(_Branch)) _Branch = $"AND BRANCH_ID='{_Branch}'"; //_Branch = "",mn009
        //        if (!string.IsNullOrEmpty(_Route)) _Route = $"AND ANDROID_ROUTE.ROUTEID='{_Route}'"; // _Route = '901'

        //        string sql = $@"
        //            SELECT  '0' AS [CHECK],
        //                    INVENTTRANSFERJOUR.TRANSFERID AS VOUCHERID,INVENTTRANSFERJOUR.INVENTLOCATIONIDFROM,
        //              CONVERT(VARCHAR,INVENTTRANSFERJOUR.CREATEDDATETIME,23) AS DATEDOCNO,  
        //              CONVERT(VARCHAR,DATEADD(HOUR,+7, INVENTTRANSFERJOUR.CREATEDDATETIME),24) AS TIMEDOCNO,   
        //              SHOP_BRANCH.BRANCH_ID,SHOP_BRANCH.BRANCH_NAME AS BRANCHNAME,   
        //              Android_Route.ROUTEID,
        //              ANDROID_ROUTE.NAME AS ROUTENAME,  
        //              ISNULL(Shop_TypeStatus.Shop_statypedesc,'รอจัดส่งสินค้า')  AS STATUSDOCNO,
        //              ISNULL(CountPrint,0) AS COUNTPRINT,
        //              ROW_NUMBER( ) OVER(PARTITION BY INVENTTRANSFERJOUR.TRANSFERID ORDER BY LINENUM ASC) AS LINENUM,
        //              INVENTTRANSFERJOURLINE.SPC_ITEMNAME AS SPC_ITEMNAME,
        //              CAST(ROUND(INVENTTRANSFERJOURLINE.SPC_QTYSHIPPEDUNIT,2) AS decimal(18,2)) AS QTY,INVENTTRANSFERJOURLINE.SPC_ITEMBARCODEUNIT,
        //                    UPDATEDBY AS CASHIERID,
        //		SPC_NAME AS CASHIERNAME,
        //                    SPC_ITEMBARCODE AS ITEMBARCODE,
        //		COUNT(BOXGROUP) AS BOXGROUP

        //            FROM	SHOP2013TMP.dbo.INVENTTRANSFERJOUR WITH (NOLOCK)  
        //              INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERJOURLINE WITH(NOLOCK) ON INVENTTRANSFERJOUR.VOUCHERID = INVENTTRANSFERJOURLINE.VOUCHERID
        //                    INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) ON INVENTTRANSFERJOUR.UPDATEDBY = EMPLTABLE.EMPLID
        //              INNER JOIN  (
        //               SELECT	ACCOUNTNUM,SPC_ROUTETABLE.ROUTEID,SPC_ROUTETABLE.NAME	
        //               FROM	(
        //	                SELECT	ACCOUNTNUM,ROUTEID,ROW_NUMBER( ) OVER(PARTITION BY ACCOUNTNUM ORDER BY ROUTEID ) AS SEQ
        //	                FROM	[SHOP2013TMP].[dbo].[SPC_ROUTINGPOLICY] WITH (NOLOCK)
        //	                WHERE	ACCOUNTNUM LIKE 'MN%'
        //			                AND ISPRIMARY = '1' AND [SPC_ROUTINGPOLICY].DATAAREAID = N'SPC'		
        //	                )TMP INNER JOIN SHOP2013TMP.dbo.SPC_ROUTETABLE WITH (NOLOCK) ON TMP.ROUTEID = SPC_ROUTETABLE.ROUTEID
        //               WHERE	SEQ = 1 AND SPC_ROUTETABLE.DATAAREAID = N'SPC'
        //               )ANDROID_ROUTE  ON INVENTTRANSFERJOUR.INVENTLOCATIONIDTO = ANDROID_ROUTE.ACCOUNTNUM   
        //              INNER JOIN  SHOP_BRANCH   WITH (NOLOCK) ON SHOP_BRANCH.BRANCH_ID = ANDROID_ROUTE.ACCOUNTNUM     
        //              LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_SHIPMENTLINE WITH (NOLOCK)  ON INVENTTRANSFERJOUR.TRANSFERID = SPC_SHIPMENTLINE.DOCUMENTNUM 
        //              LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_SHIPMENTTABLE WITH (NOLOCK)  ON SPC_SHIPMENTTABLE.SHIPMENTID = SPC_SHIPMENTLINE.SHIPMENTID  
        //              LEFT OUTER JOIN 
        //               (SELECT Shop_Desc,Shop_Table,Shop_StaType,Shop_StatypeDesc 
        //               FROM    Shop_TypeStatus WITH (NOLOCK) 
        //               WHERE   SHOP_TYPE = 'SH')Shop_TypeStatus ON SPC_SHIPMENTTABLE.SHIPMENTSTATUS = Shop_TypeStatus.Shop_StaType
        //              LEFT OUTER JOIN ANDROID_COUNTPRINT WITH (NOLOCK) ON INVENTTRANSFERJOUR.VOUCHERID = ANDROID_COUNTPRINT.DocNo AND  Type = 'Bill'
        //		INNER JOIN SHOP2013TMP.dbo.SPC_BOXFACESHEET WITH(NOLOCK) ON INVENTTRANSFERJOURLINE.VOUCHERID = SPC_BOXFACESHEET.DOCUMENTNUM 

        //            WHERE 	CONVERT(VARCHAR, DATEADD(HOUR,+7,INVENTTRANSFERJOUR.CREATEDDATETIME),23) = '{_DateDoc}' 
        //              AND SPC_REMARKS ='{_Dept}'
        //                    AND INVENTTRANSFERJOUR.DATAAREAID = N'SPC'
        //		AND SPC_BOXFACESHEET.DATAAREAID = N'SPC'
        //		AND EMPLTABLE.DATAAREAID = N'SPC'
        //		AND INVENTTRANSFERJOURLINE.DATAAREAID = N'SPC'
        //                    {_Branch} {_Route}
        //GROUP BY INVENTTRANSFERJOUR.TRANSFERID, INVENTTRANSFERJOUR.INVENTLOCATIONIDFROM, 
        //		 INVENTTRANSFERJOUR.CREATEDDATETIME,SHOP_BRANCH.BRANCH_ID,
        //		 SHOP_BRANCH.BRANCH_NAME,Android_Route.ROUTEID,
        //		 ANDROID_ROUTE.NAME,Shop_TypeStatus.Shop_statypedesc,
        //		 ANDROID_COUNTPRINT.CountPrint,INVENTTRANSFERJOURLINE.LINENUM,
        //		 INVENTTRANSFERJOURLINE.SPC_ITEMNAME,INVENTTRANSFERJOURLINE.SPC_QTYSHIPPEDUNIT,
        //		 INVENTTRANSFERJOURLINE.SPC_ITEMBARCODEUNIT,INVENTTRANSFERJOUR.UPDATEDBY,
        //		 EMPLTABLE.SPC_NAME,INVENTTRANSFERJOURLINE.SPC_ITEMBARCODE
        //            ORDER BY INVENTTRANSFERJOUR.CREATEDDATETIME,CONVERT(VARCHAR,DATEADD(HOUR,+7, INVENTTRANSFERJOUR.CREATEDDATETIME),24) DESC
        //        ";

        //        return ConnectionClass.SelectSQL_Main(sql);

        //    }
        #endregion

        //#region BoxGroup
        //internal static DataTable BoxGroup(string _Doc)
        //{
        //    string sql;

        //    sql = $@"
        //            SELECT  BOXGROUP
        //            FROM    SHOP2013TMP.dbo.SPC_BOXFACESHEET  WITH (NOLOCK)   
        //            WHERE   DOCUMENTNUM = '{_Doc}'
        //        ";

        //    return ConnectionClass.SelectSQL_Main(sql);
        //}
        //#endregion

        #region UpdateCountPrint
        internal static string InsertAndroid_CountPrint(string _docNo, int _countPrint)
        {
            string sql;
            if (_countPrint == 0)
            {
                sql = $@"
                       INSERT INTO  ANDROID_COUNTPRINT
                                    (DocNo, CountPrint,Type, Dept)
                       VALUES       ( '{_docNo}', '1','Bill', 'D204')";
            }
            else
            {
                sql = $@" UPDATE    ANDROID_COUNTPRINT
                          SET       CountPrint = CountPrint + 1
                          WHERE     DocNo = '{_docNo}'";
            }
            return ConnectionClass.ExecuteSQL_Main(sql);
        }
        #endregion

        public class BILLTRANS
        {
            public string DOCNO { get; set; }
            public string BRANCHID { get; set; }
            public string ROUTE { get; set; }
            public string DEPT { get; set; }
            public string TYPEBILL { get; set; }
            public string DATEDOC { get; set; }
            public string TIMEDOC { get; set; }
        }

        public class BILLPRINT
        {
            //voucher, inventlocationid, datedoc, timedoc, branchid, branchname, itembarcode, cashierid, cashiername
            public string VOUCHER { get; set; }

            public string INVENTLOCATIONID { get; set; }

            public string DATEDOC { get; set; }

            public string TIMEDOC { get; set; }

            public string BRANCHID { get; set; }

            public string BRANCHNAME { get; set; }

            public string ROUTEID { get; set; }

            public string ROUTENAME { get; set; }

            public string CASHIERID { get; set; }

            public string CASHIERNAME { get; set; }
        }


    }
}
