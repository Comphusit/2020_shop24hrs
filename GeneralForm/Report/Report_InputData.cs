﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.Report
{
    public partial class Report_InputData : Telerik.WinControls.UI.RadForm
    {
        public string pInputData;
        public string pInputRemark;
        public string pEmplID;
        public string pEmplName;

        readonly string sTANumberOtText; //1 ต้องการเปนตัวเลข 0 ต้องการเป็นข้อความ

        public Report_InputData(string sSTA_1Number_0Text, string sShowData, string sShowInput, string sShowUnit)
        {
            InitializeComponent();
            radLabel_Show.Text = sShowData;
            radLabel_Input.Text = sShowInput + " [Enter]";
            radLabel_Unit.Text = sShowUnit;
            sTANumberOtText = sSTA_1Number_0Text;
        }
        //load
        private void Report_InputData_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radTextBox_Remaek.Enabled = false;
            textBox_EmplID.Enabled = false;
            radButton_Save.Enabled = false;

            radTextBox_Input.Text = pInputData;
            radTextBox_Remaek.Text = pInputRemark;
            textBox_EmplID.Text = "";
            radLabel_EmplName.Text = "";

            pEmplID = ""; pEmplName = "";

            if (sTANumberOtText == "0")
            {
                radTextBox_Input.Multiline = true;
                radTextBox_Input.Size = new System.Drawing.Size(359, 63);
            }
            else
            {
                radTextBox_Input.Multiline = false;
                radTextBox_Input.Size = new System.Drawing.Size(286, 34);
            }
            radTextBox_Input.SelectAll();
            radTextBox_Input.Focus();
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            EnterData();
        }

        void EnterData()
        {
            pInputData = radTextBox_Input.Text.Trim();
            pInputRemark = radTextBox_Remaek.Text;


            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Enter
        private void RadTextBox_Input_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:

                    switch (sTANumberOtText)
                    {
                        case "1":
                            try
                            {
                                double i = Convert.ToDouble(radTextBox_Input.Text.Trim());
                                if (i == 0) if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการระบุข้อมูลที่เป็น 0 ?.") == DialogResult.No) return;
                            }
                            catch (Exception)
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุข้อมูลเป็นตัวเลขเท่านั้น ลองใหม่อีกครั้ง.");
                                radTextBox_Input.Focus();
                                return;
                            }
                            break;

                        case "0":
                            if (radTextBox_Input.Text.Trim() == "")
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning("ต้องระบุข้อมูลก่อนการตกลงให้เรียบร้อย ลองใหม่อีกครั้ง.");
                                radTextBox_Input.Focus();
                                return;
                            }
                            break;
                        default:
                            return;
                    }

                    pInputData = radTextBox_Input.Text;

                    radTextBox_Input.Enabled = false;
                    radTextBox_Remaek.Enabled = true;
                    textBox_EmplID.Enabled = true;
                    textBox_EmplID.Focus();
                    break;
                default:
                    break;
            }
        }

        private void TextBox_EmplID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //เชค รหัสพนักงาน
                if (string.IsNullOrEmpty(textBox_EmplID.Text))
                {
                    MsgBoxClass.MsgBoxShow_InputDataBeforeEnter("รหัสพนักงาน");
                    textBox_EmplID.SelectAll();
                    return;
                }
                var dtEmp = Class.Models.EmplClass.GetEmployee_Altnum(textBox_EmplID.Text);
                if (dtEmp.Rows.Count == 0)
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("รหัสพนักงาน");
                    textBox_EmplID.SelectAll();
                    return;
                }
                pEmplID = dtEmp.Rows[0]["ALTNUM"].ToString();
                pEmplName = dtEmp.Rows[0]["SPC_NAME"].ToString();
                radLabel_EmplName.Text = pEmplName;
                textBox_EmplID.Enabled = false;
                radButton_Save.Enabled = true;
                radButton_Save.Focus();
            }
        }

        private void TextBox_EmplID_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void RadTextBox_Input_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (sTANumberOtText == "1") if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }
    }
}
