﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.Report
{
    public partial class Report_CheckVDOWebCam : Telerik.WinControls.UI.RadForm
    {
        readonly string _pTableName;
        readonly string _pPathVDO;
        readonly string _pDocno;
        readonly string _pCash;
        readonly string _pStaCheck;
        readonly string _pRemark;
        readonly string _pDateTime;
        readonly string _CstName;

        public string sRemark;

        public Report_CheckVDOWebCam(string pTableName, string pPathVDO, string pDocno, string pCash, string pStaCheck, string pRemark, string pDateTime,string CstName)
        {
            InitializeComponent();
            _pTableName = pTableName;
            _pPathVDO = pPathVDO;
            _pDocno = pDocno;
            _pCash = pCash;
            _pStaCheck = pStaCheck;
            _pRemark = pRemark;
            _pDateTime = pDateTime;
            _CstName = CstName;
        }
        //Load
        private void Report_CheckVDOWebCam_Load(object sender, EventArgs e)
        {
            radCheckBox_OK.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Cancle.ButtonElement.ShowBorder = true;
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินค้า", 300));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALESPRICE", "ราคา/หน่วย", 135));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 100));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALESUNIT", "หน่วย", 100));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "ราคารวม", 150));
            RadGridView_ShowDT.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter("T4UTOPUPSONUMBER", "ประเภทการป้อน", 130));

            radLabel_Docno.Text = _pDocno;
            radLabel_cashID.Text = _pCash;
            radLabel_DateTime.Text = _pDateTime;
            radLabel_Cst.Text = _CstName;

            axWindowsMediaPlayer_Show.URL = _pPathVDO;


            if (_pTableName == "") splitContainer2.Panel2.Enabled = false;
            if (SystemClass.SystemBranchID != "MN000") splitContainer2.Panel2.Enabled = false;

            radCheckBox_OK.Checked = false;
            radTextBox_Remark.Enabled = true;
            radTextBox_Remark.Focus();

            if (_pStaCheck == "1")
            {
                radCheckBox_OK.Enabled = false;
                radTextBox_Remark.Enabled = false; radTextBox_Remark.Text = _pRemark;
                RadButton_Save.Enabled = false;
            }

            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            RadGridView_ShowDT.DataSource = PosSaleClass.GetDetail_XXXPOSLINE(_pDocno);
        }


        #region "ROWS DGV"

        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _pTableName);
        }
        //close
        private void RadButton_Cancle_Click(object sender, EventArgs e)
        {
            //axWindowsMediaPlayer_Show.Ctlcontrols.stop();
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        //change sta
        private void RadCheckBox_OK_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_OK.Checked == true) radTextBox_Remark.Enabled = false;
            else
            {
                radTextBox_Remark.Enabled = true;
                radTextBox_Remark.Focus();
            }
        }
        //Save Check
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            sRemark = radTextBox_Remark.Text.Trim();
            switch (_pTableName)
            {
                case "SHOP_BARCODEDISCOUNT":
                    if ((radCheckBox_OK.Checked == false) && (sRemark == ""))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ในกรณีบิลไม่เรียบร้อยให้ระบุเหตุผลก่อนบันทึกทุกครั้ง{Environment.NewLine}ลองใหม่อีกครั้ง");
                        radTextBox_Remark.Focus();
                        return;
                    }
                    string sqlUp = $@"
                        UPDATE  SHOP_BARCODEDISCOUNT
                        SET     CHECK_STA = '1',CHECK_REMARK = '{sRemark}',CHECK_WHOID = '{SystemClass.SystemUserID}',
                                CHECK_WHONAME = '{SystemClass.SystemUserName}',CHECK_DATE = GETDATE() 
                        WHERE   XXX_INVOICEID = '{_pDocno}' ";
                    string resualt = ConnectionClass.ExecuteSQL_Main(sqlUp);
                    if (resualt == "")
                    {
                        //axWindowsMediaPlayer_Show.Ctlcontrols.stop();
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        //axWindowsMediaPlayer_Show.Ctlcontrols.stop();
                        MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
                        return;
                    }
                    break;
                default:
                    break;
            }
        }

        private void D054_ReportCheckVDOWebCam_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            //axWindowsMediaPlayer_Show.Ctlcontrols.stop();
        }
    }
}
