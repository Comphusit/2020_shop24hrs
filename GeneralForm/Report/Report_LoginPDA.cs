﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using System.Data;
using System.Linq;
using Telerik.Windows.Diagrams.Core;
using System.Collections.Generic;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.Report
{
    public partial class Report_LoginPDA : Telerik.WinControls.UI.RadForm
    {
        public enum PDAReport
        {
            Login,              //0
            ReportLogin,        //1
            FileinPDA,          //2
            LogShop24Hrs_SUPC,  //3
            QuantityPDA,         //4
            ErrorLogRunjob,  //5
            CheckReciveBox     //6 ลังที่รับทั้งหมด แต่ยังไม่เข้า AX
        }
        DataTable dt_Data = new DataTable("dt_Data");
        DataTable dtBranch = new DataTable("dtBranch");
        readonly PDAReport _pTypeReport;
        string[] columns;
        const int maxPda = 9;
        public Report_LoginPDA(PDAReport pTypePDA)
        {
            InitializeComponent();
            this.KeyPreview = true;
            _pTypeReport = pTypePDA;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        #region SetFontInRadGridview

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        #region Event
        private void ReportLoginPDA_Load(object sender, EventArgs e)
        {
            dtBranch = BranchClass.GetBranchAll("'1'", "'1'");
            this.SetProperty();
            this.CheckFormErrorLogRunjob();
            this.LoadDataGrid();
            this.GetGridViewSummary();
        }
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            this.ClearDataShow();
        }
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.ClearDataShow();
            this.LoadDataGrid();
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport.ToString());
        }
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.RowIndex <= -1) return;
            if (e.ColumnIndex <= -1) return;

            switch (_pTypeReport.ToString())
            {
                //case "ErrorLogRunjob":
                //    if (Convert.ToInt32(RadGridView_Show.CurrentRow.Cells[e.Column.Name].Value) > 0)
                //        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("คุณต้องการ RunJob [" + e.Column.Name + @"] สำหรับสาขา " + RadGridView_Show.CurrentRow.Cells["ErrorBranchID"].Value.ToString() + @" ใช่หรือไม่.") == DialogResult.Yes)
                //        {
                //            RunJob(e.Column.Name, RadGridView_Show.CurrentRow.Cells["ErrorBranchID"].Value.ToString(), RadGridView_Show.CurrentRow.Cells["ErrorBranchName"].Value.ToString());
                //            dt_Data.Rows[e.RowIndex][e.Column.FieldName] = GetRowLog(e.Column.Name, RadGridView_Show.CurrentRow.Cells["ErrorBranchID"].Value.ToString()).Rows.Count;
                //            dt_Data.AcceptChanges();
                //        }

                //    break;
                case "CheckReciveBox":
                    if (RadGridView_Show.CurrentRow.Cells["LINENUM"].Value.ToString() == "1") return;

                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการ Clear รายการรับลังที่ระบุ " + RadGridView_Show.CurrentRow.Cells["BOXNUMBER"].Value.ToString() + @" ?") == DialogResult.No) return;

                    string upSql = $@"
                            DELETE	Shop_ReciveBOX
                            WHERE	Stadoc_Process = '0' AND Stadoc_AX = '0'
		                            AND ID_Auto = '{RadGridView_Show.CurrentRow.Cells["ID_Auto"].Value}' ";
                    string res = ConnectionClass.ExecuteSQL_Main(upSql);
                    MsgBoxClass.MsgBoxShow_SaveStatus(res);
                    if (res == "")
                    {
                        dt_Data = GetDateReciveBox();
                        dt_Data.AcceptChanges();
                        RadGridView_Show.DataSource = dt_Data;
                    }
                    break;
                default:
                    break;
            }
        }
        private void RadGridView_Show_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (_pTypeReport.Equals("ErrorLogRunjob"))
                if (e.CellElement.ColumnInfo.FieldName.Equals("ErrorDesc") && e.CellElement.RowInfo.Cells["ErrorDesc"].Value != null || e.CellElement.ColumnInfo.FieldName.Equals("RunOrderByWeb") && Convert.ToInt32(e.CellElement.RowInfo.Cells["RunOrderByWeb"].Value) > 30) e.CellElement.BackColor = Color.Red;
                else if (e.CellElement.ColumnInfo.FieldName.Equals("RunOrderByMNPO") && Convert.ToInt32(e.CellElement.RowInfo.Cells["RunOrderByMNPO"].Value) > 30) e.CellElement.BackColor = Color.Red;
                else if (e.CellElement.ColumnInfo.FieldName.Equals("RunReciveBox") && Convert.ToInt32(e.CellElement.RowInfo.Cells["RunReciveBox"].Value) > 0) e.CellElement.BackColor = Color.Red;
                else e.CellElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
        }
        #endregion
        #region Method
        void CheckFormErrorLogRunjob()
        {
            if (!_pTypeReport.ToString().Equals("ErrorLogRunjob"))
            {
                radRadioButton_Report.Visible = false;
                radRadioButton_Detail.Visible = false;
            }
        }
        void SetProperty()
        {
            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_First, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Last, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
        }
        void LoadDataGrid()
        {
            this.Cursor = Cursors.WaitCursor;
            switch (_pTypeReport)
            {
                case PDAReport.Login:
                    {
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEPDA", "วันที่ใน PDA", 100));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATETIME", "วันเวลา", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PDANAME", "ชื่อเครื่อง", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 220));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("VERSION", "VERSION", 100));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERVICE", "SERVICE", 120));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOGINNAME", "ชื่อเข้าใช้งาน", 250));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("IPPDA", "IP", 100));
                        this.GetNewColumn();
                        dt_Data = LoginPDA(radDateTimePicker_First.Value, radDateTimePicker_Last.Value);
                    }
                    break;
                case PDAReport.ReportLogin:
                    {
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 180));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("QTY", "QTY"));
                        this.GetNewColumn();
                        columns = GetColumns(maxPda);
                        AppendNewColumn(dt_Data, columns);  //เพิ่ม new column 
                        dt_Data = LoginPDASummary(GetLoginPda(radDateTimePicker_First.Value, radDateTimePicker_Last.Value), dt_Data);//GetBranchLoginPDA(radDateTimePicker_First.Value, radDateTimePicker_Last.Value),
                        columns.ForEach(colName => SetConditions(string.Format(@"{0} <= QTY AND {1} IS NULL", colName.Substring(colName.Length - 1, 1), colName), colName, true, Color.Red, colName));
                    }
                    break;
                case PDAReport.FileinPDA:
                    {
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATE", "วันที่", 100));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 70));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PDANAME", "ชื่อเครื่อง", 100));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("EXTENSION", "ชื่อไฟล์", 100));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QTY", "จำนวนค้าง", 100));
                        this.GetNewColumn();
                        dt_Data = LogFiles(radDateTimePicker_First.Value, radDateTimePicker_Last.Value);
                    }
                    break;
                case PDAReport.LogShop24Hrs_SUPC:
                    {
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 70));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "แผนก", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOGTYPE", "ประเภท", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESC", "ชื่อประเภท", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOGDATE", "วันเวลา", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOGWHO", "ผู้ใช้", 100));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อผู้ใช้", 200));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("USE", "ชื่อเครื่อง", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TYPE", "ใช้บน", 70));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("LOGROW", "จำนวนรายการ", 100));
                        this.GetNewColumn();
                        dt_Data = GetBranchLogshop_supc(radDateTimePicker_First.Value, radDateTimePicker_Last.Value);
                    }
                    break;
                case PDAReport.QuantityPDA:
                    {
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 100));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อแผนก", 200));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QTY", "จำนวน", 100));
                        this.GetNewColumn();
                        dt_Data = GetBranchPDA();
                        TableLayoutColumnStyleCollection stylesc = this.tableLayoutPanel1.ColumnStyles;
                        foreach (ColumnStyle style in stylesc)
                        {
                            if (style.SizeType == SizeType.Absolute) style.Width = 0;
                        }
                    }
                    break;
                case PDAReport.ErrorLogRunjob:
                    {
                        this.RadGridView_Show.DataSource = null;
                        this.RadGridView_Show.MasterTemplate.Columns.Clear();
                        if (radRadioButton_Report.CheckState == CheckState.Checked)
                        {
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorBranchID", "สาขา", 70));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorBranchName", "ชื่อสาขา", 150));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DateTime", "วันที่", 150));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorSubject", "ErrorSubject", 100));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorRows", "Rows", 70));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorExMail", "ErrorExMail", 300));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorDesc", "ErrorDesc", 400));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorWhoIDIns", "รหัส", 70));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorWhoName", "ชื่อพนักงาน", 200));
                            this.GetNewColumn();
                            dt_Data = GetErrorLogRunjob(radDateTimePicker_First.Value, radDateTimePicker_Last.Value);
                        }
                        else
                        {
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorBranchID", "สาขา", 70));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ErrorBranchName", "ชื่อสาขา", 150));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RunOrderByWeb", "RunOrderByWeb", 150));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RunOrderByMNPO", "RunOrderByMNPO", 150));
                            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("RunReciveBox", "RunReciveBox", 150));
                            this.GetNewColumn();
                            dt_Data = GetErrorLogRow(BranchClass.GetBranchAll("1", "1"));
                        }
                    }
                    break;
                case PDAReport.CheckReciveBox:
                    {
                        radDateTimePicker_First.Visible = false; radDateTimePicker_Last.Visible = false;
                        radLabel_Date.Visible = false;
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ID_Auto", "รหัสอ้างอิง", 80));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BOXBRANCH", "สาขา", 80));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BOXNUMBER", "เลขที่ลัง", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCUMENTNUM", "เลขที่เอกสาร", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MACHINENAME", "เครื่องรับ", 150));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEIN", "วันที่รับ", 110));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TIMEIN", "เวลาที่รับ", 80));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOIN", "ผู้บันทึก", 100));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BOXSPCITEMNAME", "สินค้า", 400));
                        RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("LINENUM", "LINENUM"));

                        ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition", "LINENUM <> '1' ", false)
                        { CellBackColor = ConfigClass.SetColor_Red() };
                        this.RadGridView_Show.Columns["BOXNUMBER"].ConditionalFormattingObjectList.Add(obj1);
                        this.RadGridView_Show.Columns["DOCUMENTNUM"].ConditionalFormattingObjectList.Add(obj1);

                        this.GetNewColumn();
                        dt_Data = GetDateReciveBox();
                    }
                    break;
                default:
                    break;
            }
            dt_Data.AcceptChanges();
            RadGridView_Show.DataSource = dt_Data;
            this.Cursor = Cursors.Default;
        }
        void GetNewColumn()
        {
            List<string> col = new List<string>();
            RadGridView_Show.Columns.ForEach(c => col.Add(c.Name));
            dt_Data = AddNewColumn(col.ToArray());
        }
        private DataTable LoginPDA(DateTime begine, DateTime end, string branchID = "")
        {
            if (!branchID.Equals("")) { branchID = string.Format(@"AND LoginBranch = '{0}'", branchID); }

            string sql = string.Format(@"
                    SELECT      ISNULL(CONVERT(VARCHAR,DATEPDA,23),'') AS DATEPDA,CONVERT(VARCHAR,LoginDate,23)+' '+ Logintime AS [DATETIME], LoginPDAName AS PDANAME,
                                LoginBranch +' : '+ LoginBranchName AS BRANCH_NAME, LoginVersion AS [VERSION], LoginService AS [SERVICE], LoginId+' '+ LoginName AS LOGINNAME,[IP] AS IPPDA 
				    FROM        SHOP_COMPDANAMESHOP24HRS   WITH (NOLOCK) 
                    WHERE       LoginDate BETWEEN '{0:yyyy-MM-dd}' AND '{1:yyyy-MM-dd}' {2}
                    ORDER BY    [DATETIME]  DESC", begine, end, branchID);
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable LoginPDASummary(DataTable branchpda, DataTable dtLogin)// DataTable loginpda,
        {
            var branch = branchpda.AsEnumerable()
                       .GroupBy(r => new { LogBranch = r["LogBranch"] })
                       .Select(g => g.OrderBy(r => r["LogBranch"]).First())
                       .CopyToDataTable();

            foreach (DataRow rowbranch in branch.Rows)
            {
                DataRow[] rows = branchpda.Select(string.Format(@"LogBranch = '{0}'", rowbranch["LogBranch"].ToString()));
                List<string> lists = new List<string>();
                foreach (var row in rows)
                {
                    lists.Add(row["LogMachine"].ToString());
                }
                DataRow dr = dt_Data.NewRow();
                dr["BRANCH_ID"] = rowbranch["LogBranch"];
                dr["BRANCH_NAME"] = rowbranch["BranchName"];
                dr["QTY"] = rows != null ? rows.ToList().Count : 0;

                for (int i = 0; i < Convert.ToInt32(dr["QTY"]); i++)
                {
                    if (i < lists.Count)
                    {
                        dr[columns[i]] = lists[i];
                    }
                }
                dtLogin.Rows.Add(dr);
            }

            return dtLogin;
        }
        private DataTable LogFiles(DateTime begine, DateTime end)
        {
            string sql = string.Format(@"
                        SELECT      CONVERT(VARCHAR, DateCheck,23) AS DATE, PdaBranch AS BRANCH_ID, BRANCH_NAME, PdaName, TypeFile as Extension,QtyFile as Qty
                        FROM        Shop_PDAFileCheck WITH(NOLOCK)  INNER JOIN 
                                    SHOP_BRANCH WITH (NOLOCK) ON Shop_PDAFileCheck.PdaBranch = SHOP_BRANCH.BRANCH_ID
                        WHERE       DateCheck BETWEEN '{0:yyyy-MM-dd}' AND '{1:yyyy-MM-dd}'  Order by PdaBranch,PdaName,TypeFile", begine, end);
            DataTable ret = ConnectionClass.SelectSQL_Main(sql);
            return ret;
        }

        private DataTable GetBranchPDA()
        {
            DataTable dtGetLoginPda = GetLoginPda(DateTime.Today.AddDays(-30), DateTime.Today);
            var branch = dtGetLoginPda.AsEnumerable()
                       .GroupBy(r => new { LogBranch = r["LogBranch"] })
                       .Select(g => g.OrderBy(r => r["LogBranch"]).First())
                       .CopyToDataTable();

            DataTable dtBranchPDA = dt_Data;
            foreach (DataRow rowbranch in branch.Rows)
            {
                DataRow[] rows = dtGetLoginPda.Select(string.Format(@"LogBranch = '{0}'", rowbranch["LogBranch"].ToString()));
                DataRow dr = dt_Data.NewRow();
                dr["BRANCH_ID"] = rowbranch["LogBranch"];
                dr["BRANCH_NAME"] = rowbranch["BranchName"];
                dr["QTY"] = rows != null ? rows.ToList().Count : 0;
                dtBranchPDA.Rows.Add(dr);
            }
            return dtBranchPDA;
        }

        private DataTable GetBranchLogshop_supc(DateTime first, DateTime last)
        {
            string sql = string.Format(@" 
                            SELECT  [LogBranch] AS BRANCH_ID
                                      ,[LogBranchName] AS BRANCH_NAME
                                      ,[LogDate]
                                      ,[LogType] AS LOGTYPE,ISNULL(Shop_Desc,'') AS [DESC]
                                      ,[LogWhoID] AS LOGWHO
                                      ,[LogWhoIDName] AS SPC_NAME
                                      ,[LogRow]
                                      ,[LogMachine] AS [USE]
	                                  , CASE WHEN LogTmp IS NULL THEN 'PC' ELSE 'PDA' END AS TYPE
                            FROM        [SHOP_LOGSHOP24HRS] LEFT OUTER JOIN
                                                  (SELECT Shop_Desc, Shop_Table FROM  Shop_TypeStatus WHERE  Shop_Type = 'Log')   Shop_TypeStatus ON Shop_LogShop24Hrs.LogType = Shop_TypeStatus.Shop_Table LEFT OUTER JOIN
                                                    SHOP_BRANCH WITH(NOLOCK) ON Shop_LogShop24Hrs.LogBranch = SHOP_BRANCH.BRANCH_ID LEFT OUTER JOIN
                                                    SHOP2013TMP.dbo.DIMENSIONS WITH (NOLOCK) ON Shop_LogShop24Hrs.LogBranch = DIMENSIONS.NUM

                            WHERE       convert(varchar,[LogDate],23) BETWEEN '{0:yyyy-MM-dd}' AND '{1:yyyy-MM-dd}'
                            GROUP BY    [LogBranch],[LogBranchName],[LogDate],[LogType],[LogWhoID],[LogWhoIDName],[LogMachine],Shop_Desc,[LogRow],LogTmp ", first, last);

            return ConnectionClass.SelectSQL_Main(sql);
        }

        DataTable GetDateReciveBox()
        {
            string sql = $@"
                SELECT	ID_Auto,BOXBRANCH,BOXNUMBER,DOCUMENTNUM,MACHINENAME,CONVERT(VARCHAR,DATEIN,23) AS DATEIN,TIMEIN,WHOIN,BOXSPCITEMNAME,
                        ROW_NUMBER( ) OVER(PARTITION BY BOXNUMBER ORDER BY BOXNUMBER ASC) AS LINENUM 
                FROM	Shop_ReciveBOX WITH (NOLOCK) 
                WHERE	Stadoc_Process = '0' AND Stadoc_AX = '0'		
                ORDER BY BOXNUMBER,DOCUMENTNUM
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetErrorLogRunjob(DateTime first, DateTime last)
        {
            string sql = string.Format(@"
                    SELECT [DATEINS]
						  ,[ErrorSubject]
						  ,[ErrorRows]
						  ,[ErrorExMail]
						  ,[ErrorDesc]
						  ,[ErrorBranchID]
						  ,[ErrorBranchName]
						  ,[ErrorWhoIDIns]
						  ,[ErrorWhoName]
                    FROM    SHOP_LOGERROR WITH (NOLOCK)
                    WHERE   CONVERT(VARCHAR,DATEINS,23) 
		                    BETWEEN '{0:yyyy-MM-dd}' AND '{1:yyyy-MM-dd}'
                    ORDER BY DATEINS DESC", first, last);
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            DataTable _dt_Data = dt_Data;
            var result = from rows in dt.AsEnumerable()
                         group rows by new { ErrorSubject = rows["ErrorSubject"], ErrorBranchID = rows["ErrorBranchID"] } into grp
                         select grp.FirstOrDefault();

            foreach (var row in result)
            {
                DataRow dr = _dt_Data.NewRow();
                dr["ErrorBranchID"] = row["ErrorBranchID"].ToString();
                dr["ErrorSubject"] = row["ErrorSubject"].ToString();
                DataTable dtErrorlog = GetRowLog(dr["ErrorSubject"].ToString(), dr["ErrorBranchID"].ToString());
                if (dtErrorlog.Rows.Count > 0)
                {
                    dr["ErrorExMail"] = row["ErrorExMail"].ToString();
                    dr["ErrorDesc"] = row["ErrorDesc"].ToString();
                    dr["ErrorBranchName"] = row["ErrorBranchName"].ToString();
                    dr["ErrorWhoIDIns"] = row["ErrorWhoIDIns"].ToString();
                    dr["ErrorWhoName"] = row["ErrorWhoName"].ToString();
                    dr["DateTime"] = row["DATEINS"].ToString();
                    dr["ErrorRows"] = dtErrorlog.Rows.Count;
                    _dt_Data.Rows.Add(dr);
                }
            }
            return _dt_Data;
        }
        private DataTable GetErrorLogRow(DataTable tablebranch)
        {
            DataTable _dt_Data = dt_Data;
            string[] error = { "RunOrderByWeb", "RunOrderByMNPO", "RunReciveBox" };
            foreach (DataRow row in tablebranch.Rows)
            {
                DataRow dr = _dt_Data.NewRow();
                dr["ErrorBranchID"] = row["BRANCH_ID"].ToString();
                dr["ErrorBranchName"] = row["BRANCH_NAME"].ToString();
                foreach (var er in error)
                {
                    dr[er] = GetRowLog(er, dr["ErrorBranchID"].ToString()).Rows.Count;
                }
                _dt_Data.Rows.Add(dr);
            }
            return _dt_Data;
        }
        private DataTable GetRowLog(string _er, string branch)
        {
            switch (_er)
            {
                case "RunOrderByWeb":
                    _er = @"0";
                    break;
                case "RunOrderByMNPO":
                    _er = @"1";
                    break;
                case "RunReciveBox":
                    _er = @"2";
                    break;
            }
            return ConnectionClass.SelectSQL_Main(@"RunJob_FindData '" + _er + @"','" + branch + @"'");
        }
        DataTable GetLoginPda(DateTime first, DateTime last)
        {
            string sql = string.Format(@"
                SELECT		[LogBranch],
			                CASE
			                WHEN [LogBranchName] = '' THEN BRANCH_NAME 
			                ELSE [LogBranchName]
			                END AS BranchName
			                ,[LogMachine]
                FROM		[SHOP24HRS].[dbo].[SHOP_LOGSHOP24HRS]  left join 
                            SHOP_BRANCH on [SHOP_LOGSHOP24HRS].LogBranch = SHOP_BRANCH.BRANCH_ID
                where		[LogMachine] like 'PDA%'
			                and CAST([LogDate] AS date) between '{0}' and '{1}'
                group by	[LogBranch]
			                ,[LogBranchName]
			                ,[LogMachine]
                            ,BRANCH_NAME
                ", first.ToString("yyyy-MM-dd"), last.ToString("yyyy-MM-dd"));
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable AddNewColumn(string[] columnNames, string[] primaryKeys = null, string tableName = "newTable")
        {
            DataTable newColumn = new DataTable(tableName);
            if (columnNames == null)
            {
                return newColumn;
            }
            foreach (var columnName in columnNames.Distinct())
            {
                newColumn.Columns.Add(columnName, typeof(string));
            }

            if (primaryKeys != null && primaryKeys.Length > 0)
            {
                var colPks = new DataColumn[primaryKeys.Length];
                for (var i = 0; i < primaryKeys.Length; i++)
                {
                    colPks[i] = newColumn.Columns[primaryKeys[i]];
                }
            }
            return newColumn;
        }
        private string[] GetColumns(int count)//, string[] columnName = null
        {
            string[] columnName = new string[count];
            for (int i = 0; i < count; i++)
            {
                columnName[i] = "DEVICE" + (i + 1);
            }
            return columnName;
        }
        private void AppendNewColumn(DataTable table, string[] colNames)
        {
            colNames.ForEach(colName => table.Columns.Add(new DataColumn(colName, typeof(string))));
            colNames.ForEach(colNameGrid =>
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetCenter(colNameGrid, colNameGrid.Replace("DEVICE", "เครื่องที่ "), 150)));
        }
        private void GetGridViewSummary()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem("BRANCH_NAME", "ทั้งหมด : {0}", GridAggregateFunction.Count);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(new GridViewSummaryItem[] { summaryItem });
            this.RadGridView_Show.SummaryRowsTop.Add(summaryRowItem);
        }
        private void SetConditions(string Expression, string ColName, bool applyToRow, Color color, string array)
        {
            ExpressionFormattingObject obj1 = new ExpressionFormattingObject(ColName, Expression, applyToRow)
            {
                CellBackColor = color
            };
            this.RadGridView_Show.Columns[array].ConditionalFormattingObjectList.Add(obj1);
        }
        private void ClearDataShow()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            if (RadGridView_Show.MasterTemplate.Columns.Count > 0) { RadGridView_Show.MasterTemplate.Columns.Clear(); }
            RadButton_Search.Focus();
        }
        //private void RunJob(string job, string branch, string branchname)
        //{
        //    switch (job)
        //    {
        //        case "RunOrderByWeb":
        //            RunJOBClass.RunOrderByWeb(branch, branchname);
        //            break;
        //        case "RunOrderByMNPO":
        //            //RunJOBClass.RunOrderByMNPO("SHOP_MNPO_HD", branch, branchname);
        //            break;
        //        case "RunReciveBox":
        //            RunJOBClass.RunReciveBox(branch, branchname);
        //            break;
        //        default:
        //            break;
        //    }
        //}

        #endregion



    }
}
