﻿namespace PC_Shop24Hrs.GeneralForm.Report
{
    partial class Report_InputData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report_InputData));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Input = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Show = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Unit = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Input = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_Remaek = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.textBox_EmplID = new System.Windows.Forms.TextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_EmplName = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remaek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(105, 315);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 3;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(201, 315);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 4;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radLabel_Input
            // 
            this.radLabel_Input.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Input.Location = new System.Drawing.Point(23, 66);
            this.radLabel_Input.Name = "radLabel_Input";
            this.radLabel_Input.Size = new System.Drawing.Size(76, 19);
            this.radLabel_Input.TabIndex = 24;
            this.radLabel_Input.Text = "LB_INPUT";
            // 
            // radLabel_Show
            // 
            this.radLabel_Show.AutoSize = false;
            this.radLabel_Show.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Show.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Show.Location = new System.Drawing.Point(24, 7);
            this.radLabel_Show.Name = "radLabel_Show";
            this.radLabel_Show.Size = new System.Drawing.Size(364, 53);
            this.radLabel_Show.TabIndex = 25;
            this.radLabel_Show.Text = "LB_SHOW";
            // 
            // radLabel_Unit
            // 
            this.radLabel_Unit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Unit.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Unit.Location = new System.Drawing.Point(316, 96);
            this.radLabel_Unit.Name = "radLabel_Unit";
            this.radLabel_Unit.Size = new System.Drawing.Size(67, 19);
            this.radLabel_Unit.TabIndex = 26;
            this.radLabel_Unit.Text = "LB_UNIT";
            // 
            // radTextBox_Input
            // 
            this.radTextBox_Input.AcceptsReturn = true;
            this.radTextBox_Input.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Input.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Input.Location = new System.Drawing.Point(24, 91);
            this.radTextBox_Input.Multiline = true;
            this.radTextBox_Input.Name = "radTextBox_Input";
            // 
            // 
            // 
            this.radTextBox_Input.RootElement.StretchVertically = true;
            this.radTextBox_Input.Size = new System.Drawing.Size(359, 63);
            this.radTextBox_Input.TabIndex = 0;
            this.radTextBox_Input.Tag = "";
            this.radTextBox_Input.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Input_KeyDown);
            this.radTextBox_Input.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Input_KeyPress);
            // 
            // radTextBox_Remaek
            // 
            this.radTextBox_Remaek.AcceptsReturn = true;
            this.radTextBox_Remaek.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Remaek.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remaek.Location = new System.Drawing.Point(23, 179);
            this.radTextBox_Remaek.Multiline = true;
            this.radTextBox_Remaek.Name = "radTextBox_Remaek";
            // 
            // 
            // 
            this.radTextBox_Remaek.RootElement.StretchVertically = true;
            this.radTextBox_Remaek.Size = new System.Drawing.Size(359, 63);
            this.radTextBox_Remaek.TabIndex = 1;
            this.radTextBox_Remaek.Tag = "";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(23, 158);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 19);
            this.radLabel1.TabIndex = 28;
            this.radLabel1.Text = "หมายเหตุ";
            // 
            // textBox_EmplID
            // 
            this.textBox_EmplID.BackColor = System.Drawing.Color.White;
            this.textBox_EmplID.Font = new System.Drawing.Font("Tahoma", 12F);
            this.textBox_EmplID.ForeColor = System.Drawing.Color.Blue;
            this.textBox_EmplID.Location = new System.Drawing.Point(24, 268);
            this.textBox_EmplID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox_EmplID.MaxLength = 7;
            this.textBox_EmplID.Name = "textBox_EmplID";
            this.textBox_EmplID.Size = new System.Drawing.Size(86, 27);
            this.textBox_EmplID.TabIndex = 2;
            this.textBox_EmplID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_EmplID_KeyDown);
            this.textBox_EmplID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox_EmplID_KeyPress);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(24, 248);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(106, 19);
            this.radLabel2.TabIndex = 30;
            this.radLabel2.Text = "ผู้บันทึก [Enter]";
            // 
            // radLabel_EmplName
            // 
            this.radLabel_EmplName.AutoSize = false;
            this.radLabel_EmplName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_EmplName.Location = new System.Drawing.Point(116, 273);
            this.radLabel_EmplName.Name = "radLabel_EmplName";
            this.radLabel_EmplName.Size = new System.Drawing.Size(266, 19);
            this.radLabel_EmplName.TabIndex = 31;
            this.radLabel_EmplName.Text = "ผู้บันทึก";
            // 
            // InputData_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(400, 356);
            this.Controls.Add(this.radLabel_EmplName);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.textBox_EmplID);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.radTextBox_Remaek);
            this.Controls.Add(this.radTextBox_Input);
            this.Controls.Add(this.radLabel_Unit);
            this.Controls.Add(this.radLabel_Show);
            this.Controls.Add(this.radLabel_Input);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Report_InputData";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบุข้อมูล";
            this.Load += new System.EventHandler(this.Report_InputData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remaek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_EmplName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel_Input;
        private Telerik.WinControls.UI.RadLabel radLabel_Show;
        private Telerik.WinControls.UI.RadLabel radLabel_Unit;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Input;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remaek;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private System.Windows.Forms.TextBox textBox_EmplID;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_EmplName;
    }
}
