﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;
using Telerik.WinControls.Data;
using System.IO;
using System.Diagnostics;

namespace PC_Shop24Hrs.GeneralForm.Report
{
    public partial class Report_TextComMN : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        readonly string _pPermission;
        // 0 - จัดการ LogAX
        // 1 - ตรวจทะเบียนสินทรัพย์
        // 2 - การใช้คูปองไลน์ ละเอียด
        // 3 - การใช้คูปองไลน์ สรุป
        // 4 - การใช้คูปองไลน์ ควบคุม
        // 5 - monitor pos
        // 6 - สรุปการใช้ PDA รายวัน
        // 7 - รายงาน รับสินค้ามินิมาร์ท [MA]
        // 8 - monitor job

        //Load
        public Report_TextComMN(string pTypeReport, string pPermission)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pPermission = pPermission;
        }
        //Load
        private void Report_TextComMN_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            radLabel_Date.Visible = false;
            radDateTimePicker_D2.Visible = false;
            radDateTimePicker_D1.Visible = false;

            switch (_pTypeReport)
            {
                case "0":
                    radTextBox_Asset.Visible = false; radLabel_Asset.Visible = false;
                    radLabel_Detail.Text = "สีแดง >> มีปัญหาในการส่งข้อมูลเข้า AX | Double Cick ชื่อตาราง >> ลบรายการออก";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("iVALUE", "เลขที่ค้าง", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TABLENAME", "ชื่อตาราง AX", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TBL", "ชื่อตาราง", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("iDESC", "คำอธิบาย", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOG", "LOG", 1000)));
                    RadGridView_ShowHD.Columns["iVALUE"].IsPinned = true;

                    DatagridClass.SetCellBackClolorByExpression("LOG", "LOG <> '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add("TABLENAME", System.ComponentModel.ListSortDirection.Ascending);
                    RadGridView_ShowHD.GroupDescriptors.Add(descriptor);

                    this.GetGridViewSummary0();

                    ClearTxt();
                    SetDGV_HD();
                    break;
                case "1":
                    radTextBox_Asset.Visible = true; radLabel_Asset.Visible = true;
                    radLabel_Date.Visible = false; radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
                    radLabel_Detail.Text = "สีแดง >> ช่องสินทรัพย์ ตรวจจากโปรแกรมเช็คสินทรัพย์ล่าสุด | สีแดง >> วันที่ปรับ CHECKASS ยังไม่เคยตรวจจากโปรแกรมเช็คสินทรัพย์ | DoubleClick >> Image ดูรูปใหญ่";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("AssetId", "หมายเลขสินทรัพย์", 130));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Name", "ชื่อสินทรัพย์", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAMEALIAS", "ชื่อค้นหา", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOCATION", "ที่เก็บ", 70));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LOCATIONMEMO", "บันทึกที่ตั้ง", 250));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SERIALNUM", "S/N", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CONDITION", "สภาพ", 100));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COMMN_DATE", "วันที่ปรับ SHOP24HRS", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("COMMN", "ผู้ปรับ SHOP24HRS", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROMOTION_DATE", "วันที่ปรับ CHECKASS", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROMOTION", "ผู้ปรับ CHECKASS", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("staCheck", "STA"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("PATH", "Image", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PATH_STR", "Image")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PATH_C", "Image")));


                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REFERENCE", "บิลเบิก", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TECHINFO1", "รายละเอียด 1", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TECHINFO2", "รายละเอียด 2", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TECHINFO3", "รายละเอียด 3", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("LEASE", "สถานที่เคยใช้", 300));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MAKE", "ผู้จำหน่าย", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("GUARANTEEDATE", "วันที่รับประกัน", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("MAINTENANCEINFO3", "หมายเหตุ", 200));

                    RadGridView_ShowHD.TableElement.RowHeight = 100;

                    RadGridView_ShowHD.Columns["AssetId"].IsPinned = true;
                    RadGridView_ShowHD.Columns["Name"].IsPinned = true;
                    RadGridView_ShowHD.Columns["NAMEALIAS"].IsPinned = true;
                    RadGridView_ShowHD.Columns["LOCATION"].IsPinned = true;
                    RadGridView_ShowHD.Columns["LOCATIONMEMO"].IsPinned = true;

                    ExpressionFormattingObject obj1_0 = new ExpressionFormattingObject("MyCondition1", "staCheck = '1' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["AssetId"].ConditionalFormattingObjectList.Add(obj1_0);
                    RadGridView_ShowHD.Columns["Name"].ConditionalFormattingObjectList.Add(obj1_0);

                    ExpressionFormattingObject obj1_1 = new ExpressionFormattingObject("MyCondition1", "PROMOTION_DATE = '1900-01-01' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["PROMOTION_DATE"].ConditionalFormattingObjectList.Add(obj1_1);
                    RadGridView_ShowHD.Columns["PROMOTION"].ConditionalFormattingObjectList.Add(obj1_1);

                    ClearTxt();
                    radTextBox_Asset.Focus();
                    break;
                case "2": // คูปองไลน์ ละเอียด
                    radTextBox_Asset.Visible = false; radLabel_Asset.Visible = false;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Date.Visible = true;
                    radLabel_Detail.Text = "แสดงรายการ แยกตามประเภทคูปองที่ใช้";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSNAME", "ชื่อสาขา", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATETIME", "วันที่ซื้อ", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEACCOUNT", "รหัส", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อลูกค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("INVOICEAMOUNT", "ยอดซื้อรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COUPONVALUE", "ยอดส่วนลด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("LineNum", "จน.รายการในบิล", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 600)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("COUPONNUMBER", "COUPONNUMBER")));

                    this.LoadFreightSummary();

                    ClearTxt();
                    SetDGV_HD();
                    break;
                case "3": // คูปองไลน์ สรุป
                    radTextBox_Asset.Visible = false; radLabel_Asset.Visible = false;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Date.Visible = true;
                    radLabel_Detail.Text = "แสดงรายการ แยกตามบาร์โค้ด";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 500)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COUPONVALUE", "ส่วนลด", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวนที่ขายได้", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("AMOUNT", "ส่วนลดรวม", 100)));
                    this.LoadFreightSummaryUse();

                    ClearTxt();
                    SetDGV_HD();
                    break;
                case "4": // คูปองไลน์ ควบคุม
                    radTextBox_Asset.Visible = false; radLabel_Asset.Visible = false;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = false;
                    radLabel_Date.Text = "ระบุเดือนที่ต้องการ"; radLabel_Date.Visible = true;
                    radLabel_Detail.Text = "แสดงรายการ ตามที่แจกลูกค้า";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("YEAR", "ปี", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MONTH", "เดือน", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSITION", "ตำแหน่ง", 70)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSNAME", "ชื่อสาขา", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATETIME", "วันที่ซื้อ", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEACCOUNT", "รหัส", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อลูกค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("INVOICEAMOUNT", "ยอดซื้อรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("COUPONVALUE", "ยอดส่วนลด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("COUPONNUMBER", "COUPONNUMBER")));

                    DatagridClass.SetCellBackClolorByExpression("INVOICEID", "INVOICEID = '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    this.LoadFreightSummaryUse();

                    ClearTxt();
                    SetDGV_HD();
                    break;

                case "5"://pos monitor
                    radTextBox_Asset.Visible = false; radLabel_Asset.Visible = false;
                    radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
                    radLabel_Detail.Text = "สีแดง DB OFFLINE >> การโอนข้อมูลสำหรับระบบออฟไลน์มีปัญหา | สีแดง สถานะ WEBCAM ล่าสุด >> ไม่เจอ WEBCAM | สีแดง SPC >> มี JOB แล้ว >> DoubleClick SPC >> เปิด-แก้ไข-ปิด JOB";
                    radLabel_Asset.Text = "ระบุสาขา [Enter]"; radLabel_Asset.Visible = true;
                    radTextBox_Asset.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STOREID", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONEID", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TERMINAL", $@"หมายเลข{Environment.NewLine}เครื่องขาย", 70)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MACHINE", "ชื่อเครื่องขาย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LOCATION", "ลำดับเครื่อง", 80)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIER", "รหัสแคชเชียร์", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTOFFLINEDB", "DB OFFLINE", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTINVOICEID", "บิลล่าสุด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTINVOICEDATETIME", "เวลาบิลล่าสุด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTRECEIPT", "ขายล่าสุด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTINVOICEIDWEBCAM", "บิลล่าสุดของ WEBCAM", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTWEBCAM", "เวลาล่าสุด WEBCAM", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DD", "สถานะ WEBCAM ล่าสุด", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CLICK", "VDO บิลขาย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SERVER", "SERVER")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DATEBILL", "DATEBILL")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VERSION", "VERSION", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STAJOB", "STAJOB")));

                    RadGridView_ShowHD.MasterTemplate.Columns["STOREID"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["ZONEID"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["TERMINAL"].IsPinned = true;


                    DatagridClass.SetCellBackClolorByExpression("LASTOFFLINEDB", "LASTOFFLINEDB <> '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("LASTWEBCAM", "LASTWEBCAM = '(No device)' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("MACHINE", "STAJOB <> '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    ClearTxt();
                    radTextBox_Asset.Text = "MN"; radTextBox_Asset.Focus();
                    break;
                case "6":
                    radLabel_Date.Visible = true;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Detail.Text = "จำนวนที่แสดง >> จำนวนที่ล็อกอินต่อวัน[ใช้ได้ 2ชม/ครั้งที่ล็อกอิน]";
                    radLabel_Asset.Visible = false; radTextBox_Asset.Visible = false;
                    ClearTxt();
                    break;
                case "7":// รับสินค้ามินิมาร์ท [MA]
                    radTextBox_Asset.Visible = false; radLabel_Asset.Visible = false;
                    radLabel_Date.Text = "วันที่รับสินค้า"; radLabel_Date.Visible = true;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radLabel_Detail.Text = "สีแดง MA >> สาขายังไม่ได้บันทึกรับบิล MA";
                    //                   
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchBill", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BillTrans", "เลขที่ MA", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Docno", "บิลผู้จำหน่าย", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RemarkVender", "รหัสผู้จำหน่าย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("VenderName", "ชื่อผู้จำหน่าย", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Price", "ยอดรวมทั้งบิล", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmpIDReceive", "ผู้รับ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplNameReceive", "ชื่อผู้รับ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RDateIn", "วันที่รับ", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RTimeIn", "เวลาที่รับ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RemarkBill", "หมายเหตุ", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PrintBill", "จำนวนพิมพ์", 100)));
                    DatagridClass.SetCellBackClolorByExpression("BillTrans", "BillTrans = '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    ClearTxt();
                    SetDGV_HD();
                    break;
                case "8":
                    Case8_MonitorJOB("0");
                    break;
                default:
                    break;
            }

        }

        //SetData case 8 For Monitor RunJob 
        void Case8_MonitorJOB(string pCase)
        {
            switch (pCase)
            {
                case "0"://set grid
                    radTextBox_Asset.Visible = false; radLabel_Asset.Visible = false;
                    radLabel_Date.Text = "วันที่รับสินค้า"; radLabel_Date.Visible = true;
                    radDateTimePicker_D1.Visible = false; radDateTimePicker_D2.Visible = false;
                    radLabel_Detail.Text = "สีแดง >> JOB มีปัญหา";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JobName", "ชื่อ JOB", 400)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NextRunDateTime", "เวลาทำงานรอบต่อไป", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastRunDateTime", "เวลาทำงานล่าสุด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastRunStatus", "สถานะการทำงาน", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastRunDuration", "ระยะเวลาที่ทำงาน", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LastRunStatusMessage", "ข้อความอธิบาย", 1000)));

                    ExpressionFormattingObject obj7_0 = new ExpressionFormattingObject("MyCondition1", "LastRunStatus <> 'Succeeded' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["LastRunDateTime"].ConditionalFormattingObjectList.Add(obj7_0);
                    RadGridView_ShowHD.Columns["LastRunStatus"].ConditionalFormattingObjectList.Add(obj7_0);

                    ClearTxt();
                    SetDGV_HD();
                    break;
                case "1":
                    string sql = $@"
                            SELECT	sysjobsteps.database_name,
		                            [sJOB].[name] AS [JobName],
		                            CASE	WHEN [sJOBH].[run_date] IS NULL OR [sJOBH].[run_time] IS NULL THEN NULL
				                            ELSE CAST(
					                            CAST([sJOBH].[run_date] AS CHAR(8))
					                            + ' ' 
					                            + STUFF(
						                            STUFF(RIGHT('000000' + CAST([sJOBH].[run_time] AS VARCHAR(6)),  6)
							                            , 3, 0, ':')
						                            , 6, 0, ':')
					                            AS DATETIME)
		                            END AS [LastRunDateTime], 
		                            CASE [sJOBH].[run_status]
			                            WHEN 0 THEN 'Failed'
			                            WHEN 1 THEN 'Succeeded'
			                            WHEN 2 THEN 'Retry'
			                            WHEN 3 THEN 'Canceled'
			                            WHEN 4 THEN 'Running' -- In Progress
		                            END AS [LastRunStatus],
		                            STUFF(
                                        STUFF(RIGHT('000000' + CAST([sJOBH].[run_duration] AS VARCHAR(6)),  6)
                                            , 3, 0, ':')
                                        , 6, 0, ':')  AS [LastRunDuration],
		                            [sJOBH].[message] AS [LastRunStatusMessage],
		                            CASE [sJOBSCH].[NextRunDate]
			                            WHEN 0 THEN NULL
			                            ELSE CAST(
					                            CAST([sJOBSCH].[NextRunDate] AS CHAR(8))
					                            + ' ' 
					                            + STUFF(
						                            STUFF(RIGHT('000000' + CAST([sJOBSCH].[NextRunTime] AS VARCHAR(6)),  6)
							                            , 3, 0, ':')
						                            , 6, 0, ':')
					                            AS DATETIME)
		                              END AS [NextRunDateTime]
                            FROM 
                                [msdb].[dbo].[sysjobs] (NOLOCK) AS [sJOB] 
	                            INNER JOIN  msdb.dbo.sysjobsteps ON [sJOB].job_id =sysjobsteps.job_id
                                LEFT JOIN (
                                            SELECT
                                                [job_id]
                                                , MIN([next_run_date]) AS [NextRunDate]
                                                , MIN([next_run_time]) AS [NextRunTime]
                                            FROM [msdb].[dbo].[sysjobschedules] (NOLOCK)
                                            GROUP BY [job_id]
                                        ) AS [sJOBSCH] ON [sJOB].[job_id] = [sJOBSCH].[job_id]
                                LEFT JOIN (
                                            SELECT 
                                                [job_id]
                                                , [run_date]
                                                , [run_time]
                                                , [run_status]
                                                , [run_duration]
                                                , [message]
                                                , ROW_NUMBER() OVER (
                                                                        PARTITION BY [job_id] 
                                                                        ORDER BY [run_date] DESC, [run_time] DESC
                                                  ) AS RowNumber
                                            FROM [msdb].[dbo].[sysjobhistory] (NOLOCK)
                                            WHERE [step_id] = 0
                                        ) AS [sJOBH] ON [sJOB].[job_id] = [sJOBH].[job_id] AND [sJOBH].[RowNumber] = 1
                            WHERE	sysjobsteps.database_name = 'SHOP24HRS'
                            ORDER BY CASE	WHEN [sJOBH].[run_date] IS NULL OR [sJOBH].[run_time] IS NULL THEN NULL
				                            ELSE CAST(
					                            CAST([sJOBH].[run_date] AS CHAR(8))
					                            + ' ' 
					                            + STUFF(
						                            STUFF(RIGHT('000000' + CAST([sJOBH].[run_time] AS VARCHAR(6)),  6)
							                            , 3, 0, ':')
						                            , 6, 0, ':')
					                            AS DATETIME)
		                            END DESC
                    ";

                    dt_Data = ConnectionClass.SelectSQL_Main(sql);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }

        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            switch (_pTypeReport)
            {
                case "0":// 
                    string sqlSelect0 = $@"  RUNJOB_FindData '3','' ";

                    dt_Data = ConnectionClass.SelectSQL_Main(sqlSelect0);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "1":
                    if (radTextBox_Asset.Text == "")
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning("กำหนดการระบุกลุ่มสินทรพย์ย่อยทุกครั้งก่อนการค้นหา.");
                        radTextBox_Asset.Focus();
                        this.Cursor = Cursors.Default;
                        return;
                    }

                    //string sqlSelect1 = @"
                    //    SELECT	ASSETID,NAME,NAMEALIAS,LOCATION,SERIALNUM,MAKE,
                    //      CONVERT(VARCHAR,GUARANTEEDATE,23) AS GUARANTEEDATE,TECHINFO1,TECHINFO2,TECHINFO3,
                    //      MAINTENANCEINFO3,REFERENCE,NOTES,LOCATIONMEMO,LEASE,CONDITION   
                    //    FROM    SHOP2013TMP.dbo.ASSETTABLE WITH(NOLOCK)   
                    //    WHERE   DATAAREAID = N'SPC' AND ASSETID LIKE '" + radTextBox_Asset.Text + @"%'
                    //    ORDER   BY ASSETID
                    //";

                    dt_Data = Models.AssetClass.FindAsset_ByAssetID(radTextBox_Asset.Text.Trim());// ConnectionClass.SelectSQL_Main(sqlSelect1);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                    {
                        string assetID = RadGridView_ShowHD.Rows[i].Cells["ASSETID"].Value.ToString();
                        string PROMOTION_DATE = "1900-01-01", PROMOTION = "", PROMOTION_RMK = "", path = PathImageClass.pImageEmply, path_C = "0";
                        //DataTable dtPromotion = CheckByPromotion(assetID);
                        //if (dtPromotion.Rows.Count > 0)
                        //{
                        //    PROMOTION_DATE = dtPromotion.Rows[0]["createdate"].ToString();
                        //    PROMOTION = dtPromotion.Rows[0]["EmpId"].ToString() + Environment.NewLine + dtPromotion.Rows[0]["SPC_NAME"].ToString();
                        //    PROMOTION_RMK = Environment.NewLine + dtPromotion.Rows[0]["CONDITION"].ToString() + "-" + dtPromotion.Rows[0]["LOCATIONMEMOold"].ToString();
                        //    path = dtPromotion.Rows[0]["PATH"].ToString();
                        //    path_C = "1";
                        //}

                        string COMMN_DATE = "1900-01-01", COMMN = "", COMMN_RMK = "";
                        DataTable dtCom = CheckByCom(assetID);
                        if (dtCom.Rows.Count > 0)
                        {
                            COMMN_DATE = dtCom.Rows[0]["UPDATE_DATE"].ToString();
                            COMMN = dtCom.Rows[0]["UPDATE_WHOID"].ToString() + Environment.NewLine + dtCom.Rows[0]["UPDATE_WHONAME"].ToString();
                            COMMN_RMK = Environment.NewLine + dtCom.Rows[0]["REMARK"].ToString();
                        }

                        string staCheck = "0";
                        if (Convert.ToDateTime(PROMOTION_DATE) > Convert.ToDateTime(COMMN_DATE))
                        {
                            staCheck = "1";
                        }

                        RadGridView_ShowHD.Rows[i].Cells["PROMOTION_DATE"].Value = PROMOTION_DATE + PROMOTION_RMK;
                        RadGridView_ShowHD.Rows[i].Cells["PROMOTION"].Value = PROMOTION;
                        RadGridView_ShowHD.Rows[i].Cells["COMMN_DATE"].Value = COMMN_DATE + COMMN_RMK;
                        RadGridView_ShowHD.Rows[i].Cells["COMMN"].Value = COMMN;
                        RadGridView_ShowHD.Rows[i].Cells["staCheck"].Value = staCheck;
                        RadGridView_ShowHD.Rows[i].Cells["PATH_STR"].Value = path;
                        RadGridView_ShowHD.Rows[i].Cells["PATH_C"].Value = path_C;
                        RadGridView_ShowHD.Rows[i].Cells["PATH"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(path);
                    }

                    this.Cursor = Cursors.Default;
                    break;
                case "2":
                    //CASE WHEN ISNULL(SHOP_COUPONONLINE_DT.SPC_ITEMNAME,'') = '' THEN 'ส่วนลดท้ายบิล' ELSE SHOP_COUPONONLINE_DT.SPC_ITEMNAME  END AS TYPEUSE_NAME,
                    //string sqlSelect2 = string.Format(@" 
                    //   SELECT	SHOP_COUPONONLINETRANS.COUPONNUMBER,ISNULL(XXX_POSTABLE.POSGROUP,'') AS POSGROUP,
                    //      CASE ISNULL(XXX_POSTABLE.POSGROUP,'') WHEN '' THEN 'บิลยังไม่โอน' WHEN 'RT' THEN 'สาขาใหญ่' ELSE Shop_Branch.BRANCH_NAME END AS POSNAME,
                    //      CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23) AS INVOICEDATE,CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.CREATEDDATETIME,25) AS CREATEDDATETIME,
                    //      SHOP_COUPONONLINETRANS.INVOICEID AS INVOICEID,
                    //      ISNULL(XXX_POSTABLE.INVOICEACCOUNT,'') AS INVOICEACCOUNT,ISNULL(XXX_POSTABLE.NAME,'') + CASE ISNULL(CUSTGROUP,'') WHEN 'CEMP' THEN ' [พนง.]' ELSE '' END AS NAME,
                    //      ISNULL(CAST(XXX_POSTABLE.INVOICEAMOUNT AS DECIMAL(7,2)),0) AS INVOICEAMOUNT,COUPONVALUE,
                    //      SHOP_COUPONONLINE.REMARK,
                    //      ISNULL(COUNT(XXX_POSLINE.LINENUM),0) AS LineNum
                    //    FROM	SHOP_COUPONONLINETRANS WITH (NOLOCK)
                    //      LEFT OUTER JOIN XXX_POSTABLE WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.INVOICEID = XXX_POSTABLE.INVOICEID
                    //      LEFT OUTER JOIN XXX_POSLINE WITH (NOLOCK) ON XXX_POSTABLE.INVOICEID = XXX_POSLINE.INVOICEID AND XXX_POSLINE.DOCUTYPE = '1'
                    //      LEFT OUTER JOIN SHOP_COUPONONLINE WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.COUPONNUMBER = SHOP_COUPONONLINE.COUPONNUMBER
                    //      LEFT OUTER JOIN Shop_Branch WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = Shop_Branch.BRANCH_ID
                    //            LEFT OUTER JOIN CUSTTABLE WITH (NOLOCK) ON XXX_POSTABLE.INVOICEACCOUNT = CUSTTABLE.ACCOUNTNUM 
                    //            LEFT OUTER JOIN SHOP_COUPONONLINE_DT WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.ITEMBARCODE = SHOP_COUPONONLINE_DT.ITEMBARCODE AND 
                    //       SHOP_COUPONONLINETRANS.COUPONNUMBER = SHOP_COUPONONLINE_DT.COUPONNUMBER

                    //    WHERE	CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23) BETWEEN 
                    //            '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND '" + radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + @"' 

                    //    GROUP BY SHOP_COUPONONLINETRANS.COUPONNUMBER,ISNULL(XXX_POSTABLE.POSGROUP,''),CASE ISNULL(XXX_POSTABLE.POSGROUP,'') WHEN '' THEN 'บิลยังไม่โอน' WHEN 'RT' THEN 'สาขาใหญ่' ELSE Shop_Branch.BRANCH_NAME END ,
                    //      CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23),CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.CREATEDDATETIME,25),
                    //      SHOP_COUPONONLINETRANS.INVOICEID,ISNULL(XXX_POSTABLE.INVOICEACCOUNT,''),ISNULL(XXX_POSTABLE.NAME,''),ISNULL(CAST(XXX_POSTABLE.INVOICEAMOUNT AS DECIMAL(7,2)),0),
                    //      CASE WHEN ISNULL(SHOP_COUPONONLINE_DT.SPC_ITEMNAME,'') = '' THEN 'ส่วนลดท้ายบิล' ELSE SHOP_COUPONONLINE_DT.SPC_ITEMNAME  END,SHOP_COUPONONLINE.REMARK,ISNULL(CUSTGROUP,''),COUPONVALUE

                    //    ORDER BY CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.CREATEDDATETIME,25) 
                    //");

                    dt_Data = PosSaleClass.GetDetailSale_ByLineAdd("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_POSRetail707(sqlSelect2);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "3":
                    //SHOP_COUPONONLINETRANS.ITEMBARCODE,CASE WHEN ISNULL(SHOP_COUPONONLINE_DT.SPC_ITEMNAME,'') = '' THEN 'ส่วนลดท้ายบิล' ELSE SHOP_COUPONONLINE_DT.SPC_ITEMNAME  END AS SPC_ITEMNAME,SHOP_COUPONONLINE_DT.UNITID,
                    //INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION
                    //LEFT OUTER JOIN INVENTITEMBARCODE WITH(NOLOCK) ON SHOP_COUPONONLINETRANS.ITEMBARCODE = INVENTITEMBARCODE.ITEMBARCODE  AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
                    //LEFT OUTER JOIN DIMENSIONS WITH(NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM AND DIMENSIONCODE = '0' AND DIMENSIONS.DATAAREAID = N'SPC'

                    //string sqlSelect3 = string.Format(@" 
                    //   SELECT	
                    //      COUPONVALUE,COUNT(SHOP_COUPONONLINETRANS.ITEMBARCODE) AS QTY,
                    //      COUPONVALUE*COUNT(SHOP_COUPONONLINETRANS.ITEMBARCODE) AS AMOUNT,SHOP_COUPONONLINE.REMARK

                    //    FROM	SHOP_COUPONONLINETRANS WITH (NOLOCK)
                    //      LEFT OUTER JOIN SHOP_COUPONONLINE WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.COUPONNUMBER = SHOP_COUPONONLINE.COUPONNUMBER
                    //      LEFT OUTER JOIN SHOP_COUPONONLINE_DT WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.ITEMBARCODE = SHOP_COUPONONLINE_DT.ITEMBARCODE AND 
                    //       SHOP_COUPONONLINETRANS.COUPONNUMBER = SHOP_COUPONONLINE_DT.COUPONNUMBER

                    //    WHERE	CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23) BETWEEN 
                    //            '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' AND '" + radDateTimePicker_D2.Value.ToString("yyyy-MM-dd") + @"' 

                    //    GROUP BY SHOP_COUPONONLINE.REMARK,COUPONVALUE

                    //    ORDER BY SHOP_COUPONONLINE.REMARK
                    //");

                    dt_Data = PosSaleClass.GetDetailSale_ByLineAdd("1", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_POSRetail707(sqlSelect3);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "4":
                    //string sqlSelect4 = string.Format($@" 
                    //   SELECT	MONTH,YEAR,POSITION,
                    //       SHOP_COUPONONLINE_CUST.COUPONNUMBER,ISNULL(XXX_POSTABLE.POSGROUP,'') AS POSGROUP,
                    //      CASE ISNULL(XXX_POSTABLE.POSGROUP,'') WHEN '' THEN 'บิลยังไม่โอน' WHEN 'RT' THEN 'สาขาใหญ่' ELSE Shop_Branch.BRANCH_NAME END AS POSNAME,
                    //      CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.TRANSDATE,23) AS INVOICEDATE,CONVERT(VARCHAR,SHOP_COUPONONLINETRANS.CREATEDDATETIME,25) AS CREATEDDATETIME,
                    //      ISNULL(SHOP_COUPONONLINETRANS.INVOICEID,'') AS INVOICEID,
                    //      ISNULL(XXX_POSTABLE.INVOICEACCOUNT,'') AS INVOICEACCOUNT,XXX_POSTABLE.NAME,
                    //      ISNULL(CAST(XXX_POSTABLE.INVOICEAMOUNT AS DECIMAL(7,2)),0) AS INVOICEAMOUNT,COUPONVALUE,
                    //      SHOP_COUPONONLINE.REMARK

                    //    FROM	SHOP_COUPONONLINE_CUST WITH (NOLOCK)
                    //      INNER JOIN SHOP_COUPONONLINE WITH (NOLOCK) ON SHOP_COUPONONLINE_CUST.COUPONNUMBER = SHOP_COUPONONLINE.COUPONNUMBER
                    //      LEFT OUTER JOIN SHOP_COUPONONLINETRANS WITH (NOLOCK) 
                    //      ON SHOP_COUPONONLINE_CUST.COUPONNUMBER = SHOP_COUPONONLINETRANS.COUPONNUMBER
                    //       AND SHOP_COUPONONLINE_CUST.CUSTACCOUNT = SHOP_COUPONONLINETRANS.CUSTACCOUNT
                    //      LEFT OUTER JOIN XXX_POSTABLE WITH (NOLOCK) ON SHOP_COUPONONLINETRANS.INVOICEID = XXX_POSTABLE.INVOICEID
                    //      LEFT OUTER JOIN Shop_Branch WITH (NOLOCK) ON XXX_POSTABLE.POSGROUP = Shop_Branch.BRANCH_ID  

                    //    WHERE   MONTH = MONTH('{radDateTimePicker_D1.Value:yyyy-MM-dd}') AND  YEAR = YEAR('{radDateTimePicker_D1.Value:yyyy-MM-dd}') 
                    //            AND SHOP_COUPONONLINE_CUST.CUSTACCOUNT != 'CONTROL'

                    //    ORDER BY ISNULL(SHOP_COUPONONLINETRANS.INVOICEID,'')
                    //");

                    dt_Data = PosSaleClass.GetDetailSale_ByLineAdd("2", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_POSRetail707(sqlSelect4);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "5": // 5 - monitor pos
                    if (radTextBox_Asset.Text == "")
                    {
                        if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"การดึงข้อมูลทั้งระบบอาจจะใช้เวลานาน{Environment.NewLine}ยืนยันการดึงข้อมูล ?") == DialogResult.No)
                        {
                            this.Cursor = Cursors.Default; return;
                        }
                    }

                    //string pPos = " AND POSGROUP LIKE 'MN%' AND POSGROUP NOT IN ('MN098','MN998') AND POSGROUP NOT LIKE '%R'  ";
                    //if (radTextBox_Asset.Text != "")
                    //{
                    //    pPos = $@" AND POSGROUP LIKE '%{radTextBox_Asset.Text}%' AND POSGROUP NOT LIKE '%R' AND POSGROUP NOT IN ('MN098','MN998') ";
                    //}
                    //string sqlSelect5 = $@"
                    //    DECLARE @MAXVERSION AS VARCHAR(20)
                    //    SELECT @MAXVERSION=MAX(POSVERSION) FROM SYSVERSION;
                    //    SELECT
                    //           [STOREID]=T3.POSGROUP
                    //           ,[ZONEID]=T3.ZONEID
                    //           ,[TERMINAL]=T1.TERMINALID
                    //           ,[MACHINE]=T3.MACHINENAME
                    //           ,[LOCATION]=T3.LOCATIONID
                    //           ,[VERSION]=T4.POSVERSION+CASE T4.POSVERSION WHEN @MAXVERSION THEN '(Latest)' ELSE '' END
                    //           ,[CASHIER]=ISNULL(T6.EMPLID,'')
                    //           ,[LASTOFFLINEDB]=CASE WHEN T6.EMPLID IS NOT NULL AND DATEDIFF(DD,T5.LASTUPDATE,GETDATE())>=1 THEN CAST(DATEDIFF(DD,T5.LASTUPDATE,GETDATE()) AS VARCHAR)+' day' ELSE '' END
                    //           ,T1.LASTINVOICEID
                    //           ,T1.LASTINVOICEDATETIME
                    //           ,[LASTRECEIPT]=CASE WHEN ISNULL(T6.EMPLID,'') != '' AND T6.SIGNINDATETIME<T1.LASTINVOICEDATETIME THEN CAST(DATEDIFF(MI,T1.LASTINVOICEDATETIME,GETDATE()) AS VARCHAR)+' minute' ELSE '' END
                    //           ,'0' AS STAJOB 
                    //    FROM
                    //           (
                    //                  SELECT
                    //                         [TERMINALID]=S1.POSNUMBER
                    //                         ,[LASTINVOICEID]=MAX(S1.INVOICEID)
                    //                         ,[LASTINVOICEDATETIME]=MAX(S1.CREATEDDATETIME)
                    //                  FROM
                    //                         XXX_POSTABLE S1 WITH(NOLOCK)
                    //                         INNER JOIN (SELECT POSNUMBER FROM SYSVERSION WITH(NOLOCK) WHERE POSVERSION LIKE N'6%') S2 ON S2.POSNUMBER=S1.POSNUMBER
                    //                  GROUP BY 
                    //                         S1.POSNUMBER
                    //           ) T1
                    //           INNER JOIN POSTABLE T3 WITH(NOLOCK) ON T3.POSNUMBER=T1.TERMINALID AND T3.POSSTATUS=1 {pPos}
                    //           INNER JOIN SYSVERSION T4 WITH(NOLOCK) ON T4.POSNUMBER=T3.POSNUMBER AND T4.MACHINENAME=T3.MACHINENAME
                    //           LEFT JOIN SYSTABLELASTUPDATE T5 WITH(NOLOCK) ON T5.MACHINENAME=T3.MACHINENAME
                    //           LEFT JOIN POSLOGINOPEN T6 WITH(NOLOCK) ON T6.POSNUMBER=T1.TERMINALID AND T6.TRANSSTATUS<>4
                    //    ORDER BY
                    //        T3.POSGROUP,
                    //           T3.LOCATIONID ASC

                    //";

                    string pPos = " AND POSGROUP LIKE 'MN%' AND POSGROUP NOT IN ('MN098','MN998') AND POSGROUP NOT LIKE '%R'  ";
                    if (radTextBox_Asset.Text != "") pPos = $@" AND POSGROUP LIKE '%{radTextBox_Asset.Text}%' AND POSGROUP NOT LIKE '%R' AND POSGROUP NOT IN ('MN098','MN998') ";
                    dt_Data = PosSaleClass.GetDetail_MonitorPOS(pPos); //ConnectionClass.SelectSQL_POSRetail707(sqlSelect5);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    DataTable dtBch = new DataTable();
                    string bchServer = "", serverPathImage = "";
                    for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                    {
                        //CheckConnecttionServer Branch
                        string bchID = RadGridView_ShowHD.Rows[i].Cells["STOREID"].Value.ToString();
                        if (bchServer != bchID)
                        {
                            bchServer = bchID;
                            string sqlBch = $@"
                            SELECT	TERMINALID,[LASTINVOICEID]=MAX(RECEIPTID),[LASTWEBCAMDATETIME]=MAX(CREATEDDATETIME),
                                    SUBSTRING(CONVERT(VARCHAR,MAX(CREATEDDATETIME),23),1,7) + '\' + CONVERT(VARCHAR,MAX(CREATEDDATETIME),23) AS DATEBILL 
                            FROM	RETAILTRANSACTIONWEBCAMTRANS WITH(NOLOCK)
                            GROUP BY	TERMINALID";

                            DataTable dtServer = BranchClass.GetDBBranch(bchID);
                            if (dtServer.Rows.Count > 0)
                            {
                                dtBch = ConnectionClass.SelectSQL_SentServer(sqlBch, dtServer.Rows[0]["SERVER_Connection"].ToString(), 15);
                                serverPathImage = dtServer.Rows[0]["SERVER_PATHVDO"].ToString();
                            }
                        }

                        if (dtBch.Rows.Count > 0)
                        {
                            DataRow[] result = dtBch.Select($@" TERMINALID = '{RadGridView_ShowHD.Rows[i].Cells["TERMINAL"].Value}' ");

                            RadGridView_ShowHD.Rows[i].Cells["SERVER"].Value = serverPathImage;

                            if (result.Length > 0)
                            {
                                RadGridView_ShowHD.Rows[i].Cells["LASTINVOICEIDWEBCAM"].Value = result[0]["LASTINVOICEID"].ToString();
                                RadGridView_ShowHD.Rows[i].Cells["LASTWEBCAM"].Value = result[0]["LASTWEBCAMDATETIME"].ToString();
                                RadGridView_ShowHD.Rows[i].Cells["DATEBILL"].Value = result[0]["DATEBILL"].ToString();

                                DateTime DD1 = Convert.ToDateTime(RadGridView_ShowHD.Rows[i].Cells["LASTINVOICEDATETIME"].Value.ToString());
                                DateTime DD2 = Convert.ToDateTime(result[0]["LASTWEBCAMDATETIME"].ToString());
                                TimeSpan Span = DD1 - DD2;
                                if (DD2 >= DD1) RadGridView_ShowHD.Rows[i].Cells["DD"].Value = "";
                                else RadGridView_ShowHD.Rows[i].Cells["DD"].Value = Span.Days + " วัน : " + Span.Hours + " ชม : " + Span.Minutes + " นาที";

                                RadGridView_ShowHD.Rows[i].Cells["CLICK"].Value = "Click";
                            }
                            else RadGridView_ShowHD.Rows[i].Cells["LASTWEBCAM"].Value = "(No device)";
                        }
                        else RadGridView_ShowHD.Rows[i].Cells["LASTWEBCAM"].Value = "(No device)";

                        //Check JOB
                        DataTable dt_iR = ConnectionClass.SelectSQL_Main(
                            $@"
                                SELECT	*
                                FROM	SHOP_JOBComMinimart WITH(NOLOCK)
                                WHERE	JOB_BRANCHID = '{bchID}' AND JOB_STACLOSE = '0'
		                                AND JOB_SN = '{RadGridView_ShowHD.Rows[i].Cells["MACHINE"].Value}'
		                                AND JOB_IP = 'WEBCAM'
                                ORDER BY JOB_Number DESC
                            ");
                        if (dt_iR.Rows.Count > 0)
                        {
                            RadGridView_ShowHD.Rows[i].Cells["STAJOB"].Value = dt_iR.Rows[0]["JOB_Number"].ToString();
                        }
                    }

                    this.Cursor = Cursors.Default;
                    break;
                case "6":
                    RptCase6();
                    break;
                case "7":
                    string sqlSelect7 = $@" 
                       SELECT	BranchBill,BranchName,BillTrans,Docno,RemarkVender,VenderName,
                                CAST(ISNULL(Price,'0') AS DECIMAL(18,2)) AS Price,PrintBill,EmpIDReceive,EmplNameReceive,
		                        RemarkBill,CONVERT(VARCHAR,RDateIn,23) AS RDateIn,CONVERT(VARCHAR,RTimeIn,24) AS RTimeIn
                        FROM	SHOP_RECIVEDOCUMENT WITH (NOLOCK)
                        WHERE	CONVERT(VARCHAR,RDateIn,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
                        ORDER BY RemarkVender,BranchBill,CONVERT(VARCHAR,RDateIn,25)";

                    dt_Data = ConnectionClass.SelectSQL_Main(sqlSelect7);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "8":
                    Case8_MonitorJOB("1");
                    break;
                default:
                    break;
            }
        }
        //Report Case สรุปการใช้ PDA รายวัน
        void RptCase6()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
            }

            if (dt_Data.Rows.Count == 0)
            {
                dt_Data.Rows.Clear();
                RadGridView_ShowHD.DataSource = dt_Data;
                dt_Data.AcceptChanges();
            }

            //string sql = $@"
            //    SELECT	LoginPDAName,LoginBranch,LoginBranchName,ISNULL(ASSETTABLE.NAME,'CN50/CN51') + '   ['+Condition+']' AS NAME
            //    FROM	SHOP_COMPDANAMESHOP24HRS WITH (NOLOCK)
            //            LEFT OUTER JOIN SHOP2013TMP.dbo.ASSETTABLE WITH (NOLOCK) ON SHOP_COMPDANAMESHOP24HRS.LoginPDAName = ASSETTABLE.ASSETID AND DATAAREAID = N'SPC'
            //    WHERE	LoginDate BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
            //      AND LoginBranch != 'MN000'
            //    GROUP BY LoginPDAName,LoginBranch,LoginBranchName,ASSETTABLE.NAME,Condition
            //    ORDER BY LoginBranch,LoginPDAName
            //";
            dt_Data = Models.AssetClass.FindAssetLoginPDA(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sql);
            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ช้อมูลการใช้งาน");
                this.Cursor = Cursors.Default;
                return;
            }


            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LoginBranch", "รหัสสาขา", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LoginBranchName", "ชื่อสาขา", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LoginPDAName", "ชื่อเครื่อง", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "รุ่น", 250)));
            RadGridView_ShowHD.Columns["LoginBranch"].IsPinned = true;
            RadGridView_ShowHD.Columns["LoginBranchName"].IsPinned = true;
            RadGridView_ShowHD.Columns["LoginPDAName"].IsPinned = true;

            DataTable dtDay = Controllers.DateTimeSettingClass.GetDayByDate(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));

            foreach (DataRow item in dtDay.Rows)
            {
                string valueF = item["Day"].ToString();
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(valueF, valueF, 100)));

            }
            //RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            string sqlCount = $@"
                SELECT	LoginDate,LoginPDAName,LoginBranch,LoginBranchName,COUNT(LoginPDAName) AS CountLogin
                FROM	SHOP_COMPDANAMESHOP24HRS WITH (NOLOCK)
                WHERE	LoginDate BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
		                AND LoginBranch != 'MN000'
                GROUP BY LoginDate,LoginPDAName,LoginBranch,LoginBranchName
                ORDER BY LoginBranch,LoginPDAName,LoginBranchName
            ";
            DataTable dtCount = ConnectionClass.SelectSQL_Main(sqlCount);

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                foreach (DataRow item in dtDay.Rows)
                {
                    string headF = item["Day"].ToString();
                    double countCheck = 0;
                    DataRow[] result = dtCount.Select($@"LoginBranch = '{dt_Data.Rows[i]["LoginBranch"]}' AND LoginDate  = '{headF}' AND LoginPDAName = '{dt_Data.Rows[i]["LoginPDAName"]}' ");
                    if (result.Length > 0) countCheck = Convert.ToDouble(result[0]["CountLogin"].ToString());

                    RadGridView_ShowHD.Rows[i].Cells[headF].Value = countCheck;
                    if (countCheck == 0)
                    {
                        RadGridView_ShowHD.Rows[i].Cells[headF].Style.ForeColor = ConfigClass.SetColor_Red();
                    }
                }
            }

            this.Cursor = Cursors.Default;
        }
        ////Check Asset
        //DataTable CheckByPromotion(string pAssetID)
        //{
        //    string sqlPromotion = @"SELECT TOP 1
        //           [LOCATION]
        //          ,[NAMEALIAS]
        //          ,[CONDITION]
        //          ,[PATH]+[IMAGES] AS PATH
        //          ,[EmpId],SPC_NAME
        //          ,[LOCATIONMEMOold]
        //          ,CONVERT(VARCHAR,[createdate],25) AS [createdate]
        //      FROM [PA_ASSETTABLE] WITH (NOLOCK)
        //      LEFT OUTER JOIN EMPLTABLE WITH (NOLOCK) ON [PA_ASSETTABLE].EmpId = EMPLTABLE.ALTNUM
        //      where ASSETID = '" + pAssetID + @"'
        //      ORDER BY [createdate] DESC";
        //    return ConnectionClass.SelectSQL_SentServer(sqlPromotion, IpServerConnectClass.ConSUPC2014);
        //}

        //Check Asset Com
        DataTable CheckByCom(string pAssetID)
        {
            string sqlCom = $@"SELECT	TOP 1 UPDATE_WHOID,UPDATE_WHONAME,CONVERT(VARCHAR,UPDATE_DATE,25) AS UPDATE_DATE,ISNULL(REMARK,'') AS REMARK 
            FROM	SHOP_ASSETTABLELOG WITH (NOLOCK)
            WHERE	ASSETID = '{pAssetID}'
            ORDER BY UPDATE_DATE DESC";
            return ConnectionClass.SelectSQL_Main(sqlCom);
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); RadGridView_ShowHD.DataSource = dt_Data; dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;
            radTextBox_Asset.Text = "";
            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T;
            if (_pTypeReport == "1") T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "2");
            else T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            return;

        }

        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (_pTypeReport)
            {
                case "0":
                    switch (e.Column.Name)
                    {
                        case "TBL":
                            if (RadGridView_ShowHD.CurrentRow.Cells["LOG"].Value.ToString() == "") return;

                            string billNo = RadGridView_ShowHD.CurrentRow.Cells["iVALUE"].Value.ToString();

                            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ยืนยันการ ClearLog Error " + billNo + @" ?") == DialogResult.No) return;

                            ClearLog(billNo);
                            break;

                        default:
                            break;
                    }
                    break;
                case "1":
                    switch (e.Column.Name)
                    {//PATH_STR
                        case "PATH":
                            if (RadGridView_ShowHD.CurrentRow.Cells["PATH_C"].Value.ToString() == "0") return;
                            Process.Start(RadGridView_ShowHD.CurrentRow.Cells["PATH_STR"].Value.ToString());
                            break;
                        default:
                            break;
                    }
                    break;
                case "5"://open Job  
                    switch (e.Column.Name)
                    {
                        case "MACHINE":

                            if (RadGridView_ShowHD.CurrentRow.Cells["STOREID"].Value.ToString() == "0") return;
                            if (RadGridView_ShowHD.CurrentRow.Cells["STAJOB"].Value.ToString() == "0")
                            {
                                JOB.Com.JOBCOM_SHOP_ADD frmJOB_ADD = new JOB.Com.JOBCOM_SHOP_ADD("SHOP_JOBComMinimart",
                                    "MJOB", "00001", "ComMinimart.", RadGridView_ShowHD.CurrentRow.Cells["STOREID"].Value.ToString(),
                                    "WEBCAM", RadGridView_ShowHD.CurrentRow.Cells["MACHINE"].Value.ToString());

                                if (frmJOB_ADD.ShowDialog(this) == DialogResult.OK)
                                {
                                    SetDGV_HD();
                                }
                            }
                            else
                            {
                                JOB.Com.JOBCOM_EDIT frmJOB_Edit = new JOB.Com.JOBCOM_EDIT("SHOP_JOBComMinimart", "00001", "ComMinimart.", "1", "SHOP",
                                    RadGridView_ShowHD.CurrentRow.Cells["STAJOB"].Value.ToString());
                                if (frmJOB_Edit.ShowDialog(this) == DialogResult.OK)
                                {
                                    SetDGV_HD();
                                }
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        void ClearLog(string billID)
        {
            ArrayList sqlDel = AX_SendData.JOBWaitForSendDataToAX(RadGridView_ShowHD.CurrentRow.Cells["TBL"].Value.ToString(),
                RadGridView_ShowHD.CurrentRow.Cells["TABLENAME"].Value.ToString(), billID);

            //switch (RadGridView_ShowHD.CurrentRow.Cells["TBL"].Value.ToString())
            //{
            //    case "SPC_EXTERNALLIST":
            //        sqlDel.Add(@"
            //           DELETE   SPC_EXTERNALLIST
            //           WHERE    LOG != '' 
            //                    AND TABLENAME = '" + RadGridView_ShowHD.CurrentRow.Cells["TABLENAME"].Value.ToString() + @"'  AND CAST(PKFIELDVALUE AS NVARCHAR) = N'" + billID + @"'
            //        ");
            //        break;
            //    case "SPC_POSTOTABLE20":
            //        sqlDel.Add(@"
            //           DELETE   SPC_POSTOLINE20
            //           WHERE    VOUCHERID IN (SELECT VOUCHERID FROM SPC_POSTOTABLE20 WHERE    VOUCHERID = '" + billID + @"' AND LOG != '' )
            //        ");
            //        sqlDel.Add(@"
            //           DELETE   SPC_POSTOTABLE20
            //           WHERE    VOUCHERID = '" + billID + @"' AND LOG != ''
            //        ");
            //        break;
            //    case "SPC_POSTOTABLE18":
            //        sqlDel.Add(@"
            //           DELETE   SPC_POSTOLINE18
            //           WHERE    VOUCHERID IN (SELECT VOUCHERID FROM SPC_POSTOTABLE18 WHERE    VOUCHERID = '" + billID + @"' AND LOG != '' )
            //        ");
            //        sqlDel.Add(@"
            //           DELETE   SPC_POSTOTABLE18
            //           WHERE    VOUCHERID = '" + billID + @"' AND LOG != ''
            //        ");
            //        break;
            //    case "SPC_POSTOTABLE10":
            //        sqlDel.Add(@"
            //           DELETE   SPC_POSTOLINE10
            //           WHERE    VOUCHERID IN (SELECT VOUCHERID FROM SPC_POSTOTABLE10 WHERE    VOUCHERID = '" + billID + @"' AND LOG != '' )
            //        ");
            //        sqlDel.Add(@"
            //           DELETE   SPC_POSTOTABLE10
            //           WHERE    VOUCHERID = '" + billID + @"' AND LOG != ''
            //        ");
            //        break;
            //    case "SPC_PURCHTABLE00":
            //        sqlDel.Add(@"
            //           DELETE   SPC_PURCHLINE00
            //           WHERE    PURCHID IN (SELECT PURCHID FROM SPC_PURCHTABLE00 WHERE  PURCHID = '" + billID + @"' AND LOG != '' )
            //        ");
            //        sqlDel.Add(@"
            //           DELETE   SPC_PURCHTABLE00
            //           WHERE    PURCHID = '" + billID + @"' AND LOG != ''
            //        ");
            //        break;
            //    case "SPC_POSWSTABLE20":
            //        sqlDel.Add(@"
            //           DELETE   SPC_POSWSLINE20
            //           WHERE    INVOICEID IN (SELECT INVOICEID FROM SPC_POSWSTABLE20 WHERE  INVOICEID = '" + billID + @"' AND LOG != '' )
            //        ");
            //        sqlDel.Add(@"
            //           DELETE   SPC_POSWSTABLE20
            //           WHERE    INVOICEID = '" + billID + @"' AND LOG != ''
            //        ");
            //        break;
            //    case "SPC_TRANSFERUPDRECEIVE":
            //        sqlDel.Add(@"
            //           DELETE   SPC_TRANSFERUPDRECEIVE
            //           WHERE    BOXID = '" + billID + @"' AND LOG != ''
            //        ");
            //        break;
            //    case "SPC_REDEMPTABLE00":
            //        sqlDel.Add(@"
            //           DELETE   SPC_REDEMPLINE00
            //           WHERE    REDEMPID IN (SELECT REDEMPID FROM SPC_REDEMPTABLE00 WHERE  REDEMPID = '" + billID + @"' AND LOG != '' )
            //        ");
            //        sqlDel.Add(@"
            //           DELETE   SPC_REDEMPTABLE00
            //           WHERE    REDEMPID = '" + billID + @"' AND LOG != ''
            //        ");
            //        break;
            //    case "SPC_INVENTJOURNALTABLE":
            //        sqlDel.Add(@"
            //           DELETE   SPC_INVENTJOURNALTRANS
            //           WHERE    JOURNALID IN (SELECT JOURNALID FROM SPC_INVENTJOURNALTABLE WHERE  JOURNALID = '" + billID + @"' AND LOG != '' )
            //        ");
            //        sqlDel.Add(@"
            //           DELETE   SPC_INVENTJOURNALTABLE
            //           WHERE    JOURNALID = '" + billID + @"' AND LOG != ''
            //        ");
            //        break;
            //    default:
            //        break;
            //}

            string result;
            if (RadGridView_ShowHD.CurrentRow.Cells["TBL"].Value.ToString() == "SPC_REDEMPTABLE00")
            {
                ArrayList sql24 = new ArrayList()
                { $@" DELETE	SHOP_MNFR_REDEMPTABLE WHERE	REDEMPID = '{billID}' "};
                result = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlDel);
            }
            else
            {
                result = ConnectionClass.ExecuteSQL_ArrayMainAX(sqlDel);
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(result);
            if (result == "") SetDGV_HD();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, _pTypeReport);
        }
        //Count Rows
        private void GetGridViewSummary0()
        {
            GridViewSummaryItem summaryItem = new GridViewSummaryItem
            {
                Name = "TBL",
                Aggregate = GridAggregateFunction.Count,
                FormatString = "{0:n2}"
            };
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem { summaryItem };
            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
        }
        //Count Rows
        private void LoadFreightSummary()
        {
            GridViewSummaryRowItem item1 = new GridViewSummaryRowItem
            {
                new GridViewSummaryItem("INVOICEID", "จำนวนคูปอง : {0:F2} ใบ ", GridAggregateFunction.Count)
            };
            this.RadGridView_ShowHD.MasterTemplate.SummaryRowsTop.Add(item1);
        }

        //Count Rows
        private void LoadFreightSummaryUse()
        {
            GridViewSummaryItem summaryItemShipName = new GridViewSummaryItem("QTY", "รวม[รายการ] : {0:F2}", GridAggregateFunction.Sum);
            GridViewSummaryItem summaryItemFreight = new GridViewSummaryItem("AMOUNT", "รวม[บาท] : {0:F2} ", GridAggregateFunction.Sum);
            GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(
                new GridViewSummaryItem[] { summaryItemShipName, summaryItemFreight });
            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
        }
        //Enter Asset
        private void RadTextBox_Asset_KeyDown(object sender, KeyEventArgs e)
        {
            switch (_pTypeReport)
            {
                case "1":
                    if (e.KeyCode == Keys.Enter)
                    {
                        if (radTextBox_Asset.Text == "")
                        {
                            radTextBox_Asset.Focus(); return;
                        }
                        SetDGV_HD();
                    }

                    break;
                case "5":
                    if (e.KeyCode == Keys.Enter) SetDGV_HD();
                    break;
                default:
                    break;
            }

        }
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (_pTypeReport != "5") return;

            if (e.Column.Name == "CLICK")
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["SERVER"].Value.ToString() == "") return;
                if (RadGridView_ShowHD.CurrentRow.Cells["DATEBILL"].Value.ToString() == "") return;
                this.Cursor = Cursors.WaitCursor;

                DirectoryInfo DirInfoVDO = new DirectoryInfo($@"\\{RadGridView_ShowHD.CurrentRow.Cells["SERVER"].Value}\MNCamera\{RadGridView_ShowHD.CurrentRow.Cells["STOREID"].Value}\{RadGridView_ShowHD.CurrentRow.Cells["DATEBILL"].Value}");
                if (DirInfoVDO.Exists == false)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูลที่เก็บ File" + Environment.NewLine + DirInfoVDO.FullName);
                    this.Cursor = Cursors.Default;
                    return;
                }

                //S2109B64-0002872_20210905105424
                FileInfo[] FilesInvoice = DirInfoVDO.GetFiles(RadGridView_ShowHD.CurrentRow.Cells["LASTINVOICEIDWEBCAM"].Value.ToString() + @"_*", SearchOption.AllDirectories);
                if (FilesInvoice.Length == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูล VDO บิลขาย ที่ระบุ");
                    this.Cursor = Cursors.Default;
                    return;
                }
                try
                {
                    System.Diagnostics.Process.Start(FilesInvoice[FilesInvoice.Length - 1].FullName);
                    this.Cursor = Cursors.Default;
                    return;
                }
                catch (Exception ex)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถเปิด File VDO ได้" + Environment.NewLine + ex.Message);
                    this.Cursor = Cursors.Default;
                    return;
                }
            }


        }
    }
}
