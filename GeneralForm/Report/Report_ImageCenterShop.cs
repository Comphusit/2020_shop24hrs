﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;
using System.Diagnostics;

namespace PC_Shop24Hrs.GeneralForm.Report
{
    public partial class Report_ImageCenterShop : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        readonly string _pTypeOpen;
        //0 - รายงานบิลเปลี่ยน
        //1 - void ละเอียด
        //2 - พนักงานจัดสินค้าตามชั้นวาง
        //3 - การจัดร้าน ของสาขาและทีมจัดร้านที่ทำในโปรแกรม PDA
        //4 - ทีมจัดร้าน เข้าสาขา
        //5 - รายงานการตรวจตัวช่างเข้าซ่อมสาขา
        //6 - รายงาน JOB ตรวจสาขา
        //Load
        public Report_ImageCenterShop(string pTypeReport, string pTypeOpen)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pTypeOpen = pTypeOpen;
        }
        //Load
        private void Report_ImageCenterShop_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            radCheckBox_SelectAll.ButtonElement.Font = SystemClass.SetFontGernaral;

            if (_pTypeOpen == "SUPC")
            {
                DataTable dtBch = BranchClass.GetBranchAll("1", "1");
                radGridView_BCH.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("STA", "/", 30));
                radGridView_BCH.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 70));
                radGridView_BCH.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อ", 120));
                radGridView_BCH.MasterTemplate.ShowRowHeaderColumn = false;
                radGridView_BCH.DataSource = dtBch;
                dtBch.AcceptChanges();
                DatagridClass.SetDefaultRadGridView(radGridView_BCH);
            }
            else
            {
                radGridView_BCH.Visible = false;
                radCheckBox_SelectAll.Visible = false;
            }

            RadButton_Search.ButtonElement.ShowBorder = true;
            radLabel_Detail.Visible = false; radLabel_Detail.Text = "";

            switch (_pTypeReport)
            {
                case "0":
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "DoubleClick >> ดูรูปขนาดใหญ่ | สีแดง >> ไม่แนบรูป | สีเขียว >> ที่อยู่ File ไม่ถูกต้อง";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("Cash", "เงินสด")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEACCOUNT", "รหัสลูกค้า", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อลูกค้า", 230)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PHONE", "เบอร์โทร", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEDATE", "วันที่บิล", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "ผู้ทำบิล", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERIDREF", "Centershop", 150)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ItemName", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SALESUNIT", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดเงินรวม", 100)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("ImageC", "Centershop", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImageC", "CountImageC")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImageC", "PathImageC")));

                    DatagridClass.SetCellBackClolorByExpression("ImageC", "CountImageC = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("Cash", "Cash = '1' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("ImageC", "CountImageC = '2' ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);

                    RadGridView_ShowHD.TableElement.RowHeight = 150;
                    break;
                case "1":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEDATE", "วันที่บิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อแคชเชียร์", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME1", "ชื่อผู้ยืนยัน", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARKS", "หมายเหตุการ Void", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SALESUNIT", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดเงินรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATETIME", "วันที่-เวลา", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image", "รูป", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("CountImage", "CountImage")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("PathImage", "PathImage")));

                    RadGridView_ShowHD.Columns["INVOICEDATE"].IsPinned = true;
                    RadGridView_ShowHD.Columns["NAME"].IsPinned = true;
                    RadGridView_ShowHD.Columns["INVOICEID"].IsPinned = true;
                    DatagridClass.SetCellBackClolorByExpression("Image", "CountImage = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("Image", "CountImage = '2' ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);

                    RadGridView_ShowHD.TableElement.RowHeight = 150;

                    radLabel_Detail.Visible = true;
                    radLabel_Detail.Text = "DoubleClick >> รูป เพื่อดูรูปขนาดใหญ่ | สีแดง >> ไม่แนบรูป | สีเขียว >> ที่อยู่ File ไม่ถูกต้อง";
                    break;
                case "2":
                    radDateTimePicker_D2.Visible = false;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันที่เข้างาน", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "โซน", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "ชั้นวาง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MAXIMAGE", "รูป", 50)));

                    RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
                    RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;

                    int iRow = 0;
                    for (int i = 0; i < 12; i++)
                    {
                        iRow++;
                        string HeadColumnCount = "" + iRow.ToString() + @"";
                        string HeadTextDesc_Count = "[" + iRow.ToString() + @"/12]";
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image(HeadColumnCount, HeadTextDesc_Count, 150)));
                    }

                    RadGridView_ShowHD.TableElement.RowHeight = 150;

                    radLabel_Detail.Visible = true;
                    radLabel_Detail.Text = "วันที่ที่ระบุ คือ วันที่ลงเวลาเข้างาน | ช่องที่แสดง X แสดงว่าพนักงานถ่ายรูปไม่ครบตามที่ระบุ";
                    break;
                case "3":
                    radDateTimePicker_D2.Visible = false;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateIns", "วันที่", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Team", "ทีมงาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchID", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BranchName", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EmplID", "รหัสพนักงาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_Name", "ชื่อพนักงาน", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Desc_Job", "งานที่ระบุ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Before_FullNameA", "ก่อนทำ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Back_FullNameA", "หลังทำ", 150)));

                    RadGridView_ShowHD.TableElement.RowHeight = 150;

                    radLabel_Detail.Visible = true;
                    radLabel_Detail.Text = "ช่องที่แสดง X แสดงว่าพนักงานถ่ายรูปไม่ครบตามที่ระบุ";
                    break;
                case "4":
                    radDateTimePicker_D2.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_Date", "วันที่เข้างาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_Branch", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSV_BranchName", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSVDocno", "รหัสเข้าสาขา", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNSVDetail", "รายละเอียดงาน", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TYPE", "ประเภท", 50)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNSVSeq", "ลำดับ")));

                    RadGridView_ShowHD.Columns["MNSV_Date"].IsPinned = true;
                    RadGridView_ShowHD.Columns["MNSV_Branch"].IsPinned = true;

                    int iRow4 = 0;
                    for (int i = 0; i < 20; i++)
                    {
                        iRow4++;
                        string HeadColumnCount = "" + iRow4.ToString() + @"";
                        string HeadTextDesc_Count = "[" + iRow4.ToString() + @"/20]";
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image(HeadColumnCount, HeadTextDesc_Count, 150)));
                    }

                    RadGridView_ShowHD.TableElement.RowHeight = 150;

                    radLabel_Detail.Visible = false; radLabel_Detail.Text = "";
                    break;
                case "5":
                    radDateTimePicker_D2.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("", "วันที่เข้างาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("", "รหัสเข้าสาขา", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("", "รายละเอียดงาน", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("", "ประเภท", 50)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("", "ลำดับ")));

                    RadGridView_ShowHD.Columns["MNSV_Date"].IsPinned = true;
                    RadGridView_ShowHD.Columns["MNSV_Branch"].IsPinned = true;

                    RadGridView_ShowHD.TableElement.RowHeight = 150;

                    radLabel_Detail.Visible = false;
                    radLabel_Detail.Text = "";
                    break;
                case "6":
                    radDateTimePicker_D2.Visible = false;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SUMDATE", "S", 50)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SUMDATEUPD", "Up", 50)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JOB_Number", "เลขที่ JOB", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JOB_HEAD_STA", "หน.", 50)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JOB_SHORTNAMEINS", "ผู้เปิด", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JOBGROUPSUB_DESCRIPTION", "ประเภท", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JOB_SN", "หัวเรื่อง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("JOB_DESCRIPTION", "รายละเอียด", 500)));

                    int iRow6 = 0;
                    for (int i = 0; i < 15; i++)
                    {
                        iRow6++;
                        string HeadColumnCount = "" + iRow6.ToString() + @"";
                        string HeadTextDesc_Count = "[" + iRow6.ToString() + @"/15]";
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image(HeadColumnCount, HeadTextDesc_Count, 150)));
                    }

                    RadGridView_ShowHD.TableElement.RowHeight = 150;

                    radLabel_Detail.Visible = true;
                    radLabel_Detail.Text = "วันที่ที่ระบุ คือ วันที่เปิด JOB | แสดงรูปไม่เกิน 15 รูปเท่านั้น";
                    break;
                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            switch (_pTypeReport)
            {
                case "0":
                    Set0();
                    break;
                case "1":
                    Set1();
                    break;
                case "2":
                    Set2();
                    break;
                case "3":
                    Set3();
                    break;
                case "4":
                    Set4();
                    break;
                case "6":
                    Set6();
                    break;
                default:
                    break;
            }
        }
        //set Bch
        string SetBch()
        {
            string bch = "";
            for (int i = 0; i < radGridView_BCH.Rows.Count; i++)
            {
                if (radGridView_BCH.Rows[i].Cells["STA"].Value.ToString() == "1") bch += "'" + radGridView_BCH.Rows[i].Cells["BRANCH_ID"].Value.ToString() + "',";
            }
            if (bch.Length > 0) bch = bch.Substring(0, bch.Length - 1);

            return bch;
        }
        //1 - void ละเอียด
        void Set1()//1 - void ละเอียด
        {
            this.Cursor = Cursors.WaitCursor;
            string bchID;
            if (_pTypeOpen == "SUPC") bchID = SetBch(); else bchID = $@"'{SystemClass.SystemBranchID}'";

            if (bchID.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำหนดให้ระบุการเลือกสาขาทุกครั้งก่อนการดึงข้อมูล{Environment.NewLine}ระบุสาขาให้เรียบร้อยแล้วกดดึงข้อมูลใหม่อีกครั้ง");
                return;
            }
             
            dt_Data = PosSaleClass.GetDetailSale_BillVoid("0", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), bchID); //ConnectionClass.SelectSQL_POSRetail707(sqlSelect1);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string pCount = "0", pathFullName = PathImageClass.pImageEmply;
                if (dt_Data.Rows[i]["IMGPATH"].ToString() != "")
                {
                    pCount = "1";
                    pathFullName = dt_Data.Rows[i]["IMGPATH"].ToString();
                }

                try
                {
                    RadGridView_ShowHD.Rows[i].Cells["Image"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullName);
                    RadGridView_ShowHD.Rows[i].Cells["PathImage"].Value = pathFullName;//pathFileName + @"|" + wantFile;
                    RadGridView_ShowHD.Rows[i].Cells["CountImage"].Value = pCount;
                }
                catch
                {

                    RadGridView_ShowHD.Rows[i].Cells["Image"].Value = PathImageClass.pImageEmply;
                    RadGridView_ShowHD.Rows[i].Cells["PathImage"].Value = "";//pathFileName + @"|" + wantFile;
                    RadGridView_ShowHD.Rows[i].Cells["CountImage"].Value = "2";
                }
            }
            this.Cursor = Cursors.Default;
        }
        //บิลเปลี่ยน
        void Set0()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();
            string bchID;
            if (_pTypeOpen == "SUPC") bchID = SetBch(); else bchID = "'" + SystemClass.SystemBranchID + "'";

            if (bchID.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำหนดให้ระบุการเลือกสาขาทุกครั้งก่อนการดึงข้อมูล{Environment.NewLine}ระบุสาขาให้เรียบร้อยแล้วกดดึงข้อมูลใหม่อีกครั้ง");
                return;
            }

            string pBch = $@" AND INVENTLOCATIONID IN ({bchID}) ";

            dt_Data = PosSaleClass.GetBillU(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), pBch);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string pCountC = "0", pathFullNameC = PathImageClass.pImageEmply;
                if (dt_Data.Rows[i]["IMGPATH"].ToString() != "")
                {
                    pCountC = "1";
                    pathFullNameC = dt_Data.Rows[i]["IMGPATH"].ToString();
                }
                try
                {
                    RadGridView_ShowHD.Rows[i].Cells["ImageC"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFullNameC);
                    RadGridView_ShowHD.Rows[i].Cells["PathImageC"].Value = pathFullNameC;
                    RadGridView_ShowHD.Rows[i].Cells["CountImageC"].Value = pCountC;
                }
                catch (Exception)
                {
                    RadGridView_ShowHD.Rows[i].Cells["ImageC"].Value = PathImageClass.pImageEmply;
                    RadGridView_ShowHD.Rows[i].Cells["PathImageC"].Value = "";
                    RadGridView_ShowHD.Rows[i].Cells["CountImageC"].Value = "2";
                }
            }

            this.Cursor = Cursors.Default;
        }
        //Set พนักงานตามชั้นวาง
        void Set2()
        {
            this.Cursor = Cursors.WaitCursor;
            string bchID;
            if (_pTypeOpen == "SUPC") bchID = SetBch(); else bchID = "'" + SystemClass.SystemBranchID + "'";

            if (bchID.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กำหนดให้ระบุการเลือกสาขาทุกครั้งก่อนการดึงข้อมูล" + Environment.NewLine +
                    "ระบุสาขาให้เรียบร้อยแล้วกดดึงข้อมูลใหม่อีกครั้ง");
                return;
            }
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            dt_Data = Models.EmplClass.GetEmpAllShelf_ByDate(bchID, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), "", "");
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            string pDate = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string pZone = dt_Data.Rows[i]["ZONE_ID"].ToString();
                string pSH = dt_Data.Rows[i]["SHELF_ID"].ToString();
                int MaxImage = Convert.ToInt32(dt_Data.Rows[i]["MAXIMAGE"].ToString());
                string pathFileName = PathImageClass.pPathShelfNew + pDate + @"\" + dt_Data.Rows[i]["NUM"].ToString();
                DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);

                FileInfo[] Files;
                if (DirInfo.Exists)
                {
                    Files = DirInfo.GetFiles("*" + dt_Data.Rows[i]["EMPLID"].ToString().Substring(1, 7) + @"_" + pZone + "#" + pSH + @".JPG", SearchOption.AllDirectories);
                    int cCheck = Files.Length;
                    int iRow = 10;
                    for (int iK = 0; iK < MaxImage; iK++)
                    {
                        iRow++;
                        if (iK >= cCheck) RadGridView_ShowHD.Rows[i].Cells[iRow].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
                        else RadGridView_ShowHD.Rows[i].Cells[iRow].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(Files[iK].FullName);
                    }
                }
            }

            this.Cursor = Cursors.Default;
        }
        //Set พนักงานจัดสินค้า ใน PDA
        void Set3()
        {
            this.Cursor = Cursors.WaitCursor;
            string bchID;
            if (_pTypeOpen == "SUPC") bchID = SetBch(); else bchID = "'" + SystemClass.SystemBranchID + "'";

            if (bchID.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กำหนดให้ระบุการเลือกสาขาทุกครั้งก่อนการดึงข้อมูล" + Environment.NewLine +
                    "ระบุสาขาให้เรียบร้อยแล้วกดดึงข้อมูลใหม่อีกครั้ง");
                return;
            }

            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            string sql = $@"
                SELECT	CONVERT(VARCHAR,DateIns,23) AS DateIns,BranchID,BranchName,
		                CASE TeamType WHEN '1' THEN 'ทีมจัดร้าน' WHEN '2' THEN 'ทีมสาขา' ELSE TeamType END AS Team,
		                EmplID,SPC_Name,Desc_Job,
	                    SUBSTRING(CONVERT(VARCHAR,DateIns,23),0,8)+'\'+CONVERT(VARCHAR,DateIns,23)+'\'+Before_FullName AS Before_FullName,
		                SUBSTRING(CONVERT(VARCHAR,DateBack,23),0,8)+'\'+CONVERT(VARCHAR,DateBack,23)+'\'+Back_FullName AS Back_FullName
                FROM	SHOP_EMPLOYEE_SORTPRODUCT WITH (NOLOCK)
                WHERE	DateIns = '{radDateTimePicker_D1.Value:yyyy-MM-dd}'
		                AND BranchID IN ({bchID}) 
                ORDER BY BranchID,EmplID,SeqNo
            ";
            dt_Data = ConnectionClass.SelectSQL_Main(sql);
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string pathFileName1 = PathImageClass.pPathEmplSorting + dt_Data.Rows[i]["Before_FullName"].ToString();
                string pathFileName2 = PathImageClass.pPathEmplSorting + dt_Data.Rows[i]["Back_FullName"].ToString();

                if (File.Exists(pathFileName1)) RadGridView_ShowHD.Rows[i].Cells["Before_FullNameA"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFileName1);
                else RadGridView_ShowHD.Rows[i].Cells["Back_FullNameA"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);

                if (File.Exists(pathFileName2)) RadGridView_ShowHD.Rows[i].Cells["Back_FullNameA"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(pathFileName2);
                else RadGridView_ShowHD.Rows[i].Cells["Back_FullNameA"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(PathImageClass.pImageEmply);
            }
            this.Cursor = Cursors.Default;
        }
        //Set ทีมจัดร้าน
        void Set4()
        {
            this.Cursor = Cursors.WaitCursor;
            string bchID;
            if (_pTypeOpen == "SUPC") bchID = SetBch(); else bchID = "'" + SystemClass.SystemBranchID + "'";

            if (bchID.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำหนดให้ระบุการเลือกสาขาทุกครั้งก่อนการดึงข้อมูล{Environment.NewLine}ระบุสาขาให้เรียบร้อยแล้วกดดึงข้อมูลใหม่อีกครั้ง");
                return;
            }

            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            string sql = $@"
                SELECT	MNSV_Branch ,MNSV_BranchName ,MNSV_Type,Shop_MNSV_RC.MNSV_Docno,MNSVSeq,MNSVDetail,CONVERT(VARCHAR,Shop_MNSV_HD.MNSV_Date,23) AS MNSV_Date,
                        SUBSTRING(CONVERT(VARCHAR,Shop_MNSV_HD.MNSV_Date,23),0,8) + '\' + CONVERT(VARCHAR,Shop_MNSV_HD.MNSV_Date,23)+'\'+ MNSV_Branch+ '\'  AS PathImage 
                FROM	Shop_MNSV_RC WITH (NOLOCK) 
		                INNER JOIN Shop_MNSV_HD WITH (NOLOCK)	ON Shop_MNSV_RC.MNSV_Docno = Shop_MNSV_HD.MNSV_Docno 
                WHERE	Shop_MNSV_HD.MNSV_Date = '{radDateTimePicker_D1.Value:yyyy-MM-dd}' 
		                AND MNSV_STA = '1' AND MNSV_Branch IN ({bchID}) 
                ORDER BY MNSV_Branch,Shop_MNSV_HD.MNSV_Docno,Shop_MNSV_RC.MNSVSeq
            ";
            dt_Data = ConnectionClass.SelectSQL_Main(sql);

            int RowI = 0;
            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string date = dt_Data.Rows[i]["MNSV_Date"].ToString();
                string bchid = dt_Data.Rows[i]["MNSV_Branch"].ToString();
                string bchName = dt_Data.Rows[i]["MNSV_BranchName"].ToString();
                string docno = dt_Data.Rows[i]["MNSV_Docno"].ToString();
                string detail = dt_Data.Rows[i]["MNSVDetail"].ToString();
                string SeqNo = dt_Data.Rows[i]["MNSVSeq"].ToString();

                string pathFile = PathImageClass.pPathMNSV + dt_Data.Rows[i]["PathImage"].ToString();
                DirectoryInfo DirInfo = new DirectoryInfo(pathFile);

                if (DirInfo.Exists)
                {
                    FileInfo[] FilesBefore = DirInfo.GetFiles("*" + docno + @"-Before*_" + SeqNo + @".JPG", SearchOption.AllDirectories);
                    FileInfo[] FilesBack = DirInfo.GetFiles("*" + docno + @"-Back*_" + SeqNo + @".JPG", SearchOption.AllDirectories);

                    int iRowBefore = FilesBefore.Length; if (iRowBefore > 20) iRowBefore = 20;
                    int iRowBack = FilesBack.Length; if (iRowBack > 20) iRowBack = 20;

                    int iRowA = 6;
                    RadGridView_ShowHD.Rows.Add(date, bchid, bchName, docno, detail, "ก่อนทำ", SeqNo);
                    for (int iK = 0; iK < iRowBefore; iK++)
                    {
                        iRowA++;
                        RadGridView_ShowHD.Rows[RowI].Cells[iRowA].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesBefore[iK].FullName);
                    }
                    RowI++;

                    int iRowB = 6;
                    RadGridView_ShowHD.Rows.Add(date, bchid, bchName, docno, detail, "หลังทำ", SeqNo);
                    for (int iK = 0; iK < iRowBack; iK++)
                    {
                        iRowB++;
                        RadGridView_ShowHD.Rows[RowI].Cells[iRowB].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesBack[iK].FullName);
                    }
                    RowI++;
                }
            }
            this.Cursor = Cursors.Default;
        }
        //Set job เข้าตรวจสาขา
        void Set6()
        {
            this.Cursor = Cursors.WaitCursor;
            string bchID;
            if (_pTypeOpen == "SUPC") bchID = SetBch(); else bchID = "'" + SystemClass.SystemBranchID + "'";

            if (bchID.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"กำหนดให้ระบุการเลือกสาขาทุกครั้งก่อนการดึงข้อมูล{Environment.NewLine}ระบุสาขาให้เรียบร้อยแล้วกดดึงข้อมูลใหม่อีกครั้ง");
                return;
            }
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();
            if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();

            string sqlJob = $@"
                    SELECT	JOB_Number,JOB_BRANCHID AS BRANCH_ID,JOB_BRANCHNAME AS BRANCH_NAME,JOBGROUPSUB_DESCRIPTION,
		                    JOB_SN,JOB_DESCRIPTION,JOB_DESCRIPTION_UPD,
		                    CASE ISNULL(JOB_BRANCHOUTPHUKET,'X') WHEN '0' THEN CONVERT(VARCHAR,'X') ELSE CONVERT(VARCHAR,'/') END  AS JOB_BRANCHOUTPHUKET,JOB_STACLOSE,
		                    JOB_SHORTNAMEINS, JOB_HEAD, 
		                    CASE ISNULL(JOB_HEAD, 'X') WHEN '0' THEN CONVERT(VARCHAR, 'X') ELSE CONVERT(VARCHAR, '/') END  AS JOB_HEAD_STA, 
		                    DateDiff(Day, JOB_DATEINS, GETDATE()) AS SUMDATE   ,
		                    CASE CONVERT(VARCHAR,JOB_DATEUPD,23) WHEN '1900-01-01' THEN  DateDiff(Day,JOB_DATEINS,GETDATE())
		                    ELSE ISNULL(DATEDIFF(DAY,JOB_DATEUPD,GETDATE()),0) END AS SUMDATEUPD 
                    FROM	SHOP_JOBCENTER WITH(NOLOCK)  
                    WHERE	CONVERT(VARCHAR,JOB_DATEINS,23) = '{radDateTimePicker_D1.Value:yyyy-MM-dd}'
		                    AND  JOB_TYPE = '00004'
                            AND JOB_BRANCHID IN ({bchID})
                    ORDER BY CONVERT(VARCHAR,JOB_DATEINS,23) DESC
            ";
            dt_Data = ConnectionClass.SelectSQL_Main(sqlJob);
            RadGridView_ShowHD.FilterDescriptors.Clear();

            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลที่ต้องการค้นหา{Environment.NewLine}ลองใหม่อีกครั้ง");
                this.Cursor = Cursors.Default;
                return;
            }
            string pDate = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            int MaxImage = 15;
            int iiRows = 0;

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                string pJobNumber = dt_Data.Rows[i]["JOB_Number"].ToString();
                string pathFileNameOpen = PathImageClass.pPathJOBHD + pDate.Substring(0, 7) + @"\" + pDate + @"\" + pJobNumber + @"\";
                string pathFileNameClose = PathImageClass.pPathJOBHDBCH + pDate.Substring(0, 7) + @"\" + pDate + @"\" + pJobNumber + @"\";

                RadGridView_ShowHD.Rows.Add(dt_Data.Rows[i]["SUMDATE"], dt_Data.Rows[i]["SUMDATEUPD"], pJobNumber,
                    dt_Data.Rows[i]["JOB_HEAD_STA"], dt_Data.Rows[i]["BRANCH_ID"], dt_Data.Rows[i]["BRANCH_NAME"], dt_Data.Rows[i]["JOB_SHORTNAMEINS"],
                    dt_Data.Rows[i]["JOBGROUPSUB_DESCRIPTION"], dt_Data.Rows[i]["JOB_SN"], dt_Data.Rows[i]["JOB_DESCRIPTION"]);

                DirectoryInfo DirInfoOpen = new DirectoryInfo(pathFileNameOpen);
                DirectoryInfo DirInfoClose = new DirectoryInfo(pathFileNameClose);

                FileInfo[] FilesOpen;
                if (DirInfoOpen.Exists)
                {
                    FilesOpen = DirInfoOpen.GetFiles("*.JPG", SearchOption.AllDirectories);
                    int cCheck = FilesOpen.Length;
                    int iRow = 9;
                    for (int iK = 0; iK < MaxImage; iK++)
                    {
                        iRow++;
                        if (iK < cCheck)
                            RadGridView_ShowHD.Rows[iiRows].Cells[iRow].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesOpen[iK].FullName);
                    }
                }

                iiRows++;
                RadGridView_ShowHD.Rows.Add("", "", "", "", "", "", "", "", "", dt_Data.Rows[i]["JOB_DESCRIPTION_UPD"]);
                FileInfo[] FilesClose;
                if (DirInfoClose.Exists)
                {
                    FilesClose = DirInfoClose.GetFiles("*.JPG", SearchOption.AllDirectories);
                    int cCheck = FilesClose.Length;
                    int iRow = 9;
                    for (int iK = 0; iK < MaxImage; iK++)
                    {
                        iRow++;
                        if (iK < cCheck)
                            RadGridView_ShowHD.Rows[iiRows].Cells[iRow].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(FilesClose[iK].FullName);
                    }
                }

                iiRows++;
            }

            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
            if (_pTypeOpen == "SUPC")
            {
                for (int i = 0; i < radGridView_BCH.Rows.Count; i++)
                {
                    if (radGridView_BCH.Rows[i].Cells["STA"].Value.ToString() == "1") radGridView_BCH.Rows[i].Cells["STA"].Value = "0";
                }
            }

            switch (_pTypeReport)
            {
                case "4":
                    if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear(); break;
                case "6":
                    if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear(); break;
                default:
                    break;
            }

            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }

        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (_pTypeReport)
            {
                case "0":
                    if (RadGridView_ShowHD.CurrentRow.Cells["CountImageC"].Value.ToString() == "1") Process.Start(RadGridView_ShowHD.CurrentRow.Cells["PathImageC"].Value.ToString());
                    break;
                case "1":
                    if (RadGridView_ShowHD.CurrentRow.Cells["CountImage"].Value.ToString() == "1") Process.Start(RadGridView_ShowHD.CurrentRow.Cells["PathImage"].Value.ToString());
                    break;
                default:
                    break;
            }
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);

        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //SetFont
        #region "SetFont"
        private void RadGridView_BCH_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
        }

        private void RadGridView_BCH_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_BCH_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_BCH_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //Cell Choose
        private void RadGridView_BCH_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (e.Column.Name)
            {
                case "STA":
                    if (true)
                    {
                        if (radGridView_BCH.CurrentRow.Cells["STA"].Value.ToString() == "1") radGridView_BCH.CurrentRow.Cells["STA"].Value = "0";
                        else radGridView_BCH.CurrentRow.Cells["STA"].Value = "1";
                    }
                    break;
                default: break;
            }
        }
        //select All BCH
        private void RadCheckBox_SelectAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_SelectAll.CheckState == CheckState.Checked)
            {
                for (int i = 0; i < radGridView_BCH.Rows.Count; i++)
                {
                    radGridView_BCH.Rows[i].Cells["STA"].Value = "1";
                }
                radCheckBox_SelectAll.Text = "เอาทุกสาขาออก";
            }
            else
            {
                for (int i = 0; i < radGridView_BCH.Rows.Count; i++)
                {
                    radGridView_BCH.Rows[i].Cells["STA"].Value = "0";
                }
                radCheckBox_SelectAll.Text = "เลือกทุกสาขา";
            }
        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
    }
}
