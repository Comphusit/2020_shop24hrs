﻿namespace PC_Shop24Hrs.GeneralForm.Report
{
    partial class Report_CheckVDOWebCam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report_CheckVDOWebCam));
            this.RadGridView_ShowDT = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.axWindowsMediaPlayer_Show = new AxWMPLib.AxWindowsMediaPlayer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_DateTime = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_cashID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.radCheckBox_OK = new Telerik.WinControls.UI.RadCheckBox();
            this.RadButton_Cancle = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Date = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Remark = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Cst = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT.MasterTemplate)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer_Show)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_cashID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Cst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // RadGridView_ShowDT
            // 
            this.RadGridView_ShowDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ShowDT.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ShowDT.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RadGridView_ShowDT.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.RadGridView_ShowDT.Name = "RadGridView_ShowDT";
            // 
            // 
            // 
            this.RadGridView_ShowDT.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ShowDT.Size = new System.Drawing.Size(636, 305);
            this.RadGridView_ShowDT.TabIndex = 17;
            this.RadGridView_ShowDT.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_ShowDT_ViewCellFormatting);
            this.RadGridView_ShowDT.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_ShowDT_ConditionalFormattingFormShown);
            this.RadGridView_ShowDT.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_ShowDT_FilterPopupRequired);
            this.RadGridView_ShowDT.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_ShowDT_FilterPopupInitialized);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel3.Controls.Add(this.splitContainer1, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(874, 642);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(863, 636);
            this.splitContainer1.SplitterDistance = 327;
            this.splitContainer1.TabIndex = 18;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.axWindowsMediaPlayer_Show, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(863, 327);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // axWindowsMediaPlayer_Show
            // 
            this.axWindowsMediaPlayer_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axWindowsMediaPlayer_Show.Enabled = true;
            this.axWindowsMediaPlayer_Show.Location = new System.Drawing.Point(3, 63);
            this.axWindowsMediaPlayer_Show.Name = "axWindowsMediaPlayer_Show";
            this.axWindowsMediaPlayer_Show.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer_Show.OcxState")));
            this.axWindowsMediaPlayer_Show.Size = new System.Drawing.Size(857, 261);
            this.axWindowsMediaPlayer_Show.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_DateTime, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel_cashID, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Docno, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Cst, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(857, 54);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // radLabel_DateTime
            // 
            this.radLabel_DateTime.AutoSize = false;
            this.radLabel_DateTime.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_DateTime.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_DateTime.ForeColor = System.Drawing.Color.Black;
            this.radLabel_DateTime.Location = new System.Drawing.Point(453, 3);
            this.radLabel_DateTime.Name = "radLabel_DateTime";
            this.radLabel_DateTime.Size = new System.Drawing.Size(401, 18);
            this.radLabel_DateTime.TabIndex = 57;
            // 
            // radLabel_cashID
            // 
            this.radLabel_cashID.AutoSize = false;
            this.radLabel_cashID.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_cashID.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_cashID.ForeColor = System.Drawing.Color.Black;
            this.radLabel_cashID.Location = new System.Drawing.Point(153, 3);
            this.radLabel_cashID.Name = "radLabel_cashID";
            this.radLabel_cashID.Size = new System.Drawing.Size(294, 18);
            this.radLabel_cashID.TabIndex = 56;
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Docno.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(3, 3);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(144, 18);
            this.radLabel_Docno.TabIndex = 55;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.RadGridView_ShowDT);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.radCheckBox_OK);
            this.splitContainer2.Panel2.Controls.Add(this.RadButton_Cancle);
            this.splitContainer2.Panel2.Controls.Add(this.RadButton_Save);
            this.splitContainer2.Panel2.Controls.Add(this.radLabel_Date);
            this.splitContainer2.Panel2.Controls.Add(this.radTextBox_Remark);
            this.splitContainer2.Size = new System.Drawing.Size(863, 305);
            this.splitContainer2.SplitterDistance = 636;
            this.splitContainer2.TabIndex = 19;
            // 
            // radCheckBox_OK
            // 
            this.radCheckBox_OK.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_OK.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_OK.Location = new System.Drawing.Point(8, 16);
            this.radCheckBox_OK.Name = "radCheckBox_OK";
            this.radCheckBox_OK.Size = new System.Drawing.Size(115, 19);
            this.radCheckBox_OK.TabIndex = 68;
            this.radCheckBox_OK.Text = "สถานะเรียบร้อย";
            this.radCheckBox_OK.CheckStateChanged += new System.EventHandler(this.RadCheckBox_OK_CheckStateChanged);
            // 
            // RadButton_Cancle
            // 
            this.RadButton_Cancle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Cancle.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancle.Location = new System.Drawing.Point(24, 265);
            this.RadButton_Cancle.Name = "RadButton_Cancle";
            this.RadButton_Cancle.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Cancle.TabIndex = 67;
            this.RadButton_Cancle.Text = "ยกเลิก";
            this.RadButton_Cancle.ThemeName = "Fluent";
            this.RadButton_Cancle.Click += new System.EventHandler(this.RadButton_Cancle_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancle.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancle.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancle.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancle.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancle.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancle.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancle.GetChildAt(0).GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancle.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Save
            // 
            this.RadButton_Save.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Save.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Save.Location = new System.Drawing.Point(24, 227);
            this.RadButton_Save.Name = "RadButton_Save";
            this.RadButton_Save.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Save.TabIndex = 66;
            this.RadButton_Save.Text = "บันทึก";
            this.RadButton_Save.ThemeName = "Fluent";
            this.RadButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel_Date
            // 
            this.radLabel_Date.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Date.Location = new System.Drawing.Point(8, 43);
            this.radLabel_Date.Name = "radLabel_Date";
            this.radLabel_Date.Size = new System.Drawing.Size(119, 19);
            this.radLabel_Date.TabIndex = 61;
            this.radLabel_Date.Text = "หมายเหตุการตรวจ";
            // 
            // radTextBox_Remark
            // 
            this.radTextBox_Remark.AcceptsReturn = true;
            this.radTextBox_Remark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radTextBox_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radTextBox_Remark.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Remark.Location = new System.Drawing.Point(8, 68);
            this.radTextBox_Remark.Multiline = true;
            this.radTextBox_Remark.Name = "radTextBox_Remark";
            // 
            // 
            // 
            this.radTextBox_Remark.RootElement.StretchVertically = true;
            this.radTextBox_Remark.Size = new System.Drawing.Size(206, 125);
            this.radTextBox_Remark.TabIndex = 0;
            // 
            // radLabel_Cst
            // 
            this.radLabel_Cst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Cst.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Cst.Location = new System.Drawing.Point(153, 30);
            this.radLabel_Cst.Name = "radLabel_Cst";
            this.radLabel_Cst.Size = new System.Drawing.Size(60, 19);
            this.radLabel_Cst.TabIndex = 58;
            this.radLabel_Cst.Text = "CstName";
            // 
            // D054_ReportCheckVDOWebCam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 642);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IconScaling = Telerik.WinControls.Enumerations.ImageScaling.None;
            this.Name = "Report_CheckVDOWebCam";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VDO การขาย";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.D054_ReportCheckVDOWebCam_FormClosed);
            this.Load += new System.EventHandler(this.Report_CheckVDOWebCam_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ShowDT)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer_Show)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_cashID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Cst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView RadGridView_ShowDT;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer_Show;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadLabel radLabel_cashID;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Remark;
        private Telerik.WinControls.UI.RadLabel radLabel_Date;
        protected Telerik.WinControls.UI.RadButton RadButton_Save;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancle;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_OK;
        private Telerik.WinControls.UI.RadLabel radLabel_DateTime;
        private Telerik.WinControls.UI.RadLabel radLabel_Cst;
    }
}
