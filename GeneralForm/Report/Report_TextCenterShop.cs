﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.IO;

namespace PC_Shop24Hrs.GeneralForm.Report
{
    public partial class Report_TextCenterShop : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        //0 - รายงานการพิมพ์บิลขายซ้ำ
        //1 - รายงานการ Void สินค้าแบบละเอียด ย้ายไป D054_Report_Image
        //2 - รายงานการ Void สินค้าแบบสรุป
        //3 - รายงานการเป็นทศนิยม
        //4 - รายงานการเปลี่ยนเเปลงราคา
        //5 - รายงานจำนวนสินค้า ขาย-เบิก-คืน
        //6 - รายงานรายละเอียดการเบิก
        //7 - รายงานรายละเอียดการคืน
        //8 - รายงานรายละเอียดการขาย
        //9 - รายงานมูลค่า เบิก-ขาย-คืน-สต็อก ตามสาขา
        //10 - รายงานมูลค่า เบิก-ขาย-คืน-สต็อก ตามวันที่
        //11 - รายงานประจำวัน ละเอียด
        //12 - รายงานประจำวัน สรุป
        //13- รายงานพนักงานจัดสินค้า สรุป
        //14- รายงานพนักงานจัดสินค้า ละเอียด
        //15 - รายงานการนับเงินในลิ้นชัก
        //16 - การส่งสต็อกหมู-ไก่/ผัก/ผลไม้
        //17 - รายงานอัตราการขายย้อนหลัง 3 เดือน / ตามจัดซื้อ
        //18 - รายงานอัตราการขายย้อนหลัง 3 เดือน / ตามกลุ่ม
        //19 - รายการการปรับเปลี่ยนลูกค้าในบิลขาย
        //20 - รายงานการ Void สินค้าแบบสรุป.
        //21 - รายการงานเบิกสินค้า
        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

        //Load
        public Report_TextCenterShop(string pTypeReport)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
        }
        //Load
        private void Report_TextCenterShop_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Grp);

            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Grp.ButtonElement.Font = SystemClass.SetFontGernaral;

            RadDropDownList_1.Visible = false; RadCheckBox_1.Visible = false;
            radLabel_Detail.Visible = false;
            radRadioButton_P1.Visible = false; radRadioButton_P2.Visible = false;
            radDropDownList_Grp.Visible = false; radCheckBox_Grp.Visible = false;

            switch (_pTypeReport)
            {
                case "0":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEDATE", "วันที่บิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("INVOICEAMOUNT", "ยอดเงินในบิล", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PRINTCOUNT", "จำนวนพิมพ์", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "แคชเชียร์", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMECH", "ชื่อแคชเชียร์", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTPRINTBY", "ผู้พิมพ์", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMELAST", "ชื่อผู้พิมพ์", 250)));

                    break;
                case "1":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'"); else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEDATE", "วันที่บิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "แคชเชียร์", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อแคชเชียร์", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLKBFUNCTION", "ผู้พิมพ์", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME1", "ชื่อผู้พิมพ์", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARKS", "หมายเหตุการ Void", 180)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SALESUNIT", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดเงินรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATETIME", "วันที่-เวลา", 200)));
                    break;
                case "2":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSName", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("YearVoice", "ปี", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MonthVoice", "เดือน", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "แคชเชียร์", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อแคชเชียร์", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLKBFUNCTION", "ผู้ช่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEEMP", "ชื่อผู้ช่วย", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("CountVoice", "จำนวนครั้ง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("CountSum", "ยอดรวม", 100)));
                    break;
                case "3":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEDATE", "วันที่บิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "แคชเชียร์", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อแคชเชียร์", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SALESUNIT", "หน่วย", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SALESPRICE", "ราคาขาย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("DIF", "ผลต่าง", 100)));

                    break;
                case "4":
                    radRadioButton_P1.Visible = true; radRadioButton_P2.Visible = true; radRadioButton_P1.CheckState = CheckState.Checked;
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "สีฟ้า >> สินค้าปรับราคาขึ้น | สีแดง >> ยังไม่พิมพ์ป้ายราคาในวันที่ปรับล่าสุด";
                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else
                    {
                        RadCheckBox_1.Checked = true; RadCheckBox_1.Enabled = false;
                        RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    }

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID"; radDateTimePicker_D2.Visible = false;

                    dt_Data.Columns.Add("BRANCH_ID");
                    dt_Data.Columns.Add("BRANCH_NAME");
                    dt_Data.Columns.Add("DATEPRT");
                    dt_Data.Columns.Add("ITEMBARCODE");
                    dt_Data.Columns.Add("SPC_ITEMNAME");
                    dt_Data.Columns.Add("ORIGPRICESALES");
                    dt_Data.Columns.Add("NEWPRICESALES");
                    dt_Data.Columns.Add("UNITID");
                    dt_Data.Columns.Add("TRANSDATEEDIT");
                    dt_Data.Columns.Add("SPC_ITEMBUYERGROUPID");
                    dt_Data.Columns.Add("DESCRIPTION");
                    dt_Data.Columns.Add("GRPID0");
                    dt_Data.Columns.Add("TAX0");
                    dt_Data.Columns.Add("STAPRT");
                    dt_Data.Columns.Add("STA_1");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEPRT", "พิมพ์ล่าสุด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("ORIGPRICESALES", "ราคาเก่า", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("NEWPRICESALES", "ราคาใหม่", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATEEDIT", "เวลาปรับราคา", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBUYERGROUPID", "จัดซื้อ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อจัดซื้อ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GRPID0", "รหัสกลุ่มหลัก", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TAX0", "กลุ่มสินค้าหลัก", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STAPRT", "สถานะพิมพ์")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA_1", "STA_1")));

                    RadGridView_ShowHD.Columns["ORIGPRICESALES"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["NEWPRICESALES"].FormatString = "{0:#,##0.00}";

                    ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "STA_1 = '1' ", false)
                    { CellBackColor = ConfigClass.SetColor_SkyPastel() };
                    this.RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_ShowHD.Columns["NEWPRICESALES"].ConditionalFormattingObjectList.Add(obj2);

                    //ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition3", "STAPRT > '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_Red() };
                    //this.RadGridView_ShowHD.Columns["DATEPRT"].ConditionalFormattingObjectList.Add(obj3);
                    DatagridClass.SetCellBackClolorByExpression("DATEPRT", "STAPRT > '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    break;
                case "5":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true; RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;

                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";
                    radDropDownList_Grp.Visible = true; radCheckBox_Grp.Visible = true;
                    radDropDownList_Grp.DataSource = ItembarcodeClass.GetInventGroup();
                    radDropDownList_Grp.DisplayMember = "TXT";
                    radDropDownList_Grp.ValueMember = "INVENTGROUP";

                    dt_Data.Columns.Add("ITEMBARCODE");
                    dt_Data.Columns.Add("SPC_ITEMNAME");
                    dt_Data.Columns.Add("UNITID");
                    dt_Data.Columns.Add("qtyTO");
                    dt_Data.Columns.Add("qtySale");
                    dt_Data.Columns.Add("qtyCN");
                    dt_Data.Columns.Add("SumQTY");
                    dt_Data.Columns.Add("qtyStock");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtyTO", "จำนวนเบิก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtySale", "จำนวนขาย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtyCN", "จำนวนคืน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SumQTY", "คงเหลือ", 100)));

                    RadGridView_ShowHD.Columns["qtyTO"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["qtySale"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["qtyCN"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["SumQTY"].FormatString = "{0:#,##0.00}";

                    break;
                case "6":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true; RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;

                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";
                    radDropDownList_Grp.Visible = true; radCheckBox_Grp.Visible = true;
                    radDropDownList_Grp.DataSource = ItembarcodeClass.GetInventGroup();
                    radDropDownList_Grp.DisplayMember = "TXT";
                    radDropDownList_Grp.ValueMember = "INVENTGROUP";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PRODUCT_1", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("INVENTQTY", "จำนวนเบิก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดเงินรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ENTITY", "จัดซื้อ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อจัดซื้อ", 250)));

                    GridViewSummaryItem summaryItem6 = new GridViewSummaryItem
                    {
                        Name = "LINEAMOUNT",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem6 = new GridViewSummaryRowItem { summaryItem6 };

                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem6);

                    break;
                case "7":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true; RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;

                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";
                    radDropDownList_Grp.Visible = true; radCheckBox_Grp.Visible = true;
                    radDropDownList_Grp.DataSource = ItembarcodeClass.GetInventGroup();
                    radDropDownList_Grp.DisplayMember = "TXT";
                    radDropDownList_Grp.ValueMember = "INVENTGROUP";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PRODUCT_1", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("INVENTQTY", "จำนวนคืน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดเงินรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ENTITY", "จัดซื้อ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อจัดซื้อ", 250)));

                    GridViewSummaryItem summaryItem7 = new GridViewSummaryItem
                    {
                        Name = "LINEAMOUNT",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem7 = new GridViewSummaryRowItem { summaryItem7 };

                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem7);
                    break;
                case "8":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true; RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;
                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";
                    radDropDownList_Grp.Visible = true; radCheckBox_Grp.Visible = true;
                    radDropDownList_Grp.DataSource = ItembarcodeClass.GetInventGroup();
                    radDropDownList_Grp.DisplayMember = "TXT";
                    radDropDownList_Grp.ValueMember = "INVENTGROUP";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("PRODUCT_1", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("INVENTQTY", "จำนวนขาย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดเงินรวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ENTITY", "จัดซื้อ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อจัดซื้อ", 250)));

                    GridViewSummaryItem summaryItem8 = new GridViewSummaryItem
                    {
                        Name = "LINEAMOUNT",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem8 = new GridViewSummaryRowItem { summaryItem8 };

                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem8);
                    break;
                case "9":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    dt_Data.Columns.Add("BRANCH_ID");
                    dt_Data.Columns.Add("BRANCH_NAME");
                    dt_Data.Columns.Add("qtyTO");
                    dt_Data.Columns.Add("qtySale");
                    dt_Data.Columns.Add("qtyCN");
                    dt_Data.Columns.Add("qtyStk");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtyTO", "มูลค่าเบิก", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtySale", "มูลค่าขาย", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtyCN", "มูลค่าคืน", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtyStk", "มูลค่าสต็อก", 150)));
                    break;
                case "10":
                    //TimePeriod
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true; RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;
                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    dt_Data.Columns.Add("TimePeriod");
                    dt_Data.Columns.Add("qtyTO");
                    dt_Data.Columns.Add("qtySale");
                    dt_Data.Columns.Add("qtyCN");
                    dt_Data.Columns.Add("qtyStk");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TimePeriod", "วันที่", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtyTO", "มูลค่าเบิก", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtySale", "มูลค่าขาย", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtyCN", "มูลค่าคืน", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("qtyStk", "มูลค่าสต็อก", 150)));
                    break;
                case "11":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    radCheckBox_Grp.Visible = true; radDropDownList_Grp.Visible = true;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1); radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                    radLabel_Detail.Visible = true;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                        RadCheckBox_1.Enabled = true; RadCheckBox_1.CheckState = CheckState.Unchecked;
                        radLabel_Detail.Text = "DoubleClick >> เพิ่มหรือแก้ไขข้อมูล";
                    }
                    else
                    {
                        RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                        RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;
                        radLabel_Detail.Text = "DoubleClick >> เพิ่มข้อมูล";
                    }

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    radDropDownList_Grp.DataSource = FindGrpCase11_Report("");
                    radDropDownList_Grp.DisplayMember = "SHOW_NAME";
                    radDropDownList_Grp.ValueMember = "SHOW_ID";

                    dt_Data.Columns.Add("SHOW_ID");
                    dt_Data.Columns.Add("SHOW_NAME");
                    dt_Data.Columns.Add("BRANCH_ID");
                    dt_Data.Columns.Add("BRANCH_NAME");
                    dt_Data.Columns.Add("Rpt_DATE");
                    dt_Data.Columns.Add("Rpt_Values");
                    dt_Data.Columns.Add("Rpt_Remark");
                    dt_Data.Columns.Add("WhoIns");
                    dt_Data.Columns.Add("WhoNameIns");
                    dt_Data.Columns.Add("DateIns");
                    dt_Data.Columns.Add("TypeReport");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_ID", "รหัส", 50)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "ชื่อรายงาน", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Rpt_DATE", "วันที่", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("Rpt_Values", "รายงาน", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Rpt_Remark", "หมายเหตุ", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoIns", "ผู้บันทึก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoNameIns", "ชื่อผู้บันทึก", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateIns", "วันที่บิันทึก", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TypeReport", "ประเภทรายงาน")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ID", "ID")));

                    RadGridView_ShowHD.MasterTemplate.Columns["SHOW_ID"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["SHOW_NAME"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_ID"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["Rpt_Values"].FormatString = "{0:N2}";
                    GridViewSummaryItem summaryItem11 = new GridViewSummaryItem
                    {
                        Name = "Rpt_Values",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem11 = new GridViewSummaryRowItem { summaryItem11 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem11);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    //กำหนดเงื่อนไขเพื่อกำหนดสี cell 
                    ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "Rpt_Values = '' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    this.RadGridView_ShowHD.Columns["SHOW_ID"].ConditionalFormattingObjectList.Add(obj1);
                    this.RadGridView_ShowHD.Columns["SHOW_NAME"].ConditionalFormattingObjectList.Add(obj1);
                    this.RadGridView_ShowHD.Columns["BRANCH_ID"].ConditionalFormattingObjectList.Add(obj1);
                    this.RadGridView_ShowHD.Columns["BRANCH_NAME"].ConditionalFormattingObjectList.Add(obj1);

                    break;
                case "12":
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    radCheckBox_Grp.Visible = true; radDropDownList_Grp.Visible = true;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true;
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-7); radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1);

                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                        RadCheckBox_1.Enabled = true; RadCheckBox_1.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                        RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;
                    }

                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    radDropDownList_Grp.DataSource = FindGrpCase11_Report("");
                    radDropDownList_Grp.DisplayMember = "SHOW_NAME";
                    radDropDownList_Grp.ValueMember = "SHOW_ID";
                    radCheckBox_Grp.Enabled = false; radCheckBox_Grp.CheckState = CheckState.Checked;

                    break;
                case "13":
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "Double Click >> เพื่อดูรูปขนาดใหญ่ | สีฟ้า >> มีรูปถ่าย";

                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                        RadCheckBox_1.Enabled = true; RadCheckBox_1.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                        RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;
                    }
                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";
                    break;
                case "14":
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "Double Click >> เพื่อดูรูปขนาดใหญ่ | สีแแดง >> ไม่มีรูปถ่าย";
                    RadDropDownList_1.Visible = true; RadCheckBox_1.Visible = true;
                    radRadioButton_P1.Visible = true; radRadioButton_P1.Text = "ทั้งหมด"; radRadioButton_P1.CheckState = CheckState.Checked;
                    radRadioButton_P2.Visible = true; radRadioButton_P2.Text = "ไม่มีรูป";
                    radDateTimePicker_D2.Visible = false;
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                        RadCheckBox_1.Enabled = true; RadCheckBox_1.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                        RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;
                    }
                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";
                    break;

                case "15":
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "สีแดง >> ยอดที่นับได้น้อยกว่าเงินในลิ้นชัก | สีฟ้า >> ยอดที่นับได้มากกว่าเงินในลิ้นชัก";
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Transdate", "วันที่", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "แคชเชียร์", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อแคชเชียร์", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONEID", "ชื่อสาขา", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSNUMBER", "เครื่องขาย", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("LINEAMOUNT", "ยอดทั้งหมด", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("CountAMOUNT", "ยอดที่นับได้", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("DiffAMOUNT", "ผลต่าง", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Remark", "หมายเหตุ", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoIns", "รหัสนับ", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WhoNameIns", "ผู้นับ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateIns", "วันที่นับ", 150)));

                    //this.RadGridView_ShowHD.Columns["DiffAMOUNT"].ConditionalFormattingObjectList.Add(
                    //  new ExpressionFormattingObject("MyCondition2", "DiffAMOUNT" + " > 0 ", false)
                    //  { CellBackColor = ConfigClass.SetColor_Red() });
                    DatagridClass.SetCellBackClolorByExpression("DiffAMOUNT", "DiffAMOUNT" + " > 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    //this.RadGridView_ShowHD.Columns["DiffAMOUNT"].ConditionalFormattingObjectList.Add(
                    //  new ExpressionFormattingObject("MyCondition2", "DiffAMOUNT" + " < 0 ", false)
                    //  { CellBackColor = ConfigClass.SetColor_SkyPastel() });
                    DatagridClass.SetCellBackClolorByExpression("DiffAMOUNT", "DiffAMOUNT" + " < 0 ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                    break;

                case "16": //สต็อกหมู-ไก่
                    Report16("0");
                    break;
                case "17":
                    radLabel_Detail.Visible = true;
                    radLabel_Detail.Text = "สีแดง >> สินค้าที่ต้องขาย มากกว่า 10 เดือน ถึงจะหมด";
                    radDateTimePicker_D1.Visible = false;
                    radDateTimePicker_D2.Visible = false; radLabel_Date.Visible = false;

                    RadDropDownList_1.Visible = true;
                    RadCheckBox_1.Visible = true;
                    RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;

                    radCheckBox_Grp.Visible = true; radCheckBox_Grp.Enabled = false; radCheckBox_Grp.CheckState = CheckState.Checked; radCheckBox_Grp.Text = "ระบุจัดซื้อ";
                    radDropDownList_Grp.Visible = true;
                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);


                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    radDropDownList_Grp.Visible = true; radCheckBox_Grp.Visible = true;
                    radDropDownList_Grp.DataSource = Models.DptClass.GetDpt_Purchase();
                    radDropDownList_Grp.DisplayMember = "DESCRIPTION";
                    radDropDownList_Grp.ValueMember = "NUM";

                    dt_Data.Columns.Add("ITEMBARCODE");
                    dt_Data.Columns.Add("SPC_ITEMNAME");
                    dt_Data.Columns.Add("QTYPR");
                    dt_Data.Columns.Add("SALEQTY");
                    dt_Data.Columns.Add("PCQTY");
                    dt_Data.Columns.Add("PER_MONTH");
                    dt_Data.Columns.Add("QTYHAVE");
                    dt_Data.Columns.Add("STOCK");
                    dt_Data.Columns.Add("PER_SALE");
                    dt_Data.Columns.Add("STA");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYPR", "จำนวนเบิก", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SALEQTY", "จำนวนขาย", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PCQTY", "จำนวนคืน", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PER_MONTH", "ขายเฉลี่ย/เดือน", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYHAVE", "จำนวนควรเหลือ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("STOCK", "สต็อกในระบบ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PER_SALE", "จำนวนเดือนที่จะขายหมด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "STA")));

                    RadGridView_ShowHD.Columns["QTYPR"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["SALEQTY"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["PCQTY"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["PER_MONTH"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["QTYHAVE"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["STOCK"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["PER_SALE"].FormatString = "{0:#,##0.00}";

                    ExpressionFormattingObject obj107 = new ExpressionFormattingObject("AA", " STA = '1' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    this.RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj107);
                    this.RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj107);
                    this.RadGridView_ShowHD.Columns["PER_SALE"].ConditionalFormattingObjectList.Add(obj107);

                    break;
                case "18":
                    radLabel_Detail.Visible = true;
                    radLabel_Detail.Text = "สีแดง >> สินค้าที่ไม่มียอดขายภายใน 3 เดือน ควรคืนสินค้า | สีฟ้า >> สินค้าที่ต้องขาย มากกว่า 3 เดือน ถึงจะหมด | สีเหลือง >> สต็อกติดลบ ควรนับสต็อกแล้วปรับปรุง";
                    radDateTimePicker_D1.Visible = false;
                    radDateTimePicker_D2.Visible = false; radLabel_Date.Visible = false;

                    RadDropDownList_1.Visible = true;
                    RadCheckBox_1.Visible = true;
                    RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;

                    radCheckBox_Grp.Visible = true; radCheckBox_Grp.Enabled = false; radCheckBox_Grp.CheckState = CheckState.Checked; radCheckBox_Grp.Text = "ระบุจัดซื้อ";

                    if (SystemClass.SystemBranchID == "MN000") RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    else RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);


                    RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                    RadDropDownList_1.ValueMember = "BRANCH_ID";

                    radDropDownList_Grp.Visible = true; radCheckBox_Grp.Visible = true; radCheckBox_Grp.Text = "กลุ่มสินค้า";
                    radDropDownList_Grp.DataSource = ItembarcodeClass.GetInventGroup();
                    radDropDownList_Grp.DisplayMember = "TXT";
                    radDropDownList_Grp.ValueMember = "INVENTGROUP";

                    dt_Data.Columns.Add("ITEMBARCODE");
                    dt_Data.Columns.Add("SPC_ITEMNAME");
                    dt_Data.Columns.Add("SALEQTY");
                    dt_Data.Columns.Add("PER_MONTH");
                    dt_Data.Columns.Add("STOCK");
                    dt_Data.Columns.Add("PER_SALE");
                    dt_Data.Columns.Add("STA");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SALEQTY", "จำนวนขาย 3 เดือน", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PER_MONTH", "ขายเฉลี่ย/เดือน", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("STOCK", "สต็อกในระบบ", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("PER_SALE", "จำนวนเดือนที่จะขายหมด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STA", "STA")));

                    RadGridView_ShowHD.Columns["SALEQTY"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["PER_MONTH"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["STOCK"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["PER_SALE"].FormatString = "{0:#,##0.00}";

                    ExpressionFormattingObject obj108 = new ExpressionFormattingObject("AA", " STA = '1' ", false)
                    { CellBackColor = ConfigClass.SetColor_SkyPastel() };
                    this.RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj108);
                    this.RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj108);
                    this.RadGridView_ShowHD.Columns["SALEQTY"].ConditionalFormattingObjectList.Add(obj108);
                    this.RadGridView_ShowHD.Columns["PER_MONTH"].ConditionalFormattingObjectList.Add(obj108);
                    this.RadGridView_ShowHD.Columns["STOCK"].ConditionalFormattingObjectList.Add(obj108);
                    this.RadGridView_ShowHD.Columns["PER_SALE"].ConditionalFormattingObjectList.Add(obj108);

                    ExpressionFormattingObject obj110 = new ExpressionFormattingObject("AA", " STA = '2' ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    this.RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj110);
                    this.RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj110);
                    this.RadGridView_ShowHD.Columns["SALEQTY"].ConditionalFormattingObjectList.Add(obj110);
                    this.RadGridView_ShowHD.Columns["PER_MONTH"].ConditionalFormattingObjectList.Add(obj110);
                    this.RadGridView_ShowHD.Columns["STOCK"].ConditionalFormattingObjectList.Add(obj110);
                    this.RadGridView_ShowHD.Columns["PER_SALE"].ConditionalFormattingObjectList.Add(obj110);

                    ExpressionFormattingObject obj109 = new ExpressionFormattingObject("AA", " STA = '3' ", false)
                    { CellBackColor = ConfigClass.SetColor_YellowPastel() };
                    this.RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj109);
                    this.RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj109);
                    this.RadGridView_ShowHD.Columns["SALEQTY"].ConditionalFormattingObjectList.Add(obj109);
                    this.RadGridView_ShowHD.Columns["PER_MONTH"].ConditionalFormattingObjectList.Add(obj109);
                    this.RadGridView_ShowHD.Columns["STOCK"].ConditionalFormattingObjectList.Add(obj109);
                    this.RadGridView_ShowHD.Columns["PER_SALE"].ConditionalFormattingObjectList.Add(obj109);

                    break;
                case "19":
                    radLabel_Detail.Visible = false;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true; radLabel_Date.Visible = true;

                    RadDropDownList_1.Visible = false; RadCheckBox_1.Visible = false; RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;

                    radCheckBox_Grp.Visible = false; radCheckBox_Grp.Enabled = false; radCheckBox_Grp.CheckState = CheckState.Checked; radCheckBox_Grp.Text = "ระบุจัดซื้อ";
                    radDropDownList_Grp.Visible = false;

                    radDropDownList_Grp.Visible = false; radCheckBox_Grp.Visible = false;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "เลขที่บิล", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUM", "ลูกค้าใหม่", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUMNAME", "ชื่อลูกค้าใหม่", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUMOLD", "ลูกค้าเก่า", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ACCOUNTNUMOLDNAME", "ชื่อลูกค้าเก่า", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANDATE", "วันที่บิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSGROUPNAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDATEIME", "วันที่บันทึก", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "แคชเชียร์", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CASHIERIDNAME", "ชื่อแคชเชียร์", 200)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOID", "ผู้บันทึก", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHONAME", "ชื่อผู้บันทึก", 200)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("INVOICEAMOUNT", "ยอดเงินในบิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("NEWPOINT", "แต้มในบิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุ", 350)));

                    break;
                case "20":
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "แสดงจำนวนรายการ/คน/วันที่";
                    RadDropDownList_1.Visible = false; RadCheckBox_1.Visible = false;
                    break;
                case "21":
                    radLabel_Detail.Visible = false;
                    radDateTimePicker_D1.Visible = true; radDateTimePicker_D2.Visible = true; radLabel_Date.Visible = true;

                    RadDropDownList_1.Visible = false; RadCheckBox_1.Visible = false; RadCheckBox_1.CheckState = CheckState.Checked; RadCheckBox_1.Enabled = false;

                    radCheckBox_Grp.Visible = false; radCheckBox_Grp.Enabled = false; radCheckBox_Grp.CheckState = CheckState.Checked; radCheckBox_Grp.Text = "ระบุจัดซื้อ";
                    radDropDownList_Grp.Visible = false;
                    radDropDownList_Grp.Visible = false; radCheckBox_Grp.Visible = false;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRRBRANCH", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRRBarcode", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRRName", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("MNRRQtyOrder", "จำนวนเบิก", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRRQtyUnitID", "หน่วย", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("MNRRPriceSum", "ราคารวม", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNRRRemark", "เหตุผลการเบิก", 600)));
                    RadGridView_ShowHD.Columns["MNRRBRANCH"].IsPinned = true;
                    RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;
                    GridViewSummaryItem summaryItem21 = new GridViewSummaryItem
                    {
                        Name = "MNRRPriceSum",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRow21 = new GridViewSummaryRowItem { summaryItem21 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRow21);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    break;
                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            switch (_pTypeReport)
            {
                case "0"://รายงานพิมพ์บิลซ้ำ
                    string bchID = "";
                    if (RadCheckBox_1.CheckState == CheckState.Checked) bchID = RadDropDownList_1.SelectedValue.ToString();
                    dt_Data = PosSaleClass.GetDetail_BillRePrint(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), bchID);// ConnectionClass.SelectSQL_Main(sqlSelect0);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "1":// void ละเอียด
                    string bch1 = "";
                    if (RadCheckBox_1.CheckState == CheckState.Checked) bch1 = RadDropDownList_1.SelectedValue.ToString();

                    dt_Data = PosSaleClass.GetDetailSale_BillVoid("1",
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), bch1); //ConnectionClass.SelectSQL_POSRetail707(sqlSelect1);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "2"://2 - รายงานการ Void สินค้าแบบสรุป

                    dt_Data = PosSaleClass.GetDetailSale_BillVoid("2",
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), ""); //ConnectionClass.SelectSQL_POSRetail707(sqlSelect2);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "3"://ขายเปนทศนิยน

                    dt_Data = PosSaleClass.GetDetail_BillSaleDecimal(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(sqlSelect3);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    this.Cursor = Cursors.Default;
                    break;
                case "4": //เปลี่ยนแปลงราคาสินค้า
                    SetCase4();
                    break;
                case "5":
                    SetCase5();
                    break;
                case "6":
                    SetCase6("6");
                    break;
                case "7":
                    SetCase6("7");
                    break;
                case "8":
                    SetCase8();
                    break;
                case "9":
                    SetCase9();
                    break;
                case "10":
                    SetCase10();
                    break;
                case "11":
                    var sqlUp11 = $@"
                        Update	SHOP_REPORTDATA Set Rpt_Values = REPLACE(Rpt_Values,',','')
                        FROM	SHOP_REPORTDATA WITH (NOLOCK) INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
			                        ON SHOP_REPORTDATA.Rpt_ID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
                        WHERE	TYPE_CONFIG = '28' AND CHARINDEX(',',Rpt_Values) > '0'
		                        AND MaxImage = '1' ";
                    var sqlUp12 = $@"
                        Update	SHOP_REPORTDATA Set Rpt_Values = '0'
                        FROM	SHOP_REPORTDATA WITH (NOLOCK) INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
			                        ON SHOP_REPORTDATA.Rpt_ID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID
                        WHERE	TYPE_CONFIG = '28' AND  Rpt_Values = '-'
		                        AND MaxImage = '1' ";
                    ConnectionClass.ExecuteSQL2Str_Main(sqlUp11, sqlUp12);
                    SetCase11();
                    break;
                case "12":
                    SetCase12();
                    break;
                case "13":
                    SetCase13();
                    break;
                case "14":
                    SetCase14();
                    break;
                case "15":
                    string sqlSelect15 = $@"
                        SELECT	CONVERT(VARCHAR,Transdate,23) AS Transdate,EMPLID,NAME,POSGROUP,ZONEID,POSNUMBER+'-'+LOCATIONID AS POSNUMBER,
                                LINEAMOUNT,CountAMOUNT,DiffAMOUNT,Remark,WhoIns,WhoNameIns,CONVERT(VARCHAR,DateIns,25) AS DateIns
                        FROM	SHOP_COUNTMONEY WITH (NOLOCK)
                        WHERE	Transdate BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
                        ORDER BY DateIns  ";
                    dt_Data = ConnectionClass.SelectSQL_Main(sqlSelect15);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "16"://สต็อกหมู ไก่
                    SetCase16();
                    break;
                case "17"://อัตราการขายย้อนหลัง 3 เดือน
                    SetCase17();
                    break;
                case "18"://อัตราการขายย้อนหลัง 3 เดือน // ตามกลุ่ม
                    SetCase18();
                    break;
                case "19": //19 - รายการการปรับเปลี่ยนลูกค้าในบิลขาย
                    SetCase19();
                    break;
                case "20": //20 - รายการสรุปการ Void
                    SetCase20();
                    break;
                case "21"://21 - รายการงานเบิกสินค้า
                    SetCase21();
                    break;
                default:
                    break;
            }

        }
        // 4 การเปลี่ยนแปลงราคาสินค้าของมินิมาร์ท
        void SetCase4()
        {
            DataTable dtBch;
            if (RadCheckBox_1.CheckState == CheckState.Checked) dtBch = BranchClass.GetDetailBranchByID(RadDropDownList_1.SelectedValue.ToString());
            else dtBch = BranchClass.GetBranchAll("'1'", "'1'");

            string qty = " AND QTY = '1' ";
            if (radRadioButton_P2.CheckState == CheckState.Checked) qty = " AND QTY > '1' ";

            for (int i = 0; i < dtBch.Rows.Count; i++)
            {
                //string sql = string.Format(@"
                //        select	CONVERT(VARCHAR,ISNULL(DATEMAX,'1900-01-01'),23) AS DATEPRT  ,
                //          SPC_INVENTITEMPRICEHISTORY.ITEMID,SPC_INVENTITEMPRICEHISTORY.INVENTDIMID,SPC_INVENTITEMPRICEHISTORY.UNITID,  
                //          INVENTITEMBARCODE.ITEMBARCODE,SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION,SPC_ITEMNAME,QTY,
                //                CAST (ROUND(NEWPRICESALES,2) AS decimal(18,2)) AS   NEWPRICESALES,
                //          CAST (ROUND(ORIGPRICESALES,2) AS decimal(18,2)) AS ORIGPRICESALES,MAX(dateAdd(mi,420,SPC_INVENTITEMPRICEHISTORY.MODIFIEDDATETIME)) as TRANSDATEEDIT  ,
                //          CONVERT(VARCHAR,MAX(dateAdd(mi,420,SPC_INVENTITEMPRICEHISTORY.MODIFIEDDATETIME)),24) AS TIMEEDIT ,
                //                CASE WHEN CONVERT(VARCHAR,ISNULL(DATEMAX,'1900-01-01'),23) >=	CONVERT(VARCHAR,'" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"',23) THEN '0' ELSE '1' END AS STAPRT,
                //                SPC_INVENTGROUPID AS GRPID0,SPC_INVENTGROUP.TXT AS TAX0

                //        from	SHOP2013TMP.dbo.SPC_INVENTITEMPRICEHISTORY WITH (NOLOCK) 
                //          INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON  INVENTITEMBARCODE.UNITID = SPC_INVENTITEMPRICEHISTORY.UNITID  
                //            AND INVENTITEMBARCODE.ITEMID = SPC_INVENTITEMPRICEHISTORY.ITEMID   AND INVENTITEMBARCODE.INVENTDIMID = SPC_INVENTITEMPRICEHISTORY.INVENTDIMID   
                //                INNER JOIN SHOP2013TMP.dbo.INVENTTABLE WITH (NOLOCK) ON  INVENTITEMBARCODE.ITEMID =INVENTTABLE.ITEMID AND INVENTTABLE.DATAAREAID = N'SPC'
                //          LEFT OUTER JOIN SHOP2013TMP.dbo. DIMENSIONS WITH (NOLOCK) ON INVENTITEMBARCODE.SPC_ITEMBUYERGROUPID = DIMENSIONS.NUM  
                //          INNER JOIN 
                //            ( SELECT	BRANCH,BARCODE	FROM	SHOP2013TMP.dbo.SPC_BARCODEBYBRANCH WITH (NOLOCK) WHERE	BRANCH = '" + dtBch.Rows[i]["BRANCH_ID"].ToString() + @"' GROUP BY BRANCH,BARCODE )TMPSALE 
                //            ON INVENTITEMBARCODE.ITEMBARCODE = TMPSALE.BARCODE
                //          LEFT OUTER JOIN (  SELECT	BRANCH,ITEMBARCODE,MAX(DATE) AS DATEMAX 	FROM	SHOP_ITEMPRTBARCODE WITH (NOLOCK) 
                //            WHERE	BRANCH = '" + dtBch.Rows[i]["BRANCH_ID"].ToString() + @"' GROUP BY	BRANCH,ITEMBARCODE )TMPPRT ON  INVENTITEMBARCODE.ITEMBARCODE = TMPPRT.ITEMBARCODE 
                //                LEFT OUTER JOIN SHOP2013TMP.dbo.SPC_INVENTGROUP WITH (NOLOCK) ON INVENTTABLE.SPC_INVENTGROUPID = SPC_INVENTGROUP.INVENTGROUP AND SPC_INVENTGROUP.DATAAREAID = N'SPC'

                //        WHERE	PRICEFIELDID  IN (SELECT FieldID FROM Shop_Price WITH (NOLOCK) WHERE ColumnName = '" + dtBch.Rows[i]["BRANCH_PRICE"].ToString() + @"' ) 
                //          AND CONVERT(VARCHAR,TRANSDATE,23)  = '" + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @"' 
                //                AND SPC_ITEMBUYERGROUPID NOT IN ('D110','D164','D032','D144') AND INVENTITEMBARCODE.SPC_ItemActive = '1'  " + qty + @"


                //        GROUP BY	SPC_INVENTITEMPRICEHISTORY.ITEMID,SPC_INVENTITEMPRICEHISTORY.INVENTDIMID,QTY, 
                //           SPC_ITEMBUYERGROUPID,DIMENSIONS.DESCRIPTION,SPC_ITEMNAME, INVENTITEMBARCODE.ITEMBARCODE, SPC_INVENTITEMPRICEHISTORY.UNITID, 
                //           NEWPRICESALES, ORIGPRICESALES ,	ISNULL(DATEMAX,'1900-01-01')  ,
                //                    CASE WHEN CONVERT(VARCHAR,ISNULL(DATEMAX,'1900-01-01'),23) <	CONVERT(VARCHAR," + radDateTimePicker_D1.Value.ToString("yyyy-MM-dd") + @",23) THEN '1' ELSE '0' END ,
                //                    SPC_INVENTGROUPID,SPC_INVENTGROUP.TXT

                //        ORDER BY	CONVERT(VARCHAR,MAX(dateAdd(mi,420,SPC_INVENTITEMPRICEHISTORY.MODIFIEDDATETIME)),24),SPC_ITEMBUYERGROUPID,SPC_ITEMNAME,QTY  
                //        ");
                DataTable dtAll = ItembarcodeClass.GetItemDetailPriceChange(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), dtBch.Rows[i]["BRANCH_ID"].ToString(),
                    dtBch.Rows[i]["BRANCH_PRICE"].ToString(), qty); //ConnectionClass.SelectSQL_Main(sql);
                for (int ik = 0; ik < dtAll.Rows.Count; ik++)
                {
                    string y = "0";
                    if (double.Parse(dtAll.Rows[ik]["NEWPRICESALES"].ToString()) > double.Parse(dtAll.Rows[ik]["ORIGPRICESALES"].ToString())) y = "1";

                    dt_Data.Rows.Add(dtBch.Rows[i]["BRANCH_ID"].ToString(), dtBch.Rows[i]["BRANCH_NAME"].ToString(),
                        dtAll.Rows[ik]["DATEPRT"].ToString(), dtAll.Rows[ik]["ITEMBARCODE"].ToString(),
                        dtAll.Rows[ik]["SPC_ITEMNAME"].ToString(), dtAll.Rows[ik]["ORIGPRICESALES"].ToString(),
                        dtAll.Rows[ik]["NEWPRICESALES"].ToString(), dtAll.Rows[ik]["UNITID"].ToString(),
                        dtAll.Rows[ik]["TRANSDATEEDIT"].ToString(), dtAll.Rows[ik]["SPC_ITEMBUYERGROUPID"].ToString(),
                        dtAll.Rows[ik]["DESCRIPTION"].ToString(),
                        dtAll.Rows[ik]["GRPID0"].ToString(), dtAll.Rows[ik]["TAX0"].ToString(),
                        dtAll.Rows[ik]["STAPRT"].ToString(),
                        y);
                }

            }
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        // 5
        void SetCase5()
        {
            string pGrp = "";
            if (radCheckBox_Grp.CheckState == CheckState.Checked) pGrp = " AND INVENTGROUPID = '" + radDropDownList_Grp.SelectedValue.ToString() + @"' ";

            DataTable dtItem = ItembarcodeClass.GetBarcodeMin(pGrp);

            DataTable dtTO = PosSaleClass.GetQtyTOSumBarcodeMin_ByBch(pGrp, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                 RadDropDownList_1.SelectedValue.ToString(), "6");
            DataTable dtCN = PosSaleClass.GetQtyTOSumBarcodeMin_ByBch(pGrp, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                 RadDropDownList_1.SelectedValue.ToString(), "7");
            DataTable dtSale = PosSaleClass.GetQtySaleSumBarcodeMin_ByBch(pGrp, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                 RadDropDownList_1.SelectedValue.ToString());


            for (int i = 0; i < dtItem.Rows.Count; i++)
            {
                double qtyTO = 0, qtySale = 0, qtyCN = 0;
                double qtyStock = 0;

                if (dtTO.Rows.Count > 0)
                {
                    DataRow[] resultTO = dtTO.Select("PRODUCT_1 = '" + dtItem.Rows[i]["ITEMBARCODE"].ToString() + @"'");
                    if (resultTO.Length > 0) qtyTO = Convert.ToDouble(resultTO[0]["INVENTQTY"].ToString());
                }
                if (dtCN.Rows.Count > 0)
                {
                    DataRow[] resultCN = dtCN.Select("PRODUCT_1 = '" + dtItem.Rows[i]["ITEMBARCODE"].ToString() + @"' ");
                    if (resultCN.Length > 0) qtyCN = Convert.ToDouble(resultCN[0]["INVENTQTY"].ToString());
                }

                if (dtSale.Rows.Count > 0)
                {
                    DataRow[] resultSale = dtSale.Select("PRODUCT_1 = '" + dtItem.Rows[i]["ITEMBARCODE"].ToString() + @"' ");
                    if (resultSale.Length > 0) qtySale = Convert.ToDouble(resultSale[0]["INVENTQTY"].ToString());
                }

                if ((qtyTO + qtySale + qtyCN) > 0)
                {
                    double SumQTY = qtyTO - (qtySale - qtyCN);
                    dt_Data.Rows.Add(dtItem.Rows[i]["ITEMBARCODE"].ToString(), dtItem.Rows[i]["SPC_ITEMNAME"].ToString(), dtItem.Rows[i]["UNITID"].ToString(),
                        qtyTO.ToString("N2"), qtySale.ToString("N2"), qtyCN.ToString("N2"), SumQTY.ToString("N2"), qtyStock.ToString("N2"));
                }
            }
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //6 - รายงานรายละเอียดการเบิก 7 การคืน
        void SetCase6(string pType)
        {
            string pGrp = "";
            if (radCheckBox_Grp.CheckState == CheckState.Checked) pGrp = " AND INVENTGROUPID = '" + radDropDownList_Grp.SelectedValue.ToString() + @"' ";
            dt_Data = PosSaleClass.GetQtyTOSumBarcodeMin_ByBch(pGrp, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                 RadDropDownList_1.SelectedValue.ToString(), pType);

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //8
        void SetCase8()
        {
            string pGrp = "";
            if (radCheckBox_Grp.CheckState == CheckState.Checked) pGrp = " AND INVENTGROUPID = '" + radDropDownList_Grp.SelectedValue.ToString() + @"' ";

            dt_Data = PosSaleClass.GetQtySaleSumBarcodeMin_ByBch(pGrp, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"),
                 RadDropDownList_1.SelectedValue.ToString());

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //9
        void SetCase9()
        {
            DataTable dtTO = PosSaleClass.GetQtyTOSumGrandAll_ByBch("6", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "BRANCH", "");
            DataTable dtCN = PosSaleClass.GetQtyTOSumGrandAll_ByBch("7", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "BRANCH", "");
            DataTable dtSale = PosSaleClass.GetQtySaleSumGrandAll_ByBch(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "BRANCH", "");

            DataTable dtBch;
            if (RadCheckBox_1.CheckState == CheckState.Checked) dtBch = BranchClass.GetDetailBranchByID(RadDropDownList_1.SelectedValue.ToString());
            else dtBch = BranchClass.GetBranchAll("'1'", "'1'");

            for (int i = 0; i < dtBch.Rows.Count; i++)
            {
                double qtyTO = 0, qtySale = 0, qtyCN = 0;

                if (dtTO.Rows.Count > 0)
                {
                    DataRow[] resultTO = dtTO.Select("BRANCH = '" + dtBch.Rows[i]["BRANCH_ID"].ToString() + @"'");
                    if (resultTO.Length > 0) qtyTO = Convert.ToDouble(resultTO[0]["LINEAMOUNT"].ToString());
                }

                if (dtCN.Rows.Count > 0)
                {
                    DataRow[] resultCN = dtCN.Select("BRANCH = '" + dtBch.Rows[i]["BRANCH_ID"].ToString() + @"' ");
                    if (resultCN.Length > 0) qtyCN = Convert.ToDouble(resultCN[0]["LINEAMOUNT"].ToString());
                }

                if (dtSale.Rows.Count > 0)
                {
                    DataRow[] resultSale = dtSale.Select("BRANCH = '" + dtBch.Rows[i]["BRANCH_ID"].ToString() + @"' ");
                    if (resultSale.Length > 0) qtySale = Convert.ToDouble(resultSale[0]["LINEAMOUNT"].ToString());
                }

                double qtyStk = qtySale - (qtyTO + qtyCN);
                dt_Data.Rows.Add(dtBch.Rows[i]["BRANCH_ID"].ToString(), dtBch.Rows[i]["BRANCH_NAME"].ToString(),
                      qtyTO.ToString("N2"), qtySale.ToString("N2"), qtyCN.ToString("N2"), qtyStk.ToString("N2"));
            }

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //9
        void SetCase10()
        {
            string pBch = " AND BRANCH = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
            DataTable dtTO = PosSaleClass.GetQtyTOSumGrandAll_ByBch("6", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "TimePeriod", pBch);
            DataTable dtCN = PosSaleClass.GetQtyTOSumGrandAll_ByBch("7", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "TimePeriod", pBch);
            DataTable dtSale = PosSaleClass.GetQtySaleSumGrandAll_ByBch(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "TimePeriod", pBch);

            DateTime Date1 = Convert.ToDateTime(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"));
            DateTime Date2 = Convert.ToDateTime(radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));
            int daysDiff = ((TimeSpan)(Date2 - Date1)).Days;

            for (int i = 0; i < daysDiff + 1; i++)
            {
                double qtyTO = 0, qtySale = 0, qtyCN = 0;

                if (dtTO.Rows.Count > 0)
                {
                    DataRow[] resultTO = dtTO.Select("TimePeriod = '" + Date1.AddDays(i) + @"'");
                    if (resultTO.Length > 0) qtyTO = Convert.ToDouble(resultTO[0]["LINEAMOUNT"].ToString());
                }
                if (dtCN.Rows.Count > 0)
                {
                    DataRow[] resultCN = dtCN.Select("TimePeriod = '" + Date1.AddDays(i) + @"' ");
                    if (resultCN.Length > 0) qtyCN = Convert.ToDouble(resultCN[0]["LINEAMOUNT"].ToString());
                }

                if (dtSale.Rows.Count > 0)
                {
                    DataRow[] resultSale = dtSale.Select("TimePeriod = '" + Date1.AddDays(i) + @"' ");
                    if (resultSale.Length > 0) qtySale = Convert.ToDouble(resultSale[0]["LINEAMOUNT"].ToString());
                }

                double qtyStk = qtySale - (qtyTO + qtyCN);
                dt_Data.Rows.Add(Date1.AddDays(i).ToShortDateString(),
                      qtyTO.ToString("N2"), qtySale.ToString("N2"), qtyCN.ToString("N2"), qtyStk.ToString("N2"));
            }

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //11    //11 - รายงานประจำวัน ละเอียด
        void SetCase11()
        {
            string pBchConfig = "", pBchReport = "", pTypeConfig = "", pTypeReport = "";
            if (RadCheckBox_1.CheckState == CheckState.Checked)
            {
                pBchConfig = " AND SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID  = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
                pBchReport = " AND SHOP_REPORTDATA.Rpt_Bch  = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
            }
            if (radCheckBox_Grp.CheckState == CheckState.Checked)
            {
                pTypeConfig = " AND SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = '" + radDropDownList_Grp.SelectedValue.ToString() + @"' ";
                pTypeReport = " AND Rpt_ID = '" + radDropDownList_Grp.SelectedValue.ToString() + @"' ";
            }

            var sql = $@"
                SELECT	BCH.*,
		                CONVERT(VARCHAR,ISNULL(RPT.Rpt_DATE,''),23) AS Rpt_DATE,ISNULL(RPT.Rpt_Bch,'') AS Rpt_Bch,ISNULL(RPT.Rpt_ID,'') AS Rpt_ID,
		                CASE TypeReport WHEN '1' THEN FORMAT(CONVERT(float,ISNULL(RPT.Rpt_Values,'0')), 'N') ELSE ISNULL(RPT.Rpt_Values,'') END AS Rpt_Values,
                        ISNULL(RPT.Rpt_Remark,'') AS Rpt_Remark,
		                ISNULL(RPT.WhoIns,'') AS WhoIns,ISNULL(RPT.WhoNameIns,'') AS WhoNameIns,ISNULL(DateIns,'') AS DateIns 
                FROM	(
		                SELECT	SHOP_CONFIGBRANCH_DETAIL.SHOW_ID,SHOP_CONFIGBRANCH_DETAIL.SHOW_NAME,SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID,BRANCH_NAME
                                ,SHOP_CONFIGBRANCH_GenaralDetail.MaxImage AS TypeReport 
		                FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) 
                                INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                                INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID 
                                    AND SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '28'
		                WHERE	SHOP_CONFIGBRANCH_DETAIL.TYPE_CONFIG = '28'  {pTypeConfig} {pBchConfig}
		                )BCH LEFT OUTER JOIN 
		                (	SELECT	Rpt_DATE,Rpt_Bch,Rpt_ID,Rpt_Values,Rpt_Remark,WhoIns,WhoNameIns,CONVERT(VARCHAR,DateIns,23)+' '+TimeIns AS DateIns
			                FROM	SHOP_REPORTDATA WITH (NOLOCK) 
			                WHERE	CONVERT(VARCHAR,Rpt_DATE,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' 
                                    AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'  {pTypeReport} {pBchReport}
		                )RPT ON BCH.BRANCH_ID = RPT.Rpt_Bch AND BCH.SHOW_ID = RPT.Rpt_ID

                ORDER BY SHOW_ID,BRANCH_ID ";
            DataTable dtData = ConnectionClass.SelectSQL_Main(sql);

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value.Date - radDateTimePicker_D1.Value.Date).TotalDays) + 1;

            string pConGrp = "";
            if (radCheckBox_Grp.CheckState == CheckState.Checked) pConGrp = $@" AND SHOW_ID = '{radDropDownList_Grp.SelectedValue}' ";

            DataTable dtGrp = FindGrpCase11_Report(pConGrp);
            for (int iGrp = 0; iGrp < dtGrp.Rows.Count; iGrp++)
            {
                string fGrpID = dtGrp.Rows[iGrp]["SHOW_ID"].ToString();
                string fGrpName = dtGrp.Rows[iGrp]["SHOW_Name"].ToString();

                DataTable dtBch = FindBchCase11_Report(fGrpID);
                for (int iBch = 0; iBch < dtBch.Rows.Count; iBch++)
                {
                    string fBchID = dtBch.Rows[iBch]["BRANCH_ID"].ToString();
                    string fBchName = dtBch.Rows[iBch]["BRANCH_Name"].ToString();
                    string fTypeReport = dtBch.Rows[iBch]["TypeReport"].ToString();

                    for (int iDay = 0; iDay < intDay; iDay++)
                    {
                        var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                        string r_values = "", r_Remark = "", r_WhoIns = "", r_WhoNameIns = "", r_DateIns = "";
                        if (dtData.Rows.Count > 0)
                        {
                            DataRow[] result = dtData.Select("Rpt_DATE = '" + pDate + @"' AND BRANCH_ID  = '" + fBchID + @"' AND SHOW_ID = '" + fGrpID + @"' ");
                            if (result.Length > 0)
                            {
                                r_values = result[0]["Rpt_Values"].ToString();
                                r_Remark = result[0]["Rpt_Remark"].ToString();
                                r_WhoIns = result[0]["WhoIns"].ToString();
                                r_WhoNameIns = result[0]["WhoNameIns"].ToString();
                                r_DateIns = result[0]["DateIns"].ToString();
                            }
                        }
                        dt_Data.Rows.Add(fGrpID, fGrpName, fBchID, fBchName, pDate,
                            r_values, r_Remark, r_WhoIns, r_WhoNameIns, r_DateIns, fTypeReport);
                    }
                }
            }

            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //12 //12 - รายงานประจำวัน สรุป
        void SetCase12()
        {
            string pConGrp = "";
            if (radCheckBox_Grp.CheckState == CheckState.Checked) pConGrp = $@" AND SHOW_ID = '{radDropDownList_Grp.SelectedValue}' ";

            DataTable dtGrp = FindGrpCase11_Report(pConGrp);
            string fGrpID = dtGrp.Rows[0]["SHOW_ID"].ToString();

            string fTypeReport = dtGrp.Rows[0]["MaxImage"].ToString();

            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                RadGridView_ShowHD.SummaryRowsTop.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 170)));
            RadGridView_ShowHD.Columns["BRANCH_ID"].IsPinned = true;
            RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value.Date - radDateTimePicker_D1.Value.Date).TotalDays) + 1;

            string col_Express = "";
            int cc;

            //เพิ่มคอลัม

            if (fTypeReport == "1")
            {
                cc = 2;
                for (int iDay = 0; iDay < intDay; iDay++)
                {
                    string headColume = "A" + Convert.ToString(iDay);
                    if (col_Express == "") col_Express += headColume; else col_Express += "+" + headColume;
                }
                //Sum Image
                GridViewDecimalColumn col = new GridViewDecimalColumn
                {
                    Name = "SumRows",
                    HeaderText = "ยอดรวมทั้งหมด",
                    IsVisible = true,
                    FormatString = "{0:n2}",
                    Width = 120,
                    Expression = col_Express
                };
                RadGridView_ShowHD.MasterTemplate.Columns.Add(col);

                GridViewSummaryItem summaryItemA = new GridViewSummaryItem("SumRows", "{0}", GridAggregateFunction.Sum)
                {
                    FormatString = "{0:n2}"
                };
                summaryRowItem.Add(summaryItemA);

                int i_Column = 0;
                for (int iDay = 0; iDay < intDay; iDay++)
                {
                    string headColume = "A" + Convert.ToString(iDay);
                    var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(headColume, pDate, 150)));

                    GridViewSummaryItem summaryItem = new GridViewSummaryItem(headColume, "{0}", GridAggregateFunction.Sum)
                    {
                        FormatString = "{0:n2}"
                    };
                    summaryRowItem.Add(summaryItem);

                    i_Column += 1;
                }


                this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
                this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
            }
            else
            {
                cc = 1;
                int i_Column = 0;
                for (int iDay = 0; iDay < intDay; iDay++)
                {
                    string headColume = "A" + Convert.ToString(iDay);
                    var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight(headColume, pDate, 150)));
                    i_Column += 1;
                }
            }

            string pBchConfig = "", pBchReport = "", pTypeConfig = "", pTypeReport = "";
            if (RadCheckBox_1.CheckState == CheckState.Checked)
            {
                pBchConfig = " AND SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID  = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
                pBchReport = " AND SHOP_REPORTDATA.Rpt_Bch  = '" + RadDropDownList_1.SelectedValue.ToString() + @"' ";
            }
            if (radCheckBox_Grp.CheckState == CheckState.Checked)
            {
                pTypeConfig = " AND SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = '" + radDropDownList_Grp.SelectedValue.ToString() + @"' ";
                pTypeReport = " AND Rpt_ID = '" + radDropDownList_Grp.SelectedValue.ToString() + @"' ";
            }

            var sql = $@"
                SELECT	BCH.*,
		                CONVERT(VARCHAR,ISNULL(RPT.Rpt_DATE,''),23) AS Rpt_DATE,ISNULL(RPT.Rpt_Bch,'') AS Rpt_Bch,ISNULL(RPT.Rpt_ID,'') AS Rpt_ID,
		                CASE TypeReport WHEN '1' THEN FORMAT(CONVERT(float,ISNULL(RPT.Rpt_Values,'0')), 'N') ELSE ISNULL(RPT.Rpt_Values,'') END AS Rpt_Values,
                        ISNULL(RPT.Rpt_Remark,'') AS Rpt_Remark,
		                ISNULL(RPT.WhoIns,'') AS WhoIns,ISNULL(RPT.WhoNameIns,'') AS WhoNameIns,ISNULL(DateIns,'') AS DateIns 
                FROM	(
		                SELECT	SHOP_CONFIGBRANCH_DETAIL.SHOW_ID,SHOP_CONFIGBRANCH_DETAIL.SHOW_NAME,SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID,BRANCH_NAME
                                ,SHOP_CONFIGBRANCH_GenaralDetail.MaxImage AS TypeReport 
		                FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) INNER JOIN SHOP_BRANCH WITH (NOLOCK)
					                ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                                INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID 
                                    AND SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '28'
		                WHERE	SHOP_CONFIGBRANCH_DETAIL.TYPE_CONFIG = '28' {pTypeConfig} {pBchConfig}
		                )BCH LEFT OUTER JOIN 
		                (	SELECT	Rpt_DATE,Rpt_Bch,Rpt_ID,Rpt_Values,Rpt_Remark,WhoIns,WhoNameIns,CONVERT(VARCHAR,DateIns,23)+' '+TimeIns AS DateIns 
			                FROM	SHOP_REPORTDATA WITH (NOLOCK) 
			                WHERE	CONVERT(VARCHAR,Rpt_DATE,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' 
                                    AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'  {pTypeReport}  {pBchReport}
		                )RPT ON BCH.BRANCH_ID = RPT.Rpt_Bch AND BCH.SHOW_ID = RPT.Rpt_ID
                ORDER BY SHOW_ID,BRANCH_ID ";
            DataTable dtData = ConnectionClass.SelectSQL_Main(sql);

            DataTable dtBch = FindBchCase11_Report(fGrpID);
            for (int iBch = 0; iBch < dtBch.Rows.Count; iBch++)
            {
                string fBchID = dtBch.Rows[iBch]["BRANCH_ID"].ToString();
                string fBchName = dtBch.Rows[iBch]["BRANCH_Name"].ToString();

                RadGridView_ShowHD.Rows.Add(fBchID, fBchName);

                for (int iDay = 0; iDay < intDay; iDay++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                    string r_values;
                    if (fTypeReport == "0")
                    {
                        r_values = "";
                    }
                    else
                    {
                        r_values = "0";
                    }

                    if (dtData.Rows.Count > 0)
                    {
                        DataRow[] result = dtData.Select("Rpt_DATE = '" + pDate + @"' AND BRANCH_ID  = '" + fBchID + @"' AND SHOW_ID = '" + fGrpID + @"' ");
                        if (result.Length > 0)
                        {
                            r_values = result[0]["Rpt_Values"].ToString();
                        }
                    }
                    string iicc = "A" + Convert.ToString(iDay);
                    RadGridView_ShowHD.Rows[iBch].Cells[iicc].Value = r_values;
                    cc++;
                }
            }

            this.Cursor = Cursors.Default;
        }
        //13รูปจัดสินค้า สรุป
        void SetCase13()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                RadGridView_ShowHD.SummaryRowsTop.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 250)));
            RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
            RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value.Date - radDateTimePicker_D1.Value.Date).TotalDays) + 1;

            string col_Express = "";
            //เพิ่มคอลัม

            for (int iDay = 0; iDay < intDay; iDay++)
            {
                string headColume = "A" + Convert.ToString(iDay);
                if (col_Express == "") col_Express += headColume; else col_Express += "+" + headColume;
            }
            //Sum Image
            GridViewDecimalColumn col = new GridViewDecimalColumn
            {
                Name = "SumRows",
                HeaderText = "รวมทั้งหมด",
                IsVisible = true,
                FormatString = "{0:n2}",
                Width = 120,
                Expression = col_Express
            };
            RadGridView_ShowHD.MasterTemplate.Columns.Add(col);

            GridViewSummaryItem summaryItemA = new GridViewSummaryItem("SumRows", "{0}", GridAggregateFunction.Sum)
            {
                FormatString = "{0:n2}"
            };
            summaryRowItem.Add(summaryItemA);

            int i_Column = 0;
            for (int iDay = 0; iDay < intDay; iDay++)
            {
                string headColume = "A" + Convert.ToString(iDay);
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(headColume, pDate, 75)));

                this.RadGridView_ShowHD.Columns[headColume].ConditionalFormattingObjectList.Add(
                          new ExpressionFormattingObject("MyCondition2", headColume + " > 0 ", false)
                          { CellBackColor = ConfigClass.SetColor_SkyPastel() });


                GridViewSummaryItem summaryItem = new GridViewSummaryItem(headColume, "{0}", GridAggregateFunction.Sum)
                {
                    FormatString = "{0:n2}"
                };
                summaryRowItem.Add(summaryItem);

                i_Column += 1;
            }


            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            //Data
            DataTable dtData;
            //if (RadCheckBox_1.Checked == true) dtData = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", $@" = '{RadDropDownList_1.SelectedValue}' ", "", "");
            if (RadCheckBox_1.Checked == true) dtData = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", RadDropDownList_1.SelectedValue.ToString(), "", "");
            else dtData = Models.EmplClass.GetEmployeeAll_ByDptID("0", "1", " LIKE 'MN%' ", "", "");

            for (int iEmp = 0; iEmp < dtData.Rows.Count; iEmp++)
            {
                string fEmpID = dtData.Rows[iEmp]["EMPLID"].ToString();
                string fEmpName = dtData.Rows[iEmp]["SPC_NAME"].ToString();
                string fEmpDptID = dtData.Rows[iEmp]["NUM"].ToString();
                string fEmpDptDesc = dtData.Rows[iEmp]["DESCRIPTION"].ToString();
                string fEmpPossion = dtData.Rows[iEmp]["POSSITION"].ToString();

                RadGridView_ShowHD.Rows.Add(fEmpID, fEmpName, fEmpDptID, fEmpDptDesc, fEmpPossion);

                for (int iDay = 0; iDay < intDay; iDay++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                    int r_values = 0;

                    string pathFileName = PathImageClass.pPathShelfNew + pDate + @"\" + fEmpDptID;

                    DirectoryInfo DirInfo = new DirectoryInfo(pathFileName);
                    if (DirInfo.Exists)
                    {
                        FileInfo[] Files = DirInfo.GetFiles("*" + fEmpID.Substring(1, 7) + @"*.JPG", SearchOption.AllDirectories);
                        r_values = Files.Length;
                    }
                    string iicc = "A" + Convert.ToString(iDay);
                    RadGridView_ShowHD.Rows[iEmp].Cells[iicc].Value = r_values;

                }
            }

            this.Cursor = Cursors.Default;
        }
        //14 รูปจัดสินค้า ละเอียด
        void SetCase14()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                RadGridView_ShowHD.SummaryRowsTop.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TimeIns", "เวลาเข้างาน", 190)));
            RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
            RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;

            double intDay = 2;
            string col_Express = "";
            //เพิ่มคอลัม

            for (int iDay = 0; iDay < intDay; iDay++)
            {
                string headColume = "A" + Convert.ToString(iDay);
                if (col_Express == "") col_Express += headColume; else col_Express += "+" + headColume;
            }
            //Sum Image
            GridViewDecimalColumn col = new GridViewDecimalColumn
            {
                Name = "SumRows",
                HeaderText = "รวมทั้งหมด",
                IsVisible = true,
                FormatString = "{0:n2}",
                Width = 80,
                Expression = col_Express
            };
            RadGridView_ShowHD.MasterTemplate.Columns.Add(col);

            GridViewSummaryItem summaryItemA = new GridViewSummaryItem("SumRows", "{0}", GridAggregateFunction.Sum)
            {
                FormatString = "{0:n2}"
            };
            summaryRowItem.Add(summaryItemA);

            int i_Column = 0;
            for (int iDay = 0; iDay < intDay; iDay++)
            {
                string headColume = "A" + Convert.ToString(iDay);
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(headColume, pDate, 75)));

                this.RadGridView_ShowHD.Columns[headColume].ConditionalFormattingObjectList.Add(
                          new ExpressionFormattingObject("MyCondition2", headColume + " = 0 ", false)
                          { CellBackColor = ConfigClass.SetColor_Red() });

                GridViewSummaryItem summaryItem = new GridViewSummaryItem(headColume, "{0}", GridAggregateFunction.Sum)
                {
                    FormatString = "{0:n2}"
                };
                summaryRowItem.Add(summaryItem);

                i_Column += 1;
            }


            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            //Data
            DataTable dtData;
            if (RadCheckBox_1.Checked == true) dtData = Models.EmplClass.GetEmployeeAllCheckTimeKeeper_ByDptID($@" = '{RadDropDownList_1.SelectedValue}' ", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"));
            else dtData = Models.EmplClass.GetEmployeeAllCheckTimeKeeper_ByDptID($@" LIKE  'MN%' ", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"));

            int iRow = 0;
            for (int iEmp = 0; iEmp < dtData.Rows.Count; iEmp++)
            {
                string fEmpID = dtData.Rows[iEmp]["EMPLID"].ToString();
                string fEmpName = dtData.Rows[iEmp]["SPC_NAME"].ToString();
                string fEmpDptID = dtData.Rows[iEmp]["NUM"].ToString();
                string fEmpDptDesc = dtData.Rows[iEmp]["DESCRIPTION"].ToString();
                string fEmpPossion = dtData.Rows[iEmp]["POSSITION"].ToString();
                string fEmpTime = dtData.Rows[iEmp]["TimeIns"].ToString();

                var pDateA = radDateTimePicker_D1.Value.AddDays(0).ToString("yyyy-MM-dd");
                int r_valuesA = 0;
                string pathFileNameA = PathImageClass.pPathShelfNew + pDateA + @"\" + fEmpDptID;

                DirectoryInfo DirInfoA = new DirectoryInfo(pathFileNameA);
                if (DirInfoA.Exists)
                {
                    FileInfo[] FilesA = DirInfoA.GetFiles("*" + fEmpID.Substring(1, 7) + @"*.JPG", SearchOption.AllDirectories);
                    r_valuesA = FilesA.Length;
                }

                var pDateB = radDateTimePicker_D1.Value.AddDays(1).ToString("yyyy-MM-dd");
                int r_valuesB = 0;
                string pathFileNameB = PathImageClass.pPathShelfNew + pDateB + @"\" + fEmpDptID;

                DirectoryInfo DirInfoB = new DirectoryInfo(pathFileNameB);
                if (DirInfoB.Exists)
                {
                    FileInfo[] FilesB = DirInfoB.GetFiles("*" + fEmpID.Substring(1, 7) + @"*.JPG", SearchOption.AllDirectories);
                    r_valuesB = FilesB.Length;
                }

                if (radRadioButton_P2.CheckState == CheckState.Checked)
                {
                    if ((r_valuesA + r_valuesB) == 0)
                    {
                        RadGridView_ShowHD.Rows.Add(fEmpID, fEmpName, fEmpDptID, fEmpDptDesc, fEmpPossion, fEmpTime);
                        RadGridView_ShowHD.Rows[iRow].Cells["A0"].Value = r_valuesA;
                        RadGridView_ShowHD.Rows[iRow].Cells["A1"].Value = r_valuesB;
                        iRow++;
                    }
                }
                else
                {
                    RadGridView_ShowHD.Rows.Add(fEmpID, fEmpName, fEmpDptID, fEmpDptDesc, fEmpPossion, fEmpTime);
                    RadGridView_ShowHD.Rows[iRow].Cells["A0"].Value = r_valuesA;
                    RadGridView_ShowHD.Rows[iRow].Cells["A1"].Value = r_valuesB;
                    iRow++;
                }

            }
            this.Cursor = Cursors.Default;
        }
        //16 สต็อก หมู ไก่
        void SetCase16()
        {
            Report16("1");
        }
        //16 ดึงข้อมูลจำนวนสั่งตามสาขา
        double GetSumQty(DataTable dtsum, string pCon)
        {
            DataRow[] sall = dtsum.Select(pCon);
            double sAll = 0;
            if (sall.Length > 0) sAll = double.Parse(sall[0]["ItemAll"].ToString());
            return sAll;
        }
        //17 อัตราการขายย้อนหลัง 3 เดือน
        void SetCase17()
        {
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            string bchID = RadDropDownList_1.SelectedValue.ToString();
            string num = radDropDownList_Grp.SelectedValue.ToString();

            //string sql = string.Format(@"
            //        SELECT	ITEMBARCODE,INVENTITEMBARCODE_MINMAX.ITEMID,INVENTITEMBARCODE_MINMAX.INVENTDIMID,SPC_ITEMNAME, 
            //        INVENTITEMBARCODE_MINMAX.SPC_ITEMBUYERGROUPID,INVENTITEMBARCODE_MINMAX.CONFIGID,INVENTITEMBARCODE_MINMAX.INVENTSIZEID,INVENTITEMBARCODE_MINMAX.INVENTCOLORID, 
            //        ISNULL(TMPSALE.SALEQTY,0) AS SALEQTY,ISNULL(TMPPR.QTYPR,0) AS QTYPR,ISNULL(TMPPC.PCQTY,0) AS PCQTY 
            //        ,CASE ISNULL(SALEQTY,0) WHEN 0 THEN '0' ELSE ISNULL(SALEQTY,0)/3 END AS PER_MONTH,  
            //        (ISNULL(QTYPR,0)-ISNULL(SALEQTY,0)-ISNULL(PCQTY,0)) AS QTYHAVE, 
            //        CASE ISNULL(SALEQTY,0) WHEN 0 THEN '100' ELSE (ISNULL(QTYPR,0)-ISNULL(SALEQTY,0)-ISNULL(PCQTY,0))/(ISNULL(SALEQTY,0)/3) END AS PER_SALE 
            //        FROM	dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) 
            //        LEFT OUTER JOIN 
            //        ( 
            //        SELECT	SUM(INVENTQTY) AS SALEQTY,INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
            //        FROM	dbo.VIEW_PT_RETAIL	WITH (NOLOCK)  
            //        WHERE	BRANCH = '" + bchID + @"'   
            //        AND TimePeriod BETWEEN GETDATE()-90 AND GETDATE()  
            //        AND ENTITY = '" + num + @"' 
            //        GROUP BY INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
            //        )TMPSALE	ON INVENTITEMBARCODE_MINMAX.CONFIGID  = TMPSALE.CONFIGID 
            //        AND INVENTITEMBARCODE_MINMAX.INVENTSIZEID  = TMPSALE.INVENTSIZEID 
            //        AND INVENTITEMBARCODE_MINMAX.INVENTCOLORID  = TMPSALE.INVENTCOLORID 
            //        AND INVENTITEMBARCODE_MINMAX.ITEMID  = TMPSALE.ITEMID 
            //        LEFT OUTER JOIN  
            //        ( SELECT	 SUM(INVENTQTY) AS QTYPR	,INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
            //        FROM	dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)  
            //        WHERE	BRANCH = '" + bchID + @"'   
            //        AND TRANSTYPE = '6'  
            //        AND TimePeriod BETWEEN GETDATE()-90 AND GETDATE()  
            //        AND ENTITY = '" + num + @"' 
            //        GROUP BY INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
            //        )TMPPR	ON INVENTITEMBARCODE_MINMAX.CONFIGID  = TMPPR.CONFIGID 
            //        AND INVENTITEMBARCODE_MINMAX.INVENTSIZEID  = TMPPR.INVENTSIZEID 
            //        AND INVENTITEMBARCODE_MINMAX.INVENTCOLORID  = TMPPR.INVENTCOLORID 
            //        AND INVENTITEMBARCODE_MINMAX.ITEMID  = TMPPR.ITEMID 
            //        LEFT OUTER JOIN  
            //        ( SELECT	 SUM(INVENTQTY) AS PCQTY	,INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
            //        FROM	dbo.PT_PURCHASE_TRANSFERSHIP	WITH (NOLOCK)   
            //        WHERE	BRANCH = '" + bchID + @"'   
            //        AND TRANSTYPE = '7'   
            //        AND TimePeriod BETWEEN GETDATE()-90 AND GETDATE()   
            //        AND ENTITY = '" + num + @"'
            //        GROUP BY INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
            //        )TMPPC	ON INVENTITEMBARCODE_MINMAX.CONFIGID  = TMPPC.CONFIGID 
            //        AND INVENTITEMBARCODE_MINMAX.INVENTSIZEID  = TMPPC.INVENTSIZEID 
            //        AND INVENTITEMBARCODE_MINMAX.INVENTCOLORID  = TMPPC.INVENTCOLORID 
            //        AND INVENTITEMBARCODE_MINMAX.ITEMID  = TMPPC.ITEMID 
            //        WHERE	SPC_ITEMBUYERGROUPID = '" + num + @"' 
            //        AND TYPE_ = 'MIN' 
            //        AND SPC_ITEMACTIVE = '1'  
            //        AND ISNULL(TMPSALE.SALEQTY,0) + ISNULL(TMPPR.QTYPR,0) + (ISNULL(TMPPC.PCQTY,0)*-1)   > 0
            //");
            DataTable dtAll = PosSaleClass.GetDataSaleTOPC_ByNum(bchID, num); //ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
            for (int ik = 0; ik < dtAll.Rows.Count; ik++)
            {
                string sta = "0";
                if (double.Parse(dtAll.Rows[ik]["PER_SALE"].ToString()) > 10)
                {
                    sta = "1";
                }
                string barcode = dtAll.Rows[ik]["ITEMBARCODE"].ToString();
                double stk = ItembarcodeClass.FindStock_ByBarcode(barcode, bchID);
                dt_Data.Rows.Add(barcode, dtAll.Rows[ik]["SPC_ITEMNAME"].ToString(),
                    double.Parse(dtAll.Rows[ik]["QTYPR"].ToString()).ToString("#,#0.00"),
                    double.Parse(dtAll.Rows[ik]["SALEQTY"].ToString()).ToString("#,#0.00"),
                    double.Parse(dtAll.Rows[ik]["PCQTY"].ToString()).ToString("#,#0.00"),
                    double.Parse(dtAll.Rows[ik]["PER_MONTH"].ToString()).ToString("#,#0.00"),
                    double.Parse(dtAll.Rows[ik]["QTYHAVE"].ToString()).ToString("#,#0.00"),
                    stk.ToString("#,#0.00"),
                    double.Parse(dtAll.Rows[ik]["PER_SALE"].ToString()).ToString("#,#0.00"), sta);
            }
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //18 อัตราการขายย้อนหลัง 3 เดือน ตามกลุ่ม
        void SetCase18()
        {
            if (dt_Data.Rows.Count > 0) dt_Data.Rows.Clear();

            string bchID = RadDropDownList_1.SelectedValue.ToString();
            string grp = radDropDownList_Grp.SelectedValue.ToString();

            //string sql = string.Format(@"
            //        SELECT	ITEMBARCODE,INVENTITEMBARCODE_MINMAX.ITEMID,INVENTITEMBARCODE_MINMAX.INVENTDIMID,SPC_ITEMNAME, 
            //          INVENTITEMBARCODE_MINMAX.SPC_ITEMBUYERGROUPID,DIMENSIONNAME,INVENTITEMBARCODE_MINMAX.CONFIGID,
            //          INVENTITEMBARCODE_MINMAX.INVENTSIZEID,INVENTITEMBARCODE_MINMAX.INVENTCOLORID, 
            //          ISNULL(TMPSALE.SALEQTY,0) AS SALEQTY    ,CASE ISNULL(SALEQTY,0) WHEN 0 THEN '0' ELSE ISNULL(SALEQTY,0)/3 END AS PER_MONTH             
            //        FROM	dbo.INVENTITEMBARCODE_MINMAX WITH (NOLOCK) 
            //          LEFT OUTER JOIN 
            //          ( 
            //           SELECT	SUM(INVENTQTY) AS SALEQTY,INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
            //           FROM	dbo.VIEW_PT_RETAIL	WITH (NOLOCK)  
            //           WHERE	BRANCH = '" + bchID + @"'   
            //           AND TimePeriod BETWEEN GETDATE()-90 AND GETDATE()  
            //           GROUP BY INVENTSITEID,CONFIGID,INVENTSIZEID,INVENTCOLORID,ITEMID 
            //          )TMPSALE	ON INVENTITEMBARCODE_MINMAX.CONFIGID  = TMPSALE.CONFIGID 
            //           AND INVENTITEMBARCODE_MINMAX.INVENTSIZEID  = TMPSALE.INVENTSIZEID 
            //           AND INVENTITEMBARCODE_MINMAX.INVENTCOLORID  = TMPSALE.INVENTCOLORID 
            //           AND INVENTITEMBARCODE_MINMAX.ITEMID  = TMPSALE.ITEMID        
            //          LEFT OUTER JOIN dbo.RPTINVENTTABLE on dbo.INVENTITEMBARCODE_MINMAX.ITEMID = dbo.RPTINVENTTABLE.ITEMID 
            //        WHERE	TYPE_ = 'MIN' 
            //          AND SPC_ITEMACTIVE = '1'  
            //          AND INVENTGROUPID = '" + grp + @"'
            //        ORDER BY SPC_ITEMNAME			
            //");
            DataTable dtAll = PosSaleClass.GetDataSale_ByGroupID(bchID, grp); //ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.ConMainReportProcess, 200);
            for (int ik = 0; ik < dtAll.Rows.Count; ik++)
            {
                string barcode = dtAll.Rows[ik]["ITEMBARCODE"].ToString();
                double stk = ItembarcodeClass.FindStock_ByBarcode(barcode, bchID);
                double PER_MONTH = double.Parse(dtAll.Rows[ik]["PER_MONTH"].ToString());
                double SALEQTY = double.Parse(dtAll.Rows[ik]["SALEQTY"].ToString());
                double haveStock = 0;
                if (stk > 0)
                {
                    if (SALEQTY == 0) haveStock = 100; else haveStock = (stk / PER_MONTH);
                }
                string sta = "0";
                if (haveStock > 3)
                {
                    if (haveStock == 100) sta = "2"; else sta = "1";
                }
                if (stk < 0) sta = "3";
                if ((stk == 0) && (SALEQTY == 0)) sta = "4";
                if (sta != "4")
                {
                    dt_Data.Rows.Add(barcode, dtAll.Rows[ik]["SPC_ITEMNAME"].ToString(),
                   SALEQTY.ToString("#,#0.00"),
                   PER_MONTH.ToString("#,#0.00"),
                   stk.ToString("#,#0.00"),
                   haveStock.ToString("#,#0.00"), sta);
                }
            }
            RadGridView_ShowHD.FilterDescriptors.Clear();
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //19 - รายการการปรับเปลี่ยนลูกค้าในบิลขาย
        void SetCase19()
        {
            this.Cursor = Cursors.WaitCursor;
            string sql = $@"
            SELECT	INVOICEID,ACCOUNTNUM,ACCOUNTNUMNAME,ACCOUNTNUMOLD,ACCOUNTNUMOLDNAME,CONVERT(VARCHAR,TRANSDATE,23) AS TRANDATE,
		            INVOICEAMOUNT,FORMAT(NEWPOINT,'N2') AS NEWPOINT,POSGROUP,POSGROUPNAME,
		            CASHIERID,CASHIERIDNAME,REMARK,
		            WHOID,WHONAME,CONVERT(VARCHAR,CREATEDATEIME,25) AS CREATEDATEIME
            FROM	[SHOP_SPC_CUSTPOINTHISTORY] WITH (NOLOCK)
            WHERE	CONVERT(VARCHAR,CREATEDATEIME,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}'  AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
            ORDER BY CONVERT(VARCHAR,CREATEDATEIME,24)
            ";
            dt_Data = ConnectionClass.SelectSQL_Main(sql);
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //20 void สรุป
        void SetCase20()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                RadGridView_ShowHD.SummaryRowsTop.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLKBFUNCTION", "รหัสพนักงาน", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 250)));
            RadGridView_ShowHD.Columns["EMPLKBFUNCTION"].IsPinned = true;
            RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;

            double intDay = Math.Ceiling((radDateTimePicker_D2.Value.Date - radDateTimePicker_D1.Value.Date).TotalDays) + 1;

            string col_Express = "";
            //เพิ่มคอลัม
            for (int iDay = 0; iDay < intDay; iDay++)
            {
                string headColume = "A" + Convert.ToString(iDay);
                if (col_Express == "") col_Express += headColume; else col_Express += "+" + headColume;
            }
            //Sum จำนนวน/คน
            GridViewDecimalColumn col = new GridViewDecimalColumn
            {
                Name = "SumRows",
                HeaderText = "รวมทั้งหมด",
                IsVisible = true,
                FormatString = "{0:n2}",
                Width = 120,
                Expression = col_Express
            };
            RadGridView_ShowHD.MasterTemplate.Columns.Add(col);

            GridViewSummaryItem summaryItemA = new GridViewSummaryItem("SumRows", "{0}", GridAggregateFunction.Sum)
            {
                FormatString = "{0:n2}"
            };
            summaryRowItem.Add(summaryItemA);

            int i_Column = 0;
            for (int iDay = 0; iDay < intDay; iDay++)
            {
                string headColume = "A" + Convert.ToString(iDay);
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(headColume, pDate, 85)));

                this.RadGridView_ShowHD.Columns[headColume].ConditionalFormattingObjectList.Add(
                          new ExpressionFormattingObject("MyCondition2", headColume + " = 0 ", false)
                          { CellBackColor = ConfigClass.SetColor_Wheat() });

                GridViewSummaryItem summaryItem = new GridViewSummaryItem(headColume, "{0}", GridAggregateFunction.Sum)
                {
                    FormatString = "{0:n2}"
                };
                summaryRowItem.Add(summaryItem);

                i_Column += 1;
            }

            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            //Data
            //string sqlData = $@" 
            //    SELECT  CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME,23) AS DateSale, 
            //      EMPLKBFUNCTION,SPC_NAME,COUNT(EMPLKBFUNCTION) AS COUNTVOID

            //    FROM	POSLINEVOID WITH (NOLOCK) 
            //      INNER JOIN EMPLTABLE TMP WITH (NOLOCK) ON POSLINEVOID.EMPLKBFUNCTION  = TMP.EMPLID

            //    WHERE	CONVERT(VARCHAR(10),POSLINEVOID.CREATEDDATETIME ,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' 
            //      AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' AND DIMENSION = 'D054'

            //    GROUP BY  CONVERT(VARCHAR,POSLINEVOID.CREATEDDATETIME,23),EMPLKBFUNCTION,SPC_NAME
            //";
            DataTable dtDataShow = PosSaleClass.GetDetailSale_BillVoid("3", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), ""); //ConnectionClass.SelectSQL_POSRetail707(sqlData);
            DataTable dataEmp = PosSaleClass.GetDetailSale_BillVoid("4", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "");
            //    ConnectionClass.SelectSQL_POSRetail707($@"
            //    SELECT	EMPLKBFUNCTION,SPC_NAME
            //    FROM	POSLINEVOID WITH (NOLOCK) 
            //      INNER JOIN EMPLTABLE TMP WITH (NOLOCK) ON POSLINEVOID.EMPLKBFUNCTION  = TMP.EMPLID
            //    WHERE	CONVERT(VARCHAR(10),POSLINEVOID.CREATEDDATETIME ,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' 
            //      AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}' AND DIMENSION = 'D054' 
            //    GROUP BY EMPLKBFUNCTION,SPC_NAME
            //    ORDER BY EMPLKBFUNCTION,SPC_NAME
            //");

            for (int iEmp = 0; iEmp < dataEmp.Rows.Count; iEmp++)
            {
                string fEmpID = dataEmp.Rows[iEmp]["EMPLKBFUNCTION"].ToString();
                string fEmpName = dataEmp.Rows[iEmp]["SPC_NAME"].ToString();
                RadGridView_ShowHD.Rows.Add(fEmpID, fEmpName);

                for (int iDay = 0; iDay < intDay; iDay++)
                {
                    var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                    Double r_values = 0;
                    DataRow[] result = dtDataShow.Select("DateSale = '" + pDate + @"' AND EMPLKBFUNCTION  = '" + fEmpID + @"'   ");
                    if (result.Length > 0)
                    {
                        r_values = Convert.ToDouble(result[0]["COUNTVOID"].ToString());
                    }
                    string iicc = "A" + Convert.ToString(iDay);
                    RadGridView_ShowHD.Rows[iEmp].Cells[iicc].Value = r_values;
                }
            }

            this.Cursor = Cursors.Default;
        }
        //21 รายการงานเบิกสินค้า
        void SetCase21()
        {
            this.Cursor = Cursors.WaitCursor;
            string sql = $@"
            SELECT	MNRRBRANCH,BRANCH_NAME,MNRRBarcode,MNRRName,SUM(MNRRQtyOrder) AS MNRRQtyOrder,MNRRQtyUnitID,SUM(MNRRPriceSum) AS MNRRPriceSum,SHOP_MNRR_HD.MNRRRemark
            FROM	SHOP_MNRR_HD WITH (NOLOCK)	
		            INNER JOIN SHOP_MNRR_DT WITH (NOLOCK) ON SHOP_MNRR_HD.MNRRDOCNO = SHOP_MNRR_DT.MNRRDOCNO
		            INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNRR_HD.MNRRBRANCH = SHOP_BRANCH.BRANCH_ID
            WHERE	CONVERT(VARCHAR,SHOP_MNRR_HD.MNRRDate,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
		            AND MNRRStaDoc = '1'
		            AND MNRRSta = '1'
		            AND MNRRQtyOrder > 0
            GROUP BY MNRRBRANCH,BRANCH_NAME,MNRRBarcode,MNRRName,MNRRQtyUnitID,SHOP_MNRR_HD.MNRRRemark
            ORDER BY MNRRBRANCH,MNRRName		
            ";
            dt_Data = ConnectionClass.SelectSQL_Main(sql);
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        DataTable FindGrpCase11_Report(string pCon)
        {
            var sql = $@"
                        SELECT	SHOW_ID,SHOW_NAME,MaxImage
                        FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
                        WHERE	TYPE_CONFIG = '28' AND STA = '1' {pCon}
                        ORDER BY SHOW_ID ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        DataTable FindBchCase11_Report(string pTypeGrp)
        {
            string pConBch = "";
            if (RadCheckBox_1.CheckState == CheckState.Checked) pConBch = " AND SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = '" + RadDropDownList_1.SelectedValue.ToString() + "' ";

            var sql = string.Format(@"                 
						SELECT	SHOP_CONFIGBRANCH_DETAIL.SHOW_ID,SHOP_CONFIGBRANCH_DETAIL.SHOW_NAME,SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID,BRANCH_NAME
                                ,SHOP_CONFIGBRANCH_GenaralDetail.MaxImage AS TypeReport 
		                FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) INNER JOIN SHOP_BRANCH WITH (NOLOCK)
					                ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = SHOP_BRANCH.BRANCH_ID 
                                INNER JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID 
                                    AND SHOP_CONFIGBRANCH_GenaralDetail.TYPE_CONFIG = '28' AND STA = '1'  
		                WHERE	SHOP_CONFIGBRANCH_DETAIL.TYPE_CONFIG = '28'  AND SHOP_CONFIGBRANCH_DETAIL.SHOW_ID = '" + pTypeGrp + @"' " + pConBch + @"
						ORDER BY SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID
                    ");
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;


            switch (_pTypeReport)
            {
                case "11":
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1); radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                    break;
                case "12":
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-7); radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1);
                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                        summaryRowItem.Clear();
                        RadGridView_ShowHD.SummaryRowsTop.Clear();
                    }
                    break;
                case "13":
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1); radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                        summaryRowItem.Clear();
                        RadGridView_ShowHD.SummaryRowsTop.Clear();
                    }
                    break;
                case "16":
                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        RadGridView_ShowHD.Columns.Clear();
                        RadGridView_ShowHD.Rows.Clear();
                    }
                    break;
                case "17":
                    RadCheckBox_1.Checked = true; RadCheckBox_1.Enabled = false;
                    break;
                default:
                    break;
            }

            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
        }

        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            if (_pTypeReport == "11")
            {
                //ID
                string pBchID = RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                if ((SystemClass.SystemBranchID != "MN000") && (pBchID != "") && (RadGridView_ShowHD.CurrentRow.Cells["Rpt_Values"].Value.ToString() != ""))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่สามารถแก้ไขรายงานได้" + Environment.NewLine + "ต้องการแก้ไขข้อมูลติดต่อ CenterShop Tel.1022");
                    return;
                }

                string pInputValues = RadGridView_ShowHD.CurrentRow.Cells["Rpt_Values"].Value.ToString();
                string pInputRemark = RadGridView_ShowHD.CurrentRow.Cells["Rpt_Remark"].Value.ToString();

                string pTypeReport = RadGridView_ShowHD.CurrentRow.Cells["TypeReport"].Value.ToString();
                string showData = "ประจำสาขา " + RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + '-' +
                    RadGridView_ShowHD.CurrentRow.Cells["BRANCH_NAME"].Value.ToString() + Environment.NewLine +
                    "ประจำวันที่ " + RadGridView_ShowHD.CurrentRow.Cells["Rpt_DATE"].Value.ToString();
                string showDataInput = RadGridView_ShowHD.CurrentRow.Cells["SHOW_NAME"].Value.ToString();
                Report_InputData _InputData_Report = new Report_InputData(pTypeReport, showData, showDataInput, "")
                {
                    pInputData = pInputValues,
                    pInputRemark = pInputRemark
                };
                if (_InputData_Report.ShowDialog(this) == DialogResult.Yes)
                {
                    string sql;
                    if (RadGridView_ShowHD.CurrentRow.Cells["Rpt_Values"].Value.ToString() == "")
                    {
                        sql = string.Format(@"INSERT INTO SHOP_REPORTDATA   
                    (Rpt_DATE,Rpt_Bch,Rpt_ID,Rpt_Values,WhoUser,Rpt_Remark,WhoNameIns,WhoIns,Rpt_Name) VALUES (
                     '" + RadGridView_ShowHD.CurrentRow.Cells["Rpt_DATE"].Value.ToString() + @"',
                     '" + RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + @"',
                     '" + RadGridView_ShowHD.CurrentRow.Cells["SHOW_ID"].Value.ToString() + @"',
                     '" + _InputData_Report.pInputData + @"','" + SystemClass.SystemUserID + @"','" + _InputData_Report.pInputRemark + @"',
                     '" + _InputData_Report.pEmplName + @"',
                     '" + _InputData_Report.pEmplID + @"',
                     '" + RadGridView_ShowHD.CurrentRow.Cells["SHOW_NAME"].Value.ToString() + @"'
                     )       
                ");
                    }
                    else
                    {
                        sql = string.Format(@"
                    Update  SHOP_REPORTDATA  Set Rpt_Values = '" + _InputData_Report.pInputData + @"',
                            Rpt_Remark = '" + _InputData_Report.pInputRemark + @"',
                            DateUpd = CONVERT(VARCHAR,GETDATE(),23),TimeUpd = CONVERT(VARCHAR,GETDATE(),24) ,  
                            WhoUpd = '" + _InputData_Report.pEmplID + @"' 
                    WHERE   Rpt_DATE = '" + RadGridView_ShowHD.CurrentRow.Cells["Rpt_DATE"].Value.ToString() + @"' AND 
                            Rpt_Bch = '" + RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString() + @"' AND 
                            Rpt_ID = '" + RadGridView_ShowHD.CurrentRow.Cells["SHOW_ID"].Value.ToString() + @"' 
                    ");
                    }
                    //WHERE ID = '" + RadGridView_ShowHD.CurrentRow.Cells["ID"].Value.ToString() + @"' 
                    //[] = '2020-08-30' AND [] = 'MN030' AND [] = 'A08'
                    string T = ConnectionClass.ExecuteSQL_Main(sql);
                    if (T == "")
                    {
                        SetDGV_HD();
                    }
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    return;
                }
                else
                {
                    return;
                }
            }

            if (_pTypeReport == "13")
            {
                if (RadGridView_ShowHD.CurrentCell.ColumnIndex > 5)
                {
                    try { if (int.Parse((RadGridView_ShowHD.CurrentCell.Value.ToString())) == 0) { return; } }
                    catch (Exception) { return; }

                    string pathFileNameB = PathImageClass.pPathShelfNew + RadGridView_ShowHD.CurrentCell.ColumnInfo.HeaderText + @"\" +
                        RadGridView_ShowHD.CurrentRow.Cells["NUM"].Value.ToString();

                    string path = pathFileNameB + @"\" + "|" + "*" + RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString().Substring(1, 7) + "*.JPG";

                    ImageClass.OpenShowImage("1", path);
                }

            }


            if (_pTypeReport == "14")
            {
                if ((RadGridView_ShowHD.CurrentCell.ColumnIndex == 7) || (RadGridView_ShowHD.CurrentCell.ColumnIndex == 8))
                {
                    try { if (int.Parse((RadGridView_ShowHD.CurrentCell.Value.ToString())) == 0) { return; } }
                    catch (Exception) { return; }

                    string pathFileNameB = PathImageClass.pPathShelfNew + RadGridView_ShowHD.CurrentCell.ColumnInfo.HeaderText + @"\" +
                        RadGridView_ShowHD.CurrentRow.Cells["NUM"].Value.ToString();

                    string path = pathFileNameB + @"\" + "|" + "*" + RadGridView_ShowHD.CurrentRow.Cells["EMPLID"].Value.ToString().Substring(1, 7) + "*.JPG";

                    ImageClass.OpenShowImage("1", path);
                }

            }
        }
        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }

        //16 - การส่งสต็อกหมู-ไก่/ผัก/ผลไม้
        void Report16(string pType)
        {
            if (pType == "0")
            {
                radLabel_Detail.Visible = false;
                radDateTimePicker_D2.Visible = false;
                RadCheckBox_1.Visible = true; RadDropDownList_1.Visible = true;
                if (SystemClass.SystemBranchID == "MN000")
                {
                    RadDropDownList_1.DataSource = BranchClass.GetBranchAll("'1'", "'1'");
                    RadCheckBox_1.Enabled = true; RadCheckBox_1.CheckState = CheckState.Unchecked;
                }
                else
                {
                    RadDropDownList_1.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;
                }
                RadDropDownList_1.DisplayMember = "NAME_BRANCH";
                RadDropDownList_1.ValueMember = "BRANCH_ID";

            }
            else
            {
                this.Cursor = Cursors.WaitCursor;
                if (RadGridView_ShowHD.Columns.Count > 0)
                {
                    RadGridView_ShowHD.Columns.Clear();
                    RadGridView_ShowHD.Rows.Clear();
                }
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 250)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("D032", $@"หมู-ไก่{Environment.NewLine}05.00-10.00", 90)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("D110", $@"ผัก{Environment.NewLine}10.00-12.00", 90)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("D164", $@"ผลไม้{Environment.NewLine}05.00-07.00", 90)));

                DatagridClass.SetCellBackClolorByExpression("D032", "D032 = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("D110", "D110 = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression("D164", "D164 = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                DataTable dt_Bch;
                if (RadCheckBox_1.Checked == true) dt_Bch = BranchClass.GetDetailBranchByID(RadDropDownList_1.SelectedValue.ToString());
                else dt_Bch = BranchClass.GetBranchAll("'1'", "'1'");


                DataTable dtA = ItembarcodeClass.MNOS_GetItemSendStockByBuyer(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"));

                if (dt_Bch.Rows.Count > 0)
                {
                    for (int iR = 0; iR < dt_Bch.Rows.Count; iR++)
                    {
                        string bch = dt_Bch.Rows[iR]["BRANCH_ID"].ToString();
                        string branchName = dt_Bch.Rows[iR]["BRANCH_NAME"].ToString();

                        double d032 = GetSumQty(dtA, $@" SPC_ITEMBUYERGROUPID = 'D032' AND MNOSBranch = '{bch}'");
                        double d110 = GetSumQty(dtA, $@" SPC_ITEMBUYERGROUPID = 'D110' AND MNOSBranch = '{bch}'");
                        double d164 = GetSumQty(dtA, $@" SPC_ITEMBUYERGROUPID = 'D164' AND MNOSBranch = '{bch}'");

                        RadGridView_ShowHD.Rows.Add(bch, branchName, d032, d110, d164);
                    }
                }

                this.Cursor = Cursors.Default;
            }
        }
    }
}

