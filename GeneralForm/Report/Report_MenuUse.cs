﻿//CheckOK
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.Windows.Diagrams.Core;
using System.Collections.Generic;
using System.Linq;
using PC_Shop24Hrs.GeneralForm.Employee;

namespace PC_Shop24Hrs.GeneralForm.Report
{
    public partial class Report_MenuUse : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        DataTable dtBranch = new DataTable();
        //string pConfigDB;
        public Report_MenuUse()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape) this.Close(); else base.OnKeyDown(e);
        }
        //Load
        private void Report_MenuUse_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_clear.ShowBorder = true; radButtonElement_clear.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export To Excel";
            radStatusStrip1.SizingGrip = false;
            RadButton_Search.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Beg, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_TypeUse);
            radCheckBox_CheckScal.ButtonElement.Font = SystemClass.SetFontGernaral;

            radLabel_Detail.Text = "ข้อมูลแสดงจำนวนครั้งที่ทำในแต่ละวัน";

            dtBranch = dtBranch = BranchClass.GetBranchAll("'1'", "'1'");
            RadDropDownList_TypeUse.DataSource = GetUseType();
            RadDropDownList_TypeUse.ValueMember = "Shop_StaPlace";
            RadDropDownList_TypeUse.DisplayMember = "Shop_Desc";
            RadDropDownList_TypeUse.SelectedValue = "SendScaleShop";
            radDateTimePicker_Beg.Value = DateTime.Now.AddDays(-7);
        }
        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion
        //Clear
        private void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            if (RadGridView_ShowHD.MasterTemplate.Rows.Count > 0) RadGridView_ShowHD.DataSource = null;
            RadButton_Search.Focus();
        }
        private void BindGrid()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            if (RadGridView_ShowHD.MasterTemplate.Columns.Count > 0) RadGridView_ShowHD.MasterTemplate.Columns.Clear();

            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 100));
            RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 200));

            if (radCheckBox_CheckScal.Checked == true)
            {
                dt_Data = GeneralForm.Employee.Employee_Class.AddNewColumn(new string[] { "BRANCH_ID", "BRANCH_NAME", "ROW" }, new string[] { "BRANCH_ID" });
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("ROW", "ข้อมูลค้างส่ง", 100));
                List<BranchServerIp> list_Server = GetBranchServerIP();
                if (list_Server.Count > 0)
                {
                    for (int i = 0; i < dtBranch.Rows.Count; i++)
                    {
                        DataRow dr = dt_Data.NewRow();
                        dr["BRANCH_ID"] = dtBranch.Rows[i]["BRANCH_ID"].ToString();
                        dr["BRANCH_NAME"] = dtBranch.Rows[i]["BRANCH_NAME"].ToString();

                        var _list_Server = list_Server.Where(a => a.BRANCH_ID.Equals(dtBranch.Rows[i]["BRANCH_ID"].ToString())).ToList();
                        foreach (var list in _list_Server)
                        {
                            dr["ROW"] = DataScalrow(@"Data Source= " + list.BRANCH_SERVER_IP + " ;Initial Catalog= ScaleSHOP;User Id=sa;Password=Ca999999");
                        }
                        dt_Data.Rows.Add(dr);
                    }
                    RadGridView_ShowHD.DataSource = dt_Data;
                }
            }
            else
            {
                dt_Data = GeneralForm.Employee.Employee_Class.AddNewColumn(new string[] { "BRANCH_ID", "BRANCH_NAME" }, new string[] { "BRANCH_ID" });
                string[] col = GetColumnsDate();
                AppendNewColumn(dt_Data, col);  //เพิ่ม new column 
                DataTable dt = Getedata();
                col.ForEach(colNameDate => this.SetConditions(string.Format(@"{0} <> '0'", colNameDate), colNameDate, true, Color.LightSkyBlue, colNameDate));

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBranch.Rows.Count; i++)
                    {
                        DataRow dr = dt_Data.NewRow();
                        dr["BRANCH_ID"] = dtBranch.Rows[i]["BRANCH_ID"].ToString();
                        dr["BRANCH_NAME"] = dtBranch.Rows[i]["BRANCH_NAME"].ToString();
                        foreach (var columnsDate in col)
                        {
                            DateTime time = DateTime.Parse(columnsDate.Substring(4, 4) + "-" + columnsDate.Substring(8, 2) + "-" + columnsDate.Substring(10, 2));
                            dr[columnsDate] = 0;
                            List<LogUse> Loglist = (from DataRow drs in dt.Select().Where(e => e["LOGBRANCH"].ToString().Equals(dtBranch.Rows[i]["BRANCH_ID"].ToString())
                                                    && Convert.ToDateTime(e["LOGDATETIME"]).ToString("yyyy-MM-dd").Equals(time.ToString("yyyy-MM-dd")))
                                                    select new LogUse()
                                                    {
                                                        LOGBRANCH = drs["LOGBRANCH"].ToString(),
                                                        LOGDATETIME = drs["LOGDATETIME"].ToString(),
                                                        LOGDATE = drs["LOGDATE"].ToString(),
                                                        LOGMONTH = drs["LOGMONTH"].ToString(),
                                                        LOGROW = Convert.ToDouble(drs["LOGROW"]),
                                                        LOGCOUNT = Convert.ToDouble(drs["LOGCOUNT"])
                                                    }).ToList();

                            foreach (var list in Loglist)
                            {
                                dr[columnsDate] = list.LOGCOUNT;
                            }
                        }
                        dt_Data.Rows.Add(dr);
                    }
                    RadGridView_ShowHD.DataSource = dt_Data;
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูลที่ต้องการ ลองใหม่อีกครั้ง.");
                    //return;
                }
            }
            this.Cursor = Cursors.Default;
        }
        private string[] GetColumnsDate()
        {
            int count = radDateTimePicker_End.Value.Subtract(radDateTimePicker_Beg.Value).Days + 1;
            DateTime datetimes, dateTime = radDateTimePicker_Beg.Value;
            int lastdate = DateTime.DaysInMonth(radDateTimePicker_Beg.Value.Year, radDateTimePicker_Beg.Value.Month);
            string[] columnName = new string[count];
            for (int i = 0; i < count; i++)
            {
                if (int.Parse(dateTime.AddDays(i).Date.ToString("dd")) <= lastdate)
                {
                    datetimes = dateTime.AddDays(i);
                }
                else
                {
                    dateTime.AddMonths(1);
                    datetimes = dateTime.AddDays(i);
                }
                // columnName[i] = datetimes.ToString("dd-MM-yyyy");
                columnName[i] = "DATE" + datetimes.ToString("yyyyMMdd");
            }
            return columnName;
        }
        private void AppendNewColumn(DataTable table, string[] colNames)
        {
            colNames.ForEach(colName => table.Columns.Add(new DataColumn(colName, typeof(string))));
            colNames.ForEach(colNameGrid => RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight(colNameGrid,
                string.Format(@"{0}-{1}-{2}", colNameGrid.Substring(4, 4), colNameGrid.Substring(8, 2), colNameGrid.Substring(10, 2)), 120)));
        }
        private void SetConditions(string Expression, string ColName, bool applyToRow, Color color, string array)
        {
            this.RadGridView_ShowHD.Columns[array].ConditionalFormattingObjectList.Clear();
            ExpressionFormattingObject obj1 = new ExpressionFormattingObject(ColName, Expression, applyToRow)
            {
                CellBackColor = color
            };
            this.RadGridView_ShowHD.Columns[array].ConditionalFormattingObjectList.Add(obj1);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            this.BindGrid();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            this.ClearTxt();
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Beg, radDateTimePicker_End);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Beg, radDateTimePicker_End);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายงานการใช้เมนู" + RadDropDownList_TypeUse.Text, RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        private void RadCheckBox_CheckScal_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            if (radCheckBox_CheckScal.Checked == true)
            {
                radDateTimePicker_Beg.Value = DateTime.Now;
                radDateTimePicker_End.Value = DateTime.Now;
                RadDropDownList_TypeUse.SelectedValue = "SendScaleShop";
                RadDropDownList_TypeUse.Enabled = false;
            }
            else RadDropDownList_TypeUse.Enabled = true;
        }
        #region Method
        private DataTable Getedata()
        {
            string sql = $@"
                    SELECT	LogBranch AS LOGBRANCH
                            ,Convert(date,Logdate) As LOGDATETIME
                            ,DAY(LOGDATE) AS LOGDATE 
                            ,MONTH(LOGDATE) AS LOGMONTH
                            ,ISNULL(SUM(LogRow),0) AS LOGROW ,Count(LogBranch) AS LOGCOUNT 
                    FROM	SHOP_LOGSHOP24HRS WITH (NOLOCK)    
                    WHERE 	Convert(varchar,LOGDATE,23)  between '{radDateTimePicker_Beg.Value:yyyy-MM-dd}' and '{radDateTimePicker_End.Value:yyyy-MM-dd}' 
		                    AND  LogType = '{RadDropDownList_TypeUse.SelectedValue}'
                    GROUP BY Logbranch
		                    ,DAY(LOGDATE)
		                    ,MONTH(LOGDATE)	
                            ,Convert(date,Logdate) 
                    ORDER BY Logbranch ";

            return ConnectionClass.SelectSQL_Main(sql);
        }
        private DataTable GetUseType()
        {
            string sql = $@"
                    SELECT  Shop_Desc, Shop_StaPlace 
                    FROM    Shop_TypeStatus WITH (NOLOCK) 
                    WHERE   Shop_Type = 'Log' AND Shop_StaType = 'O' 
                    Order By Shop_Desc ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
        private string DataScalrow(string ipserver)
        {
            string sql = $@"SELECT COUNT(*) AS ROW_COUNT FROM [ScaleSHOP].[dbo].[T_MasterProduct] WITH (NOLOCK)  ";
            DataTable dt = ConnectionClass.SelectSQL_SentServer(sql, ipserver);
            if (dt.Rows.Count > 0) return dt.Rows[0]["ROW_COUNT"].ToString(); else return "0";
        }
        private List<BranchServerIp> GetBranchServerIP()
        {
            string sql = $@"SELECT BRANCH_ID
                                                ,BRANCH_CHANNEL
                                                ,BRANCH_SERVER_IP
                                                ,BRANCH_USERNAME
                                                ,BRANCH_PASSWORDNAME
                                        FROM    SHOP_BRANCH_CONFIGDB WITH (NOLOCK) 
                                        WHERE   BRANCH_CHANNEL like 'MN%' ";

            DataTable dt = ConnectionClass.SelectSQL_Main(sql);

            List<BranchServerIp> serverIp = new List<BranchServerIp>();
            foreach (DataRow row in dt.Rows)
            {
                BranchServerIp branchServer = new BranchServerIp()
                {
                    BRANCH_ID = row["BRANCH_ID"].ToString(),
                    BRANCH_SERVER_IP = row["BRANCH_SERVER_IP"].ToString(),
                    BRANCH_USERNAME = row["BRANCH_USERNAME"].ToString(),
                    BRANCH_PASSWORDNAME = row["BRANCH_PASSWORDNAME"].ToString()
                };
                serverIp.Add(branchServer);
            }
            return serverIp;
        }
        #endregion
    }
}