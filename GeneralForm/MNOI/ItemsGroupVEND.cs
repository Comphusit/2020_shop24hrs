﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowData;
using Telerik.WinControls.UI;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.MNOI
{
    public partial class ItemsGroupVEND : Telerik.WinControls.UI.RadForm
    {
        private string TypeEdit;
        Data_VENDTABLE Vend;

        public ItemsGroupVEND() //01 สั่ง 02 แลกแต้ม
        {
            InitializeComponent();
        }

        private void ItemsGroup_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);

            radButton_Save.ButtonElement.ShowBorder = true; radButton_Cancel.ButtonElement.ShowBorder = true;

            radButtonElement_Add.ToolTipText = "เพิ่ม"; radButtonElement_Add.ShowBorder = true;
            radButtonElement_Edit.ToolTipText = "แก้ไข"; radButtonElement_Edit.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            
            RadGridView_Show.AllowAddNewRow = false;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPID", "รหัสกลุ่ม", 130));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPNAME", "ชื่อกลุ่ม", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPVEND", "ผู้จำหน่าย", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPVENDNAME", "ชื่อผู้จำหน่าย", 250));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPREMARK", "หมายเหตุ", 200));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMGROUPSTATUS", "สถานะ"));

            TypeEdit = "Default";
            ClearData(TypeEdit);

            RadDropDownList_Dept.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("64", "", "", "1"); ; //Models.DptClass.GetDpt_AllD()
            RadDropDownList_Dept.DisplayMember = "SHOW_NAME"; //DESCRIPTION
            RadDropDownList_Dept.ValueMember = "SHOW_ID"; //NUM
            RadDropDownList_Dept.SelectedIndex = 0;

            RadGridView_Show.ShowGroupPanel = false;
            if (RadDropDownList_Dept.SelectedValue.ToString() != "") RadGridView_Show.DataSource = ItemGroupOrder_Class.GetOrderGroup(RadDropDownList_Dept.SelectedValue.ToString(),"");
        }
        #region FUNCTION 
        //ClearData
        void ClearData(string Type)
        {
            switch (Type)
            {
                case "Default":

                    radTextBox_ID.Text = string.Empty;
                    radTextBox_Name.Text = string.Empty;
                    RadTextBox_VEND.Text = string.Empty;
                    radTextBox_Remark.Text = string.Empty;
                    radLabel_VendName.Text = string.Empty;

                    radTextBox_ID.Enabled = false;
                    radTextBox_Name.Enabled = false;
                    RadTextBox_VEND.Enabled = false;
                    radTextBox_Remark.Enabled = false;

                    
                    radButton_Save.Enabled = false;
                    radButton_Cancel.Enabled = false;
                    break;

                case "Add":

                    radTextBox_ID.Enabled = false; radTextBox_ID.Text = "";
                    radTextBox_Name.Enabled = true; radTextBox_Name.Text = "";
                    RadTextBox_VEND.Enabled = true; RadTextBox_VEND.Text = "";
                    radLabel_VendName.Text = "";
                    radTextBox_Remark.Enabled = true; radTextBox_Remark.Text = "";

                   
                    radButton_Save.Enabled = true;
                    radButton_Cancel.Enabled = true;
                    radCheckBox_Sta.CheckState = CheckState.Checked;
                    radTextBox_Name.SelectAll();
                    radTextBox_Name.Focus();
                    break;

                case "Edit":

                    radTextBox_ID.Enabled = false;
                    radTextBox_Name.Enabled = true;
                    RadTextBox_VEND.Enabled = true;
                    radTextBox_Remark.Enabled = true;

                    radButton_Save.Enabled = true;
                    radButton_Cancel.Enabled = true;

                    radTextBox_Name.SelectAll();
                    radTextBox_Name.Focus();
                    break;
            }
        }

        void SetDatagrid(DataTable dtGroupItems)
        {
            RadGridView_Show.DataSource = dtGroupItems;
        }

        #endregion
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        #endregion

        #region EVEN

        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            TypeEdit = "Add";
            ClearData(TypeEdit);
        }

        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count < 1)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเพิ่มกลุ่มสินค้า ก่อนเพิ่มสินค้า.");
                return;
            }

            radTextBox_ID.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPID"].Value.ToString();
            radTextBox_Name.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPNAME"].Value.ToString();
            RadTextBox_VEND.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPVEND"].Value.ToString();
            radLabel_VendName.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPVENDNAME"].Value.ToString();
            radTextBox_Remark.Text = RadGridView_Show.CurrentRow.Cells["ITEMGROUPREMARK"].Value.ToString();

            if (RadGridView_Show.CurrentRow.Cells["ITEMGROUPSTATUS"].Value.ToString() == "1") radCheckBox_Sta.CheckState = CheckState.Checked;
            else radCheckBox_Sta.CheckState = CheckState.Unchecked;
            TypeEdit = "Edit";
            ClearData(TypeEdit);
        }

        private void RadTextBox_VEND_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(RadTextBox_VEND.Text))
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ รหัสผู้จำหน่าย ให้เรียบร้อย.");
                    RadTextBox_VEND.SelectAll();
                    RadTextBox_VEND.Focus();
                    return;
                }

                this.Vend = new Data_VENDTABLE(Class.Models.VendTableClass.GetVENDTABLE_All(RadTextBox_VEND.Text, RadDropDownList_Dept.SelectedValue.ToString()));

                if (!(string.IsNullOrEmpty(Vend.VENDTABLE_ACCOUNTNUM)))
                {
                    radLabel_VendName.Text = this.Vend.VENDTABLE_NAME;
                    radTextBox_Remark.Focus();
                }
                else
                {

                    MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่เจอ ผู้จำหน่าย ที่ต้องการค้นหา.");
                    RadTextBox_VEND.SelectAll();
                    RadTextBox_VEND.Focus();
                    return;

                }
            }
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            TypeEdit = "Default";
            ClearData(TypeEdit);
        }

        private void RadTextBox_VEND_KeyUp(object sender, KeyEventArgs e)
        {
            if (RadTextBox_VEND.Text.Length < 7) radLabel_VendName.Text = string.Empty;
        }

        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            RadGridView_Show.DataSource = ItemGroupOrder_Class.GetOrderGroup(RadDropDownList_Dept.SelectedValue.ToString(), "");
        }

        private void RadTextBox_Name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                RadTextBox_VEND.SelectAll();
                RadTextBox_VEND.Focus();
            }
        }

        private void RadTextBox_Remark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radButton_Save.Focus();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radTextBox_Name.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อกลุ่มสินค้า ให้เรียบร้อย.");
                radTextBox_Name.SelectAll();
                radTextBox_Name.Focus();
                return;
            }
            if (!(string.IsNullOrEmpty(RadTextBox_VEND.Text)) & String.IsNullOrEmpty(radLabel_VendName.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ผู้จำหน่าย ให้เรียบร้อย.");
                RadTextBox_VEND.SelectAll();
                RadTextBox_VEND.Focus();
                return;
            }

            if (TypeEdit == "Add") radTextBox_ID.Text = Class.ConfigClass.GetMaxINVOICEID($@"ORDER{RadDropDownList_Dept.SelectedValue}", "", $@"ORDER_{RadDropDownList_Dept.SelectedValue}", "3");

            string STA = "0";
            if (radCheckBox_Sta.CheckState == CheckState.Checked) STA = "1";

            SaveItemGroup ITEMSAVE = new SaveItemGroup 
            {
                ITEMGROUPID = radTextBox_ID.Text,
                ITEMGROUPNAME = radTextBox_Name.Text,
                ITEMGROUPDEPT = RadDropDownList_Dept.SelectedValue.ToString(),
                ITEMGROUPVEND = RadTextBox_VEND.Text,
                ITEMGROUPVENDNAME = radLabel_VendName.Text,
                ITEMGROUPREMARK = radTextBox_Remark.Text,
                ITEMGROUPSTATUS = STA,
            };


            string resualt = ConnectionClass.ExecuteSQL_Main(ItemGroupOrder_Class.Save_OrderGroup(TypeEdit,ITEMSAVE));
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "")
            {
                TypeEdit = "Default";
                ClearData(TypeEdit);
                SetDatagrid(ItemGroupOrder_Class.GetOrderGroup(RadDropDownList_Dept.SelectedValue.ToString(), ""));
            }

            this.Cursor = Cursors.Default;

            
        }

        #endregion

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
