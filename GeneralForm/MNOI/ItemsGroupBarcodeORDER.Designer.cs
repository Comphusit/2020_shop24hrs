﻿namespace PC_Shop24Hrs.GeneralForm.MNOI
{
    partial class ItemsGroupBarcodeORDER
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemsGroupBarcodeORDER));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Delete = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_Remark = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_ItemName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radDropDownList_GroupItems = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadDropDownList_Dept = new Telerik.WinControls.UI.RadDropDownList();
            this.radCheckBox_forOrder = new System.Windows.Forms.CheckBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Unit = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Rmk = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_ItemBarcode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_ID = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_importExcel = new Telerik.WinControls.UI.RadButtonElement();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Delete)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ItemName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Rmk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ItemBarcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(7, 373);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(88, 32);
            this.radButton_Save.TabIndex = 4;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Delete
            // 
            this.radButton_Delete.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Delete.Location = new System.Drawing.Point(97, 373);
            this.radButton_Delete.Name = "radButton_Delete";
            this.radButton_Delete.Size = new System.Drawing.Size(84, 32);
            this.radButton_Delete.TabIndex = 5;
            this.radButton_Delete.Text = "ลบ";
            this.radButton_Delete.ThemeName = "Fluent";
            this.radButton_Delete.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Delete.GetChildAt(0))).Text = "ลบ";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Delete.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Delete.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Delete.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Delete.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Delete.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Delete.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radLabel_Remark);
            this.panel1.Controls.Add(this.radLabel_ItemName);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radDropDownList_GroupItems);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadDropDownList_Dept);
            this.panel1.Controls.Add(this.radCheckBox_forOrder);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel_Unit);
            this.panel1.Controls.Add(this.radLabel_Name);
            this.panel1.Controls.Add(this.radTextBox_Rmk);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radTextBox_ItemBarcode);
            this.panel1.Controls.Add(this.radLabel_ID);
            this.panel1.Controls.Add(this.radButton_Save);
            this.panel1.Controls.Add(this.radButton_Delete);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(3, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 574);
            this.panel1.TabIndex = 5;
            // 
            // radLabel_Remark
            // 
            this.radLabel_Remark.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Remark.ForeColor = System.Drawing.Color.Red;
            this.radLabel_Remark.Location = new System.Drawing.Point(62, 8);
            this.radLabel_Remark.Name = "radLabel_Remark";
            this.radLabel_Remark.Size = new System.Drawing.Size(120, 19);
            this.radLabel_Remark.TabIndex = 70;
            this.radLabel_Remark.Text = "เฉพาะจซ.ที่มีสินค้า";
            this.radLabel_Remark.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.radLabel_Remark.Visible = false;
            // 
            // radLabel_ItemName
            // 
            this.radLabel_ItemName.AutoSize = false;
            this.radLabel_ItemName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_ItemName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_ItemName.Location = new System.Drawing.Point(7, 180);
            this.radLabel_ItemName.Name = "radLabel_ItemName";
            this.radLabel_ItemName.Size = new System.Drawing.Size(175, 42);
            this.radLabel_ItemName.TabIndex = 62;
            this.radLabel_ItemName.Text = "ชื่อสินค้า";
            this.radLabel_ItemName.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel5.Location = new System.Drawing.Point(7, 57);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(34, 19);
            this.radLabel5.TabIndex = 71;
            this.radLabel5.Text = "กลุ่ม";
            // 
            // radDropDownList_GroupItems
            // 
            this.radDropDownList_GroupItems.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_GroupItems.DropDownHeight = 124;
            this.radDropDownList_GroupItems.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.radDropDownList_GroupItems.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDropDownList_GroupItems.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDropDownList_GroupItems.Location = new System.Drawing.Point(7, 81);
            this.radDropDownList_GroupItems.Name = "radDropDownList_GroupItems";
            this.radDropDownList_GroupItems.Size = new System.Drawing.Size(175, 21);
            this.radDropDownList_GroupItems.TabIndex = 70;
            this.radDropDownList_GroupItems.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_GroupItems_SelectedValueChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(7, 8);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(43, 19);
            this.radLabel1.TabIndex = 69;
            this.radLabel1.Text = "จัดซื้อ";
            // 
            // RadDropDownList_Dept
            // 
            this.RadDropDownList_Dept.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RadDropDownList_Dept.DropDownHeight = 124;
            this.RadDropDownList_Dept.DropDownSizingMode = Telerik.WinControls.UI.SizingMode.RightBottom;
            this.RadDropDownList_Dept.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Dept.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadDropDownList_Dept.Location = new System.Drawing.Point(7, 30);
            this.RadDropDownList_Dept.Name = "RadDropDownList_Dept";
            this.RadDropDownList_Dept.Size = new System.Drawing.Size(175, 21);
            this.RadDropDownList_Dept.TabIndex = 68;
            this.RadDropDownList_Dept.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Dept_SelectedValueChanged);
            // 
            // radCheckBox_forOrder
            // 
            this.radCheckBox_forOrder.AutoSize = true;
            this.radCheckBox_forOrder.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_forOrder.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_forOrder.Location = new System.Drawing.Point(9, 253);
            this.radCheckBox_forOrder.Name = "radCheckBox_forOrder";
            this.radCheckBox_forOrder.Size = new System.Drawing.Size(66, 20);
            this.radCheckBox_forOrder.TabIndex = 66;
            this.radCheckBox_forOrder.Text = "ใช้งาน";
            this.radCheckBox_forOrder.UseVisualStyleBackColor = true;
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(7, 228);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(41, 19);
            this.radLabel2.TabIndex = 63;
            this.radLabel2.Text = "หน่วย";
            // 
            // radLabel_Unit
            // 
            this.radLabel_Unit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Unit.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Unit.Location = new System.Drawing.Point(58, 228);
            this.radLabel_Unit.Name = "radLabel_Unit";
            this.radLabel_Unit.Size = new System.Drawing.Size(41, 19);
            this.radLabel_Unit.TabIndex = 62;
            this.radLabel_Unit.Text = "หน่วย";
            // 
            // radLabel_Name
            // 
            this.radLabel_Name.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Name.Location = new System.Drawing.Point(7, 155);
            this.radLabel_Name.Name = "radLabel_Name";
            this.radLabel_Name.Size = new System.Drawing.Size(59, 19);
            this.radLabel_Name.TabIndex = 61;
            this.radLabel_Name.Text = "ชื่อสินค้า";
            // 
            // radTextBox_Rmk
            // 
            this.radTextBox_Rmk.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Rmk.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Rmk.Location = new System.Drawing.Point(6, 301);
            this.radTextBox_Rmk.Multiline = true;
            this.radTextBox_Rmk.Name = "radTextBox_Rmk";
            // 
            // 
            // 
            this.radTextBox_Rmk.RootElement.StretchVertically = true;
            this.radTextBox_Rmk.Size = new System.Drawing.Size(176, 66);
            this.radTextBox_Rmk.TabIndex = 3;
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(7, 279);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(65, 19);
            this.radLabel3.TabIndex = 21;
            this.radLabel3.Text = "หมายเหตุ";
            // 
            // radTextBox_ItemBarcode
            // 
            this.radTextBox_ItemBarcode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_ItemBarcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_ItemBarcode.Location = new System.Drawing.Point(7, 128);
            this.radTextBox_ItemBarcode.MaxLength = 20;
            this.radTextBox_ItemBarcode.Name = "radTextBox_ItemBarcode";
            this.radTextBox_ItemBarcode.Size = new System.Drawing.Size(175, 21);
            this.radTextBox_ItemBarcode.TabIndex = 0;
            this.radTextBox_ItemBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_ItemBarcode_KeyDown);
            // 
            // radLabel_ID
            // 
            this.radLabel_ID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_ID.Location = new System.Drawing.Point(7, 107);
            this.radLabel_ID.Name = "radLabel_ID";
            this.radLabel_ID.Size = new System.Drawing.Size(103, 19);
            this.radLabel_ID.TabIndex = 18;
            this.radLabel_ID.Text = "บาร์โค๊ด[Enter]";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.AutoSize = false;
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Add,
            this.commandBarSeparator1,
            this.radButtonElement_Edit,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator3,
            this.radButtonElement_importExcel});
            this.radStatusStrip1.Location = new System.Drawing.Point(3, 3);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(188, 42);
            this.radStatusStrip1.TabIndex = 7;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.ToolTipText = "เพิ่ม";
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.AutoSize = true;
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement2";
            this.radButtonElement_Edit.ToolTipText = "แก้ไข";
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_importExcel
            // 
            this.radButtonElement_importExcel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_importExcel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_importExcel.Name = "radButtonElement_importExcel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_importExcel, false);
            this.radButtonElement_importExcel.Text = "radButtonElement_importExcel";
            this.radButtonElement_importExcel.Click += new System.EventHandler(this.RadButtonElement_importExcel_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 19;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.radStatusStrip1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(632, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(194, 628);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_F2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(3, 606);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(617, 19);
            this.radLabel_F2.TabIndex = 69;
            this.radLabel_F2.Text = "<html>กด + &gt;&gt; เพื่อเพิ่มบาร์โค้ดสินค้า | เลือกสินค้า กด / &gt;&gt; เพื่อแก้" +
    "ไขสินค้า</html>";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(617, 597);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(611, 591);
            this.RadGridView_Show.TabIndex = 0;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_Show.SelectionChanged += new System.EventHandler(this.RadGridView_Show_SelectionChanged);
            this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.RadGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // ItemsGroupBarcodeORDER
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ItemsGroupBarcodeORDER";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "การตั้งค่าสินค้า";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ItemsGroupBarcodeORDER_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Delete)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Remark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ItemName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_GroupItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Dept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Unit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Rmk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_ItemBarcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Delete;
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadLabel radLabel_ID;
        private Telerik.WinControls.UI.RadTextBox radTextBox_ItemBarcode;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Rmk;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel_Name;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Unit;
        private System.Windows.Forms.CheckBox radCheckBox_forOrder;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        public Telerik.WinControls.UI.RadDropDownList radDropDownList_GroupItems;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        public Telerik.WinControls.UI.RadDropDownList RadDropDownList_Dept;
        private Telerik.WinControls.UI.RadLabel radLabel_ItemName;
        private Telerik.WinControls.UI.RadLabel radLabel_Remark;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_importExcel;
    }
}
