﻿using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNOI
{
    public partial class MNOI_FindData : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDataHD = new DataTable();
        DataTable dtDataDT = new DataTable();
        readonly string _pTypeOpen;
        public string pDocno;

        public MNOI_FindData(string pTypeOpen)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
        }
        //Load
        private void MNOR_FindData_Load(object sender, EventArgs e)
        {
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radDateTimePicker_Begin.MaxDate = radDateTimePicker_End.Value;
            radDateTimePicker_End.MinDate = radDateTimePicker_Begin.Value;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now.AddDays(1));

            radCheckBox_Apv.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadCheckBox_GrpMain.ButtonElement.Font = SystemClass.SetFontGernaral;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GrpMain);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่บิล", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCDATE", "วันที่บิล", 120)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DATERECIVE", "วันที่ต้องการสินค้า")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPMAIN", "จัดซื้อ", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPSUB", "กลุ่มย่อย", 200)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "ผู้บันทึก", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STADOC", "ยกเลิก")));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHOCANCLE", "ผู้ยกเลิก", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("DPTID", "แผนก")));

            DatagridClass.SetCellBackClolorByExpression("DOCNO", $@" STADOC = '1'", ConfigClass.SetColor_PinkPastel(), RadGridView_ShowHD);

            RadGridView_ShowHD.Columns["DOCNO"].IsPinned = true;
            RadGridView_ShowHD.Columns["DOCDATE"].IsPinned = true;

            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

            RadButton_Search.ButtonElement.ShowBorder = true;
            RadButton_Choose.ButtonElement.ShowBorder = true;

            RadGridView_ShowHD.DataSource = dtDataHD;
            RadGridView_ShowDT.DataSource = dtDataDT;

            radDateTimePicker_End.Value = DateTime.Now.AddDays(1);
            radDateTimePicker_Begin.Value = DateTime.Now;

            if (_pTypeOpen == "1") RadButton_Choose.Visible = true; else RadButton_Choose.Visible = false;

            Set_GroupMain();
            Set_GropMainSub();
            SetDGV_HD();
        }
        //Set HD
        void SetDGV_HD()
        {
            if (dtDataHD.Rows.Count > 0) { dtDataHD.Rows.Clear(); dtDataHD.AcceptChanges(); }

            string dptID = "", groupID = "";
            if (RadCheckBox_GrpMain.Checked == true) groupID = radDropDownList_MainGroup.SelectedValue.ToString();
            else
            if (SystemClass.SystemBranchID == "MN000") dptID = RadDropDownList_GrpMain.SelectedValue.ToString();


            dtDataHD = MNOI_Class.FindMNOIHD(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"),
                dptID, groupID);
            RadGridView_ShowHD.DataSource = dtDataHD;
          
            if (dtDataHD.Rows.Count == 0) if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }

        }
        //Set DT
        void SetDGV_DT(string _pDocno)
        {
            dtDataDT = MNOI_Class.FindMNOIDT(_pDocno);
            RadGridView_ShowDT.DataSource = dtDataDT;
            dtDataDT.AcceptChanges();
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        #endregion

        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Select HD Change DT
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.CurrentRow.Index == -1) return;

            if (RadGridView_ShowHD.Rows.Count == 0)
            {
                if (dtDataDT.Rows.Count > 0) { dtDataDT.Rows.Clear(); dtDataDT.AcceptChanges(); }
                return;

            }

            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
        }
        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            pDocno = RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString();
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //GroupMain
        void Set_GroupMain()
        {
            RadDropDownList_GrpMain.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("64", "", "", "1");
            RadDropDownList_GrpMain.ValueMember = "SHOW_ID";//NUM
            RadDropDownList_GrpMain.DisplayMember = "SHOW_NAME";
            RadDropDownList_GrpMain.SelectedIndex = 0;
        }
        //sub grp
        void Set_GropMainSub()
        {
            radDropDownList_MainGroup.DataSource = ItemGroupOrder_Class.GetOrderGroup(RadDropDownList_GrpMain.SelectedValue.ToString(), "AND ITEMGROUPSTATUS = '1'");
            radDropDownList_MainGroup.ValueMember = "ITEMGROUPID";
            radDropDownList_MainGroup.DisplayMember = "ITEMGROUPNAME";
            radDropDownList_MainGroup.SelectedIndex = 0;
        }
        //Group Main
        private void RadCheckBox_GrpMain_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_GrpMain.Checked == true) radDropDownList_MainGroup.Enabled = true;
            else radDropDownList_MainGroup.Enabled = false;
        }

        private void RadDropDownList_GrpMain_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {
            Set_GropMainSub();
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

    }
}
