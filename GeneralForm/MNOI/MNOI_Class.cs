﻿using PC_Shop24Hrs.Controllers;
using System.Data;

namespace PC_Shop24Hrs.GeneralForm.MNOI
{
    class MNOI_ITEM
    {
        public string BRANCH { get; set; } = "";
        public string BARCODE { get; set; } = "";
        public string ITEMNAME { get; set; } = "";
        public string UNITID { get; set; } = "";
        public double QTY { get; set; } = 0.0;
        public string DOCNO { get; set; } = "";
    }
    class MNOI_Class
    {
        public static DataTable FindMNOIHD(string date1, string date2, string dptID, string groupID)
        {
            string whereGroup = "";
            if (groupID != "") whereGroup = $@" AND SHOP_MNOI_HD.GROUPID = '{groupID}' ";

            string whereDptID = "";
            if (dptID != "") whereDptID = $@" AND SHOP_MNOI_HD.DPTID = '{dptID}' ";

            string str = $@"
            SELECT	DOCNO,CONVERT(VARCHAR,SHOP_MNOI_HD.DATEINS,23) AS DOCDATE, CONVERT(VARCHAR,DATE_RECIVE,23) AS DATERECIVE,
		            SHOP_MNOI_HD.DPTID + '-' + SHOP_MNOI_HD.DPTNAME AS GROUPMAIN, SHOP_MNOI_HD.GROUPID + '-' + ITEMGROUPNAME AS GROUPSUB,
		            DEPT_ID + '-' + DEPT_NAME AS DEPT,
                    SHOP_MNOI_HD.DPTID,
		            SHOP_MNOI_HD.WHOIDINS + '-' + ISNULL(SHOP_MNOI_HD.WHONAMEINS,'') AS WHOIDINS,
		            CASE STA_DOC WHEN '3' THEN '1' ELSE '0' END AS STADOC,
                    CASE STA_DOC WHEN '3' THEN SHOP_MNOI_HD.WHOIDUPD +'-'+ISNULL(SHOP_MNOI_HD.WHONAMEUPD,'')  ELSE '' END AS WHOCANCLE,
		            CASE STA_DOC WHEN '1' THEN SHOP_MNOI_HD.WHOIDUPD+'-'+ISNULL(SHOP_MNOI_HD.WHONAMEUPD ,'')ELSE '' END AS WHOIDUPD,
                    SHOP_MNOI_HD.WHOIDAPV+'-'+ISNULL(SHOP_MNOI_HD.WHONAMEAPV,'') AS WHOIDAPV,
					CONVERT(VARCHAR,SHOP_MNOI_HD.DATE_APV,25) AS DATEAPV
            FROM	SHOP_MNOI_HD WITH (NOLOCK)
                    LEFT OUTER JOIN SHOP_ITEMORDERGROUP WITH (NOLOCK) ON SHOP_MNOI_HD.GROUPID = SHOP_ITEMORDERGROUP.ITEMGROUPID
            WHERE	DATE_RECIVE BETWEEN '{date1}' AND '{date2}' 
                    {whereDptID}
                    {whereGroup} ";

            return ConnectionClass.SelectSQL_Main(str);
        }

        public static DataTable FindMNOIDT(string docno)
        {
            return ConnectionClass.SelectSQL_Main($@"
                    SELECT	ITEMBARCODE,SPC_ITEMNAME,QtyOrder AS QTY, UNITID
                    FROM	SHOP_MNOI_DT WITH (NOLOCK) 
                    WHERE	DOCNO = '{docno}' AND STADT = '1' ");
        }
    } 
}
