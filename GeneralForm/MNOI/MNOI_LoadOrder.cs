﻿using System;
using System.Data;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.FormShare;

namespace PC_Shop24Hrs.GeneralForm.MNOI
{
    public partial class MNOI_LoadOrder : Telerik.WinControls.UI.RadForm
    {
        private readonly DataTable dtBranch = new DataTable();
        readonly DataTable data = new DataTable();

        public MNOI_LoadOrder()
        {
            InitializeComponent();
        }
        //Load Main
        private void MNOR_LoadOrder_Load(object sender, EventArgs e)
        {
            radStatusStrip_Menu.SizingGrip = false;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Recieve, DateTime.Now.AddDays(1), DateTime.Now.AddDays(1));
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Recieve2, DateTime.Now.AddDays(1), DateTime.Now.AddDays(7));
            radButtonElement_Clear.ShowBorder = true; radButtonElement_Clear.ToolTipText = "Clear";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";
            RadButtonElement_pdf.ShowBorder = true; RadButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน";
            RadButton_Search.ButtonElement.ShowBorder = true;

            RadCheckBox_GrpSub.ButtonElement.Font = SystemClass.SetFontGernaral; RadCheckBox_GrpSub.Checked = false;
            radCheckBox_ShowData.ButtonElement.Font = SystemClass.SetFontGernaral; radCheckBox_ShowData.Checked = false;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_SubGroup);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_MainGroup);

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            RadGridView_Show.MasterTemplate.ReadOnly = false;


            SetDropdownlist();
            ClearData();
        }
        void ClearData()
        {
            radDateTimePicker_Recieve2.Value = DateTime.Now.AddDays(1);
            radDateTimePicker_Recieve.Value = DateTime.Now.AddDays(1);

           

            if (data.Rows.Count > 0) data.Rows.Clear();

            if (RadGridView_Show.Rows.Count > 0) RadGridView_Show.Rows.Clear();
            if (RadGridView_Show.Columns.Count > 0) { this.RadGridView_Show.Columns.Clear(); this.RadGridView_Show.SummaryRowsTop.Clear(); }

            RadCheckBox_GrpSub.ReadOnly = false;
            radDateTimePicker_Recieve.Enabled = true; radDateTimePicker_Recieve2.Enabled = true;
            radDropDownList_MainGroup.Enabled = true;
            RadCheckBox_GrpSub.Enabled = true;


        }
        void SetDropdownlist()
        {
            radDropDownList_MainGroup.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("64", "", "", "1");
            radDropDownList_MainGroup.DisplayMember = "SHOW_NAME";
            radDropDownList_MainGroup.ValueMember = "SHOW_ID";
            radDropDownList_SubGroup.Enabled = false;

            radDropDownList_MainGroup.SelectedValue = SystemClass.SystemDptID;
            if (radDropDownList_MainGroup.SelectedValue is null) radDropDownList_MainGroup.SelectedIndex = 0;
        }
        //แสดงกลุ่มย่อย
        void SetGroupSub(string _pGrpmain)
        {
            radDropDownList_SubGroup.DataSource = ItemGroupOrder_Class.GetOrderGroup(_pGrpmain, "AND ITEMGROUPSTATUS = '1'");
            radDropDownList_SubGroup.ValueMember = "ITEMGROUPID";
            radDropDownList_SubGroup.DisplayMember = "ITEMGROUPNAME";
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //checkChange
        private void RadCheckBox_GrpSub_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_GrpSub.Checked == true)
            {
                SetGroupSub(radDropDownList_MainGroup.SelectedValue.ToString());
                radDropDownList_SubGroup.Enabled = true;
            }
            else radDropDownList_SubGroup.Enabled = false;
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV();
        }
        private void RadGridView_Show_CustomFiltering(object sender, GridViewCustomFilteringEventArgs e)
        {
            try
            {
                e.Visible = Convert.ToDouble(e.Row.Cells["SUM"].Value.ToString()) > 0;
            }
            catch (Exception)
            {
                return;
            }
        }
        //LoadData
        void SetDGV()
        {
            this.Cursor = Cursors.WaitCursor;
            if (RadGridView_Show.Columns.Count > 0)
            {
                this.RadGridView_Show.Columns.Clear();
                this.RadGridView_Show.SummaryRowsTop.Clear();
            }

            string groupsub = "";
            if (RadCheckBox_GrpSub.CheckState == CheckState.Checked) groupsub = radDropDownList_SubGroup.SelectedValue.ToString();

            if (radCheckBox_ShowData.CheckState == CheckState.Unchecked)
            {
                //DataTable dtItemOrder = MNOI_Class.GetItemOrder(radDateTimePicker_Recieve.Value.ToString("yyyy-MM-dd"), radDropDownList_MainGroup.SelectedValue.ToString(), groupsub);
                DataTable dtItemOrder = ItembarcodeClass.FindItemOrderMNOI(radDateTimePicker_Recieve.Value.ToString("yyyy-MM-dd"), radDropDownList_MainGroup.SelectedValue.ToString(), groupsub);
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 500));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPNAME", "กลุ่มย่อย", 120));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTYORDER", "จำนวน", 80));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("REMARK", "หมายเหตุ"));

                DatagridClass.SetCellBackClolorByExpression("ITEMBARCODE", $@" REMARK = '1'", ConfigClass.SetColor_YellowPastel(), RadGridView_Show);
                DatagridClass.SetCellBackClolorByExpression("QTYORDER", $@" QTYORDER <> '0'", ConfigClass.SetColor_GreenPastel(), RadGridView_Show);
                radLabel_Detail.Text = "<html>บาร์โค้ด &gt;&gt; สีเหลือง : สินค้าสั่งด่วน</html>";
                RadGridView_Show.DataSource = dtItemOrder;
            }
            else
            {
                DataTable dtItemBarcode = ItembarcodeClass.FindItemBarcodeMNOI(radDropDownList_MainGroup.SelectedValue.ToString(), groupsub);
                //DataTable dtItemBarcode = MNOI_Class.FindItemBarcode(radDropDownList_MainGroup.SelectedValue.ToString(), groupsub);
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 500));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("COST", "ต้นทุน", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("PRICE", "ราคาขาย", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("QTY", "อัตราส่วน"));

                RadGridView_Show.MasterTemplate.Columns["ITEMBARCODE"].IsPinned = true;
                RadGridView_Show.MasterTemplate.Columns["SPC_ITEMNAME"].IsPinned = true;
                RadGridView_Show.MasterTemplate.Columns["COST"].IsPinned = true;
                RadGridView_Show.MasterTemplate.Columns["PRICE"].IsPinned = true;
                RadGridView_Show.MasterTemplate.Columns["UNITID"].IsPinned = true;

                //RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("REMARK", "หมายเหตุ"));
                RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMGROUPNAME", "กลุ่มย่อย"));

                DatagridClass.SetCellBackClolorByExpression("COST", $@" COST = '0'", ConfigClass.SetColor_Red(), RadGridView_Show);
                DatagridClass.SetCellBackClolorByExpression("PRICE", $@" PRICE = '0'", ConfigClass.SetColor_Red(), RadGridView_Show);

                radLabel_Detail.Text = "<html>ต้นทุน &gt;&gt; สีแดง : สินค้าไม่ได้กำหนดต้นทุน || ราคาขาย &gt;&gt; สีแดง : สินค้าไม่ได้กำหนดราคาขาย || ค้างส่ง &gt;&gt; สีชมพู : มีรายการค้างส่ง</html>";
                RadGridView_Show.DataSource = dtItemBarcode;

                double intDay = Math.Ceiling((radDateTimePicker_Recieve2.Value.Date - radDateTimePicker_Recieve.Value.Date).TotalDays) + 1;

                for (int iDay = 0; iDay < intDay; iDay++) //เพิ่มคอลัม
                {
                    string headColumeORDER = "ORDER" + Convert.ToString(iDay);
                    string headColumeRECIVE = "RECIVE" + Convert.ToString(iDay);
                    string headColumePRICE = "PRICE" + Convert.ToString(iDay);
                    string headColumeOVERDUE = "OVERDUE" + Convert.ToString(iDay);
                    string pDateORDER = $@"[{radDateTimePicker_Recieve.Value.AddDays(iDay):yyyy-MM-dd}]{Environment.NewLine}ออร์เดอร์";
                    string pDateRECIVE = $@"[{radDateTimePicker_Recieve.Value.AddDays(iDay):yyyy-MM-dd}]{Environment.NewLine}รับเข้า";
                    string pDatePRICE = $@"[{radDateTimePicker_Recieve.Value.AddDays(iDay):yyyy-MM-dd}]{Environment.NewLine}มูลค่า";
                    string pDateOVERDUE = $@"[{radDateTimePicker_Recieve.Value.AddDays(iDay):yyyy-MM-dd}]{Environment.NewLine}ค้างส่ง";

                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(headColumeORDER, pDateORDER, 85)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(headColumeRECIVE, pDateRECIVE, 85)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(headColumePRICE, pDatePRICE, 85)));
                    RadGridView_Show.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(headColumeOVERDUE, pDateOVERDUE, 85)));

                    DatagridClass.SetCellBackClolorByExpression(headColumeORDER, $@" {headColumeORDER} <> '0'", ConfigClass.SetColor_GreenPastel(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression(headColumeRECIVE, $@" {headColumeRECIVE} <> '0'", ConfigClass.SetColor_GreenPastel(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression(headColumePRICE, $@" {headColumePRICE} <> '0'", ConfigClass.SetColor_GreenPastel(), RadGridView_Show);
                    DatagridClass.SetCellBackClolorByExpression(headColumeOVERDUE, $@" {headColumeOVERDUE} <> '0'", ConfigClass.SetColor_PinkPastel(), RadGridView_Show);
                }

                for (int iR = 0; iR < RadGridView_Show.RowCount; iR++) //เพิ่มข้อมูลตามบาร์โต้ด
                {
                    for (int iC = 7; iC < RadGridView_Show.ColumnCount; iC += 4)
                    {
                        double CDate = double.Parse(RadGridView_Show.Columns[iC].Name.Replace("ORDER", ""));
                        string pDate = radDateTimePicker_Recieve.Value.AddDays(CDate).ToString("yyyy-MM-dd");
                        //DataTable dtRow = MNOI_Class.FindMNOI(pDate, radDropDownList_MainGroup.SelectedValue.ToString(), groupsub);
                        DataTable dtRow = GeneralForm.ProductRecive.ProductRecive_Class.GetDatailFactory("6", pDate, groupsub, radDropDownList_MainGroup.SelectedValue.ToString());


                        string ITEMBARCODE = dtItemBarcode.Rows[iR]["ITEMBARCODE"].ToString();

                        DataRow[] dr = dtRow.Select($@" ITEMBARCODE = '{ITEMBARCODE}' AND DATE_RECIVE = '{pDate}' ");
                        int order = 0; float recive = 0; float price = 0;
                        if (dr.Length > 0)
                        {
                            order = int.Parse(dr[0]["QTYORDER"].ToString());
                            recive = float.Parse(dr[0]["QTYRECIVEALL"].ToString()) / float.Parse(RadGridView_Show.Rows[iR].Cells["QTY"].Value.ToString());
                            price = float.Parse(RadGridView_Show.Rows[iR].Cells["COST"].Value.ToString()) * recive;
                        }
                        RadGridView_Show.Rows[iR].Cells[iC].Value = order.ToString();
                        RadGridView_Show.Rows[iR].Cells[iC+1].Value = recive.ToString();
                        RadGridView_Show.Rows[iR].Cells[iC + 2].Value = price.ToString();
                    }
                }
            }
            this.Cursor = Cursors.Default;
        }
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (radCheckBox_ShowData.CheckState == CheckState.Unchecked) return;
            if (e.Column.Name.Length < 8) return;
            string ColumnNmae = e.Column.Name.Substring(0,7); //OVERDUE
            switch (ColumnNmae)
            {
                case "OVERDUE":
                    MNOI_ITEM iTEM = new MNOI_ITEM()
                    {
                        BRANCH = e.Column.FieldName,
                        BARCODE = this.RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                        ITEMNAME = this.RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                        UNITID = this.RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString(),
                        QTY = Convert.ToDouble(e.Value),
                    };


                    InputData frm = new InputData("0", iTEM.BARCODE, iTEM.ITEMNAME, iTEM.UNITID)
                    {
                        pInputData = iTEM.QTY.ToString()
                    };
                    if (frm.ShowDialog(this) == DialogResult.Yes)
                    {
                        iTEM.QTY = Convert.ToDouble(frm.pInputData);
                        RadGridView_Show.CurrentRow.Cells[e.Column.Name].Value = iTEM.QTY.ToString("#,#0.00");
                    }
                    break;
                default:
                    break;
            }
        }
        //Excel
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView("รายการจัดสินค้าเบิก " + radDateTimePicker_Recieve.Value.ToString("yyyy-MM-dd"), RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        private void RadButtonElement_Clear_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        private void RadDropDownList_MainGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            if (!(radDropDownList_MainGroup.SelectedValue is null))
            {
                SetGroupSub(radDropDownList_MainGroup.SelectedValue.ToString());

                if (radDropDownList_MainGroup.SelectedValue.ToString() == "D034")
                {
                    RadCheckBox_GrpSub.Enabled = false; RadCheckBox_GrpSub.Checked = true;
                }
                else RadCheckBox_GrpSub.Enabled = true;
            }
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
        private void RadDateTimePicker_Recieve2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Recieve, radDateTimePicker_Recieve2);
        }
        private void RadDateTimePicker_Recieve_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Recieve, radDateTimePicker_Recieve2);
        }
        private void RadCheckBox_ShowData_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_ShowData.CheckState == CheckState.Checked) radDateTimePicker_Recieve2.Visible = true;
            else radDateTimePicker_Recieve2.Visible = false;
        }

    }
}

