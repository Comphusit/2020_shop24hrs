﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.MNOI
{
    public partial class MNOI_ORDER : Telerik.WinControls.UI.RadForm
    {
        DataTable dtDGV = new DataTable();
        DataTable dtGroupSub;
        string pApv;

        string pStaOrderAgain;
        public MNOI_ORDER()
        {
            InitializeComponent();
        }
        //Load Main
        private void MNOR_ORDER_Load(object sender, EventArgs e)
        {

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GrpMain);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_GrpSub);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now.AddDays(1));

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 400));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "สั่ง"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QTY", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY_UNITID", "อัตราส่วน"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("VENDERID", "รหัสผู้ขาย"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("BlockSend", "BlockSend"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("REMARK", "สั่งด่วน"));

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "BlockSend = '1' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            this.RadGridView_Show.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["STA"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["QTY"].ConditionalFormattingObjectList.Add(obj2);
            this.RadGridView_Show.Columns["UNITID"].ConditionalFormattingObjectList.Add(obj2);

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true;
            RadButtonElement_Find.ShowBorder = true;
            RadButton_Save.ButtonElement.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;
            radButtonElement_Excel.ShowBorder = true;
            RadButton_Cancel.ButtonElement.ShowBorder = true;
            radDateTimePicker_Begin.MinDate = DateTime.Now.AddDays(-1);
            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(1);
            Set_GroupMain();
            ClearData();
        }
        //GroupMain
        void Set_GroupMain()
        {
            RadDropDownList_GrpMain.DataSource = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("64", "", "", "1"); ;
            RadDropDownList_GrpMain.ValueMember = "SHOW_ID";//NUM
            RadDropDownList_GrpMain.DisplayMember = "SHOW_NAME";//DESCRIPTION
            RadDropDownList_GrpMain.SelectedIndex = 0;
        }
        //แสดงกลุ่มย่อย
        void Set_GroupSub()
        {
            dtGroupSub = ItemGroupOrder_Class.GetOrderGroup(RadDropDownList_GrpMain.SelectedValue.ToString(), "AND ITEMGROUPSTATUS = '1'");
            RadDropDownList_GrpSub.DataSource = dtGroupSub;
            RadDropDownList_GrpSub.ValueMember = "ITEMGROUPID";
            RadDropDownList_GrpSub.DisplayMember = "ITEMGROUPNAME";
            RadDropDownList_GrpSub.SelectedIndex = 0;
        }
        //Set DGV
        void Set_DGV()
        {
            if (RadDropDownList_GrpSub.Items.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("แผนกที่เลือกนั้น ไม่มีกลุ่มสินค้าที่สามารถสั่งได้" + Environment.NewLine + "ติดต่อจัดซื้อเจ้าของแผนกเพื่อแก้ไขหรือสอบถามข้อมูลเพิ่มเติม");
                return;
            }
            this.Cursor = Cursors.WaitCursor;

            string sub = "";
            if (radRadioButton_P1.CheckState == CheckState.Checked)
            {
                sub = $@"AND GROUPSUB = '{RadDropDownList_GrpSub.SelectedValue}'";
            }
            //dtDGV = ItemGroupOrder_Class.MNOI_GetItemBarcode(RadDropDownList_GrpMain.SelectedValue.ToString(), sub);
            dtDGV = ItembarcodeClass.FindBarcodeForOrderMNOI(RadDropDownList_GrpMain.SelectedValue.ToString(), sub);
            if (dtDGV.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("สินค้าในกลุ่มที่เลือก");
                this.Cursor = Cursors.Default;
                return;
            }

            dtDGV.AcceptChanges();
            RadGridView_Show.DataSource = dtDGV;

            RadDropDownList_GrpMain.Enabled = false;
            RadDropDownList_GrpSub.Enabled = false;
            RadButton_Search.Enabled = false;
            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก"; radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red(); pApv = "0";
            RadButton_Save.Enabled = true; radDateTimePicker_Begin.Enabled = false;
            RadGridView_Show.ReadOnly = false;
            CheckOrder(RadDropDownList_GrpSub.SelectedValue.ToString());
            this.Cursor = Cursors.Default;
        }



        //เช็คว่าสั่งไปหรือยัง ในวันที่ระบุ
        void CheckOrder(string grpSub)
        {
            string sql = $@"

            SELECT	CONVERT(VARCHAR,DATEINS,23) as MNOIDate,CONVERT(VARCHAR,DATEINS,24) AS MNOITime 
            FROM	SHOP_MNOI_HD WITH (NOLOCK)
            WHERE	DATE_RECIVE = '{radDateTimePicker_Begin.Value:yyyy-MM-dd}'
		            AND GROUPID = '{grpSub}'
		            AND STA_DOC = '1'
		            AND DEPT_ID = '{SystemClass.SystemDptID}'";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count > 0) pStaOrderAgain = "1";  
            else pStaOrderAgain = "0";
        }
        //Clear
        void ClearData()
        {

            if (dtDGV.Rows.Count > 0) { dtDGV.Rows.Clear(); dtDGV.AcceptChanges(); }

            RadDropDownList_GrpMain.SelectedIndex = 0;
            Set_GroupSub();

            radLabel_Docno.Text = "";
            radLabel_StatusBill.Text = "";
            pApv = "0"; pStaOrderAgain = "0";
            RadDropDownList_GrpMain.Enabled = true; RadDropDownList_GrpSub.Enabled = true; RadButton_Search.Enabled = true;

            radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = Color.Transparent;

            radDateTimePicker_Begin.Enabled = true;
            RadButton_Cancel.Enabled = false;
            RadButton_Save.Enabled = false;

            RadButton_Search.Focus();
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            Set_DGV();
        }

        //Claer
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Select Group Main
        private void RadDropDownList_GrpMain_SelectedValueChanged(object sender, EventArgs e)
        {
            Set_GroupSub();
        }
        //Set choose Data
        void SetDataREMARK()
        {
            DataRow[] drSTA = dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
            string pSta = "0"; double pQty = 0; string pRemark = "0";

            if (RadGridView_Show.CurrentRow.Cells["REMARK"].Value.ToString() == "0")
            {
                if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
                {
                    InputData frmSTA = new InputData("0",
                            RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                            "จำนวนที่ต้องการใช้", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    { pInputData = "1" };
                    if (frmSTA.ShowDialog(this) == DialogResult.Yes)
                    {
                        pSta = "1";
                        pQty = Convert.ToDouble(frmSTA.pInputData);
                        pRemark = "1";
                    }
                }
                else { pSta = "1"; pQty = double.Parse(RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString()); pRemark = "1"; }
            }
            else { pSta = "1"; pQty = double.Parse(RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString()); pRemark = "0"; }

            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["STA"] = pSta;
            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["QTY"] = pQty.ToString("#,##0.00");
            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["REMARK"] = pRemark;
            dtDGV.AcceptChanges();
        }
        //Set choose Data
        void SetDataSTA()
        {
            DataRow[] drSTA = dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
            string pSta = "0"; double pQty = 0; double pQOrder = 0;

            if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0")
            {
                InputData frmSTA = new InputData("0",
                        RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                        "จำนวนที่ต้องการใช้", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                { pInputData = "1" };
                if (frmSTA.ShowDialog(this) == DialogResult.Yes)
                {
                    pSta = "1";
                    pQty = Convert.ToDouble(frmSTA.pInputData);
                }
            }
            else { pSta = "0"; pQty = 0; pQOrder = 0; }

            if (radLabel_Docno.Text != "")
            {
                string sqlUp = $@" 
                        UPDATE	SHOP_MNOI_DT
                        SET	    STADT = '{pSta}',
		                        QTYORDER = '{pQty}'
                        WHERE	DOCNO = '{radLabel_Docno.Text}'
		                        AND ITEMBARCODE = '{dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["ITEMBARCODE"]}' ";
                string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
                if (T != "") return;
            }

            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["STA"] = pSta;
            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["QTY"] = pQty.ToString("#,##0.00");
            dtDGV.Rows[dtDGV.Rows.IndexOf(drSTA[0])]["REMARK"] = pQOrder.ToString("#,##0.00");
            dtDGV.AcceptChanges();
        }
        //choose
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if ((pApv == "1") || (pApv == "3")) { return; }
            switch (e.Column.Name)
            {
                case "STA":
                    SetDataSTA();
                    break;
                case "QTY":
                    if (RadGridView_Show.CurrentRow.Cells["BlockSend"].Value.ToString() == "1")
                    {
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00"; return;
                    }

                    if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "0") return;

                    DataRow[] drQTY = dtDGV.Select("ITEMBARCODE = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "'");
                    InputData frmQTY = new InputData("0",
                                              RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                              "จำนวนที่ต้องการใช้", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    { pInputData = RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString() };
                    if (frmQTY.ShowDialog(this) == DialogResult.Yes)
                    {
                        string pSta; double pQty = Convert.ToDouble(frmQTY.pInputData);
                        if (Convert.ToDouble(frmQTY.pInputData) == 0)
                        { pSta = "0"; }
                        else { pSta = "1"; }
                        if (radLabel_Docno.Text != "")
                        {
                            string sqlUp = string.Format(@" UPDATE SHOP_MNOR_DT SET STA_SEND = '" + pSta + @"',QTYORDER = '" + pQty + @"' 
                            WHERE DOCNO = '" + radLabel_Docno.Text + @"' AND ITEMBARCODE = '" + dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["ITEMBARCODE"] + @"' ");
                            if (ConnectionClass.ExecuteSQL_Main(sqlUp) != "") { MsgBoxClass.MsgBoxShow_SaveStatus("จำนวนสั่ง"); return; }
                        }
                        dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["STA"] = pSta;
                        dtDGV.Rows[dtDGV.Rows.IndexOf(drQTY[0])]["QTY"] = pQty.ToString("#,##0.00");
                        dtDGV.AcceptChanges();
                    }

                    break;
                case "REMARK":
                    SetDataREMARK();
                    break;
                default:
                    return;
            }
        }
        //choose
        private void RadGridView_Show_CellClick(object sender, GridViewCellEventArgs e)
        {
            if ((pApv == "1") || (pApv == "3")) return;
            switch (e.Column.Name)
            {
                case "STA":
                    SetDataSTA();
                    break;
                case "REMARK":
                    SetDataREMARK();
                    break;
                default:
                    return;
            }
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            int iCountRow = 0;
            int Linenum = 1;
            for (int i = 0; i < dtDGV.Rows.Count; i++)
            {
                if (Convert.ToDouble(dtDGV.Rows[i]["QTY"]) > 0) iCountRow += 1;
            }
            if (iCountRow == 0)
            {
                MsgBoxClass.MsgBoxShow_CheckDataBeforeSave("ไม่มีข้อมูลการสั่งสินค้า");
                return;
            }

            if (pStaOrderAgain == "1") { if (MsgBoxClass.MsgBoxShow_ConfirmInsert("กลุ่มสินค้านี้ มีการสั่งไปก่อนหน้านี้แล้ว ยืนยันการสั่งเพิ่ม") == DialogResult.No) return; }
            else { if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลการสั่งสินค้า ไม่สามารถแก้ไขรายการได้") == DialogResult.No) return; }

            string grpSub;
            try
            {
                grpSub = RadDropDownList_GrpSub.SelectedValue.ToString();
            }
            catch (Exception)
            {
                grpSub = "";
            }

            string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNOI", "-", "MNOI", "1");

            DataTable Dtdpt = Models.DptClass.GetDptDetail_ByDptID(RadDropDownList_GrpMain.SelectedValue.ToString());

            ArrayList sqlIn = new ArrayList
            {
                 $@"INSERT INTO SHOP_MNOI_HD (
                            DOCNO, DPTID, DPTNAME, GROUPID, 
                            DEPT_ID, DEPT_NAME, 
                            WHOIDINS, WHONAMEINS, DATE_RECIVE)
                    VALUES ('{maxDocno}', '{RadDropDownList_GrpMain.SelectedValue}', '{Dtdpt.Rows[0]["DATA_DESC"]}', '{grpSub}',
                            '{SystemClass.SystemDptID}', '{SystemClass.SystemDptName}',
                            '{SystemClass.SystemUserID}', '{SystemClass.SystemUserName}', '{radDateTimePicker_Begin.Value:yyyy-MM-dd}')"
            };

            for (int i = 0; i < dtDGV.Rows.Count; i++)
            {
                if (Convert.ToDouble(dtDGV.Rows[i]["QTY"]) > 0)
                {
                    sqlIn.Add($@"
                    INSERT INTO SHOP_MNOI_DT (
                                DOCNO, LINENUM, ITEMID, 
                                INVENTDIMID, ITEMBARCODE, SPC_ITEMNAME,
                                QTYORDER, UNITID, FACTOR, 
                                VENDERID, WHOIDINS, WHONAMEINS, 
                                REMARK, STADT)
                    VALUES      ('{maxDocno}', '{Linenum}', '{dtDGV.Rows[i]["ITEMID"]}',
                                '{dtDGV.Rows[i]["INVENTDIMID"]}', '{dtDGV.Rows[i]["ITEMBARCODE"]}', '{dtDGV.Rows[i]["SPC_ITEMNAME"]}',
                                '{float.Parse(dtDGV.Rows[i]["QTY"].ToString())}', '{dtDGV.Rows[i]["UNITID"]}', '{dtDGV.Rows[i]["QTY_UNITID"]}',
                                '{dtDGV.Rows[i]["VENDERID"]}', '{SystemClass.SystemUserID}', '{SystemClass.SystemUserName}', 
                                '{dtDGV.Rows[i]["REMARK"]}','1')");
                    Linenum++;
                }
                
            }
            string T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "")
            {
                radLabel_Docno.Text = maxDocno;
                radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                RadButton_Cancel.Enabled = true;
                RadButton_Save.Enabled = false;
                RadGridView_Show.ReadOnly = true;
            }

        }

        //ยกเลิกบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            string sql = string.Format(@"SELECT STA_APVDOC AS STAAPV FROM   SHOP_MNOI_HD WHERE DOCNO = '" + radLabel_Docno.Text + @"'  ");
            DataTable dtSelect = ConnectionClass.SelectSQL_Main(sql);
            if (dtSelect.Rows[0]["STAAPV"].ToString() == "1")
            {
                MessageBox.Show("บิลเลขที่ " + radLabel_Docno.Text + " ได้ยืนยันการรับไปแล้วจากแผนกจัดซื้อแล้ว" + Environment.NewLine + "ถ้าไม่ต้องการสินค้าในบิลนี้ ให้ติดต่อแผนกจัดซื้อโดยตรง.",
                    SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยกเลิก") == DialogResult.No) { return; }

            string sqlCancle = $@"  UPDATE  SHOP_MNOI_HD  
                                    SET     STA_DOC = '3',
                                            DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                            WHOIDUPD = '{SystemClass.SystemUserID}',
                                            WHONAMEUPD = '{SystemClass.SystemUserName}' 
                                    WHERE   DOCNO = '{radLabel_Docno.Text}' ";
            String T = ConnectionClass.ExecuteSQL_Main(sqlCancle);
            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยกเลิก");
            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                RadButton_Cancel.Enabled = false;
            }
        }

        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            MNOI_FindData frm = new MNOI_FindData("1");
            if (frm.ShowDialog(this) == DialogResult.Yes) SetDGV_Load(frm.pDocno);
        }

        void SetDGV_Load(string pDocno)
        {
            string sqlSelect = $@"
                SELECT	SHOP_MNOI_HD.DOCNO,DPTID AS GROUPMAIN, GROUPID AS GROUPSUB,
		                STA_DOC AS STADOC,STA_APVDOC AS STAAPV,
		                ITEMBARCODE,SPC_ITEMNAME,STADT AS STA,
		                QTYORDER AS QTY,UNITID,SHOP_MNOI_DT.REMARK AS REMARK
                FROM	SHOP_MNOI_HD WITH (NOLOCK)
		                INNER JOIN SHOP_MNOI_DT WITH (NOLOCK) ON SHOP_MNOI_HD.DOCNO = SHOP_MNOI_DT.DOCNO	
                WHERE	SHOP_MNOI_HD.DOCNO = '{pDocno}' AND STADT = '1'
                ORDER BY LINENUM
            ";

            dtDGV = ConnectionClass.SelectSQL_Main(sqlSelect);

            RadGridView_Show.DataSource = dtDGV;
            dtDGV.AcceptChanges();
            RadDropDownList_GrpMain.SelectedValue = dtDGV.Rows[0]["GROUPMAIN"].ToString();
            RadDropDownList_GrpSub.SelectedValue = dtDGV.Rows[0]["GROUPSUB"].ToString();

            radLabel_Docno.Text = dtDGV.Rows[0]["DOCNO"].ToString();

            RadDropDownList_GrpMain.Enabled = false; RadDropDownList_GrpSub.Enabled = false; RadButton_Search.Enabled = false;
            // check อนุมัติ

            if (dtDGV.Rows[0]["STADOC"].ToString() == "3")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black;
                radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                RadButton_Cancel.Enabled = false;
                RadButton_Save.Enabled = false;
                radDateTimePicker_Begin.Enabled = false;
                RadGridView_Show.ReadOnly = true;
            }
            else if (dtDGV.Rows[0]["STADOC"].ToString() == "1")
            {
                    radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black;
                    radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                    RadButton_Cancel.Enabled = true;
                    RadButton_Save.Enabled = false;
                    radDateTimePicker_Begin.Enabled = false;
                    RadGridView_Show.ReadOnly = true;
            }
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายการสินค้าเบิก " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Edit Qty
        private void RadGridView_Show_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "QTY":
                    if (RadGridView_Show.CurrentRow.Cells["BlockSend"].Value.ToString() == "1")
                    {
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00"; return;
                    }

                    try
                    {
                        float pQty = float.Parse(RadGridView_Show.CurrentRow.Cells["QTY"].Value.ToString());
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = pQty.ToString("N2");

                        if (Convert.ToDouble(RadGridView_Show.CurrentCell.Value) > 0) RadGridView_Show.CurrentRow.Cells["STA"].Value = "1";
                        else
                        {
                            RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00";
                            RadGridView_Show.CurrentRow.Cells["STA"].Value = "0";
                            RadGridView_Show.CurrentRow.Cells["REMARK"].Value = "0";
                        }

                    }
                    catch (Exception)
                    {
                        RadGridView_Show.CurrentRow.Cells["QTY"].Value = "0.00";
                        RadGridView_Show.CurrentRow.Cells["STA"].Value = "0";
                        RadGridView_Show.CurrentRow.Cells["REMARK"].Value = "0";
                        return;
                    }
                    break;
                case "STA":
                    SetDataSTA();
                    break;
                case "REMARK":
                    SetDataREMARK();
                    break;
                default:
                    break;
            }
        }
        //Open
        private void RadGridView_Show_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {
            if (pApv != "0") return;

            if ((e.Column.Name != "QTY") && (e.Column.Name != "REMARK"))
            {
                if (RadGridView_Show.MasterView.TableFilteringRow.Cells[0].IsCurrent == true) return;

                if (RadGridView_Show.MasterView.TableFilteringRow.Cells[1].IsCurrent == true) return;

                e.Cancel = true;
            }
        }

        private void RadRadioButton_P2_CheckStateChanged(object sender, EventArgs e)
        {
            if (radRadioButton_P2.CheckState == CheckState.Checked) RadDropDownList_GrpSub.Enabled = false;
            else RadDropDownList_GrpSub.Enabled = true;
        }
    }
}
