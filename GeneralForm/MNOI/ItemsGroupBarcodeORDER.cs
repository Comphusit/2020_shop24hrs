﻿using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using PC_Shop24Hrs.FormShare.ShowData;
using PC_Shop24Hrs.FormShare;
using PC_Shop24Hrs.GeneralForm.MNOR;
using System.Collections;

namespace PC_Shop24Hrs.GeneralForm.MNOI
{
    public partial class ItemsGroupBarcodeORDER : Telerik.WinControls.UI.RadForm
    {
        private readonly string _TypePage;//, _DptCode;
        private string TypeEdit;
        Data_ITEMBARCODE Items;
        readonly DataTable dtShow = new DataTable();
        //ตั้งค่าคู่กับหน้า  ItemsGroup
        public ItemsGroupBarcodeORDER(string TypePage)
        {
            InitializeComponent();
            _TypePage = TypePage;
        }
        private void ItemsGroupBarcodeORDER_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Dept);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_GroupItems);

            radButton_Save.ButtonElement.ShowBorder = true; radButton_Delete.ButtonElement.ShowBorder = true;

            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน"; RadButtonElement_pdt.ShowBorder = true;
            radButtonElement_Add.ToolTipText = "เพิ่ม"; radButtonElement_Add.ShowBorder = true;
            radButtonElement_Edit.ToolTipText = "แก้ไข"; radButtonElement_Edit.ShowBorder = true;
            radButtonElement_importExcel.ToolTipText = "Import Excel"; radButtonElement_importExcel.ShowBorder = true;
            if (SystemClass.SystemDptID != "D156") radButtonElement_importExcel.Enabled = false;

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);

            TypeEdit = "Default";

            SetDropdownlistDept(ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("64", "", "", "1"));
            if (RadDropDownList_Dept.SelectedItems.Count > 0 && radDropDownList_GroupItems.SelectedItems.Count > 0) SetDatagridview();

            ClearData(TypeEdit);
        }

        #region FUNCTION
        //ตั้งค่า แผนก
        void SetDropdownlistDept(DataTable DtDimension)
        {
            RadDropDownList_Dept.DataSource = DtDimension;
            RadDropDownList_Dept.DisplayMember = "SHOW_NAME";//DESCRIPTION
            RadDropDownList_Dept.ValueMember = "SHOW_ID";//NUM

            RadDropDownList_Dept.SelectedValue = SystemClass.SystemDptID;
            if (RadDropDownList_Dept.SelectedValue is null) RadDropDownList_Dept.SelectedIndex = 0;
        }
        //ตั้งค่ากลุ่ม
        void SetDropdownlistGroup(DataTable DtDimension)
        {
            radDropDownList_GroupItems.DataSource = DtDimension;
            radDropDownList_GroupItems.DisplayMember = "ITEMGROUPNAME";
            radDropDownList_GroupItems.ValueMember = "ITEMGROUPID";
        }
        //ตั้งค่า Grid
        void SetDatagridview()//DataTable DtItemBarcode
        {
            this.Cursor = Cursors.WaitCursor;
            radLabel_F2.Text = "กด + >> เพื่อเพิ่มบาร์โค้ดสินค้า | เลือกสินค้า กด / >> เพื่อแก้ไขสินค้า";
            if (RadGridView_Show.Columns.Count > 0) RadGridView_Show.Columns.Clear();

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 350));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMUNIT", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("QTY", "อัตราส่วน"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMDEPT", "แผนก"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("GROUPSUB", "กลุ่มสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMGROUPNAME", "กลุ่มสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("STA", "ใช้งาน"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMGROUPREMARK", "หมายเหตุ"));


            RadGridView_Show.DataSource = ItemGroupOrder_Class.OrderGroup_GetItemBarcode(RadDropDownList_Dept.SelectedValue.ToString(), radDropDownList_GroupItems.SelectedValue.ToString(), "");
            //iRow = -1;

            this.Cursor = Cursors.Default;
            return;
        }
        // clear ข้อมูล
        void ClearData(String Type)
        {
            switch (Type)
            {
                case "Default":
                    radTextBox_ItemBarcode.Enabled = false;
                    radTextBox_Rmk.Enabled = false;

                    radCheckBox_forOrder.Enabled = false;
                    radButton_Save.Enabled = false;
                    radButton_Delete.Enabled = false;
                    break;

                case "Add":
                    radDropDownList_GroupItems.Enabled = true;

                    radTextBox_ItemBarcode.Text = ""; radTextBox_ItemBarcode.Enabled = true;
                    radLabel_ItemName.Text = ""; radLabel_ItemName.Enabled = true;
                    radTextBox_Rmk.Text = ""; radTextBox_Rmk.Enabled = true;
                    radLabel_Unit.Text = "";

                    radCheckBox_forOrder.Enabled = true; radCheckBox_forOrder.Checked = true;


                    radButton_Save.Enabled = true;

                    radTextBox_ItemBarcode.SelectAll();
                    radTextBox_ItemBarcode.Focus();
                    break;

                case "Edit":
                    radDropDownList_GroupItems.Enabled = false;

                    radTextBox_ItemBarcode.Enabled = false;
                    radTextBox_Rmk.Enabled = true;
                    radCheckBox_forOrder.Enabled = true;
                    radCheckBox_forOrder.Enabled = true;

                    radButton_Save.Enabled = true;
                    radButton_Delete.Enabled = true;
                    break;
            }
        }
        //เปลี่ยนแผนก
        private void RadDropDownList_Dept_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.SelectedItems.Count > 0) SetDropdownlistGroup(ItemGroupOrder_Class.GetOrderGroup(RadDropDownList_Dept.SelectedValue.ToString(), "AND ITEMGROUPSTATUS = '1'"));
        }

        public void ImportExcel(string excelFilename)
        {
            DataTable DtSet = new DataTable();
            //int nonBarcode = 0;
            string sheetName;
            ArrayList sql24 = new ArrayList();
            TypeEdit = "Add";

            this.Cursor = Cursors.WaitCursor;

            System.Data.OleDb.OleDbConnection MyConnection = new System.Data.OleDb.OleDbConnection
               ("Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + excelFilename +
               @"';Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'");

            MyConnection.Open();
            DataTable dtExcelSheet = MyConnection.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);

            if (dtExcelSheet == null)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลใน File Excel ที่ระบุ{Environment.NewLine}เช็ค File Excel ใหม่อีกครั้ง");
                this.Cursor = Cursors.Default;
                return;
            }

            if (dtExcelSheet.Rows.Count == 1) //ดึงข้อมูลใน Excel ในกรณีมีแค่ 1 เอกสารในไฟล์
            {
                System.Data.OleDb.OleDbDataAdapter MyCommand1 = new System.Data.OleDb.OleDbDataAdapter($@"select * from [{dtExcelSheet.Rows[0]["TABLE_NAME"]}]  ", MyConnection);
                MyCommand1.TableMappings.Add("Table", "Net-informations.com");
                MyCommand1.Fill(DtSet);
            }
            else //ดึงข้อมูลใน Excel ในกรณีมีเอกสารมากกว่า 1 
            {
                ShowDataDGV frm = new ShowDataDGV("2")
                { dtData = dtExcelSheet };

                if (frm.ShowDialog(this) == DialogResult.Yes) sheetName = frm.pSheet;
                else{ this.Cursor = Cursors.Default; return;}
                System.Data.OleDb.OleDbDataAdapter MyCommand = new System.Data.OleDb.OleDbDataAdapter($@"select * from [{sheetName}]  ", MyConnection);
                MyCommand.TableMappings.Add("Table", "Net-informations.com");
                MyCommand.Fill(DtSet);
            }


            //ไม่พบข้อมูลสินค้า
            if (DtSet.Rows.Count == 0) { MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่พบข้อมูลสำหรับ File Excel ที่เลือก{Environment.NewLine}เช็ค File Excel ใหม่อีกครั้ง"); return; }


            for (int i = 3; i < DtSet.Rows.Count; i++)
            {
                this.Items = new Data_ITEMBARCODE(DtSet.Rows[i][0].ToString());

                //check barcode

                if (string.IsNullOrEmpty(this.Items.Itembarcode_ITEMBARCODE))
                {
                    //nonBarcode++;
                    MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"ไม่พบบาร์โค๊ด [{DtSet.Rows[i][0]}] ตรวจสอบไฟล์ Excel อีกครั้ง.");
                    this.Cursor = Cursors.Default;
                    return;
                }
                else
                {
                    DataTable DtBarcode = ItemGroupOrder_Class.OrderGroup_GetItemBarcode(RadDropDownList_Dept.SelectedValue.ToString(),
                                                                                        radDropDownList_GroupItems.SelectedValue.ToString(),
                                                                                        $@"AND SHOP_ITEMORDERBARCODE.ITEMBARCODE = '{this.Items.Itembarcode_ITEMBARCODE}'");
                    if (DtBarcode.Rows.Count > 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"บาร์โค๊ด [{this.Items.Itembarcode_ITEMBARCODE} - {this.Items.Itembarcode_SPC_ITEMNAME}]{Environment.NewLine}มีอยู่แล้วไม่ต้องเพิ่มซ้ำ.");
                        this.Cursor = Cursors.Default;
                        return;
                    }
                    else
                    {
                        DataTable DtCheck = ItemGroupOrder_Class.OrderGroup_GetItemBarcode(RadDropDownList_Dept.SelectedValue.ToString(),
                                                                                        "",
                                                                                        $@"AND SHOP_ITEMORDERBARCODE.ITEMBARCODE = '{this.Items.Itembarcode_ITEMBARCODE}'");
                        if (DtCheck.Rows.Count > 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Imformation($@"บาร์โค๊ด [{this.Items.Itembarcode_ITEMBARCODE} - {this.Items.Itembarcode_SPC_ITEMNAME}]{Environment.NewLine}มีอยู่แล้วในกลุ่มย่อยอื่น ตรวจสอบข้อมูลใหม่อีกครั้ง.");
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }

                    SaveItemGroup var_ITEMBARCODE = new SaveItemGroup
                    {
                        ITEMGROUPDEPT = RadDropDownList_Dept.SelectedValue.ToString(),
                        GROUPSUB = radDropDownList_GroupItems.SelectedValue.ToString(),
                        ITEMBARCODE = this.Items.Itembarcode_ITEMBARCODE,
                        ITEMNAME = this.Items.Itembarcode_SPC_ITEMNAME,
                        ITEMUNIT = this.Items.Itembarcode_UNITID,
                        ITEMGROUPREMARK = radTextBox_Rmk.Text,
                        ITEMGROUPSTATUS = "1",
                        ITEMGROUPVEND = this.Items.Itembarcode_VENDORID,
                        ITEMGROUPVENDNAME = this.Items.Itembarcode_VENDORIDNAME,
                        INVENTDIMID = this.Items.Itembarcode_INVENTDIMID,
                        ITEMID = this.Items.Itembarcode_ITEMID,
                    };

                    sql24.Add(ItemGroupOrder_Class.Save_OrderGroupBarcode(TypeEdit, var_ITEMBARCODE));
                }
            }
          string resualt = ConnectionClass.ExecuteSQL_ArrayMain(sql24);
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "")
            {
                SetDatagridview();
                TypeEdit = "Default"; ClearData(TypeEdit);
            }
            this.Cursor = Cursors.Default;
        }
        #endregion

        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }


        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        #endregion


        #region EVEN

        private void RadTextBox_ItemBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:

                    if (string.IsNullOrEmpty(radTextBox_ItemBarcode.Text))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ บาร์โค้ด ให้เรียบร้อย.");
                        radTextBox_ItemBarcode.SelectAll();
                        radTextBox_ItemBarcode.Focus();
                        return;
                    }
                    this.Items = new Data_ITEMBARCODE(radTextBox_ItemBarcode.Text);
                    if (string.IsNullOrEmpty(this.Items.Itembarcode_ITEMBARCODE))
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่เจอบาร์โค๊ดที่ต้องการค้นหา.");
                        radTextBox_ItemBarcode.SelectAll();
                        radTextBox_ItemBarcode.Focus();
                        return;
                    }

                    radLabel_ItemName.Text = this.Items.Itembarcode_SPC_ITEMNAME;
                    radLabel_Unit.Text = this.Items.Itembarcode_UNITID;
                    radTextBox_Rmk.Focus();
                    break;
                case Keys.F4:
                    String SqlCondition = string.Format($@" AND INVENTTABLE.DIMENSION='{RadDropDownList_Dept.SelectedValue}'");
                    using (ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new FormShare.ShowData.ShowDataDGV_Itembarcode(SqlCondition))
                    {
                        DialogResult dr = ShowDataDGV_Itembarcode.ShowDialog();

                        if (dr == DialogResult.Yes)
                        {
                            this.Items = ShowDataDGV_Itembarcode.items;
                            radTextBox_ItemBarcode.Text = this.Items.Itembarcode_ITEMBARCODE;
                            radLabel_ItemName.Text = this.Items.Itembarcode_SPC_ITEMNAME;
                            radLabel_Unit.Text = this.Items.Itembarcode_UNITID;
                        }
                    }
                    break;
            }
        }

        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            TypeEdit = "Default"; ClearData(TypeEdit);
            if (radDropDownList_GroupItems.SelectedItems.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเพิ่มกลุ่มสินค้า ก่อนเพิ่มสินค้า.");
                return;
            }
            TypeEdit = "Add"; ClearData(TypeEdit);
        }

        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            TypeEdit = "Default"; ClearData(TypeEdit);
            if (radDropDownList_GroupItems.SelectedItems.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเพิ่มกลุ่มสินค้า ก่อนแก้ไขสินค้า.");
                return;
            }

            if (radTextBox_ItemBarcode.Text == "")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาเลือกรายการก่อนการกดแก้ไข.");
                return;
            }
            TypeEdit = "Edit"; ClearData(TypeEdit);
        }

        private void RadButtonElement_importExcel_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(radDropDownList_GroupItems.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อกลุ่มสินค้า ให้เรียบร้อย.");
                radTextBox_ItemBarcode.SelectAll();
                radTextBox_ItemBarcode.Focus();
                return;
            }

            OpenFileDialog fileDialog = new OpenFileDialog
            {
                FileName = @"",
                //fileDialog.Filter = "Excel Worksheets 2003(*.xls)|*.xls|Excel Worksheets 2007(*.xlsx)|*.xlsx|Word Documents(*.doc)|*.doc"
                Filter = "Excel Files|*.xls;*.xlsx",
                FilterIndex = 2,
                RestoreDirectory = true
            };

            if (fileDialog.ShowDialog() != DialogResult.OK) return;

            try
            {
                ImportExcel(@fileDialog.FileName);
            }
            catch (Exception ex)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถ Import ไฟล์ excel ได้{Environment.NewLine}{ex.Message}");
                return;
            }
        }

        private void RadGridView_Show_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count > 0 && RadGridView_Show.CurrentRow.Index > -1)
            {
                radDropDownList_GroupItems.SelectedValue = RadGridView_Show.CurrentRow.Cells["GROUPSUB"].Value.ToString();

                radTextBox_ItemBarcode.Text = RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();
                radLabel_ItemName.Text = RadGridView_Show.CurrentRow.Cells["ITEMNAME"].Value.ToString();
                radLabel_Unit.Text = RadGridView_Show.CurrentRow.Cells["ITEMUNIT"].Value.ToString();
                if (RadGridView_Show.CurrentRow.Cells["STA"].Value.ToString() == "1") radCheckBox_forOrder.Checked = true;
                else radCheckBox_forOrder.Checked = false;

                TypeEdit = "Default"; ClearData(TypeEdit);
            }
        }

        //ลบสินค้า
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (TypeEdit == "Edit")
            {
                if (MsgBoxClass.MsgBoxShow_ConfirmDelete(Environment.NewLine +
                   "กลุ่มสินค้า " + RadGridView_Show.CurrentRow.Cells["ITEMGROUPNAME"].Value.ToString() + Environment.NewLine +
                   "บาร์โค๊ด " + radTextBox_ItemBarcode.Text + Environment.NewLine +
                   "ชื่อสินค้า " + radLabel_ItemName.Text + Environment.NewLine) == DialogResult.Yes)
                {

                    SaveItemGroup var_ITEMBARCODE = new SaveItemGroup
                    {
                        ITEMGROUPDEPT = RadDropDownList_Dept.SelectedValue.ToString(),
                        GROUPSUB = radDropDownList_GroupItems.SelectedValue.ToString(),
                        ITEMBARCODE = radTextBox_ItemBarcode.Text,
                    };

                    string SqlTranctions = ConnectionClass.ExecuteSQL_Main(ItemGroupOrder_Class.Save_OrderGroupBarcode("Del", var_ITEMBARCODE));
                    MsgBoxClass.MsgBoxShow_SaveStatus(SqlTranctions);
                    if (SqlTranctions == "") SetDatagridview();
                }
            }

            TypeEdit = "Default"; ClearData(TypeEdit);
            this.Cursor = Cursors.Default;
        }
        //บันทึก
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(radLabel_ItemName.Text))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Imformation("กรุณาใส่ ชื่อกลุ่มสินค้า ให้เรียบร้อย.");
                radTextBox_ItemBarcode.SelectAll();
                radTextBox_ItemBarcode.Focus();
                return;
            }
            string STA = "1";
            if (radCheckBox_forOrder.CheckState != CheckState.Checked) STA = "0";

            SaveItemGroup var_ITEMBARCODE = new SaveItemGroup
            {
                ITEMGROUPDEPT = RadDropDownList_Dept.SelectedValue.ToString(),
                GROUPSUB = radDropDownList_GroupItems.SelectedValue.ToString(),
                ITEMBARCODE = radTextBox_ItemBarcode.Text,
                ITEMNAME = radLabel_ItemName.Text,
                ITEMUNIT = radLabel_Unit.Text,
                ITEMGROUPREMARK = radTextBox_Rmk.Text,
                ITEMGROUPSTATUS = STA,
            };

            if (TypeEdit == "Add")
            {
                var_ITEMBARCODE.ITEMDEPTNAME = this.Items.Itembarcode_DESCRIPTION;
                var_ITEMBARCODE.ITEMGROUPVEND = this.Items.Itembarcode_VENDORID;
                var_ITEMBARCODE.ITEMGROUPVENDNAME = this.Items.Itembarcode_VENDORIDNAME;
                var_ITEMBARCODE.INVENTDIMID = this.Items.Itembarcode_INVENTDIMID;
                var_ITEMBARCODE.ITEMID = this.Items.Itembarcode_ITEMID;
            }

            string resualt = ConnectionClass.ExecuteSQL_Main(ItemGroupOrder_Class.Save_OrderGroupBarcode(TypeEdit, var_ITEMBARCODE));
            //string resualt = "";
            MsgBoxClass.MsgBoxShow_SaveStatus(resualt);
            if (resualt == "")
            {
                SetDatagridview();
                TypeEdit = "Default"; ClearData(TypeEdit);
            }
            this.Cursor = Cursors.Default;

        }
        //doument
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _TypePage);
        }

        //Ket EmpID
        private void RadTextBox_EmplId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.')) e.Handled = true;
        }
        //เปลี่ยนกลุ่มสินค้า
        private void RadDropDownList_GroupItems_SelectedValueChanged(object sender, EventArgs e)
        {
            if (RadDropDownList_Dept.SelectedItems.Count > 0 && radDropDownList_GroupItems.SelectedItems.Count > 0) SetDatagridview();
        }


        #endregion

        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            if (RadGridView_Show.ColumnCount > 0 && RadGridView_Show.RowCount > 0) if (RadGridView_Show.CurrentColumn.Index < 6) return;
            if (radDropDownList_GroupItems.SelectedValue.ToString() != "D034_01") return;

            double cost = double.Parse(RadGridView_Show.CurrentRow.Cells[e.ColumnIndex].Value.ToString());

            InputData frmQTY = new InputData("0",
                                     RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " - " + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                     "ราคา สำหรับ " + RadGridView_Show.Columns[e.ColumnIndex].HeaderText, RadGridView_Show.CurrentRow.Cells["ITEMUNIT"].Value.ToString())
            { pInputData = cost.ToString("N2") };

            if (frmQTY.ShowDialog(this) == DialogResult.Yes)
            {
                string res = ConnectionClass.ExecuteSQL_Main(ItemGroup_Class.Save_ItemCostByBranchID(RadGridView_Show.Columns[e.ColumnIndex].Name,
                        RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                        RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                        Double.Parse(frmQTY.pInputData)));
                if (res == "")
                {
                    dtShow.Rows[e.RowIndex][e.Column.FieldName] = Convert.ToDouble(frmQTY.pInputData).ToString("#,#0.00");
                    dtShow.AcceptChanges();
                }
                else MsgBoxClass.MsgBoxShow_SaveStatus(res);
            }

        }

       
    }
}
