﻿using System.Data;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.MNOI
{
    public class SaveItemGroup
    {
        //ITEMGROUP
        public string ITEMGROUPID { get; set; } = "";
        public string ITEMGROUPNAME { get; set; } = "";
        public string ITEMGROUPDEPT { get; set; } = ""; //ITEMDEPT
        public string ITEMGROUPVEND { get; set; } = ""; //VENDERID
        public string ITEMGROUPVENDNAME { get; set; } = ""; //VENDERNAME
        public string ITEMGROUPREMARK { get; set; } = "";
        public string ITEMGROUPSTATUS { get; set; } = "";

        //BARCODE
        public string ITEMBARCODE { get; set; } = "";
        public string ITEMID { get; set; } = "";
        public string INVENTDIMID { get; set; } = "";
        public string ITEMNAME { get; set; } = "";
        public string ITEMUNIT { get; set; } = "";
        public string ITEMDEPTNAME { get; set; } = "";
        public string GROUPSUB { get; set; } = "";
    }
    public class ItemGroupOrder_Class
    {
        public static DataTable GetOrderGroup(string Dept, string condition)
        {
            string Sql = $@"
                        SELECT	ITEMGROUPID, ITEMGROUPNAME, ITEMGROUPDEPT, ITEMGROUPVEND, ITEMGROUPVENDNAME, ITEMGROUPREMARK, ITEMGROUPSTATUS
                        FROM	SHOP_ITEMORDERGROUP WITH (NOLOCK)
                        WHERE	ITEMGROUPDEPT = '{Dept}'
                        {condition}";
            return ConnectionClass.SelectSQL_Main(Sql);
        }

        public static string Save_OrderGroup(string typeCase, SaveItemGroup SAVE)
        {
            string sql;
            if (typeCase == "Edit")
            {
                sql = $@"
                UPDATE	SHOP_ITEMORDERGROUP
                SET		ITEMGROUPNAME = '{SAVE.ITEMGROUPNAME}',
		                ITEMGROUPVEND = '{SAVE.ITEMGROUPVEND}',
		                ITEMGROUPVENDNAME = '{SAVE.ITEMGROUPVENDNAME}',
		                ITEMGROUPREMARK= '{SAVE.ITEMGROUPREMARK}',
		                ITEMGROUPSTATUS = '{SAVE.ITEMGROUPSTATUS}',
		                WHOUP = '{SystemClass.SystemUserID}',
		                WHONAMEUP = '{SystemClass.SystemUserName}'
                WHERE	ITEMGROUPID = '{SAVE.ITEMGROUPID}'";
            }
            else
            {
                sql = $@"
                INSERT INTO SHOP_ITEMORDERGROUP
			                (ITEMGROUPID, ITEMGROUPNAME, ITEMGROUPDEPT, 
			                ITEMGROUPVEND, ITEMGROUPVENDNAME, ITEMGROUPREMARK,
			                WHOINS, WHONAMEINS)
                VALUES		('{SAVE.ITEMGROUPID}','{SAVE.ITEMGROUPNAME}','{SAVE.ITEMGROUPDEPT}',
			                '{SAVE.ITEMGROUPVEND}','{SAVE.ITEMGROUPVENDNAME}','{SAVE.ITEMGROUPREMARK}',
			                '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}')";
            }
            return sql;
        }
        public static string Save_OrderGroupBarcode(string typeCase, SaveItemGroup var_ITEMBARCODE)
        {
            string sql = "";
            switch (typeCase)
            {
                case "Add": // ADD
                    sql = $@"
                       INSERT INTO SHOP_ITEMORDERBARCODE
		                        (ITEMBARCODE, ITEMNAME, ITEMDEPT,
		                        GROUPSUB, VENDERID, VENDERNAME, ITEMUNIT,
		                        REMARK, WHOIDINS, WHONAMEINS, STA,
                                ITEMID, INVENTDIMID)
                        VALUES	('{var_ITEMBARCODE.ITEMBARCODE}','{var_ITEMBARCODE.ITEMNAME}','{var_ITEMBARCODE.ITEMGROUPDEPT}',
		                        '{var_ITEMBARCODE.GROUPSUB}','{var_ITEMBARCODE.ITEMGROUPVEND}','{var_ITEMBARCODE.ITEMGROUPVENDNAME}','{var_ITEMBARCODE.ITEMUNIT}',
		                        '{var_ITEMBARCODE.ITEMGROUPREMARK}','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','{var_ITEMBARCODE.ITEMGROUPSTATUS}',
                                '{var_ITEMBARCODE.ITEMID}','{var_ITEMBARCODE.INVENTDIMID}') ";
                    return sql;
                case "Edit":
                    sql = $@" 
                        UPDATE  SHOP_ITEMORDERBARCODE 
                        SET     REMARK = '{var_ITEMBARCODE.ITEMGROUPREMARK}',
                                STA = '{var_ITEMBARCODE.ITEMGROUPSTATUS}',
                                WHOIDUPD = '{SystemClass.SystemUserID}',
                                WHONAMEUPD = '{SystemClass.SystemUserName}',
                                DATEUPD = CONVERT(nvarchar, getdate(), 23)
                        WHERE   ITEMBARCODE = '{var_ITEMBARCODE.ITEMBARCODE}' 
                                AND ITEMDEPT = '{var_ITEMBARCODE.ITEMGROUPDEPT}' ";
                    return sql;
                case "Del":
                    sql = $@" 
                        DELETE  
                        FROM    SHOP_ITEMORDERBARCODE 
                        WHERE   ITEMBARCODE = '{var_ITEMBARCODE.ITEMBARCODE}' 
                                AND ITEMDEPT = '{var_ITEMBARCODE.ITEMGROUPDEPT}' 
                                AND GROUPSUB = '{var_ITEMBARCODE.GROUPSUB}' ";
                    return sql;
                default:
                    return sql;

            }

        }
        public static DataTable OrderGroup_GetItemBarcode(string Dept, string GroupSub, string pCon)
        {
            string groupsub = "";
            if (GroupSub != "") groupsub = $@"AND ITEMGROUPID = '{GroupSub}'";
            string sql = $@"
                    SELECT	INVENTITEMBARCODE.ITEMBARCODE, SHOP_ITEMORDERBARCODE.ITEMID, SHOP_ITEMORDERBARCODE.INVENTDIMID,  
		                    ITEMNAME, QTY, ITEMUNIT,
		                    ITEMDEPT, DIMENSIONS.Description AS ITEMDEPTNAME, VENDERID, 
		                    VENDERNAME, GROUPSUB, ITEMGROUPNAME, STA
                    FROM	SHOP_ITEMORDERBARCODE WITH (NOLOCK) 
		                    INNER JOIN SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) 
			                    ON INVENTITEMBARCODE.ITEMBARCODE = SHOP_ITEMORDERBARCODE.ITEMBARCODE
			                    AND INVENTITEMBARCODE.DATAAREAID = N'SPC'
		                    INNER JOIN SHOP2013TMP.dbo.DIMENSIONS  WITH (NOLOCK) ON DIMENSIONS.NUM = SHOP_ITEMORDERBARCODE.ITEMDEPT
		            INNER JOIN SHOP_ITEMORDERGROUP WITH (NOLOCK) 
			            ON SHOP_ITEMORDERBARCODE.GROUPSUB = SHOP_ITEMORDERGROUP.ITEMGROUPID
			            {groupsub}
            WHERE	ITEMDEPT = '{Dept}'	 
                    {pCon}
            ";
            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
