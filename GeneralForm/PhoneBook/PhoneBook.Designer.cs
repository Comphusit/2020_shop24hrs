﻿namespace PC_Shop24Hrs.GeneralForm.PhoneBook
{
    partial class PhoneBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhoneBook));
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radStatusStrip_Main = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Edit = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_delete = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_Excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator5 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator6 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.panel_Edit = new System.Windows.Forms.Panel();
            this.radTextBox_Head = new Telerik.WinControls.UI.RadTextBox();
            this.radDropDownList_Dpt = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_Tel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_HeadName = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Tel = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radTextBox_Desc = new Telerik.WinControls.UI.RadTextBox();
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel_Show = new Telerik.WinControls.UI.RadPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel_Main = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            this.panel_Edit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Head)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_HeadName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Show)).BeginInit();
            this.radPanel_Show.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.commandBarSeparator1.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator1.UseCompatibleTextRendering = false;
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // radStatusStrip_Main
            // 
            this.radStatusStrip_Main.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip_Main.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radStatusStrip_Main.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_add,
            this.commandBarSeparator2,
            this.radButtonElement_Edit,
            this.commandBarSeparator3,
            this.radButtonElement_delete,
            this.commandBarSeparator4,
            this.radButtonElement_Excel,
            this.commandBarSeparator5,
            this.radButtonElement_pdt,
            this.commandBarSeparator6});
            this.radStatusStrip_Main.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip_Main.Name = "radStatusStrip_Main";
            this.radStatusStrip_Main.Size = new System.Drawing.Size(192, 34);
            this.radStatusStrip_Main.TabIndex = 3;
            // 
            // radButtonElement_add
            // 
            this.radButtonElement_add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_add.Name = "radButtonElement_add";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_add, false);
            this.radButtonElement_add.Text = "radButtonElement1";
            this.radButtonElement_add.ToolTipText = "เพิ่ม";
            this.radButtonElement_add.UseCompatibleTextRendering = false;
            this.radButtonElement_add.Click += new System.EventHandler(this.RadButtonElement_add_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            this.commandBarSeparator2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator2.UseCompatibleTextRendering = false;
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Edit
            // 
            this.radButtonElement_Edit.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Edit.Image = global::PC_Shop24Hrs.Properties.Resources.edit;
            this.radButtonElement_Edit.Name = "radButtonElement_Edit";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_Edit, false);
            this.radButtonElement_Edit.Text = "radButtonElement2";
            this.radButtonElement_Edit.ToolTipText = "แก้ไข";
            this.radButtonElement_Edit.UseCompatibleTextRendering = false;
            this.radButtonElement_Edit.Click += new System.EventHandler(this.RadButtonElement_Edit_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarSeparator3.UseCompatibleTextRendering = false;
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_delete
            // 
            this.radButtonElement_delete.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButtonElement_delete.Image = global::PC_Shop24Hrs.Properties.Resources.trash;
            this.radButtonElement_delete.Name = "radButtonElement_delete";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_delete, false);
            this.radButtonElement_delete.Text = "";
            this.radButtonElement_delete.ToolTipText = "ลบ";
            this.radButtonElement_delete.Click += new System.EventHandler(this.RadButtonElement_delete_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_Excel
            // 
            this.radButtonElement_Excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_Excel.Name = "radButtonElement_Excel";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_Excel, false);
            this.radButtonElement_Excel.Text = "radButtonElement1";
            this.radButtonElement_Excel.Click += new System.EventHandler(this.RadButtonElement_Excel_Click);
            // 
            // commandBarSeparator5
            // 
            this.commandBarSeparator5.Name = "commandBarSeparator5";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator5, false);
            this.commandBarSeparator5.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_pdt
            // 
            this.radButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButtonElement_pdt.Name = "radButtonElement_pdt";
            this.radStatusStrip_Main.SetSpring(this.radButtonElement_pdt, false);
            this.radButtonElement_pdt.Text = "radButtonElement1";
            this.radButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator6
            // 
            this.commandBarSeparator6.Name = "commandBarSeparator6";
            this.radStatusStrip_Main.SetSpring(this.commandBarSeparator6, false);
            this.commandBarSeparator6.VisibleInOverflowMenu = false;
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(699, 660);
            this.radGridView_Show.TabIndex = 2;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.radGridView_Show.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.radGridView_Show.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.radGridView_Show.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            this.radGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // panel_Edit
            // 
            this.panel_Edit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Edit.Controls.Add(this.radTextBox_Head);
            this.panel_Edit.Controls.Add(this.radStatusStrip_Main);
            this.panel_Edit.Controls.Add(this.radDropDownList_Dpt);
            this.panel_Edit.Controls.Add(this.radLabel_Tel);
            this.panel_Edit.Controls.Add(this.radLabel_HeadName);
            this.panel_Edit.Controls.Add(this.radTextBox_Tel);
            this.panel_Edit.Controls.Add(this.radLabel3);
            this.panel_Edit.Controls.Add(this.radButton_Cancel);
            this.panel_Edit.Controls.Add(this.radTextBox_Desc);
            this.panel_Edit.Controls.Add(this.radButton_Save);
            this.panel_Edit.Controls.Add(this.radLabel1);
            this.panel_Edit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Edit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel_Edit.Location = new System.Drawing.Point(714, 3);
            this.panel_Edit.Name = "panel_Edit";
            this.panel_Edit.Size = new System.Drawing.Size(194, 691);
            this.panel_Edit.TabIndex = 3;
            // 
            // radTextBox_Head
            // 
            this.radTextBox_Head.Enabled = false;
            this.radTextBox_Head.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Head.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Head.Location = new System.Drawing.Point(7, 290);
            this.radTextBox_Head.Name = "radTextBox_Head";
            this.radTextBox_Head.Size = new System.Drawing.Size(182, 21);
            this.radTextBox_Head.TabIndex = 36;
            // 
            // radDropDownList_Dpt
            // 
            this.radDropDownList_Dpt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownList_Dpt.BackColor = System.Drawing.Color.White;
            this.radDropDownList_Dpt.DropDownHeight = 150;
            this.radDropDownList_Dpt.Font = new System.Drawing.Font("Tahoma", 10F);
            this.radDropDownList_Dpt.Location = new System.Drawing.Point(5, 229);
            this.radDropDownList_Dpt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.radDropDownList_Dpt.Name = "radDropDownList_Dpt";
            // 
            // 
            // 
            this.radDropDownList_Dpt.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.radDropDownList_Dpt.Size = new System.Drawing.Size(184, 25);
            this.radDropDownList_Dpt.TabIndex = 2;
            this.radDropDownList_Dpt.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Dpt_SelectedValueChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Dpt.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.radDropDownList_Dpt.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0))).DrawText = false;
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Visible;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.radDropDownList_Dpt.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // radLabel_Tel
            // 
            this.radLabel_Tel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Tel.Location = new System.Drawing.Point(15, 52);
            this.radLabel_Tel.Name = "radLabel_Tel";
            this.radLabel_Tel.Size = new System.Drawing.Size(59, 19);
            this.radLabel_Tel.TabIndex = 23;
            this.radLabel_Tel.Text = "เบอร์โทร";
            // 
            // radLabel_HeadName
            // 
            this.radLabel_HeadName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_HeadName.Location = new System.Drawing.Point(15, 264);
            this.radLabel_HeadName.Name = "radLabel_HeadName";
            this.radLabel_HeadName.Size = new System.Drawing.Size(49, 19);
            this.radLabel_HeadName.TabIndex = 27;
            this.radLabel_HeadName.Text = "หัวหน้า";
            // 
            // radTextBox_Tel
            // 
            this.radTextBox_Tel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Tel.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Tel.Location = new System.Drawing.Point(5, 77);
            this.radTextBox_Tel.Name = "radTextBox_Tel";
            this.radTextBox_Tel.Size = new System.Drawing.Size(184, 21);
            this.radTextBox_Tel.TabIndex = 0;
            this.radTextBox_Tel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Tel_KeyDown);
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(15, 203);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(43, 19);
            this.radLabel3.TabIndex = 26;
            this.radLabel3.Text = "แผนก";
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(99, 327);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 4;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radTextBox_Desc
            // 
            this.radTextBox_Desc.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_Desc.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Desc.Location = new System.Drawing.Point(5, 129);
            this.radTextBox_Desc.Multiline = true;
            this.radTextBox_Desc.Name = "radTextBox_Desc";
            // 
            // 
            // 
            this.radTextBox_Desc.RootElement.StretchVertically = true;
            this.radTextBox_Desc.Size = new System.Drawing.Size(184, 68);
            this.radTextBox_Desc.TabIndex = 1;
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(1, 327);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 3;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(15, 104);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(65, 19);
            this.radLabel1.TabIndex = 25;
            this.radLabel1.Text = "คำอธิบาย";
            // 
            // radPanel_Show
            // 
            this.radPanel_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPanel_Show.Controls.Add(this.tableLayoutPanel1);
            this.radPanel_Show.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radPanel_Show.Location = new System.Drawing.Point(3, 3);
            this.radPanel_Show.Name = "radPanel_Show";
            this.radPanel_Show.Size = new System.Drawing.Size(705, 691);
            this.radPanel_Show.TabIndex = 4;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(705, 691);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 669);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(699, 19);
            this.radLabel_Detail.TabIndex = 56;
            // 
            // tableLayoutPanel_Main
            // 
            this.tableLayoutPanel_Main.ColumnCount = 2;
            this.tableLayoutPanel_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel_Main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel_Main.Controls.Add(this.radPanel_Show, 0, 0);
            this.tableLayoutPanel_Main.Controls.Add(this.panel_Edit, 1, 0);
            this.tableLayoutPanel_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_Main.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel_Main.Name = "tableLayoutPanel_Main";
            this.tableLayoutPanel_Main.RowCount = 1;
            this.tableLayoutPanel_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel_Main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel_Main.Size = new System.Drawing.Size(911, 697);
            this.tableLayoutPanel_Main.TabIndex = 5;
            // 
            // PhoneBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 697);
            this.Controls.Add(this.tableLayoutPanel_Main);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PhoneBook";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "เบอร์โทรภายใน.";
            this.Load += new System.EventHandler(this.PhoneBook_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PhoneBook_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip_Main)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            this.panel_Edit.ResumeLayout(false);
            this.panel_Edit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Head)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_HeadName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Desc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel_Show)).EndInit();
            this.radPanel_Show.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel_Main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip_Main;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Edit;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        protected System.Windows.Forms.Panel panel_Edit;
        private Telerik.WinControls.UI.RadLabel radLabel_Tel;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Tel;
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Desc;
        private Telerik.WinControls.UI.RadLabel radLabel_HeadName;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList_Dpt;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_delete;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
        private Telerik.WinControls.UI.RadPanel radPanel_Show;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Head;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator5;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Main;
    }
}
