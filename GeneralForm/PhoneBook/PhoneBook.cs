﻿//CheckOK
using System;
using PC_Shop24Hrs.Controllers;
using System.Data;
using Telerik.WinControls.UI;
using System.Windows.Forms;
using System.Diagnostics;

namespace PC_Shop24Hrs.GeneralForm.PhoneBook
{
    public partial class PhoneBook : Telerik.WinControls.UI.RadForm
    {
        DataTable DtTel = new DataTable();
        string StatusSave = "";
        readonly string _pTypeReport;

        public PhoneBook(string pTypeReport) //1 : คือ คนดู PhoneBook 0 : คือ สิทธิ์ที่สามารถแก้ไขเบอร์โทรได้ PhoneBook / 2 โปรแกรมที่ใช้งาน / 3 คือ Server VM
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
        }

        private void PhoneBook_Load(object sender, EventArgs e)
        {
            ClearData();

            radButtonElement_add.ShowBorder = true; radButtonElement_add.ToolTipText = "เพิ่มข้อ";
            radButtonElement_Edit.ShowBorder = true; radButtonElement_Edit.ToolTipText = "แก้ไขข้อมูล";
            radButtonElement_delete.ShowBorder = true; radButtonElement_delete.ToolTipText = "ลบข้อมูล";
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "Export Excel";
            radButtonElement_pdt.ShowBorder = true; radButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radButton_Cancel.ButtonElement.ShowBorder = true; radButton_Save.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dpt);

           
            panel_Edit.Visible = false;
            //SetDefault
            DatagridClass.SetDefaultRadGridView(radGridView_Show);
            if (_pTypeReport == "2")
            {
                radPanel_Show.Dock = DockStyle.Fill;
                tableLayoutPanel_Main.ColumnStyles.RemoveAt(1);
                radButtonElement_add.Enabled = false; radButtonElement_Edit.Enabled = false; radButtonElement_delete.Enabled = false; radButtonElement_pdt.Enabled = true;

                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Image("ICON", "ICON", 100));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROGRAM_NAME", "ชื่อโปรแกรม", 200));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROGRAM_DESC", "คำอธิบายโปรแกรม", 300));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PROGRAM_ICON", "ICON"));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROGRAM_DEVICE", "อุปกรณ์", 80));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROGRAM_TEL", "ติดต่อโทร", 200));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PRO", "โปรแกรมเมอร์", 300));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROGRAM_REMARK", "หมายเหตุ", 200));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PROGRAM_INSTALL", "LinkInstall", 500));
                radGridView_Show.TableElement.RowHeight = 80;
                radGridView_Show.Columns["ICON"].IsPinned = true;
                radGridView_Show.Columns["PROGRAM_NAME"].IsPinned = true;
                radLabel_Detail.Text = "DoubleClick LinkInstall >> เพื่อเปิดใช้งานโปรแกรมหรือติดตั้งโปรแกรม | ข้อมูลโปรแกรมจะแสดงเฉพาะในส่วนงานรับผิดชอบของแผนก ComPhusit และ ComMinimark เท่านั้น";
            }

            if ((_pTypeReport == "0") || (_pTypeReport == "1"))
            {
                if (_pTypeReport == "0")
                {
                    panel_Edit.Visible = true;
                    panel_Edit.Dock = DockStyle.Fill;
                    radButtonElement_add.Enabled = true; radButtonElement_Edit.Enabled = true; radButtonElement_delete.Enabled = true; radButtonElement_pdt.Enabled = true;
                }
                else
                {
                    tableLayoutPanel_Main.ColumnStyles.RemoveAt(1);
                    radPanel_Show.Dock = DockStyle.Fill;
                    radButtonElement_add.Enabled = false; radButtonElement_Edit.Enabled = false; radButtonElement_delete.Enabled = false; radButtonElement_pdt.Enabled = true;
                }

                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PHONEID", "เบอร์โทร", 100));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PHONEDESC", "คำอธิบาย", 300));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("PHONEDPT", "รหัส", 80));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนก", 250));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "หัวหน้า", 200));
                radLabel_Detail.Text = "ในกรณีที่ไม่มีเบอร์ใหม่ สามารถแจ้งเพิ่มได้ที่แผนกขายส่ง/ComService/ComMinimart";
            }

            SetDGV();
            SetCbDpt();
        }

        void SetDGV()
        {
            if (_pTypeReport == "2")
            {
                DtTel = ConnectionClass.SelectSQL_Main($@"
                SELECT	[PROGRAM_NAME],[PROGRAM_DESC],[PROGRAM_ICON],[PROGRAM_DEVICE],
		                PROGRAM_INSTALL,[PROGRAM_SERVERMAIN],ISNULL([PROGRAM_REMARK],'') AS [PROGRAM_REMARK],[PROGRAM_TEL]
		                ,ISNULL([PROGRAM_DEVELOPERID_1],'')+'-'+ISNULL([PROGRAM_DEVELOPERNAME_1],'')
		                +CHAR(10)+ISNULL([PROGRAM_DEVELOPERID_2],'')+'-'+ISNULL([PROGRAM_DEVELOPERNAME_2],'')
		                +CHAR(10)+ISNULL([PROGRAM_DEVELOPERID_3],'')+'-'+ISNULL([PROGRAM_DEVELOPERNAME_3],'') AS PRO
                FROM	[SHOP_PROGRAM_DEVELOPER] WITH (NOLOCK) 
                ORDER BY [PROGRAM_NAME] ");
                radGridView_Show.DataSource = DtTel;

                for (int i = 0; i < radGridView_Show.Rows.Count; i++)
                {
                    radGridView_Show.Rows[i].Cells["ICON"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(radGridView_Show.Rows[i].Cells["PROGRAM_ICON"].Value.ToString());
                }
            }
            if ((_pTypeReport == "0") || (_pTypeReport == "1"))
            {
                DtTel = ConnectionClass.SelectSQL_Main("  [SHOP_GetPhonebookAll]  ");
                radGridView_Show.DataSource = DtTel;
            }
        }

        //SetFontInRadGridview
        #region SetFontInRadGridview
        //Rows
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        #endregion

        //Depart
        private void RadDropDownList_Dpt_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                radTextBox_Head.Text = Class.Models.DptClass.FindDptHead(radDropDownList_Dpt.SelectedValue.ToString());
            }
            catch (Exception)
            {
                return;
            }

        }
        //cancle
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        void ClearData()
        {
            radLabel_Tel.Text = "เบอร์โทร.";
            radTextBox_Tel.Text = ""; radTextBox_Tel.Enabled = false;
            radTextBox_Desc.Text = ""; radTextBox_Desc.Enabled = false;
            radTextBox_Head.Text = ""; radDropDownList_Dpt.Enabled = false;
            radDropDownList_Dpt.SelectedIndex = 0;

            radButton_Save.Enabled = false;
            StatusSave = "";
        }
        void SetCbDpt()
        {
            DataTable dtDpt = Class.Models.DptClass.GetDpt_All();

            radDropDownList_Dpt.DataSource = dtDpt;
            radDropDownList_Dpt.ValueMember = "NUM";
            radDropDownList_Dpt.DisplayMember = "DESCRIPTION_SHOW";

        }
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            string sql = "";
            switch (StatusSave)
            {
                case "":
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"เกิดข้อผิดพลาดในการบันทึกข้อมูล ลองใหม่อีกครั้ง [ไม่มีการระบุสำหรับการจัดการ].");
                    ClearData();
                    break;
                case "0": //Inert
                    if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูลเบอร์โทร") == DialogResult.No) return;
                    sql = $@"INSERT INTO [dbo].[SHOP_PHONEBOOK] 
                            ([PHONEID],[PHONEDESC],[PHONEDPT],[WHOINS])
                            VALUES  ('{radTextBox_Tel.Text.Trim()}','{radTextBox_Desc.Text}','{radDropDownList_Dpt.SelectedValue}','{SystemClass.SystemUserID}') "
                            ;
                    break;
                case "1": //Update
                    if (MsgBoxClass.MsgBoxShow_ConfirmEdit("ข้อมูลเบอร์โทร") == DialogResult.No) return;
                    sql = $@"UPDATE [dbo].[SHOP_PHONEBOOK] 
                             SET    [PHONEDESC] = '{radTextBox_Desc.Text}',[PHONEDPT] = '{radDropDownList_Dpt.SelectedValue}',
                                    [WHOUPD] = '{SystemClass.SystemUserID}',
                                    DATEUPD = GETDATE() 
                             WHERE  [PHONEID] = '{radTextBox_Tel.Text.Trim()}' ";
                    break;
                case "2"://Delete
                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete("ข้อมูลเบอร์โทร") == DialogResult.No) return;
                    sql = $@"DELETE [dbo].[SHOP_PHONEBOOK]  WHERE [PHONEID] = '{radTextBox_Tel.Text.Trim()}' ";
                    break;
                default:
                    break;
            }
            string T = ConnectionClass.ExecuteSQL_Main(sql);

            MsgBoxClass.MsgBoxShow_SaveStatus(T);

            if (T == "")
            {
                SetDGV();
                ClearData();
            }

        }
        //ForInsert
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            radPanel_Show.Enabled = true;
            radTextBox_Tel.Enabled = true;
            radTextBox_Tel.Text = "";
            radTextBox_Tel.Focus();
            radLabel_Tel.Text = "เบอร์โทร [ENTER].";
            radTextBox_Desc.Enabled = false;
            radDropDownList_Dpt.Enabled = false;
            radButton_Save.Enabled = false;
            StatusSave = "0";
        }
        //For Update
        private void RadButtonElement_Edit_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.CurrentRow.Cells["PHONEID"].Value.ToString() == "") return;

            radPanel_Show.Enabled = true;
            radTextBox_Tel.Text = radGridView_Show.CurrentRow.Cells["PHONEID"].Value.ToString();
            radTextBox_Tel.Enabled = false;
            radTextBox_Desc.Focus();
            radTextBox_Desc.Enabled = true;
            radTextBox_Desc.Text = radGridView_Show.CurrentRow.Cells["PHONEDESC"].Value.ToString();
            radDropDownList_Dpt.SelectedValue = radGridView_Show.CurrentRow.Cells["PHONEDPT"].Value.ToString();
            radDropDownList_Dpt.Enabled = true;
            radTextBox_Head.Text = radGridView_Show.CurrentRow.Cells["SPC_NAME"].Value.ToString();
            StatusSave = "1";
            radTextBox_Desc.SelectionStart = radTextBox_Desc.Text.Length;
            radButton_Save.Enabled = true;
        }
        //For Delete
        private void RadButtonElement_delete_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.CurrentRow.Cells["PHONEID"].Value.ToString() == "") return;

            radTextBox_Tel.Text = radGridView_Show.CurrentRow.Cells["PHONEID"].Value.ToString();
            radTextBox_Tel.Enabled = false;
            radTextBox_Desc.Text = radGridView_Show.CurrentRow.Cells["PHONEDESC"].Value.ToString();
            radDropDownList_Dpt.SelectedValue = radGridView_Show.CurrentRow.Cells["PHONEDPT"].Value.ToString();
            radTextBox_Head.Text = radGridView_Show.CurrentRow.Cells["SPC_NAME"].Value.ToString();
            StatusSave = "2";
            radTextBox_Desc.SelectionStart = radTextBox_Desc.Text.Length;
            radButton_Save.Enabled = true;
        }
        //set enter Tel
        private void RadTextBox_Tel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DataRow[] dr = DtTel.Select("PHONEID ='" + radTextBox_Tel.Text.Trim() + "'");
                if (dr.Length == 0)
                {
                    radTextBox_Desc.Enabled = true;
                    radDropDownList_Dpt.Enabled = true;
                    radButton_Save.Enabled = true;
                    radTextBox_Desc.Focus();
                }
                else
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ข้อมูลเบอร์โทรที่ระบุมีอยู่แล้วในระบบ เช็คใหม่อีกครั้ง.");
                    radTextBox_Tel.SelectAll();
                    radTextBox_Tel.Focus();
                    return;
                }
            }
        }
        //open Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }

        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (radGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", radGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Double Click Rows
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            if ((_pTypeReport == "2") && (e.Column.Name == "PROGRAM_INSTALL"))
            {
                string name = radGridView_Show.CurrentRow.Cells["PROGRAM_DEVICE"].Value.ToString().ToUpper();
                if (name.Contains("PC"))
                    Process.Start("iexplore.exe", radGridView_Show.CurrentRow.Cells["PROGRAM_INSTALL"].Value.ToString());
                else if (name.Contains("WEB"))
                    Process.Start("iexplore.exe", radGridView_Show.CurrentRow.Cells["PROGRAM_INSTALL"].Value.ToString());
                else MsgBoxClass.MsgBoxShowButtonOk_Warning($@"โปรแกรมนี้ไม่สามารถใช้งานบน PC ได้{Environment.NewLine}เป็นโปรแกรมที่ต้องใช้เป็นมือถือเท่านั้น");
            }
        }
        //Refresh
        private void PhoneBook_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5) SetDGV();
        }
        //Refresh
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5) SetDGV();
        }
    }
}
