﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNPO
{
    public partial class TranferDelivary_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dtQtyAll;
        DataTable dtBchAll;
        DataTable dtTimeAll;
        DataTable dtBarcodeAll;
        DataTable dtQtySale;
        DataTable dtDiscount;

        //DataTable dtBch = new DataTable();
        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();


        readonly string _pType; // SHOP - SUPC
        readonly string _pTypeID;// รหัสกลุ่ม
        readonly string _pWH;//คลัง

        public TranferDelivary_Report(string pType, string pTypeID, string pWH)
        {
            InitializeComponent();
            _pType = pType;
            _pTypeID = pTypeID;
            _pWH = pWH;
        }
        //Load
        private void TranferDelivary_Report_Load(object sender, EventArgs e)
        {
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "ดึงข้อมูลใหม่";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export Excel";
            RadButton_Search.ButtonElement.ShowBorder = true; RadButton_Search.ButtonElement.ToolTipText = "ค้นหาข้อมูล";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            DataTable dtBch = new DataTable();
            //แยกระหว่างสาขาและ SUPC
            switch (_pType)
            {
                case "SHOP"://shop
                    RadCheckBox_Branch.Checked = true;
                    dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    RadCheckBox_Branch.Enabled = false;
                    break;
                case "SUPC": // supc
                    RadCheckBox_Branch.Checked = false;
                    dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                    RadCheckBox_Branch.Enabled = true;
                    break;
                default:
                    break;
            }

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
        }

        //Set HeadBch
        void SetGridView()
        {
            //  DataTable dtBchSend = Get_BranchQty();
            this.Cursor = Cursors.WaitCursor;

            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
            }

            string pColumn; string pColumnName;
            string pRow; string pRowName;
            string labelGrp1; string labelGrp2;


            DataTable dtColumn; DataTable dtRow;
            if (radRadioButton_Bch.CheckState == CheckState.Checked)
            {
                pColumn = "BRANCH_ID"; pColumnName = "BRANCH_NAME"; dtColumn = dtBchAll;
                pRow = "SHOW_ID"; pRowName = "SHOW_NAME"; dtRow = dtBarcodeAll;
                labelGrp1 = "บาร์โค้ด"; labelGrp2 = "ชื่อสินค้า";
            }
            else
            {
                pColumn = "SHOW_ID"; pColumnName = "SHOW_NAME"; dtColumn = dtBarcodeAll;
                pRow = "BRANCH_ID"; pRowName = "BRANCH_NAME"; dtRow = dtBchAll;
                labelGrp1 = "สาขา"; labelGrp2 = "ชื่อสาขา";
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(pRow, labelGrp1, 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(pRowName, labelGrp2, 170)));
            RadGridView_ShowHD.Columns[pRow].IsPinned = true; RadGridView_ShowHD.Columns[pRowName].IsPinned = true;

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMSend", "รวมส่ง", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMRecive", "รวมรับ", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMSale", "รวมขาย", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMDiscount", "รวมขายลด", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SUMStock", "รวมเหลือ", 110)));
            RadGridView_ShowHD.Columns["SUMSend"].IsPinned = true;
            RadGridView_ShowHD.Columns["SUMRecive"].IsPinned = true;
            RadGridView_ShowHD.Columns["SUMSale"].IsPinned = true;
            RadGridView_ShowHD.Columns["SUMDiscount"].IsPinned = true;
            RadGridView_ShowHD.Columns["SUMStock"].IsPinned = true;

            GridViewSummaryItem summaryItemSUM1 = new GridViewSummaryItem("SUMSend", "ส่ง = {0:#,##0.00}", GridAggregateFunction.Sum);
            summaryRowItem.Add(summaryItemSUM1);

            GridViewSummaryItem summaryItemSUM2 = new GridViewSummaryItem("SUMRecive", "รับ = {0:#,##0.00}", GridAggregateFunction.Sum);
            summaryRowItem.Add(summaryItemSUM2);

            GridViewSummaryItem summaryItemSUM3 = new GridViewSummaryItem("SUMSale", "ขาย = {0:#,##0.00}", GridAggregateFunction.Sum);
            summaryRowItem.Add(summaryItemSUM3);

            GridViewSummaryItem summaryItemSUM5 = new GridViewSummaryItem("SUMDiscount", "ลด = {0:#,##0.00}", GridAggregateFunction.Sum);
            summaryRowItem.Add(summaryItemSUM5);

            GridViewSummaryItem summaryItemSUM4 = new GridViewSummaryItem("SUMStock", "เหลือ = {0:#,##0.00}", GridAggregateFunction.Sum);
            summaryRowItem.Add(summaryItemSUM4);

            ConditionalFormattingObject obj1 = new ConditionalFormattingObject("MyCondition", ConditionTypes.Equal, "ยังไม่รับสินค้า", "", false)
            { CellBackColor = ConfigClass.SetColor_Red() };

            ConditionalFormattingObject obj2 = new ConditionalFormattingObject("MyCondition2", ConditionTypes.Greater, "0.00", "", false)
            { CellBackColor = ConfigClass.SetColor_GreenPastel() };

            ConditionalFormattingObject obj3 = new ConditionalFormattingObject("MyCondition3", ConditionTypes.Less, "0.00", "", false)
            { CellBackColor = ConfigClass.SetColor_PurplePastel() };

            ConditionalFormattingObject obj4 = new ConditionalFormattingObject("MyCondition4", ConditionTypes.Greater, "0.00", "", false)
            { CellBackColor = ConfigClass.SetColor_YellowPastel() };



            foreach (DataRow row in dtColumn.Rows)//ใส่ 4 ช่อง / สาขา / บาร์โค้ด [ส่ง - รับ - diff - เวลารับ] + 2 ช่องเพิ่ม ยอดขาย + สต็อก
            {
                string headText = row[pColumn].ToString() + System.Environment.NewLine + row[pColumnName].ToString();

                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                    (DatagridClass.AddNumberColumn_AddManualSetCenter(row[pColumn].ToString() + "_S", headText + System.Environment.NewLine + "  [ส่ง]  ", 200)));
                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_S"].FormatString = "{0:#,##0.00}";

                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                    (DatagridClass.AddNumberColumn_AddManualSetCenter(row[pColumn].ToString() + "_R", headText + System.Environment.NewLine + "  [รับ]  ", 200)));
                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_R"].FormatString = "{0:#,##0.00}";

                //RadGridView_ShowHD.MasterTemplate.Columns.Add(
                //    (DatagridClass.AddTextBoxColumn_AddManualSetCenter(row[pColumn].ToString() + "_D", headText + System.Environment.NewLine + " [ขาด-เกิน] ", 200)));
                //RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_D"].FormatString = "{0:#,##0.00}";
                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                 (DatagridClass.AddNumberColumn_AddManualSetCenter(row[pColumn].ToString() + "_D", headText + System.Environment.NewLine + " [ขาด-เกิน] ", 200)));
                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_D"].FormatString = "{0:#,##0.00}";


                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                    (DatagridClass.AddTextBoxColumn_AddManualSetCenter(row[pColumn].ToString() + "_T", headText + System.Environment.NewLine + " [เวลารับ] ", 200)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                    (DatagridClass.AddNumberColumn_AddManualSetCenter(row[pColumn].ToString() + "_SALE", headText + System.Environment.NewLine + "  [ขาย]  ", 200)));
                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_SALE"].FormatString = "{0:#,##0.00}";


                RadGridView_ShowHD.MasterTemplate.Columns.Add(
                    (DatagridClass.AddNumberColumn_AddManualSetCenter(row[pColumn].ToString() + "_STOCK", headText + System.Environment.NewLine + "  [เหลือ]  ", 200)));
                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_STOCK"].FormatString = "{0:#,##0.00}";

                GridViewSummaryItem summaryItem = new GridViewSummaryItem(row[pColumn].ToString() + "_S", "รวม = {0}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItem);

                GridViewSummaryItem summaryItem1 = new GridViewSummaryItem(row[pColumn].ToString() + "_R", "รวม = {0}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItem1);

                GridViewSummaryItem summaryItem2 = new GridViewSummaryItem(row[pColumn].ToString() + "_STOCK", "รวมเหลือ = {0}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItem2);

                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_T"].ConditionalFormattingObjectList.Add(obj1);

                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_D"].ConditionalFormattingObjectList.Add(obj2);
                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_D"].ConditionalFormattingObjectList.Add(obj3);

                RadGridView_ShowHD.Columns[row[pColumn].ToString() + "_STOCK"].ConditionalFormattingObjectList.Add(obj4);


            }

            RadGridView_ShowHD.TableElement.TableHeaderHeight = 65;


            for (int iR = 0; iR < dtRow.Rows.Count; iR++)
            {
                int iiC = 6;
                RadGridView_ShowHD.Rows.Add(dtRow.Rows[iR][pRow], dtRow.Rows[iR][pRowName]);
                double sumRecive = 0, sumStock = 0, sumSend = 0, sumSale = 0;
                for (int iC = 0; iC < dtColumn.Rows.Count; iC++)
                {
                    double values_S = 0; double values_R = 0; double values_D; string values_T;//= "ยังไม่รับสินค้า";
                    double values_Sale = 0;
                    DataRow[] drS = dtQtyAll.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + "'" +
                        " AND  " + pColumn + @" = '" + dtColumn.Rows[iC][pColumn].ToString() + @"' AND TYPE = 'S' ");
                    DataRow[] drR = dtQtyAll.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + "'" +
                        " AND  " + pColumn + @" = '" + dtColumn.Rows[iC][pColumn].ToString() + @"' AND TYPE = 'R' ");

                    DataRow[] drT = dtTimeAll.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + "'" +
                      " AND  " + pColumn + @" = '" + dtColumn.Rows[iC][pColumn].ToString() + @"'  ");

                    DataRow[] drSale = dtQtySale.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + @"'" +
                        " AND  " + pColumn + @" = '" + dtColumn.Rows[iC][pColumn].ToString() + @"' ");

                    if (drS.Length > 0)
                    {
                        values_S = Convert.ToDouble(drS[0]["QTY"].ToString());
                    }
                    if (drR.Length > 0)
                    {
                        values_R = Convert.ToDouble(drR[0]["QTY"].ToString());
                    }
                    values_D = values_S - values_R;
                    if (drT.Length > 0)
                    {
                        values_T = drT[0]["MAXTIME_RECIVE"].ToString();
                    }
                    else
                    {
                        if (values_S > 0) values_T = "ยังไม่รับสินค้า"; else values_T = "-";
                    }

                    if (drSale.Length > 0) values_Sale = Convert.ToDouble(drSale[0]["QTYALLSALE"].ToString());
                    
                    double _stock = values_R - values_Sale;

                    sumRecive += values_R;
                    sumStock += _stock;
                    sumSend += values_S;
                    sumSale += values_Sale;

                    RadGridView_ShowHD.Rows[iR].Cells[iiC += 1].Value = Convert.ToDouble(values_S).ToString("N2");
                    RadGridView_ShowHD.Rows[iR].Cells[iiC += 1].Value = Convert.ToDouble(values_R).ToString("N2");
                    RadGridView_ShowHD.Rows[iR].Cells[iiC += 1].Value = Convert.ToDouble(values_D).ToString("N2");
                    RadGridView_ShowHD.Rows[iR].Cells[iiC += 1].Value = values_T;
                    RadGridView_ShowHD.Rows[iR].Cells[iiC += 1].Value = Convert.ToDouble(values_Sale).ToString("N2");
                    RadGridView_ShowHD.Rows[iR].Cells[iiC += 1].Value = Convert.ToDouble(_stock).ToString("N2");
                }

                double values_Discount = 0;
                if (pRow == "BRANCH_ID")
                {
                    if (dtDiscount.Rows.Count > 0)
                    {
                        //BRANCH_ID
                        DataRow[] drDiscount = dtDiscount.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + @"'");
                        if (drDiscount.Length > 0)
                        {
                            values_Discount = Convert.ToDouble(drDiscount[0]["QTYALLSALE"].ToString());
                            sumStock -= values_Discount;
                        }
                    }
                }


                RadGridView_ShowHD.Rows[iR].Cells["SUMSend"].Value = Convert.ToDouble(sumSend).ToString("N2");
                RadGridView_ShowHD.Rows[iR].Cells["SUMRecive"].Value = Convert.ToDouble(sumRecive).ToString("N2");
                RadGridView_ShowHD.Rows[iR].Cells["SUMSale"].Value = Convert.ToDouble(sumSale).ToString("N2");
                RadGridView_ShowHD.Rows[iR].Cells["SUMDiscount"].Value = Convert.ToDouble(values_Discount).ToString("N2");
                RadGridView_ShowHD.Rows[iR].Cells["SUMStock"].Value = Convert.ToDouble(sumStock).ToString("N2");
            }

            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;



            RadCheckBox_Branch.Enabled = false; RadDropDownList_Branch.Enabled = false;
            radRadioButton_Bch.Enabled = false; radRadioButton_Item.Enabled = false;
            radDateTimePicker_Begin.Enabled = false; RadButton_Search.Enabled = false;

            this.Cursor = Cursors.Default;
        }
        //get All Branch Sen Recive
        void GetAllBranchSendRecive()
        {
            //string strBch = string.Format(@" SELECT	TMP.BRANCH_ID,BRANCH_NAME FROM ( ");
            //string strQty = string.Format(@"
            //        SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,SPC_ITEMBARCODE AS SHOW_ID,SUM(SPC_QTY) AS QTY,'R' AS TYPE

            //        FROM	SHOP2013TMP.dbo.INVENTTRANSFERTABLE WITH (NOLOCK) 
            //          INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERLINE WITH (NOLOCK) ON INVENTTRANSFERTABLE.Transferid = INVENTTRANSFERLINE.Transferid 

            //        WHERE	INVENTTRANSFERTABLE.SHIPDATE = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' 
            //          AND INVENTTRANSFERTABLE.DATAAREAID = N'SPC' AND INVENTTRANSFERLINE.DATAAREAID = N'SPC'	 
            //          AND INVENTLOCATIONIDTO LIKE '%'  
            //          AND INVENTTRANSFERTABLE.Transferid LIKE 'MNPI%'
            //          AND INVENTLOCATIONIDFROM = '" + _pWH + @"'

            //        GROUP BY INVENTLOCATIONIDTO, CONVERT(VARCHAR,DATEADD(HOUR,7,INVENTTRANSFERTABLE.CREATEDDATETIME),23),SPC_ITEMBARCODE

            //        UNION

            //        SELECT	BRANCH_ID,SHOW_ID AS SPC_ITEMBARCODE,QTY AS QTY,'S' AS TYPE
            //        FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK)
            //        WHERE	TYPE_CONFIG = '" + _pTypeID + @"'  AND QTY > 0
            //                AND CONVERT(VARCHAR,DATE_SEND,23) =  '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' ");

            //strBch += strQty + " )TMP INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON TMP.BRANCH_ID = SHOP_BRANCH.BRANCH_ID  ";
            //if (RadCheckBox_Branch.CheckState == CheckState.Checked)
            //{
            //    strBch += " WHERE	TMP.BRANCH_ID = '" + RadDropDownList_Branch.SelectedValue.ToString() + "' ";
            //}
            //strBch += " GROUP BY TMP.BRANCH_ID,BRANCH_NAME ORDER BY BRANCH_ID ,BRANCH_NAME ";

            ////ค้นหาเวลารับล่าสุด
            //string strTime = string.Format(@"
            //    SELECT	INVENTLOCATIONIDTO AS BRANCH_ID,SPC_ITEMBARCODE AS SHOW_ID,
            //            MAX(CONVERT(VARCHAR,DATEADD(HOUR,7,INVENTTRANSFERTABLE.CREATEDDATETIME),24)) AS MAXTIME_RECIVE

            //    FROM	SHOP2013TMP.dbo.INVENTTRANSFERTABLE WITH (NOLOCK) 
            //      INNER JOIN SHOP2013TMP.dbo.INVENTTRANSFERLINE WITH (NOLOCK) ON INVENTTRANSFERTABLE.Transferid = INVENTTRANSFERLINE.Transferid 

            //    WHERE	INVENTTRANSFERTABLE.SHIPDATE = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' 
            //      AND INVENTTRANSFERTABLE.DATAAREAID = N'SPC' AND INVENTTRANSFERLINE.DATAAREAID = N'SPC'
            //      AND INVENTLOCATIONIDTO LIKE 'MN%'  
            //      AND INVENTTRANSFERTABLE.Transferid LIKE 'MNPI%'
            //      AND INVENTLOCATIONIDFROM = '" + _pWH + @"'

            //    GROUP BY INVENTLOCATIONIDTO,SPC_ITEMBARCODE 
            //");

            //string strBarcode = string.Format(@" 
            //                        select SHOW_ID,SHOW_NAME, SHOW_DESC  
            //                        from  SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
            //                        where TYPE_CONFIG = '" + _pTypeID + @"' AND STA = '1' 
            //                        order by SHOW_ID ");



            //string strSale = string.Format(@"
            //        SELECT	ITEMBARCODE AS SHOW_ID,POSGROUP AS BRANCH_ID,SUM(QTY) AS 	QTYALLSALE
            //        FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK)
            //        WHERE	ITEMBARCODE IN (
            //        SELECT SHOW_ID
            //        FROM  SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
            //        WHERE TYPE_CONFIG = '" + _pTypeID + @"' AND STA = '1' 
            //        )
            //          AND INVOICEDATE = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' AND POSGROUP LIKE 'MN%' AND SIGN = '1'         
            //        GROUP BY    ITEMBARCODE, POSGROUP
            //");

            string barcodeDiscount = "''";
            switch (_pTypeID)
            {
                case "17":
                    barcodeDiscount = SystemClass.SystemBarcodeType17;
                    break;
                case "19":
                    barcodeDiscount = SystemClass.SystemBarcodeType19;
                    break;
                case "20":
                    barcodeDiscount = SystemClass.SystemBarcodeType20;
                    break;
                case "32":
                    barcodeDiscount = SystemClass.SystemBarcodeType32;
                    break;
                default:
                    break;
            }


            //string strDiscount = string.Format(@"
            //        SELECT	POSGROUP AS BRANCH_ID,SUM(QTY) AS 	QTYALLSALE
            //        FROM	SHOP2013TMP.dbo.XXX_POSLINE WITH (NOLOCK)
            //        WHERE	ITEMBARCODE IN ( 
            //            " + barcodeDiscount + @"
            //        )
            //          AND INVOICEDATE = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' AND POSGROUP LIKE 'MN%' AND SIGN = '1'         
            //        GROUP BY     POSGROUP
            //");

            string bchID = "";
            if (RadCheckBox_Branch.CheckState == CheckState.Checked) bchID = RadDropDownList_Branch.SelectedValue.ToString();

            dtBchAll = ConnectionClass.SelectSQL_Main(POClass.TranferDelivery_ItemQtyByBch(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), _pWH, _pTypeID, bchID)); //ConnectionClass.SelectSQL_Main(strBch);
            dtQtyAll = ConnectionClass.SelectSQL_Main(POClass.TranferDelivery_ItemQty(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), _pWH, _pTypeID)); //ConnectionClass.SelectSQL_Main(strQty);
            dtTimeAll = POClass.TranferDelivery_ItemLastRecive(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), _pWH); //ConnectionClass.SelectSQL_Main(strTime);
            dtBarcodeAll = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID(_pTypeID, "", " ORDER BY SHOW_ID ", "1"); //ConnectionClass.SelectSQL_Main(strBarcode);
            dtQtySale = PosSaleClass.TranferDelivery_ItemLastRecive("0", _pTypeID, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd")); //ConnectionClass.SelectSQL_Main(strSale);
            dtDiscount = PosSaleClass.TranferDelivery_ItemLastRecive("0", barcodeDiscount, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(strDiscount);
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            GetAllBranchSendRecive();

            if (dtBchAll.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลการรับ-ส่งข้าวกล่อง " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"));
                ClearData();
                return;
            }

            SetGridView();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }

        //ClearDate
        void ClearData()
        {
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
            }
            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(1);

            RadCheckBox_Branch.Enabled = true; RadDropDownList_Branch.Enabled = true;
            radRadioButton_Bch.Enabled = true; radRadioButton_Item.Enabled = true;
            radDateTimePicker_Begin.Enabled = true; RadButton_Search.Enabled = true;
        }
        //เพิ่ม ข้อมูลใหม่
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("ข้อมูลการส่ง-รับข้าวกล่อง " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}
