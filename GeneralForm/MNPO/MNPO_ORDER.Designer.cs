﻿namespace PC_Shop24Hrs.GeneralForm.MNPO
{ 
    public partial class MNPO_Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MNPO_Order));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.RadButton_Apv = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radCheckBox_PO = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_Sale = new Telerik.WinControls.UI.RadCheckBox();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radStatusStrip1 = new Telerik.WinControls.UI.RadStatusStrip();
            this.radButtonElement_Add = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator1 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_Find = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator3 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.radButtonElement_excel = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator2 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadButtonElement_pdt = new Telerik.WinControls.UI.RadButtonElement();
            this.commandBarSeparator4 = new Telerik.WinControls.UI.CommandBarSeparator();
            this.RadDropDownList_Branch = new Telerik.WinControls.UI.RadDropDownList();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_StatusBill = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.RadGridView_Show = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Apv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_PO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Sale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_StatusBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(8, 107);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(39, 19);
            this.radLabel1.TabIndex = 17;
            this.radLabel1.Text = "สาขา";
            // 
            // RadButton_Apv
            // 
            this.RadButton_Apv.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Apv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(76)))), ((int)(((byte)(169)))));
            this.RadButton_Apv.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Apv.Location = new System.Drawing.Point(8, 549);
            this.RadButton_Apv.Name = "RadButton_Apv";
            this.RadButton_Apv.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Apv.TabIndex = 2;
            this.RadButton_Apv.Text = "ยืนยัน";
            this.RadButton_Apv.ThemeName = "Fluent";
            this.RadButton_Apv.Click += new System.EventHandler(this.RadButton_Apv_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).Text = "ยืนยัน";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Apv.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Apv.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // RadButton_Cancel
            // 
            this.RadButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RadButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.RadButton_Cancel.Location = new System.Drawing.Point(8, 587);
            this.RadButton_Cancel.Name = "RadButton_Cancel";
            this.RadButton_Cancel.Size = new System.Drawing.Size(175, 32);
            this.RadButton_Cancel.TabIndex = 3;
            this.RadButton_Cancel.Text = "ยกเลิก";
            this.RadButton_Cancel.ThemeName = "Fluent";
            this.RadButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.RadButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.radCheckBox_PO);
            this.panel1.Controls.Add(this.radCheckBox_Sale);
            this.panel1.Controls.Add(this.radTextBox_Barcode);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radStatusStrip1);
            this.panel1.Controls.Add(this.RadDropDownList_Branch);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.RadButton_Apv);
            this.panel1.Controls.Add(this.RadButton_Cancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.panel1.Location = new System.Drawing.Point(632, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(194, 628);
            this.panel1.TabIndex = 5;
            // 
            // radCheckBox_PO
            // 
            this.radCheckBox_PO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox_PO.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_PO.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_PO.Location = new System.Drawing.Point(8, 78);
            this.radCheckBox_PO.Name = "radCheckBox_PO";
            this.radCheckBox_PO.ReadOnly = true;
            this.radCheckBox_PO.Size = new System.Drawing.Size(160, 19);
            this.radCheckBox_PO.TabIndex = 34;
            this.radCheckBox_PO.Text = "ประวัติสั่งย้อนหลัง 6 วัน";
            this.radCheckBox_PO.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBox_Sale
            // 
            this.radCheckBox_Sale.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_Sale.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_Sale.Location = new System.Drawing.Point(8, 51);
            this.radCheckBox_Sale.Name = "radCheckBox_Sale";
            this.radCheckBox_Sale.Size = new System.Drawing.Size(178, 19);
            this.radCheckBox_Sale.TabIndex = 33;
            this.radCheckBox_Sale.Text = "ประวัติขายย้อนหลัง 30 วัน";
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_Barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Barcode.Location = new System.Drawing.Point(8, 190);
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(174, 25);
            this.radTextBox_Barcode.TabIndex = 0;
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(8, 165);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(162, 19);
            this.radLabel2.TabIndex = 18;
            this.radLabel2.Text = "บาร์โค้ด [Enter] ชื่อ [F4]";
            // 
            // radStatusStrip1
            // 
            this.radStatusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radStatusStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radStatusStrip1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radButtonElement_Add,
            this.commandBarSeparator1,
            this.RadButtonElement_Find,
            this.commandBarSeparator3,
            this.radButtonElement_excel,
            this.commandBarSeparator2,
            this.RadButtonElement_pdt,
            this.commandBarSeparator4});
            this.radStatusStrip1.Location = new System.Drawing.Point(0, 0);
            this.radStatusStrip1.Name = "radStatusStrip1";
            this.radStatusStrip1.Size = new System.Drawing.Size(192, 34);
            this.radStatusStrip1.TabIndex = 7;
            // 
            // radButtonElement_Add
            // 
            this.radButtonElement_Add.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_Add.Image = global::PC_Shop24Hrs.Properties.Resources.add;
            this.radButtonElement_Add.Name = "radButtonElement_Add";
            this.radStatusStrip1.SetSpring(this.radButtonElement_Add, false);
            this.radButtonElement_Add.Text = "radButtonElement1";
            this.radButtonElement_Add.ToolTipText = "เพิ่มเอกสาร";
            this.radButtonElement_Add.Click += new System.EventHandler(this.RadButtonElement_Add_Click);
            // 
            // commandBarSeparator1
            // 
            this.commandBarSeparator1.Name = "commandBarSeparator1";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator1, false);
            this.commandBarSeparator1.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_Find
            // 
            this.RadButtonElement_Find.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_Find.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButtonElement_Find.Name = "RadButtonElement_Find";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_Find, false);
            this.RadButtonElement_Find.Text = "radButtonElement1";
            this.RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            this.RadButtonElement_Find.Click += new System.EventHandler(this.RadButtonElement_Find_Click);
            // 
            // commandBarSeparator3
            // 
            this.commandBarSeparator3.Name = "commandBarSeparator3";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator3, false);
            this.commandBarSeparator3.VisibleInOverflowMenu = false;
            // 
            // radButtonElement_excel
            // 
            this.radButtonElement_excel.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButtonElement_excel.Image = global::PC_Shop24Hrs.Properties.Resources.excel;
            this.radButtonElement_excel.Name = "radButtonElement_excel";
            this.radStatusStrip1.SetSpring(this.radButtonElement_excel, false);
            this.radButtonElement_excel.Text = "radButtonElement1";
            this.radButtonElement_excel.ToolTipText = "Export To Excel";
            this.radButtonElement_excel.Click += new System.EventHandler(this.RadButtonElement_excel_Click);
            // 
            // commandBarSeparator2
            // 
            this.commandBarSeparator2.Name = "commandBarSeparator2";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator2, false);
            this.commandBarSeparator2.VisibleInOverflowMenu = false;
            // 
            // RadButtonElement_pdt
            // 
            this.RadButtonElement_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButtonElement_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.RadButtonElement_pdt.Name = "RadButtonElement_pdt";
            this.radStatusStrip1.SetSpring(this.RadButtonElement_pdt, false);
            this.RadButtonElement_pdt.Text = "radButtonElement1";
            this.RadButtonElement_pdt.Click += new System.EventHandler(this.RadButtonElement_pdt_Click);
            // 
            // commandBarSeparator4
            // 
            this.commandBarSeparator4.Name = "commandBarSeparator4";
            this.radStatusStrip1.SetSpring(this.commandBarSeparator4, false);
            this.commandBarSeparator4.VisibleInOverflowMenu = false;
            // 
            // RadDropDownList_Branch
            // 
            this.RadDropDownList_Branch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.RadDropDownList_Branch.BackColor = System.Drawing.Color.White;
            this.RadDropDownList_Branch.DropDownAnimationEnabled = false;
            this.RadDropDownList_Branch.DropDownHeight = 124;
            this.RadDropDownList_Branch.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Branch.Font = new System.Drawing.Font("Tahoma", 12F);
            this.RadDropDownList_Branch.Location = new System.Drawing.Point(7, 131);
            this.RadDropDownList_Branch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RadDropDownList_Branch.Name = "RadDropDownList_Branch";
            // 
            // 
            // 
            this.RadDropDownList_Branch.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.RadDropDownList_Branch.Size = new System.Drawing.Size(175, 25);
            this.RadDropDownList_Branch.TabIndex = 6;
            this.RadDropDownList_Branch.Text = "สาขา";
            this.RadDropDownList_Branch.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Branch_SelectedValueChanged);
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.RadDropDownList_Branch.GetChildAt(0))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.RadDropDownList_Branch.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(1))).BackColor = System.Drawing.Color.White;
            ((Telerik.WinControls.UI.StackLayoutElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.StackLayoutElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListEditableAreaElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Text = "สาขา";
            ((Telerik.WinControls.UI.RadDropDownTextBoxElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Hidden;
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadTextBoxItem)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(0).GetChildAt(0).GetChildAt(0))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RippleAnimationColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).FocusBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).Font = new System.Drawing.Font("Tahoma", 12F);
            ((Telerik.WinControls.UI.RadDropDownListArrowButtonElement)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1))).RightToLeft = true;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(0))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).LineLimit = false;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(5)))), ((int)(((byte)(4)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).Font = new System.Drawing.Font("Tahoma", 15.75F);
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFont = "None";
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontSize = 14F;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).CustomFontStyle = System.Drawing.FontStyle.Regular;
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.RadDropDownList_Branch.GetChildAt(0).GetChildAt(2).GetChildAt(1).GetChildAt(5))).AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(829, 634);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.radPanel2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.RadGridView_Show, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(623, 628);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.Transparent;
            this.radPanel2.Controls.Add(this.radLabel_F2);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(3, 606);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(617, 19);
            this.radPanel2.TabIndex = 2;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(0, 0);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(617, 19);
            this.radLabel_F2.TabIndex = 51;
            this.radLabel_F2.Text = "<html>Double Click ช่องจำนวน-แก้ไขจำนวนสั่ง | เลือกรายการกด F3 ยกเลิกรายการทั้งหม" +
    "ด</html>";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_StatusBill, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_Docno, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(617, 24);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // radLabel_StatusBill
            // 
            this.radLabel_StatusBill.AutoSize = false;
            this.radLabel_StatusBill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_StatusBill.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_StatusBill.Location = new System.Drawing.Point(205, 4);
            this.radLabel_StatusBill.Name = "radLabel_StatusBill";
            this.radLabel_StatusBill.Size = new System.Drawing.Size(408, 16);
            this.radLabel_StatusBill.TabIndex = 23;
            this.radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก";
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(4, 4);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(194, 16);
            this.radLabel_Docno.TabIndex = 21;
            this.radLabel_Docno.Text = "MNPO200401000000";
            // 
            // RadGridView_Show
            // 
            this.RadGridView_Show.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_Show.Location = new System.Drawing.Point(3, 33);
            // 
            // 
            // 
            this.RadGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_Show.Name = "RadGridView_Show";
            // 
            // 
            // 
            this.RadGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_Show.Size = new System.Drawing.Size(617, 567);
            this.RadGridView_Show.TabIndex = 4;
            this.RadGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting_1);
            this.RadGridView_Show.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_Show_CellDoubleClick);
            this.RadGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // MNPO_Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 634);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MNPO_Order";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "สั่งสินค้า";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MNPO_Order_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Apv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Cancel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_PO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Sale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radStatusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Branch)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_StatusBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadLabel radLabel1;
        protected Telerik.WinControls.UI.RadButton RadButton_Apv;
        protected Telerik.WinControls.UI.RadButton RadButton_Cancel;
        protected System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadStatusStrip radStatusStrip1;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_Add;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator1;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Branch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_Find;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator3;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadLabel radLabel_StatusBill;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadButtonElement radButtonElement_excel;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadGridView RadGridView_Show;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_PO;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Sale;
        private Telerik.WinControls.UI.RadButtonElement RadButtonElement_pdt;
        private Telerik.WinControls.UI.CommandBarSeparator commandBarSeparator4;
    }
}
 