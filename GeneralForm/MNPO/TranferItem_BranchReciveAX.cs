﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Drawing.Printing;
using Newtonsoft.Json;
using PrintBarcode;
using PC_Shop24Hrs.FormShare;

namespace PC_Shop24Hrs.GeneralForm.MNPO
{
    public partial class TranferItem_BranchReciveAX : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        DataTable dtRoute = new DataTable();
        readonly string _pTypeOpen; // shop - supc
        readonly string _pBillID; // ไว้สำหรับการหา บิลที่ลงสาขาเองด้วย เช่น บิลน้ำแข็ง / รัยข้าวกล่อง
        readonly string _pWH; //สำหรับคลังที่ต้องการเช็ค
        readonly string _pTypeID;
        readonly string _pTypeReport;


        public string _sItemID;
        public string _sInventDIM;
        public string _sBchID;
        public string _sDptID;

        //0 = MNPO  check order ที่เปิดผ่านระบบทั้งหมด
        //1 = WEB   check Order ที่เปิดผ่าน Web ทั้งหมด //ค้นหาจำนวนรายการสั่งผ่าน web เฉพาะที่สั่งผ่านบาร์โค้ดเท่านั้น
        //2 = สถานะออร์เดอร์รวม [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]
        //3 = สถานะออร์เดอร์ตามแผนก [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]
        //4 = จำนวนออเดอร์ตามมินิมาร์ท [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]
        //5 = จัดสินค้าผ่านรูป + Barcode
        //6 = สร้างบิลการจัดสินค้าไปโปรแกรมคอนเทรนเนอร์โดยไม่อ้างอิงรายการ
        //7 = รายการจัดสินค้าที่จัดบิลโดยแผนกจัดซื้อ ตามพนักงานจัดบิล
        //8 =  รายการสินค้าที่จัดทั้งหมด (ได้หรือไม่ได้)
        //9 = รายงานประเมินการจัดบิล ตามพนักงานจัดบิล [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]
        //10 = รายงานประเมินการจัดบิล ตามพนักงานจัดบิล %รายเดือน [สินค้าที่จัดบิลโดยแผนกจัดซื้อ]
        //11 = รายงานการจัดออเดอร์ของมินิมาร์ท
        //12 = รายงานการใช้งานเครื่องโปรแกรม Comtainer รายเดือน
        //13 = รายงานรายเดือนตามประเภทการสั่ง ตามบุคคล รายเดือน / รายวัน
        //14 = รายงานรอทำบิลจัดสินค้าในระบบ Container
        //15 = รายงานการใข้งานเครื่อง PDA/CPW/TAB ของโปรแกรมที่ใช้ Login บน Android
        //16 = รายงานการจัดสินค้าสาขาใหญ่

        string bchID, bchName, whPrint, barcodePrint, routeBch, emplIDPacking, emplNamePacking, staContainer;
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        DataTable dt_DimensionPackIng = new DataTable();
        public TranferItem_BranchReciveAX(string pTypeReport, string pTypeOpen, string pBillID, string pWH, string pTypeID)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pTypeOpen = pTypeOpen;
            _pBillID = pBillID;
            _pWH = pWH;
            _pTypeID = pTypeID;
        }
        //Load
        private void TranferItem_BranchReciveAX_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;//1
            barcode.BarHeight = 60;//38
            barcode.LeftMargin = 0;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;
            radStatusStrip1.SizingGrip = false;

            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radImage_EMPL.ShowBorder = true; radImage_EMPL.ToolTipText = "จ่ายบิล"; radImage_EMPL.Enabled = false;
            radButtonElement_PrintContainer.ShowBorder = true; radButtonElement_PrintContainer.ToolTipText = "Print container"; radButtonElement_PrintContainer.Enabled = false;
            RadCheckBox_Branch.Checked = false; radCheckBox_Dpt.Checked = false;
            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral; radCheckBox_Dpt.ButtonElement.Font = SystemClass.SetFontGernaral;
            RadDropDownList_Branch.Enabled = false; radDropDownList_Dpt.Enabled = false;
            radButtonElement_Excel.ShowBorder = true; radButtonElement_Excel.ToolTipText = "ExportExcel";
            radRadioButton_Photo.Visible = false; radRadioButton_Barcode.Visible = false;
            radCheck_NotTask.Visible = false;//radCheck_TaskOp.Visible = false; 
            radCheck_NotTask.ButtonElement.Font = SystemClass.SetFontGernaral;
            //radCheck_TaskOp.ButtonElement.Font = SystemClass.SetFontGernaral;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Dpt);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(-1), DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_End, DateTime.Now, DateTime.Now);
            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-1);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            switch (_pTypeReport)
            {
                case "0":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Transferid", "เลขที่บิล", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDFROM", "คลังเบิก", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DateTimeCreate", "วันที่เบิก", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("SPC_QTY", "จำนวนเบิก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODEUNIT", "หน่วยเบิก", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("RemainStatus", "สถานะ")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RemainStatus_DESC", "สถานะการจัด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_REMARKS", "หมายเหตุ", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("InventTransId", "เลขที่อ้างอิง", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "จัดซื้อ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM_DESC", "แผนกจัดซื้อ", 150)));

                    RadGridView_ShowHD.Columns["SPC_QTY"].FormatString = "{0:#,##0.00}";
                    RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;
                    RadGridView_ShowHD.Columns["Transferid"].IsPinned = true;


                    DatagridClass.SetCellBackClolorByExpression("RemainStatus_DESC", "RemainStatus = '0' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("RemainStatus_DESC", "RemainStatus = '1' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("RemainStatus_DESC", "RemainStatus = '2' ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);

                    radLabel_Detail.Text = "สีเขียว >> สร้างแล้ว | สีม่วง >> จัดส่งสินค้า | สีฟ้า >> ได้รับแล้ว";
                    break;
                case "1":
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "สาขา", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("i_ROWS", "จำนวนรายการ", 200)));

                    DatagridClass.SetCellBackClolorByExpression("i_ROWS", "i_ROWS = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    radDropDownList_Dpt.Visible = false;
                    radCheckBox_Dpt.Visible = false;

                    radLabel_Detail.Text = "สีแดง >> ไม่มีจำนวนการสั่ง";
                    break;
                case "2":
                    RadCheckBox_Branch.Visible = false; radCheckBox_Dpt.Visible = false;
                    RadDropDownList_Branch.Visible = false; radDropDownList_Dpt.Visible = false;
                    radLabel1.Text = "วันที่";
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMALL", $@"จำนวน{Environment.NewLine}รายการ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM1", $@"รอจ่ายบิล{Environment.NewLine}1 รอดำเนินการ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM1", $@"% รอจ่ายบิล{Environment.NewLine}1 รอดำเนินการ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM11", $@"กำลังจัดบิล{Environment.NewLine}11 รอจัดบิล", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM11", $@"% กำลังจัดบิล{Environment.NewLine}11 รอจัดบิล", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM12", $@"จัดได้แล้ว{Environment.NewLine}12 จัดแล้วยังไม่ส่งข้อมูล", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM12", $@"% จัดได้แล้ว{Environment.NewLine}12 จัดแล้วยังไม่ส่งข้อมูล", 150)));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM13", $@"ส่งเข้า AX แล้ว{Environment.NewLine}13 ส่งเข้า AX แล้ว", 150)));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM13", $@"% ส่งเข้า AX แล้ว{Environment.NewLine}13 ส่งเข้า AX แล้ว", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM13OK", $@"ส่งเข้า AX แล้ว{Environment.NewLine} จัดได้", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM13OK", $@"% ส่งเข้า AX แล้ว{Environment.NewLine} จัดได้", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM13Not", $@"ส่งเข้า AX แล้ว{Environment.NewLine} จัดไม่ได้", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM13Not", $@"% ส่งเข้า AX แล้ว{Environment.NewLine} จัดไม่ได้", 150)));

                    RadGridView_ShowHD.Columns["TRANSDATE"].IsPinned = true;
                    RadGridView_ShowHD.Columns["SUMALL"].IsPinned = true;

                    radLabel_Detail.Text = "สีม่วง >> ผลรวม | สีเหลือง >> จำนวนบิล | สีแดง >> ต่ำกว่า 70%";
                    RadGridView_ShowHD.TableElement.RowHeight = 50;
                    SetColorColumn();

                    break;
                case "3":
                    RadCheckBox_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = true; radCheckBox_Dpt.Text = "แบ่งตามวันที่";
                    RadDropDownList_Branch.Visible = false; radDropDownList_Dpt.Visible = false;
                    radLabel1.Text = "วันที่";
                    radLabel_Detail.Text = "สีม่วง >> ผลรวม | สีเหลือง >> จำนวนบิล | สีแดง >> ต่ำกว่า 70%";
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "รหัสแผนก", 70)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMALL", $@"จำนวน{Environment.NewLine}รายการ", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM1", $@"รอจ่ายบิล{Environment.NewLine}1 รอดำเนินการ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM1", $@"% รอจ่ายบิล{Environment.NewLine}1 รอดำเนินการ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM11", $@"กำลังจัดบิล{Environment.NewLine}11 รอจัดบิล", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM11", $@"% กำลังจัดบิล{Environment.NewLine}11 รอจัดบิล", 150)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM12", $@"จัดได้แล้ว{Environment.NewLine}12 จัดแล้วยังไม่ส่งข้อมูล", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM12", $@"% จัดได้แล้ว{Environment.NewLine}12 จัดแล้วยังไม่ส่งข้อมูล", 150)));//จัดบิลเรียบร้อยแล้ว
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM13", $@"ส่งเข้า AX แล้ว{Environment.NewLine}13 ส่งเข้า AX แล้ว", 150)));
                    //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM13", $@"% ส่งเข้า AX แล้ว{Environment.NewLine}13 ส่งเข้า AX แล้ว", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM13OK", $@"ส่งเข้า AX แล้ว{Environment.NewLine} จัดได้", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM13OK", $@"% ส่งเข้า AX แล้ว{Environment.NewLine} จัดได้", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM13Not", $@"ส่งเข้า AX แล้ว{Environment.NewLine} จัดไม่ได้", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("PSUM13Not", $@"% ส่งเข้า AX แล้ว{Environment.NewLine} จัดไม่ได้", 150)));
                    RadGridView_ShowHD.Columns["TRANSDATE"].IsPinned = true;
                    RadGridView_ShowHD.Columns["DIMENSION"].IsPinned = true;
                    RadGridView_ShowHD.Columns["DESCRIPTION"].IsPinned = true;
                    RadGridView_ShowHD.Columns["SUMALL"].IsPinned = true;
                    RadGridView_ShowHD.TableElement.RowHeight = 50;
                    SetColorColumn();
                    break;
                case "4":
                    RadCheckBox_Branch.Visible = true;
                    radCheckBox_Dpt.Visible = false;
                    RadDropDownList_Branch.Visible = true; radDropDownList_Dpt.Visible = false;
                    radLabel1.Text = "วันที่";
                    radLabel_Detail.Text = "สีม่วง >> ผลรวม | สีเหลือง >> จำนวนบิล ";
                    break;
                case "5":
                    //radCheckBox_Dpt.Visible = true;
                    radCheckBox_Dpt.Visible = true; radCheckBox_Dpt.Checked = true; radCheckBox_Dpt.ReadOnly = true;
                    radRadioButton_Photo.Visible = true; radRadioButton_Barcode.Visible = true; radRadioButton_Photo.CheckState = CheckState.Checked;

                    if (_pTypeOpen == "SHOP")
                    {
                        //radCheckBox_Dpt.Checked = false; radCheckBox_Dpt.Enabled = true;
                        radDropDownList_Dpt.Visible = true;
                        //radRadioButton_Photo.Visible = false; radRadioButton_Barcode.Visible = false;
                        radCheck_NotTask.Visible = false; radCheck_NotTask.Checked = false;//radCheck_TaskOp.Visible = false; 
                        radImage_EMPL.Enabled = false;
                        radButtonElement_PrintContainer.Enabled = false;
                        radLabel_Detail.Visible = false;
                    }
                    else
                    {
                        //radCheckBox_Dpt.Checked = true; radCheckBox_Dpt.Enabled = false;
                        //radRadioButton_Photo.Visible = true; radRadioButton_Barcode.Visible = true; radRadioButton_Photo.CheckState = CheckState.Checked;
                        radCheck_NotTask.Visible = true; radCheck_NotTask.Checked = true;//  radCheck_TaskOp.Visible = true; 
                        radImage_EMPL.Enabled = true;
                        radButtonElement_PrintContainer.Enabled = true;

                    }

                    #region "DGV"                   
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("C", "เลือก")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TRANSID", "เลขที่บิล")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image1", "รูป", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "สถานะ", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ORDERIMAGEPATH", "ที่อยู่รูป")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REFMNPO", "ใบสั่งโอนย้าย", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHID", "รหัสสาขา", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHNAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("EMPLID", "รหัสพนักงาน")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAMEORDER", "พนักงานสั่ง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPNAMEPICKING", "พนักงานจัดสินค้า", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ORDERSTATUS", "รหัสสถานะ")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATE", "วันที่", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_PICKINGLOCATIONID", "คลัง", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_WMSLocationId", "คลังหยิบ", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("TARGETDIMENSION", "รหัสแผนก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEPACKINGALL", "ชื่อแผนกที่รับผิดชอบ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTEID", "สายรถ", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REFNO2", "สั่งโดย", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ORDERSTA_PACKING", "ORDERSTA_PACKING")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("EMPLIDPACKING", "EMPLIDPACKING")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("EMPLNAMEPACKING", "EMPLNAMEPACKING")));
                    RadGridView_ShowHD.TableElement.RowHeight = 120;
                    radLabel_Detail.Text = "DoubleClick ที่รูป >> เพื่อดูรูปใหญ่ | DoubleClick ที่รหัสสาขา >> เพื่อเลือกสาขานั้นทั้งหมด | DoubleClick ที่บาร์โค้ด >> เพื่อจับคู่บาร์โค้ด | DoubleClick ที่รหัสแผนก >> เพื่อแก้ไขจัดซื้อ | DoubleClick ที่ใบสั่งโอนย้าย >> เพื่อ Print | DoubleClick จำนวน >> ยกเลิกออเดอร์";
                    #endregion

                    DatagridClass.SetCellBackClolorByExpression("SHOW_NAME", " ORDERSTATUS = '13' AND ORDERSTA_PACKING NOT IN ('1','3','4') ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SHOW_NAME", " ORDERSTATUS <> '13' AND SHOW_NAME = 'รอดำเนินการ' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SHOW_NAME", " ORDERSTATUS <> '13' AND SHOW_NAME = 'รอจัดบิล' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
                    if (SystemClass.SystemBranchID == "MN000")
                        radLabel_color.Text = "DoubleClick ที่คลังหยิบ >> เพื่อเลือกคลังหยิบนั้นทั้งหมด | DoubleClick ที่สาย >> เพื่อเลือกสายนั้นทั้งหมด | สีแดง >> สินค้าจัดไม่ได้ | สีม่วง >> รอดำเนินการ | สีฟ้า >> รอจัดบิล";
                    else radLabel_color.Text = "สีแดง >> สินค้าจัดไม่ได้ | สีม่วง >> รอดำเนินการ | สีฟ้า >> รอจัดบิล";

                    dt_DimensionPackIng = ConnectionClass.SelectSQL_Main("SELECT	DIMENSION,ISPACKINGALL,ISUSEINVENTAX,STAPACKING_MN  FROM    SHOP_WEB_CONFIGTARGETDIMS WITH(NOLOCK) ");// WHERE   ISACTIVE = '1'
                    for (int i = 0; i < 3; i++)
                    {
                        this.RadGridView_ShowHD.MasterTemplate.Columns[i].IsPinned = true;
                    }

                    break;
                case "6":
                    radImage_EMPL.Enabled = true; radImage_EMPL.ToolTipText = "สร้างใบจัด";
                    radButtonElement_PrintContainer.Enabled = true;
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = true; radDropDownList_Dpt.Visible = true; radCheckBox_Dpt.Checked = true; radCheckBox_Dpt.ReadOnly = true;
                    radDateTimePicker_Begin.Visible = false; radDateTimePicker_End.Visible = false;
                    radRadioButton_Photo.Visible = false; radRadioButton_Barcode.Visible = false;
                    radLabel1.Visible = false;
                    radCheck_NotTask.Visible = false; //radCheck_TaskOp.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("C", "เลือก")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("COUNT_BILL", $@"จำนวนบิลย้อนหลัง 3 วัน{Environment.NewLine}ยังไม่ส่งเข้า AX", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTEID", "สาย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ROUTENAME", "ชื่อสาย", 350)));


                    DatagridClass.SetCellBackClolorByExpression("COUNT_BILL", " COUNT_BILL <> '0' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                    radLabel_Detail.Text = "เลือกสาขา >> เปิดบิล | พิมพ์ Container >> ระบุ พนง จัดบิล/จำนวน/เลือก Printer | สีฟ้า >> มีบิลบิลย้อนหลัง 3 วันยังไม่ส่งเข้า AX";
                    radLabel_color.Text = "DoubleClick แถว >> เพื่อพิมพ์บิลซ้ำ | DoubleClick สาย >> เพื่อเลือกสาขาในสายที่ตรงกันทั้งหมดสำหรับปล่อยบิล";

                    break;
                case "7":
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = true; radDropDownList_Dpt.Visible = true; radCheckBox_Dpt.ReadOnly = true; radCheckBox_Dpt.CheckState = CheckState.Checked;
                    radLabel1.Text = "วันที่จัดสินค้า";
                    radLabel_Detail.Text = "ข้อมูลรายงานสินค้าเฉพาะรายการที่จัดได้เท่านั้น [เฉพาะรายการที่มีการสั่งเท่านั้น]";
                    radLabel_color.Text = "";
                    break;
                case "8": //8 = รายการสินค้าที่จัดทั้งหมด (ได้หรือไม่ได้)
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = true; radDropDownList_Dpt.Visible = true;
                    radLabel1.Text = "วันที่จัดสินค้า";
                    radCheck_NotTask.Visible = true; radCheck_NotTask.Text = "แสดงรูปสินค้า";
                    radRadioButton_Photo.Visible = true; radRadioButton_Barcode.Visible = true;
                    radRadioButton_Photo.Text = "จัดไม่ได้"; radRadioButton_Barcode.Text = "จัดได้"; radRadioButton_Photo.CheckState = CheckState.Checked;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TARGETDIMENSION", "รหัสแผนก", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAMEPACKINGALL", "ชื่อแผนก", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHOW_NAME", "สถานะ", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("DIFFDATE", "จน.วันจัด", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDERNUM", "เลขที่บิลจัด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEPACKING", "วันที่จัด", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDFROM", "คลัง", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTO", "สาขา", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTONAME", "ชื่อสาขา", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPIDPICKING", "พนง จัด", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPNAMEPICKING", "ชื่อ พนง จัด", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Image("Image1", "รูป", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด จัด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า จัด", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน จัด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย จัด", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REFNO2", "ช่องทางการสั่ง", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ORDERIMAGEPATH", "ที่อยู่รูป")));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDER_TRANSDATE", "วันที่สั่ง", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDER_BARCODE", "บาร์โค้ดที่สั่ง", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDER_SPC_ITEMNAME", "ชื่อสินค้าที่สั่ง", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("ORDER_QTY", "จำนวนที่สั่ง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDER_UNITID", "หน่วยที่สั่ง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDER_EMPLID", "ผู้สั่ง", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ORDER_EMPLNAME", "ชื่อ ผู้สั่ง", 200)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("TRANSID", "TRANSID")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("VENDERID", "VENDERID")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STACOPY", "STACOPY")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("FACTOR", "FACTOR")));

                    RadGridView_ShowHD.Columns["TARGETDIMENSION"].IsPinned = true;
                    RadGridView_ShowHD.Columns["NAMEPACKINGALL"].IsPinned = true;
                    RadGridView_ShowHD.Columns["SHOW_NAME"].IsPinned = true;

                    DatagridClass.SetCellBackClolorByExpression("DIFFDATE", " DIFFDATE > 4 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    radLabel_Detail.Text = "ข้อมูลรายงานสินค้าที่ไม่สามารถจัดได้หรือจัดไม่ได้ ตามเงื่อนไขที่ระบุ | สีแดง จน.วันจัด >> ใช้เวลาจัด 5 วันขึ้นไป";
                    if (SystemClass.SystemBranchID == "MN000")
                        radLabel_color.Text = "ข้อมูลแสดงเฉพาะรายการที่ระบุจัดสินค้าเรียบร้อยแล้ว ไม่รวมรายการที่อยู่ระหว่างการจัด [ทุกรายการที่มีการสั่งและไม่สั่ง]";
                    else radLabel_color.Text = "Double Click ที่รายงาน > Copy รายการสั่ง [เฉพาะรายการที่จัดได้เท่านั้น] | ข้อมูลแสดงเฉพาะรายการที่ระบุจัดสินค้าเรียบร้อยแล้ว ไม่รวมรายการที่อยู่ระหว่างการจัด";

                    break;
                case "9": //9 = รายงานประเมินการจัดบิล ตามพนักงานจัดบิล
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = false; radDropDownList_Dpt.Visible = false;// radCheckBox_Dpt.ReadOnly = true; radCheckBox_Dpt.CheckState = CheckState.Checked;
                    radLabel1.Text = "วันที่จัดสินค้า";
                    radLabel_Detail.Text = "ข้อมูลแสดงผลทั้งหมดตามเงื่อนไขวันที่ที่ระบุโดยรวม";
                    radLabel_color.Text = "สีแดง >> สินค้าจัดไม่ได้มากกว่า 30% | สีฟ้า >> จัดได้ครบทุกรายการ";
                    break;
                case "10": //9 = รายงานประเมินการจัดบิลไม่ได้ ตามพนักงานจัดบิล รายเดือน
                    radLabel1.Visible = false;
                    radDateTimePicker_Begin.Visible = false; radDateTimePicker_End.Visible = false;
                    //radRadioButton_Barcode.Visible = true; radRadioButton_Barcode.Text = "%จัดได้";
                    //radRadioButton_Photo.Visible = true; radRadioButton_Photo.Text = "%จัดไม่ได้"; radRadioButton_Photo.CheckState = CheckState.Checked;
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = true; radDropDownList_Dpt.Visible = true; radCheckBox_Dpt.ReadOnly = true; radCheckBox_Dpt.CheckState = CheckState.Checked; radCheckBox_Dpt.Text = "ปี";
                    radLabel1.Text = "วันที่จัดสินค้า";
                    radLabel_Detail.Text = "ข้อมูลแสดงผลเฉพาะรายการที่จัดแล้วเท่านั้น [ไม่รวมรายการค้างจัด] | % การจัดจะมาจากการเปรียบเทียบจากรายการทั้งหมดที่ได้รับ ";
                    radLabel_color.Text = "สีแดง >> จัดไม่ได้มากกว่า 50% | สีเขียว >> ไม่มีรายการจัด | สีฟ้า >> จัดได้ครบทุกรายการ";
                    break;
                case "11":
                    #region "CREATE DGV"
                    radRadioButton_Photo.Text = "ทั้งหมด"; radRadioButton_Photo.Visible = true; radRadioButton_Photo.CheckState = CheckState.Checked;
                    radRadioButton_Barcode.Text = "เฉพาะที่สั่ง"; radRadioButton_Barcode.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STAORDER", "ประเภทสั่ง", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("STATUS", "สถานะจัด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่", 110)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDFROM", "คลัง", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTO", "สาขา", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_LOCATIONNAMETO", "ชื่อสาขา", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSFERID", "เลขที่บิล", 140)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ItemBarCode", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ItemName", "ชื่อสินค้า", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_Qty", "จำนวน สั่ง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_SHIP", "จำนวน จัด", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_RECIVE", "จำนวน รับ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ItemBarCodeUnit", "หน่วย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_Remarks", "หมายเหตุ", 400)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_TRACKINGID", "รหัสการติดตาม", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "จัดซื้อ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUMNAME", "แผนกจัดซื้อ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTTRANSID", "รหัสอ้างอิง", 200)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));

                    RadGridView_ShowHD.Columns["STAORDER"].IsPinned = true;
                    RadGridView_ShowHD.Columns["STATUS"].IsPinned = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("STATUSID", "STATUSID")));
                    //0 ยกเลิก  1 กำลังจัด 2 กำลังจัดส่ง 3 ได้รับแล้ว

                    DatagridClass.SetCellBackClolorByExpression("STATUS", "STATUSID = '0' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("STATUS", "STATUSID = '1' ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("STATUS", "STATUSID = '2' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("STATUS", "STATUSID = '3' ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                    radLabel_Detail.Text = "ระบบจัดซื้อจัดสินค้าเอง > ต้องดูออเดอร์ประกบที่รายงานระบบจัดซื้อจัดสินค้าเอง | ไม่มีประวัติสั่ง >> Auto มาจากระบบหรือเปิดออเดอร์ผ่าน AX โดยตรง";
                    radLabel_color.Text = "สีแดง >> ยกเลิก | สีเขียว >> กำลังจัดส่งสินค้า | สีม่วง >> กำลังจัดสินค้า | สีฟ้า >> ได้รับแล้ว";
                    #endregion
                    break;
                case "12": //12 = รายงานการใช้งานเครื่องโปรแกรม Container รายเดือน
                    radLabel1.Visible = false;
                    radDateTimePicker_Begin.Visible = false; radDateTimePicker_End.Visible = false;
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = true; radDropDownList_Dpt.Visible = true; radCheckBox_Dpt.ReadOnly = true; radCheckBox_Dpt.CheckState = CheckState.Checked; radCheckBox_Dpt.Text = "ปี";
                    radLabel1.Text = "วันที่จัดสินค้า";
                    radLabel_Detail.Text = "สีแดง > ไม่มีรายการใช้งานเครื่อง";
                    radLabel_color.Text = "เริ่มเก็บข้อมูลการใช้งานเครื่องเดือนเมษายน 2023 เป็นต้นไป | เครื่อง TAB มีการใช้งานแค่บางเครื่องเท่านั้น";
                    break;
                case "13"://13 = รายงานรายเดือนตามประเภทการสั่ง ตามบุคคล
                    radLabel1.Visible = false;
                    radDateTimePicker_Begin.Visible = false; radDateTimePicker_End.Visible = false;
                    RadCheckBox_Branch.Visible = true; RadDropDownList_Branch.Visible = true; RadCheckBox_Branch.Text = "ประเภทการสั่ง";
                    radRadioButton_Photo.Visible = true; radRadioButton_Photo.Text = "รายเดือน"; radRadioButton_Photo.CheckState = CheckState.Checked;
                    radRadioButton_Barcode.Visible = true; radRadioButton_Barcode.Text = "รายวัน";
                    radCheckBox_Dpt.Visible = true; radDropDownList_Dpt.Visible = true; radCheckBox_Dpt.ReadOnly = true; radCheckBox_Dpt.CheckState = CheckState.Checked; radCheckBox_Dpt.Text = "ปี";
                    radLabel1.Text = "วันที่สั่งสินค้า";
                    radLabel_Detail.Text = "สีแดง >> ไม่มีรายการสั่ง | ช่องจัดได้+ยอดรวมจัดได้ >> วันปัจจุบันอาจจะไม่แสดงข้อมูลเพราะรอระบบการจัดสินค้าก่อน แนะนำดึงข้อมูลย้อนหลัง 7 วัน";
                    radLabel_color.Text = "ช่อง ยอดรวมจัดได้ >> จะเปลี่ยนแปลงได้ตลอดถ้ามีการปรับราคาขายเงินสด ขึ้น-ลง เพราะฉะนั้นยอดอาจจะไม่เท่ากัน ในวันที่ดึงไว้แล้วและวันที่ดึงปัจจุบัน";
                    break;
                case "14": //14 = รายงานรอทำบิลจัดสินค้าในระบบ Container
                    #region "CREATE DGV"
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = false; radDropDownList_Dpt.Visible = false;
                    radLabel1.Visible = false; radDateTimePicker_Begin.Visible = false; radDateTimePicker_End.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CreatedDateTime", "วันที่", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("OrderNum", "เลขที่บิล", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDFROM", "คลัง", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("InventLocationIdTo", "สาขา", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสาขา", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ContainerId", "เลขที่ Container", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BarcodeConfirm", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 320)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QtyConfirm", "จำนวน สั่ง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "ผู้จัด", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อ ผู้จัด", 220)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "จัดซื้อ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนกจัดซื้อ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_Remarks", "หมายเหตุ", 400)));

                    RadGridView_ShowHD.Columns["CreatedDateTime"].IsPinned = true;
                    RadGridView_ShowHD.Columns["OrderNum"].IsPinned = true;

                    radLabel_Detail.Text = "แสดงรายการสินค้าทั้งหมดที่รอทำบิลในระบบ Container | ข้อมูลที่แสดงย้อนหลัง 15 วัน";
                    radLabel_color.Text = "แสดงรายการเพื่อสำหรับตรวจสอบข้อมูลทั้งหมดที่จัดว่าได้รับการทำบิลเรียบร้อยแล้วหรือไม่";
                    #endregion
                    break;
                case "15"://15 = รายงานการใข้งานเครื่อง PDA/CPW/TAB ของโปรแกรมที่ใช้ Login บน Android
                    #region "CREATE DGV"
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = false; radDropDownList_Dpt.Visible = false;
                    radLabel1.Visible = false; radDateTimePicker_Begin.Visible = false; radDateTimePicker_End.Visible = false;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ASSETID", "ทะเบียน", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อสินทรัพย์", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SERIALNUM", "S/N", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CONDITION", "สถานะ", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Location", "แผนก", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTTIMEUPDATE", "เวลาใช้ล่าสุด", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTUSERLOGIN", "ผู้ใช้ล่าสุด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อผู้ใช้ล่าสุด", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTAPPLICATION", "โปรแกรมที่ใช้", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("LASTAPPVERSION", "เวอร์ชั่นโปรแกรม", 90)));

                    RadGridView_ShowHD.Columns["ASSETID"].IsPinned = true;
                    RadGridView_ShowHD.Columns["NAME"].IsPinned = true;
                    DatagridClass.SetCellBackClolorByExpression("ASSETID", " LASTUSERLOGIN = '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("NAME", " LASTUSERLOGIN = '' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    radLabel_Detail.Text = "แสดงรายการทั้งหมดของ TAB/CPW/PDA ในสถานะที่ ใช้งาน เท่านั้น";
                    radLabel_color.Text = "บางโปรแกรมที่ใช้งานไม่ผ่านระบบการเก็บ Login นี้";
                    #endregion
                    break;
                case "16"://16 = รายงานการจัดสินค้าสาขาใหญ่
                    #region ""
                    RadCheckBox_Branch.Visible = false; RadDropDownList_Branch.Visible = false;
                    radCheckBox_Dpt.Visible = false; radDropDownList_Dpt.Visible = false; radDateTimePicker_End.Visible = false;
                    radDateTimePicker_Begin.Value = DateTime.Now;
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDFROM", "คลัง", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTO", "สาขา", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_LOCATIONNAMETO", "ชื่อสาขา", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันที่จัด", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHIPDATE", "วันที่ทำบิล", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("RECEIVEDATE", "วันที่รับ", 110)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSFERID", "เลขที่บิล", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY_SCAN", "จำนวนสาขาจัด", 90)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_Qty", "จำนวนทำบิล", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBARCODEUNIT", "หน่วย", 80)));

                    //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYTRANSFER", "จำนวนในบิล", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYSHIPPED", "จำนวนส่ง", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTYRECEIVED", "จำนวนรับ", 80)));

                    DatagridClass.SetCellBackClolorByExpression("QTY_SCAN", " QTY_SCAN > 0 AND QTY_SCAN > SPC_Qty ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SPC_Qty", " QTY_SCAN > 0 AND QTY_SCAN > SPC_Qty ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

                    DatagridClass.SetCellBackClolorByExpression("QTY_SCAN", " QTY_SCAN > 0 AND QTY_SCAN < SPC_Qty ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SPC_Qty", " QTY_SCAN > 0 AND QTY_SCAN < SPC_Qty ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                    DatagridClass.SetCellBackClolorByExpression("QTY_SCAN", " QTY_SCAN = 0 ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SPC_Qty", " QTY_SCAN = 0 ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);

                    DatagridClass.SetCellBackClolorByExpression("SPC_ITEMBARCODE", " QTY_SCAN > 0 AND SPC_Qty = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SPC_ITEMNAME", " QTY_SCAN > 0 AND SPC_Qty = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("QTY_SCAN", " QTY_SCAN > 0 AND SPC_Qty = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SPC_Qty", " QTY_SCAN > 0 AND SPC_Qty = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    radLabel_Detail.Text = "สีแดง >> สาขาจัดสินค้าแต่ไม่มีในบิล ";
                    radLabel_color.Text = "สีฟ้า >> จำนวนที่สาขาจัด มากกว่า สินค้าที่ทำบิล | สีม่วง >> จำนวนที่สาขาจัด น้อยกว่า สินค้าที่ทำบิล | สีเหลือง >> สาขาไม่ได้จัดสินค้า แต่มีในบิล ";
                    #endregion
                    break;
                default:
                    break;
            }

            dtRoute = LogisticClass.GetRouteAll_GroupBranch();

            RadButton_Search.ButtonElement.ShowBorder = true;

            DataTable dtBch = new DataTable();
            switch (_pTypeOpen)
            {
                case "SHOP"://shop
                    RadCheckBox_Branch.Checked = true;
                    dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    RadCheckBox_Branch.ReadOnly = true;
                    break;
                case "SUPC": // supc
                    RadCheckBox_Branch.Checked = false;
                    dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                    RadCheckBox_Branch.Enabled = true;
                    break;
                default:
                    break;
            }

            if (_pTypeReport == "8")
            {
                RadCheckBox_Branch.Checked = true;
                RadCheckBox_Branch.ReadOnly = false;
                RadCheckBox_Branch.Visible = true; RadDropDownList_Branch.Visible = true; dtBch = BranchClass.GetBranchAll("'1'", "'1'");
                radCheckBox_Dpt.Checked = true;
            }

            if (_pTypeReport == "13")
            {
                dtBch = POClass.OrderWeb_ReportByTypePO("0", "", "", "", "");
                RadCheckBox_Branch.Checked = true;
                RadCheckBox_Branch.ReadOnly = true;

            }


            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
            RadDropDownList_Branch.ValueMember = "BRANCH_ID";

            if ((_pTypeReport == "8") && (SystemClass.SystemBranchID != "MN000"))
            { RadDropDownList_Branch.SelectedValue = SystemClass.SystemBranchID; }

            if ((_pTypeReport == "5") || (_pTypeReport == "6") || (_pTypeReport == "7") || (_pTypeReport == "8"))
            {
                string fetchTargetDimension = PO_Class.FindDateDMS("5");
                DataTable targetDims = ConnectionClass.SelectSQL_Main(fetchTargetDimension);
                radDropDownList_Dpt.DataSource = targetDims;
                radDropDownList_Dpt.DisplayMember = "DESCRIPTION";
                radDropDownList_Dpt.ValueMember = "DIMENSION";
            }
            else if ((_pTypeReport == "10") || (_pTypeReport == "12") || (_pTypeReport == "13"))
            {
                radDropDownList_Dpt.DataSource = DateTimeSettingClass.Config_YY();
                radDropDownList_Dpt.DisplayMember = "Y1";
                radDropDownList_Dpt.ValueMember = "Y2";
                RadDropDownList_Branch.SelectedValue = "VanSale";
            }
            else
            {

                radDropDownList_Dpt.DataSource = Models.DptClass.GetDpt_Purchase();
                radDropDownList_Dpt.DisplayMember = "DESCRIPTION";
                radDropDownList_Dpt.ValueMember = "NUM";
            }

            RadGridView_ShowHD.DataSource = dt_Data;
            if (_sItemID == null) _sItemID = "";
            if (((_pTypeReport == "11") && (_sItemID != "")) || ((_pTypeReport == "8") && (_sItemID != "")))
            {
                SetDGV_HD();
                radDateTimePicker_Begin.Value = DateTime.Now.AddDays(-120);
                RadCheckBox_Branch.Checked = true;
                RadDropDownList_Branch.SelectedValue = _sBchID;
                if (_pTypeReport == "8") radRadioButton_Barcode.CheckState = CheckState.Checked;
                if (_sDptID != "")
                {
                    radCheckBox_Dpt.Checked = true;
                    radDropDownList_Dpt.SelectedValue = _sDptID;
                }
            }

            //SetDGV_HD();
        }

        private void RadRadioButton_Photo_CheckStateChanged(object sender, EventArgs e)
        {
            if (_pTypeReport == "13")
            {
                radDropDownList_Dpt.Visible = true; radCheckBox_Dpt.Visible = true;
                radLabel1.Visible = false;
                radDateTimePicker_Begin.Visible = false; radDateTimePicker_End.Visible = false;
            }
        }

        private void RadRadioButton_Barcode_CheckStateChanged(object sender, EventArgs e)
        {
            if (_pTypeReport == "13")
            {
                radDropDownList_Dpt.Visible = false; radCheckBox_Dpt.Visible = false;
                radLabel1.Visible = true;
                radDateTimePicker_Begin.Visible = true; radDateTimePicker_End.Visible = true;
            }
        }

        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }

            switch (_pTypeReport)
            {
                case "0":

                    string bchID = "", dptID = "";
                    if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();
                    if (radCheckBox_Dpt.Checked == true) dptID = radDropDownList_Dpt.SelectedValue.ToString();

                    dt_Data = POClass.Report_DetailMNPO(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd")
                       , bchID, _pBillID, _pTypeID, _pWH, dptID);//ConnectionClass.SelectSQL_Main(sqlSelect);
                    //dt_Data = ConnectionClass.SelectSQL_Main(sqlSelect);
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "1":

                    string bchID1 = "";
                    if (RadCheckBox_Branch.Checked == true) bchID1 = RadDropDownList_Branch.SelectedValue.ToString();
                    dt_Data = PO_Class.FindMNPO_ByWeb(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), bchID1);
                    //dt_Data = ConnectionClass.SelectSQL_Main(sqlSelect);
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;

                case "2":
                    string date1 = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
                    string date2 = radDateTimePicker_End.Value.ToString("yyyy-MM-dd");
                    string sqlData = PO_Class.FindDate(date1, date2, "2");
                    DataTable dtDate = ConnectionClass.SelectSQL_Main(sqlData);
                    DataTable dtSum = PO_Class.GetSumOrderWebImage("", date1, date2, "");
                    DataTable dtSum1 = PO_Class.GetSumOrderWebImage("1", date1, date2, "");
                    DataTable dtSum11 = PO_Class.GetSumOrderWebImage("11", date1, date2, "");
                    DataTable dtSum12 = PO_Class.GetSumOrderWebImage("12", date1, date2, "");
                    //DataTable dtSum13 = PO_Class.GetSumOrderWebImage("13", date1, date2);
                    DataTable dtSum13OK = PO_Class.GetSumOrderWebImage("13", date1, date2, "1"); //จัดได้
                    DataTable dtSum13Not = PO_Class.GetSumOrderWebImage("13", date1, date2, "0");//จัดไม่ได้

                    if (dtDate.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtDate.Rows.Count; i++)
                        {
                            string date = dtDate.Rows[i]["TRANSDATE"].ToString();
                            //ผลรวม
                            DataRow[] sumall = dtSum.Select($@" TRANSDATE = '{date}'   ");
                            double sAll = 0;
                            if (sumall.Length > 0) sAll = double.Parse(sumall[0]["SUMALL"].ToString());
                            //รอจัดบิล 1

                            double sA1 = GetSumQty(dtSum1, $@" TRANSDATE = '{date}' ");
                            double sA11 = GetSumQty(dtSum11, $@" TRANSDATE = '{date}' ");
                            double sA12 = GetSumQty(dtSum12, $@" TRANSDATE = '{date}' ");
                            //double sA13 = GetSumQty(dtSum13, $@" TRANSDATE = '{date}' ");
                            double sA13Ok = GetSumQty(dtSum13OK, $@" TRANSDATE = '{date}' ");
                            double sA13not = GetSumQty(dtSum13Not, $@" TRANSDATE = '{date}' ");
                            double s1PerCent = 0, s11PerCent = 0, s12PerCent = 0, s13OKPerCent = 0, s13notPerCent = 0;//s13PerCent = 0,

                            if (sAll > 0)
                            {
                                s1PerCent = (sA1 / sAll) * 100;
                                s11PerCent = (sA11 / sAll) * 100;
                                s12PerCent = (sA12 / sAll) * 100;
                                //s13PerCent = (sA13 / sAll) * 100;
                                s13OKPerCent = (sA13Ok / sAll) * 100;
                                s13notPerCent = (sA13not / sAll) * 100;
                            }
                            //RadGridView_ShowHD.Rows.Add(date, sAll, sA1, s1PerCent, sA11, s11PerCent, sA12, s12PerCent, sA13, s13PerCent);
                            RadGridView_ShowHD.Rows.Add(date, sAll, sA1, s1PerCent, sA11, s11PerCent, sA12, s12PerCent, sA13Ok, s13OKPerCent, sA13not, s13notPerCent);
                        }
                    }
                    this.Cursor = Cursors.Default;
                    break;
                case "3":
                    string beginDate = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
                    string endDate = radDateTimePicker_End.Value.ToString("yyyy-MM-dd");
                    string staGroupDate = "0";
                    string dataDAte;
                    if (radCheckBox_Dpt.Checked == false)
                    {
                        dataDAte = PO_Class.FindDateDMS("3");
                        RadGridView_ShowHD.Columns["TRANSDATE"].IsVisible = false;
                    }
                    else
                    {
                        staGroupDate = "1";
                        dataDAte = PO_Class.FindDate(beginDate, endDate, "3");
                        RadGridView_ShowHD.Columns["TRANSDATE"].IsVisible = true;
                    }
                    DataTable dtData = ConnectionClass.SelectSQL_Main(dataDAte);
                    DataTable dtDMSSum = PO_Class.GetSumOrderByDMS("", beginDate, endDate, staGroupDate, "0", "");
                    DataTable dtDMSSum1 = PO_Class.GetSumOrderByDMS("1", beginDate, endDate, staGroupDate, "0", "");
                    DataTable dtDMSSum11 = PO_Class.GetSumOrderByDMS("11", beginDate, endDate, staGroupDate, "0", "");
                    DataTable dtDMSSum12 = PO_Class.GetSumOrderByDMS("12", beginDate, endDate, staGroupDate, "0", "");
                    DataTable dtDMSSum13OK = PO_Class.GetSumOrderByDMS("13", beginDate, endDate, staGroupDate, "0", "1"); //จัดได้
                    DataTable dtDMSSum13Not = PO_Class.GetSumOrderByDMS("13", beginDate, endDate, staGroupDate, "0", "0");//จัดไม่ได้
                    if (dtData.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtData.Rows.Count; i++)
                        {
                            string date = dtData.Rows[i]["TRANSDATE"].ToString();
                            string dms = dtData.Rows[i]["DIMENSION"].ToString();
                            string description = dtData.Rows[i]["DESCRIPTION"].ToString();

                            double sDMSAll = GetSumQty(dtDMSSum, $@" TRANSDATE = '{date}' AND DIMENSION = '{dms}'  ");
                            double s1DMSPer = 0, s11DMSPer = 0, s12DMSPer = 0, s13OKDMSPer = 0, s13NOTDMSPer = 0; //s13DMSPer = 0;
                            double s1 = GetSumQty(dtDMSSum1, $@" TRANSDATE = '{date}' AND DIMENSION = '{dms}'  ");
                            double s11 = GetSumQty(dtDMSSum11, $@" TRANSDATE = '{date}' AND DIMENSION = '{dms}'  ");
                            double s12 = GetSumQty(dtDMSSum12, $@" TRANSDATE = '{date}' AND DIMENSION = '{dms}' ");
                            //double s13 = GetSumQty(dtDMSSum13, $@" TRANSDATE = '{date}' AND DIMENSION = '{dms}' ");
                            double s13OK = GetSumQty(dtDMSSum13OK, $@" TRANSDATE = '{date}' AND DIMENSION = '{dms}'   ");
                            double s13not = GetSumQty(dtDMSSum13Not, $@" TRANSDATE = '{date}' AND DIMENSION = '{dms}'   ");

                            if (sDMSAll > 0)
                            {
                                s1DMSPer = (s1 / sDMSAll) * 100;
                                s11DMSPer = (s11 / sDMSAll) * 100;
                                s12DMSPer = (s12 / sDMSAll) * 100;
                                //s13DMSPer = (s13 / sDMSAll) * 100;
                                s13OKDMSPer = (s13OK / sDMSAll) * 100;
                                s13NOTDMSPer = (s13not / sDMSAll) * 100;
                            }
                            //RadGridView_ShowHD.Rows.Add(date, dms, description, sDMSAll, s1, s1DMSPer, s11, s11DMSPer, s12, s12DMSPer, s13, s13DMSPer);
                            RadGridView_ShowHD.Rows.Add(date, dms, description, sDMSAll, s1, s1DMSPer, s11, s11DMSPer, s12, s12DMSPer, s13OK, s13OKDMSPer, s13not, s13NOTDMSPer);
                        }
                    }
                    this.Cursor = Cursors.Default;
                    break;
                case "4":
                    string bch_4 = "";
                    string dateStart = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
                    string dateEnd = radDateTimePicker_End.Value.ToString("yyyy-MM-dd");

                    if (RadCheckBox_Branch.Checked == true) bch_4 = RadDropDownList_Branch.SelectedValue.ToString();

                    DataTable branch = PO_Class.FindMNPO_ByWeb(dateStart, dateEnd, bch_4);
                    DataTable dtMNSum = PO_Class.GetSumOrderByDMS("", dateStart, dateEnd, "0", "1", "");
                    string DMS = PO_Class.FindDateDMS("4");
                    DataTable dtMNDms = ConnectionClass.SelectSQL_Main(DMS);

                    if (RadGridView_ShowHD.Columns.Count > 0) RadGridView_ShowHD.Columns.Clear();
                    if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();
                    RadGridView_ShowHD.SummaryRowsTop.Clear();

                    //create Columns
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัสสาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM", "รวมทั้งหมด", 100)));
                    GridViewSummaryRowItem summaryRowItemCase3 = new GridViewSummaryRowItem();
                    for (int i = 0; i < dtMNDms.Rows.Count; i++)
                    {
                        RadGridView_ShowHD.MasterTemplate.Columns.Add(
                            (DatagridClass.AddNumberColumn_AddManualSetRight(dtMNDms.Rows[i]["DIMENSION"].ToString(),
                                        dtMNDms.Rows[i]["DESCRIPTION"].ToString(), 150)));

                        DatagridClass.SetCellBackClolorByExpression(dtMNDms.Rows[i]["DIMENSION"].ToString(), dtMNDms.Rows[i]["DIMENSION"].ToString() + " > 0",
                            ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);

                        GridViewSummaryItem AA = new GridViewSummaryItem(dtMNDms.Rows[i]["DIMENSION"].ToString(), "{0:#,##0.00}", GridAggregateFunction.Sum);
                        summaryRowItemCase3.Add(AA);
                    }
                    GridViewSummaryItem BB = new GridViewSummaryItem("SUM", "{0:#,##0.00}", GridAggregateFunction.Sum);
                    summaryRowItemCase3.Add(BB);
                    RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemCase3);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
                    DatagridClass.SetCellBackClolorByExpression("SUM", " SUM <> 0", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);

                    if (branch.Rows.Count > 0)
                    {
                        for (int iR = 0; iR < branch.Rows.Count; iR++)
                        {
                            string bch = branch.Rows[iR]["BRANCH_ID"].ToString();
                            string branchName = branch.Rows[iR]["BRANCH_NAME"].ToString();
                            RadGridView_ShowHD.Rows.Add(bch, branchName, "0.00");

                            double sumAll = 0;
                            for (int iC = 0; iC < dtMNDms.Rows.Count; iC++)
                            {
                                string dms = dtMNDms.Rows[iC]["DIMENSION"].ToString();
                                double sA = GetSumQty(dtMNSum, $@" DIMENSION = '{dms}' AND BRANCHID = '{bch}'");
                                RadGridView_ShowHD.Rows[iR].Cells[dms].Value = sA;
                                sumAll += sA;
                            }
                            RadGridView_ShowHD.Rows[iR].Cells["SUM"].Value = sumAll.ToString("N2");
                        }
                    }
                    this.Cursor = Cursors.Default;
                    break;
                case "5":
                    this.Cursor = Cursors.WaitCursor;
                    SetCase5_OrderByWeb();
                    this.Cursor = Cursors.Default;
                    break;
                case "6":
                    SetCase6_WebNoOrder();
                    break;
                case "7"://จำนวนออเดอร์แต่ละจัดบิล
                    SetCase7();
                    break;
                case "8":  //8 = รายการจัดสินค้าที่ไม่สามารถจัดได้(หมด)
                    SetCase8();
                    break;
                case "9": //9 = รายงานประเมินการจัดบิล ตามพนักงานจัดบิล
                    SetCase9();
                    break;
                case "10"://10 = รายงานประเมินการจัดบิลไม่ได้ ตามพนักงานจัดบิล
                    SetCase10();
                    break;
                case "11"://11 = Check Order
                    SetCase11();
                    break;
                case "12"://12 = รายงานการใช้งานเครื่องโปรแกรม Container รายเดือน
                    SetCase12();
                    break;
                case "13": //13 = รายงานรายเดือนตามประเภทการสั่ง ตามบุคคล
                    SetCase13();
                    break;
                case "14": //14 = รายงานรอทำบิลจัดสินค้าในระบบ Container
                    this.Cursor = Cursors.WaitCursor;
                    dt_Data = POClass.ReportGetDataContainerNotApv();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "15":
                    this.Cursor = Cursors.WaitCursor;
                    string sql = $@"
                        SELECT	ASSET.*,DIMENSIONS.DESCRIPTION,ISNULL([LASTUSERLOGIN],'') AS [LASTUSERLOGIN],ISNULL(SPC_NAME,'') AS SPC_NAME,
		                        CONVERT(VARCHAR,ISNULL([LASTTIMEUPDATE],''),25) AS [LASTTIMEUPDATE],
		                        ISNULL([LASTAPPLICATION],'') AS [LASTAPPLICATION],ISNULL(LASTAPPVERSION,'') AS LASTAPPVERSION
                        FROM	(
			                        SELECT	ASSETID,NAME,SERIALNUM,CONDITION,Location
			                        FROM	SUPC2014.dbo.ASSETTABLE	WITH (NOLOCK)
			                        WHERE	CONDITION = 'ใช้งาน'
					                        AND ( ASSETID LIKE 'CPW%'  OR ASSETID LIKE 'TAB%' OR ASSETID LIKE 'PDA%')
		                        )ASSET
		                        LEFT OUTER JOIN
		                        (
			                        SELECT *
			                        FROM (
				                        SELECT	[BLUETOOTH],[MODEL],[SERIAL],[BRAND],[ID],[IMEI],
						                        REPLACE(REPLACE(REPLACE([LASTUSERLOGIN],'M',''),'D',''),'P','') AS LASTUSERLOGIN,
						                        [LASTAPPLICATION],[LASTTIMEUPDATE],LASTAPPVERSION,
						                        [LINEID],ROW_NUMBER() OVER(PARTITION BY [BLUETOOTH] ORDER BY LASTTIMEUPDATE DESC) AS ROWNUM
				                        FROM	[cp_wr_database].[dbo].[TABLETTABLE] WITH (NOLOCK)
				                        )TMP
			                        WHERE ROWNUM = 1 
		                        )LASTUSE ON  ASSET.ASSETID = LASTUSE.BLUETOOTH
		                        LEFT OUTER JOIN SUPC2014.dbo.DIMENSIONS WITH (NOLOCK) ON ASSET.LOCATION = DIMENSIONS.NUM
			                        AND DIMENSIONS.DATAAREAID = N'SPC' AND DIMENSIONS.DIMENSIONCODE = '0'
		                        LEFT OUTER JOIN SUPC2014.dbo.EMPLTABLE  WITH (NOLOCK) ON LASTUSE.LASTUSERLOGIN = EMPLTABLE.ALTNUM
			                        AND EMPLTABLE.DATAAREAID = N'SPC' AND ALTNUM != '' 

                        ORDER BY Location,ASSETID ";
                    dt_Data = ConnectionClass.SelectSQL_SentServer(sql, IpServerConnectClass.Con803SRV);
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();

                    string sql24 = $@"
                        SELECT *
                        FROM(
                            SELECT  CONVERT(VARCHAR, CONVERT(varchar, LoginDate) + ' ' + CONVERT(VARCHAR, LoginTime), 25) AS LASTTIMEUPDATE, LoginPDAName AS BLUETOOTH,
                                    LoginBranch + ' ' + LoginBranchName AS BCH, LoginVersion AS LASTAPPVERSION, LoginId AS LASTUSERLOGIN, LoginName AS SPC_NAME,
                                    'SHOP24HRS' AS LASTAPPLICATION,
                                    ROW_NUMBER() OVER(PARTITION BY LoginPDAName ORDER BY LoginDate DESC, LoginTime DESC) AS ROWNUM

                            FROM    SHOP_COMPDANAMESHOP24HRS WITH(NOLOCK)
                            )TMP
                        WHERE ROWNUM = 1  ";
                    DataTable dt24 = ConnectionClass.SelectSQL_Main(sql24);

                    for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
                    {
                        if (RadGridView_ShowHD.Rows[i].Cells["LASTUSERLOGIN"].Value.ToString() == "")
                        {
                            DataRow[] dr = dt24.Select($@" BLUETOOTH = '{RadGridView_ShowHD.Rows[i].Cells["ASSETID"].Value}' ");
                            if (dr.Length > 0)
                            {
                                RadGridView_ShowHD.Rows[i].Cells["LASTTIMEUPDATE"].Value = dr[0]["LASTTIMEUPDATE"].ToString();
                                RadGridView_ShowHD.Rows[i].Cells["LASTUSERLOGIN"].Value = dr[0]["LASTUSERLOGIN"].ToString();
                                RadGridView_ShowHD.Rows[i].Cells["SPC_NAME"].Value = dr[0]["SPC_NAME"].ToString();
                                RadGridView_ShowHD.Rows[i].Cells["LASTAPPLICATION"].Value = dr[0]["LASTAPPLICATION"].ToString();
                                RadGridView_ShowHD.Rows[i].Cells["LASTAPPVERSION"].Value = dr[0]["LASTAPPVERSION"].ToString();
                            }
                        }
                    }


                    this.Cursor = Cursors.Default;
                    break;
                case "16"://16 = รายงานการจัดสินค้าสาขาใหญ่
                    this.Cursor = Cursors.WaitCursor;
                    dt_Data = POClass.ReportMN_OrderSortProductSupc(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"));
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    if (dt_Data.Rows.Count == 0) MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูลในเงื่อนไขของวันที่ที่ระบุ.");
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }
        //13 = รายงานรายเดือนตามประเภทการสั่ง ตามบุคคล
        void SetCase13()
        {
            string type = "0";//รายเดือน
            if (radRadioButton_Barcode.CheckState == CheckState.Checked) type = "1"; //รายวัน

            string typePO = "";
            // year = "", if (radCheckBox_Dpt.Checked == true) year = radDropDownList_Dpt.SelectedValue.ToString();//ปี
            if (RadCheckBox_Branch.Checked == true) typePO = RadDropDownList_Branch.SelectedValue.ToString();//ประเภทสั่ง

            if (RadGridView_ShowHD.Columns.Count > 0) RadGridView_ShowHD.Columns.Clear();
            RadGridView_ShowHD.SummaryRowsTop.Clear(); RadGridView_ShowHD.DataSource = null;

            //create Columns
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLNAME", "ชื่อพนักงาน", 220)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM_DESC", "ชื่อแผนก", 250)));

            RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;
            RadGridView_ShowHD.Columns["EMPLNAME"].IsPinned = true;

            GridViewSummaryRowItem summaryRowItemCase13 = new GridViewSummaryRowItem();

            double intDay = 0;
            DataTable dtEmp, dtAll;
            DataTable MM = new DataTable();
            if (type == "0")
            {
                DateTime endDate = DateTime.Parse($@"{radDropDownList_Dpt.SelectedValue}-12-31");
                if (endDate > DateTime.Now) endDate = DateTime.Now;
                MM = DateTimeSettingClass.GetMonthYearByDate($@"{radDropDownList_Dpt.SelectedValue}-01-01", endDate.ToString("yyyy-MM-dd"));

                dtEmp = POClass.OrderWeb_ReportByTypePO("1", typePO, radDropDownList_Dpt.SelectedValue.ToString(), "", "");
                dtAll = POClass.OrderWeb_ReportByTypePO("2", typePO, radDropDownList_Dpt.SelectedValue.ToString(), "", "");

                for (int iC = 0; iC < MM.Rows.Count; iC++)
                {
                    string headColumeName = MM.Rows[iC]["ThaiMonth"].ToString();
                    string headColumeValue = $@"{ MM.Rows[iC]["MonthNumber"]}";

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("C_ALL" + headColumeValue, $@"{headColumeName}{Environment.NewLine}สั่งทั้งหมด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("C_OK" + headColumeValue, $@"{headColumeName}{Environment.NewLine}สั่งจัดได้", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMALL" + headColumeValue, $@"{headColumeName}{Environment.NewLine}ยอดรวมจัดได้", 100)));

                    DatagridClass.SetCellBackClolorByExpression("C_ALL" + headColumeValue, $@" C_ALL{headColumeValue} = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("C_OK" + headColumeValue, $@" C_ALL{headColumeValue} = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SUMALL" + headColumeValue, $@" C_ALL{headColumeValue} = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    summaryRowItemCase13.Add(new GridViewSummaryItem("C_ALL" + headColumeValue, "{0:#,##0.00}", GridAggregateFunction.Sum));
                    summaryRowItemCase13.Add(new GridViewSummaryItem("C_OK" + headColumeValue, "{0:#,##0.00}", GridAggregateFunction.Sum));
                    summaryRowItemCase13.Add(new GridViewSummaryItem("SUMALL" + headColumeValue, "{0:#,##0.00}", GridAggregateFunction.Sum));
                }

            }
            else
            {
                dtEmp = POClass.OrderWeb_ReportByTypePO("3", typePO, "", radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"));
                dtAll = POClass.OrderWeb_ReportByTypePO("4", typePO, "", radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"));

                intDay = (radDateTimePicker_End.Value.Date - radDateTimePicker_Begin.Value.Date).TotalDays + 1;

                for (int i = 0; i < intDay; i++)
                {
                    var pDate = radDateTimePicker_Begin.Value.AddDays(i).ToString("yyyy-MM-dd");
                    string headColume = pDate.Replace("-", "");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("C_ALL" + headColume, $@"{pDate}{Environment.NewLine}สั่งทั้งหมด", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("C_OK" + headColume, $@"{pDate}{Environment.NewLine}สั่งจัดได้", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUMALL" + headColume, $@"{pDate}{Environment.NewLine}ยอดรวมจัดได้", 100)));

                    DatagridClass.SetCellBackClolorByExpression("C_ALL" + headColume, $@" C_ALL{headColume} = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("C_OK" + headColume, $@" C_ALL{headColume} = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("SUMALL" + headColume, $@" C_ALL{headColume} = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                    summaryRowItemCase13.Add(new GridViewSummaryItem("C_ALL" + headColume, "{0:#,##0.00}", GridAggregateFunction.Sum));
                    summaryRowItemCase13.Add(new GridViewSummaryItem("C_OK" + headColume, "{0:#,##0.00}", GridAggregateFunction.Sum));
                    summaryRowItemCase13.Add(new GridViewSummaryItem("SUMALL" + headColume, "{0:#,##0.00}", GridAggregateFunction.Sum));
                }
            }

            RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemCase13);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            RadGridView_ShowHD.DataSource = dtEmp;

            if (dtEmp.Rows.Count == 0) { this.Cursor = Cursors.Default; return; }
            if (dtAll.Rows.Count == 0) { this.Cursor = Cursors.Default; return; }

            for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++)
            {
                string emp = RadGridView_ShowHD.Rows[iR].Cells["EMPLID"].Value.ToString();// พนักงาน

                if (type == "0")
                {
                    for (int iCC = 0; iCC < MM.Rows.Count; iCC++)
                    {
                        string headColumeValue = MM.Rows[iCC]["MonthNumber"].ToString();
                        DataRow[] drAll = dtAll.Select($@" EMPLID = '{emp}'  AND  MONTH = '{headColumeValue}' ");

                        double iAll = 0, iOK = 0, iSum = 0;

                        if (drAll.Length > 0)
                        {
                            iAll = Double.Parse(drAll[0]["C_ALL"].ToString());
                            iOK = Double.Parse(drAll[0]["C_OK"].ToString());
                            iSum = Double.Parse(drAll[0]["SUMALL"].ToString());
                        }

                        RadGridView_ShowHD.Rows[iR].Cells["C_ALL" + headColumeValue].Value = iAll;
                        RadGridView_ShowHD.Rows[iR].Cells["C_OK" + headColumeValue].Value = iOK;
                        RadGridView_ShowHD.Rows[iR].Cells["SUMALL" + headColumeValue].Value = iSum;
                    }
                }
                else
                {
                    for (int iC = 0; iC < intDay; iC++)
                    {
                        string headColumeValue = radDateTimePicker_Begin.Value.AddDays(iC).ToString("yyyy-MM-dd");
                        DataRow[] drAll = dtAll.Select($@" EMPLID = '{emp}'  AND  MONTH = '{headColumeValue}' ");

                        double iAll = 0, iOK = 0, iSum = 0;

                        if (drAll.Length > 0)
                        {
                            iAll = Double.Parse(drAll[0]["C_ALL"].ToString());
                            iOK = Double.Parse(drAll[0]["C_OK"].ToString());
                            iSum = Double.Parse(drAll[0]["SUMALL"].ToString());
                        }

                        RadGridView_ShowHD.Rows[iR].Cells["C_ALL" + headColumeValue.Replace("-", "")].Value = iAll;
                        RadGridView_ShowHD.Rows[iR].Cells["C_OK" + headColumeValue.Replace("-", "")].Value = iOK;
                        RadGridView_ShowHD.Rows[iR].Cells["SUMALL" + headColumeValue.Replace("-", "")].Value = iSum;
                    }
                }

            }

            this.Cursor = Cursors.Default;
        }
        //12 = รายงานการใช้งานเครื่องโปรแกรม Container รายเดือน
        void SetCase12()
        {
            string year = "";
            if (radCheckBox_Dpt.Checked == true) year = radDropDownList_Dpt.SelectedValue.ToString();//ปี

            DataTable dtDeviceName = PO_Class.OrderWeb_MachineCountLine("0", year);
            DataTable dtCountAll = PO_Class.OrderWeb_MachineCountLine("2", year);


            if (RadGridView_ShowHD.Columns.Count > 0) RadGridView_ShowHD.Columns.Clear();
            RadGridView_ShowHD.SummaryRowsTop.Clear(); RadGridView_ShowHD.DataSource = null;

            //create Columns
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ASSETID", "ชื่ออุปกรณ์", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SERIALNUM", "S/N", 150)));

            RadGridView_ShowHD.Columns["NUM"].IsPinned = true;
            RadGridView_ShowHD.Columns["DESCRIPTION"].IsPinned = true;
            RadGridView_ShowHD.Columns["ASSETID"].IsPinned = true;
            RadGridView_ShowHD.Columns["SERIALNUM"].IsPinned = true;

            DateTime endDate = DateTime.Parse($@"{year}-12-31");
            if (endDate > DateTime.Now) endDate = DateTime.Now;

            DataTable MM = DateTimeSettingClass.GetMonthYearByDate($@"{year}-01-01", endDate.ToString("yyyy-MM-dd"));
            for (int iC = 0; iC < MM.Rows.Count; iC++)
            {
                string headColumeName = MM.Rows[iC]["ThaiMonth"].ToString();
                string headColumeValue = $@"{ MM.Rows[iC]["MonthNumber"]}";

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM" + headColumeValue, $@"{headColumeName}", 100)));
                DatagridClass.SetCellBackClolorByExpression($@"SUM{headColumeValue}", $@" SUM{headColumeValue} = 0 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
            }


            RadGridView_ShowHD.DataSource = dtDeviceName;

            for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++)
            {
                string emp = RadGridView_ShowHD.Rows[iR].Cells["ASSETID"].Value.ToString();// ชื่อเครื่อง

                for (int iCC = 0; iCC < MM.Rows.Count; iCC++)
                {
                    string headColumeValue = MM.Rows[iCC]["MonthNumber"].ToString();
                    DataRow[] drAll = dtCountAll.Select($@" DeviceName = '{emp}'  AND  MM = '{headColumeValue}' ");

                    double iAll = 0;
                    if (drAll.Length > 0) iAll = Double.Parse(drAll[0]["COUNT_ROW"].ToString());

                    RadGridView_ShowHD.Rows[iR].Cells["SUM" + headColumeValue].Value = iAll;
                }
            }

            this.Cursor = Cursors.Default;
        }
        //11 = Check Order
        void SetCase11()
        {
            if (_sItemID == "")
            {
                string bchID = "", dptID = "";
                if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();
                if (radCheckBox_Dpt.Checked == true) dptID = radDropDownList_Dpt.SelectedValue.ToString();
                string pCaseOrder = "0";// "ทั้งหมด"
                if (radRadioButton_Barcode.CheckState == CheckState.Checked) pCaseOrder = "1";//"เฉพาะสาขาสั่ง"

                dt_Data = POClass.Report_DetailMNPOBySPC_TRACKINGID(pCaseOrder, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), radDateTimePicker_End.Value.ToString("yyyy-MM-dd"), bchID, dptID, "", "");
                RadGridView_ShowHD.DataSource = dt_Data;
                dt_Data.AcceptChanges();
                this.Cursor = Cursors.Default;
            }
            else
            {
                dt_Data = POClass.Report_DetailMNPOBySPC_TRACKINGID("0", DateTime.Now.AddDays(-120).ToString("yyyy-MM-dd"), DateTime.Now.AddDays(0).ToString("yyyy-MM-dd"), _sBchID, "", _sItemID, _sInventDIM);
                RadGridView_ShowHD.DataSource = dt_Data;
                dt_Data.AcceptChanges();
                this.Cursor = Cursors.Default;
            }

        }
        //10 = รายงานประเมินการจัดบิลไม่ได้ ตามพนักงานจัดบิล
        void SetCase10()
        {
            string year = "";
            if (radCheckBox_Dpt.Checked == true) year = radDropDownList_Dpt.SelectedValue.ToString();//ปี

            DataTable dtWorkerName = POClass.OrderWeb_ReportPerformanceMonth("0", year);
            DataTable dtCountAll = POClass.OrderWeb_ReportPerformanceMonth("2", year);
            DataTable dtCountNo = POClass.OrderWeb_ReportPerformanceMonth("1", year);

            if (RadGridView_ShowHD.Columns.Count > 0) RadGridView_ShowHD.Columns.Clear();
            RadGridView_ShowHD.SummaryRowsTop.Clear(); RadGridView_ShowHD.DataSource = null;

            //create Columns
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนก", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนก", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WorkerId", "รหัสพนักงาน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 220)));

            RadGridView_ShowHD.Columns["DIMENSION"].IsPinned = true;
            RadGridView_ShowHD.Columns["DESCRIPTION"].IsPinned = true;
            RadGridView_ShowHD.Columns["WorkerId"].IsPinned = true;
            RadGridView_ShowHD.Columns["SPC_NAME"].IsPinned = true;

            DateTime endDate = DateTime.Parse($@"{year}-12-31");
            if (endDate > DateTime.Now) endDate = DateTime.Now;

            DataTable MM = DateTimeSettingClass.GetMonthYearByDate($@"{year}-01-01", endDate.ToString("yyyy-MM-dd"));
            for (int iC = 0; iC < MM.Rows.Count; iC++)
            {
                string headColumeName = MM.Rows[iC]["ThaiMonth"].ToString();
                string headColumeValue = $@"{ MM.Rows[iC]["MonthNumber"]}";

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SUM" + headColumeValue, $@"{headColumeName}{Environment.NewLine}รายการรวม", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("OK" + headColumeValue, $@"{headColumeName}{Environment.NewLine}รายการจัดได้", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("NO" + headColumeValue, $@"{headColumeName}{Environment.NewLine}รายการจัดไม่ได้", 100)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("PerOK" + headColumeValue, $@"{headColumeName}{Environment.NewLine}% จัดได้", 100)));
                //RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("POK" + headColumeValue, headColumeName)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetRight("PerNO" + headColumeValue, $@"{headColumeName}{Environment.NewLine}% จัดไม่ได้", 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("PNO" + headColumeValue, headColumeName)));

                DatagridClass.SetCellBackClolorByExpression($@"PerNO{headColumeValue}", $@" PNO{headColumeValue} = 1 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression($@"PerNO{headColumeValue}", $@" PNO{headColumeValue} = 2 ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);
                DatagridClass.SetCellBackClolorByExpression($@"PerNO{headColumeValue}", $@" PNO{headColumeValue} = 3 ", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);
            }



            RadGridView_ShowHD.DataSource = dtWorkerName;

            #region "OLD"

            //for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++)
            //{
            //    string emp = RadGridView_ShowHD.Rows[iR].Cells["WorkerId"].Value.ToString();
            //    for (int iCC = 0; iCC < MM.Rows.Count; iCC++)
            //    {
            //        string headColumeValue = MM.Rows[iCC]["MonthNumber"].ToString();
            //        DataRow[] drOK = dtCountOK.Select($@" WorkerId = '{emp}'  AND  MONTH = '{headColumeValue}' ");
            //        DataRow[] drNO = dtCountNo.Select($@" WorkerId = '{emp}'   AND  MONTH = '{headColumeValue}'  ");
            //        double iOK = 0; double iNO = 0;
            //        string p;
            //        int sta = 0;

            //        if (radRadioButton_Barcode.CheckState == CheckState.Checked)
            //        {
            //            if (drOK.Length > 0) iOK = Double.Parse(drOK[0]["COUNT_BARCODE"].ToString());
            //            if (drNO.Length > 0) iNO = (iOK - Double.Parse(drNO[0]["COUNT_BARCODE"].ToString())); else iNO = iOK;
            //        }
            //        else
            //        {
            //            if (drOK.Length > 0) iOK = Double.Parse(drOK[0]["COUNT_BARCODE"].ToString());
            //            if (drNO.Length > 0) iNO = Double.Parse(drNO[0]["COUNT_BARCODE"].ToString());
            //        }

            //        if (iOK > 0)
            //        {
            //            double percent = ((100 * iNO) / iOK);
            //            if (percent < 50) sta = 4;
            //            if (percent == 0) sta = 3;
            //            if (percent > 50) sta = 1;
            //            p = percent.ToString("N2");
            //        }
            //        else
            //        {
            //            p = "-"; sta = 2;
            //        }

            //        RadGridView_ShowHD.Rows[iR].Cells[headColumeValue].Value = p;
            //        RadGridView_ShowHD.Rows[iR].Cells["A" + headColumeValue].Value = sta;
            //    }
            #endregion

            for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++)
            {
                string emp = RadGridView_ShowHD.Rows[iR].Cells["WorkerId"].Value.ToString();// พนักงาน

                for (int iCC = 0; iCC < MM.Rows.Count; iCC++)
                {
                    string headColumeValue = MM.Rows[iCC]["MonthNumber"].ToString();
                    DataRow[] drAll = dtCountAll.Select($@" WorkerId = '{emp}'  AND  MONTH = '{headColumeValue}' ");
                    DataRow[] drNO = dtCountNo.Select($@" WorkerId = '{emp}'   AND  MONTH = '{headColumeValue}'  ");

                    double iAll = 0, iOK, iNO = 0;
                    int staNO = 0;

                    if (drAll.Length > 0) iAll = Double.Parse(drAll[0]["COUNT_BARCODE"].ToString()); else staNO = 2;
                    if (drNO.Length > 0) iNO = Double.Parse(drNO[0]["COUNT_BARCODE"].ToString());
                    iOK = iAll - iNO;

                    string pOK = "-", pNO = "-";

                    if (iAll > 0)
                    {
                        double percentOK = ((100 * iOK) / iAll);
                        double percentNO = ((100 * iNO) / iAll);

                        if (percentNO > 50) staNO = 1;
                        if (percentNO == 0) staNO = 3;

                        pOK = percentOK.ToString("N2");
                        pNO = percentNO.ToString("N2");
                    }
                    RadGridView_ShowHD.Rows[iR].Cells["SUM" + headColumeValue].Value = iAll;
                    RadGridView_ShowHD.Rows[iR].Cells["OK" + headColumeValue].Value = iOK;
                    RadGridView_ShowHD.Rows[iR].Cells["NO" + headColumeValue].Value = iNO;

                    RadGridView_ShowHD.Rows[iR].Cells["PerOK" + headColumeValue].Value = pOK;
                    RadGridView_ShowHD.Rows[iR].Cells["PerNO" + headColumeValue].Value = pNO;
                    RadGridView_ShowHD.Rows[iR].Cells["PNO" + headColumeValue].Value = staNO;

                }
            }

            this.Cursor = Cursors.Default;
        }
        //9 = รายงานประเมินการจัดบิล ตามพนักงานจัดบิล
        void SetCase9()
        {
            string dateStart = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
            string dateEnd = radDateTimePicker_End.Value.ToString("yyyy-MM-dd");

            DataTable dtWorkerName = POClass.OrderWeb_ReportPerformance("2", dateStart, dateEnd);
            DataTable dtCountAll = POClass.OrderWeb_ReportPerformance("0", dateStart, dateEnd);
            DataTable dtCountNo = POClass.OrderWeb_ReportPerformance("1", dateStart, dateEnd);

            if (RadGridView_ShowHD.Columns.Count > 0) RadGridView_ShowHD.Columns.Clear();
            RadGridView_ShowHD.SummaryRowsTop.Clear(); RadGridView_ShowHD.DataSource = null;

            //create Columns
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "แผนก", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "แผนก", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WorkerId", "รหัสพนักงาน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 220)));

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BILLALL", "รายการทั้งหมด", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BILLOK", "จัดได้", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("BILLNO", "จัดไม่ได้", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("P_OK", "% จัดได้", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("P_NO", "% จัดไม่ได้", 90)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddVisible("STA", "STA")));

            RadGridView_ShowHD.Columns["DIMENSION"].IsPinned = true;
            RadGridView_ShowHD.Columns["DESCRIPTION"].IsPinned = true;

            DatagridClass.SetCellBackClolorByExpression($@"BILLNO", $@"P_NO > 30", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression($@"P_NO", $@"P_NO > 30", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression($@"BILLALL", $@"STA = 1", ConfigClass.SetColor_SkyPastel(), RadGridView_ShowHD);

            RadGridView_ShowHD.DataSource = dtWorkerName;
            for (int iC = 0; iC < RadGridView_ShowHD.Rows.Count; iC++)
            {
                string emp = RadGridView_ShowHD.Rows[iC].Cells["WorkerId"].Value.ToString();
                DataRow[] drOK = dtCountAll.Select($@" WorkerId = '{emp}'   ");
                double iOK = 0;
                if (drOK.Length > 0) iOK = Double.Parse(drOK[0]["COUNT_BARCODE"].ToString());

                double iNO = 0;
                DataRow[] drNO = dtCountNo.Select($@" WorkerId = '{emp}' ");
                if (drNO.Length > 0) iNO = Double.Parse(drNO[0]["COUNT_BARCODE"].ToString());

                double percentOK = ((100 * iOK) / (iOK + iNO));
                int sta = 0;
                if (percentOK == 100) sta = 1;


                RadGridView_ShowHD.Rows[iC].Cells["BILLALL"].Value = (iOK + iNO).ToString("N2");
                RadGridView_ShowHD.Rows[iC].Cells["BILLOK"].Value = iOK.ToString("N2");
                RadGridView_ShowHD.Rows[iC].Cells["BILLNO"].Value = iNO.ToString("N2");
                RadGridView_ShowHD.Rows[iC].Cells["P_OK"].Value = percentOK.ToString("N2");
                RadGridView_ShowHD.Rows[iC].Cells["P_NO"].Value = ((100 * iNO) / (iOK + iNO)).ToString("N2");
                RadGridView_ShowHD.Rows[iC].Cells["STA"].Value = sta.ToString();

            }
            this.Cursor = Cursors.Default;
        }
        //8 =  รายการสินค้าที่จัดทั้งหมด (ได้หรือไม่ได้)
        void SetCase8()
        {

            string dptID = ""; if (radCheckBox_Dpt.Checked == true) dptID = radDropDownList_Dpt.SelectedValue.ToString();
            string staSelect = "1"; if (radRadioButton_Photo.CheckState == CheckState.Checked) staSelect = "0";
            string bchID = ""; if (RadCheckBox_Branch.Checked == true) bchID = RadDropDownList_Branch.SelectedValue.ToString();
            string itemID = "", inventDim = "";
            string date1 = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
            string date2 = radDateTimePicker_End.Value.ToString("yyyy-MM-dd");

            if (_sItemID != "")
            {
                staSelect = "1";
                dptID = _sDptID;
                bchID = _sBchID;
                itemID = _sItemID;
                inventDim = _sInventDIM;
                date1 = DateTime.Now.AddDays(-120).ToString("yyyy-MM-dd");
                date2 = DateTime.Now.AddDays(0).ToString("yyyy-MM-dd");
            }

            this.Cursor = Cursors.WaitCursor;
            dt_Data = POClass.OrderWeb_ReportItemPacking(staSelect, dptID, date1, date2, bchID, itemID, inventDim);
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();

            if (radCheck_NotTask.CheckState == CheckState.Checked)//แสดงรูปสินค้า
            {
                RadGridView_ShowHD.Columns["Image1"].IsVisible = true;
                RadGridView_ShowHD.TableElement.RowHeight = 120;
                for (int i = 0; i < dt_Data.Rows.Count; i++)
                {
                    RadGridView_ShowHD.MasterTemplate.Rows[i].Cells["Image1"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(dt_Data.Rows[i]["ORDERIMAGEPATH"].ToString());
                }
            }
            else
            {
                RadGridView_ShowHD.Columns["Image1"].IsVisible = false; RadGridView_ShowHD.TableElement.RowHeight = 30;
            }

            if (staSelect == "0")
            {
                RadGridView_ShowHD.Columns["ITEMBARCODE"].IsVisible = false;
                RadGridView_ShowHD.Columns["SPC_ITEMNAME"].IsVisible = false;
                RadGridView_ShowHD.Columns["QTY"].IsVisible = false;
                RadGridView_ShowHD.Columns["UNITID"].IsVisible = false;
            }
            else
            {
                RadGridView_ShowHD.Columns["ITEMBARCODE"].IsVisible = true;
                RadGridView_ShowHD.Columns["SPC_ITEMNAME"].IsVisible = true;
                RadGridView_ShowHD.Columns["QTY"].IsVisible = true;
                RadGridView_ShowHD.Columns["UNITID"].IsVisible = true;
            }
            this.Cursor = Cursors.Default;
        }
        //จำนวนออเดอร์แต่ละจัดบิล
        void SetCase7()
        {
            string dateStart = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
            string dateEnd = radDateTimePicker_End.Value.ToString("yyyy-MM-dd");

            string dpt = radDropDownList_Dpt.SelectedValue.ToString();
            DataTable dtWorkerName = POClass.OrderWeb_ReportByWorker("0", dpt, dateStart, dateEnd);
            DataTable dtNum = POClass.OrderWeb_ReportByWorker("1", dpt, dateStart, dateEnd);
            DataTable dtWorker = POClass.OrderWeb_ReportByWorker("2", dpt, dateStart, dateEnd);

            if (RadGridView_ShowHD.Columns.Count > 0) RadGridView_ShowHD.Columns.Clear();
            RadGridView_ShowHD.SummaryRowsTop.Clear(); RadGridView_ShowHD.DataSource = null;

            double intDay = (radDateTimePicker_End.Value.Date - radDateTimePicker_Begin.Value.Date).TotalDays + 1;

            //create Columns
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 180)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 80)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 220)));

            RadGridView_ShowHD.Columns["NUM"].IsPinned = true;
            RadGridView_ShowHD.Columns["EMPLID"].IsPinned = true;

            GridViewSummaryRowItem summaryRowItemCase7 = new GridViewSummaryRowItem();
            for (int i = 0; i < intDay; i++)
            {
                var pDate = radDateTimePicker_Begin.Value.AddDays(i).ToString("yyyy-MM-dd");
                string headColume = pDate.Replace("-", "");

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(headColume, pDate, 100)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight($@"P{headColume}", "คิดเป็น %", 100)));

                DatagridClass.SetCellBackClolorByExpression($@"P{headColume}", $@"P{headColume} = 0", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                summaryRowItemCase7.Add(new GridViewSummaryItem(headColume, "{0:#,##0.00}", GridAggregateFunction.Sum));
                summaryRowItemCase7.Add(new GridViewSummaryItem($@"P{headColume}", "{0:#,##0.00}", GridAggregateFunction.Sum));
            }

            RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemCase7);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            RadGridView_ShowHD.DataSource = dtWorkerName;

            for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++)
            {
                RadGridView_ShowHD.Rows[iR].Cells["NUM"].Value = radDropDownList_Dpt.SelectedItem[0].ToString();
                string emp = RadGridView_ShowHD.Rows[iR].Cells["EMPLID"].Value.ToString();

                for (int iC = 0; iC < intDay; iC++)
                {
                    string pDate = radDateTimePicker_Begin.Value.AddDays(iC).ToString("yyyy-MM-dd");
                    string headColume = pDate.Replace("-", "");

                    double NumSum = 0;
                    DataRow[] drNumSum = dtNum.Select($@" DATEPICKING = '{pDate}' AND DIMENSION = '{radDropDownList_Dpt.SelectedValue}' ");
                    if (drNumSum.Length > 0) NumSum = Double.Parse(drNumSum[0]["COUNT_BARCODE"].ToString());

                    DataRow[] drWorker = dtWorker.Select($@" WorkerId = '{emp}' AND DATEPICKING = '{pDate}' ");
                    double c = 0;
                    if (drWorker.Length > 0) c = double.Parse(drWorker[0]["COUNT_BARCODE"].ToString());

                    double p = 0;
                    if (NumSum != 0) p = ((c * 100) / NumSum);

                    RadGridView_ShowHD.Rows[iR].Cells[headColume].Value = c.ToString("N2");
                    RadGridView_ShowHD.Rows[iR].Cells["P" + headColume].Value = p.ToString("N2");
                }
            }

            this.Cursor = Cursors.Default;
        }
        //การเปิดออเดอร์แบบไม่อ้างอิงรายการสั่ง
        void SetCase6_WebNoOrder()
        {
            this.Cursor = Cursors.WaitCursor;
            dt_Data = POClass.OrderWeb_NoOrder(radDropDownList_Dpt.SelectedValue.ToString()); //BranchClass.GetBranchAll("'1','4'", "'1'");
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //หาผลลัพธ์ตามเงื่อนไขที่ต้องการ
        double GetSumQty(DataTable dtsum, string pCon)
        {
            if (dtsum.Rows.Count == 0) return 0;
            DataRow[] sall = dtsum.Select(pCon);
            double sAll = 0;
            for (int i = 0; i < sall.Length; i++)
            {
                sAll += double.Parse(sall[i]["SUMALL"].ToString());
            }

            return sAll;
        }
        //ตั้งค่าสีคอลัมน์ และผลรวมทั้งหมด
        public void SetColorColumn()
        {
            DatagridClass.SetCellBackClolorByExpression("SUMALL", "SUMALL <> '0' ", ConfigClass.SetColor_PurplePastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("SUM1", "SUM1 > '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("SUM11", "SUM11 > '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("SUM12", "SUM12 > '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("SUM13OK", "SUM13OK > '0'", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("SUM13Not", "SUM13Not > '0'", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);

            DatagridClass.SetCellBackClolorByExpression("PSUM1", "PSUM1 > '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("PSUM11", "PSUM11 > '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("PSUM12", "PSUM12 > '0' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("PSUM13OK", "PSUM13OK > '0.00' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("PSUM13OK", "SUM13OK > '0' AND PSUM13OK < '30.00' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("PSUM13Not", "PSUM13Not > '0.00' ", ConfigClass.SetColor_YellowPastel(), RadGridView_ShowHD);
            DatagridClass.SetCellBackClolorByExpression("PSUM13Not", "SUM13Not > '0' AND PSUM13Not >= '70.00' ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

            GridViewSummaryItem summaryItemA = new GridViewSummaryItem("SUMALL", "{0:n2}", GridAggregateFunction.Sum);
            GridViewSummaryItem summaryItemB = new GridViewSummaryItem("SUM1", "{0:n2}", GridAggregateFunction.Sum);
            GridViewSummaryItem summaryItemC = new GridViewSummaryItem("SUM11", "{0:n2}", GridAggregateFunction.Sum);
            GridViewSummaryItem summaryItemD = new GridViewSummaryItem("SUM12", "{0:n2}", GridAggregateFunction.Sum);
            GridViewSummaryItem summaryItemE = new GridViewSummaryItem("SUM13OK", "{0:n2}", GridAggregateFunction.Sum);
            GridViewSummaryItem summaryItemF = new GridViewSummaryItem("SUM13Not", "{0:n2}", GridAggregateFunction.Sum);
            GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { summaryItemA, summaryItemB, summaryItemC, summaryItemD, summaryItemE, summaryItemF };
            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadDateTimePicker_Begin_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }

        private void RadDateTimePicker_End_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_Begin, radDateTimePicker_End);
        }
        #endregion

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            _sBchID = "";
            _sInventDIM = "";
            _sItemID = "";
            _sDptID = "";
            SetDGV_HD();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }

        private void RadCheckBox_Dpt_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Dpt.Checked == true) radDropDownList_Dpt.Enabled = true; else radDropDownList_Dpt.Enabled = false;
        }
        //Doument
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
        //Excel
        private void RadButtonElement_Excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            string T = "NoSelect";
            switch (_pTypeReport)
            {
                case "0":
                    T = DatagridClass.ExportExcelGridView("เช็คออเดอร์ ", RadGridView_ShowHD, "1");
                    break;
                case "1":
                    T = DatagridClass.ExportExcelGridView("เช็คออเดอร์ ", RadGridView_ShowHD, "1");
                    break;
                case "2":
                    T = DatagridClass.ExportExcelGridView("สถานะออร์เดอร์รวม [สินค้าที่สั่งผ่านรูป] ", RadGridView_ShowHD, "1");
                    break;
                case "3":
                    T = DatagridClass.ExportExcelGridView("สถานะออร์เดอร์ตามแผนก [สินค้าที่สั่งผ่านรูป] ", RadGridView_ShowHD, "1");
                    break;
                case "4":
                    T = DatagridClass.ExportExcelGridView("จำนวนออเดอร์ตามมินิมาร์ท [สินค้าที่สั่งผ่านรูป] ", RadGridView_ShowHD, "1");
                    break;
                case "5":
                    T = DatagridClass.ExportExcelGridView("การจัดสินค้าตามแผนก [อ้างอิงการสั่ง]", RadGridView_ShowHD, "1");
                    break;
                case "6":
                    T = DatagridClass.ExportExcelGridView("การจัดสินค้าตามแผนก [ไม่อ้างอิงการสั่ง]", RadGridView_ShowHD, "1");
                    break;
                case "7":
                    T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");
                    break;
                case "8":
                    T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");
                    break;
                case "9":
                    T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");
                    break;
                case "10":
                    T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");
                    break;
                case "11":
                    T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");
                    break;
                default:
                    break;
            }
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //RePrint
        void RePrintBill(string docno, string inventTo, string inVentToName, string inVentFrom, string emplID, string emplName)
        {
            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                int cContainer = CheckPrintContainer(SystemClass.SystemDptID);

                printDocument1.PrintController = printController;
                printDocument1.PrinterSettings = printDialog1.PrinterSettings;

                DataRow[] route = dtRoute.Select($@"  ACCOUNTNUM = '{inventTo}' ");
                if (route.Length == 0) routeBch = "";
                else routeBch = route[0]["ROUTEID"].ToString() + Environment.NewLine + route[0]["NAME"].ToString();
                bchID = inventTo;
                bchName = inVentToName;
                whPrint = inVentFrom;
                barcodePrint = docno;
                emplIDPacking = emplID;
                emplNamePacking = emplName;

                if (staContainer == "1")
                {
                    for (int iC = 0; iC < cContainer; iC++)
                    {
                        printDocument1.Print();
                    }
                }
                else { printDocument1.Print(); }
                //printDocument1.Print();
            }
            this.DialogResult = DialogResult.Yes;
        }
        //ดับเบิ้ลคลิก
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            if ((_pTypeOpen == "SHOP") && (_pTypeReport == "8"))//Copy Order 
            {
                if (RadGridView_ShowHD.CurrentRow.Cells["STACOPY"].Value.ToString() == "0") return;

                InputData inputData = new InputData("0", RadGridView_ShowHD.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                    "จำนวน", RadGridView_ShowHD.CurrentRow.Cells["UNITID"].Value.ToString());
                if (inputData.ShowDialog() != DialogResult.Yes) return;

                string transID = SystemClass.SystemUserID + "_" + DateTime.Now.ToString("yyyy-MM-dd HH-MM-ss").Replace("-", "").Replace(" ", "");
                string sqlIN = $@"
                INSERT INTO SHOP_WEB_PDTTRANSORDER (
                        [TRANSID],[BRANCHID],[BRANCHNAME],[EMPLID],[EMPLNAME],[TRANSDATE],
                        [ITEMBARCODE],[ITEMID],[INVENTDIMID],
                        [QTY],[UNITID],
                        [FACTOR],[SPC_ITEMNAME],[SPC_PICKINGLOCATIONID],
                        [TARGETDIMENSION],[CREATEDBY],
                        [ORDERREMARK],[ORDERIMAGEPATH],
                        [REFNO2],[VENDERID] )
                 VALUES ('{transID}','{SystemClass.SystemBranchID}','{SystemClass.SystemBranchName}','{SystemClass.SystemUserID_M}','{SystemClass.SystemUserName}',CONVERT(VARCHAR,GETDATE(),23),
                        '{RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value}','{RadGridView_ShowHD.CurrentRow.Cells["ITEMID"].Value}','{RadGridView_ShowHD.CurrentRow.Cells["INVENTDIMID"].Value}',
                        '{inputData.pInputData}','{RadGridView_ShowHD.CurrentRow.Cells["UNITID"].Value}',
                        '{Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["FACTOR"].Value.ToString())}','{RadGridView_ShowHD.CurrentRow.Cells["SPC_ITEMNAME"].Value}','{RadGridView_ShowHD.CurrentRow.Cells["INVENTLOCATIONIDFROM"].Value}',
                        '{RadGridView_ShowHD.CurrentRow.Cells["TARGETDIMENSION"].Value}','{SystemClass.SystemUserID}',
                        '{RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value}','{RadGridView_ShowHD.CurrentRow.Cells["ORDERIMAGEPATH"].Value}',
                        'CopyOrderPC','{RadGridView_ShowHD.CurrentRow.Cells["VENDERID"].Value}' )";

                MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(sqlIN));
                return;
            }

            if (_pTypeOpen == "SHOP") return;
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            if (_pTypeReport == "6")
            {
                if (e.Column.Name == "ROUTEID")
                {
                    string route = RadGridView_ShowHD.CurrentRow.Cells["ROUTEID"].Value.ToString();
                    for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count - 1; iR++)
                    {
                        if (RadGridView_ShowHD.Rows[iR].Cells["ROUTEID"].Value.ToString() == route) RadGridView_ShowHD.Rows[iR].Cells["C"].Value = "1";
                    }
                }
                else
                {
                    int iRows = Convert.ToInt32(RadGridView_ShowHD.CurrentRow.Cells["COUNT_BILL"].Value.ToString());
                    if (iRows == 0) return;

                    DataTable dtRePrint = POClass.OrderWeb_NoOrder(radDropDownList_Dpt.SelectedValue.ToString(), RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
                    if (dtRePrint.Rows.Count == 1)
                    {
                        RePrintBill(
                            dtRePrint.Rows[0]["ORDERNUM"].ToString(), dtRePrint.Rows[0]["INVENTLOCATIONIDTO"].ToString(),
                            dtRePrint.Rows[0]["INVENTLOCATIONIDTONAME"].ToString(), dtRePrint.Rows[0]["INVENTLOCATIONIDFROM"].ToString(),
                            dtRePrint.Rows[0]["EMPLID"].ToString(), dtRePrint.Rows[0]["SPC_NAME"].ToString());
                        return;
                    }
                    else
                    {
                        FormShare.ShowData.ShowDataDGV showDataDGV = new FormShare.ShowData.ShowDataDGV("4")
                        {
                            dtData = dtRePrint
                        };
                        if (showDataDGV.ShowDialog() != DialogResult.Yes) return;

                        DataRow[] drRePrint = dtRePrint.Select($@" ORDERNUM = '{showDataDGV.pID}' ");
                        if (drRePrint.Length == 0) return;

                        RePrintBill(
                           drRePrint[0]["ORDERNUM"].ToString(), drRePrint[0]["INVENTLOCATIONIDTO"].ToString(),
                           drRePrint[0]["INVENTLOCATIONIDTONAME"].ToString(), drRePrint[0]["INVENTLOCATIONIDFROM"].ToString(),
                           drRePrint[0]["EMPLID"].ToString(), drRePrint[0]["SPC_NAME"].ToString());
                        return;

                    }
                }
            }

            if ((_pTypeReport == "11") && (RadGridView_ShowHD.CurrentRow.Cells["STAORDER"].Value.ToString() == "ระบบจัดซื้อจัดสินค้าเอง"))
            {
                MNPO.TranferItem_BranchReciveAX frm = new MNPO.TranferItem_BranchReciveAX("8", "SUPC", "", "", "")
                {
                    Text = "เช็คสถานะสินค้าส่งมินิมาร์ท ระบบจัดสินค้าทั้งหมด.",
                    _sItemID = RadGridView_ShowHD.CurrentRow.Cells["ITEMID"].Value.ToString(),
                    _sInventDIM = RadGridView_ShowHD.CurrentRow.Cells["INVENTDIMID"].Value.ToString(),
                    _sBchID = RadGridView_ShowHD.CurrentRow.Cells["INVENTLOCATIONIDTO"].Value.ToString(),
                    _sDptID = RadGridView_ShowHD.CurrentRow.Cells["NUM"].Value.ToString(),
                    WindowState = FormWindowState.Maximized
                };
                if (frm.ShowDialog() == DialogResult.OK) { }
                return;
            }

            if (_pTypeReport != "5") return;

            switch (e.Column.Name)
            {
                case "Image1"://ดับเบิ้ลคลิกรูปใหญ่
                    System.Diagnostics.Process.Start(RadGridView_ShowHD.CurrentRow.Cells["ORDERIMAGEPATH"].Value.ToString());
                    break;
                case "BRANCHID":
                    string bchSelect = RadGridView_ShowHD.CurrentRow.Cells["BRANCHID"].Value.ToString();
                    //if (SystemClass.SystemComMinimart != "1")
                    //{
                    //    if (RadGridView_ShowHD.CurrentRow.Cells["TARGETDIMENSION"].Value.ToString() != SystemClass.SystemDptID) return;
                    //}
                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        if (RadGridView_ShowHD.Rows[i].Cells["BRANCHID"].Value.ToString() == bchSelect
                        && RadGridView_ShowHD.Rows[i].Cells["ORDERSTATUS"].Value.ToString() == "1")
                            RadGridView_ShowHD.Rows[i].Cells["C"].Value = "1";

                    }
                    break;
                case "TARGETDIMENSION": //เปลี่ยนแผนกจัดซื้อ
                    int a = CheckStatus();
                    if (a > 0) return;
                    string trandIdTarget = RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString();
                    InputPicking frm = new InputPicking("1", radDropDownList_Dpt.SelectedValue.ToString(), "");
                    DialogResult dr = frm.ShowDialog();
                    if (dr == DialogResult.Yes)
                    {
                        string newDim = frm.pInputPicking;
                        string result = PO_Class.OrderByWeb_UpdateTargetDim(trandIdTarget, newDim);
                        if (result != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(result);
                            return;
                        }
                        SetCase5_OrderByWeb();  //รีเฟรช  
                    }
                    break;
                case "ITEMBARCODE": //จับคู่บาร์โค้ด
                    int b = CheckStatus();
                    if (b > 0) return;
                    FormShare.InputData f = new FormShare.InputData("0", "ระบุบาร์โค้ด", "บาร์โค้ด", "");
                    DialogResult it = f.ShowDialog();
                    if (it == DialogResult.Yes)
                    {
                        string bchID = RadGridView_ShowHD.CurrentRow.Cells["BRANCHID"].Value.ToString();
                        DataTable result = ConnectionClass.SelectSQL_Main($@" POClass_GetDetailItembarcode_ForPO '{f.pInputData}','{bchID}' "); //ItembarcodeClass.GetItembarcode_ALLDeatil(newBarcode);
                        if (result.Rows.Count == 0)
                        {
                            MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                            return;
                        }

                        Itembarcode.ItembarcodeDetail_Order _itembarcodeDetailOrder = new Itembarcode.ItembarcodeDetail_Order(result, "0", "0", RadDropDownList_Branch.SelectedValue.ToString());
                        if (_itembarcodeDetailOrder.ShowDialog(this) == DialogResult.Yes)
                        {
                            //ระบุจำนวน
                            FormShare.InputData _inputdata = new FormShare.InputData("0",
                                _itembarcodeDetailOrder.sBarcode + "-" + _itembarcodeDetailOrder.sSpc_Name, "จำนวนที่ต้องการสั่ง", _itembarcodeDetailOrder.sUnitID)
                            { pInputData = "1.00" };
                            if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                            {
                                //Check Qty
                                if (Convert.ToDouble(_inputdata.pInputData) > 1000)
                                {
                                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่อนุญาติให้สั่งสินค้ามากกว่า 1000/รายการ ทุกกรณี"); return;
                                }
                                //ถ้าผ่าน Update

                                double qtyOrder = Convert.ToDouble(RadGridView_ShowHD.CurrentRow.Cells["QTY"].Value.ToString());
                                string trand = RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString();


                                string resultBarcode = PO_Class.OrderByWeb_MatchingTransOrder(trand, qtyOrder, Convert.ToDouble(_inputdata.pInputData), _itembarcodeDetailOrder);
                                if (resultBarcode != "")
                                {
                                    MsgBoxClass.MsgBoxShow_SaveStatus(resultBarcode);
                                    return;
                                }
                                SetCase5_OrderByWeb();  //รีเฟรช  
                            }
                            else return;
                        }
                    }

                    break;
                case "REFMNPO": //ปริ้น
                    string refmnpo = RadGridView_ShowHD.CurrentRow.Cells["REFMNPO"].Value.ToString();
                    //if (SystemClass.SystemComMinimart != "1")
                    //{
                    //    if (RadGridView_ShowHD.CurrentRow.Cells["TARGETDIMENSION"].Value.ToString() != SystemClass.SystemDptID) return;
                    //}
                    if (refmnpo != "")
                    {
                        RePrintBill(RadGridView_ShowHD.CurrentRow.Cells["REFMNPO"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["BRANCHID"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["BRANCHNAME"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["SPC_PICKINGLOCATIONID"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["EMPLIDPACKING"].Value.ToString(),
                                    RadGridView_ShowHD.CurrentRow.Cells["EMPLNAMEPACKING"].Value.ToString()
                                    );

                        this.DialogResult = DialogResult.Yes;

                    }
                    break;
                case "QTY": //ยกเลิกออเดอร์
                    int c = CheckStatus();
                    if (c > 0) return;
                    string trandIdTargetCancel = RadGridView_ShowHD.CurrentRow.Cells["TRANSID"].Value.ToString();
                    string nameItem = RadGridView_ShowHD.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString();
                    string nameEmpl = RadGridView_ShowHD.CurrentRow.Cells["EMPLNAMEORDER"].Value.ToString();
                    if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการลบรายการ{Environment.NewLine}{nameItem}{Environment.NewLine}สั่งโดย {nameEmpl}") == DialogResult.Yes)
                    {
                        string resultCancel = PO_Class.OrderByWeb_CancelOrder(trandIdTargetCancel);
                        if (resultCancel != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(resultCancel);
                            return;
                        }
                        SetCase5_OrderByWeb();  //รีเฟรช  
                    }
                    else
                    {
                        return;
                    }
                    break;
                case "ROUTEID":
                    string routeSelect = RadGridView_ShowHD.CurrentRow.Cells["ROUTEID"].Value.ToString();
                    //if (SystemClass.SystemComMinimart != "1")
                    //{
                    //    if (RadGridView_ShowHD.CurrentRow.Cells["TARGETDIMENSION"].Value.ToString() != SystemClass.SystemDptID) return;
                    //}

                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        if (RadGridView_ShowHD.Rows[i].Cells["ROUTEID"].Value.ToString() == routeSelect
                        && RadGridView_ShowHD.Rows[i].Cells["ORDERSTATUS"].Value.ToString() == "1")
                            RadGridView_ShowHD.Rows[i].Cells["C"].Value = "1";

                    }
                    break;
                case "SPC_WMSLocationId":
                    string WMS_Select = RadGridView_ShowHD.CurrentRow.Cells["SPC_WMSLocationId"].Value.ToString();
                    if (WMS_Select == "") return;
                    //if (SystemClass.SystemComMinimart != "1")
                    //{
                    //    if (RadGridView_ShowHD.CurrentRow.Cells["TARGETDIMENSION"].Value.ToString() != SystemClass.SystemDptID) return;
                    //}

                    for (int i = 0; i < dt_Data.Rows.Count; i++)
                    {
                        if (RadGridView_ShowHD.Rows[i].Cells["SPC_WMSLocationId"].Value.ToString() == WMS_Select
                        && RadGridView_ShowHD.Rows[i].Cells["ORDERSTATUS"].Value.ToString() == "1")
                            RadGridView_ShowHD.Rows[i].Cells["C"].Value = "1";

                    }
                    break;
                default:
                    break;
            }
        }
        //เปลี่ยนจัดซื้อให้โหลดสาขาใหม่
        private void RadDropDownList_Dpt_SelectedValueChanged(object sender, EventArgs e)
        {
            if (_pTypeReport == "5")
            {
                if (radDropDownList_Dpt.DataSource != null && radCheckBox_Dpt.Checked == true)
                {
                    DataRow[] iR = dt_DimensionPackIng.Select($@" DIMENSION = '{radDropDownList_Dpt.SelectedValue}' ");
                    if (iR.Length > 0)
                    {
                        if (iR[0]["ISPACKINGALL"].ToString() == "1")//เฉพาะสินค้าที่สั่งผ่านบาร์โค้ด
                        {
                            if (iR[0]["ISUSEINVENTAX"].ToString() == "0")//กรณีที่เอาคลังจาก AX
                            {
                                radRadioButton_Photo.Enabled = false; radRadioButton_Barcode.Enabled = false; radRadioButton_Photo.Text = "รูปภาพ+บาร์โค้ด";
                                radRadioButton_Photo.IsChecked = true; radRadioButton_Barcode.IsChecked = true; radRadioButton_Barcode.Visible = false;
                            }
                            else //กรณีที่เอาคลังจาก AX
                            {
                                radRadioButton_Photo.Enabled = true; radRadioButton_Barcode.Enabled = true; radRadioButton_Photo.Text = "รูปภาพ";
                                radRadioButton_Photo.IsChecked = true; radRadioButton_Barcode.IsChecked = false; radRadioButton_Barcode.Visible = true;
                            }

                        }
                        else//เฉพาะสินค้าที่สั่งผ่านรูป
                        {
                            radRadioButton_Photo.Enabled = true; radRadioButton_Photo.IsChecked = true; radRadioButton_Photo.Text = "รูปภาพ";
                            radRadioButton_Barcode.Enabled = false; radRadioButton_Barcode.IsChecked = false; radRadioButton_Barcode.Visible = false;
                        }
                    }
                }
            }
        }
        //Cell Click
        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {

            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }
            switch (_pTypeReport)
            {
                case "5":
                    if (e.Column.Name == "C")
                    {
                        //if (SystemClass.SystemComMinimart != "1")
                        //{
                        //    if (RadGridView_ShowHD.CurrentRow.Cells["TARGETDIMENSION"].Value.ToString() != SystemClass.SystemDptID) return;
                        //}
                        if (RadGridView_ShowHD.CurrentRow.Cells["ORDERSTATUS"].Value.ToString() == "1")
                        {
                            if (RadGridView_ShowHD.CurrentRow.Cells["C"].Value.ToString() == "1") RadGridView_ShowHD.CurrentRow.Cells["C"].Value = 0;
                            else RadGridView_ShowHD.CurrentRow.Cells["C"].Value = 1;
                        }

                    }
                    break;
                case "6":
                    if (e.Column.Name == "C")
                    {
                        if (RadGridView_ShowHD.CurrentRow.Cells["C"].Value.ToString() == "1") RadGridView_ShowHD.CurrentRow.Cells["C"].Value = 0;
                        else RadGridView_ShowHD.CurrentRow.Cells["C"].Value = 1;
                    }
                    break;
                default:
                    break;
            }
        }

        //print container
        private void RadButtonElement_PrintContainer_Click(object sender, EventArgs e)
        {
            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ปิดการใช้งานฟังก์ชั่นการพิมพ์ที่ไม่ระบุเลขที่ MNPT{Environment.NewLine}ถ้าต้องการเลข Container เพิ่ม ให้พิมพ์ในใบจัดแทน");
            return;
            //string dpt = radDropDownList_Dpt.SelectedValue.ToString();
            //InputPicking frm = new InputPicking("2", dpt, "");
            //DialogResult it = frm.ShowDialog();
            //if (it != DialogResult.Yes) return;

            //PrintDialog _dai = new PrintDialog();
            //if (_dai.ShowDialog(this) == DialogResult.OK)
            //{
            //    for (int i = 0; i < frm.pNumber; i++)
            //    {
            //        string numberContainer = Class.ConfigClass.GetMaxINVOICEID("", "", "", "13");
            //        string staSaveAX = AX_SendData.Save_WHSContainer(numberContainer, frm.pInputPicking2,"");
            //        if (staSaveAX != "")
            //        {
            //            MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถบันทึกข้อมูลได้{Environment.NewLine}ลองใหม่อีกครั้ง{staSaveAX}");
            //            return;
            //        }
            //        else
            //        {
            //            if (_dai.PrinterSettings.PrinterName.Contains("Godex"))
            //            {
            //                RawPrinterHelper.SendStringToPrinter(printDialog1.PrinterSettings.PrinterName, PrintClass.SetPrintBarcodeContainer_Goldex(numberContainer, frm.pInputPicking2).ToString());
            //            }
            //            else
            //            {
            //                printTM_Tag = numberContainer;
            //                printTM_EmplID = frm.pInputPicking2;
            //                printTM_EmplName = frm.pInputPicking3;
            //                printDocument2.PrintController = printController;
            //                printDocument2.PrinterSettings = _dai.PrinterSettings;
            //                printDocument2.Print();
            //            }
            //        }
            //    }
            //}
        }
        //print tag container
        private void PrintDocument2_PrintPage(object sender, PrintPageEventArgs e)
        {
            //PrintClass.SetPrintBarcodeContainer_TM(e, printTM_Tag, printTM_EmplID, printTM_EmplName);
        }
        //Check สำหรับพิมพ์ Container
        int CheckPrintContainer(string DptID)
        {
            int countContainer = 1;
            string staPrintContainer = "0";
            staContainer = "0";
            if (DptID == "D168") staPrintContainer = "1";
            if (staPrintContainer == "0")
            {
                InputData inputData = new InputData("0", "จำนวน Container ที่ต้องการในแต่ละสาขา [ต้องจำนวนเท่ากันทุกสาขา]", "ถ้าไม่ต้องการเลข Container ในบิล กดยกเลิก", "ใบ")
                { pInputData = "1" };
                if (inputData.ShowDialog(this) == DialogResult.Yes)
                {
                    countContainer = Convert.ToInt32(inputData.pInputData);
                    staContainer = "1";
                }
            }
            return countContainer;
        }
        // สร้างใบโอนย้าย ที่ไม่มีการสั่ง
        void ApvOrderNoOrder()
        {
            //code isbarcode 1
            DataTable dt_branch = new DataTable();
            dt_branch.Columns.Add("BRANCHID");
            dt_branch.Columns.Add("BRANCHNAME");

            for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
            {
                if (RadGridView_ShowHD.Rows[i].Cells["C"].Value.ToString() == "1")
                {
                    dt_branch.Rows.Add(RadGridView_ShowHD.Rows[i].Cells["BRANCH_ID"].Value.ToString(), RadGridView_ShowHD.Rows[i].Cells["BRANCH_NAME"].Value.ToString());
                }
            }

            if (dt_branch.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กรุณาเลือกสาขาก่อนกดสร้างใบจัด"); return;
            }

            InputPicking frm = new InputPicking("0", radDropDownList_Dpt.SelectedValue.ToString(), "0");
            DialogResult it = frm.ShowDialog();
            if (it != DialogResult.Yes) return;

            string location = frm.pInputPicking; //คลังที่เลือก
            string empTarget = frm.pInputPicking2; //รหัสพนักงานที่จะจ่ายบิลให้
                                                   //string empNameTarget = frm.pInputPicking3; //ชื่อพนักงานที่จะจ่ายบิลให้

            DataTable dt_Result = PO_Class.OrderByWeb_CreateOrderTableNoOrder(location, dt_branch, empTarget, frm.pInputPicking3, radDropDownList_Dpt.SelectedValue.ToString());
            if (dt_Result.Rows.Count > 0)
            {
                DialogResult result = printDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    int cContainer = CheckPrintContainer(radDropDownList_Dpt.SelectedValue.ToString());

                    for (int i = 0; i < dt_Result.Rows.Count; i++)
                    {
                        printDocument1.PrintController = printController;
                        printDocument1.PrinterSettings = printDialog1.PrinterSettings;

                        bchID = dt_Result.Rows[i]["BRANCHID"].ToString();
                        bchName = dt_Result.Rows[i]["BRANCHNAME"].ToString();
                        routeBch = dt_Result.Rows[i]["ROUTE"].ToString();
                        whPrint = dt_Result.Rows[i]["PICKINGLOCATIONID"].ToString();
                        barcodePrint = dt_Result.Rows[i]["ORDERNUM"].ToString();
                        emplIDPacking = dt_Result.Rows[i]["EMPLPACKING"].ToString();
                        emplNamePacking = dt_Result.Rows[i]["EMPLNAMEPACKING"].ToString();
                        //boxRuningPrint = 1.ToString() + "/" + (i + 1).ToString();
                        if (staContainer == "1")
                        {
                            for (int iC = 0; iC < cContainer; iC++)
                            {
                                printDocument1.Print();
                            }
                        }
                        else { printDocument1.Print(); }
                    }
                }
                this.DialogResult = DialogResult.Yes;
            }
            SetCase6_WebNoOrder();
        }
        //ปุ่มจ่ายบิล
        private void RadImage_EMPL_Click(object sender, EventArgs e)
        {
            if (_pTypeReport == "6") { ApvOrderNoOrder(); return; }
            if (_pTypeReport != "5") return;

            this.RadGridView_ShowHD.SortDescriptors.Expression = "BRANCHID,SPC_PICKINGLOCATIONID ASC";
            this.RadGridView_ShowHD.GroupDescriptors.Clear();

            DataTable dt_ForPacking = new DataTable();
            dt_ForPacking.Columns.Add("TRANSID");
            dt_ForPacking.Columns.Add("DIMENSION");
            dt_ForPacking.Columns.Add("BRANCHID");
            dt_ForPacking.Columns.Add("BRANCHNAME");
            dt_ForPacking.Columns.Add("PICKINGLOCATIONID");

            //code isbarcode 1
            DataTable dt_branch = new DataTable();
            dt_branch.Columns.Add("BRANCHID");
            dt_branch.Columns.Add("BRANCHNAME");
            dt_branch.Columns.Add("LOCATION");

            string CheckISUSEINVENTAX = "0";
            DataRow[] iR = dt_DimensionPackIng.Select($@" DIMENSION = '{radDropDownList_Dpt.SelectedValue}' ");
            if (iR.Length > 0) CheckISUSEINVENTAX = iR[0]["ISUSEINVENTAX"].ToString();

            string bchCheck = "";
            string InventCheck = "";
            for (int i = 0; i < RadGridView_ShowHD.Rows.Count; i++)
            {
                if (RadGridView_ShowHD.Rows[i].Cells["C"].Value.ToString() == "1")
                {
                    string bchIDPacking = RadGridView_ShowHD.Rows[i].Cells["BRANCHID"].Value.ToString();
                    string InventPacking = RadGridView_ShowHD.Rows[i].Cells["SPC_PICKINGLOCATIONID"].Value.ToString();

                    if (CheckISUSEINVENTAX == "0") InventPacking = "";

                    if (bchCheck != bchIDPacking || InventCheck != InventPacking)
                    {
                        DataRow[] dr = dt_branch.Select($@" BRANCHID = '{bchIDPacking}' AND LOCATION = '{InventPacking}' ");
                        if (dr.Length == 0) dt_branch.Rows.Add(bchIDPacking, RadGridView_ShowHD.Rows[i].Cells["BRANCHNAME"].Value.ToString(), InventPacking);
                    }


                    dt_ForPacking.Rows.Add(RadGridView_ShowHD.Rows[i].Cells["TRANSID"].Value.ToString(),
                                           RadGridView_ShowHD.Rows[i].Cells["TARGETDIMENSION"].Value.ToString(),
                                           bchIDPacking,
                                           RadGridView_ShowHD.Rows[i].Cells["BRANCHNAME"].Value.ToString(),
                                           InventPacking);

                    bchCheck = bchIDPacking;
                    InventCheck = InventPacking;
                }

            }

            if (dt_ForPacking.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กรุณาเลือกรายการก่อนกดจ่ายบิล"); return;
            }

            string checkBarcode = "0";

            if (radRadioButton_Barcode.IsChecked == true)
            {
                //if ((radDropDownList_Dpt.SelectedValue.ToString() == "D005") || (radDropDownList_Dpt.SelectedValue.ToString() == "D101")) checkBarcode = "0"; else checkBarcode = "1";
                if (CheckISUSEINVENTAX == "0") checkBarcode = "0"; else checkBarcode = "1";
            }

            InputPicking frm = new InputPicking("0", radDropDownList_Dpt.SelectedValue.ToString(), checkBarcode);
            DialogResult it = frm.ShowDialog();
            if (it != DialogResult.Yes) return;

            string location = frm.pInputPicking; //คลังที่เลือก
            string empTarget = frm.pInputPicking2; //รหัสพนักงานที่จะจ่ายบิลให้
            string empNameTarget = frm.pInputPicking3; //ชื่อพนักงานที่จะจ่ายบิลให้


            DataTable dt_Result = PO_Class.OrderByWeb_CreateOrderTable(checkBarcode, dt_branch, dt_ForPacking, empTarget, location, empNameTarget);
            if (dt_Result.Rows.Count > 0)
            {
                //กรณีต้องการพิมพ์บิล
                DialogResult result = printDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    int cContainer = CheckPrintContainer(radDropDownList_Dpt.SelectedValue.ToString());
                    for (int i = 0; i < dt_Result.Rows.Count; i++)
                    {
                        printDocument1.PrintController = printController;
                        printDocument1.PrinterSettings = printDialog1.PrinterSettings;


                        bchID = dt_Result.Rows[i]["BRANCHID"].ToString();
                        bchName = dt_Result.Rows[i]["BRANCHNAME"].ToString();

                        //DataRow[] route = dtRoute.Select($@"  ACCOUNTNUM = '{bchID}' ");
                        //if (route.Length == 0) routeBch = "";
                        //else routeBch = route[0]["ROUTEID"].ToString() + Environment.NewLine + route[0]["NAME"].ToString();
                        routeBch = dt_Result.Rows[i]["ROUTE"].ToString();

                        whPrint = dt_Result.Rows[i]["PICKINGLOCATIONID"].ToString();
                        barcodePrint = dt_Result.Rows[i]["ORDERNUM"].ToString();
                        emplIDPacking = dt_Result.Rows[i]["EMPLPACKING"].ToString();
                        emplNamePacking = dt_Result.Rows[i]["EMPLNAMEPACKING"].ToString();

                        //boxRuningPrint = 1.ToString() + "/" + (i + 1).ToString();
                        if (staContainer == "1")
                        {
                            for (int iC = 0; iC < cContainer; iC++)
                            {
                                printDocument1.Print();
                            }
                        }
                        else { printDocument1.Print(); }
                    }
                }
            }
            this.DialogResult = DialogResult.Yes;

            SetCase5_OrderByWeb();  //รีเฟรช  
        }
        //Find OrderByWeb
        void SetCase5_OrderByWeb()
        {
            string start_Date = radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd");
            string end_Date = radDateTimePicker_End.Value.ToString("yyyy-MM-dd");

            string bch = "";
            if (RadCheckBox_Branch.Checked == true) bch = RadDropDownList_Branch.SelectedValue.ToString();

            string targetDim = "";
            if (radCheckBox_Dpt.Checked == true) targetDim = radDropDownList_Dpt.SelectedValue.ToString();

            string ordersta = "0";//1 เฉพาะยังไม่จ่าย
            if (radCheck_NotTask.Checked == true) ordersta = "1";

            string staALl = "0";
            //if (_pTypeOpen == "SUPC")
            //{
            DataRow[] iR = dt_DimensionPackIng.Select($@" DIMENSION = '{radDropDownList_Dpt.SelectedValue}' ");
            if ((iR[0]["ISPACKINGALL"].ToString() == "1") && (iR[0]["ISUSEINVENTAX"].ToString() == "0")) staALl = "2"; // รูป+บาร์โค้ด
            else if (radRadioButton_Barcode.IsChecked == true) staALl = "1"; // บาร์โค้ด
                                                                             //}

            dt_Data = POClass.OrderByWeb_GetDataOrder(targetDim, bch, ordersta, start_Date, end_Date, staALl, iR[0]["STAPACKING_MN"].ToString());
            RadGridView_ShowHD.Columns["Image1"].IsVisible = true;
            RadGridView_ShowHD.TableElement.RowHeight = 120;
            RadGridView_ShowHD.DataSource = dt_Data;
            dt_Data.AcceptChanges();
            if (dt_Data.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ค้นหา ลองใหม่อีกครั้ง");
                return;
            }

            for (int i = 0; i < dt_Data.Rows.Count; i++)
            {
                RadGridView_ShowHD.MasterTemplate.Rows[i].Cells["Image1"].Value = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(dt_Data.Rows[i]["ORDERIMAGEPATH"].ToString());
            }
        }
        //Check STatus
        int CheckStatus()
        {
            int a = 0;
            if (RadGridView_ShowHD.CurrentRow.Cells["ORDERSTATUS"].Value.ToString() != "1") { a += 1; } //ดักออเดอร์ที่จ่ายบิลแล้ว
            //if (SystemClass.SystemComMinimart != "1")
            //{
            //    //if (RadGridView_ShowHD.CurrentRow.Cells["TARGETDIMENSION"].Value.ToString() != SystemClass.SystemDptID) { a += 1; } //ดักออเดอร์ที่ไม่ใช่แผนกตัวเอง
            //}
            return a;
        }
        //ปริ้น
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Var_Print_MNPT_MNPF var = new Var_Print_MNPT_MNPF
            {
                Docno = barcodePrint,
                BranchID = bchID,
                BranchName = bchName,
                InventLocationID = whPrint,
                Route = routeBch,
                BOX = "",
                EmplIDPacking = emplIDPacking,
                EmplNamePacking = emplNamePacking
            };
            PrintClass.Print_MNPT_MNPF("MNPT", e, var, staContainer, "0");
        }

    }
}
