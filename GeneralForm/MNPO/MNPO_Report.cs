﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.GeneralForm.MNPO
{
    public partial class MNPO_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        readonly string _pTypeOpen;
        //0 -  สั่งสินค้าผ่านมือถือ สรุป
        //1 -  สั่งสินค้าผ่านมือถือ ละเอียด

        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
        //Load
        public MNPO_Report(string pTypeReport, string pTypeOpen)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pTypeOpen = pTypeOpen;
        }
        //Load
        private void MNPO_Report_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";
            radButtonElement_add.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            radCheckBox_SelectAll.ButtonElement.Font = SystemClass.SetFontGernaral;

            if (_pTypeOpen == "SUPC")
            {
                DataTable dtBch = BranchClass.GetBranchAll("1", "1");
                radGridView_BCH.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_AddManual("STA", "/", 30));
                radGridView_BCH.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 70));
                radGridView_BCH.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อ", 120));
                radGridView_BCH.MasterTemplate.ShowRowHeaderColumn = false;
                radGridView_BCH.DataSource = dtBch;
                dtBch.AcceptChanges();
                DatagridClass.SetDefaultRadGridView(radGridView_BCH);
            }
            else
            {
                radGridView_BCH.Visible = false;
                radCheckBox_SelectAll.Visible = false;
            }

            RadButton_Search.ButtonElement.ShowBorder = true;

            radLabel_Detail.Visible = false; radLabel_Detail.Text = "";
            switch (_pTypeReport)
            {
                case "0":
                    radLabel_Detail.Visible = true;
                    radLabel_Detail.Text = "วันที่ที่ระบุ คือ วันที่ลงเวลาเข้างาน | สีแดง > ไม่มีจำนวนรายการ";
                    radDateTimePicker_D1.Visible = true;
                    radDateTimePicker_D2.Visible = false;

                    break;
                case "1":
                    radDateTimePicker_D1.Visible = true;
                    radDateTimePicker_D2.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 60)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TRANSDATE", "วันที่สั่ง", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("QTY", "จำนวน", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));

                    RadGridView_ShowHD.Columns["NUM"].IsPinned = true;
                    RadGridView_ShowHD.Columns["DESCRIPTION"].IsPinned = true;

                    radLabel_Detail.Visible = true;
                    radLabel_Detail.Text = "วันที่ที่ระบุ คือ วันที่สั่งสินค้า";
                    break;

                default:
                    break;
            }
            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            switch (_pTypeReport)
            {
                case "0":
                    Set0();
                    break;
                case "1":
                    Set1();
                    break;

                default:
                    break;
            }
        }
        //set Bch
        string SetBch()
        {
            string bch = "";
            for (int i = 0; i < radGridView_BCH.Rows.Count; i++)
            {
                if (radGridView_BCH.Rows[i].Cells["STA"].Value.ToString() == "1")
                {
                    bch += "'" + radGridView_BCH.Rows[i].Cells["BRANCH_ID"].Value.ToString() + "',";
                }
            }
            if (bch.Length > 0)
            {
                bch = bch.Substring(0, bch.Length - 1);
            }
            return bch;
        }
        void Set1()
        {
            this.Cursor = Cursors.WaitCursor;
            string bchID;
            if (_pTypeOpen == "SUPC") { bchID = SetBch(); } else { bchID = "'" + SystemClass.SystemBranchID + "'"; }

            if (bchID.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กำหนดให้ระบุการเลือกสาขาทุกครั้งก่อนการดึงข้อมูล" + Environment.NewLine +
                    "ระบุสาขาให้เรียบร้อยแล้วกดดึงข้อมูลใหม่อีกครั้ง");
                return;
            }

            //var pDateA = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");
            //var pDateB = radDateTimePicker_D2.Value.ToString("yyyy-MM-dd");
            dt_Data = POClass.Report_WebDetail(radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), bchID);
            //    ConnectionClass.SelectSQL_Main($@"
            //        SELECT	SHOP_BRANCH.BRANCH_ID AS NUM,SHOP_BRANCH.BRANCH_NAME AS DESCRIPTION,SHOP_WEB_PDTTRANSORDER.EMPLID,EMPLTABLE.SPC_NAME,
            //          HRPPartyJobTableRelationship.DESCRIPTION AS POSSITION,CONVERT(VARCHAR,TRANSDATE,23) AS TRANSDATE,
            //          SHOP_WEB_PDTTRANSORDER.ITEMBARCODE,SPC_ITEMNAME,QTY,UNITID
            //        FROM	SHOP_BRANCH WITH (NOLOCK)
            //          LEFT OUTER JOIN SHOP_WEB_PDTTRANSORDER WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.BRANCHID = SHOP_BRANCH.BRANCH_ID
            //            AND CONVERT(VARCHAR,TRANSDATE,23) BETWEEN '{pDateA}'
            //                        AND '{pDateB}'
            //                        AND ISBARCODEMATCHING = '1'
            //          INNER JOIN SHOP2013TMP.dbo.EMPLTABLE WITH(NOLOCK) ON SHOP_WEB_PDTTRANSORDER.EMPLID = EMPLTABLE.EMPLID AND EMPLTABLE.DATAAREAID = N'SPC'
            //          LEFT OUTER JOIN SHOP2013TMP.dbo.HRPPARTYPOSITIONTABLERELAT2226 WITH (NOLOCK)
            //            ON EMPLTABLE.EMPLID = HRPPARTYPOSITIONTABLERELAT2226.REFERENCE AND  DATEADD(HOUR, 7, HRPPARTYPOSITIONTABLERELAT2226.VALIDTODATETIME) >= CAST(GETDATE() AS DATE)
            //          LEFT OUTER JOIN SHOP2013TMP.dbo.HRPPartyJobTableRelationship with (nolock)  ON HRPPARTYPOSITIONTABLERELAT2226.JOBID = HRPPartyJobTableRelationship.JOBID  
            //        WHERE	SHOP_BRANCH.BRANCH_ID  IN ({bchID})
            //        ORDER BY   SHOP_BRANCH.BRANCH_ID,CONVERT(VARCHAR,TRANSDATE,23),SHOP_WEB_PDTTRANSORDER.EMPLID,HRPPartyJobTableRelationship.DESCRIPTION
            //");
            RadGridView_ShowHD.DataSource = dt_Data;

            this.Cursor = Cursors.Default;
        }
        //รายงานสรุป
        void Set0()
        {
            string bchID;
            if (_pTypeOpen == "SUPC") bchID = SetBch(); else bchID = "'" + SystemClass.SystemBranchID + "'";

            if (bchID.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("กำหนดให้ระบุการเลือกสาขาทุกครั้งก่อนการดึงข้อมูล" + Environment.NewLine +
                 "ระบุสาขาให้เรียบร้อยแล้วกดดึงข้อมูลใหม่อีกครั้ง");
                ClearTxt(); return;
            }

            this.Cursor = Cursors.WaitCursor;

            RadGridView_ShowHD.DataSource = null;
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                RadGridView_ShowHD.SummaryRowsTop.Clear();
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NUM", "แผนก", 60)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อแผนก", 150)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("EMPLID", "รหัสพนักงาน", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อพนักงาน", 170)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("POSSITION", "ตำแหน่ง", 250)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TimeIns", "เวลาเข้างาน", 190)));
            RadGridView_ShowHD.Columns["NUM"].IsPinned = true;
            RadGridView_ShowHD.Columns["DESCRIPTION"].IsPinned = true;

            double intDay = 2;

            string col_Express = "";
            //เพิ่มคอลัม
            for (int iDay = 0; iDay < intDay; iDay++)
            {
                string headColume = "A" + Convert.ToString(iDay);
                if (col_Express == "")
                {
                    col_Express += headColume;
                }
                else
                {
                    col_Express += "+" + headColume;
                }
            }
            //Sum Image
            GridViewDecimalColumn col = new GridViewDecimalColumn
            {
                Name = "SumRows",
                HeaderText = "รวมทั้งหมด",
                IsVisible = true,
                FormatString = "{0:n2}",
                Width = 80,
                Expression = col_Express
            };
            RadGridView_ShowHD.MasterTemplate.Columns.Add(col);

            GridViewSummaryItem summaryItemA = new GridViewSummaryItem("SumRows", "{0}", GridAggregateFunction.Sum)
            {
                FormatString = "{0:n2}"
            };
            summaryRowItem.Add(summaryItemA);


            int i_Column = 0;
            for (int iDay = 0; iDay < intDay; iDay++)
            {
                string headColume = "A" + Convert.ToString(iDay);
                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(headColume, pDate, 75)));

                this.RadGridView_ShowHD.Columns[headColume].ConditionalFormattingObjectList.Add(
                          new ExpressionFormattingObject("MyCondition2", headColume + " = 0 ", false)
                          { CellBackColor = ConfigClass.SetColor_Red() });

                GridViewSummaryItem summaryItem = new GridViewSummaryItem(headColume, "{0}", GridAggregateFunction.Sum)
                {
                    FormatString = "{0:n2}"
                };
                summaryRowItem.Add(summaryItem);

                i_Column += 1;
            }

            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
            this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            ////Data
            //DataTable dtData = EmplClass.GetEmployeeAllCheckTimeKeeper_ByDptID(" (" + bchID + ") ", " IN ",
            //        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
            //        " ");
            //Data
            DataTable dtData = Models.EmplClass.GetEmployeeAllCheckTimeKeeper_ByDptID($@" IN ({bchID}) ", radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"));

            //AND  case when ISNULL(HRPPartyJobTableRelationship.DESCRIPTION,'') like '%ผู้จัดการ%' then '02' else IVZ_HROMPositionTitle end = '02'
            RadGridView_ShowHD.DataSource = dtData;

            var pDateA = radDateTimePicker_D1.Value.AddDays(0).ToString("yyyy-MM-dd");
            var pDateB = radDateTimePicker_D1.Value.AddDays(1).ToString("yyyy-MM-dd");
            DataTable dtRows = ConnectionClass.SelectSQL_Main($@"
                    SELECT	EMPLID,CONVERT(VARCHAR,TRANSDATE,23) AS TRANSDATE,ISNULL(COUNT(TRANSID),0) AS i_ROWS
                    FROM	SHOP_BRANCH WITH (NOLOCK)
		                    LEFT OUTER JOIN SHOP_WEB_PDTTRANSORDER WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.BRANCHID = SHOP_BRANCH.BRANCH_ID
				                    AND CONVERT(VARCHAR,TRANSDATE,23) BETWEEN '{pDateA}'
                                    AND '{pDateB}'
                                    AND REFNO2 = 'Shop24HrsWeb' AND ORDERSTATUS != '0'  
                    WHERE	SHOP_BRANCH.BRANCH_STAOPEN IN ('1') AND SHOP_BRANCH.BRANCH_STA IN ('1')
                    GROUP BY	EMPLID,CONVERT(VARCHAR,TRANSDATE,23)
                    ORDER BY    EMPLID,CONVERT(VARCHAR,TRANSDATE,23)
            ");

            for (int iEmp = 0; iEmp < dtData.Rows.Count; iEmp++)
            {
                string fEmpID = dtData.Rows[iEmp]["EMPLID"].ToString();

                double r_A = 0, r_B = 0;
                if (dtRows.Rows.Count > 0)
                {
                    DataRow[] resultA = dtRows.Select($@"EMPLID = '{fEmpID}' AND TRANSDATE = '{pDateA}' ");
                    if (resultA.Length > 0)
                    { r_A = Convert.ToDouble(resultA[0]["i_ROWS"].ToString()); }

                    DataRow[] resultB = dtRows.Select($@"EMPLID = '{fEmpID}' AND TRANSDATE = '{pDateB}' ");
                    if (resultB.Length > 0)
                    { r_B = Convert.ToDouble(resultB[0]["i_ROWS"].ToString()); }
                }

                RadGridView_ShowHD.Rows[iEmp].Cells["A0"].Value = r_A;
                RadGridView_ShowHD.Rows[iEmp].Cells["A1"].Value = r_B;

            }
            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
            radDateTimePicker_D1.Value = DateTime.Now.AddDays(-1);
            radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);

            if (_pTypeOpen == "SUPC")
            {
                for (int i = 0; i < radGridView_BCH.Rows.Count; i++)
                {
                    if (radGridView_BCH.Rows[i].Cells["STA"].Value.ToString() == "1")
                    {
                        radGridView_BCH.Rows[i].Cells["STA"].Value = "0";
                    }
                }
            }

            RadGridView_ShowHD.DataSource = dt_Data;
            RadButton_Search.Focus();

        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }

        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }

        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายละเอียดข้อมูล", RadGridView_ShowHD, "2");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //SetFont
        #region "SetFont"
        private void RadGridView_BCH_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFontGernaral;
        }

        private void RadGridView_BCH_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_BCH_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_BCH_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //Cell Choose
        private void RadGridView_BCH_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
            switch (e.Column.Name)
            {
                case "STA":
                    if (true)
                    {
                        if (radGridView_BCH.CurrentRow.Cells["STA"].Value.ToString() == "1")
                        {
                            radGridView_BCH.CurrentRow.Cells["STA"].Value = "0";
                        }
                        else
                        {
                            radGridView_BCH.CurrentRow.Cells["STA"].Value = "1";
                        }
                    }
                    break;
                default: break;
            }
        }
        //select All BCH
        private void RadCheckBox_SelectAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_SelectAll.CheckState == CheckState.Checked)
            {
                for (int i = 0; i < radGridView_BCH.Rows.Count; i++)
                {
                    radGridView_BCH.Rows[i].Cells["STA"].Value = "1";
                }
                radCheckBox_SelectAll.Text = "เอาทุกสาขาออก";
            }
            else
            {
                for (int i = 0; i < radGridView_BCH.Rows.Count; i++)
                {
                    radGridView_BCH.Rows[i].Cells["STA"].Value = "0";
                }
                radCheckBox_SelectAll.Text = "เลือกทุกสาขา";
            }
        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
    }
}
