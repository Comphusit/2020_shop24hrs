﻿namespace PC_Shop24Hrs.GeneralForm.MNPO
{
    partial class InputPicking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputPicking));
            this.radButton_Save = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Head1 = new Telerik.WinControls.UI.RadLabel();
            this.RadDropDownList_Location = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_Name1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Name2 = new Telerik.WinControls.UI.RadLabel();
            this.RadDropDownList_Emp = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel_Head2 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_InputData = new Telerik.WinControls.UI.RadTextBox();
            this.radCheckBox_D094 = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Location)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Emp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_InputData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_D094)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Save
            // 
            this.radButton_Save.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Save.Location = new System.Drawing.Point(103, 171);
            this.radButton_Save.Name = "radButton_Save";
            this.radButton_Save.Size = new System.Drawing.Size(92, 32);
            this.radButton_Save.TabIndex = 1;
            this.radButton_Save.Text = "บันทึก";
            this.radButton_Save.ThemeName = "Fluent";
            this.radButton_Save.Click += new System.EventHandler(this.RadButton_Save_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Text = "บันทึก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Save.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Save.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(199, 171);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(92, 32);
            this.radButton_Cancel.TabIndex = 2;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).CanFocus = true;
            // 
            // radLabel_Head1
            // 
            this.radLabel_Head1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Head1.Location = new System.Drawing.Point(101, 98);
            this.radLabel_Head1.Name = "radLabel_Head1";
            this.radLabel_Head1.Size = new System.Drawing.Size(127, 19);
            this.radLabel_Head1.TabIndex = 24;
            this.radLabel_Head1.Text = "กำหนดคลังจัดสินค้า";
            // 
            // RadDropDownList_Location
            // 
            this.RadDropDownList_Location.DropDownAnimationEnabled = false;
            this.RadDropDownList_Location.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Location.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Location.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Location.Location = new System.Drawing.Point(101, 123);
            this.RadDropDownList_Location.Name = "RadDropDownList_Location";
            this.RadDropDownList_Location.Size = new System.Drawing.Size(233, 25);
            this.RadDropDownList_Location.TabIndex = 48;
            this.RadDropDownList_Location.Text = "radDropDownList1";
            // 
            // radLabel_Name1
            // 
            this.radLabel_Name1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Name1.Location = new System.Drawing.Point(12, 129);
            this.radLabel_Name1.Name = "radLabel_Name1";
            this.radLabel_Name1.Size = new System.Drawing.Size(32, 19);
            this.radLabel_Name1.TabIndex = 25;
            this.radLabel_Name1.Text = "คลัง";
            // 
            // radLabel_Name2
            // 
            this.radLabel_Name2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Name2.Location = new System.Drawing.Point(12, 58);
            this.radLabel_Name2.Name = "radLabel_Name2";
            this.radLabel_Name2.Size = new System.Drawing.Size(59, 19);
            this.radLabel_Name2.TabIndex = 26;
            this.radLabel_Name2.Text = "พนักงาน";
            // 
            // RadDropDownList_Emp
            // 
            this.RadDropDownList_Emp.DropDownAnimationEnabled = false;
            this.RadDropDownList_Emp.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.RadDropDownList_Emp.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadDropDownList_Emp.ForeColor = System.Drawing.Color.Blue;
            this.RadDropDownList_Emp.Location = new System.Drawing.Point(101, 54);
            this.RadDropDownList_Emp.Name = "RadDropDownList_Emp";
            this.RadDropDownList_Emp.Size = new System.Drawing.Size(233, 25);
            this.RadDropDownList_Emp.TabIndex = 49;
            this.RadDropDownList_Emp.Text = "radDropDownList1";
            this.RadDropDownList_Emp.SelectedValueChanged += new System.EventHandler(this.RadDropDownList_Emp_SelectedValueChanged);
            // 
            // radLabel_Head2
            // 
            this.radLabel_Head2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Head2.Location = new System.Drawing.Point(103, 29);
            this.radLabel_Head2.Name = "radLabel_Head2";
            this.radLabel_Head2.Size = new System.Drawing.Size(103, 19);
            this.radLabel_Head2.TabIndex = 50;
            this.radLabel_Head2.Text = "กำหนดพนักงาน";
            // 
            // radTextBox_InputData
            // 
            this.radTextBox_InputData.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radTextBox_InputData.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_InputData.Location = new System.Drawing.Point(101, 124);
            this.radTextBox_InputData.MaxLength = 30;
            this.radTextBox_InputData.Name = "radTextBox_InputData";
            this.radTextBox_InputData.Size = new System.Drawing.Size(233, 25);
            this.radTextBox_InputData.TabIndex = 51;
            this.radTextBox_InputData.Text = "0054450";
            this.radTextBox_InputData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_InputData_KeyDown);
            this.radTextBox_InputData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_InputData_KeyPress);
            // 
            // radCheckBox_D094
            // 
            this.radCheckBox_D094.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radCheckBox_D094.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_D094.Location = new System.Drawing.Point(216, 29);
            this.radCheckBox_D094.Name = "radCheckBox_D094";
            this.radCheckBox_D094.Size = new System.Drawing.Size(95, 19);
            this.radCheckBox_D094.TabIndex = 53;
            this.radCheckBox_D094.Text = "พนักงาน PC";
            this.radCheckBox_D094.CheckStateChanged += new System.EventHandler(this.RadCheckBox_D094_CheckStateChanged);
            // 
            // InputPicking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(390, 225);
            this.Controls.Add(this.radCheckBox_D094);
            this.Controls.Add(this.radLabel_Head2);
            this.Controls.Add(this.RadDropDownList_Emp);
            this.Controls.Add(this.radLabel_Name2);
            this.Controls.Add(this.radLabel_Name1);
            this.Controls.Add(this.RadDropDownList_Location);
            this.Controls.Add(this.radLabel_Head1);
            this.Controls.Add(this.radButton_Save);
            this.Controls.Add(this.radButton_Cancel);
            this.Controls.Add(this.radTextBox_InputData);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputPicking";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ระบุข้อมูลจัด";
            this.Load += new System.EventHandler(this.InputPicking_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Location)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Name2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDropDownList_Emp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_InputData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_D094)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Save;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private Telerik.WinControls.UI.RadLabel radLabel_Head1;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Location;
        private Telerik.WinControls.UI.RadLabel radLabel_Name1;
        private Telerik.WinControls.UI.RadLabel radLabel_Name2;
        private Telerik.WinControls.UI.RadDropDownList RadDropDownList_Emp;
        private Telerik.WinControls.UI.RadLabel radLabel_Head2;
        private Telerik.WinControls.UI.RadTextBox radTextBox_InputData;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_D094;
    }
}
