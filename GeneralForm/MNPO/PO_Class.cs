﻿//CheckOK
using PC_Shop24Hrs.Controllers;
using System;
using System.Collections;
using System.Data;
using System.Net;

namespace PC_Shop24Hrs.GeneralForm.MNPO
{
    public class Var_MNPO_HD
    {
        public string MNPODocNo { get; set; } = "";
        public string MNPOUserCode { get; set; } = "";
        public string MNPOUserNameCode { get; set; } = "";
        public string MNPOBranch { get; set; } = "";
        public string MNPOBranchName { get; set; } = "";
        public string MNPOTypeOrder { get; set; } = "";
        public string MNPODptCode { get; set; } = "";
        public string MNPORemark { get; set; } = "";
        public string MNPOStaApvDoc { get; set; } = "";
        public string MNPOStaPrcDoc { get; set; } = "";
        public string MNPOWhoIn { get; set; } = "";
        public string MNPODateApv { get; set; } = "";
        public string MNPOTimeApv { get; set; } = "";
        public string MNPOWhoApv { get; set; } = "";
        public string MNPOWhoNameApv { get; set; } = "";
    }
    public class Var_MNPO_DT
    {
        public string MNPODocNo { get; set; } = "";
        public int MNPOSeqNo { get; set; } = 1;
        public string MNPOItemID { get; set; } = "";
        public string MNPODimid { get; set; } = "";
        public string MNPOBarcode { get; set; } = "";
        public string MNPOName { get; set; } = "";
        public double MNPOQtyOrder { get; set; } = 0;
        public string MNPOUnitID { get; set; } = "";
        public double MNPOFactor { get; set; } = 0;
        public string MNPODATEPO { get; set; } = "";
        public string MNPODATEPR { get; set; } = "";
        public string MNPOINVENT { get; set; } = "";
        public string MNPOWhoIn { get; set; } = "";

    }
    //ตัวแปรสำหรับการสร้างเลข Cpntainer แบบไม่มีน้ำหนัก
    public class MsgRecive
    {
        public string Message { get; set; }
        public Boolean Success { get; set; }
    }
    class PO_Class
    {
        //save MNPO HD
        public static string Save_MNPOHD(Var_MNPO_HD varHD,string staContainer)//1 ไม่ใช้ Container 0 ใช้ Container
        {
            string sql = $@"
                    INSERT INTO SHOP_MNPO_HD
	                    ([MNPODocNo],[MNPOUserCode],[MNPOUserNameCode],
                        [MNPOBranch],[MNPOBranchName],[MNPOTypeOrder],[MNPODptCode],[MNPORemark],[MNPOTYPE_PCORPDA],
                        MNPOStaApvDoc,MNPOStaPrcDoc,
                        MNPOWhoIn,MNPODateApv,MNPOTimeApv,MNPOWhoApv,MNPOWhoNameApv,MNPODlvTerm)
                    VALUES ('{varHD.MNPODocNo}','{varHD.MNPOUserCode}','{varHD.MNPOUserNameCode}',
                        '{varHD.MNPOBranch}','{varHD.MNPOBranchName}','{varHD.MNPOTypeOrder}','{varHD.MNPODptCode}','{varHD.MNPORemark}','{SystemClass.SystemPcName}',
                        '{varHD.MNPOStaApvDoc}','{varHD.MNPOStaPrcDoc}',
                        '{varHD.MNPOWhoIn}','{varHD.MNPODateApv}','{varHD.MNPOTimeApv}','{varHD.MNPOWhoApv}','{varHD.MNPOWhoNameApv}','{staContainer}') ";
            return sql;
        }

        //save MNPO DT
        public static string Save_MNPODT(Var_MNPO_DT varDT)
        {
            string sql = $@"
                    INSERT INTO SHOP_MNPO_DT
                        ([MNPODocNo],[MNPOSeqNo],[MNPOItemID],[MNPODimid],[MNPOBarcode],[MNPOName],
                        [MNPOQtyOrder],[MNPOUnitID],[MNPOFactor],[MNPODATEPO],[MNPODATEPR],[MNPOINVENT],[MNPOWhoIn] ) 
                    VALUES ('{varDT.MNPODocNo}','{varDT.MNPOSeqNo}','{varDT.MNPOItemID}','{varDT.MNPODimid}','{varDT.MNPOBarcode}','{varDT.MNPOName}',
                       '{varDT.MNPOQtyOrder}','{varDT.MNPOUnitID}','{varDT.MNPOFactor}','{varDT.MNPODATEPO}','{varDT.MNPODATEPR}','{varDT.MNPOINVENT}','{varDT.MNPOWhoIn}') ";
            return sql;
        }
        //ค้นหา MNPO HD
        public static DataTable FindMNPOHD_ByBillID(string date1, string date2, string bchId, string prcDoc)
        {
            string conBch = "";
            if (bchId != "") conBch = $@" AND MNPOBranch = '{bchId}' ";

            string conPrcdoc = "";
            if (prcDoc == "1") conPrcdoc = " AND MNPOStaDoc = '1'  AND MNPOStaPrcDoc = '0' AND MNPOStaApvDoc = '0' ";

            string sqlSelect = $@"
                SELECT	MNPODOCNO AS DOCNO,CONVERT(VARCHAR,MNPODATE,23)  + ' ' + MNPOTime  AS DOCDATE,
		                MNPOBranch AS BRANCH_ID,MNPOBranchName AS BRANCH_NAME,
                        CASE MNPOTypeOrder WHEN '1' THEN 'สาขาเปิด [PDA]' WHEN '2' THEN 'สาขาเปิด [PC]'  ELSE MNPOTypeOrder END AS TypeOrder,
                        MNPOUserCode+' ' + MNPOUserNameCode AS WHOIDINS,
		                CASE WHEN MNPOStaDoc = '3' THEN '1' ELSE '0' END AS STADOC,MNPOStaPrcDoc AS STAPRCDOC,
		                MNPORemark AS RMK,
		                CASE MNPOStaPrcDoc WHEN '1' THEN MNPOWhoApv + ' '+ ISNULL(MNPOWhoNameApv,'')  
		                ELSE CASE WHEN MNPOStaDoc = '3' THEN MNPOWhoUp + ' '+ ISNULL(MNPOWhoUpName,'')  ELSE '' END  END   AS WHOIDUPD,MNPOTYPE_PCORPDA
                FROM	SHOP_MNPO_HD  WITH (NOLOCK)
                WHERE	CONVERT(VARCHAR,MNPODATE,23) BETWEEN '{date1}'  AND '{date2}'  {conPrcdoc} {conBch}
                ORDER BY MNPODOCNO DESC
            ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ค้นหา MNPO DT
        public static DataTable FindMNPODT_ByBillID(string docno)
        {
            string sqlSelect = $@"
                SELECT	MNPOBarcode AS ITEMBARCODE,MNPOName AS SPC_ITEMNAME,MNPOQtyOrder AS QtyOrder,
		                MNPOUnitID AS UNITID,MNPODATEPR AS Purchase,MNPODATEPO AS POLast
                FROM	SHOP_MNPO_DT  WITH (NOLOCK)
                WHERE	MNPODOCNO = '{docno}'
                ORDER BY MNPOSeqNo ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //ค้นหาจำนวนรายการสั่งผ่าน web เฉพาะที่สั่งผ่านบาร์โค้ดเท่านั้น
        public static DataTable FindMNPO_ByWeb(string date1, string date2, string bchID)
        {
            string conBch = "";
            if (bchID != "") conBch = $@"  AND BRANCH_ID = '{bchID}' ";
            string sqlSelect = $@"
                    SELECT	BRANCH_ID,BRANCH_NAME,ISNULL(COUNT(TRANSID),0) AS i_ROWS
                    FROM	SHOP_BRANCH WITH (NOLOCK)
		                    LEFT OUTER JOIN SHOP_WEB_PDTTRANSORDER WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.BRANCHID = SHOP_BRANCH.BRANCH_ID
				                    AND CONVERT(VARCHAR,TRANSDATE,23) BETWEEN  '{date1}'  AND '{date2}'  AND ISBARCODEMATCHING = '1'
                                    AND REFNO2 = 'Shop24HrsWeb' AND ORDERSTATUS != '0' 
                    WHERE	SHOP_BRANCH.BRANCH_STAOPEN IN ('1') AND SHOP_BRANCH.BRANCH_STA IN ('1')
                            {conBch}
                    GROUP BY  BRANCH_ID,BRANCH_NAME
                    ORDER BY BRANCH_ID,BRANCH_NAME 
                ";
            return ConnectionClass.SelectSQL_Main(sqlSelect);
        }
        //แสดงวันที่
        public static string FindDate(string date1, string date2, string casePj)
        {
            string message = "";
            if (casePj == "2")
            {
                message = "SELECT	*	FROM	@TDATE";
            }
            if (casePj == "3")
            {
                message = $@"
                     ; WITH
                     DPT         AS(SELECT[DIMENSION],[DESCRIPTION]  FROM [SHOP_WEB_CONFIGTARGETDIMS] WITH (NOLOCK)    WHERE ISACTIVE = 1),
                     DATE_DPT AS(SELECT* FROM    DPT CROSS JOIN @TDATE)
                     SELECT TRANSDATE, DIMENSION, DESCRIPTION FROM DATE_DPT
                    ";
            }
            string sqlSelect = $@"
                    DECLARE	@TDATE TABLE (TRANSDATE NVARCHAR(50));
                    DECLARE	@STARTDATE DATE = CAST('{date1}'AS DATE);
                    DECLARE	@ENDDATE DATE = CAST('{date2}' AS DATE);
                    WHILE   @STARTDATE <= @ENDDATE
                    BEGIN
	                        INSERT INTO @TDATE VALUES (@STARTDATE);
	                        SET	@STARTDATE = DATEADD(DAY, 1, @STARTDATE);
                    END
                    {message}
            ";
            return sqlSelect;
        }
        //ค้นหาสถานะออเดอร์ที่สั่งผ่านรูป
        public static DataTable GetSumOrderWebImage(string pcase, string date1, string date2, string ptype) // สำหรับ13 กรณี 0 คือ จัดไม่ได้ 1 จัดได้ 
        {
            string sql;
            string conditionCase = "";
            string statusOrder = "";
            if (pcase != "") conditionCase = $@" AND [ORDERSTATUS] = '{pcase}' ";
            if ((pcase == "13") && (ptype != ""))
            {
                if (ptype == "0") statusOrder = $@"  AND B.[OrderStatus] NOT IN ('1','3','4') ";
                if (ptype == "1") statusOrder = $@"  AND B.[OrderStatus] = '1' "; //$@"  AND B.[OrderStatus] IN('1','3') ";
                sql = $@"
                SELECT	[TRANSDATE],COUNT([SHOP_WEB_PDTTRANSORDER].[ORDERSTATUS]) AS SUMALL
                FROM	[SHOP_WEB_PDTTRANSORDER] WITH (NOLOCK)
		                --LEFT JOIN [SHOP_WEB_PDTTRANSORDER_PICKING] WITH (NOLOCK) ON [SHOP_WEB_PDTTRANSORDER].TRANSID = [SHOP_WEB_PDTTRANSORDER_PICKING].TRANSID
		                LEFT OUTER JOIN SupcAndroid.dbo.ProductContainer_OrderImageRef  AS B WITH (NOLOCK) ON [SHOP_WEB_PDTTRANSORDER].TRANSID = B.TRANSID  {statusOrder}
                WHERE	CONVERT(VARCHAR,[TRANSDATE],23) BETWEEN '{date1}' AND '{date2}'
		                AND ([ISBYBARCODE] = '0' AND ISBARCODEMATCHING = '0') 
                        --AND [SHOP_WEB_PDTTRANSORDER].[ORDERSTATUS] != 0
                        --AND [SHOP_WEB_PDTTRANSORDER].[ORDERSTATUS] = '13'
                        AND ISNULL(B.TRANSID,'') != ''
                GROUP BY [TRANSDATE] ";
            }
            else
            {
                sql = $@"
                SELECT  [TRANSDATE],COUNT([ORDERSTATUS]) AS SUMALL
                FROM	[SHOP_WEB_PDTTRANSORDER] WITH (NOLOCK)
		                --LEFT JOIN [SHOP_WEB_PDTTRANSORDER_PICKING] WITH (NOLOCK) ON [SHOP_WEB_PDTTRANSORDER].TRANSID = [SHOP_WEB_PDTTRANSORDER_PICKING].TRANSID
                WHERE	CONVERT(VARCHAR,[TRANSDATE],23) BETWEEN '{date1}' AND '{date2}'
		                AND ([ISBYBARCODE] = '0' AND ISBARCODEMATCHING = '0') 
                        AND [ORDERSTATUS] != 0
                        {conditionCase}
                GROUP BY [TRANSDATE] ";
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }
        //ค้นหาออเดอร์ตามแผนก
        public static DataTable GetSumOrderByDMS(string pcase, string date1, string date2, string pGroupDate, string pMN, string ptype)// สำหรับ13 กรณี 0 คือ จัดไม่ได้ 1 จัดได้ 
        {

            string conditionCase = "";
            string sql;
            if (pcase != "") conditionCase = $@" AND [SHOP_WEB_PDTTRANSORDER].[ORDERSTATUS] = '{pcase}' ";

            string groupDateHD = " '' AS [TRANSDATE], ", groupDT = "";
            if (pGroupDate == "1")
            {
                groupDateHD = " [SHOP_WEB_PDTTRANSORDER].[TRANSDATE], ";
                groupDT = " [SHOP_WEB_PDTTRANSORDER].[TRANSDATE], ";
            }

            string brhSelect = "[SHOP_WEB_PDTTRANSORDER].[ISBYBARCODE], ";
            if (pMN == "1") brhSelect = " [SHOP_WEB_PDTTRANSORDER].[ISBYBARCODE],[SHOP_WEB_PDTTRANSORDER].BRANCHID, ";

            if ((pcase == "13") && (ptype != ""))
            {
                string statusOrder = "";
                if (ptype == "0") statusOrder = $@"  AND B.[OrderStatus] NOT IN ('1','3','4') ";
                if (ptype == "1") statusOrder = $@"  AND B.[OrderStatus] = '1' "; //$@"  AND B.[OrderStatus] IN ('1','3') ";
                sql = $@"
                SELECT  {groupDateHD}[TARGETDIMENSION] AS [DIMENSION],{brhSelect}
		                COUNT([TARGETDIMENSION]) AS SUMALL
                FROM	[SHOP_WEB_PDTTRANSORDER] WITH (NOLOCK)
		                --INNER JOIN [SHOP_WEB_PDTTRANSORDER_PICKING] WITH (NOLOCK) ON [SHOP_WEB_PDTTRANSORDER].TRANSID = [SHOP_WEB_PDTTRANSORDER_PICKING].TRANSID
                        LEFT OUTER JOIN SupcAndroid.dbo.ProductContainer_OrderImageRef  AS B WITH (NOLOCK) ON [SHOP_WEB_PDTTRANSORDER].TRANSID = B.TRANSID  {statusOrder}
                WHERE	[SHOP_WEB_PDTTRANSORDER].[ORDERSTATUS] != 0
                        AND [SHOP_WEB_PDTTRANSORDER].[TRANSDATE] BETWEEN '{date1}' AND '{date2}'
                        AND ISNULL(B.TRANSID,'') != ''
                GROUP BY {groupDT}{brhSelect}[TARGETDIMENSION] ";
            }
            else
            {
                sql = $@"
                SELECT  {groupDateHD}[SHOP_WEB_PDTTRANSORDER_PICKING].[DIMENSION],{brhSelect}
		                COUNT([SHOP_WEB_PDTTRANSORDER_PICKING].[DIMENSION]) AS SUMALL
                FROM	[SHOP_WEB_PDTTRANSORDER] WITH (NOLOCK)
		                INNER JOIN [SHOP_WEB_PDTTRANSORDER_PICKING] WITH (NOLOCK) ON [SHOP_WEB_PDTTRANSORDER].TRANSID = [SHOP_WEB_PDTTRANSORDER_PICKING].TRANSID
                WHERE	[SHOP_WEB_PDTTRANSORDER].[ORDERSTATUS] != 0
                        AND [SHOP_WEB_PDTTRANSORDER].[TRANSDATE] BETWEEN '{date1}' AND '{date2}'
                        {conditionCase}
                GROUP BY {groupDT}{brhSelect}[SHOP_WEB_PDTTRANSORDER_PICKING].[DIMENSION] ";
            }


            return ConnectionClass.SelectSQL_Main(sql);
        }
        //หาวันที่และรหัสแผนก
        public static string FindDateDMS(string casePj)
        {
            string select = "";
            string description = "";
            string conAddOn = "";
            if (casePj == "3")
            {
                select = "'' AS TRANSDATE,";
                description = "[DESCRIPTION],ISPACKINGALL";
            }
            if (casePj == "4")
            {
                select = "";
                description = "[DIMENSION]+CHAR(10)+[DESCRIPTION] AS [DESCRIPTION]";
            }
            if (casePj == "5")
            {
                select = "";
                description = "[DIMENSION]+' - '+[DESCRIPTION] AS [DESCRIPTION]";
                if (SystemClass.SystemBranchID != "MN000") conAddOn = "  AND STAPACKING_MN = '1' ";
            }

            string sqlSelect = $@"
                    SELECT  {select}[DIMENSION],{description}	
                    FROM    [SHOP_WEB_CONFIGTARGETDIMS]	WITH (NOLOCK)
                    WHERE   ISACTIVE = 1  {conAddOn}
            ";
            return sqlSelect;
        }
        ////แสดงออเดอร์เมนูจัดสินค้าผ่านรูป
        //public static DataTable OrderByWeb_GetFindorderbydim(string dimCode, string date1, string date2, string pMN, string check, string checkTask)//check 0 photo 1 Barcode
        //{
        //    //// 0:ยังไม่จ่าย  1:จ่ายบิลแล้ว 2:ทั้งหมด
        //    //string orderStatus = "AND ORDERSTATUS = 1"; //ยังไม่จ่าย
        //    //if (checkTask == "1") orderStatus = $@" AND ORDERSTATUS != 0 AND ORDERSTATUS != 1"; //จ่ายบิลแล้ว
        //    //if (checkTask == "2") orderStatus = $@" AND ORDERSTATUS != 0 "; // ทั้งหมด
        //    //string sql = $@"
        //    //                    ROW_NUMBER() OVER(ORDER BY BRANCHID, CREATEDBY, CREATEDDATE DESC) AS ROWNO
        //    //                    ,'0' AS C,[TRANSID],[TRANSDATE],[EMPLID],BRANCHID,
        //    //              IIF (ISNULL([BRANCHNAME], '') = '',BRANCH_NAME,[BRANCHNAME]) AS [BRANCHNAME],
        //    //              IIF (ISBYBARCODE = '0','',[ITEMBARCODE]) AS [ITEMBARCODE],
        //    //              IIF (ISBYBARCODE = '0',[SPC_ITEMNAME2],[SPC_ITEMNAME]) AS [SPC_ITEMNAME],
        //    //              IIF (ISBYBARCODE = '0',[QTY2],[QTY]) AS [QTY],
        //    //              IIF (ISBYBARCODE = '0',[UNITID2],[UNITID]) AS [UNITID],
        //    //              [SPC_PICKINGLOCATIONID],[TARGETDIMENSION],NAMEPACKINGALL,ORDERIMAGEPATH,ORDERSTATUS,SHOW_NAME
        //    //                    ,REFNO2,CREATEDDATE,REFMNPO,[EMPLID]+CHAR(10)+[EMPLNAME] AS [EMPLNAMEORDER],
        //    //                    [EMPIDPICKING]+CHAR(10)+[EMPNAMEPICKING] AS [EMPNAMEPICKING]
        //    //         ";
        //    //string sqlBarcode = $@"
        //    //            DECLARE @TARTGETDIM NVARCHAR(10) = N'{dimCode}';
        //    //            SELECT  {sql}
        //    //            FROM	SHOP_WEB_PDTTRANSORDER A WITH(NOLOCK)
        //    //                    LEFT JOIN SHOP_BRANCH WITH(NOLOCK) ON A.BRANCHID = SHOP_BRANCH.BRANCH_ID
        //    //                    LEFT JOIN ( SELECT SHOW_ID,SHOW_ID+':'+SHOW_NAME AS SHOW_NAME
        //    //                                FROM   SHOP_CONFIGBRANCH_GenaralDetail
        //    //                                WHERE  TYPE_CONFIG = '54'
        //    //                                       AND STA = '1') B ON B.SHOW_ID = A.ORDERSTATUS
        //    //                    LEFT OUTER JOIN (SELECT	DIMENSION AS DPT_PACKINGALL	 , DESCRIPTION AS NAMEPACKINGALL 
        //    //                                     FROM	SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) 
        //    //                                     WHERE	ISACTIVE = '1' AND ISPACKINGALL = '1')TMP_DIM ON TMP_DIM.DPT_PACKINGALL = A.TARGETDIMENSION
        //    //            WHERE   ISBYBARCODE IN ('1') 
        //    //                    {orderStatus}
        //    //                    AND TARGETDIMENSION = COALESCE(NULLIF(RTRIM(@TARTGETDIM), ''), TARGETDIMENSION)
        //    //                    AND TRANSDATE BETWEEN '{date1}' AND '{date2}'
        //    //                    AND BRANCHID = COALESCE(NULLIF(RTRIM('{pMN}'), ''), BRANCHID)
        //    //                    AND IIF (ISBYBARCODE = '0','1',ISNULL(DPT_PACKINGALL,'0')) != '0'
        //    //            ORDER BY BRANCHID, CREATEDBY, CREATEDDATE DESC
        //    //";
        //    //string sqlPhoto = $@"
        //    //            DECLARE @TARTGETDIM NVARCHAR(10) = N'{dimCode}';
        //    //            SELECT  {sql}
        //    //            FROM	SHOP_WEB_PDTTRANSORDER A WITH(NOLOCK)
        //    //                    LEFT JOIN SHOP_BRANCH WITH(NOLOCK) ON A.BRANCHID = SHOP_BRANCH.BRANCH_ID
        //    //              LEFT JOIN ( SELECT SHOW_ID,SHOW_ID+':'+SHOW_NAME AS SHOW_NAME
        //    //                          FROM   SHOP_CONFIGBRANCH_GenaralDetail
        //    //                          WHERE  TYPE_CONFIG = '54'
        //    //                        AND STA = '1') B ON B.SHOW_ID = A.ORDERSTATUS
        //    //                    LEFT OUTER JOIN (SELECT	DIMENSION AS DPT_PACKINGALL	 , DESCRIPTION AS NAMEPACKINGALL 
        //    //                                     FROM	SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) 
        //    //                                     WHERE	ISACTIVE = '1')TMP_DIM ON TMP_DIM.DPT_PACKINGALL = A.TARGETDIMENSION
        //    //            WHERE   ISBYBARCODE IN ('0') 
        //    //                    {orderStatus}
        //    //                    AND ISBARCODEMATCHING = 0
        //    //                    AND TARGETDIMENSION = COALESCE(NULLIF(RTRIM(@TARTGETDIM), ''), TARGETDIMENSION)
        //    //                    AND TRANSDATE BETWEEN '{date1}' AND '{date2}'
        //    //                    AND BRANCHID = COALESCE(NULLIF(RTRIM('{pMN}'), ''), BRANCHID)
        //    //            ORDER BY BRANCHID, CREATEDBY, CREATEDDATE DESC
        //    //";
        //    //if (check == "0") return ConnectionClass.SelectSQL_Main(sqlPhoto); else return ConnectionClass.SelectSQL_Main(sqlBarcode);

        //    // 0:ยังไม่จ่าย  1:จ่ายบิลแล้ว 2:ทั้งหมด
        //    string orderStatus = "AND ORDERSTATUS = 1"; //ยังไม่จ่าย
        //    if (checkTask == "1") orderStatus = $@" AND ORDERSTATUS != 0 AND ORDERSTATUS != 1"; //จ่ายบิลแล้ว
        //    if (checkTask == "2") orderStatus = $@" AND ORDERSTATUS != 0 "; // ทั้งหมด
        //    string isBarcode = " ISBYBARCODE IN ('0')  AND ISBARCODEMATCHING = 0 ";
        //    if (check == "1") isBarcode = " ISBYBARCODE IN ('1')  ";

        //    string stlSelect = $@"
        //        DECLARE @TARTGETDIM NVARCHAR(10) = N'{dimCode}';
        //        SELECT  
        //                ROW_NUMBER() OVER(ORDER BY BRANCHID, CREATEDBY, SHOP_WEB_PDTTRANSORDER.CREATEDDATE DESC) AS ROWNO
        //                ,'0' AS C,[TRANSID],[TRANSDATE],[EMPLID],BRANCHID,
        //          IIF (ISNULL([BRANCHNAME], '') = '',BRANCH_NAME,[BRANCHNAME]) AS [BRANCHNAME],
        //          IIF (ISBYBARCODE = '0','',[ITEMBARCODE]) AS [ITEMBARCODE],
        //          IIF (ISBYBARCODE = '0',[SPC_ITEMNAME2],[SPC_ITEMNAME]) AS [SPC_ITEMNAME],
        //          IIF (ISBYBARCODE = '0',[QTY2],[QTY]) AS [QTY],
        //          IIF (ISBYBARCODE = '0',[UNITID2],[UNITID]) AS [UNITID],
        //          [SPC_PICKINGLOCATIONID],[TARGETDIMENSION],DESCRIPTION AS NAMEPACKINGALL,ORDERIMAGEPATH,ORDERSTATUS
        //          ,IIF (ORDERSTATUS = '13',Order_NAME,SHOW_NAME) AS SHOW_NAME,ISNULL(ORDERSTA_PACKING,'0') AS ORDERSTA_PACKING
        //                ,REFNO2,SHOP_WEB_PDTTRANSORDER.CREATEDDATE,REFMNPO,[EMPLID]+CHAR(10)+[EMPLNAME] AS [EMPLNAMEORDER],
        //                [EMPIDPICKING]+CHAR(10)+[EMPNAMEPICKING] AS [EMPNAMEPICKING]

        //        FROM	SHOP_WEB_PDTTRANSORDER WITH(NOLOCK)
        //                LEFT OUTER	JOIN SHOP_BRANCH WITH(NOLOCK) ON SHOP_WEB_PDTTRANSORDER.BRANCHID = SHOP_BRANCH.BRANCH_ID
        //          LEFT OUTER	JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH(NOLOCK) ON SHOP_WEB_PDTTRANSORDER.ORDERSTATUS = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID AND  TYPE_CONFIG = '54' AND STA = '1'
        //                LEFT OUTER	JOIN SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK) ON SHOP_WEB_PDTTRANSORDER.TARGETDIMENSION = SHOP_WEB_CONFIGTARGETDIMS.DIMENSION AND ISACTIVE = '1' 							 
        //          LEFT JOIN ( 
        //          SELECT	[TransId] AS Trans,[OrderNum],[OrderStatus] AS ORDERSTA_PACKING,SHOW_NAME AS Order_NAME 
        //          FROM	SupcAndroid.dbo.ProductContainer_OrderImageRef  WITH (NOLOCK)
        //            LEFT JOIN SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) ON 
        //             ProductContainer_OrderImageRef.[OrderStatus] = SHOP_CONFIGBRANCH_GenaralDetail.SHOW_ID AND  TYPE_CONFIG = '53' AND STA = '1'
        //            )STA_PACKING ON SHOP_WEB_PDTTRANSORDER.TRANSID =  STA_PACKING.Trans

        //        WHERE   {isBarcode}
        //                {orderStatus}
        //                AND TARGETDIMENSION = COALESCE(NULLIF(RTRIM(@TARTGETDIM), ''), TARGETDIMENSION)
        //                AND TRANSDATE BETWEEN '{date1}' AND '{date2}'
        //                AND BRANCHID = COALESCE(NULLIF(RTRIM('{pMN}'), ''), BRANCHID)

        //        ORDER BY BRANCHID, CREATEDBY, SHOP_WEB_PDTTRANSORDER.CREATEDDATE DESC
        //    ";
        //    return ConnectionClass.SelectSQL_Main(stlSelect);
        //}

        //เปลี่ยนแผนกจัดซื้อ
        public static string OrderByWeb_UpdateTargetDim(string trandId, string dim)
        {
            string sqlUp = $@"
                    UPDATE  SHOP_WEB_PDTTRANSORDER SET
                            TARGETDIMENSION = N'{dim}' , RECVERSION = (RECVERSION + 1)
                    WHERE   TRANSID = N'{trandId}' ";
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }
        //เปลี่ยนบาร์โค้ด
        public static string OrderByWeb_MatchingTransOrder(string trandID, double qtyOrder, double adjustQty, Itembarcode.ItembarcodeDetail_Order _itembarcodeDetailOrder)// (string itembarcode, string itemID, string itemDim,  string itemName, double qty, string unitID, string factor, string locationID, string isadjustqty, string trandID)
        {
            string isadjustqty = "0";
            if (qtyOrder != adjustQty) isadjustqty = "1";

            string sqlUp = $@"
                        UPDATE  SHOP_WEB_PDTTRANSORDER SET
                                ITEMBARCODE = N'{_itembarcodeDetailOrder.sBarcode}', ITEMID = N'{_itembarcodeDetailOrder.sID}', INVENTDIMID = N'{_itembarcodeDetailOrder.sDim}', 
                                QTY = {adjustQty}, UNITID = N'{_itembarcodeDetailOrder.sUnitID}', FACTOR ={_itembarcodeDetailOrder.sFactor}, SPC_ITEMNAME = N'{_itembarcodeDetailOrder.sSpc_Name}',
                                SPC_PICKINGLOCATIONID = N'{_itembarcodeDetailOrder.sPOINVENT}',
                                ISADJUSTQTY = {isadjustqty},
                                ISBARCODEMATCHING = 1, MATCHINGBY = N'{SystemClass.SystemUserID} ', MATCHINGDATE = GETDATE(), RECVERSION = (RECVERSION + 1),
                                ISBYBARCODE = 1
                        WHERE
                                ISBYBARCODE = 0--TAKE PHOTO, NON-BARCODE
                                AND ORDERSTATUS = 1--PENDING STATUS
                                AND TRANSID = N'{trandID}'
             ";
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
            //return sqlUp;
        }
        //จ่ายบิล อ้่างอิงใบสั่งจัดสินค้า
        public static DataTable OrderByWeb_CreateOrderTable(string isbyBarcode, DataTable dt_branch, DataTable dt_ForPacking, string empTarget, string locationTarget, string empNameTarget)
        {
            DataTable dtRoute = Class.LogisticClass.GetRouteAll_GroupBranch();  //Class.LogisticClass.GetRouteExcel_ByDept($@" AND ACCOUNTNUM = '{_emplScan.BRANCHID}' ");

            DataTable dt_Print = new DataTable();
            dt_Print.Columns.Add("ORDERNUM");
            dt_Print.Columns.Add("ROUTE");
            dt_Print.Columns.Add("PICKINGLOCATIONID");
            dt_Print.Columns.Add("BRANCHID");
            dt_Print.Columns.Add("BRANCHNAME");
            dt_Print.Columns.Add("EMPLPACKING");
            dt_Print.Columns.Add("EMPLNAMEPACKING");

            ArrayList sql24 = new ArrayList();
            ArrayList sqlAx = new ArrayList();
            string location = $@", SPC_PICKINGLOCATIONID = '{locationTarget}'";
            if (isbyBarcode == "1") location = "";

            for (int x = 0; x < dt_branch.Rows.Count; x++)
            {
                string ordernum = Class.ConfigClass.GetMaxINVOICEID("MNPT", "-", "MNPT", "1");
                string pickingLocation = locationTarget;
                if (isbyBarcode == "1") pickingLocation = dt_branch.Rows[x]["LOCATION"].ToString();

                sqlAx.Add(TimeKeeper.ScanDptTime.SqlSendAXHD("AX", ordernum, pickingLocation, SystemClass.SystemUserID_M, SystemClass.SystemUserName,
                   dt_branch.Rows[x]["BRANCHID"].ToString(), empTarget + " - " + dt_branch.Rows[x]["BRANCHNAME"], dt_ForPacking.Rows[0]["DIMENSION"].ToString()));

                sqlAx.Add(TimeKeeper.ScanDptTime.SqlSendAXDT("AX", ordernum, pickingLocation, SystemClass.SystemUserID_M));

                DataRow[] branchSelect = dt_ForPacking.Select($@" BRANCHID = '{dt_branch.Rows[x]["BRANCHID"]}' AND PICKINGLOCATIONID = '{dt_branch.Rows[x]["LOCATION"]}' ");
                for (int y = 0; y < branchSelect.Length; y++)
                {
                    sql24.Add(
                         $@"
                               INSERT INTO SHOP_WEB_PDTTRANSORDER_PICKING (ORDERNUM, TRANSID, DIMENSION, EMPLID, USERCREATE
                               ,[INVENTLOCATIONIDFROM],[INVENTLOCATIONIDTO] ,[INVENTLOCATIONIDTONAME])
                                VALUES(
                                    N'{ordernum}',N'{branchSelect[y]["TRANSID"]}', N'{branchSelect[y]["DIMENSION"]}', N'{empTarget}', N'{SystemClass.SystemUserID_M}'
                                    ,'{pickingLocation}','{branchSelect[y]["BRANCHID"]}','{branchSelect[y]["BRANCHNAME"]}'
                                ) 
                        "
                     );
                    sql24.Add(
                        $@"
                               UPDATE   SHOP_WEB_PDTTRANSORDER 
                               SET      ORDERSTATUS = 11 
                                        ,REFMNPO = '{ordernum}'
                                        {location}
                                        ,EMPIDPICKING = '{empTarget}'
                                        ,EMPNAMEPICKING = '{empNameTarget}'
                               WHERE    TRANSID  = N'{branchSelect[y]["TRANSID"]}'  
                        "
                    );
                }

                string routeBch = "";
                DataRow[] route = dtRoute.Select($@"  ACCOUNTNUM = '{ dt_branch.Rows[x]["BRANCHID"] }' ");
                if (route.Length > 0) routeBch = route[0]["ROUTEID"].ToString() + Environment.NewLine + route[0]["NAME"].ToString();

                dt_Print.Rows.Add(
                     ordernum, routeBch, pickingLocation,
                    dt_branch.Rows[x]["BRANCHID"].ToString(), dt_branch.Rows[x]["BRANCHNAME"], empTarget, empNameTarget
                );

            }
            string result = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAx);
            if (result != "") dt_Print.Rows.Clear();

            return dt_Print;

        }
        //จ่ายบิล
        public static DataTable OrderByWeb_CreateOrderTableNoOrder(string locationTarget, DataTable dt_branch, string empTarget, string empNameTarget, string empDpt)
        {
            DataTable dtRoute = Class.LogisticClass.GetRouteAll_GroupBranch();

            DataTable dt_Print = new DataTable();
            dt_Print.Columns.Add("ORDERNUM");
            dt_Print.Columns.Add("ROUTE");
            dt_Print.Columns.Add("PICKINGLOCATIONID");
            dt_Print.Columns.Add("BRANCHID");
            dt_Print.Columns.Add("BRANCHNAME");
            dt_Print.Columns.Add("EMPLPACKING");
            dt_Print.Columns.Add("EMPLNAMEPACKING");

            ArrayList sql24 = new ArrayList();
            ArrayList sqlAx = new ArrayList();
            //string location = $@", SPC_PICKINGLOCATIONID = '{locationTarget}'";
            //if (isbyBarcode == "1") location = "";

            for (int x = 0; x < dt_branch.Rows.Count; x++)
            {
                string ordernum = Class.ConfigClass.GetMaxINVOICEID("MNPT", "-", "MNPT", "1");
                string pickingLocation = locationTarget;


                sqlAx.Add(TimeKeeper.ScanDptTime.SqlSendAXHD("AX", ordernum, pickingLocation, SystemClass.SystemUserID_M, SystemClass.SystemUserName,
                   dt_branch.Rows[x]["BRANCHID"].ToString(), empTarget + " - " + dt_branch.Rows[x]["BRANCHNAME"], empDpt));

                sqlAx.Add(TimeKeeper.ScanDptTime.SqlSendAXDT("AX", ordernum, pickingLocation, SystemClass.SystemUserID_M));
                sql24.Add(
                     $@"
                               INSERT INTO SHOP_WEB_PDTTRANSORDER_PICKING (ORDERNUM, TRANSID, DIMENSION, EMPLID, USERCREATE
                               ,[INVENTLOCATIONIDFROM],[INVENTLOCATIONIDTO] ,[INVENTLOCATIONIDTONAME],STATYPEOPEN)
                                VALUES(
                                    N'{ordernum}',N'{ordernum.Replace("MNPT", "")}', N'{empDpt}', N'{empTarget}', N'{SystemClass.SystemUserID_M}'
                                    ,'{pickingLocation}','{dt_branch.Rows[x]["BRANCHID"]}','{dt_branch.Rows[x]["BRANCHNAME"]}','1' ) "
                 );

                string routeBch;
                DataRow[] route = dtRoute.Select($@"  ACCOUNTNUM = '{dt_branch.Rows[x]["BRANCHID"]}' ");
                if (route.Length == 0) routeBch = "";
                else routeBch = route[0]["ROUTEID"].ToString() + Environment.NewLine + route[0]["NAME"].ToString();

                dt_Print.Rows.Add(ordernum, routeBch, pickingLocation,
                    dt_branch.Rows[x]["BRANCHID"].ToString(), dt_branch.Rows[x]["BRANCHNAME"], empTarget, empNameTarget);

            }
            string result = ConnectionClass.ExecuteMain_AX_24_SameTime(sql24, sqlAx);
            if (result != "") dt_Print.Rows.Clear();

            return dt_Print;
        }
        // ยกเลิกออเดอร์
        public static string OrderByWeb_CancelOrder(string trandID) //emplidไม่มีM
        {
            string sqlUp = $@"
                        UPDATE	SHOP_WEB_PDTTRANSORDER SET
		                        ORDERSTATUS = 0
		                        , CANCELEDBY = N'{SystemClass.SystemUserID}'
		                        , CANCELEDDATE = GETDATE()
		                        , RECVERSION = (RECVERSION + 1)
                        WHERE	TRANSID = N'{trandID}' 
		                        AND ORDERSTATUS = 1
             ";
            // AND RECVERSION = 1
            return ConnectionClass.ExecuteSQL_Main(sqlUp);
        }
        ////สร้าง ccontainer แบบไม่มีน้ำหนัก
        //public static string CreatContainerNumberTypeNotWeight(string emplID)
        //{
        //    //string requestdata = "{\"containerType\":\"1\",\"weight\":0,\"workerId\":\"" + emplID + "\"}";
        //    //try
        //    //{
        //    //    System.Net.WebClient client = new System.Net.WebClient();
        //    //    client.Headers.Add("content-type", "application/json");
        //    //    client.Headers[HttpRequestHeader.Accept] = "*/*";
        //    //    //string response = client.UploadString("http://192.168.100.29/spcproducttranferapi/api/Global/GenerateBarcodeContainer", "POST", requestdata);
        //    //    string response = client.UploadString("http://SPC311SRV/spcproducttranferapi/api/Global/GenerateBarcodeContainer", "POST", requestdata);
        //    //    //MsgRecive responsemodel = (MsgRecive)JsonConvert.DeserializeObject(response, (typeof(MsgRecive)));
        //    //    return response;
        //    //}
        //    //catch (Exception)
        //    //{
        //    //    throw;
        //    //}

        //}


        //รายงานการใช้งานเครื่องโปรแกรม Container รายเดือน
        public static DataTable OrderWeb_MachineCountLine(string pCase, string year)
        {
            string sql;
            if (pCase == "0")
            {
                sql = $@"
                    SELECT	Location AS NUM,DESCRIPTION,ASSETID,ASSETTABLE.SERIALNUM
                    FROM	SHOP2013TMP.dbo.ASSETTABLE WITH (NOLOCK)
		                    INNER JOIN SHOP_WEB_CONFIGTARGETDIMS WITH (NOLOCK)  ON ASSETTABLE.LOCATION = SHOP_WEB_CONFIGTARGETDIMS.DIMENSION
                    WHERE	SPC_AssetSubGroup IN ('TAB','PDA') AND DATAAREAID = N'SPC'
		                    AND DIMENSION NOT IN ('D156','D168')
		                    AND ISACTIVE = '1' AND Condition = 'ใช้งาน'
                    ORDER BY Location,ASSETID ";
            }
            else
            {
                //sql = $@"
                //    SELECT	DeviceName,MONTH(CONVERT(VARCHAR,CreateDateTime,23)) AS MM,Count(*) AS COUNT_ROW
                // FROM	SupcAndroid.dbo.ProductContainer_OrderImageRef WITH (NOLOCK)
                // WHERE	YEAR(CONVERT(VARCHAR,CreateDateTime,23)) = '{year}'
                // GROUP BY DeviceName,MONTH(CONVERT(VARCHAR,CreateDateTime,23)) ";
                sql = $@"
                    SELECT	DeviceName,MM,SUM(COUNT_ROW) AS COUNT_ROW
                    FROM	(
                    SELECT	DeviceName,MONTH(CONVERT(VARCHAR,CreateDateTime,23)) AS MM,Count(*) AS COUNT_ROW
                    FROM	SupcAndroid.dbo.ProductContainer_OrderImageRef WITH (NOLOCK)
                    WHERE	YEAR(CONVERT(VARCHAR,CreateDateTime,23)) = '{year}'
		                    AND DeviceName != ''
                    GROUP BY DeviceName,MONTH(CONVERT(VARCHAR,CreateDateTime,23))
                    UNION
                    SELECT	MNTFSPCNAME AS DeviceName,MONTH(CONVERT(VARCHAR,MNTFDate,23)) AS MM,COUNT(*)AS COUNT_ROW
                    FROM	SHOP_MNTF_HD WITH (NOLOCK)
		                    INNER JOIN SHOP_MNTF_DT WITH (NOLOCK) ON SHOP_MNTF_HD.MNTFDocNo = SHOP_MNTF_DT.MNTFDocNo
                    WHERE	YEAR(CONVERT(VARCHAR,MNTFDate,23)) = '{year}' AND SHOP_MNTF_HD.MNTFStaApvDoc = '1' AND SHOP_MNTF_HD.MNTFStaPrcDoc = '1'
		                    AND MNTFSPCNAME != ''
                    GROUP BY MNTFSPCNAME,MONTH(CONVERT(VARCHAR,MNTFDate,23))
		                    )TMP
                    GROUP BY DeviceName,MM
                ";
            }
            return ConnectionClass.SelectSQL_Main(sql);
        }
    }
}
