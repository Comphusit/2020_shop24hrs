﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Collections;
using System.Drawing;

namespace PC_Shop24Hrs.GeneralForm.MNPO
{
    public partial class TranferDelivary_Template : Telerik.WinControls.UI.RadForm
    {
        DataTable dtBch = new DataTable();
        string pEdit;

        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();

        readonly string _pPermiss; // 0 view , 1 Insert
        readonly string _pType; // SHOP - SUPC
        readonly string _pTypeID;// รหัสกลุ่ม
        readonly int _pHourEdit; //ข้อมูล ณ วันนี้จะแก้ไขได้ถึงกี่โมง
        public TranferDelivary_Template(string pType, string pPermiss, string pTypeID, int pHourEdit)
        {
            InitializeComponent();
            _pType = pType;
            _pPermiss = pPermiss;
            _pTypeID = pTypeID;
            _pHourEdit = pHourEdit;
        }
        //Load
        private void TranferDelivary_Template_Load(object sender, EventArgs e)
        {
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เปิดออเดอร์ใหม่";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export Excel";
            RadButton_Search.ButtonElement.ShowBorder = true; RadButton_Search.ButtonElement.ToolTipText = "ค้นหาข้อมูล";
            RadButton_Save.ButtonElement.ShowBorder = true; RadButton_Save.ButtonElement.ToolTipText = "บันทึกข้อมูล";
            radButtonElement_print.ShowBorder = true; radButtonElement_print.ToolTipText = "พิมพ์";
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            RadCheckBox_Branch.ButtonElement.Font = SystemClass.SetFontGernaral;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_Begin, DateTime.Now.AddDays(1), DateTime.Now.AddDays(10));

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            if (_pPermiss == "1") { RadGridView_ShowHD.ReadOnly = false; }
            else { RadGridView_ShowHD.ReadOnly = true; }

            //แยกระหว่างสาขาและ SUPC
            switch (_pType)
            {
                case "SHOP"://shop
                    RadCheckBox_Branch.Checked = true;
                    dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                    RadCheckBox_Branch.Enabled = false;
                    break;
                case "SUPC": // supc
                    RadCheckBox_Branch.Checked = false;
                    dtBch = Get_AllBranch();
                    RadCheckBox_Branch.Enabled = true;
                    break;
                default:
                    break;
            }

            RadDropDownList_Branch.DataSource = dtBch;
            RadDropDownList_Branch.DisplayMember = "BRANCH_NAME"; RadDropDownList_Branch.ValueMember = "BRANCH_ID";
        }

        //Set Head GridView
        void SetGridView()
        {
            DataTable dtBchSend = Get_BranchQty();
            if (_pPermiss == "1")
            {
                if (dtBchSend.Rows.Count > 0) { RadButton_Save.Enabled = false; pEdit = "1"; }
                else { RadButton_Save.Enabled = true; pEdit = "0"; }
            }
            else { RadButton_Save.Enabled = false; pEdit = "0"; }


            if (_pType == "SUPC")
            {
                if (RadCheckBox_Branch.CheckState == CheckState.Checked) dtBch = dtBch = BranchClass.GetDetailBranchByID(RadDropDownList_Branch.SelectedValue.ToString());
                else dtBch = Get_AllBranch();
            }

            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
            }

            string pColumn; string pColumnName;
            string pRow; string pRowName;
            string labelGrp1; string labelGrp2;

            DataTable dt_ShowID = Get_ShowID();

            DataTable dtColumn; DataTable dtRow;
            if (radRadioButton_Bch.CheckState == CheckState.Checked)
            {
                pColumn = "BRANCH_ID"; pColumnName = "BRANCH_NAME"; dtColumn = dtBch;
                pRow = "SHOW_ID"; pRowName = "SHOW_NAME"; dtRow = dt_ShowID;
                labelGrp1 = "บาร์โค้ด"; labelGrp2 = "ชื่อสินค้า";
            }
            else
            {
                pColumn = "SHOW_ID"; pColumnName = "SHOW_NAME"; dtColumn = dt_ShowID;
                pRow = "BRANCH_ID"; pRowName = "BRANCH_NAME"; dtRow = dtBch;
                labelGrp1 = "สาขา"; labelGrp2 = "ชื่อสาขา";
            }

            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(pRow, labelGrp1, 130)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual(pRowName, labelGrp2, 170)));
            RadGridView_ShowHD.Columns[pRow].IsPinned = true; RadGridView_ShowHD.Columns[pRowName].IsPinned = true;

            ConditionalFormattingObject obj1 = new ConditionalFormattingObject("MyCondition", ConditionTypes.Equal, "X", "", false)
            { CellBackColor = ConfigClass.SetColor_Red() };

            foreach (DataRow row in dtColumn.Rows)
            {
                string headText = row[pColumn].ToString() + System.Environment.NewLine + row[pColumnName].ToString();
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter(row[pColumn].ToString(), headText, 180)));
                RadGridView_ShowHD.Columns[row[pColumn].ToString()].FormatString = "{0:#,##0.00}";

                RadGridView_ShowHD.Columns[row[pColumn].ToString()].ConditionalFormattingObjectList.Add(obj1);

                GridViewSummaryItem summaryItem = new GridViewSummaryItem(row[pColumn].ToString(), "รวม = {0}", GridAggregateFunction.Sum);
                summaryRowItem.Add(summaryItem);
            }

            DataTable dtBranchSet = Get_BranchSend();
            for (int iR = 0; iR < dtRow.Rows.Count; iR++)
            {
                RadGridView_ShowHD.Rows.Add(dtRow.Rows[iR][pRow], dtRow.Rows[iR][pRowName]);
                for (int iC = 0; iC < dtColumn.Rows.Count; iC++)
                {
                    string values_Set = "X";
                    DataRow[] dr = dtBranchSet.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + "'" +
                        " AND  " + pColumn + @" = '" + dtColumn.Rows[iC][pColumn].ToString() + @"' ");

                    if (dr.Length > 0)
                    {
                        DataRow[] drQty = dtBchSend.Select(" " + pRow + @" = '" + dtRow.Rows[iR][pRow].ToString() + "'" +
                        " AND  " + pColumn + @" = '" + dtColumn.Rows[iC][pColumn].ToString() + @"' ");
                        if (drQty.Length > 0)
                        {
                            values_Set = Convert.ToDouble(drQty[0]["QTY"].ToString()).ToString("N2");
                        }
                        else
                        {
                            values_Set = "0.00";
                        }
                    }
                    RadGridView_ShowHD.Rows[iR].Cells[dtColumn.Rows[iC][pColumn].ToString()].Value = values_Set;// dr.Length;
                }
            }
            this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);

            RadCheckBox_Branch.Enabled = false; RadDropDownList_Branch.Enabled = false;
            radRadioButton_Bch.Enabled = false; radRadioButton_Item.Enabled = false;
            radDateTimePicker_Begin.Enabled = false; RadButton_Search.Enabled = false;

            //Check DateTime แก้ไขได้เฉพาะก่อน 8 โมงเท่านั้น
            if (_pPermiss == "1")
            {
                if (Convert.ToDateTime(radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd")) <= Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd")))
                {
                    if (DateTime.Now.Hour > _pHourEdit) RadGridView_ShowHD.ReadOnly = true; else RadGridView_ShowHD.ReadOnly = false;
                }
                else RadGridView_ShowHD.ReadOnly = false;
            }
            else RadGridView_ShowHD.ReadOnly = false;
        }
        //GetBranch All
        DataTable Get_AllBranch()
        {
            return ConnectionClass.SelectSQL_Main(string.Format(@" 
                                    SELECT	SHOP_BRANCH.BRANCH_ID,BRANCH_NAME
                                    FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) INNER JOIN SHOP_BRANCH WITH (NOLOCK)
			                                    ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
                                    WHERE	TYPE_CONFIG = '" + _pTypeID + @"'
                                    GROUP BY SHOP_BRANCH.BRANCH_ID,BRANCH_NAME
                                    ORDER BY SHOP_BRANCH.BRANCH_ID "));
        }
        //รายการสินค้า
        private DataTable Get_ShowID()
        {
            //return ConnectionClass.SelectSQL_Main(string.Format(@" 
            //                        select SHOW_ID,SHOW_NAME, SHOW_DESC  
            //                        from  SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) 
            //                        where TYPE_CONFIG = '" + _pTypeID + @"' AND STA = '1' 
            //                        order by SHOW_ID "));

            return ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID(_pTypeID, "", " ORDER BY SHOW_ID", "1");
        }
        //สาขาที่ส่ง ได้
        private DataTable Get_BranchSend()
        {
            //return ConnectionClass.SelectSQL_Main(string.Format(@" 
            //                        SELECT	SHOW_ID,BRANCH_ID
            //                        FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK)
            //                        WHERE	TYPE_CONFIG = '" + _pTypeID + @"'
            //                        ORDER BY SHOW_ID "));

            return ConfigClass.FindData_CONFIGBRANCH_DETAIL(_pTypeID);
        }
        //สาขาที่ส่ง ได้
        private DataTable Get_BranchQty()
        {
            //return ConnectionClass.SelectSQL_Main(string.Format(@" 
            //                        SELECT	SHOW_ID,BRANCH_ID,QTY	
            //                        FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK)
            //                        WHERE	TYPE_CONFIG = '" + _pTypeID + @"' 
            //                                AND CONVERT(VARCHAR,DATE_SEND,23) = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' "));
            return ConfigClass.FindData_CONFIGBRANCH_QTY(_pTypeID, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"));
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion

        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetGridView();
        }
        //Branch
        private void RadCheckBox_Branch_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_Branch.Checked == true) RadDropDownList_Branch.Enabled = true; else RadDropDownList_Branch.Enabled = false;
        }

        //Begin Edit
        private void RadGridView_ShowHD_CellBeginEdit(object sender, GridViewCellCancelEventArgs e)
        {

            if ((e.Column == RadGridView_ShowHD.Columns[0]) || (e.Column == RadGridView_ShowHD.Columns[1])) { e.Cancel = true; }
            try
            {
                if (RadGridView_ShowHD.CurrentCell.Value.ToString() == "X") { e.Cancel = true; }
            }
            catch (Exception) { return; }

        }
        //EndEdit
        private void RadGridView_ShowHD_CellEndEdit(object sender, GridViewCellEventArgs e)
        {
            try
            {
                RadGridView_ShowHD.CurrentCell.Value = Convert.ToDouble(RadGridView_ShowHD.CurrentCell.Value.ToString()).ToString("N2");
            }
            catch (Exception) { return; }
        }
        //Number Only 
        private void RadGridView_ShowHD_CellValidated(object sender, CellValidatedEventArgs e)
        {

            if ((e.Column == RadGridView_ShowHD.Columns[0]) || (e.Column == RadGridView_ShowHD.Columns[1])) return;
            try
            {
                if (RadGridView_ShowHD.CurrentCell.Value.ToString() == "X") return;
            }
            catch (Exception) { return; }


            if (!ConfigClass.IsNumber(e.Value.ToString()))
            {
                MsgBoxClass.MsgBoxShow_InputNumbersOnly("จำนวนส่ง");
                RadGridView_ShowHD.CurrentCell.Value = "0.00";
            }
            else
            {
                if (pEdit == "1") //ในกรณีที่ Save ครั้งแรกไว้แล้ว
                {
                    string showID; string bchID;
                    if (radRadioButton_Bch.CheckState == CheckState.Checked)
                    {
                        showID = RadGridView_ShowHD.CurrentRow.Cells["SHOW_ID"].Value.ToString();
                        bchID = RadGridView_ShowHD.CurrentColumn.Name;
                    }
                    else
                    {
                        showID = RadGridView_ShowHD.CurrentColumn.Name;
                        bchID = RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                    }

                    string strUp = string.Format(@"
                        UPDATE  [SHOP_CONFIGBRANCH_QTY] 
                        SET     QTY = '" + Convert.ToDouble(e.Value.ToString()) + @"' , DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                WHOIDUPD = '" + SystemClass.SystemUserID + @"' ,WHONAMEUPD = '" + SystemClass.SystemUserName + @"' 
                        WHERE   SHOW_ID = '" + showID + @"' AND DATE_SEND = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"' 
                                AND TYPE_CONFIG = '" + _pTypeID + @"' AND BRANCH_ID = '" + bchID + @"' 
                    ");
                    string T = ConnectionClass.ExecuteSQL_Main(strUp);
                    if (T != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    }
                }
            }
        }
        //ClearDate
        void ClearData()
        {
            if (RadGridView_ShowHD.Columns.Count > 0)
            {
                RadGridView_ShowHD.Columns.Clear();
                RadGridView_ShowHD.Rows.Clear();
                summaryRowItem.Clear();
                this.RadGridView_ShowHD.SummaryRowsTop.Clear();
            }
            radDateTimePicker_Begin.Value = DateTime.Now.AddDays(1);
            pEdit = "0";
            RadCheckBox_Branch.Enabled = true; RadDropDownList_Branch.Enabled = true;
            radRadioButton_Bch.Enabled = true; radRadioButton_Item.Enabled = true;
            radDateTimePicker_Begin.Enabled = true; RadButton_Search.Enabled = true;
        }
        //เพิ่ม ข้อมูลใหม่
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            if (MsgBoxClass.MsgBoxShow_ConfirmInsert("ข้อมูล ออเดอร์ข้าวกล่องประจำวัน " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd")) == DialogResult.No) return;

            ArrayList strIn = new ArrayList();
            string showID = ""; string bchID = "";
            string billSend;

            for (int iR = 0; iR < RadGridView_ShowHD.Rows.Count; iR++) //สินค้า
            {
                if (radRadioButton_Bch.CheckState == CheckState.Checked)
                { showID = RadGridView_ShowHD.Rows[iR].Cells["SHOW_ID"].Value.ToString(); }
                else { bchID = RadGridView_ShowHD.Rows[iR].Cells["BRANCH_ID"].Value.ToString(); }

                for (int iC = 2; iC < RadGridView_ShowHD.Columns.Count; iC++) // สาขา
                {
                    double qty = 0;

                    if (RadGridView_ShowHD.Rows[iR].Cells[iC].Value.ToString() != "X")
                    {
                        qty = Convert.ToDouble(RadGridView_ShowHD.Rows[iR].Cells[iC].Value.ToString());
                    }

                    if (radRadioButton_Bch.CheckState == CheckState.Checked) { bchID = RadGridView_ShowHD.Columns[iC].Name; }
                    else { showID = RadGridView_ShowHD.Columns[iC].Name; }

                    billSend = bchID + "-" + radDateTimePicker_Begin.Value.ToString("yyMMdd") + "-" + Convert.ToString(iR) + Convert.ToString(iC) + "_" + _pTypeID;

                    strIn.Add(String.Format(@" 
                            INSERT INTO [dbo].[SHOP_CONFIGBRANCH_QTY]
                               ([SHOW_ID],[DATE_SEND],[TYPE_CONFIG],[BRANCH_ID]
                               ,[QTY],[WHOIDINS],[WHONAMEINS],[BILLSEND]
                               ) 
                            VALUES 
                                ('" + showID + @"','" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"',
                                '" + _pTypeID + @"','" + bchID + @"','" + qty + @"',
                                '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + "','" + billSend + @"')"));
                }
            }

            string T = ConnectionClass.ExecuteSQL_ArrayMain(strIn);
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "") RadButton_Save.Enabled = false;

        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("ออเดอร์ข้าวกล่องประจำวัน " + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //print All
        private void RadButtonElement_printAll_Click(object sender, EventArgs e)
        {
            if (RadButton_Save.Enabled == true) return;
            if (RadGridView_ShowHD.Rows.Count == 0) return;

            if (RadGridView_ShowHD.CurrentRow.RowElementType != typeof(GridDataRowElement)) return;
            if ((RadGridView_ShowHD.CurrentCell.ColumnIndex == 0) || (RadGridView_ShowHD.CurrentCell.ColumnIndex == 1)) return;

            DialogResult result = printDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
                printDialog1.PrinterSettings.DefaultPageSettings.PaperSize = ps;
                PrintDocument1.PrinterSettings = printDialog1.PrinterSettings;
            }
            else { return; }

            FormShare.ChooseData _ChooseData = new FormShare.ChooseData("ทั้งรายการ[บาร์โค้ด]", "เฉพาะที่เลือก[1 รายการ]");
            if (_ChooseData.ShowDialog(this) == DialogResult.Yes)
            {
                string showID; string bchID;
                if (radRadioButton_Bch.CheckState == CheckState.Checked)
                {
                    showID = RadGridView_ShowHD.CurrentRow.Cells["SHOW_ID"].Value.ToString();
                    bchID = RadGridView_ShowHD.CurrentColumn.Name;
                }
                else
                {
                    showID = RadGridView_ShowHD.CurrentColumn.Name;
                    bchID = RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
                }

                if (_ChooseData.sSendData == "1")
                {
                    Print("1", bchID, showID);
                }
                else
                {
                    if ((RadGridView_ShowHD.CurrentRow.Cells[0].IsSelected) && (RadGridView_ShowHD.CurrentRow.Cells[1].IsSelected)) { return; }
                    if (RadGridView_ShowHD.CurrentCell.Value.ToString() == "X") { return; }
                    if (Convert.ToDouble(RadGridView_ShowHD.CurrentCell.Value.ToString()) == 0) { return; }

                    Print("0", bchID, showID);
                }
            }
        }
        //Print
        string pBchID, pBchName, pBarcodeID, pBarcodeName, pQty, pBillID, pUnitID;

        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {

            FormClass.Document_Check(this.Name, "");
        }

        void Print(string pTypePrint, string pBch, string pBarcode) //pTypePrint 1 = All 0 = only Select
        {
            pBchID = ""; pBchName = ""; pBarcodeID = ""; pBarcodeName = ""; pQty = ""; pBillID = ""; pUnitID = "";
            //string sql;
            //if (pTypePrint == "1")
            //{
            //    sql = string.Format(@"
            //    SELECT  SHOW_ID,SPC_ITEMNAME,SHOP_CONFIGBRANCH_QTY.BRANCH_ID,BRANCH_NAME,SHOP_CONFIGBRANCH_QTY.QTY,BILLSEND,UNITID  
            //    FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK) 
            //                INNER JOIN SHOP_BRANCH WITH (NOLOCK)    ON SHOP_CONFIGBRANCH_QTY.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
            //                INNER JOIN	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_CONFIGBRANCH_QTY.SHOW_ID = INVENTITEMBARCODE.ITEMBARCODE
            //    WHERE	TYPE_CONFIG = '" + _pTypeID + @"' 
            //      AND CONVERT(VARCHAR,DATE_SEND,23) = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"'
            //            AND SHOW_ID = '" + pBarcode + @"' 
            //      AND SHOP_CONFIGBRANCH_QTY.QTY > 0
            //    ORDER BY SHOW_ID,SHOP_CONFIGBRANCH_QTY.BRANCH_ID  
            //");
            //}
            //else
            //{
            //    sql = string.Format(@"
            //    SELECT  SHOW_ID,SPC_ITEMNAME,SHOP_CONFIGBRANCH_QTY.BRANCH_ID,BRANCH_NAME,SHOP_CONFIGBRANCH_QTY.QTY,BILLSEND,UNITID  
            //    FROM	SHOP_CONFIGBRANCH_QTY WITH (NOLOCK) 
            //                INNER JOIN SHOP_BRANCH WITH (NOLOCK)    ON SHOP_CONFIGBRANCH_QTY.BRANCH_ID = SHOP_BRANCH.BRANCH_ID
            //                INNER JOIN	SHOP2013TMP.dbo.INVENTITEMBARCODE WITH (NOLOCK) ON SHOP_CONFIGBRANCH_QTY.SHOW_ID = INVENTITEMBARCODE.ITEMBARCODE
            //    WHERE	TYPE_CONFIG = '" + _pTypeID + @"' 
            //      AND CONVERT(VARCHAR,DATE_SEND,23) = '" + radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd") + @"'
            //            AND SHOW_ID = '" + pBarcode + @"' AND SHOP_CONFIGBRANCH_QTY.BRANCH_ID = '" + pBch + @"' 
            //      AND SHOP_CONFIGBRANCH_QTY.QTY > 0
            //    ORDER BY SHOP_CONFIGBRANCH_QTY.BRANCH_ID  
            //");
            //}

            DataTable dtPrint = POClass.TranferDelivery_ForPrint(pTypePrint, _pTypeID, radDateTimePicker_Begin.Value.ToString("yyyy-MM-dd"), pBarcode, pBch); //ConnectionClass.SelectSQL_Main(sql); if (dtPrint.Rows.Count == 0) { return; }

            for (int iR = 0; iR < dtPrint.Rows.Count; iR++)
            {
                pBchID = dtPrint.Rows[iR]["BRANCH_ID"].ToString();
                pBchName = dtPrint.Rows[iR]["BRANCH_NAME"].ToString();
                pBarcodeID = dtPrint.Rows[iR]["SHOW_ID"].ToString();
                pBarcodeName = dtPrint.Rows[iR]["SPC_ITEMNAME"].ToString();
                pQty = dtPrint.Rows[iR]["Qty"].ToString();
                pBillID = dtPrint.Rows[iR]["BILLSEND"].ToString();
                pUnitID = dtPrint.Rows[iR]["UNITID"].ToString();

                PrintDocument1.Print();

            }
        }
        //Print
        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = pBillID; Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 10, Y);
            Y += 110;
            e.Graphics.DrawString(pBchID + "-" + pBchName, SystemClass.printFont15, Brushes.Black, 15, Y);
            Y += 25;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString(pBarcodeName, SystemClass.printFont, Brushes.Black, 20, Y);
            Y += 20;
            e.Graphics.DrawString(pBarcodeID + "  X  " + pQty + "  " + pUnitID, SystemClass.printFont, Brushes.Black, 20, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;

            //e.Graphics.DrawString(pBchName, new Font(new FontFamily("Tahoma"), 80), Brushes.Black, 0, Y);

            Rectangle rect1 = new Rectangle(0, Y, 350, 400);
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near
            };
            e.Graphics.DrawString(pBchName, new Font(new FontFamily("Tahoma"), 80), Brushes.Black, rect1, stringFormat);
        }
    }
}
