﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using System.Data;
using System.Collections;
using System.Drawing;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.GeneralForm.MNPO
{
    public partial class MNPO_Order : Telerik.WinControls.UI.RadForm
    {
        readonly DataTable dt_PO = new DataTable();

        string pApv;

        readonly string _pType;// 0 = SHOP หรือ  1 = SUPC
        readonly string _pTypeOrderOpen;// เปน Desc เลย

        //string tblHD;
        //string tblDT;

        public MNPO_Order(string pType, string pTypeOrderOpen)
        {
            InitializeComponent();

            _pType = pType;
            _pTypeOrderOpen = pTypeOrderOpen;
        }
        //Load Main
        private void MNPO_Order_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ShowBorder = true; RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            //tblHD = "SHOP_MNPO_HD";
            //tblDT = "SHOP_MNPO_DT";

            DatagridClass.SetDefaultRadGridView(RadGridView_Show);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Branch);

            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QtyOrder", "จำนวน", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY", "อัตราส่วน"));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Purchase", "แผนกจัดซื้อ", 120));
            RadGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POLast", "สั่งล่าสุด", 250));

            RadGridView_Show.Columns["QtyOrder"].FormatString = "{0:#,##0.00}";
            RadGridView_Show.Columns["QTY"].FormatString = "{0:#,##0.00}";
            RadGridView_Show.MasterTemplate.EnableFiltering = false;

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "QtyOrder = '0.00' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            RadGridView_Show.Columns["QtyOrder"].ConditionalFormattingObjectList.Add(obj1);

            dt_PO.Columns.Add("ITEMID");
            dt_PO.Columns.Add("INVENTDIMID");
            dt_PO.Columns.Add("ITEMBARCODE");
            dt_PO.Columns.Add("SPC_ITEMNAME");
            dt_PO.Columns.Add("QtyOrder");
            dt_PO.Columns.Add("UNITID");
            dt_PO.Columns.Add("QTY");
            dt_PO.Columns.Add("Purchase");
            dt_PO.Columns.Add("POLast");

            radCheckBox_PO.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Sale.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;

            radStatusStrip1.SizingGrip = false;
            radButtonElement_Add.ShowBorder = true; radButtonElement_Add.ToolTipText = "เปิดเอกสารใหม่";
            RadButtonElement_Find.ShowBorder = true; RadButtonElement_Find.ToolTipText = "ค้นหาเอกสาร";
            radButtonElement_excel.ShowBorder = true; radButtonElement_excel.ToolTipText = "Export Excel";

            RadButton_Apv.ButtonElement.ShowBorder = true; RadButton_Apv.ButtonElement.ToolTipText = "ยืนยันการสั่ง ";
            RadButton_Cancel.ButtonElement.ShowBorder = true; RadButton_Cancel.ButtonElement.ToolTipText = "ยกเลิกการสั่ง ";

            ClearData();
        }
        //สาขาที่เปิด Order
        void Set_Branch()
        {
            if (_pType == "SHOP") RadDropDownList_Branch.DataSource = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
            else RadDropDownList_Branch.DataSource = BranchClass.GetBranchAll(" '1','4' ", " '1' ");


            RadDropDownList_Branch.ValueMember = "BRANCH_ID";
            RadDropDownList_Branch.DisplayMember = "NAME_BRANCH";
        }
        //Clear
        void ClearData()
        {
            if (dt_PO.Rows.Count > 0) { dt_PO.Rows.Clear(); dt_PO.AcceptChanges(); }

            Set_Branch();
            radLabel_Docno.Text = "";
            pApv = "0";

            RadDropDownList_Branch.Enabled = true;

            radLabel_StatusBill.Text = "สถานะบิล : ยังไม่บันทึก";
            radLabel_StatusBill.ForeColor = ConfigClass.SetColor_Red();
            radLabel_StatusBill.BackColor = Color.Transparent;
            radLabel_StatusBill.Text = "";
            RadDropDownList_Branch.Enabled = true;

            radCheckBox_Sale.CheckState = CheckState.Checked;
            radCheckBox_PO.CheckState = CheckState.Checked;

            radTextBox_Barcode.Enabled = true;
            RadButton_Cancel.Enabled = false;
            RadButton_Apv.Enabled = false;
            radTextBox_Barcode.Focus();
        }

        //Claer
        private void RadButtonElement_Add_Click(object sender, EventArgs e)
        {
            if ((radLabel_Docno.Text != "") && (pApv == "0"))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("เอกสารการสั่งยังไม่ยืนยัน ต้องยืนยันให้เรียบร้อยก่อน จึงสามารถจะเปิดเอกสารใหม่ได้.");
                return;
            }
            ClearData();
            Set_Branch();
        }

        //ยืนยัน
        private void RadButton_Apv_Click(object sender, EventArgs e)
        {
            DataTable dtForSend = POClass.GetDataDetailMNPO_ForSendAX("0", radLabel_Docno.Text, "");
            //if (dtForSend.Rows.Count == 0)
            //{
            //    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่พบข้อมูลการสั่งสินค้า {radLabel_Docno.Text}{Environment.NewLine}เช็คข้อมูลของบิลนี้แล้วลองใหม่อีกครั้ง");
            //    return;
            //}

            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยืนยันการสั่ง") == DialogResult.No) return;

            //string T_orderToWeb = ConnectionClass.SelectSQL_Main($@"  POClass_GetDataDetailMNPO_ForSendAX_TRANSWEB '1','{radLabel_Docno.Text}','' ").Rows[0][0].ToString();
            //ConnectionClass.ExecuteSQL_Main($@" [POClass_TransMNPOTOWEB] '{radLabel_Docno.Text}' ");
            //if (T_orderToWeb != "")
            //{
            //    MsgBoxClass.MsgBoxShowButtonOk_Error($@"ไม่สามารถยืนยันการสั่งได้{Environment.NewLine}มีปัญหาในส่วนของการส่งข้อมูล{Environment.NewLine}ลองใหม่อีกครั้ง{Environment.NewLine}{T_orderToWeb}");
            //    return;
            //}

            ArrayList sqlUp = new ArrayList
            {
                $@" 
                            UPDATE  SHOP_MNPO_HD   
                            SET     MNPOStaPrcDoc = '1',MNPOStaApvDoc = '1',
                                    MNPODateApv = CONVERT(VARCHAR,GETDATE(),23),MNPOTimeApv = CONVERT(VARCHAR,GETDATE(),24),
                                    MNPOWhoApv = '{SystemClass.SystemUserID}',MNPOWhoNameApv = '{SystemClass.SystemUserName}' 
                            WHERE   MNPODocNo = '{radLabel_Docno.Text}'  "
            };
            sqlUp.Add(ConnectionClass.SelectSQL_Main($@"  POClass_GetDataDetailMNPO_ForSendAX_TRANSWEB '2','{radLabel_Docno.Text}','' ").Rows[0]["STRINSERT"].ToString());

            string T = AX_SendData.Save_POSTOLINE10(sqlUp, dtForSend, "98");
            //if (dtForSend.Rows.Count == 0) T = ConnectionClass.ExecuteSQL_ArrayMain(sqlUp); else T = AX_SendData.Save_POSTOLINE10(sqlUp, dtForSend, "98");

            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยืนยันการสั่ง");

            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel();
                pApv = "1";
                radTextBox_Barcode.Enabled = false; RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                RadDropDownList_Branch.Enabled = false;
                return;
            }
        }
        //ยกเลิกบิล
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            if (MsgBoxClass.MsgBoxShow_Bill_ComfirmStatus(radLabel_Docno.Text, "ยกเลิกการสั่ง") == DialogResult.No) return;

            string sqlCancle = string.Format(@"
                            UPDATE  SHOP_MNPO_HD  
                            SET     MNPOStaDoc = '3',
                                    MNPODateUp = CONVERT(VARCHAR,GETDATE(),23),MNPOTimeUp = CONVERT(VARCHAR,GETDATE(),24),
                                    MNPOWhoUp = '" + SystemClass.SystemUserID + @"',MNPOWhoUpName = '" + SystemClass.SystemUserName + @"' 
                            WHERE   MNPODocNo = '" + radLabel_Docno.Text + @"'  ");
            String T = ConnectionClass.ExecuteSQL_Main(sqlCancle);
            MsgBoxClass.MsgBoxShow_Bill_SaveStatus(T, radLabel_Docno.Text, "ยกเลิกการสั่ง");
            if (T == "")
            {
                radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black;
                radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                radTextBox_Barcode.Enabled = false; RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                RadDropDownList_Branch.Enabled = false;
            }
        }
        //Find Data
        private void RadButtonElement_Find_Click(object sender, EventArgs e)
        {
            MNPO_FindData _mnpo_FindData = new MNPO_FindData();
            if (_mnpo_FindData.ShowDialog(this) == DialogResult.Yes)
            {
                SetDGV_Load(_mnpo_FindData.pDocno);
            }
        }
        //set Data Find
        void SetDGV_Load(string pDocno)
        {
            string sqlSelect = string.Format(@"
                SELECT	SHOP_MNPO_HD.MNPODOCNO,MNPOBRANCH,MNPOStaDoc AS STADOC,MNPOStaPrcDoc AS STAPRCDOC,MNPOStaApvDoc,
		                MNPOSeqNo,MNPOItemID AS ITEMID,MNPODimid AS INVENTDIMID,MNPOBarcode AS ITEMBARCODE,
		                MNPOName AS SPC_ITEMNAME,MNPOQtyOrder AS QtyOrder,MNPOUnitID AS UNITID,MNPOFactor AS QTY,MNPODATEPR AS Purchase,MNPODATEPO AS POLast
                FROM	SHOP_MNPO_HD WITH (NOLOCK) 
		                INNER JOIN SHOP_MNPO_DT WITH (NOLOCK) ON  SHOP_MNPO_HD.MNPODOCNO = SHOP_MNPO_DT.MNPODOCNO
                WHERE	SHOP_MNPO_HD.MNPODocno = '" + pDocno + @"'
                ORDER BY MNPOSeqNo");
            DataTable dt = ConnectionClass.SelectSQL_Main(sqlSelect);
            if (dt_PO.Rows.Count > 0) { dt_PO.Rows.Clear(); }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt_PO.Rows.Add(
                    dt.Rows[i]["ITEMID"].ToString(), dt.Rows[i]["INVENTDIMID"].ToString(),
                    dt.Rows[i]["ITEMBARCODE"].ToString(), dt.Rows[i]["SPC_ITEMNAME"].ToString(),
                    string.Format("{0:0.00}", Convert.ToDouble(dt.Rows[i]["QtyOrder"].ToString())),
                    dt.Rows[i]["UNITID"].ToString(),
                    string.Format("{0:0.00}", Convert.ToDouble(dt.Rows[i]["QTY"].ToString())),
                    dt.Rows[i]["Purchase"].ToString(),
                    dt.Rows[i]["POLast"].ToString());
            }

            RadGridView_Show.DataSource = dt_PO;
            dt_PO.AcceptChanges();

            Set_Branch();
            RadDropDownList_Branch.SelectedValue = dt.Rows[0]["MNPOBRANCH"].ToString(); RadDropDownList_Branch.Enabled = false;
            radLabel_Docno.Text = dt.Rows[0]["MNPODOCNO"].ToString();

            switch (dt.Rows[0]["STADOC"].ToString())
            {
                case "1":
                    if (dt.Rows[0]["STAPRCDOC"].ToString() == "1")
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : ยืนยันแล้ว"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_SkyPastel(); pApv = "1";
                        RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                        radTextBox_Barcode.Enabled = false;
                    }
                    else
                    {
                        radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel(); pApv = "0";
                        RadButton_Apv.Enabled = true; RadButton_Cancel.Enabled = true;
                    }
                    break;
                case "3":
                    radLabel_StatusBill.Text = "สถานะบิล : ถูกยกเลิก"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_PinkPastel(); pApv = "3";
                    RadButton_Apv.Enabled = false; RadButton_Cancel.Enabled = false;
                    radTextBox_Barcode.Enabled = false;
                    break;
                default:
                    break;
            }
        }
        //Enter + F4 In Itembarcode
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(radTextBox_Barcode.Text.Trim());
                if (!(dtBarcode.GetItembarcodeStatus))
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus(); return;
                }
                SetDataInputBarcode(radTextBox_Barcode.Text.Trim());
            }

            else if (e.KeyCode == Keys.F4)
            {
                string pCon;
                if (radTextBox_Barcode.Text != "")
                { pCon = "AND SPC_ITEMNAME LIKE '%" + radTextBox_Barcode.Text.Replace(" ", "%") + @"%' "; }
                else { pCon = ""; }

                ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(pCon);
                if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
                {
                    Data_ITEMBARCODE dtBarcode = ShowDataDGV_Itembarcode.items;
                    SetDataInputBarcode(dtBarcode.Itembarcode_ITEMBARCODE);
                }
                else
                {
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus(); return;
                }
            }
        }
        //Choose Barcode And Input QtyOrder
        void SetDataInputBarcode(string pBarcode)
        {
            string pCheckPO_6; string pCheckSale_30;
            if (radCheckBox_PO.CheckState == CheckState.Checked) { pCheckPO_6 = "1"; } else { pCheckPO_6 = "0"; }
            if (radCheckBox_Sale.CheckState == CheckState.Checked) { pCheckSale_30 = "1"; } else { pCheckSale_30 = "0"; }

            this.Cursor = Cursors.WaitCursor;
            //DataTable dtItem = ConnectionClass.SelectSQL_Main(" POClass_GetDetailItembarcode_ForPO '" + pBarcode + @"','" + RadDropDownList_Branch.SelectedValue.ToString() + @"' ");
            DataTable dtItem = ConnectionClass.SelectSQL_Main($@" POClass_GetDetailItembarcode '0','{RadDropDownList_Branch.SelectedValue}','{pBarcode}','','' ");
            if (dtItem.Rows.Count == 0)
            {
                MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ดที่ระบุ");
                this.Cursor = Cursors.Default;
                return;
            }

            if (dtItem.Rows[0]["GRPID0"].ToString() == "033")
            {
                if (dtItem.Rows[0]["GRPID1"].ToString() == "001")
                {
                    double itemStock = ItembarcodeClass.FindStock_ByBarcode(dtItem.Rows[0]["ITEMBARCODE"].ToString(), RadDropDownList_Branch.SelectedValue.ToString());
                    if (itemStock > 0)
                    {
                        double itemSale90 = 0;
                        DataTable dtSale90 = PosSaleClass.GetSaleSumInventQty_ByItemInvent(dtItem.Rows[0]["ITEMBARCODE"].ToString(), RadDropDownList_Branch.SelectedValue.ToString(), 90);

                        if (dtSale90.Rows.Count > 0)
                        {
                            itemSale90 = Convert.ToDouble(dtSale90.Rows[0]["QTY"].ToString());
                        }
                        if ((itemStock - itemSale90) > 0)
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Error("สินค้ามีสต็อก  >> " + itemStock.ToString("#,#0.00") + Environment.NewLine +
                                "ยอดขายเฉลี่ยวันละ " + (itemSale90 / 90).ToString("#,#0.00") + Environment.NewLine +
                                "เพียงพอที่จะขายมากกว่า 3 เดือน" + Environment.NewLine + "ไม่อนุญาติให้สั่งสินค้าเพิ่มทุกกรณี");
                            radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }
                }
            }

            DataRow[] dr = dt_PO.Select("ITEMID = '" + dtItem.Rows[0]["ITEMID"].ToString() + "' AND INVENTDIMID = '" + dtItem.Rows[0]["INVENTDIMID"].ToString() + "' ");
            if (dr.Length > 0)
            {
                foreach (DataRow item in dr)
                {
                    if (double.Parse(item["QtyOrder"].ToString()) > 0)
                    {
                        MessageBox.Show("รายการที่เลือก มีอยู่ในบิลเรียบร้อยแล้ว ไม่สามารถเพิ่มรายการได้ " + Environment.NewLine +
                        "[ให้แก้ไขจำนวนการสั่งใหม่ แทนการเพิ่มรายการ]", SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }
            }
            Itembarcode.ItembarcodeDetail_Order _itembarcodeDetailOrder = new Itembarcode.ItembarcodeDetail_Order(dtItem, pCheckPO_6, pCheckSale_30, RadDropDownList_Branch.SelectedValue.ToString());
            if (_itembarcodeDetailOrder.ShowDialog(this) == DialogResult.Yes)
            {
                //ระบุจำนวน
                FormShare.InputData _inputdata = new FormShare.InputData("0",
                    _itembarcodeDetailOrder.sBarcode + "-" + _itembarcodeDetailOrder.sSpc_Name, "จำนวนที่ต้องการสั่ง", _itembarcodeDetailOrder.sUnitID)
                { pInputData = "1.00" };
                if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                {
                    //Check Qty
                    if (Convert.ToDouble(_inputdata.pInputData) > 1000)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่อนุญาติให้สั่งสินค้ามากกว่า 1000/รายการ ทุกกรณี");
                        radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus(); this.Cursor = Cursors.Default; return;
                    }

                    if (dt_PO.Rows.Count == 0)
                    {
                        SaveDataHD(_itembarcodeDetailOrder.sID, _itembarcodeDetailOrder.sDim, _itembarcodeDetailOrder.sBarcode,
                        _itembarcodeDetailOrder.sSpc_Name, Convert.ToDouble(_inputdata.pInputData),
                        _itembarcodeDetailOrder.sUnitID, Convert.ToDouble(_itembarcodeDetailOrder.sFactor),
                        _itembarcodeDetailOrder.sPurchase, _itembarcodeDetailOrder.sPOLast, _itembarcodeDetailOrder.sPOINVENT,
                        _itembarcodeDetailOrder.sPrice, _itembarcodeDetailOrder.sDimension, _itembarcodeDetailOrder.sPathImage, _itembarcodeDetailOrder.sVenderID);
                        this.Cursor = Cursors.Default;
                    }
                    else
                    {
                        SaveDataDT(_itembarcodeDetailOrder.sID, _itembarcodeDetailOrder.sDim, _itembarcodeDetailOrder.sBarcode,
                         _itembarcodeDetailOrder.sSpc_Name, Convert.ToDouble(_inputdata.pInputData),
                         _itembarcodeDetailOrder.sUnitID, Convert.ToDouble(_itembarcodeDetailOrder.sFactor),
                         _itembarcodeDetailOrder.sPurchase, _itembarcodeDetailOrder.sPOLast, _itembarcodeDetailOrder.sPOINVENT,
                         _itembarcodeDetailOrder.sPrice, _itembarcodeDetailOrder.sDimension, _itembarcodeDetailOrder.sPathImage, _itembarcodeDetailOrder.sVenderID);
                        this.Cursor = Cursors.Default;
                    }
                }
                else
                {
                    radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus(); this.Cursor = Cursors.Default; return;
                }
            }
            else
            {
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus(); this.Cursor = Cursors.Default; return;
            }


        }
        //Check Values In DGV IS NULL + ยืนยันหรือยัง
        string CheckValuesInDGV()
        {
            string rr = "1";
            switch (pApv)
            {
                case "0":
                    if ((RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() == "") ||
                           (RadGridView_Show.CurrentRow.Index == -1) ||
                           (RadGridView_Show.Rows.Count == 0))
                    {
                        rr = "1";//กรณีไม่มีค่า
                    }
                    else
                    {
                        rr = "0";//กรณีที่มีค่าให้ทำต่อได้
                    }
                    break;
                case "1":
                    rr = "1";
                    break;
                case "3":
                    rr = "1";
                    break;
                default:
                    break;
            }
            return rr;
        }
        //CellDoubleClick
        private void RadGridView_Show_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "QtyOrder"://แก้ไขจำนวนสั่ง
                    if (CheckValuesInDGV() == "1")
                    {
                        return;
                    }

                    FormShare.InputData _inputdata = new FormShare.InputData("0",
                         RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" + RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                         "จำนวนที่ต้องการสั่ง", RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                    {
                        pInputData = String.Format("{0:0.00}", Convert.ToDouble(RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString()))
                    };

                    if (_inputdata.ShowDialog(this) == DialogResult.Yes)
                    {
                        //ในกรณีที่บันทึกแล้ว
                        String T = ConnectionClass.ExecuteSQL_Main(String.Format(@"
                                UPDATE  SHOP_MNPO_DT
                                SET     MNPOQtyOrder = '" + _inputdata.pInputData + @"',MNPODateUp = CONVERT(VARCHAR,GETDATE(),23),MNPOTimeUp =  CONVERT(VARCHAR,GETDATE(),24),
                                        MNPOWhoUp = '" + SystemClass.SystemUserID + @"',MNPOWhoNameUp = '" + SystemClass.SystemUserName + @"' 
                                WHERE   MNPODocNo = '" + radLabel_Docno.Text + @"' 
                                        AND MNPOBarcode = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' ")
                        );
                        if (T != "")
                        {
                            MsgBoxClass.MsgBoxShow_SaveStatus(T);
                            return;
                        }

                        RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value = String.Format("{0:0.00}", Convert.ToDouble(_inputdata.pInputData));
                        dt_PO.AcceptChanges();
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                    }
                    else
                    {
                        radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus(); return;
                    }
                    break;
                default:
                    break;
            }
        }
        //Rows and Font
        private void RadGridView_Show_ViewCellFormatting_1(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }
        //กรณีที่ต้องการยเลิกการสั่งทั้งรายการ
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F3:
                    if (CheckValuesInDGV() == "1")
                    {
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete("ไม่สั่งรายการ " +
                        RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + "-" +
                        RadGridView_Show.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString() +
                         " จำนวน " + RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value.ToString() +
                         "   " + RadGridView_Show.CurrentRow.Cells["UNITID"].Value.ToString())
                        == DialogResult.No)
                    {
                        return;
                    }

                    String T = ConnectionClass.ExecuteSQL_Main(String.Format(@"
                            UPDATE  SHOP_MNPO_DT 
                            SET     MNPOQtyOrder = '0',MNPODateUp = CONVERT(VARCHAR,GETDATE(),23),MNPOTimeUp =  CONVERT(VARCHAR,GETDATE(),24),
                                    MNPOWhoUp = '" + SystemClass.SystemUserID + @"',MNPOWhoNameUp = '" + SystemClass.SystemUserName + @"' 
                            WHERE   MNPODocNo = '" + radLabel_Docno.Text + @"' 
                                    AND MNPOBarcode = '" + RadGridView_Show.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + @"' ")
                        );
                    if (T != "")
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        return;
                    }


                    RadGridView_Show.CurrentRow.Cells["QtyOrder"].Value = String.Format("{0:0.00}", 0);
                    dt_PO.AcceptChanges();
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    break;
                default:
                    break;
            }
        }
        //Save Data HD
        void SaveDataHD(string sID, string sDim, string sBarcode, string sSpc_Name,
            double pInputData, string sUnitID, double sFactor, string sPurchase, string sPOLast, string sPOINVENT, double sPrice, string sDimension, string sPathImage, string sVenderID)
        {

            string bchID = RadDropDownList_Branch.SelectedValue.ToString();
            string bchName = BranchClass.GetBranchNameByID(RadDropDownList_Branch.SelectedValue.ToString());
            string maxDocno = Class.ConfigClass.GetMaxINVOICEID("MNPO", "-", "MNPO", "1");

            ArrayList sqlIn = new ArrayList
            {
                String.Format(@"
                    INSERT INTO SHOP_MNPO_HD  
	                    ([MNPODocNo],[MNPOUserCode],[MNPOUserNameCode]
	                    ,[MNPOBranch],[MNPOBranchName],[MNPOTypeOrder],[MNPODptCode],[MNPORemark],[MNPOTYPE_PCORPDA])
                    VALUES ('" + maxDocno + @"','"+SystemClass.SystemUserID_M + @"','" + SystemClass.SystemUserName + @"',
                    '" + bchID  + @"','" +  bchName  + @"','"+_pTypeOrderOpen + @"',
                    '" + SystemClass.SystemDptID + @"','','" + SystemClass.SystemPcName + @"') ")
            };

            _ = sqlIn.Add(String.Format(@"
                    INSERT INTO SHOP_MNPO_DT  
                        ([MNPODocNo],[MNPOSeqNo],[MNPOItemID],[MNPODimid]
                       ,[MNPOBarcode],[MNPOName],[MNPOQtyOrder],[MNPOUnitID]
                       ,[MNPOFactor],[MNPODATEPO],[MNPODATEPR],[MNPOINVENT],PRICE,DIMENSION,PATHIMAGE,VENDERID ) 
                    VALUES ('" + maxDocno + @"','1',
                       '" + sID + @"',
                       '" + sDim + @"',
                       '" + sBarcode + @"',
                       '" + sSpc_Name.Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                       '" + Convert.ToDouble(pInputData) + @"',
                       '" + sUnitID + @"',
                       '" + Convert.ToDouble(sFactor) + @"',
                       '" + sPOLast + @"',
                       '" + sPurchase + @"','" + sPOINVENT + @"','" + sPrice + @"','" + sDimension + @"','" + sPathImage + @"','" + sVenderID + "') "));
            //}  
            String T = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn);
            if (T == "")
            {
                radLabel_Docno.Text = maxDocno;
                radLabel_StatusBill.Text = "สถานะบิล : ยังไม่ยืนยัน"; radLabel_StatusBill.ForeColor = Color.Black; radLabel_StatusBill.BackColor = ConfigClass.SetColor_GreenPastel();
                pApv = "0";// pSave = "1";
                RadButton_Apv.Enabled = true;
                RadButton_Cancel.Enabled = true;
                RadDropDownList_Branch.Enabled = false;

                dt_PO.Rows.Add(sID, sDim, sBarcode,
                       sSpc_Name, String.Format("{0:0.00}", Convert.ToDouble(pInputData)),
                       sUnitID, String.Format("{0:0.00}", Convert.ToDouble(sFactor)),
                       sPurchase, sPOLast);
                RadGridView_Show.DataSource = dt_PO;
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus(); return;

            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //Save Data DT
        void SaveDataDT(string sID, string sDim, string sBarcode, string sSpc_Name,
            double pInputData, string sUnitID, double sFactor, string sPurchase, string sPOLast, string sPOINVENT, double sPrice, string sDimension, string sPathImage, string sVenderID)
        {
            String T = ConnectionClass.ExecuteSQL_Main(String.Format(@"
                    INSERT INTO SHOP_MNPO_DT 
                        ([MNPODocNo],[MNPOSeqNo],[MNPOItemID],[MNPODimid]
                       ,[MNPOBarcode],[MNPOName],[MNPOQtyOrder],[MNPOUnitID]
                       ,[MNPOFactor],[MNPODATEPO],[MNPODATEPR],[MNPOINVENT],PRICE,DIMENSION,PATHIMAGE,VENDERID ) 
                    VALUES ('" + radLabel_Docno.Text + @"','" + (RadGridView_Show.Rows.Count + 1) + @"',
                       '" + sID + @"',
                       '" + sDim + @"',
                       '" + sBarcode + @"',
                       '" + sSpc_Name.Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                       '" + Convert.ToDouble(pInputData) + @"',
                       '" + sUnitID + @"',
                       '" + Convert.ToDouble(sFactor) + @"',
                       '" + sPOLast + @"',
                       '" + sPurchase + @"','" + sPOINVENT + @"','" + sPrice + @"','" + sDimension + @"','" + sPathImage + @"','" + sVenderID + "') "));
            if (T == "")
            {
                dt_PO.Rows.Add(sID, sDim, sBarcode,
                      sSpc_Name, String.Format("{0:0.00}", Convert.ToDouble(pInputData)),
                      sUnitID, String.Format("{0:0.00}", Convert.ToDouble(sFactor)),
                      sPurchase, sPOLast);
                RadGridView_Show.DataSource = dt_PO;
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus(); return;
            }
            else
            {
                MsgBoxClass.MsgBoxShow_SaveStatus(T);
            }
        }
        //Export Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_Show.Rows.Count == 0) return;
            string T = DatagridClass.ExportExcelGridView("รายการสั่งสินค้า " + radLabel_Docno.Text, RadGridView_Show, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Select Change
        private void RadDropDownList_Branch_SelectedValueChanged(object sender, EventArgs e)
        {
            radTextBox_Barcode.Focus();
        }
        //Doc
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, "");
        }
    }
}

