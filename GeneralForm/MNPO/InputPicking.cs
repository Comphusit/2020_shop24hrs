﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.Controllers;

namespace PC_Shop24Hrs.GeneralForm.MNPO
{
    public partial class InputPicking : Telerik.WinControls.UI.RadForm
    {
        public string pInputPicking;
        public string pInputPicking2;
        public string pInputPicking3;
        public double pNumber;
        // readonly string sTANumberOtText; //0 ต้องการเปนตัวเลข 1 ต้องการเป็นข้อความ
        readonly string _targetdms;
        readonly string _Case;//0 จ่ายบิล 1 ย้ายจัดซื้อ 2 พิมพ์ Container
        readonly string _checkBarcode;
        public InputPicking(string pCase, string targetdms, string checkBarcode)
        {
            InitializeComponent();
            _Case = pCase;
            _targetdms = targetdms;
            _checkBarcode = checkBarcode;
        }
        //load
        private void InputPicking_Load(object sender, EventArgs e)
        {
            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
            radTextBox_InputData.Visible = false;
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Emp);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_Location);
            radCheckBox_D094.ButtonElement.Font = SystemClass.SetFontGernaral;

            switch (_Case)
            {
                case "0":
                    this.Text = "ระบุข้อมูลการจ่ายบิล";
                    radCheckBox_D094.Visible = true;
                    if (_targetdms == "D101") radCheckBox_D094.Text = "พนักงาน PC"; else radCheckBox_D094.Text = "พนักงานจัดสินค้าเครดิต";
                    if (_targetdms == "D129") radCheckBox_D094.Text = "พนักงาน PC"; else radCheckBox_D094.Text = "พนักงานจัดสินค้าเครดิต";

                    radLabel_Head1.Text = "กำหนดคลังจัดสินค้า";
                    radLabel_Head2.Text = "กำหนดพนักงาน";
                    radLabel_Name1.Text = "คลัง";
                    radLabel_Name2.Text = "พนักงาน";
                    if (_checkBarcode == "1")
                    {
                        RadDropDownList_Location.Visible = false;
                        radLabel_Head1.Visible = false;
                        radLabel_Name1.Visible = false;
                    }
                    Set_GroupEmp();
                    Set_GroupLocation();
                    break;
                case "1":
                    this.Text = "ระบุข้อมูลการย้ายจัดซื้อ";
                    radCheckBox_D094.Visible = false;
                    radLabel_Head2.Text = "ย้ายจัดซื้อ";
                    radLabel_Name1.Visible = false;
                    radLabel_Head1.Visible = false;
                    RadDropDownList_Location.Visible = false;
                    radLabel_Name2.Text = "จัดซื้อ";
                    Set_GroupDMS();
                    break;
                case "2":
                    this.Text = "ระบุข้อมูลการพิมพ์เลข Container";
                    radCheckBox_D094.Visible = false;
                    radLabel_Head2.Text = "กำหนดพนักงาน";
                    radLabel_Name2.Text = "พนักงาน";
                    radTextBox_InputData.Visible = true; radTextBox_InputData.Text = "";
                    radLabel_Name1.Visible = true; radLabel_Name1.Text = "จำนวน";
                    radLabel_Head1.Visible = true; radLabel_Head1.Text = "ระบุจำนวนที่ต้องการ";
                    RadDropDownList_Location.Visible = false;
                    radButton_Save.Text = "ตกลง";
                    Set_GroupEmp();
                    radTextBox_InputData.Focus();
                    break;
                default:
                    break;
            }
        }
        //cancel
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //Save
        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            switch (_Case)
            {
                case "0":
                    EnterData();
                    break;
                case "1":
                    pInputPicking2 = RadDropDownList_Emp.SelectedValue.ToString();
                    this.DialogResult = DialogResult.Yes;
                    this.Close();
                    break;
                case "2":
                    SetEnter();
                    break;
                default:
                    break;
            }

        }
        //Set Data
        void EnterData()
        {
            pInputPicking = RadDropDownList_Location.SelectedValue.ToString();
            pInputPicking2 = RadDropDownList_Emp.SelectedValue.ToString();
            pInputPicking3 = RadDropDownList_Emp.SelectedItem[0].ToString().Replace($@"{pInputPicking2} - ", "");
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Set Enter Textbox + Case 2
        void SetEnter()
        {
            if (radTextBox_InputData.Text.Length == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุจำนวน Container ก่อนกดตกลง{Environment.NewLine}ลองใหม่อีกครั้ง");
                radTextBox_InputData.SelectAll();
                radTextBox_InputData.Focus();
                return;
            }
            if (double.Parse(radTextBox_InputData.Text) == 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ระบุจำนวน Container ต้องมากกว่า 0 {Environment.NewLine}ลองใหม่อีกครั้ง");
                radTextBox_InputData.SelectAll();
                radTextBox_InputData.Focus();
                return;
            }
            pInputPicking2 = RadDropDownList_Emp.SelectedValue.ToString();
            pInputPicking3 = RadDropDownList_Emp.SelectedItem[0].ToString().Replace($@"{pInputPicking2} - ", "");
            pNumber = double.Parse(radTextBox_InputData.Text);
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //พนักงานตามแผนกที่เลือก
        void Set_GroupEmp()
        {
            if (radCheckBox_D094.Checked == true)
            {
                if (_targetdms == "D101") RadDropDownList_Emp.DataSource = Models.EmplClass.GetEmployeeAll_ByDptID("1", "0", "D094", "", "");
                if (_targetdms == "D129") RadDropDownList_Emp.DataSource = Models.EmplClass.GetEmployeeAll_ByDptID("1", "0", "D094", "", "");
                else RadDropDownList_Emp.DataSource = Models.EmplClass.GetEmployeeAll_ByDptID("1", "0", "D168", "", "");
            }
            else if (_targetdms == "D048") RadDropDownList_Emp.DataSource = Models.EmplClass.GetEmployeeAll_ByDptID("1", "0", "D034", "", "");
            else RadDropDownList_Emp.DataSource = Models.EmplClass.GetEmployeeAll_ByDptID("1", "0", _targetdms, "", "");


            RadDropDownList_Emp.DisplayMember = "DISPLAYNAME";
            RadDropDownList_Emp.ValueMember = "EMPLID";
        }
        //คลังสินค้า
        void Set_GroupLocation()
        {
            RadDropDownList_Location.DataSource = Models.InventClass.Find_InventSupc($@" AND INVENTLOCATIONID IN ('WH-A','RETAILAREA','WH-RICE') ");
            RadDropDownList_Location.DisplayMember = "INVENTLOCATIONNAME";
            RadDropDownList_Location.ValueMember = "INVENTLOCATIONID";

        }
        //การแก้ไขสำหรับย้ายแผนก
        void Set_GroupDMS()
        {
            string fetchTargetDimension = PO_Class.FindDateDMS("5");
            DataTable targetDims = ConnectionClass.SelectSQL_Main(fetchTargetDimension);
            targetDims.Rows.Add(new Object[]{
                    "D156",
                    "D156 - คอมพิวเตอร์-โปรแกรมเมอร์"
                });
            foreach (DataRow dr in targetDims.Rows)
            {
                if (dr["DIMENSION"].ToString() == _targetdms)
                    dr.Delete();
            }
            RadDropDownList_Emp.DataSource = targetDims;
            RadDropDownList_Emp.DisplayMember = "DESCRIPTION";
            RadDropDownList_Emp.ValueMember = "DIMENSION";
        }
        //set Number only
        private void RadTextBox_InputData_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Enter texbox
        private void RadTextBox_InputData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SetEnter();
        }

        private void RadDropDownList_Emp_SelectedValueChanged(object sender, EventArgs e)
        {
            if (_Case == "2")
            {
                radTextBox_InputData.SelectAll();
                radTextBox_InputData.Focus();
            }
        }
        //เลือกจ่ายบิล PC
        private void RadCheckBox_D094_CheckStateChanged(object sender, EventArgs e)
        {
            Set_GroupEmp();
        }
    }
}
