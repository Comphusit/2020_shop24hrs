﻿using NVRCsharpDemo;
using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using PC_Shop24Hrs.Controllers;

namespace HikvisionPreview
{
    public partial class ShowSingleLarge : Form
    {

        private bool m_bInitSDK = false;

        public CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo;

        public string pCase;//ในกรณีที่กล้องเปิดอยู่แล้ว Login ได้แน่ๆให้ ส่ง 1 มาพร้อม 2 ตัวด้านล่าง
        public Int32 m_lUserID; //ในกรณีที่เป็น 1 ต้องส่ง มาด้วย
        public Int32 m_lRealHandle = -1; //ในกรณีที่เป็น 1 ต้องส่ง มาด้วย

        //public int CH;
        public string DVR_BchID;
        public string DVR_BchName;
        public string DVR_TypeCam;
        public int DVR_CH;
        public string DVR_IP;
        public string DVR_User;
        public string DVR_Password;
        public string pJOBMN;


        public ShowSingleLarge()
        {
            InitializeComponent();

            this.FormClosing += ShowSingleLarge_FormClosing;
        }

        //Close
        private void ShowSingleLarge_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (pCase == "1")
            {
                CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle);
                return;
            }
            else
            {
                if (m_lRealHandle >= 0)
                {
                    CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle);
                    m_lRealHandle = -1;
                }
                //注销登录
                if (m_lUserID >= 0)
                {
                    CHCNetSDK.NET_DVR_Logout(m_lUserID);
                    m_lUserID = -1;
                }
                CHCNetSDK.NET_DVR_Cleanup();
                return;
            }

        }
        //load
        public void InitSdk()
        {
            if (pCase == "1")
            {
                // m_lRealHandle = ClassShow.Preview(DVR_CH, pictureBoxPreview1, m_lUserID);
                m_lRealHandle = ClassShow.Preview(DVR_CH, pictureBoxPreview1, m_lUserID);
                if (pJOBMN != "")
                {
                    this.Text += " [กด F2 เพื่อ PrintScreen รูปเข้า JOB]";
                }
                return;
            }
            else
            {
                m_bInitSDK = CHCNetSDK.NET_DVR_Init();
                if (m_bInitSDK == false)
                {
                    this.Text = "ไม่สามารถเข้า DVR ได้.";
                    ClassShow.PathEmp(pictureBoxPreview1);
                    return;
                }

                m_lUserID = CHCNetSDK.NET_DVR_Login_V30(DVR_IP, ClassShow.DVRPortNumber, DVR_User, DVR_Password, ref DeviceInfo);
                if (m_lUserID < 0)
                {

                    this.Text = "รหัสผู้ใช้มีปัญหา ไม่สามารถ Login ได้.";
                    ClassShow.PathEmp(pictureBoxPreview1);
                    return;
                }
                else
                {
                    m_lRealHandle = ClassShow.Preview(DVR_CH, pictureBoxPreview1, m_lUserID);
                    if (pJOBMN != "")
                    {
                        this.Text += " [กด F2 เพื่อ PrintScreen รูปเข้า JOB]";
                    }
                    return;
                  
                }
            }
        }

        //Load
        private void ShowSingleLarge_Load(object sender, EventArgs e)
        {
            this.Text = DVR_BchID + "-" + DVR_BchName + " [" + DVR_TypeCam + "  ช่องที่ " + DVR_CH.ToString() + " ]";

            if ((DVR_IP == "") || (DVR_IP is null))
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบค่า IP ลองใหม่อีกครั้ง.");
                this.Close();
                return;
            }

            DVR_IP = DVR_IP.Replace("http://", "").Replace("/", "");
            //pCase = "0";
            InitSdk();
        }
        //Cap
        private void ShowSingleLarge_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                if (pJOBMN == "") { return; }

                try
                {
                    string[] jJobMN = pJOBMN.Split('|');
                    string sqlUp = jJobMN[0];
                    string path = jJobMN[1];
                    string filename = jJobMN[2];

                    if (Directory.Exists(path) == false) { Directory.CreateDirectory(path); }

                    Rectangle form = this.Bounds;
                    
                    Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
                    Graphics graphic = Graphics.FromImage(bitmap);

                    graphic.CopyFromScreen(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y, 0, 0, Screen.PrimaryScreen.Bounds.Size, CopyPixelOperation.SourceCopy);
                    bitmap.Save(path + filename);
                    if (sqlUp != "")
                    {
                        string T = ConnectionClass.ExecuteSQL_Main(sqlUp);
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("บันทึกหน้าจอเรียบร้อย.");
                    }
                }
                catch (Exception ex)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่สามารถใช้งาน ฟังก์ชั่นนี้ ได้ใหม่อีกครั้ง " + Environment.NewLine + "[" + ex.Message + "]");
                    return;
                }

            }
        }
    }
}
