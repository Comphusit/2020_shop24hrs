﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace PC_Shop24Hrs
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Telerik.WinControls.Themes.FluentTheme theme = new Telerik.WinControls.Themes.FluentTheme();
            Telerik.WinControls.ThemeResolutionService.ApplicationThemeName = theme.ThemeName;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");

            string strProcessName = Process.GetCurrentProcess().ProcessName;

            Process[] Oprocess = Process.GetProcessesByName(strProcessName);
            if (!(System.Environment.MachineName != "SPCN1467"))
            {

            }
            if ((Oprocess.Length > 1)
                    && (System.Environment.MachineName != "SPCN1465")
                    && (System.Environment.MachineName != "SPCN1467")
                    && (System.Environment.MachineName != "SPCN1468")

                    && (System.Environment.MachineName != "SPCN1520") //Hling
                    && (System.Environment.MachineName != "SPCN1525") //พี่แต
                    && (System.Environment.MachineName != "SPCN1529") //ไผ่

                    && (System.Environment.MachineName != "SPCN1521") //เที้ยน
                    && (System.Environment.MachineName != "SPCN1649") //กาย
                    && (System.Environment.MachineName != "SPCN1526") //เอส
                    && (System.Environment.MachineName != "SPCN1494") //มัง
                    && (System.Environment.MachineName != "SPCN1522") //เจ
                    && (System.Environment.MachineName != "SPCN1496") //โจ
                    && (System.Environment.MachineName != "SPCN1497") //ฟิวส์
                    && (System.Environment.MachineName != "SPCN1427") //เอ
                    && (System.Environment.MachineName != "SPCN2544") //ผัง
                    )
            {
                MessageBox.Show("ไม่สามารถเปิดใช้งานโปรแกรมซ้ำได้.",
                    Controllers.SystemClass.SystemHeadprogram, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainMenu());
            }

        }
    }
}

