﻿namespace PC_Shop24Hrs.Itembarcode
{
    partial class ItembarcodeDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItembarcodeDetail));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.RadGridView_ItemBarcode = new Telerik.WinControls.UI.RadGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_DptHead = new Telerik.WinControls.UI.RadLabel();
            this.button_NoOrder = new System.Windows.Forms.Button();
            this.radButton_pdt = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox_SPC_GiftPoint = new Telerik.WinControls.UI.RadCheckBox();
            this.Button_pc = new System.Windows.Forms.Button();
            this.radCheckBox_PO = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox_Sale = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel_Stock = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_StockShow = new Telerik.WinControls.UI.RadLabel();
            this.radButton_Refresh = new Telerik.WinControls.UI.RadButton();
            this.RadButton_Find = new Telerik.WinControls.UI.RadButton();
            this.radLabel_Tel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_GroupSub1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_GroupSub2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Vender = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_GroupMain = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_SpcName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Dpt = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_ItemID = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Dim = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.pictureBox_Barcode = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_NotPO = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_NoSale = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Sale = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_PO = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ItemBarcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ItemBarcode.MasterTemplate)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_SPC_GiftPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_PO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Sale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Stock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_StockShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Refresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Find)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_GroupSub1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_GroupSub2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Vender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_GroupMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SpcName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ItemID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Barcode)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NotPO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NoSale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Sale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.RadGridView_ItemBarcode);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel1.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radPanel1.Location = new System.Drawing.Point(3, 323);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(786, 188);
            this.radPanel1.TabIndex = 4;
            // 
            // RadGridView_ItemBarcode
            // 
            this.RadGridView_ItemBarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadGridView_ItemBarcode.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.RadGridView_ItemBarcode.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.RadGridView_ItemBarcode.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.RadGridView_ItemBarcode.Name = "RadGridView_ItemBarcode";
            // 
            // 
            // 
            this.RadGridView_ItemBarcode.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.RadGridView_ItemBarcode.Size = new System.Drawing.Size(786, 188);
            this.RadGridView_ItemBarcode.TabIndex = 0;
            this.RadGridView_ItemBarcode.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.RadGridView_ItemBarcode.SelectionChanged += new System.EventHandler(this.RadGridView_ItemBarcode_SelectionChanged);
            this.RadGridView_ItemBarcode.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ItemBarcode_CellClick);
            this.RadGridView_ItemBarcode.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.RadGridView_ItemBarcode_CellDoubleClick);
            this.RadGridView_ItemBarcode.ConditionalFormattingFormShown += new System.EventHandler(this.RadGridView_Show_ConditionalFormattingFormShown);
            this.RadGridView_ItemBarcode.FilterPopupRequired += new Telerik.WinControls.UI.FilterPopupRequiredEventHandler(this.RadGridView_Show_FilterPopupRequired);
            this.RadGridView_ItemBarcode.FilterPopupInitialized += new Telerik.WinControls.UI.FilterPopupInitializedEventHandler(this.RadGridView_Show_FilterPopupInitialized);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radPanel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 320F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(792, 574);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.68421F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.31579F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox_Barcode, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(786, 314);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel_DptHead);
            this.panel1.Controls.Add(this.button_NoOrder);
            this.panel1.Controls.Add(this.radButton_pdt);
            this.panel1.Controls.Add(this.radCheckBox_SPC_GiftPoint);
            this.panel1.Controls.Add(this.Button_pc);
            this.panel1.Controls.Add(this.radCheckBox_PO);
            this.panel1.Controls.Add(this.radCheckBox_Sale);
            this.panel1.Controls.Add(this.radLabel_Stock);
            this.panel1.Controls.Add(this.radLabel_StockShow);
            this.panel1.Controls.Add(this.radButton_Refresh);
            this.panel1.Controls.Add(this.RadButton_Find);
            this.panel1.Controls.Add(this.radLabel_Tel);
            this.panel1.Controls.Add(this.radLabel_GroupSub1);
            this.panel1.Controls.Add(this.radLabel_GroupSub2);
            this.panel1.Controls.Add(this.radLabel_Vender);
            this.panel1.Controls.Add(this.radLabel_GroupMain);
            this.panel1.Controls.Add(this.radLabel_SpcName);
            this.panel1.Controls.Add(this.radLabel_Dpt);
            this.panel1.Controls.Add(this.radLabel10);
            this.panel1.Controls.Add(this.radLabel_ItemID);
            this.panel1.Controls.Add(this.radLabel7);
            this.panel1.Controls.Add(this.radLabel6);
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel_1);
            this.panel1.Controls.Add(this.radLabel_Dim);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radTextBox_Barcode);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(573, 308);
            this.panel1.TabIndex = 0;
            // 
            // radLabel_DptHead
            // 
            this.radLabel_DptHead.AutoSize = false;
            this.radLabel_DptHead.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_DptHead.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_DptHead.Location = new System.Drawing.Point(332, 144);
            this.radLabel_DptHead.Name = "radLabel_DptHead";
            this.radLabel_DptHead.Size = new System.Drawing.Size(219, 23);
            this.radLabel_DptHead.TabIndex = 79;
            this.radLabel_DptHead.Text = ":";
            // 
            // button_NoOrder
            // 
            this.button_NoOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(219)))), ((int)(((byte)(124)))));
            this.button_NoOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_NoOrder.ForeColor = System.Drawing.Color.Black;
            this.button_NoOrder.Location = new System.Drawing.Point(131, 272);
            this.button_NoOrder.Name = "button_NoOrder";
            this.button_NoOrder.Size = new System.Drawing.Size(112, 31);
            this.button_NoOrder.TabIndex = 78;
            this.button_NoOrder.Text = "ปลดล็อกห้ามสั่ง";
            this.button_NoOrder.UseVisualStyleBackColor = false;
            this.button_NoOrder.Click += new System.EventHandler(this.Button_NoOrder_Click);
            // 
            // radButton_pdt
            // 
            this.radButton_pdt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radButton_pdt.BackColor = System.Drawing.Color.Transparent;
            this.radButton_pdt.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_pdt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_pdt.Image = global::PC_Shop24Hrs.Properties.Resources.pdf;
            this.radButton_pdt.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_pdt.Location = new System.Drawing.Point(539, 3);
            this.radButton_pdt.Name = "radButton_pdt";
            this.radButton_pdt.Size = new System.Drawing.Size(26, 26);
            this.radButton_pdt.TabIndex = 71;
            this.radButton_pdt.Text = "radButton3";
            this.radButton_pdt.Click += new System.EventHandler(this.RadButton_pdt_Click);
            // 
            // radCheckBox_SPC_GiftPoint
            // 
            this.radCheckBox_SPC_GiftPoint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radCheckBox_SPC_GiftPoint.Enabled = false;
            this.radCheckBox_SPC_GiftPoint.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_SPC_GiftPoint.ForeColor = System.Drawing.Color.Black;
            this.radCheckBox_SPC_GiftPoint.Location = new System.Drawing.Point(505, 69);
            this.radCheckBox_SPC_GiftPoint.Name = "radCheckBox_SPC_GiftPoint";
            this.radCheckBox_SPC_GiftPoint.Size = new System.Drawing.Size(60, 18);
            this.radCheckBox_SPC_GiftPoint.TabIndex = 77;
            this.radCheckBox_SPC_GiftPoint.Text = "ให้แต้ม";
            // 
            // Button_pc
            // 
            this.Button_pc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(219)))), ((int)(((byte)(124)))));
            this.Button_pc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_pc.ForeColor = System.Drawing.Color.Black;
            this.Button_pc.Location = new System.Drawing.Point(13, 272);
            this.Button_pc.Name = "Button_pc";
            this.Button_pc.Size = new System.Drawing.Size(112, 31);
            this.Button_pc.TabIndex = 76;
            this.Button_pc.Text = "ประวัติสินค้า";
            this.Button_pc.UseVisualStyleBackColor = false;
            this.Button_pc.Click += new System.EventHandler(this.Button_pc_Click);
            // 
            // radCheckBox_PO
            // 
            this.radCheckBox_PO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_PO.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_PO.Location = new System.Drawing.Point(49, 69);
            this.radCheckBox_PO.Name = "radCheckBox_PO";
            this.radCheckBox_PO.Size = new System.Drawing.Size(149, 18);
            this.radCheckBox_PO.TabIndex = 75;
            this.radCheckBox_PO.Text = "ประวัติสั่งย้อนหลัง 6 วัน";
            // 
            // radCheckBox_Sale
            // 
            this.radCheckBox_Sale.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.radCheckBox_Sale.ForeColor = System.Drawing.Color.Blue;
            this.radCheckBox_Sale.Location = new System.Drawing.Point(230, 66);
            this.radCheckBox_Sale.Name = "radCheckBox_Sale";
            this.radCheckBox_Sale.Size = new System.Drawing.Size(166, 18);
            this.radCheckBox_Sale.TabIndex = 74;
            this.radCheckBox_Sale.Text = "ประวัติขายย้อนหลัง 30 วัน";
            // 
            // radLabel_Stock
            // 
            this.radLabel_Stock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_Stock.AutoSize = false;
            this.radLabel_Stock.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Stock.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Stock.Location = new System.Drawing.Point(442, 38);
            this.radLabel_Stock.Name = "radLabel_Stock";
            this.radLabel_Stock.Size = new System.Drawing.Size(128, 23);
            this.radLabel_Stock.TabIndex = 73;
            this.radLabel_Stock.Text = ":";
            this.radLabel_Stock.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radLabel_StockShow
            // 
            this.radLabel_StockShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radLabel_StockShow.AutoSize = false;
            this.radLabel_StockShow.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_StockShow.Location = new System.Drawing.Point(343, 39);
            this.radLabel_StockShow.Name = "radLabel_StockShow";
            this.radLabel_StockShow.Size = new System.Drawing.Size(101, 19);
            this.radLabel_StockShow.TabIndex = 72;
            this.radLabel_StockShow.Text = "ปริมาณคงเหลือ";
            this.radLabel_StockShow.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radButton_Refresh
            // 
            this.radButton_Refresh.BackColor = System.Drawing.Color.Transparent;
            this.radButton_Refresh.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.radButton_Refresh.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radButton_Refresh.Image = global::PC_Shop24Hrs.Properties.Resources.refresh;
            this.radButton_Refresh.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radButton_Refresh.Location = new System.Drawing.Point(13, 34);
            this.radButton_Refresh.Name = "radButton_Refresh";
            this.radButton_Refresh.Size = new System.Drawing.Size(26, 26);
            this.radButton_Refresh.TabIndex = 71;
            this.radButton_Refresh.Text = "radButton3";
            this.radButton_Refresh.Click += new System.EventHandler(this.RadButton_Refresh_Click);
            // 
            // RadButton_Find
            // 
            this.RadButton_Find.BackColor = System.Drawing.Color.Transparent;
            this.RadButton_Find.DisplayStyle = Telerik.WinControls.DisplayStyle.Image;
            this.RadButton_Find.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadButton_Find.Image = global::PC_Shop24Hrs.Properties.Resources.search;
            this.RadButton_Find.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.RadButton_Find.Location = new System.Drawing.Point(230, 34);
            this.RadButton_Find.Name = "RadButton_Find";
            this.RadButton_Find.Size = new System.Drawing.Size(26, 26);
            this.RadButton_Find.TabIndex = 70;
            this.RadButton_Find.Text = "radButton3";
            this.RadButton_Find.Click += new System.EventHandler(this.RadButton_Find_Click);
            // 
            // radLabel_Tel
            // 
            this.radLabel_Tel.AutoSize = false;
            this.radLabel_Tel.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Tel.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Tel.Location = new System.Drawing.Point(142, 249);
            this.radLabel_Tel.Name = "radLabel_Tel";
            this.radLabel_Tel.Size = new System.Drawing.Size(409, 23);
            this.radLabel_Tel.TabIndex = 69;
            this.radLabel_Tel.Text = ":";
            // 
            // radLabel_GroupSub1
            // 
            this.radLabel_GroupSub1.AutoSize = false;
            this.radLabel_GroupSub1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_GroupSub1.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_GroupSub1.Location = new System.Drawing.Point(142, 220);
            this.radLabel_GroupSub1.Name = "radLabel_GroupSub1";
            this.radLabel_GroupSub1.Size = new System.Drawing.Size(184, 23);
            this.radLabel_GroupSub1.TabIndex = 68;
            this.radLabel_GroupSub1.Text = ":";
            // 
            // radLabel_GroupSub2
            // 
            this.radLabel_GroupSub2.AutoSize = false;
            this.radLabel_GroupSub2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_GroupSub2.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_GroupSub2.Location = new System.Drawing.Point(332, 220);
            this.radLabel_GroupSub2.Name = "radLabel_GroupSub2";
            this.radLabel_GroupSub2.Size = new System.Drawing.Size(219, 23);
            this.radLabel_GroupSub2.TabIndex = 67;
            this.radLabel_GroupSub2.Text = ":";
            // 
            // radLabel_Vender
            // 
            this.radLabel_Vender.AutoSize = false;
            this.radLabel_Vender.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Vender.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Vender.Location = new System.Drawing.Point(142, 170);
            this.radLabel_Vender.Name = "radLabel_Vender";
            this.radLabel_Vender.Size = new System.Drawing.Size(409, 23);
            this.radLabel_Vender.TabIndex = 66;
            this.radLabel_Vender.Text = ":";
            // 
            // radLabel_GroupMain
            // 
            this.radLabel_GroupMain.AutoSize = false;
            this.radLabel_GroupMain.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_GroupMain.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_GroupMain.Location = new System.Drawing.Point(142, 196);
            this.radLabel_GroupMain.Name = "radLabel_GroupMain";
            this.radLabel_GroupMain.Size = new System.Drawing.Size(409, 23);
            this.radLabel_GroupMain.TabIndex = 65;
            this.radLabel_GroupMain.Text = ":";
            // 
            // radLabel_SpcName
            // 
            this.radLabel_SpcName.AutoSize = false;
            this.radLabel_SpcName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_SpcName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_SpcName.Location = new System.Drawing.Point(142, 119);
            this.radLabel_SpcName.Name = "radLabel_SpcName";
            this.radLabel_SpcName.Size = new System.Drawing.Size(409, 23);
            this.radLabel_SpcName.TabIndex = 64;
            this.radLabel_SpcName.Text = ":";
            // 
            // radLabel_Dpt
            // 
            this.radLabel_Dpt.AutoSize = false;
            this.radLabel_Dpt.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Dpt.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Dpt.Location = new System.Drawing.Point(142, 145);
            this.radLabel_Dpt.Name = "radLabel_Dpt";
            this.radLabel_Dpt.Size = new System.Drawing.Size(184, 23);
            this.radLabel_Dpt.TabIndex = 63;
            this.radLabel_Dpt.Text = ":";
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel10.Location = new System.Drawing.Point(13, 249);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(108, 19);
            this.radLabel10.TabIndex = 62;
            this.radLabel10.Text = "เบอร์ติดต่อจัดซื้อ";
            // 
            // radLabel_ItemID
            // 
            this.radLabel_ItemID.AutoSize = false;
            this.radLabel_ItemID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_ItemID.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_ItemID.Location = new System.Drawing.Point(142, 93);
            this.radLabel_ItemID.Name = "radLabel_ItemID";
            this.radLabel_ItemID.Size = new System.Drawing.Size(184, 23);
            this.radLabel_ItemID.TabIndex = 61;
            this.radLabel_ItemID.Text = ":";
            // 
            // radLabel7
            // 
            this.radLabel7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel7.Location = new System.Drawing.Point(13, 221);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(89, 19);
            this.radLabel7.TabIndex = 59;
            this.radLabel7.Text = "กลุ่มย่อย 1/2";
            // 
            // radLabel6
            // 
            this.radLabel6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel6.Location = new System.Drawing.Point(13, 196);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(61, 19);
            this.radLabel6.TabIndex = 58;
            this.radLabel6.Text = "กลุ่มหลัก";
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel5.Location = new System.Drawing.Point(13, 171);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(67, 19);
            this.radLabel5.TabIndex = 57;
            this.radLabel5.Text = "ผู้จำหน่าย";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel3.Location = new System.Drawing.Point(13, 146);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(43, 19);
            this.radLabel3.TabIndex = 56;
            this.radLabel3.Text = "จัดซื้อ";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(13, 121);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(59, 19);
            this.radLabel2.TabIndex = 55;
            this.radLabel2.Text = "ชื่อสินค้า";
            // 
            // radLabel_1
            // 
            this.radLabel_1.AutoSize = false;
            this.radLabel_1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_1.Location = new System.Drawing.Point(13, 6);
            this.radLabel_1.Name = "radLabel_1";
            this.radLabel_1.Size = new System.Drawing.Size(487, 19);
            this.radLabel_1.TabIndex = 53;
            this.radLabel_1.Text = "ป้อนบาร์โค้ด Enter : ป้อนบาร์โค้ดกด F4 : ป้อนชื่อสินค้ากด F1";
            // 
            // radLabel_Dim
            // 
            this.radLabel_Dim.AutoSize = false;
            this.radLabel_Dim.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Dim.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Dim.Location = new System.Drawing.Point(332, 93);
            this.radLabel_Dim.Name = "radLabel_Dim";
            this.radLabel_Dim.Size = new System.Drawing.Size(219, 23);
            this.radLabel_Dim.TabIndex = 52;
            this.radLabel_Dim.Text = ":";
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel4.Location = new System.Drawing.Point(13, 95);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(66, 19);
            this.radLabel4.TabIndex = 51;
            this.radLabel4.Text = "รหัสสินค้า";
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.AutoSize = false;
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 12F);
            this.radTextBox_Barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Barcode.Location = new System.Drawing.Point(49, 30);
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(175, 33);
            this.radTextBox_Barcode.TabIndex = 0;
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            this.radTextBox_Barcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_Barcode_KeyPress);
            this.radTextBox_Barcode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyUp);
            // 
            // pictureBox_Barcode
            // 
            this.pictureBox_Barcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_Barcode.Location = new System.Drawing.Point(582, 3);
            this.pictureBox_Barcode.Name = "pictureBox_Barcode";
            this.pictureBox_Barcode.Size = new System.Drawing.Size(201, 308);
            this.pictureBox_Barcode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_Barcode.TabIndex = 1;
            this.pictureBox_Barcode.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_NotPO, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_NoSale, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_Sale, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_PO, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 517);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(786, 54);
            this.tableLayoutPanel3.TabIndex = 6;
            // 
            // radLabel_NotPO
            // 
            this.radLabel_NotPO.AutoSize = false;
            this.radLabel_NotPO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_NotPO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_NotPO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_NotPO.ForeColor = System.Drawing.Color.Black;
            this.radLabel_NotPO.Location = new System.Drawing.Point(396, 3);
            this.radLabel_NotPO.Name = "radLabel_NotPO";
            this.radLabel_NotPO.Size = new System.Drawing.Size(387, 21);
            this.radLabel_NotPO.TabIndex = 54;
            this.radLabel_NotPO.Text = "<html>ห้ามสั่ง</html>";
            // 
            // radLabel_NoSale
            // 
            this.radLabel_NoSale.AutoSize = false;
            this.radLabel_NoSale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_NoSale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_NoSale.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_NoSale.ForeColor = System.Drawing.Color.Black;
            this.radLabel_NoSale.Location = new System.Drawing.Point(3, 3);
            this.radLabel_NoSale.Name = "radLabel_NoSale";
            this.radLabel_NoSale.Size = new System.Drawing.Size(387, 21);
            this.radLabel_NoSale.TabIndex = 53;
            this.radLabel_NoSale.Text = "<html>ห้ามขาย</html>";
            // 
            // radLabel_Sale
            // 
            this.radLabel_Sale.AutoSize = false;
            this.radLabel_Sale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_Sale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Sale.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Sale.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Sale.Location = new System.Drawing.Point(396, 30);
            this.radLabel_Sale.Name = "radLabel_Sale";
            this.radLabel_Sale.Size = new System.Drawing.Size(387, 21);
            this.radLabel_Sale.TabIndex = 52;
            this.radLabel_Sale.Text = "<html>ประวัติการขายย้อนหลัง</html>";
            // 
            // radLabel_PO
            // 
            this.radLabel_PO.AutoSize = false;
            this.radLabel_PO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            this.radLabel_PO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_PO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_PO.ForeColor = System.Drawing.Color.Black;
            this.radLabel_PO.Location = new System.Drawing.Point(3, 30);
            this.radLabel_PO.Name = "radLabel_PO";
            this.radLabel_PO.Size = new System.Drawing.Size(387, 21);
            this.radLabel_PO.TabIndex = 55;
            this.radLabel_PO.Text = "<html>ประวัติการสั่งย้อนหลัง</html>";
            // 
            // ItembarcodeDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 574);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ItembarcodeDetail";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "สินค้า";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ItembarcodeDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ItemBarcode.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadGridView_ItemBarcode)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_DptHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_pdt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_SPC_GiftPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_PO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox_Sale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Stock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_StockShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Refresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadButton_Find)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_GroupSub1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_GroupSub2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Vender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_GroupMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_SpcName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ItemID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Dim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Barcode)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NotPO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_NoSale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Sale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_PO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGridView RadGridView_ItemBarcode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadLabel radLabel_Dim;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel_1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel_ItemID;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel_GroupSub1;
        private Telerik.WinControls.UI.RadLabel radLabel_GroupSub2;
        private Telerik.WinControls.UI.RadLabel radLabel_Vender;
        private Telerik.WinControls.UI.RadLabel radLabel_GroupMain;
        private Telerik.WinControls.UI.RadLabel radLabel_SpcName;
        private Telerik.WinControls.UI.RadLabel radLabel_Dpt;
        private Telerik.WinControls.UI.RadLabel radLabel_Tel;
        private System.Windows.Forms.PictureBox pictureBox_Barcode;
        private Telerik.WinControls.UI.RadButton RadButton_Find;
        private Telerik.WinControls.UI.RadButton radButton_Refresh;
        private Telerik.WinControls.UI.RadLabel radLabel_StockShow;
        private Telerik.WinControls.UI.RadLabel radLabel_Stock;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private Telerik.WinControls.UI.RadLabel radLabel_PO;
        private Telerik.WinControls.UI.RadLabel radLabel_NotPO;
        private Telerik.WinControls.UI.RadLabel radLabel_NoSale;
        private Telerik.WinControls.UI.RadLabel radLabel_Sale;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_PO;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_Sale;
        private System.Windows.Forms.Button Button_pc;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox_SPC_GiftPoint;
        private Telerik.WinControls.UI.RadButton radButton_pdt;
        private System.Windows.Forms.Button button_NoOrder;
        private Telerik.WinControls.UI.RadLabel radLabel_DptHead;
    }
}
