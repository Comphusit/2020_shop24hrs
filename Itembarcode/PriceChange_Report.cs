﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.Collections;

namespace PC_Shop24Hrs.Itembarcode
{
    public partial class PriceChange_Report : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_Data = new DataTable();
        readonly string _pTypeReport;
        //0 สินค้าเปลี่ยนแปลงราคา
        //1 สินค้าที่ยิงเพื่อเปลี่ยนป้ายราคา

        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();

        //Load
        public PriceChange_Report(string pTypeReport)
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
        }
        //Load
        private void PriceChange_Report_Load(object sender, EventArgs e)
        {
            radButtonElement_add.ShowBorder = true; radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล";
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            RadButton_Search.ButtonElement.ShowBorder = true;

            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            DatagridClass.SetDefaultFontDropDown(radDropDownList_Grp);

            radCheckBox_Grp.ButtonElement.Font = SystemClass.SetFontGernaral;

            radLabel_Detail.Visible = false;
            radRadioButton_P1.Visible = false; radRadioButton_P2.Visible = false;
            radDropDownList_Grp.Visible = false; radCheckBox_Grp.Visible = false;

            switch (_pTypeReport)
            {
                case "0": //0 สินค้าเปลี่ยนแปลงราคา
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "สีฟ้า >> สินค้าที่ปรับราคาขึ้น";
                    radDropDownList_Grp.DataSource = Models.DptClass.GetDpt_Purchase();
                    radDropDownList_Grp.ValueMember = "NUM";
                    radDropDownList_Grp.DisplayMember = "DESCRIPTION";

                    radRadioButton_P1.Visible = true; radRadioButton_P2.Visible = true;
                    radCheckBox_Grp.Visible = true; radDropDownList_Grp.Visible = true;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("C", "เลือก")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MAXPRT", "พิมพ์ล่าสุด", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("NEWPRICESALES", "ราคาใหม่", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("ORIGPRICESALES", "ราคาเก่า", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "จัดซื้อ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TIMEEDIT", "เวลาปรับ", 180)));

                    RadGridView_ShowHD.MasterTemplate.Columns["NEWPRICESALES"].FormatString = "{0:N2}";
                    RadGridView_ShowHD.MasterTemplate.Columns["ORIGPRICESALES"].FormatString = "{0:N2}";

                    ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "NEWPRICESALES" + " > " + " ORIGPRICESALES ", false)
                    { CellBackColor = ConfigClass.SetColor_SkyPastel() };
                    this.RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(obj2);
                    this.RadGridView_ShowHD.Columns["NEWPRICESALES"].ConditionalFormattingObjectList.Add(obj2);

                    break;
                case "1"://1 สินค้าที่ยิงเพื่อเปลี่ยนป้ายราคา
                    radDropDownList_Grp.DataSource = Models.DptClass.GetDpt_AllD();
                    radDropDownList_Grp.ValueMember = "NUM";
                    radDropDownList_Grp.DisplayMember = "DESCRIPTION";
                    radCheckBox_Grp.Visible = true; radDropDownList_Grp.Visible = true; radCheckBox_Grp.Checked = true;
                    radDropDownList_Grp.SelectedValue = SystemClass.SystemDptID;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("C", "เลือก")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MAXPRT", "พิมพ์ล่าสุด", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CheckDate", "วันที่เช็ค", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 160)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("CheckPrice", "ราคาเช็ค", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DptName", "จัดซื้อ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CheckDepartName", "แผนกรับผิดชอบ", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CheckWhoName", "ผู้เช็ค", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns["CheckPrice"].FormatString = "{0:N2}";
                    break;
                default:
                    break;
            }

            ClearTxt();
        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); this.RadGridView_ShowHD.FilterDescriptors.Clear(); }

            switch (_pTypeReport)
            {
                case "0": //0 สินค้าเปลี่ยนแปลงราคา

                    string buyerID = "";
                    if (radCheckBox_Grp.Checked == true) buyerID = radDropDownList_Grp.SelectedValue.ToString();

                    string conditionQty = "";
                    if (radRadioButton_P1.CheckState == CheckState.Checked) conditionQty = " AND QTY = 1 ";
                    if (radRadioButton_P2.CheckState == CheckState.Checked) conditionQty = " AND QTY > 1 ";

                    dt_Data = ItembarcodeClass.GetPriceChange("0", buyerID, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"),
                                    radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), conditionQty); //ConnectionClass.SelectSQL_Main(sql0);
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;

                case "1": //1 สินค้าที่ยิงเพื่อเปลี่ยนป้ายราคา
                    string DptID = "";
                    if (radCheckBox_Grp.Checked == true) DptID = radDropDownList_Grp.SelectedValue.ToString();

                    dt_Data = ItembarcodeClass.GetPriceChange("1", DptID, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), ""); //ConnectionClass.SelectSQL_Main(sql0);
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }

        }


        #region "ROWS DGV"
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        #endregion


        //Clear
        void ClearTxt()
        {
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }

            radDateTimePicker_D1.Value = DateTime.Now;
            radDateTimePicker_D2.Value = DateTime.Now;

            RadButton_Search.Focus();
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }


        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }

            switch (_pTypeReport)
            {
                case "0":
                    ArrayList sqlIn0 = new ArrayList();
                    foreach (var item in RadGridView_ShowHD.Rows)
                    {
                        if (item.Cells["C"].Value.ToString() == "1")
                        {
                            //sqlIn0.Add(string.Format(@"
                            //    INSERT INTO SHOP_ITEMPRTBARCODE (
                            //    Branch,ItemBarcode,ItemName,ItemPrice, ItemUnit, WhoIns) VALUES ( 
                            //    'RT','" + item.Cells["ITEMBARCODE"].Value.ToString() + @"','" + item.Cells["SPC_ITEMNAME"].Value.ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"',
                            //    '" + item.Cells["NEWPRICESALES"].Value.ToString() + @"','" + item.Cells["UNITID"].Value.ToString() + @"','" + SystemClass.SystemUserID + @"') 
                            //"));
                            sqlIn0.Add(ItembarcodeClass.Save_PriceChange(item.Cells["ITEMBARCODE"].Value.ToString(), item.Cells["SPC_ITEMNAME"].Value.ToString(),
                               item.Cells["UNITID"].Value.ToString(), Double.Parse(item.Cells["NEWPRICESALES"].Value.ToString())));
                        }
                    }

                    if (sqlIn0.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลบาร์โค้ดที่เลือก ไม่สามารถ Export To Excel ได้{Environment.NewLine}ลองใหม่อีกครั้ง.");
                        return;
                    }

                    CompositeFilterDescriptor compositeFilter = new CompositeFilterDescriptor();
                    compositeFilter.FilterDescriptors.Add(new FilterDescriptor("C", FilterOperator.IsEqualTo, "1"));
                    compositeFilter.LogicalOperator = FilterLogicalOperator.And;
                    this.RadGridView_ShowHD.FilterDescriptors.Add(compositeFilter);

                    string TT = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn0);
                    if (TT == "")
                    {
                        string T = DatagridClass.ExportExcelGridView("ข้อมูลการเปลี่ยนแปลงราคา", RadGridView_ShowHD, "1");
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        SetDGV_HD();
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(TT);
                    }
                    break;
                case "1":

                    ArrayList sqlIn1 = new ArrayList();
                    foreach (var item in RadGridView_ShowHD.Rows)
                    {
                        if (item.Cells["C"].Value.ToString() == "1")
                        {
                            //sqlIn1.Add(string.Format(@"
                            //    INSERT INTO SHOP_ITEMPRTBARCODE (
                            //    Branch,ItemBarcode,ItemName,ItemPrice, ItemUnit, WhoIns) VALUES ( 
                            //    'RT','" + item.Cells["ITEMBARCODE"].Value.ToString() + @"',
                            //    '" + item.Cells["SPC_ITEMNAME"].Value.ToString().Replace("{", " ").Replace("}", " ").Replace("'", " ") + @"',
                            //    '" + item.Cells["CheckPrice"].Value.ToString() + @"','" + item.Cells["UNITID"].Value.ToString() + @"','" + SystemClass.SystemUserID + @"') 
                            //"));
                            sqlIn1.Add(ItembarcodeClass.Save_PriceChange(item.Cells["ITEMBARCODE"].Value.ToString(), item.Cells["SPC_ITEMNAME"].Value.ToString(),
                                 item.Cells["UNITID"].Value.ToString(), Double.Parse(item.Cells["CheckPrice"].Value.ToString())));
                        }
                    }

                    if (sqlIn1.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ไม่พบข้อมูลบาร์โค้ดที่เลือก ไม่สามารถ Export To Excel ได้{Environment.NewLine}ลองใหม่อีกครั้ง.");
                        return;
                    }

                    CompositeFilterDescriptor compositeFilter1 = new CompositeFilterDescriptor();
                    compositeFilter1.FilterDescriptors.Add(new FilterDescriptor("C", FilterOperator.IsEqualTo, "1"));
                    compositeFilter1.LogicalOperator = FilterLogicalOperator.And;
                    this.RadGridView_ShowHD.FilterDescriptors.Add(compositeFilter1);

                    string TT1 = ConnectionClass.ExecuteSQL_ArrayMain(sqlIn1);
                    if (TT1 == "")
                    {
                        string T1 = DatagridClass.ExportExcelGridView("ข้อมูลการเปลี่ยนป้ายราคา", RadGridView_ShowHD, "1");
                        MsgBoxClass.MsgBoxShow_SaveStatus(T1);
                        SetDGV_HD();
                    }
                    else
                    {
                        MsgBoxClass.MsgBoxShow_SaveStatus(TT1);
                    }
                    break;
                default:
                    break;
            }


        }

        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;
        }

        private void RadGridView_ShowHD_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) { return; }

            switch (_pTypeReport)
            {
                case "0":
                    if (e.Column.Name == "C")
                    {
                        if (RadGridView_ShowHD.CurrentRow.Cells["C"].Value.ToString() == "1") RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "0";
                        else RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "1";

                    }
                    break;
                case "1":
                    if (e.Column.Name == "C")
                    {
                        if (RadGridView_ShowHD.CurrentRow.Cells["C"].Value.ToString() == "1") RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "0";
                        else RadGridView_ShowHD.CurrentRow.Cells["C"].Value = "1";
                    }
                    break;
                default:
                    break;
            }

        }
        //Document
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, _pTypeReport);
        }

        private void RadCheckBox_Grp_CheckStateChanged(object sender, EventArgs e)
        {
            if (radCheckBox_Grp.Checked == true) radDropDownList_Grp.Enabled = true; else radDropDownList_Grp.Enabled = false;
        }
    }
}

