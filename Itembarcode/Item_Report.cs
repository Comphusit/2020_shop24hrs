﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;
using Telerik.WinControls.Data;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using PrintBarcode;
using PC_Shop24Hrs.GeneralForm.Report;
using PC_Shop24Hrs.GeneralForm.BillOut;

namespace PC_Shop24Hrs.Itembarcode
{
    public partial class Item_Report : Telerik.WinControls.UI.RadForm
    {
        //DataTable dtBch;
        DataTable dt_Data = new DataTable();
        DataTable dt_Grp = new DataTable();
        readonly string _pTypeReport, _pCondition;
        readonly GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem();
        // 0 - สินค้าห้ามขาย
        // 1 - สินค้าห้ามสั่ง
        // 2 - รับซื้อสินค้าลูกค้า MNBC
        // 3 - สินค้าประจำชั้นวาง
        // 4 - รายงานสินค้าแลกแต้ม 
        // 5 - รายงานรายการหักมินิมาร์ท 
        // 6 - รายงานบาร์โค้ดลดราคา
        // 7 - การขายสินค้าทั้งหมด
        // 8 - รายงานสินค้าลดราคาตามยอด/จำนวน
        // 9 - รายงานสินค้าลดราคาตามบาร์โค๊ด
        // 10 - รายงานการขายสินค้าตามกลุ่มสินค้าที่ตั้งไว้

        readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();
        string barcodePrint, namePrint;
        //Load
        public Item_Report(string pTypeReport, string pCondition = "")//pCondition ส่งค่าแผนก
        {
            InitializeComponent();
            _pTypeReport = pTypeReport;
            _pCondition = pCondition;
        }
        //Set Form หลัก
        void SetForm()
        {
            radStatusStrip1.SizingGrip = false;
            radButtonElement_add.ToolTipText = "ล้างข้อมูล"; radButtonElement_add.ShowBorder = true;
            radButtonElement_excel.ToolTipText = "Export To Excel"; radButtonElement_excel.ShowBorder = true;
            radButtonElement_pdf.ToolTipText = "คู่มือการใช้งาน"; radButtonElement_pdf.ShowBorder = true;
            RadButtonElement_barcode.ToolTipText = "พิมพ์บาร์โค้ด"; RadButtonElement_barcode.ShowBorder = true; RadButtonElement_barcode.Enabled = false;

            RadButton_Search.ButtonElement.ShowBorder = true;

            RadButton_Copy.Visible = false; RadButton_Copy.ButtonElement.ShowBorder = true;

            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now, DateTime.Now);
            DatagridClass.SetDefaultFontDropDown(RadDropDownList_1);
            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);

            RadCheckBox_1.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_ChangeShelf.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_NoShelf.ButtonElement.Font = SystemClass.SetFontGernaral;
            radCheckBox_Case3.ButtonElement.Font = SystemClass.SetFontGernaral;

            radLabel_Txt.Text = ""; radLabel_Txt.Visible = false;
            radTextBox_Barcode.Text = ""; radTextBox_Barcode.Visible = false;
            radCheckBox_ChangeShelf.Visible = false;
            radCheckBox_NoShelf.Visible = false;
            radCheckBox_Case3.Visible = false;
            RadDropDownList_1.Visible = false;
            RadCheckBox_1.Visible = false;

            radRadioButton_date1.Visible = false; radRadioButton_date2.Visible = false;
        }
        //Load
        private void Item_Report_Load(object sender, EventArgs e)
        {
            SetForm();
            ClearTxt();

            switch (_pTypeReport)
            {
                case "0": // 0 - สินค้าห้ามขาย
                    radDateTimePicker_D2.Visible = false;
                    radDateTimePicker_D1.Visible = false;
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Visible = false;
                    RadCheckBox_1.Visible = true;
                    RadDropDownList_1.Visible = true;
                    RadCheckBox_1.Enabled = false;
                    RadCheckBox_1.CheckState = CheckState.Checked;

                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "รหัสจัดซื้อ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อจัดซื้อ", 180)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMID", "รหัสสินค้า", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMNAME", "ชื่อสินค้า", 450)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("NAME", "ผู้ที่แก้ไขล่าสุด", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MODIFIEDDATETIME", "วันที่แก้ไขล่าสุด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GROUPDESC", "กลุ่มสินค้า", 500)));

                    break;
                case "1":// 1 - สินค้าห้ามสั่ง
                    radDateTimePicker_D2.Visible = false;
                    radDateTimePicker_D1.Visible = false;
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Visible = false;
                    RadCheckBox_1.Visible = true;
                    RadDropDownList_1.Visible = true;

                    RadCheckBox_1.CheckState = CheckState.Checked;
                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "DoubleClick ที่รายการ >> นำสินค้าเข้าแผนใบสั่งตามปกติ [ปลดล็อกห้ามสั่ง]";

                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONIDTO", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMID", "รหัสสินค้า", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTDIMID", "มิติสินค้า", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 450)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("FROMDATE", "วันที่เริ่มต้น", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TODATE", "วันที่สิ้นสุด", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("REMARKS", "หมายเหตุ", 250)));

                    break;

                case "2":// 2 - รับซื้อสินค้าลูกค้า MNBC
                    radDateTimePicker_D2.Visible = true;
                    radDateTimePicker_D1.Visible = true;
                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Visible = true; radLabel_Date.Text = "วันที่ซื้อสินค้า";
                    RadCheckBox_1.Visible = true; RadDropDownList_1.Visible = true;

                    radLabel_Detail.Visible = true; radLabel_Detail.Text = "สีเขียว >> สาขายังไม่ได้ส่งสินค้า | สีชมพู >> จัดซื้อยังไม่ได้รับสินค้า";
                    if (SystemClass.SystemBranchID == "MN000") RadCheckBox_1.Enabled = true; else RadCheckBox_1.Enabled = false; RadCheckBox_1.CheckState = CheckState.Checked;

                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNBCBranch", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNBCDocNo", "เลขที่บิล", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNBCDate", "วันที่บิล", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHONAME", "ชื่อผู้บันทึก", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNBCBarcode", "บาร์โค้ด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNBCName", "ชื่อสินค้า", 250)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("MNBCQty", "จำนวน", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("MNBCPrice", "ราคา/หน่วย", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("MNBCNet", "ราคารวม", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNBCUnitID", "หน่วย", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("MNBCStaPurchase", "จัดซื้อรับ")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNBCMoneyRound", "รอบส่งเงิน", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("MNBCCstName", "ชื่อลูกค้า", 200)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("ShipSta", "สถานะส่งของ")));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ShipDriverName", "ชื่อ พขร.", 250)));

                    RadGridView_ShowHD.MasterTemplate.Columns["MNBCBranch"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["MNBCDocNo"].IsPinned = true;

                    //ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition2", "ShipSta = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_GreenPastel() };
                    //RadGridView_ShowHD.Columns["MNBCDocNo"].ConditionalFormattingObjectList.Add(obj2);
                    DatagridClass.SetCellBackClolorByExpression("MNBCDocNo", "ShipSta = '0' ", ConfigClass.SetColor_GreenPastel(), RadGridView_ShowHD);
                    DatagridClass.SetCellBackClolorByExpression("MNBCDocNo", "MNBCStaPurchase = '0' ", ConfigClass.SetColor_PinkPastel(), RadGridView_ShowHD);
                    //ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "MNBCStaPurchase = '0' ", false)
                    //{ CellBackColor = ConfigClass.SetColor_PinkPastel() };
                    //RadGridView_ShowHD.Columns["MNBCDocNo"].ConditionalFormattingObjectList.Add(obj1);

                    RadGridView_ShowHD.TableElement.RowHeight = 50;

                    GridViewSummaryItem summaryItem6 = new GridViewSummaryItem
                    {
                        Name = "MNBCNet",
                        Aggregate = GridAggregateFunction.Sum,
                        FormatString = "{0:n2}"
                    };
                    GridViewSummaryRowItem summaryRowItem6 = new GridViewSummaryRowItem { summaryItem6 };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem6);

                    GroupDescriptor descriptor = new GroupDescriptor();
                    descriptor.GroupNames.Add("MNBCDocNo", System.ComponentModel.ListSortDirection.Descending);
                    RadGridView_ShowHD.GroupDescriptors.Add(descriptor);


                    break;
                case "3": // 3 - สินค้าประจำชั้นวาง
                    radDateTimePicker_D2.Visible = false;
                    radDateTimePicker_D1.Visible = false;

                    radLabel_Detail.Text = ""; radLabel_Detail.Visible = false;
                    radLabel_Date.Visible = false;
                    RadCheckBox_1.Visible = true; RadCheckBox_1.Enabled = false;
                    RadDropDownList_1.Visible = true;

                    RadCheckBox_1.CheckState = CheckState.Checked;
                    radLabel_Detail.Visible = true;

                    radCheckBox_ChangeShelf.Visible = true;
                    radCheckBox_NoShelf.Visible = true;

                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        RadCheckBox_1.Enabled = true;
                        //dtBch = BranchClass.GetBranchAll("'1','4'", " '1'");
                        radLabel_Detail.Text = "DoubleClick รายการสินค้า >> หน้าจอรายละเอียดสินค้า | DoubleClick โซน-ชั้นวาง >> อนุมัติเปลี่ยนชั้นวาง | สีชมพู >> รอเปลี่ยนชั้นวาง";
                        RadButton_Copy.Visible = false;
                    }
                    else
                    {
                        RadCheckBox_1.Enabled = false;
                        //dtBch = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID);
                        radLabel_Detail.Text = "DoubleClick รายการสินค้า >> หน้าจอรายละเอียดสินค้า | DoubleClick โซน-ชั้นวาง >> ตั้งค่าโซน-ชั้นวาง | สีชมพู >> รอเปลี่ยนชั้นวาง";
                        RadButton_Copy.Visible = true;
                    }

                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 450)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 120)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DIMENSION", "รหัสจัดซื้อ", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อจัดซื้อ", 150)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("GRPID0", "รหัสกลุ่ม", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("TAX0", "ชื่อกลุ่มหลัก", 140)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_ID", "โซน", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ZONE_NAME", "ชื่อโซน", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_ID", "ชั้นวาง", 140)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SHELF_NAME", "ชื่อชั้นวาง", 140)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("CC", "เเปลี่ยนชั้นวาง", 140)));

                    RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_ID"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["BRANCH_NAME"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["ITEMBARCODE"].IsPinned = true;
                    RadGridView_ShowHD.MasterTemplate.Columns["SPC_ITEMNAME"].IsPinned = true;

                    ExpressionFormattingObject o3_1 = new ExpressionFormattingObject("MyCondition1", "CC <> '' ", false)
                    { CellBackColor = ConfigClass.SetColor_PinkPastel() };
                    RadGridView_ShowHD.Columns["ITEMBARCODE"].ConditionalFormattingObjectList.Add(o3_1);
                    RadGridView_ShowHD.Columns["SPC_ITEMNAME"].ConditionalFormattingObjectList.Add(o3_1);


                    break;
                case "4"://รายงานสินค้าแลกแต้ม
                    radLabel_Date.Visible = false;
                    radDateTimePicker_D1.Visible = false;
                    radDateTimePicker_D2.Visible = false;
                    radLabel_Detail.Text = "สินค้าที่สามารถให้ลูกค้าแลกแต้มได้ทั้งหมด";
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค๊ด", 140));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMUNIT", "หน่วย", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Description", "แผนก", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMGROUPNAME", "กลุ่มสินค้า", 150));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddCheckBoxColumn_TYPEID("ITEMSTATUS1", "สาขาสั่ง"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("SPC_REDEMPPOINT", "จำนวนแต้ม", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMREMARK", "หมายเหตุ", 200));
                    SetDGV_HD();
                    break;
                case "5":// 5 - รายงานรายการหักมินิมาร์ท

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่", 140));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("TYPE", "ประเภท", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOID", "รหัส", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHONAME", "ชื่อพนักงาน", 200));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DPTID", "รหัส", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("GRAND", "ยอดหัก", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DPTNAME", "แผนก", 300));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("REMARK", "หมายเหตุรายการหัก", 300));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHOIDINS", "บันทึกโดย", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("WHONAMEINS", "ชื่อผู้บันทึก", 180));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันที่บันทึก", 130));

                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-31);

                    if (_pCondition == "D062") radLabel_Detail.Text = "Double Click>>ดูรายการบิล"; else radLabel_Detail.Text = "";

                    break;
                case "6"://6 - รายงานบาร์โค้ดลดราคา 
                    barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
                    barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
                    barcode.BarWidth = 1;
                    barcode.BarHeight = 38;
                    barcode.LeftMargin = 0;
                    barcode.RightMargin = 0;
                    barcode.TopMargin = 0;
                    barcode.BottomMargin = 0;

                    Case6_BarcodeDiscount("0");
                    Case6_BarcodeDiscount("1");
                    break;
                case "7":
                    Case7_SaleBarcode164("0");
                    break;
                case "8":
                    Case8_SaleBarcodeDiscount("0");
                    break;
                case "9":
                    Case9_SaleBarcodeDiscount("0");
                    break;
                case "10":
                    Case10_SaleBarcodeByGroupFix("0");
                    break;
                default:
                    break;
            }

        }
        //Set HD
        void SetDGV_HD()
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); }

            switch (_pTypeReport)
            {
                case "0": // 0 - สินค้าห้ามขาย
                    dt_Data = ItembarcodeClass.GetTransferBlockedItemTable_All(RadDropDownList_1.SelectedValue.ToString());
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;

                case "1":// 1 - สินค้าห้ามสั่ง
                    string pCon = "";
                    if (RadCheckBox_1.Checked == true) pCon = RadDropDownList_1.SelectedValue.ToString();
                    dt_Data = ItembarcodeClass.GetRETAILPLANORDERITEMBLOCKED_All(pCon);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;

                    break;

                case "2":
                    string bchID = "";
                    if (RadCheckBox_1.Checked == true) bchID = RadDropDownList_1.SelectedValue.ToString();
                    dt_Data = ItembarcodeClass.GetMNBC(bchID, radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"));// ConnectionClass.SelectSQL_Main(sql);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;

                    break;

                case "3":
                    string noShelf = "", changeShelf = "", bchShelf = "";
                    if (radCheckBox_NoShelf.CheckState == CheckState.Checked) noShelf = "1";

                    if (radCheckBox_ChangeShelf.CheckState == CheckState.Checked) changeShelf = "1";

                    if (RadCheckBox_1.CheckState == CheckState.Checked) bchShelf = RadDropDownList_1.SelectedValue.ToString();

                    dt_Data = ItembarcodeClass.GetSHELFITEMS_All(noShelf, changeShelf, bchShelf);// ConnectionClass.SelectSQL_Main(sql3);
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                case "4": //4 รายงานสินค้าแลกแต้ม
                    this.Cursor = Cursors.WaitCursor;
                    RadGridView_ShowHD.DataSource = ItembarcodeClass.ItemGroup_GetItemBarcode("D038", "02", "D038_01", "");
                    this.Cursor = Cursors.Default;
                    break;

                case "5": // 5 - รายงานรายการหักมินิมาร์ท
                    string pBch = "";
                    if (SystemClass.SystemBranchID != "MN000")
                    {
                        pBch = " AND DPTID = '" + SystemClass.SystemBranchID + @"' ";
                    }
                    DataTable dt = BillOutClass.Find_EMP_ListInCorrect(_pCondition,
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "", pBch);

                    if (dt.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("ข้อมูลบิล");
                        RadGridView_ShowHD.DataSource = dt;
                        dt.AcceptChanges();
                        this.Cursor = Cursors.Default;
                        return;
                    }

                    RadGridView_ShowHD.DataSource = dt;
                    this.Cursor = Cursors.Default;
                    break;
                case "6"://รายงานบาร์โค้ดลดราคา
                    Case6_BarcodeDiscount("1");
                    break;
                case "7":
                    Case7_SaleBarcode164("1");
                    break;
                case "8":
                    Case8_SaleBarcodeDiscount("1");
                    break;
                case "9":
                    Case9_SaleBarcodeDiscount("1");
                    break;
                case "10":
                    Case10_SaleBarcodeByGroupFix("1");
                    break;
                default:
                    break;
            }
        }

        //6 - รายงานบาร์โค้ดลดราคา
        void Case6_BarcodeDiscount(string setCase, int iRowIndex = 0, int iCount = 0)
        {
            if (setCase == "0")
            {
                radCheckBox_NoShelf.Text = "เฉพาะที่ขายไปแล้วเท่านั้น"; radCheckBox_NoShelf.Visible = true;
                radCheckBox_ChangeShelf.Text = "เฉพาะที่ว่างเท่านั้น"; radCheckBox_ChangeShelf.Visible = true;
                radCheckBox_Case3.Text = "เฉพาะที่ยังไม่ขาย"; radCheckBox_Case3.Visible = true;
                RadButtonElement_barcode.Enabled = true;
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-7); radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                RadCheckBox_1.Visible = true; RadDropDownList_1.Visible = true;
                radLabel_Txt.Text = "ระบุวันที่พิมพ์บาร์โค๊ด";
                radLabel_Date.Visible = false;// radLabel_Date.Text = "วันที่สร้างบาร์โค้ดลดราคา";
                radRadioButton_date1.Visible = true; radRadioButton_date2.Visible = true;
                if (SystemClass.SystemBranchID == "MN000")
                {
                    RadCheckBox_1.Enabled = true;
                    radLabel_Detail.Text = "DoubleClick บาร์โค้ดลด >> พิมพ์ซ้ำ [เฉพาะที่ยังไม่ขาย] | สีเขียว >> ยังไม่ใช้งาน | สีแดง >> ยังขายไม่ได้ | สีเหลือง >> ยังไม่ตรวจบิล | DoubleClick แถว >> เพื่อตรวจบิล";
                    radCheckBox_ChangeShelf.Checked = true;
                }
                else
                {
                    radLabel_Detail.Text = "DoubleClick บาร์โค้ดลด >> พิมพ์ซ้ำ [เฉพาะที่ยังไม่ขาย] | สีเขียว >> ยังไม่ใช้งาน | สีแดง >> ยังขายไม่ได้ | สีเหลือง >> ยังไม่ตรวจบิล | DoubleClick แถว >> เพื่อตรวจบิล [เฉพาะ ผจก]";
                    RadCheckBox_1.Enabled = false; RadCheckBox_1.Checked = true;
                }

                DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHID", "สาขา", 80)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHNAME", "ชื่อสาขา", 130)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetCenter("COUNT_PRINT", "จน.ครั้งที่พิมพ์", 80));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE_DIS", "บาร์โค๊ดลด", 130));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME_DIS", "ชื่อสินค้า", 350));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PRICE_DIS", "ราคาลด", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PRICE", "ราคาก่อนลด", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PERCENT_DIS", "เปอร์เซ็นลด", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));


                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค๊ด", 140));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID_DIS", "หน่วยลด", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEINS", "วันที่สร้างลด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHONAMEINS", "ผู้สร้างลด", 200)));

                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DPTNAME", "แผนกสินค้า", 150));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BOMID", "เลขที่เอกสาร", 150));

                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("XXX_INVOICEID", "บิลขาย", 150));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("XXX_INVOICEDATE", "เวลาขาย", 150));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("XXX_CASHIERID", "รหัสแคชเชียร์", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("XXX_CASHIERNAME", "ชื่อแคชเชียร์", 200));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("XXX_INVOICEACCOUNT", "รหัสลูกค้า", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("XXX_INVOICEACCOUNTNAME", "ชื่อลูกค้า", 200));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PATH_WEBCAM", "PATH_WEBCAM"));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CHECK_STA", "CHECK_STA"));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CHECK_REMARK", "หมายเหตุ", 350));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CHECK_WHOID", "รหัสผู้ตรวจ", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CHECK_WHONAME", "ชื่อผู้ตรวจ", 200));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CHECK_DATE", "วันที่ตรวจ", 170));

                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DATEINS_DIS", "วันที่สร้างโค้ด", 150)));
                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("WHONAMEINS_DIS", "ผู้สร้างโค้ด", 200)));

                //ExpressionFormattingObject o1_0 = new ExpressionFormattingObject("MyCondition1", "COUNT_PRINT > 1 ", false)
                //{ CellBackColor = ConfigClass.SetColor_Red() };
                //RadGridView_ShowHD.Columns["COUNT_PRINT"].ConditionalFormattingObjectList.Add(o1_0);
                DatagridClass.SetCellBackClolorByExpression("COUNT_PRINT", "COUNT_PRINT > 1 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                ExpressionFormattingObject o1_1 = new ExpressionFormattingObject("MyCondition1", "CHECK_STA = '0' ", false)
                { CellBackColor = ConfigClass.SetColor_YellowPastel() };
                RadGridView_ShowHD.Columns["ITEMBARCODE_DIS"].ConditionalFormattingObjectList.Add(o1_1);
                RadGridView_ShowHD.Columns["XXX_INVOICEID"].ConditionalFormattingObjectList.Add(o1_1);

                ExpressionFormattingObject o2_1 = new ExpressionFormattingObject("MyCondition1", "ITEMBARCODE <> '' AND XXX_INVOICEID = '' ", false)
                { CellBackColor = ConfigClass.SetColor_Red() };
                RadGridView_ShowHD.Columns["ITEMBARCODE_DIS"].ConditionalFormattingObjectList.Add(o2_1);
                RadGridView_ShowHD.Columns["XXX_INVOICEID"].ConditionalFormattingObjectList.Add(o2_1);

                ExpressionFormattingObject o3_1 = new ExpressionFormattingObject("MyCondition1", "ITEMBARCODE = '' ", false)
                { CellBackColor = ConfigClass.SetColor_GreenPastel() };
                RadGridView_ShowHD.Columns["ITEMBARCODE_DIS"].ConditionalFormattingObjectList.Add(o3_1);
                RadGridView_ShowHD.Columns["XXX_INVOICEID"].ConditionalFormattingObjectList.Add(o3_1);
                RadGridView_ShowHD.Columns["XXX_INVOICEDATE"].ConditionalFormattingObjectList.Add(o3_1);

                //ExpressionFormattingObject o4_1 = new ExpressionFormattingObject("MyCondition1", "PERCENT_DIS >= 50 ", false)
                //{ CellBackColor = ConfigClass.SetColor_Red() };
                //RadGridView_ShowHD.Columns["PERCENT_DIS"].ConditionalFormattingObjectList.Add(o4_1);
                DatagridClass.SetCellBackClolorByExpression("PERCENT_DIS", "PERCENT_DIS >= 50 ", ConfigClass.SetColor_Red(), RadGridView_ShowHD);

                GridViewSummaryItem summaryItemA = new GridViewSummaryItem("PRICE_DIS", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemB = new GridViewSummaryItem("PRICE", "{0:n2}", GridAggregateFunction.Sum);

                GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { summaryItemA, summaryItemB };
                this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA);
                this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            }
            else if (setCase == "1")//ดึงข้อมูล
            {
                this.Cursor = Cursors.WaitCursor;
                string conditionBch = "", conditionSale = "", conditionNull = "", conditionNotSale = "", setFiledDate = "0";
                if (RadCheckBox_1.Checked == true) conditionBch = $@" AND BRANCHID = '{RadDropDownList_1.SelectedValue}' ";
                if (radCheckBox_NoShelf.Checked == true) conditionSale = " AND XXX_INVOICEID != '' ";
                if (radCheckBox_ChangeShelf.Checked == true) conditionNull = " AND ITEMBARCODE = '' ";
                if (radCheckBox_Case3.Checked == true) conditionNotSale = " AND ITEMBARCODE != ''  AND XXX_INVOICEID = '' ";
                if (radRadioButton_date2.CheckState == CheckState.Checked) setFiledDate = "1";
                dt_Data = ItembarcodeClass.GetDataMNBM("0", setFiledDate, conditionBch,
                    radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), $@"{conditionSale} {conditionNull} {conditionNotSale}");
                RadGridView_ShowHD.FilterDescriptors.Clear();
                RadGridView_ShowHD.DataSource = dt_Data;
                dt_Data.AcceptChanges();
                this.Cursor = Cursors.Default;
            }
            else if (setCase == "2")//บันทึกเพิ่มบาร์โค้ด
            {
                JOB.CheckLockOilCap checkLockOilCap = new JOB.CheckLockOilCap("1");
                if (checkLockOilCap.ShowDialog() == DialogResult.OK) Case6_BarcodeDiscount("1");
            }
            else if (setCase == "3")// จัดการ ณ ตอน DoubleClick สาขาพิมพ์ซ้ำ
            {
                DialogResult result = printDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    printDocument1.PrintController = printController;
                    printDocument1.PrinterSettings = printDialog1.PrinterSettings;
                    printDocument1.Print();

                    string sqlUpPrint = $@"
                        UPDATE  SHOP_BARCODEDISCOUNT 
                        SET     COUNT_PRINT  = COUNT_PRINT + 1
                        WHERE   ITEMBARCODE_DIS = '{barcodePrint}'
                    ";
                    if (ConnectionClass.ExecuteSQL_Main(sqlUpPrint) == "") RadGridView_ShowHD.Rows[iRowIndex].Cells["COUNT_PRINT"].Value = iCount + 1;

                }
            }
            else if (setCase == "4")// จัดการ ณ ตอน DoubleClick supc พิมพ์ซ่ำ
            {
                DialogResult result = printDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    RawPrinterHelper.SendStringToPrinter(printDialog1.PrinterSettings.PrinterName,
                        PrintClass.SetPrintBarcodeDiscountGodex(RadGridView_ShowHD.Rows[iRowIndex].Cells["ITEMBARCODE_DIS"].Value.ToString()).ToString());
                }
            }
            else if (setCase == "5")//ตรวจบิล
            {
                //ดูกล้อง
                string docno = RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEID"].Value.ToString();
                string path = RadGridView_ShowHD.CurrentRow.Cells["PATH_WEBCAM"].Value.ToString();

                if (docno == "") return;
                if (docno.Contains("MN")) return;
                if (docno.Contains("CANCEL")) return;

                string chkPath = PathImageClass.CheckDirectory(path);
                if (chkPath != "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error(chkPath); return;
                }

                FileInfo[] FilesInvoice = PathImageClass.CheckFileWant(path, docno);
                if (FilesInvoice.Length == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูล VDO บิลขาย ที่ระบุ"); return;
                }

                string cash = RadGridView_ShowHD.CurrentRow.Cells["XXX_CASHIERID"].Value.ToString() + " - " + RadGridView_ShowHD.CurrentRow.Cells["XXX_CASHIERNAME"].Value.ToString();
                string staCheck = RadGridView_ShowHD.CurrentRow.Cells["CHECK_STA"].Value.ToString();
                string remark = RadGridView_ShowHD.CurrentRow.Cells["CHECK_REMARK"].Value.ToString();
                string dateTime = RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEDATE"].Value.ToString();
                string cstName = RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEACCOUNT"].Value.ToString() + " " + RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEACCOUNTNAME"].Value.ToString();
                Report_CheckVDOWebCam d054_ReportCheckVDOWebCam = new Report_CheckVDOWebCam("SHOP_BARCODEDISCOUNT",
                    FilesInvoice[FilesInvoice.Length - 1].FullName, docno, cash, staCheck, remark, dateTime, cstName);
                if (d054_ReportCheckVDOWebCam.ShowDialog() == DialogResult.OK)
                {
                    RadGridView_ShowHD.CurrentRow.Cells["CHECK_STA"].Value = "1";
                    RadGridView_ShowHD.CurrentRow.Cells["CHECK_REMARK"].Value = d054_ReportCheckVDOWebCam.sRemark;
                    RadGridView_ShowHD.CurrentRow.Cells["CHECK_WHOID"].Value = SystemClass.SystemUserID;
                    RadGridView_ShowHD.CurrentRow.Cells["CHECK_WHONAME"].Value = SystemClass.SystemUserName;
                    RadGridView_ShowHD.CurrentRow.Cells["CHECK_DATE"].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
        }
        // 7 - การขายสินค้าลดราคาชองแป๊ะ 164
        void Case7_SaleBarcode164(string setCase)
        {
            if (setCase == "0")
            {
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(-2); radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1);
                RadCheckBox_1.Visible = true; RadDropDownList_1.Visible = true;

                radLabel_Txt.Text = "ระบุบาร์โค้ด"; radLabel_Txt.Visible = true;
                radTextBox_Barcode.Visible = true; radTextBox_Barcode.Text = ""; radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                radCheckBox_NoShelf.Visible = true; radCheckBox_NoShelf.Text = "เฉพาะที่แก้ไขราคาได้";
                radCheckBox_ChangeShelf.Visible = true; radCheckBox_ChangeShelf.Text = "เฉพาะที่ต้องยืนยันบาร์โค้ดหลัก";
                if (SystemClass.SystemBranchID == "MN000")
                {
                    RadCheckBox_1.Enabled = true;
                    radLabel_Detail.Text = "DoubleClick แถว >> เพื่อดู VDO บิลขาย";
                }
                else
                {
                    radLabel_Detail.Text = "DoubleClick แถว >> เพื่อดู VDO บิลขาย [เฉพาะผู้จัดการ]"; ;
                    RadCheckBox_1.Enabled = false; RadCheckBox_1.Checked = true;
                }

                DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");

                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVOICEID", "บิลขาย", 150));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSGROUP", "สาขา", 80));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("POSNAME", "ชื่อสาขา", 130));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CASHIERID", "แคชเชียร์", 80));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_NAME", "ชื่อแคชเชียร์", 200));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CREATEDDATETIME", "วันที่ขาย", 170));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BARCODE", "บาร์โค๊ดขาย", 130));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("BARCODENAME", "ชื่อสินค้าขาย", 350));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "จำนวน", 80));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SALESUNIT", "หน่วย", 80));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "ราคาขาย", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("INVOICEACCOUNT", "รหัสลูกค้า", 100));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อลูกค้า", 200));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMBUYERGROUPID", "จัดซื้อ", 80));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DESCRIPTION", "ชื่อจัดซื้อ", 180));
                RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PATH_WEBCAM", "PATH_WEBCAM"));

                GridViewSummaryItem summaryItemA = new GridViewSummaryItem("QTY", "{0:n2}", GridAggregateFunction.Sum);
                GridViewSummaryItem summaryItemB = new GridViewSummaryItem("LINEAMOUNT", "{0:n2}", GridAggregateFunction.Sum);

                GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { summaryItemA, summaryItemB };
                this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA);
                this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

            }
            else if (setCase == "1")
            {

                if (radTextBox_Barcode.Text.Trim() == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("บังคับระบุบาร์โค้ดสินค้าก่อนการค้นหาเท่านั้น."); return;
                }

                this.Cursor = Cursors.WaitCursor;
                string bch = "";
                if (RadCheckBox_1.Checked == true) bch = RadDropDownList_1.SelectedValue.ToString();
                dt_Data = PosSaleClass.GetDetailSale_ByBarcode("2", radTextBox_Barcode.Text.Trim(),
                    radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), bch, "");//conditionSalePriceType
                RadGridView_ShowHD.FilterDescriptors.Clear();
                RadGridView_ShowHD.DataSource = dt_Data;
                dt_Data.AcceptChanges();
                this.Cursor = Cursors.Default;
            }
            else if (setCase == "2")
            {
                //ดูกล้อง
                string docno = RadGridView_ShowHD.CurrentRow.Cells["INVOICEID"].Value.ToString();
                string path = RadGridView_ShowHD.CurrentRow.Cells["PATH_WEBCAM"].Value.ToString();
                string dateTime = RadGridView_ShowHD.CurrentRow.Cells["CREATEDDATETIME"].Value.ToString();

                if (docno == "") return;
                if (docno.Contains("MN")) return;

                string chkPath = PathImageClass.CheckDirectory(path);
                if (chkPath != "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error(chkPath); return;
                }

                FileInfo[] FilesInvoice = PathImageClass.CheckFileWant(path, docno);
                if (FilesInvoice.Length == 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("ไม่พบข้อมูล VDO บิลขาย ที่ระบุ"); return;
                }

                string cash = RadGridView_ShowHD.CurrentRow.Cells["CASHIERID"].Value.ToString() + " - " + RadGridView_ShowHD.CurrentRow.Cells["SPC_NAME"].Value.ToString();
                string staCheck = "";
                string remark = "";
                string cstName = RadGridView_ShowHD.CurrentRow.Cells["INVOICEACCOUNT"].Value.ToString() + " " + RadGridView_ShowHD.CurrentRow.Cells["NAME"].Value.ToString();
                Report_CheckVDOWebCam d054_ReportCheckVDOWebCam = new Report_CheckVDOWebCam("",
                    FilesInvoice[FilesInvoice.Length - 1].FullName, docno, cash, staCheck, remark, dateTime, cstName);
                if (d054_ReportCheckVDOWebCam.ShowDialog() == DialogResult.OK)
                {
                }

            }
        }
        // 8 - รายงานสินค้าลดราคาตามยอด
        void Case8_SaleBarcodeDiscount(string setCase)
        {
            switch (setCase)
            {
                case "0":
                    radCheckBox_NoShelf.Text = "เฉพาะยอดขายเท่านั้น"; radCheckBox_NoShelf.Visible = true; radCheckBox_NoShelf.Checked = false;
                    radCheckBox_ChangeShelf.Text = "เฉพาะจำนวนขายเท่านั้น"; radCheckBox_ChangeShelf.Visible = true; radCheckBox_ChangeShelf.Checked = false;
                    radCheckBox_Case3.Text = "แสดงทั้งยอดและจำนวน"; radCheckBox_Case3.Visible = true; radCheckBox_Case3.Checked = true;
                    radLabel_Txt.Text = "ระบุวันที่ใช้งานบาร์โค๊ด";
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-7); radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                    RadCheckBox_1.Visible = true; RadDropDownList_1.Visible = true;
                    radLabel_Detail.Text = "วันที่ที่ระบุ >> วันที่ระบุการใช้งานบาร์โค้ดลดราคา | สีแดงข่อง ยอดรวมค้าง >> ยอดรวมค้าง มากกว่ายอดรวมลดเกินครึ่ง | สีแดงช่อง จน.รายการค้าง >> จน.รายการค้าง มากกว่าจน.รายการลดเกินครึ่ง  | สีแดงข่อง เปอร์เซ็นลด >> ลดราคาสินค้ามากกว่า 50%";
                    if (SystemClass.SystemBranchID == "MN000") RadCheckBox_1.Enabled = true;
                    else
                    {
                        RadCheckBox_1.Enabled = false; RadCheckBox_1.Checked = true;
                    }

                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHNAME", "ชื่อสาขา", 130)));

                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_PRICE", "ยอดรวมก่อนลด", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_DIS", "ยอดรวมลด", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PERCENT_DIS", "เปอร์เซ็นลด", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_XXX", "ยอดรวมขาย", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_MNBM", "ยอดรวมลดซ้ำ", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_MNPC", "ยอดรวมคืน CN", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SUM_NULL", "ยอดรวมค้าง", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SUM_CHECK", "SUM_CHECK"));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COUNT_DIS", "จน.รายการลด", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COUNT_XXX", "จน.รายการขาย", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COUNT_MNBM", "จน.รายการลดซ้ำ", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COUNT_MNPC", "จน.รายการคืนCN", 105));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COUNT_NULL", "จน.รายการค้าง", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("COUNT_CHECK", "COUNT_CHECK"));

                    ExpressionFormattingObject o1_1 = new ExpressionFormattingObject("MyCondition1", "SUM_CHECK = 1 ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["SUM_NULL"].ConditionalFormattingObjectList.Add(o1_1);

                    ExpressionFormattingObject o1_0 = new ExpressionFormattingObject("MyCondition1", "COUNT_CHECK = 1 ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["COUNT_NULL"].ConditionalFormattingObjectList.Add(o1_0);

                    ExpressionFormattingObject o1_2 = new ExpressionFormattingObject("MyCondition1", "PERCENT_DIS > 49 ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["PERCENT_DIS"].ConditionalFormattingObjectList.Add(o1_2);


                    GridViewSummaryItem summaryItemA = new GridViewSummaryItem("SUM_PRICE", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemB = new GridViewSummaryItem("SUM_DIS", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemC = new GridViewSummaryItem("SUM_XXX", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemD = new GridViewSummaryItem("SUM_MNBM", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemE = new GridViewSummaryItem("SUM_MNPC", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemF = new GridViewSummaryItem("SUM_NULL", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemG = new GridViewSummaryItem("COUNT_DIS", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemH = new GridViewSummaryItem("COUNT_XXX", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemI = new GridViewSummaryItem("COUNT_MNBM", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemJ = new GridViewSummaryItem("COUNT_MNPC", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemK = new GridViewSummaryItem("COUNT_NULL", "{0:n2}", GridAggregateFunction.Sum);

                    GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { summaryItemA, summaryItemB, summaryItemC, summaryItemD, summaryItemE, summaryItemF,
                            summaryItemG,summaryItemH,summaryItemI,summaryItemJ,summaryItemK};
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    break;
                case "1":
                    this.Cursor = Cursors.WaitCursor;

                    string pTypeShow = "0";
                    if (radCheckBox_NoShelf.Checked == true) pTypeShow = "0";
                    if (radCheckBox_ChangeShelf.Checked == true) pTypeShow = "1";
                    if (radCheckBox_Case3.Checked == true) pTypeShow = "2";

                    DataTable dtdt = new DataTable();
                    dtdt.Columns.Add("BRANCHID", typeof(string));
                    dtdt.Columns.Add("BRANCHNAME", typeof(string));
                    dtdt.Columns.Add("SUM_PRICE", typeof(double));
                    dtdt.Columns.Add("SUM_DIS", typeof(double));
                    dtdt.Columns.Add("PERCENT_DIS", typeof(double));
                    dtdt.Columns.Add("SUM_XXX", typeof(double));
                    dtdt.Columns.Add("SUM_MNBM", typeof(double));
                    dtdt.Columns.Add("SUM_MNPC", typeof(double));
                    dtdt.Columns.Add("SUM_NULL", typeof(double));
                    dtdt.Columns.Add("SUM_CHECK");

                    dtdt.Columns.Add("COUNT_DIS", typeof(double));
                    dtdt.Columns.Add("COUNT_XXX", typeof(double));
                    dtdt.Columns.Add("COUNT_MNBM", typeof(double));
                    dtdt.Columns.Add("COUNT_MNPC", typeof(double));
                    dtdt.Columns.Add("COUNT_NULL", typeof(double));
                    dtdt.Columns.Add("COUNT_CHECK", typeof(string));

                    string conditionBch = "";
                    if (RadCheckBox_1.Checked == true) conditionBch = $@" AND BRANCHID = '{RadDropDownList_1.SelectedValue}' ";
                    DataTable dtSum = ItembarcodeClass.GetDataMNBM("1", "0", conditionBch,
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "");
                    DataTable dtXXX = ItembarcodeClass.GetDataMNBM("1", "0", conditionBch,
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), $@" AND XXX_INVOICEID LIKE '%-%' ");
                    DataTable dtMNPC = ItembarcodeClass.GetDataMNBM("1", "0", conditionBch,
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), $@" AND XXX_INVOICEID LIKE 'MNPC%' ");
                    DataTable dtMNBM = ItembarcodeClass.GetDataMNBM("1", "0", conditionBch,
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), $@" AND XXX_INVOICEID LIKE 'MNBM%' ");
                    DataTable dtNull = ItembarcodeClass.GetDataMNBM("1", "0", conditionBch,
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), $@" AND XXX_INVOICEID = '' ");


                    for (int i = 0; i < dtSum.Rows.Count; i++)
                    {
                        double sum_dis, count_dis, s_xxx = 0, s_mnpc = 0, s_mnbm = 0, s_null = 0, c_xxx = 0, c_mnpc = 0, c_mnbm = 0, c_null = 0, percentDis;
                        string checkStaSum = "0", checkStaCount = "0";

                        sum_dis = double.Parse(dtSum.Rows[i]["SUM_DIS"].ToString());
                        count_dis = double.Parse(dtSum.Rows[i]["COUNT_DIS"].ToString());
                        percentDis = 100 - (sum_dis * 100) / double.Parse(dtSum.Rows[i]["SUM_PRICE"].ToString());

                        DataRow[] drXXX = dtXXX.Select($@" BRANCHID = '{dtSum.Rows[i]["BRANCHID"]}' ");
                        if (drXXX.Length > 0) { s_xxx = double.Parse(drXXX[0]["SUM_DIS"].ToString()); c_xxx = double.Parse(drXXX[0]["COUNT_DIS"].ToString()); }

                        DataRow[] drMNPC = dtMNPC.Select($@" BRANCHID = '{dtSum.Rows[i]["BRANCHID"]}' ");
                        if (drMNPC.Length > 0) { s_mnpc = double.Parse(drMNPC[0]["SUM_DIS"].ToString()); c_mnpc = double.Parse(drMNPC[0]["COUNT_DIS"].ToString()); }

                        DataRow[] drMNBM = dtMNBM.Select($@" BRANCHID = '{dtSum.Rows[i]["BRANCHID"]}' ");
                        if (drMNBM.Length > 0) { s_mnbm = double.Parse(drMNBM[0]["SUM_DIS"].ToString()); c_mnbm = double.Parse(drMNBM[0]["COUNT_DIS"].ToString()); }

                        DataRow[] drNull = dtNull.Select($@" BRANCHID = '{dtSum.Rows[i]["BRANCHID"]}' ");
                        if (drNull.Length > 0) { s_null = double.Parse(drNull[0]["SUM_DIS"].ToString()); c_null = double.Parse(drNull[0]["COUNT_DIS"].ToString()); }

                        if (s_null > (sum_dis / 2)) checkStaSum = "1";
                        if (c_null > (count_dis / 2)) checkStaCount = "1";

                        dtdt.Rows.Add(dtSum.Rows[i]["BRANCHID"], dtSum.Rows[i]["BRANCHNAME"],
                           double.Parse(dtSum.Rows[i]["SUM_PRICE"].ToString()).ToString("N2"), sum_dis.ToString("N2"), percentDis.ToString("N2"),
                           s_xxx.ToString("N2"), s_mnbm.ToString("N2"), s_mnpc.ToString("N2"), s_null.ToString("N2"), checkStaSum,
                           count_dis.ToString("N2"),
                           c_xxx.ToString("N2"), c_mnbm.ToString("N2"), c_mnpc.ToString("N2"), c_null.ToString("N2"), checkStaCount);
                    }

                    dt_Data = dtdt;
                    RadGridView_ShowHD.FilterDescriptors.Clear();
                    RadGridView_ShowHD.DataSource = dt_Data;
                    switch (pTypeShow)
                    {
                        case "0":
                            RadGridView_ShowHD.Columns["SUM_DIS"].IsVisible = true;
                            RadGridView_ShowHD.Columns["PERCENT_DIS"].IsVisible = true;
                            RadGridView_ShowHD.Columns["SUM_XXX"].IsVisible = true;
                            RadGridView_ShowHD.Columns["SUM_MNBM"].IsVisible = true;
                            RadGridView_ShowHD.Columns["SUM_MNPC"].IsVisible = true;
                            RadGridView_ShowHD.Columns["SUM_NULL"].IsVisible = true;

                            RadGridView_ShowHD.Columns["COUNT_DIS"].IsVisible = false;
                            RadGridView_ShowHD.Columns["COUNT_XXX"].IsVisible = false;
                            RadGridView_ShowHD.Columns["COUNT_MNBM"].IsVisible = false;
                            RadGridView_ShowHD.Columns["COUNT_MNPC"].IsVisible = false;
                            RadGridView_ShowHD.Columns["COUNT_NULL"].IsVisible = false;
                            break;
                        case "1":
                            RadGridView_ShowHD.Columns["SUM_DIS"].IsVisible = false;
                            RadGridView_ShowHD.Columns["PERCENT_DIS"].IsVisible = false;
                            RadGridView_ShowHD.Columns["SUM_XXX"].IsVisible = false;
                            RadGridView_ShowHD.Columns["SUM_MNBM"].IsVisible = false;
                            RadGridView_ShowHD.Columns["SUM_MNPC"].IsVisible = false;
                            RadGridView_ShowHD.Columns["SUM_NULL"].IsVisible = false;

                            RadGridView_ShowHD.Columns["COUNT_DIS"].IsVisible = true;
                            RadGridView_ShowHD.Columns["COUNT_XXX"].IsVisible = true;
                            RadGridView_ShowHD.Columns["COUNT_MNBM"].IsVisible = true;
                            RadGridView_ShowHD.Columns["COUNT_MNPC"].IsVisible = true;
                            RadGridView_ShowHD.Columns["COUNT_NULL"].IsVisible = true;
                            break;
                        default:
                            RadGridView_ShowHD.Columns["SUM_DIS"].IsVisible = true;
                            RadGridView_ShowHD.Columns["PERCENT_DIS"].IsVisible = true;
                            RadGridView_ShowHD.Columns["SUM_XXX"].IsVisible = true;
                            RadGridView_ShowHD.Columns["SUM_MNBM"].IsVisible = true;
                            RadGridView_ShowHD.Columns["SUM_MNPC"].IsVisible = true;
                            RadGridView_ShowHD.Columns["SUM_NULL"].IsVisible = true;

                            RadGridView_ShowHD.Columns["COUNT_DIS"].IsVisible = true;
                            RadGridView_ShowHD.Columns["COUNT_XXX"].IsVisible = true;
                            RadGridView_ShowHD.Columns["COUNT_MNBM"].IsVisible = true;
                            RadGridView_ShowHD.Columns["COUNT_MNPC"].IsVisible = true;
                            RadGridView_ShowHD.Columns["COUNT_NULL"].IsVisible = true;
                            break;
                    }
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }
        // 9 - รายงานสินค้าลดราคา ตามสินค้า
        void Case9_SaleBarcodeDiscount(string setCase)
        {
            switch (setCase)
            {
                case "0":
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-7); radDateTimePicker_D2.Value = DateTime.Now.AddDays(0);
                    RadCheckBox_1.Visible = true; RadDropDownList_1.Visible = true;
                    radLabel_Detail.Text = "วันที่ที่ระบุ >> วันที่ระบุการใช้งานบาร์โค้ดลดราคา | สีแดงข่อง เปอร์เซ็นลด >> ลดราคาสินค้ามากกว่า 50%";
                    if (SystemClass.SystemBranchID == "MN000") RadCheckBox_1.Enabled = true;
                    else { RadCheckBox_1.Enabled = false; RadCheckBox_1.Checked = true; }
                    radLabel_Date.Text = "วันที่ทำการลดราคา";
                    DatagridClass.SetDropDownList_Branch(RadDropDownList_1, " 1 ");
                    radCheckBox_Case3.Visible = true; radCheckBox_Case3.Text = "แยกสาขา";
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHID", "สาขา", 80)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCHNAME", "ชื่อสาขา", 130)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค๊ด", 140));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COUNT_RECORD", "รายการที่ลด", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("COUNT_SALE", "รายการที่ขายได้", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTYSUM", "จำนวนที่ลด", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PRICESUM", "ราคาทั้งหมดก่อนลด", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PRICESUM_DIS", "ราคาทั้งหมดหลังลด", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PRICESUM_SALE", "ราคาที่ขายได้", 120));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("PERCENT_DIS", "เปอร์เซ็นลด", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("AVG_RECORD", "เฉลี่ยรายการ/วัน", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LOSS", "ขาดทุน", 100));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DPTID", "ชื่อแผนก", 80));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("DPTNAME", "แผนกสินค้า", 300));

                    ExpressionFormattingObject o1_2 = new ExpressionFormattingObject("MyCondition1", "PERCENT_DIS >= 50 ", false)
                    { CellBackColor = ConfigClass.SetColor_Red() };
                    RadGridView_ShowHD.Columns["PERCENT_DIS"].ConditionalFormattingObjectList.Add(o1_2);

                    GridViewSummaryItem summaryItemA = new GridViewSummaryItem("QTYSUM", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemB = new GridViewSummaryItem("PRICESUM", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemC = new GridViewSummaryItem("PRICESUM_DIS", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemD = new GridViewSummaryItem("COUNT_RECORD", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemE = new GridViewSummaryItem("COUNT_SALE", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemF = new GridViewSummaryItem("PRICESUM_SALE", "{0:n2}", GridAggregateFunction.Sum);
                    GridViewSummaryItem summaryItemG = new GridViewSummaryItem("LOSS", "{0:n2}", GridAggregateFunction.Sum);

                    GridViewSummaryRowItem summaryRowItemA = new GridViewSummaryRowItem { summaryItemA, summaryItemB, summaryItemC, summaryItemD, summaryItemE, summaryItemF, summaryItemG };
                    this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItemA);
                    this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;

                    break;
                case "1":
                    this.Cursor = Cursors.WaitCursor;

                    string conditionBch = "", setGroup = "0";
                    if (RadCheckBox_1.Checked == true) conditionBch = $@" AND BRANCHID = '{RadDropDownList_1.SelectedValue}' ";
                    if (radCheckBox_Case3.Checked == true)
                    {
                        setGroup = "1";
                        RadGridView_ShowHD.Columns["BRANCHID"].IsVisible = true;
                        RadGridView_ShowHD.Columns["BRANCHNAME"].IsVisible = true;
                    }
                    else
                    {
                        RadGridView_ShowHD.Columns["BRANCHID"].IsVisible = false;
                        RadGridView_ShowHD.Columns["BRANCHNAME"].IsVisible = false;
                    }

                    dt_Data = ItembarcodeClass.GetDataMNBM("2", setGroup, conditionBch,
                        radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "");

                    RadGridView_ShowHD.FilterDescriptors.Clear();

                    RadGridView_ShowHD.DataSource = dt_Data;
                    dt_Data.AcceptChanges();
                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }
        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        //Value
        private void RadDateTimePicker_D1_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        //Value
        private void RadDateTimePicker_D2_ValueChanged(object sender, EventArgs e)
        {
            DateTimeSettingClass.SetDateTime_MinMaxValue(radDateTimePicker_D1, radDateTimePicker_D2);
        }
        #endregion

        //Clear
        void ClearTxt()
        {
            if (_pTypeReport == "10")
            {
                if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();
                if (RadGridView_ShowHD.Columns.Count > 0)
                {
                    RadGridView_ShowHD.Columns.Clear();
                    summaryRowItem.Clear();
                    RadGridView_ShowHD.SummaryRowsTop.Clear();
                }
            }
            else
            {
                if (dt_Data.Rows.Count > 0) { dt_Data.Rows.Clear(); dt_Data.AcceptChanges(); }
                radDateTimePicker_D1.Value = DateTime.Now;
                radDateTimePicker_D2.Value = DateTime.Now;
                RadButton_Search.Focus();
            }
        }
        //Find
        private void RadButton_Search_Click(object sender, EventArgs e)
        {
            SetDGV_HD();
        }
        //Add
        private void RadButtonElement_add_Click(object sender, EventArgs e)
        {
            ClearTxt();
        }
        //Excel
        private void RadButtonElement_excel_Click(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0) { return; }
            string T = DatagridClass.ExportExcelGridView(this.Text, RadGridView_ShowHD, "1");
            MsgBoxClass.MsgBoxShow_SaveStatus(T);
        }
        //Change CheckBox
        private void RadCheckBox_1_CheckStateChanged(object sender, EventArgs e)
        {
            if (RadCheckBox_1.CheckState == CheckState.Checked) RadDropDownList_1.Enabled = true; else RadDropDownList_1.Enabled = false;
            if (_pTypeReport == "10")
            {
                if (RadCheckBox_1.CheckState == CheckState.Unchecked) radTextBox_Barcode.Enabled = true; else radTextBox_Barcode.Enabled = false;
                radTextBox_Barcode.Text = ""; radTextBox_Barcode.Focus();
            }

        }
        //DoubleClick
        private void RadGridView_ShowHD_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (e.Row.RowElementType != typeof(GridDataRowElement)) return;

            switch (_pTypeReport)
            {
                case "1":
                    if ((RadGridView_ShowHD.CurrentRow.Cells["INVENTLOCATIONIDTO"].Value.ToString() == "") || (RadGridView_ShowHD.CurrentRow.Cells["ITEMID"].Value.ToString() == ""))
                    {
                        return;
                    }

                    if (MsgBoxClass.MsgBoxShow_ConfirmDelete("นำสินค้าเข้าแผนใบสั่งตามปกติ" + Environment.NewLine +
                        RadGridView_ShowHD.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString()) == DialogResult.No)
                    {
                        return;
                    }

                    string pID = RadGridView_ShowHD.CurrentRow.Cells["ITEMID"].Value.ToString();
                    string pDim = RadGridView_ShowHD.CurrentRow.Cells["INVENTDIMID"].Value.ToString();
                    string pBch = RadGridView_ShowHD.CurrentRow.Cells["INVENTLOCATIONIDTO"].Value.ToString();

                    //string T = Itembarcode_Class.SPC_EXTERNALLIST_SPC_RETAILPLANORDERITEMBLOCKED(pID, pDim, pBch, "DELETE");
                    string T = ConnectionClass.ExecuteSQL_ArrayMainAX(AX_SendData.Save_EXTERNALLIST_RETAILPLANORDERITEMBLOCKED(pID, pDim, pBch, "DELETE"));
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    break;
                case "3":
                    switch (e.Column.Name)
                    {
                        case "ITEMBARCODE":
                            OpenItembarcode(RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(), RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
                            break;
                        case "SPC_ITEMNAME":
                            OpenItembarcode(RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(), RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
                            break;
                        case "UNITID":
                            OpenItembarcode(RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(), RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
                            break;
                        case "ZONE_ID":
                            ConfigZone(RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                                RadGridView_ShowHD.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                                RadGridView_ShowHD.CurrentRow.Cells["UNITID"].Value.ToString(),
                                RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                                RadGridView_ShowHD.CurrentRow.Cells["ZONE_ID"].Value.ToString());
                            break;
                        case "ZONE_NAME":
                            ConfigZone(RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["UNITID"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["ZONE_ID"].Value.ToString());
                            break;
                        case "SHELF_ID":
                            ConfigZone(RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["UNITID"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["ZONE_ID"].Value.ToString());
                            break;
                        case "SHELF_NAME":
                            ConfigZone(RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["UNITID"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString(),
                               RadGridView_ShowHD.CurrentRow.Cells["ZONE_ID"].Value.ToString());
                            break;
                        default:
                            break;
                    }
                    break;
                case "6"://Barcode Discount 
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        if ((e.Column.Name == "ITEMBARCODE_DIS") && ((SystemClass.SystemDptID == "D054") || (SystemClass.SystemComMinimart == "1"))) // ยกเลิกบาร์โค้ดลดราคา && (SystemClass.SystemComMinimart == "1")
                        {
                            if ((RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEID"].Value.ToString() == "") && (Double.Parse(RadGridView_ShowHD.CurrentRow.Cells["PRICE_DIS"].Value.ToString()) > 0))
                            {
                                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการยกเลิก การใช้งานบาร์โค้ดนี้ ?") == DialogResult.Yes)
                                {
                                    FormShare.InputData f = new FormShare.InputData("1", RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE_DIS"].Value.ToString(), "หมายเหตุที่ยกเลิก.", "");
                                    if (f.ShowDialog(this) == DialogResult.Yes)
                                    {
                                        string sqlUp = $@"
                                            UPDATE  SHOP_BARCODEDISCOUNT
                                            SET     CHECK_STA = '1',XXX_INVOICEID  = 'CANCEL',
                                                    CHECK_REMARK = '{f.pInputData}',CHECK_WHOID = '{SystemClass.SystemUserID}',
                                                    CHECK_WHONAME = '{SystemClass.SystemUserName}',CHECK_DATE = GETDATE() 
                                            WHERE   ITEMBARCODE_DIS = '{ RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE_DIS"].Value}' ";
                                        string T6 = ConnectionClass.ExecuteSQL_Main(sqlUp);
                                        MsgBoxClass.MsgBoxShow_SaveStatus(T6);
                                        if (T6 == "")
                                        {
                                            RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEID"].Value = "CANCEL";
                                            RadGridView_ShowHD.CurrentRow.Cells["CHECK_STA"].Value = "1";
                                            RadGridView_ShowHD.CurrentRow.Cells["CHECK_REMARK"].Value = f.pInputData;
                                            RadGridView_ShowHD.CurrentRow.Cells["CHECK_WHOID"].Value = SystemClass.SystemUserID;
                                            RadGridView_ShowHD.CurrentRow.Cells["CHECK_WHONAME"].Value = SystemClass.SystemUserName;
                                            RadGridView_ShowHD.CurrentRow.Cells["CHECK_DATE"].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                            return;
                                        }
                                        else return;
                                    }
                                    else return;
                                }
                            }
                        }
                        if ((e.Column.Name == "ITEMBARCODE_DIS") && (RadGridView_ShowHD.CurrentRow.Cells["PRICE_DIS"].Value.ToString() == "0"))
                        {
                            if (RadGridView_ShowHD.CurrentRow.Cells["BRANCHID"].Value.ToString() != "MN000")
                            {
                                MsgBoxClass.MsgBoxShowButtonOk_Warning("บาร์โค๊ดที่ถูกระบุสาขาแล้ว จะไม่สามารถพิมพ์ซ้ำที่สาขาใหญ่ได้");
                                return;
                            }
                            int iii = RadGridView_ShowHD.CurrentRow.Index;
                            Case6_BarcodeDiscount("4", iii);
                            return;
                        }
                        else
                        {
                            if (RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEID"].Value.ToString() != "")
                                Case6_BarcodeDiscount("5");
                            else return;
                        }

                    }
                    else
                    {
                        if ((e.Column.Name == "ITEMBARCODE_DIS"))
                        {
                            if ((RadGridView_ShowHD.CurrentRow.Cells["PRICE_DIS"].Value.ToString() == "0"))
                            {
                                int iii = RadGridView_ShowHD.CurrentRow.Index;
                                barcodePrint = RadGridView_ShowHD.Rows[iii].Cells["ITEMBARCODE_DIS"].Value.ToString();
                                namePrint = "";
                                Case6_BarcodeDiscount("3", iii, int.Parse(RadGridView_ShowHD.Rows[iii].Cells["COUNT_PRINT"].Value.ToString()));
                                return;
                            }
                            if ((RadGridView_ShowHD.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() != "") && (RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEID"].Value.ToString() == ""))
                            {
                                //MsgBoxClass.MsgBoxShowButtonOk_Imformation(RadGridView_ShowHD.CurrentRow.Index.ToString());
                                int iii = RadGridView_ShowHD.CurrentRow.Index;
                                barcodePrint = RadGridView_ShowHD.Rows[iii].Cells["ITEMBARCODE_DIS"].Value.ToString();
                                namePrint = RadGridView_ShowHD.Rows[iii].Cells["SPC_ITEMNAME_DIS"].Value.ToString() + Environment.NewLine +
                                    "ราคาขาย : " + RadGridView_ShowHD.Rows[iii].Cells["PRICE_DIS"].Value.ToString() + " บาท";
                                Case6_BarcodeDiscount("3", iii, int.Parse(RadGridView_ShowHD.Rows[iii].Cells["COUNT_PRINT"].Value.ToString()));
                                return;
                            }
                        }
                        else
                        {
                            if (SystemClass.SystemUserPositionName.StartsWith("ผู้จัดการร้าน") || (SystemClass.SystemComMinimart == "1"))
                            {
                                if (RadGridView_ShowHD.CurrentRow.Cells["XXX_INVOICEID"].Value.ToString() != "")
                                    Case6_BarcodeDiscount("5");
                                else return;
                            }
                        }
                    }
                    break;
                case "7":
                    if (SystemClass.SystemBranchID == "MN000") Case7_SaleBarcode164("2");
                    else
                    {
                        if (SystemClass.SystemUserPositionName.StartsWith("ผู้จัดการร้าน")) { Case7_SaleBarcode164("2"); return; }
                        if (SystemClass.SystemComMinimart == "1") { Case7_SaleBarcode164("2"); return; }
                    }
                    break;
              
                default:
                    break;
            }
        }
        //Document
        private void RadButtonElement_pdf_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeReport);
        }
        //open
        void OpenItembarcode(string barcode, string bch)
        {
            Itembarcode.ItembarcodeDetail _itembarcodeDetail = new Itembarcode.ItembarcodeDetail("0", bch, "0", "1", barcode);
            _itembarcodeDetail.Show();
        }
        //Set Zone
        void ConfigZone(string barcode, string itemname, string unitid, string bch, string zoneID)
        {
            if ((RadGridView_ShowHD.Rows.Count == 0) || (barcode == "") || (RadGridView_ShowHD.CurrentRow.Index == -1)) return;

            string sta = "1", rmk = "";
            if (zoneID != "")
            {
                sta = "0";

                DataTable dtChange = ItembarcodeClass.GetSHELFITEMS_CHANGE(barcode, bch);
                if (dtChange.Rows.Count == 0) return;

                if (SystemClass.SystemBranchID != "MN000")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"สินค้าที่ระบุอยู่ในสถานะรอเปลี่ยนชั้นวาง{Environment.NewLine}ไม่สามารถแก้ไขชั้นวางได้{Environment.NewLine}รอการยืนยันสถานการเปลี่ยนชั้นวางเก่าให้เรียบร้อยก่อนการเปลี่ยนชั้นวางใหม่อีกครั้ง");
                    return;
                }
                else
                {
                    string _zoneID = dtChange.Rows[0]["ZONE_ID"].ToString();
                    string _zoneName = dtChange.Rows[0]["ZONE_NAME"].ToString();
                    string _shelfID = dtChange.Rows[0]["SHELF_ID"].ToString();
                    string _shelfName = dtChange.Rows[0]["SHELF_NAME"].ToString();

                    string strNew = @"ยืนยันการเปลี่ยนชั้นวาง" + Environment.NewLine + dtChange.Rows[0]["ZONE_NAME"].ToString() + Environment.NewLine +
                        dtChange.Rows[0]["SHELF_NAME"].ToString();
                    string strOld = @"ไม่เปลี่ยนชั้นวาง" + Environment.NewLine + dtChange.Rows[0]["ZONE_NAME_OLD"].ToString() + Environment.NewLine +
                        dtChange.Rows[0]["SHELF_NAME_OLD"].ToString();
                    FormShare.ChooseData _chooseData = new FormShare.ChooseData(strNew, strOld, dtChange.Rows[0]["REAMRK"].ToString());
                    if (_chooseData.ShowDialog(this) == DialogResult.Yes)
                    {
                        ArrayList strIn = new ArrayList();
                        switch (_chooseData.sSendData)
                        {
                            case "1":
                                strIn.Add(ItembarcodeClass.Update_SHELFITEMS("1", dtChange.Rows[0]["SHELF_ID"].ToString(),
                             dtChange.Rows[0]["ZONE_ID"].ToString(), barcode, bch));
                                strIn.Add(ItembarcodeClass.Update_SHELFITEMS("2", "", "", barcode, bch));

                                break;
                            case "0":
                                strIn.Add(ItembarcodeClass.Update_SHELFITEMS("0", "", "", barcode, bch));
                                _zoneID = dtChange.Rows[0]["ZONE_ID_OLD"].ToString();
                                _zoneName = dtChange.Rows[0]["ZONE_NAME_OLD"].ToString();
                                _shelfID = dtChange.Rows[0]["SHELF_ID_OLD"].ToString();
                                _shelfName = dtChange.Rows[0]["SHELF_NAME_OLD"].ToString();
                                break;
                            default:
                                break;
                        }

                        if (strIn.Count == 0) return;

                        string T = ConnectionClass.ExecuteSQL_ArrayMain(strIn);
                        if (T == "")
                        {
                            RadGridView_ShowHD.CurrentRow.Cells["ZONE_ID"].Value = _zoneID;
                            RadGridView_ShowHD.CurrentRow.Cells["ZONE_NAME"].Value = _zoneName;
                            RadGridView_ShowHD.CurrentRow.Cells["SHELF_ID"].Value = _shelfID;
                            RadGridView_ShowHD.CurrentRow.Cells["SHELF_NAME"].Value = _shelfName;
                            RadGridView_ShowHD.CurrentRow.Cells["CC"].Value = "";
                        }
                        dt_Data.AcceptChanges();
                        MsgBoxClass.MsgBoxShow_SaveStatus(T);
                        return;

                    }
                }

                if (SystemClass.SystemBranchID == "MN000") return;

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("สินค้ามีจัดวางเรียบร้อยแล้ว ต้องการเปลี่ยนชันวาง ?") == DialogResult.No) return;

                FormShare.InputData _inputData = new FormShare.InputData("1", barcode + " " + itemname, "ระบุ - เหตุผลสำหรับการเปลี่ยนแปลงชั้นวาง", unitid);
                if (_inputData.ShowDialog(this) == DialogResult.No)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("การเปลี่ยนแปลงชั้นวาง จำเป็นต้องระบุเหตุผลในการเปลี่ยนเท่านั้น.");
                    return;
                }

                rmk = _inputData.pInputData;
            }

            if (SystemClass.SystemBranchID == "MN000") return;

            DataTable dt = ItembarcodeClass.GetSHELF_Add(bch);
            if (dt.Rows.Count > 0)
            {
                FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV() { dtData = dt };
                if (frm.ShowDialog(this) == DialogResult.Yes)
                {
                    string strIn, staChange = "";
                    if (sta == "1")
                    {

                        strIn = ItembarcodeClass.Save_SHELFITEMS(frm.pID, frm.pID2, barcode, unitid, "", "", "");
                    }
                    else
                    {
                        DataTable dtZone = ItembarcodeClass.GetSHELFITEMS_Check(barcode, bch);
                        if (dtZone.Rows.Count == 0) return;

                        strIn = ItembarcodeClass.Save_SHELFITEMS(frm.pID, frm.pID2, barcode, unitid,
                                    dtZone.Rows[0]["ZONE_ID"].ToString(), dtZone.Rows[0]["SHELF_ID"].ToString(), rmk);
                        staChange = barcode;
                    }

                    string T = ConnectionClass.ExecuteSQL_Main(strIn);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "")
                    {
                        RadGridView_ShowHD.CurrentRow.Cells["ZONE_ID"].Value = frm.pID2;
                        RadGridView_ShowHD.CurrentRow.Cells["ZONE_NAME"].Value = frm.pDesc2;
                        RadGridView_ShowHD.CurrentRow.Cells["SHELF_ID"].Value = frm.pID;
                        RadGridView_ShowHD.CurrentRow.Cells["SHELF_NAME"].Value = frm.pDesc;
                        RadGridView_ShowHD.CurrentRow.Cells["CC"].Value = staChange;
                    }
                }
            }
        }

        private void RadGridView_ShowHD_DoubleClick(object sender, EventArgs e)
        {
            switch (_pTypeReport)
            {
                case "5":
                    if (_pCondition == "D062")
                    {
                        BillOut_Detail frm = new BillOut_Detail(RadGridView_ShowHD.CurrentRow.Cells["DOCNO"].Value.ToString());
                        frm.ShowDialog(this);
                    }
                    break;
                default:
                    break;
            }

        }

        private void RadButtonElement_barcode_Click(object sender, EventArgs e)
        {
            if (_pTypeReport == "6")
            {
                Case6_BarcodeDiscount("2");
            }

        }

        private void RadRadioButton_date2_CheckStateChanged(object sender, EventArgs e)
        {
            radCheckBox_NoShelf.Checked = false;
            radCheckBox_Case3.Checked = false;
            radCheckBox_ChangeShelf.Checked = false;
        }

        private void RadRadioButton_date1_CheckStateChanged(object sender, EventArgs e)
        {
            radCheckBox_NoShelf.Checked = false;
            radCheckBox_Case3.Checked = false;
            radCheckBox_ChangeShelf.Checked = true;
        }

        private void PrintDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            StringFormat stringFormat = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };
            int Y = 0;
            barcode.Data = barcodePrint;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            e.Graphics.DrawString("*** สินค้าลดราคา ***", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            Rectangle rect = new Rectangle(0, Y, 150, 20);
            e.Graphics.DrawString(barcodePrint, SystemClass.printFont, Brushes.Black, rect, stringFormat);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 60;
            if (namePrint != "")
            {
                e.Graphics.DrawString(namePrint, SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 20;
                Rectangle rect1 = new Rectangle(0, Y, 150, 45);
                e.Graphics.DrawString(SystemClass.SystemBranchName, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            }
            else
            {
                Rectangle rect1 = new Rectangle(0, Y, 150, 20);
                e.Graphics.DrawString(SystemClass.SystemBranchName, SystemClass.printFont, Brushes.Black, rect1, stringFormat);
            }
            Y += 30;
            e.Graphics.DrawString("*ซื้อแล้วไม่รับคืนทุกกรณี*", SystemClass.printFont, Brushes.Black, 0, Y);

        }

        //Copy
        private void RadButton_Copy_Click(object sender, EventArgs e)
        {
            if (SystemClass.SystemBranchID == "MN000")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("สำหรับฟังก์ชั่นนี้ เปิดใช้งานที่สาขาเท่านั้น.");
                return;
            }

            DataTable dt;
            dt = BranchClass.GetBranchAll_ByConditions("'1'", " '1'", "");
            FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV()
            {
                dtData = dt
            };

            string bchID_Old, bchID_Name;
            if (frm.ShowDialog(this) == DialogResult.Yes)
            {
                bchID_Old = frm.pID;
                bchID_Name = frm.pDesc;
            }
            else
            { return; }

            if (bchID_Old == "") { return; }
            if (bchID_Old == SystemClass.SystemBranchID)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"การคัดลอกชั้นวางไม่สามารถใช้งานได้ ถ้าเลือกสาขาเดียวกับที่เปิดใช้งาน{Environment.NewLine}ต้องเลือกสาขาอื่นแทน.");
                return;
            }

            if (MsgBoxClass.MsgBoxShowYesNo_DialogResult($@"ยืนยันการคัดลองชั้นวางจากสาขา {bchID_Name} ?") == DialogResult.No) { return; }

            MsgBoxClass.MsgBoxShow_SaveStatus(ConnectionClass.ExecuteSQL_Main(ItembarcodeClass.Copy_SHELFITEMS(bchID_Old, SystemClass.SystemBranchID, SystemClass.SystemBranchName)));
            return;

        }
        // 10 - รายงานการขายสินค้าตามกลุ่มสินค้าที่ตั้งไว้
        void Case10_SaleBarcodeByGroupFix(string setCase)
        {
            switch (setCase)
            {
                case "0"://SetForm
                    dt_Grp = ConfigClass.FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID("55", "", "ORDER BY SHOW_ID", "1");
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(-2); radDateTimePicker_D2.Value = DateTime.Now.AddDays(-1);
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(-2), DateTime.Now.AddDays(-1));
                    DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D2, DateTime.Now.AddDays(-1), DateTime.Now.AddDays(0));

                    RadCheckBox_1.Visible = true; RadCheckBox_1.Text = "ระบุกลุ่ม"; RadCheckBox_1.Checked = true;
                    RadDropDownList_1.Visible = true;
                    RadDropDownList_1.DataSource = dt_Grp;
                    RadDropDownList_1.ValueMember = "SHOW_ID"; RadDropDownList_1.DisplayMember = "SHOW_NAME";

                    radLabel_Date.Text = "ระบุวันที่ขาย";
                    radLabel_Detail.Text = "ถ้าไม่เลือก ระบุกลุ่ม >> สามารถดึงได้ตามบาร์โค้ด[ที่ไม่ต้องตั้งค่า] จะออกเป็นการแยกบาร์โค้ด หรือ รหัสสินค้า [ต้องขึ้นต้นด้วย P] จะออกเป็นการรวมบาร์โค้ด";

                    radLabel_Txt.Text = "ระบุบาร์โค้ด หรือ รหัสสินค้า"; radLabel_Txt.Visible = true;
                    radTextBox_Barcode.Visible = true; radTextBox_Barcode.Visible = true; radTextBox_Barcode.Enabled = false;
                    break;
                case "1"://Query
                    //สามารถออกรายงานเพื่อระบุโค้ดได้ โดยไม่ต้องยึดตามกลุ่ม
                    if (RadCheckBox_1.Checked != true)
                    {
                        if (radTextBox_Barcode.Text.Trim() == "")
                        {
                            MsgBoxClass.MsgBoxShowButtonOk_Warning($@"ถ้าไม่เลือกตามกลุ่มที่ตั้งไว้{Environment.NewLine}ให้ระบุบาร์โค้ดเท่านั้น");
                            radTextBox_Barcode.Focus();
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }

                    //กลุ่มสินค้าที่เลือกนั้น ต้องการแสดงรายงานแบบรวมโค้ดหรือแยกโค้ด 0 รวมบาร์โค้ด 1 แยกโค้ด
                    DataRow[] iRGrp = dt_Grp.Select($@" SHOW_ID = '{RadDropDownList_1.SelectedValue}' ");
                    string staGrpBarcode = iRGrp[0]["MaxImage"].ToString();

                    //ใช้สำหรับการค้นหาบาร์โค้ดในกลุ่มที่เลือก กรณีแรก ต้องการข้อมูลทั้งหมด กรณีต่อมาคือต้องการเฉพาะบาร์โค้ด เอาไปใส่ใน sql เพื่อหาการขาย
                    string sqlSelect = $@" SELECT TMP.SHOW_ID AS GRP_ID,TMP.SHOW_NAME AS GRP_NAME,SHOP_CONFIGBRANCH_DETAIL.SHOW_ID AS ITEMBARCODE,SHOP_CONFIGBRANCH_DETAIL.SHOW_NAME AS SPC_ITEMNAME ";
                    string sqlOrderBy = " ORDER BY SHOP_CONFIGBRANCH_DETAIL.SHOW_ID ";
                    string sqlMain = $@"
                            FROM	SHOP_CONFIGBRANCH_DETAIL WITH (NOLOCK) 
		                            INNER JOIN (SELECT * FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK) WHERE	TYPE_CONFIG = '55' AND STA = '1' AND SHOW_ID = '{RadDropDownList_1.SelectedValue}')TMP ON SHOP_CONFIGBRANCH_DETAIL.BRANCH_ID = TMP.SHOW_ID
                            WHERE	SHOP_CONFIGBRANCH_DETAIL.TYPE_CONFIG = '56'  AND STA = '1' ";
                    string sqlBarcodeFindSale = $@" AND XXX_POSLINE.ITEMBARCODE IN ( SELECT SHOP_CONFIGBRANCH_DETAIL.SHOW_ID AS BARCODE  {sqlMain}) ";

                    //ค้นหาสาขา
                    DataTable dt_Row_Bch = ConfigClass.FindData_BranchconfigByTypeID("55", $@" AND SHOW_ID = '{RadDropDownList_1.SelectedValue}' ");
                    //ค้นหาบาร์โค้ดทั้งหมดของกลุ่มที่เลือก
                    DataTable dtBarcode = ConnectionClass.SelectSQL_Main(sqlSelect + sqlMain + sqlOrderBy);

                    //กรณีที่เลือกตามบาร์โค้ดที่ต้องการ ก็จะปรับเงื่อนไขในการแสดงข้อมูล
                    if (RadCheckBox_1.Checked == false)
                    {
                        //เปนทุกสาขาแทน
                        dt_Row_Bch = BranchClass.GetBranchAll("1", "1");
                        if (radTextBox_Barcode.Text.Trim().StartsWith("P"))//กำหนดดึงตามบาร์โค้ด หรือ รหัสสินค้า
                        {
                            staGrpBarcode = "0";
                            //เงื่อนไขการดึงข้อมูลการขาย
                            sqlBarcodeFindSale = $@" AND XXX_POSLINE.ITEMID = '{radTextBox_Barcode.Text.Trim()}' ";
                        }
                        else
                        {
                            staGrpBarcode = "1";
                            //เงื่อนไขการดึงข้อมูลการขาย
                            sqlBarcodeFindSale = $@" AND XXX_POSLINE.ITEMBARCODE = '{radTextBox_Barcode.Text.Trim()}' ";
                            //เงื่อนไขการแสดงบาร์โค้ดที่ต้องการ
                            dtBarcode = ItembarcodeClass.GetItembarcode_ALLDeatil(radTextBox_Barcode.Text.Trim());
                        }
                    }

                    this.Cursor = Cursors.WaitCursor;

                    if (RadGridView_ShowHD.Columns.Count > 0)
                    {
                        RadGridView_ShowHD.Columns.Clear();
                        summaryRowItem.Clear();
                        RadGridView_ShowHD.SummaryRowsTop.Clear();
                    }
                    if (RadGridView_ShowHD.Rows.Count > 0) RadGridView_ShowHD.Rows.Clear();

                    double intDay = Math.Ceiling((radDateTimePicker_D2.Value.Date - radDateTimePicker_D1.Value.Date).TotalDays) + 1;

                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "รหัส", 100)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 170)));
                    RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("GRAND", "ยอดรวมขาย", 120)));

                    RadGridView_ShowHD.Columns["BRANCH_ID"].IsPinned = true;
                    RadGridView_ShowHD.Columns["BRANCH_NAME"].IsPinned = true;
                    RadGridView_ShowHD.Columns["GRAND"].IsPinned = true;

                    string staMNRR = "0";//คิดยอกเบิกสินค้า
                    if (RadDropDownList_1.SelectedValue.ToString() == "R005")
                    {
                        staMNRR = "1";
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("GRAND_MNRR", "ยอดรวมเบิก", 120)));
                        RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("GRAND_DIFF", "สรุปยอด", 120)));
                        RadGridView_ShowHD.Columns["GRAND_MNRR"].IsPinned = true;
                        RadGridView_ShowHD.Columns["GRAND_DIFF"].IsPinned = true;
                        DatagridClass.SetCellBackClolorByExpression($@"GRAND_DIFF", $@" GRAND_DIFF < 0", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                    }

                    //สร้างคอลัมภ์ทั้งหมด
                    if (staGrpBarcode == "0")    //0 รวมบาร์โค้ด
                    {
                        RadGridView_ShowHD.TableElement.TableHeaderHeight = 50;
                        //สร้างคอลัมตามจำนวนวัน เนื่องจากเป็นการรวมบาร์โค้ด
                        for (int iDay = 0; iDay < intDay; iDay++)
                        {
                            string headColume = "A" + Convert.ToString(iDay);
                            var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight(headColume, pDate, 150)));
                            if (staMNRR == "1")
                            {
                                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("MNRR" + Convert.ToString(iDay), "ยอดเบิก", 150)));
                                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("DIFF" + Convert.ToString(iDay), "สรุป", 150)));
                                DatagridClass.SetCellBackClolorByExpression($@"DIFF{Convert.ToString(iDay)}", $@"DIFF{Convert.ToString(iDay)} < 0", ConfigClass.SetColor_Red(), RadGridView_ShowHD);
                            }
                            //สร้างจำนวนรวมไปบนสุดของคอลัมภ์
                            GridViewSummaryItem summaryItem = new GridViewSummaryItem(headColume, "{0}", GridAggregateFunction.Sum) { FormatString = "{0:N2}" };
                            summaryRowItem.Add(summaryItem);
                            summaryRowItem.Add(new GridViewSummaryItem("MNRR" + Convert.ToString(iDay), "{0}", GridAggregateFunction.Sum) { FormatString = "{0:N2}" });
                            summaryRowItem.Add(new GridViewSummaryItem("DIFF" + Convert.ToString(iDay), "{0}", GridAggregateFunction.Sum) { FormatString = "{0:N2}" });
                        }
                        this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
                        this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
                    }
                    else // 1 แยกบาร์โค้ด
                    {
                        RadGridView_ShowHD.TableElement.TableHeaderHeight = 80;
                        //สร้างคอลัมตามจำนวนวัน 
                        for (int iDay = 0; iDay < intDay; iDay++)
                        {
                            //สร้างคอลัมภ์ตามจำนวนบาร์โค้ดด้วยจากจำนวนวัน
                            for (int iC_Barcode = 0; iC_Barcode < dtBarcode.Rows.Count; iC_Barcode++)
                            {
                                string barcode = dtBarcode.Rows[iC_Barcode]["ITEMBARCODE"].ToString();
                                string headColume = "A" + Convert.ToString(iDay) + "|" + barcode;
                                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd") + Environment.NewLine + barcode + Environment.NewLine + dtBarcode.Rows[iC_Barcode]["SPC_ITEMNAME"].ToString();
                                RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter(headColume, pDate, 150)));
                                //สร้างจำนวนรวมไปบนสุดของคอลัมภ์
                                GridViewSummaryItem summaryItem = new GridViewSummaryItem(headColume, "{0}", GridAggregateFunction.Sum) { FormatString = "{0:N2}" };
                                summaryRowItem.Add(summaryItem);
                            }
                        }
                        this.RadGridView_ShowHD.SummaryRowsTop.Add(summaryRowItem);
                        this.RadGridView_ShowHD.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Top;
                    }

                    //ดึงข้อมูลลการขายทั้งหมด ตามกลุ่มที่เลือก โดยที่ staGrpBarcode จะเป็นการแยกว่า ข้อมูลจะออกมาเป็นบาร์โค้ดหรือรวมทั้งหมด
                    DataTable dtData = PosSaleClass.GetDetailSale_ByBarcode($@"{staGrpBarcode}", "",
                            radDateTimePicker_D1.Value.ToString("yyyy-MM-dd"), radDateTimePicker_D2.Value.ToString("yyyy-MM-dd"), "", sqlBarcodeFindSale);

                    DataTable dtMNRR = new DataTable();
                    if (staMNRR == "1")
                    {
                        string sqlMNRR = $@"
                        SELECT	MNRRBRANCH,CONVERT(VARCHAR,SHOP_MNRR_HD.MNRRDate,23) AS DATEMNRR,SUM(MNRRPriceSum) AS MNRRPriceSum
                        FROM	SHOP_MNRR_HD WITH (NOLOCK)	
		                        INNER JOIN SHOP_MNRR_DT WITH (NOLOCK) ON SHOP_MNRR_HD.MNRRDOCNO = SHOP_MNRR_DT.MNRRDOCNO
                        WHERE	CONVERT(VARCHAR,SHOP_MNRR_HD.MNRRDate,23) BETWEEN '{radDateTimePicker_D1.Value:yyyy-MM-dd}' AND '{radDateTimePicker_D2.Value:yyyy-MM-dd}'
		                        AND MNRRStaDoc = '1'
		                        AND MNRRSta = '1'
		                        AND MNRRQtyOrder > 0
		                        AND SHOP_MNRR_HD.MNRRRemark = 'เบิกสำหรับทำกับข้าว-ขนมหวาน-ชงเครื่องดื่ม-แพ็คอาหารขาย'
                        GROUP BY MNRRBRANCH,CONVERT(VARCHAR,SHOP_MNRR_HD.MNRRDate,23)  ";
                        dtMNRR = ConnectionClass.SelectSQL_Main(sqlMNRR);
                    }

                    if (dtData.Rows.Count == 0)
                    {
                        MsgBoxClass.MsgBoxShowButtonOk_Imformation("ไม่พบข้อมูลการขายที่ค้นหา ลองใหม่อีกครั้ง");
                        this.Cursor = Cursors.Default;
                        return;
                    }

                    //ใส่ข้อมูลทั้งหมดในรายงาน หลักการก็เหมือนกันการ การสร้างคอลัมภ์เลย

                    if (staGrpBarcode == "0")//Sum Barcode
                    {
                        for (int iR = 0; iR < dt_Row_Bch.Rows.Count; iR++)
                        {
                            double sumRow = 0, sumMNRR = 0;
                            RadGridView_ShowHD.Rows.Add(dt_Row_Bch.Rows[iR]["BRANCH_ID"].ToString(), dt_Row_Bch.Rows[iR]["BRANCH_NAME"].ToString());
                            for (int iDay = 0; iDay < intDay; iDay++)
                            {
                                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                                DataRow[] drResualt = dtData.Select($@" INVOICEDATE = '{pDate}' AND POSGROUP = '{dt_Row_Bch.Rows[iR]["BRANCH_ID"]}' ");
                                double grand = 0;
                                if (drResualt.Length > 0) grand = double.Parse(drResualt[0]["LINEAMOUNT"].ToString());
                                RadGridView_ShowHD.Rows[iR].Cells[$@"A{Convert.ToString(iDay)}"].Value = grand.ToString("N2");
                                sumRow += grand;

                                if (staMNRR == "1")
                                {
                                    double mnrr = 0;
                                    if (dtMNRR.Rows.Count > 0)
                                    {
                                        DataRow[] drMNRR = dtMNRR.Select($@" MNRRBRANCH = '{dt_Row_Bch.Rows[iR]["BRANCH_ID"]}' AND DATEMNRR = '{pDate}' ");
                                        if (drMNRR.Length > 0) mnrr = Double.Parse(drMNRR[0]["MNRRPriceSum"].ToString());

                                        RadGridView_ShowHD.Rows[iR].Cells[$@"MNRR{Convert.ToString(iDay)}"].Value = mnrr.ToString("N2");
                                        RadGridView_ShowHD.Rows[iR].Cells[$@"DIFF{Convert.ToString(iDay)}"].Value = (grand - mnrr).ToString("N2");
                                        sumMNRR += mnrr;
                                    }

                                }
                            }
                            RadGridView_ShowHD.Rows[iR].Cells["GRAND"].Value = sumRow.ToString("N2");
                            if (staMNRR == "1")
                            {
                                RadGridView_ShowHD.Rows[iR].Cells["GRAND_MNRR"].Value = sumMNRR.ToString("N2");
                                RadGridView_ShowHD.Rows[iR].Cells["GRAND_DIFF"].Value = (sumRow - sumMNRR).ToString("N2");
                            }
                        }
                    }
                    else // By Barcode
                    {
                        for (int iR = 0; iR < dt_Row_Bch.Rows.Count; iR++)
                        {
                            double sumRow = 0;
                            RadGridView_ShowHD.Rows.Add(dt_Row_Bch.Rows[iR]["BRANCH_ID"].ToString(), dt_Row_Bch.Rows[iR]["BRANCH_NAME"].ToString());
                            for (int iDay = 0; iDay < intDay; iDay++)
                            {
                                var pDate = radDateTimePicker_D1.Value.AddDays(iDay).ToString("yyyy-MM-dd");
                                for (int iC_Barcode = 0; iC_Barcode < dtBarcode.Rows.Count; iC_Barcode++)
                                {
                                    string barcode = dtBarcode.Rows[iC_Barcode]["ITEMBARCODE"].ToString();
                                    string headColume = "A" + Convert.ToString(iDay) + "|" + barcode;

                                    DataRow[] drResualt = dtData.Select($@" INVOICEDATE = '{pDate}' AND POSGROUP = '{dt_Row_Bch.Rows[iR]["BRANCH_ID"]}' AND ITEMBARCODE = '{barcode}' ");
                                    double grand = 0;
                                    if (drResualt.Length > 0) grand = double.Parse(drResualt[0]["LINEAMOUNT"].ToString());
                                    RadGridView_ShowHD.Rows[iR].Cells[headColume].Value = grand.ToString("N2");
                                    sumRow += grand;
                                }
                            }
                            RadGridView_ShowHD.Rows[iR].Cells["GRAND"].Value = sumRow.ToString("N2");
                        }
                    }

                    //ในกรณีที่ระบุบาร์โค้ด หรือ รหัสสินค้า จะซ่อนยอดเงินออกสำหรับสาขาเป็น 0
                    if (RadCheckBox_1.Checked == false)
                    {
                        for (int iCheck = 0; iCheck < RadGridView_ShowHD.Rows.Count; iCheck++)
                        {
                            if (Double.Parse(RadGridView_ShowHD.Rows[iCheck].Cells["GRAND"].Value.ToString()) == 0) RadGridView_ShowHD.Rows[iCheck].IsVisible = false;
                        }
                    }

                    this.Cursor = Cursors.Default;
                    break;
                default:
                    break;
            }
        }
    }
}
