﻿//CheckOK
using System;
using System.Data;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using PC_Shop24Hrs.Class;
using Telerik.WinControls.UI;

namespace PC_Shop24Hrs.Itembarcode
{
    public partial class Itembarcode_History : Telerik.WinControls.UI.RadForm
    {
        DataTable dt_DataHD = new DataTable();
        DataTable dt_DataDT = new DataTable();

        readonly string _pItembarcode;
        

        string pBchID;

      
        //Load
        public Itembarcode_History(string pItembarcode) 
        {
            InitializeComponent();
            _pItembarcode = pItembarcode;
           
        }
        //Load
        private void Itembarcode_History_Load(object sender, EventArgs e)
        {
            RadButtonElement_pdt.ShowBorder = true;
            RadButtonElement_pdt.ToolTipText = "คู่มือการใช้งาน";

            radStatusStrip1.SizingGrip = false;
            radRadioButtonElement_Recive.ShowBorder = true;
            radRadioButtonElement_Sale.ShowBorder = true;
            radRadioButtonElement_PC.ShowBorder = true;
            radRadioButtonElement_Stock.ShowBorder = true;
            radRadioButtonElement_CountStock.ShowBorder = true;

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowHD);
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_ID", "สาขา", 100)));
            RadGridView_ShowHD.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("BRANCH_NAME", "ชื่อสาขา", 180)));

            DatagridClass.SetDefaultRadGridView(RadGridView_ShowDT);
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SALEDATE", "ข้อมูล", 100)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetCenter("SALEQTY", "จำนวน", 120)));
            RadGridView_ShowDT.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("DOCNO", "เลขที่นับ", 150)));
            RadGridView_ShowDT.MasterTemplate.Columns["DOCNO"].IsVisible = false;

            RadGridView_ShowDT.Columns["SALEQTY"].FormatString = "{0:#,##0.00}";

            pBchID = "";

            SetDGV_HD();
        }

        //Set HD
        void SetDGV_HD()
        {
            if (dt_DataHD.Rows.Count > 0) { dt_DataHD.Rows.Clear(); }
            this.Cursor = Cursors.WaitCursor;
            if (SystemClass.SystemBranchID == "MN000")
            {
                DataTable dtB = BranchClass.GetBranchAll("'1','4'", "'1'");

                DataTable dtBch = new DataTable();
                dtBch.Columns.Add("BRANCH_ID");
                dtBch.Columns.Add("BRANCH_NAME");

                dtBch.Rows.Add("COM-MN", "ComMinimart");
                dtBch.Rows.Add("RETAILAREA", "พื้นที่ขาย");
                dtBch.Rows.Add("WH-A", "คลังสินค้า A");
                dtBch.Rows.Add("WH-CENTER", "WH-CENTER");

                for (int i = 0; i < dtB.Rows.Count; i++)
                {
                    dtBch.Rows.Add(dtB.Rows[i]["BRANCH_ID"].ToString(), dtB.Rows[i]["BRANCH_NAME"].ToString());
                }

                dt_DataHD = dtBch;
            }
            else
            {
                dt_DataHD = BranchClass.GetDetailBranchByID(SystemClass.SystemBranchID); ;
            }
            RadGridView_ShowHD.DataSource = dt_DataHD;
            dt_DataHD.AcceptChanges();
            this.Cursor = Cursors.Default;
        }
        //Set DT
        void SetDGV_DT(string pBchID)
        {
            this.Cursor = Cursors.WaitCursor;
            if (dt_DataDT.Rows.Count > 0) dt_DataDT.Rows.Clear();

            if (radRadioButtonElement_PC.CheckState == CheckState.Checked)
            {
                dt_DataDT = PosSaleClass.GetQtyPC_ByItembarcodeByBch(_pItembarcode, pBchID);
                RadGridView_ShowDT.MasterTemplate.Columns["DOCNO"].IsVisible = false;
            }
            if (radRadioButtonElement_Sale.CheckState == CheckState.Checked)
            {
                dt_DataDT = PosSaleClass.GetQtySale_ByItembarcodeByBch(_pItembarcode, pBchID);
                RadGridView_ShowDT.MasterTemplate.Columns["DOCNO"].IsVisible = false;
            }
            if (radRadioButtonElement_Recive.CheckState == CheckState.Checked)
            {
                dt_DataDT = PosSaleClass.GetQtyRecive_ByItembarcodeByDate(_pItembarcode, pBchID);
                RadGridView_ShowDT.MasterTemplate.Columns["DOCNO"].IsVisible = false;
            }
            if (radRadioButtonElement_Stock.CheckState == CheckState.Checked)
            {
                dt_DataDT = PosSaleClass.GetQtyStock_ByItembarcodeByBch(_pItembarcode, pBchID);
                RadGridView_ShowDT.MasterTemplate.Columns["DOCNO"].IsVisible = false;
            }
            if (radRadioButtonElement_CountStock.CheckState == CheckState.Checked)
            {
                dt_DataDT = PosSaleClass.GetQtyCountStock_ByItembarcodeBYBch(_pItembarcode, pBchID);
                RadGridView_ShowDT.MasterTemplate.Columns["DOCNO"].IsVisible = true;
            }
            RadGridView_ShowDT.DataSource = dt_DataDT;
            dt_DataDT.AcceptChanges();
            this.Cursor = Cursors.Default;
        }

        #region "ROWS DGV"
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_ShowDT_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }
        private void RadGridView_ShowDT_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        private void RadGridView_ShowDT_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        #endregion

        //Change
        private void RadGridView_ShowHD_SelectionChanged(object sender, EventArgs e)
        {
            if (RadGridView_ShowHD.Rows.Count == 0)
            {
                if (dt_DataDT.Rows.Count > 0) { dt_DataDT.Rows.Clear(); dt_DataDT.AcceptChanges(); }
                return;
            }
            if (RadGridView_ShowHD.CurrentRow.Index == -1) return;

            if (RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString() == "") return;

            if (RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString() != pBchID)
            {
                SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
                pBchID = RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString();
            }
        }
        //Change
        private void RadRadioButtonElement_Stock_CheckStateChanged(object sender, EventArgs e)
        {
            //SetDGV_HD();
            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
        }
        //Change
        private void RadRadioButtonElement_Recive_CheckStateChanged(object sender, EventArgs e)
        {
            //   SetDGV_HD();
            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
        }
        //Change
        private void RadRadioButtonElement_Sale_CheckStateChanged(object sender, EventArgs e)
        {
            //SetDGV_HD();
            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
        }
        //Change
        private void RadRadioButtonElement_PC_CheckStateChanged(object sender, EventArgs e)
        {
            //SetDGV_HD();
            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
        }
        //Change
        private void RadRadioButtonElement_CountStock_CheckStateChanged(object sender, EventArgs e)
        {
            SetDGV_DT(RadGridView_ShowHD.CurrentRow.Cells["BRANCH_ID"].Value.ToString());
        }
        private void RadButtonElement_pdt_Click(object sender, EventArgs e)
        {
            Controllers.FormClass.Document_Check(this.Name, "");
        }

    }
}

