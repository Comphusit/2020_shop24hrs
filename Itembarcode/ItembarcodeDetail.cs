﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Data;
using System.Drawing;
using PC_Shop24Hrs.Class;
using PC_Shop24Hrs.FormShare.ShowData;

namespace PC_Shop24Hrs.Itembarcode
{
    public partial class ItembarcodeDetail : Telerik.WinControls.UI.RadForm
    {
        private DateTime TimeStartInput;

        public string sID;
        public string sDim;
        public string sBarcode;
        public string sSpc_Name;
        public string sUnitID;
        public double sFactor;
        public string sPurchase;
        public string sPOLast;
        public double sPrice;

        string pNoSale = "0";
        string pNoPO = "0";


        readonly string _pTypeOpenByPO;//0 เปิดจากหน้าจอรายละเอียด 1 เปิดจากหน้าจอ PO
        string _pBchID;// ถ้าเปนการเปิดออเดอร์ให้ใส่สาขาด้วย
        string _pCheckPO_6;
        string _pCheckSale_30;
        readonly string _pBarcode;

        string StaNoOrderDeleteInvent = "";
        string StaNoOrderDelete = "0";

        Data_ITEMBARCODE dtBarcode;
        //Load
        public ItembarcodeDetail(string pTypeOpenByPO, string pBchID, string pCheckPO_6, string pCheckSale_30, string pBarcode)
        {
            InitializeComponent();
            //_pTypeOpen = pTypeOpen;
            _pTypeOpenByPO = pTypeOpenByPO;
            _pBchID = pBchID;
            _pCheckPO_6 = pCheckPO_6;
            _pCheckSale_30 = pCheckSale_30;
            _pBarcode = pBarcode;
        }
        //Load Main
        private void ItembarcodeDetail_Load(object sender, EventArgs e)
        {
            RadButton_Find.ButtonElement.ShowBorder = true;
            radButton_Refresh.ButtonElement.ShowBorder = true;
            radCheckBox_SPC_GiftPoint.ButtonElement.Font = SystemClass.SetFontGernaral;

            radButton_pdt.ButtonElement.ToolTipText = "คู่มือการใช้งาน";

            DatagridClass.SetDefaultRadGridView(RadGridView_ItemBarcode);

            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("SPC_ITEMACTIVE", "ใช้งาน")));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("USEFORPRINTING", "ใช้พิมพ์")));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 140)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "อัตราส่วน", 80)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));

            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคาสาขาใหญ่", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP13", "ราคาขาย MN_1", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP14", "ราคาขาย MN_2", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP15", "ราคาขาย MN_3", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP18", $@"ราคาขาย MN_5", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP19", $@"ราคาขาย MN_6", 105)));

            if ((SystemClass.SystemBranchID == "MN000"))
            {
                //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคาสาขาใหญ่", 105)));
                //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP13", "ราคาขาย MN_1", 105)));
                //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP14", "ราคาขาย MN_2", 105)));
                //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP15", "ราคาขาย MN_3", 105)));
                //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก", 105)));
                //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP18", $@"ราคาขาย MN_5", 105)));
                //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP19", $@"ราคาขาย MN_6", 105)));

                radLabel_Stock.Visible = false; radLabel_StockShow.Visible = false;
                tableLayoutPanel3.Visible = false;
            }
            else
            {
                RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP3"].IsVisible = false;
                RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP13"].IsVisible = false;
                RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP14"].IsVisible = false;
                RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP15"].IsVisible = false;
                RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP17"].IsVisible = false;
                RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP18"].IsVisible = false;
                RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP19"].IsVisible = false;

                switch (SystemClass.SystemBranchPrice)
                {
                    case "SPC_PriceGroup3":
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP3"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP13"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP14"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP15"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP17"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP18"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP19"].IsVisible = true;

                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคาสาขาใหญ่", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP13", "ราคาขาย MN_1")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP14", "ราคาขาย MN_2")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP15", "ราคาขาย MN_3")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP18", $@"ราคาขาย MN_5{Environment.NewLine}นาใน-บ้านมอญ-สายกอ{Environment.NewLine}หาดงาม-แม่น้ำ")));
                        break;
                    case "SPC_PriceGroup13":
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP13"].IsVisible = true;
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP3", "ราคาสาขาใหญ่")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP13", "ราคาขาย MN_1", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP14", "ราคาขาย MN_2")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP15", "ราคาขาย MN_3")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP18", $@"ราคาขาย MN_5{Environment.NewLine}นาใน-บ้านมอญ-สายกอ{Environment.NewLine}หาดงาม-แม่น้ำ")));
                        break;
                    case "SPC_PriceGroup14":
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP14"].IsVisible = true;
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP3", "ราคาสาขาใหญ่")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP13", "ราคาขาย MN_1")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP14", "ราคาขาย MN_2", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP15", "ราคาขาย MN_3")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP18", $@"ราคาขาย MN_5{Environment.NewLine}นาใน-บ้านมอญ-สายกอ{Environment.NewLine}หาดงาม-แม่น้ำ")));
                        break;
                    case "SPC_PriceGroup15":
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP15"].IsVisible = true;
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP3", "ราคาสาขาใหญ่")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP13", "ราคาขาย MN_1")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP14", "ราคาขาย MN_2")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP15", "ราคาขาย MN_3", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP18", $@"ราคาขาย MN_5{Environment.NewLine}นาใน-บ้านมอญ-สายกอ{Environment.NewLine}หาดงาม-แม่น้ำ")));
                        break;
                    case "SPC_PriceGroup17":
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP17"].IsVisible = true;
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP3", "ราคาสาขาใหญ่")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP13", "ราคาขาย MN_1")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP14", "ราคาขาย MN_2")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP15", "ราคาขาย MN_3")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP18", $@"ราคาขาย MN_5{Environment.NewLine}นาใน-บ้านมอญ-สายกอ{Environment.NewLine}หาดงาม-แม่น้ำ")));
                        break;
                    case "SPC_PriceGroup18":
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP18"].IsVisible = true;
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP3", "ราคาสาขาใหญ่")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP13", "ราคาขาย MN_1")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP14", "ราคาขาย MN_2")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP15", "ราคาขาย MN_3")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก")));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP18", $@"ราคาขาย MN_5{Environment.NewLine}นาใน-บ้านมอญ-สายกอ{Environment.NewLine}หาดงาม-แม่น้ำ", 140)));
                        break;
                    case "SPC_PriceGroup19":
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP19"].IsVisible = true;
                        break;
                    default:
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคาสาขาใหญ่", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP13", "ราคาขาย MN_1", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP14", "ราคาขาย MN_2", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP15", "ราคาขาย MN_3", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP17", $@"ราคาขาย MN_4{Environment.NewLine}เวียงสระ-ในลึก", 105)));
                        //RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP18", $@"ราคาขาย MN_5{Environment.NewLine}นาใน-บ้านมอญ-สายกอ{Environment.NewLine}หาดงาม-แม่น้ำ", 140)));
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP3"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP13"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP14"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP15"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP17"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP18"].IsVisible = true;
                        RadGridView_ItemBarcode.Columns["SPC_PRICEGROUP19"].IsVisible = true;
                        break;
                }

                radLabel_Stock.Visible = true; radLabel_StockShow.Visible = true;

            }
            RadGridView_ItemBarcode.TableElement.TableHeaderHeight = 60;
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_IMAGEPATH", "Image")));

            if (_pTypeOpenByPO != "0")
            {
                RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("OK", "เลือกสั่ง", 80)));
            }
            else
            {
                RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("OK", "OK")));
                if (SystemClass.SystemBranchID != "MN000")
                    RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("Zone", "ชั้นวางสินค้า", 300)));
                else
                    RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("Zone", "ชั้นวางสินค้า")));
            }

            radCheckBox_PO.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;
            radCheckBox_Sale.ButtonElement.Font = SystemClass.SetFontGernaral_Bold;


            ClearData();
            if (_pCheckPO_6 == "1") { radCheckBox_PO.CheckState = CheckState.Checked; }
            else { radCheckBox_PO.CheckState = CheckState.Unchecked; }

            if (_pCheckSale_30 == "1") { radCheckBox_Sale.CheckState = CheckState.Checked; }
            else { radCheckBox_Sale.CheckState = CheckState.Unchecked; }

            if (_pTypeOpenByPO != "0") // หน้า PO
            {
                tableLayoutPanel3.Visible = true;
                Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(_pBarcode);
                this.dtBarcode = dtBarcode;
                if (!(dtBarcode.GetItembarcodeStatus))
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    this.DialogResult = DialogResult.No;
                    this.Close();
                }
                else
                {
                    SetFindEnter();
                }
                radButton_Refresh.Enabled = false; RadButton_Find.Enabled = false;
            }
            else // หน้ารายละเอียดสินค้า
            {
                _pBchID = SystemClass.SystemBranchID;

                if (_pBarcode != "")
                {
                    tableLayoutPanel3.Visible = true;
                    Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(_pBarcode);
                    this.dtBarcode = dtBarcode;
                    if (!(dtBarcode.GetItembarcodeStatus))
                    {
                        MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                        this.DialogResult = DialogResult.No;
                        this.Close();
                    }
                    else
                    {
                        SetFindEnter();
                    }
                }
                else
                {
                    if (SystemClass.SystemBranchID == "MN000")
                    {
                        tableLayoutPanel3.Visible = false;
                        radLabel_Stock.Visible = false; radLabel_StockShow.Visible = false;
                        radCheckBox_PO.Enabled = false; radCheckBox_Sale.Enabled = false;
                    }
                    else
                    {
                        tableLayoutPanel3.Visible = true;
                        radLabel_Stock.Visible = true; radLabel_StockShow.Visible = true;
                    }
                }

            }
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //ClearData
        void ClearData()
        {
            if (pictureBox_Barcode.Image != null)
            {
                pictureBox_Barcode.Image.Dispose();
                pictureBox_Barcode.Image = null;
            }

            if (RadGridView_ItemBarcode.Rows.Count > 0) RadGridView_ItemBarcode.Rows.Clear();
            TimeStartInput = DateTime.Now;
            radLabel_Stock.Text = "";
            radLabel_ItemID.Text = "";
            radLabel_Dim.Text = "";
            radLabel_SpcName.Text = "";
            radLabel_Dpt.Text = "";
            radLabel_DptHead.Text = "";
            radLabel_Vender.Text = "";
            radLabel_GroupMain.Text = "";
            radLabel_GroupSub1.Text = "";
            radLabel_GroupSub2.Text = "";
            radLabel_Tel.Text = "";

            button_NoOrder.Text = "ปลดล็อกห้ามสั่ง"; button_NoOrder.Enabled = false; button_NoOrder.BackColor = Color.FromArgb(255, 203, 226);
            radLabel_NotPO.Text = "";
            radLabel_NoSale.Text = "";
            radLabel_PO.Text = "";
            radLabel_Sale.Text = "";
            pNoSale = "0";
            pNoPO = "0";
            radCheckBox_SPC_GiftPoint.Checked = false;
            radTextBox_Barcode.SelectAll();
            radTextBox_Barcode.Focus();
        }
        void SetFindEnter()
        {
            this.Cursor = Cursors.WaitCursor;
            radLabel_ItemID.Text = this.dtBarcode.Itembarcode_ITEMID;
            radLabel_Dim.Text = this.dtBarcode.Itembarcode_INVENTDIMID;
            radLabel_SpcName.Text = this.dtBarcode.Itembarcode_SPC_ITEMNAME;
            radLabel_Dpt.Text = this.dtBarcode.Itembarcode_DIMENSION + " : " + this.dtBarcode.Itembarcode_DESCRIPTION;
            if (_pTypeOpenByPO == "0")
            {
                radLabel_DptHead.Text = Models.DptClass.FindDptHead(this.dtBarcode.Itembarcode_DIMENSION);
            }
            radLabel_Vender.Text = this.dtBarcode.Itembarcode_VENDORID + " - " + this.dtBarcode.Itembarcode_VENDORIDNAME;
            radLabel_GroupMain.Text = this.dtBarcode.Itembarcode_GRPID0 + " - " + this.dtBarcode.Itembarcode_TAX0;
            radLabel_GroupSub1.Text = this.dtBarcode.Itembarcode_GRPID1 + " - " + this.dtBarcode.Itembarcode_TAX1;
            radLabel_GroupSub2.Text = this.dtBarcode.Itembarcode_GRPID2 + " - " + this.dtBarcode.Itembarcode_TAX2;
            radLabel_Tel.Text = Class.BranchClass.GetTel_ByBranchID(this.dtBarcode.Itembarcode_DIMENSION);

            if (this.dtBarcode.Itembarcode_SPC_GiftPoint == "0") radCheckBox_SPC_GiftPoint.Checked = false;
            else radCheckBox_SPC_GiftPoint.Checked = true;


            DataTable dtItem = Class.ItembarcodeClass.FindItembarcodeAll_ByItemidDim(this.dtBarcode.Itembarcode_ITEMID, this.dtBarcode.Itembarcode_INVENTDIMID);
            if (RadGridView_ItemBarcode.Rows.Count > 0) RadGridView_ItemBarcode.Rows.Clear();

            for (int i = 0; i < dtItem.Rows.Count; i++)
            {
                string zone = "";
                if (SystemClass.SystemBranchID != "MN000")
                {
                    DataTable dtZone = ItembarcodeClass.GetSHELFITEMS_Check(dtItem.Rows[i]["ITEMBARCODE"].ToString(), SystemClass.SystemBranchID);
                    if (dtZone.Rows.Count > 0)
                    {
                        zone = dtZone.Rows[0]["ZONE_NAME"].ToString() + @" / " + dtZone.Rows[0]["SHELF_ID"].ToString() + "-" + dtZone.Rows[0]["SHELF_NAME"].ToString();
                    }
                }
                Boolean active = true;
                if (dtItem.Rows[i]["SPC_ITEMACTIVE"].ToString() != "1") active = false;

                Boolean activePrint = true;
                if (dtItem.Rows[i]["USEFORPRINTING"].ToString() != "1") activePrint = false;

                RadGridView_ItemBarcode.Rows.Add(
                    active, activePrint,
                    dtItem.Rows[i]["ITEMBARCODE"].ToString(),
                    dtItem.Rows[i]["SPC_ITEMNAME"].ToString(),
                    Convert.ToDecimal(dtItem.Rows[i]["QTY"].ToString()).ToString("N2"),
                    dtItem.Rows[i]["UNITID"].ToString(),
                    Convert.ToDecimal(dtItem.Rows[i]["SPC_PRICEGROUP3"].ToString()).ToString("N2"),
                    Convert.ToDecimal(dtItem.Rows[i]["SPC_PRICEGROUP13"].ToString()).ToString("N2"),
                    Convert.ToDecimal(dtItem.Rows[i]["SPC_PRICEGROUP14"].ToString()).ToString("N2"),
                    Convert.ToDecimal(dtItem.Rows[i]["SPC_PRICEGROUP15"].ToString()).ToString("N2"),
                    Convert.ToDecimal(dtItem.Rows[i]["SPC_PRICEGROUP17"].ToString()).ToString("N2"),
                    Convert.ToDecimal(dtItem.Rows[i]["SPC_PRICEGROUP18"].ToString()).ToString("N2"),
                    Convert.ToDecimal(dtItem.Rows[i]["SPC_PRICEGROUP19"].ToString()).ToString("N2"),
                    dtItem.Rows[i]["SPC_IMAGEPATH"].ToString(),
                    dtItem.Rows[i]["OK"].ToString(),
                    zone);
            }

            if (_pTypeOpenByPO == "0") //รายละเอียดสินค้า
            {
                if (SystemClass.SystemBranchID == "MN000")
                {
                    radLabel_Stock.Visible = true; radLabel_StockShow.Visible = true; radLabel_StockShow.Text = "Retail";
                    radLabel_Stock.Text = ItembarcodeClass.FindStock_ByBarcode(RadGridView_ItemBarcode.Rows[0].Cells["ITEMBARCODE"].Value.ToString(), "RETAILAREA").ToString("N2");
                }
                else
                { // รายละเอียดสินค้า หน้าจอสาขา
                    radLabel_Stock.Visible = true; radLabel_StockShow.Visible = true;
                    radLabel_Stock.Text = ItembarcodeClass.FindStock_ByBarcode(RadGridView_ItemBarcode.Rows[0].Cells["ITEMBARCODE"].Value.ToString(), SystemClass.SystemBranchID).ToString("N2");

                    SetNoSaleNoOrder();

                    if (radCheckBox_PO.CheckState == CheckState.Checked) { radLabel_PO.Visible = true; _pCheckPO_6 = "1"; } else { radLabel_PO.Visible = false; _pCheckPO_6 = "0"; }
                    if (radCheckBox_Sale.CheckState == CheckState.Checked) { radLabel_Sale.Visible = true; _pCheckSale_30 = "1"; } else { radLabel_Sale.Visible = false; _pCheckSale_30 = "0"; }
                    SetSaleLast60OrderLast6();
                }
            }
            else
            {//การสั่งสินค้า

                if (radCheckBox_PO.CheckState == CheckState.Checked) { _pCheckPO_6 = "1"; } else { _pCheckPO_6 = "0"; }
                if (radCheckBox_Sale.CheckState == CheckState.Checked) { _pCheckSale_30 = "1"; } else { _pCheckSale_30 = "0"; }

                radLabel_Stock.Visible = true; radLabel_StockShow.Visible = true;
                radLabel_Stock.Text = Class.ItembarcodeClass.FindStock_ByBarcode(this.dtBarcode.Itembarcode_ITEMBARCODE, _pBchID).ToString("#,#0.00");

                ////ค้นหาสินค้าห้ามสั่ง + ค้นหาสินค้าห้ามขาย
                SetNoSaleNoOrder();

                //ค้นหาประวัติการสั่ง + ค้นหาประวัติการขาย
                SetSaleLast60OrderLast6();
            }

            if (_pTypeOpenByPO != "0") radTextBox_Barcode.Enabled = false; else radTextBox_Barcode.Text = "";


            this.Cursor = Cursors.Default;
        }
        //เช้คห้ามขาย ห้ามสั่ง
        void SetNoSaleNoOrder()
        {
            SetData_Shop(ItembarcodeClass.GetRETAILPLANORDERITEMBLOCKED_ByDim(_pBchID, radLabel_ItemID.Text, radLabel_Dim.Text), "1", radLabel_NotPO);
        }
        //เช็ค ยอดการขายและการสั่ง
        void SetSaleLast60OrderLast6()
        {
            //ค้นหาประวัติการสั่ง
            if (_pCheckPO_6 == "1")
            {
                //SetData_Shop(POClass.GetINVENTTRANSFERJOURLINE_ByDim(_pBchID, radLabel_ItemID.Text, radLabel_Dim.Text, 6), "3", radLabel_PO);
                SetData_Shop(POClass.GetINVENTTRANSFERJOURLINE_ByDim(_pBchID, radLabel_ItemID.Text, radLabel_Dim.Text), "3", radLabel_PO);
            }
            else
            {
                radLabel_PO.Text = "ไม่มีประวัติสั่งอ้างอิง [ผู้ใช้เลือกไม่ดึงข้อมูล]"; radLabel_PO.ForeColor = Color.Black;

            }
            //ค้นหาประวัติการขาย
            if (_pCheckSale_30 == "1")
            {
                SetData_Shop(PosSaleClass.GetSaleSumInventQty_ByItemInvent(this.dtBarcode.Itembarcode_ITEMBARCODE, _pBchID, 30), "4", radLabel_Sale);
            }
            else
            {
                radLabel_Sale.Text = "ไม่มีประวัติขายอ้างอิง [ผู้ใช้เลือกไม่ดึงข้อมูล]"; radLabel_Sale.ForeColor = Color.Black;
            }
        }
        //Enter
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            string condition;
            if (e.KeyCode == Keys.Enter)
            {
                string txtBox = ItembarcodeClass.CheckTextboxBarcode(TimeStartInput, radTextBox_Barcode.Text);

                this.Cursor = Cursors.WaitCursor;
                Data_ITEMBARCODE dtBarcode = new Data_ITEMBARCODE(txtBox);
                this.dtBarcode = dtBarcode;
                if (!(dtBarcode.GetItembarcodeStatus))
                {
                    MsgBoxClass.MsgBoxShow_FindRecordNoData("บาร์โค้ด");
                    ClearData();
                }
                else
                {
                    SetFindEnter();
                }

                this.Cursor = Cursors.Default;
            }
            else if (e.KeyCode == Keys.F1)
            {
                this.Cursor = Cursors.WaitCursor;
                if (radTextBox_Barcode.Text != "")
                {
                    condition = "AND SPC_ITEMNAME LIKE '%" + radTextBox_Barcode.Text.Replace(" ", "%") + @"%' ";
                }
                else { condition = ""; }
                SetData(condition);
                this.Cursor = Cursors.Default;
            }
            else if (e.KeyCode == Keys.F4)
            {
                this.Cursor = Cursors.WaitCursor;
                if (radTextBox_Barcode.Text != "")
                {
                    condition = "AND ITEMBARCODE LIKE '%" + radTextBox_Barcode.Text.Replace(" ", "%") + @"%' ";
                }
                else { condition = ""; }
                SetData(condition);
                this.Cursor = Cursors.Default;
            }
        }
        //setData
        void SetData(string pCon)
        {
            ShowDataDGV_Itembarcode ShowDataDGV_Itembarcode = new ShowDataDGV_Itembarcode(pCon);
            if (ShowDataDGV_Itembarcode.ShowDialog() == DialogResult.Yes)
            {
                this.dtBarcode = ShowDataDGV_Itembarcode.items;
                radTextBox_Barcode.Text = "";
                SetFindEnter();
            }
            else
            {
                radTextBox_Barcode.Text = "";
                radTextBox_Barcode.SelectAll();
            }
        }
        //set Data 
        void SetData_Shop(DataTable dtData, string pCase, RadLabel radLabelName)//pCase 1 =ค้นหาสินค้าห้ามสั่ง  2 =ค้นหาสินค้าห้ามขาย 3 =ค้นหาประวัติการสั่ง 4 =ค้นหาประวัติการขาย
        {

            if (dtData.Rows.Count == 0)
            {
                switch (pCase)
                {
                    case "1": radLabelName.Text = "สินค้าปกติ [สามารถสั่งได้]"; radLabelName.ForeColor = Color.Black; pNoPO = "0"; button_NoOrder.Enabled = true; button_NoOrder.Text = "ล็อกห้ามสั่ง"; button_NoOrder.BackColor = Color.FromArgb(173, 244, 215); break;
                    case "2": radLabelName.Text = "สินค้าปกติ"; radLabelName.ForeColor = Color.Black; pNoSale = "0"; break;
                    case "3": radLabelName.Text = "ไม่มีรายการสั่งย้อนหลัง ภายใน 6 วัน ที่ผ่านมา"; radLabelName.ForeColor = Color.Blue; break;
                    case "4": radLabelName.Text = "ไม่มียอดขายย้อนหลังภายใน 30 วัน"; radLabelName.ForeColor = Color.Red; break;
                    default: radLabelName.Text = ""; break;
                }
            }
            else
            {
                switch (pCase)
                {
                    case "1":
                        radLabelName.Text = "สินค้าห้ามสั่ง จนถึง  " + dtData.Rows[0]["STA_DATELOCK"].ToString();
                        radLabelName.ForeColor = Color.Red; pNoPO = "1"; button_NoOrder.Enabled = true;
                        button_NoOrder.Text = "ปลดล็อกห้ามสั่ง"; button_NoOrder.BackColor = Color.FromArgb(255, 203, 226);
                        StaNoOrderDelete = dtData.Rows[0]["STA_DELETE"].ToString();
                        StaNoOrderDeleteInvent = dtData.Rows[0]["INVENTLOCATIONIDTO"].ToString();
                        break;
                    case "2": radLabelName.Text = "ไม่สามารถสั่งได้ เนื่องจากสินค้าถูกระบุหยุดส่งสาขา"; radLabelName.ForeColor = Color.Red; pNoSale = "1"; break;
                    case "3":
                        radLabelName.Text = "รายการสั่งล่าสุด " + dtData.Rows[0]["TRANSDATE"].ToString() + " จำนวน " +
                            Convert.ToDouble(dtData.Rows[0]["QTY"].ToString()).ToString() + " " + dtData.Rows[0]["SPC_ITEMBARCODEUNIT"].ToString() +
                            " [" + dtData.Rows[0]["STA"].ToString() + "]";
                        radLabelName.ForeColor = Color.Red;
                        break;
                    case "4":
                        double avg = Convert.ToDouble(dtData.Rows[0]["QTY"].ToString()) / 30;
                        string unitId = RadGridView_ItemBarcode.Rows[0].Cells["UNITID"].Value.ToString();
                        radLabelName.Text = "ยอดขายย้อนหลัง 30 วัน : " +
                            String.Format("{0:0.00}", Convert.ToDouble(dtData.Rows[0]["QTY"].ToString())) + " " + unitId +
                            " [ เฉลี่ย " + String.Format("{0:0.00}", avg) + " " + unitId + " /วัน ]";
                        radLabelName.ForeColor = Color.Blue;
                        break;
                    default: radLabelName.Text = ""; break;
                }
            }
        }

        //ChangeImage
        private void RadGridView_ItemBarcode_SelectionChanged(object sender, EventArgs e)
        {
            if ((RadGridView_ItemBarcode.Rows.Count == 0) ) return;

            try
            {
                //    ค้นหาสินค้าหยุดส่ง
                SetData_Shop(ItembarcodeClass.GetTransferBlockedTable_ByBarcode(_pBchID, RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString()), "2", radLabel_NoSale);
            }
            catch (Exception) {}

            try
            {
                pictureBox_Barcode.Image = ImageClass.ScaleImageDataGridView150_200_ForDelete_SendPath(RadGridView_ItemBarcode.CurrentRow.Cells["SPC_IMAGEPATH"].Value.ToString());
            }
            catch (Exception)
            {
                pictureBox_Barcode.Image = Image.FromFile(PathImageClass.pImageEmply);
            }
        }
        //refresh
        private void RadButton_Refresh_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        //Find
        private void RadButton_Find_Click(object sender, EventArgs e)
        {
            SetData("");
        }
        //Cell Double Click
        private void RadGridView_ItemBarcode_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            if (_pTypeOpenByPO == "0") return;
            ChooseForPO();
        }
        //cell OK Click
        private void RadGridView_ItemBarcode_CellClick(object sender, GridViewCellEventArgs e)
        {
            if (_pTypeOpenByPO == "0")
            {
                switch (e.Column.Name)
                {
                    case "Zone":
                        ConfigZone();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (e.Column.Name)
                {
                    case "OK":
                        ChooseForPO();
                        break;
                    default:
                        break;
                }
            }

        }
        //Choose For PO
        void ChooseForPO()
        {
            if ((RadGridView_ItemBarcode.Rows.Count == 0) || (RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() == "") ||
                            (RadGridView_ItemBarcode.CurrentRow.Index == -1)) return;

            if (RadGridView_ItemBarcode.CurrentRow.Cells["SPC_ITEMACTIVE"].Value.ToString() == "False")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าไม่อยู่ในสถานะ ใช้งาน ไม่สามารถสั่งได้ เช็คบาร์โค้ดใหม่อีกครั้ง.");
                return;
            }
            //ในกรณีที่เป็นสินค้าห้ามขาย ห้ามสั่ง ต้องผ่านไป
            if (pNoSale == "1")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"สินค้าอยู่ในสถานะหยุดส่งที่สาขา ไม่สามารถสั่งได้.{Environment.NewLine}[ติดต่อจัดซื้อสำหรับข้อมูลเพิ่มเติม]");
                return;
            }

            if (pNoPO == "1")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าอยู่ในสถานะห้ามสั่ง ไม่สามารถสั่งได้.");
                return;
            }
            //ส่งค่าไปหน้าสั่งสินค้า
            sID = radLabel_ItemID.Text;
            sDim = radLabel_Dim.Text;
            sBarcode = RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();
            sSpc_Name = RadGridView_ItemBarcode.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString();
            sUnitID = RadGridView_ItemBarcode.CurrentRow.Cells["UNITID"].Value.ToString();
            sFactor = Convert.ToDouble(RadGridView_ItemBarcode.CurrentRow.Cells["QTY"].Value.ToString());
            sPurchase = radLabel_Dpt.Text;
            sPOLast = radLabel_PO.Text;
            sPrice = Convert.ToDouble(RadGridView_ItemBarcode.CurrentRow.Cells["SPC_PRICEGROUP3"].Value.ToString());

            DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Set Zone
        void ConfigZone()
        {
            if ((RadGridView_ItemBarcode.Rows.Count == 0) || (RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() == "") ||
                               (RadGridView_ItemBarcode.CurrentRow.Index == -1)) return;

            string sta = "1", rmk = "";
            if (RadGridView_ItemBarcode.CurrentRow.Cells["Zone"].Value.ToString() != "")
            {
                sta = "0";
                DataTable dtChange = ItembarcodeClass.GetSHELFITEMS_CHANGE(RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(), SystemClass.SystemBranchID);
                if (dtChange.Rows.Count > 0)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error($@"สินค้าที่ระบุอยู่ในสถานะรอเปลี่ยนชั้นวาง{Environment.NewLine}ไม่สามารถแก้ไขชั้นวางได้{Environment.NewLine}รอการยืนยันสถานการเปลี่ยนชั้นวางเก่าให้เรียบร้อยก่อนการเปลี่ยนชั้นวางใหม่อีกครั้ง");
                    return;
                }

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("สินค้ามีจัดวางเรียบร้อยแล้ว ต้องการเปลี่ยนชันวาง ?") == DialogResult.No)
                { return; }

                FormShare.InputData _inputData = new FormShare.InputData("1", RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() + " " +
                    RadGridView_ItemBarcode.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString(), "ระบุ - เหตุผลสำหรับการเปลี่ยนแปลงชั้นวาง",
                     RadGridView_ItemBarcode.CurrentRow.Cells["UNITID"].Value.ToString());
                if (_inputData.ShowDialog(this) == DialogResult.No)
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Error("การเปลี่ยนแปลงชั้นวาง จำเป็นต้องระบุเหตุผลในการเปลี่ยนเท่านั้น.");
                    return;
                }

                rmk = _inputData.pInputData;
            }


            DataTable dt = ItembarcodeClass.GetSHELF_Add(SystemClass.SystemBranchID); ;
            if (dt.Rows.Count > 0)
            {
                FormShare.ShowData.ShowDataDGV frm = new FormShare.ShowData.ShowDataDGV() { dtData = dt };
                if (frm.ShowDialog(this) == DialogResult.Yes)
                {
                    string strIn;
                    if (sta == "1")
                    {
                        strIn = ItembarcodeClass.Save_SHELFITEMS(frm.pID, frm.pID2, RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                         RadGridView_ItemBarcode.CurrentRow.Cells["UNITID"].Value.ToString(), "", "", "");
                    }
                    else
                    {
                        DataTable dtZone = ItembarcodeClass.GetSHELFITEMS_Check(RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(), SystemClass.SystemBranchID);
                        if (dtZone.Rows.Count == 0) return;

                        strIn = ItembarcodeClass.Save_SHELFITEMS(frm.pID, frm.pID2, RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString(),
                          RadGridView_ItemBarcode.CurrentRow.Cells["UNITID"].Value.ToString(), dtZone.Rows[0]["ZONE_ID"].ToString(),
                          dtZone.Rows[0]["SHELF_ID"].ToString(), rmk);

                    }

                    string T = ConnectionClass.ExecuteSQL_Main(strIn);
                    MsgBoxClass.MsgBoxShow_SaveStatus(T);
                    if (T == "") RadGridView_ItemBarcode.CurrentRow.Cells["Zone"].Value = frm.pDesc;

                }
            }
        }
        //History
        private void Button_pc_Click(object sender, EventArgs e)
        {
            if (RadGridView_ItemBarcode.Rows.Count == 0) return;
            Itembarcode_History _itemHistory = new Itembarcode_History(RadGridView_ItemBarcode.Rows[0].Cells["ITEMBARCODE"].Value.ToString());
            if (_itemHistory.ShowDialog(this) == DialogResult.OK)
            {

            }
        }
        //Open Document
        private void RadButton_pdt_Click(object sender, EventArgs e)
        {
            FormClass.Document_Check(this.Name, _pTypeOpenByPO);
        }
        //Lock + Unlock PO
        private void Button_NoOrder_Click(object sender, EventArgs e)
        {
            if (_pTypeOpenByPO != "0") return;

            if (SystemClass.SystemBranchID == "MN000") return;

            string T;

            if (ConnectionClass.SelectSQL_MainAX(ItembarcodeClass.Check_EXTERNALLIST_RETAILPLANORDERITEMBLOCKED(radLabel_ItemID.Text, radLabel_Dim.Text)).Rows.Count > 0)
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error("สินค้านี้อยู่ในระหว่างการเปลี่ยนแปลงข้อมูล ยังไม่สามารถแก้ไขได้" + Environment.NewLine +
                    "รอประมาณ 5 นาที แล้วดำเนินการใหม่อีกครั้ง");
                return;
            }

            if (pNoPO == "0") // ล็อก
            {
                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ต้องการล็อกสินค้าทั้งหมดเป็นห้ามสั่ง 30 วัน ?") == DialogResult.No) { return; }

                //T = Itembarcode_Class.SPC_EXTERNALLIST_SPC_RETAILPLANORDERITEMBLOCKED(radLabel_ItemID.Text, radLabel_Dim.Text, SystemClass.SystemBranchID, "CREATE");
                T = ConnectionClass.ExecuteSQL_ArrayMainAX(AX_SendData.Save_EXTERNALLIST_RETAILPLANORDERITEMBLOCKED(radLabel_ItemID.Text, radLabel_Dim.Text, SystemClass.SystemBranchID, "CREATE"));
            }
            else
            {
                if (StaNoOrderDelete == "1")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าถูกระบุเป็น ห้ามสั่ง ทุกมิติสินค้าในรหัสนี้" + Environment.NewLine + "ติดต่อจัดซื้อเพื่อปลดล็อกเท่านั้น.");
                    return;
                }

                if (StaNoOrderDelete == "2")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าถูกระบุไม่มีกำหนดอายุการปลดล็อก" + Environment.NewLine + "ติดต่อจัดซื้อเพื่อปลดล็อกเท่านั้น.");
                    return;
                }

                if (StaNoOrderDeleteInvent == "")
                {
                    MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าถูกระบุเป็น ห้ามสั่ง ทุกคลังสินค้า" + Environment.NewLine + "ติดต่อจัดซื้อเพื่อปลดล็อกเท่านั้น.");
                    return;
                }

                if (MsgBoxClass.MsgBoxShowYesNo_DialogResult("ต้องการปลดล็อกสินค้าทั้งหมดเป็นสินค้าที่สั่งได้ตามปกติ ?") == DialogResult.No) { return; }

                //T = Itembarcode_Class.SPC_EXTERNALLIST_SPC_RETAILPLANORDERITEMBLOCKED(radLabel_ItemID.Text, radLabel_Dim.Text, SystemClass.SystemBranchID, "DELETE");
                T = ConnectionClass.ExecuteSQL_ArrayMainAX(AX_SendData.Save_EXTERNALLIST_RETAILPLANORDERITEMBLOCKED(radLabel_ItemID.Text, radLabel_Dim.Text, SystemClass.SystemBranchID, "DELETE"));
            }

            MsgBoxClass.MsgBoxShow_SaveStatus(T);
            if (T == "") { button_NoOrder.Enabled = false; }
        }

        private void RadTextBox_Barcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (radTextBox_Barcode.Text.Length == 0) TimeStartInput = DateTime.Now;
        }
        private void RadTextBox_Barcode_KeyUp(object sender, KeyEventArgs e)
        {
            if (radTextBox_Barcode.Text.Length == 1) TimeStartInput = DateTime.Now;
        }
    }
}
