﻿//CheckOK
using System;
using System.Windows.Forms;
using PC_Shop24Hrs.Controllers;
using Telerik.WinControls.UI;
using System.Data;
using System.Drawing;
using PC_Shop24Hrs.Class;

namespace PC_Shop24Hrs.Itembarcode
{
    public partial class ItembarcodeDetail_Order : Telerik.WinControls.UI.RadForm
    {
        public string sID;
        public string sDim;
        public string sBarcode;
        public string sSpc_Name;
        public string sUnitID;
        public double sFactor;
        public string sPurchase;
        public string sPOLast;
        public double sPrice;
        public string sPOINVENT;
        public string sDimension;
        public string sPathImage;
        public string sVenderID;

        //readonly string _pTypeOpenByPO;//0 เปิดจากหน้าจอรายละเอียด 1 เปิดจากหน้าจอ PO
        readonly string _pBchID;// ถ้าเปนการเปิดออเดอร์ให้ใส่สาขาด้วย
        readonly string _pCheckPO_6;
        readonly string _pCheckSale_30;
        readonly DataTable _dtItem;

        string Sta_orderAddOn;

        public ItembarcodeDetail_Order(DataTable dtItem, string pCheckPO_6, string pCheckSale_30, string pBchID)
        {
            InitializeComponent();
            _pBchID = pBchID;
            _pCheckPO_6 = pCheckPO_6;
            _pCheckSale_30 = pCheckSale_30;
            _dtItem = dtItem;

            DatagridClass.SetDefaultRadGridView(RadGridView_ItemBarcode);

            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("BlockSend", "หยุดส่ง")));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddCheckBoxColumn_TYPEID("STA_NOTOrder", "ห้ามสั่ง")));

            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 140)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 300)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "อัตราส่วน", 80)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80)));

            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP3", "ราคาสาขาใหญ่", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP13", "ราคาขาย MN_1", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP14", "ราคาขาย MN_2", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP15", "ราคาขาย MN_3", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP17", "ราคาขาย MN_4", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP18", "ราคาขาย MN_5", 105)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddNumberColumn_AddManualSetRight("SPC_PRICEGROUP19", "ราคาขาย MN_6", 105)));

            if ((SystemClass.SystemBranchID == "MN000"))
            {
                RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = true;
                RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = true;
                RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = true;
                RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = true;
                RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = true;
                RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = true;
                RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = true;
            }
            else
            {
                switch (SystemClass.SystemBranchPrice)
                {
                    case "SPC_PriceGroup3":
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = false;
                        break;
                    case "SPC_PriceGroup13":
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = false;
                        break;
                    case "SPC_PriceGroup14":
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = false;
                        break;
                    case "SPC_PriceGroup15":
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = false;
                        break;
                    case "SPC_PriceGroup17":
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = false;
                        break;
                    case "SPC_PriceGroup18":
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = false;
                        break;
                    case "SPC_PriceGroup19":
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = false;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = true;
                        break;
                    default:
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP3"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP13"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP14"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP15"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP17"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP18"].IsVisible = true;
                        RadGridView_ItemBarcode.MasterTemplate.Columns["SPC_PRICEGROUP19"].IsVisible = true;
                        break;
                }
            }

            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("SPC_PickingLocationId", "คลังเบิก", 100)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManualSetCenter("OK", "เลือกสั่ง", 80)));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("SPC_IMAGEPATH", "SPC_IMAGEPATH")));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("NUM", "NUM")));
            RadGridView_ItemBarcode.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_Visible("VENDORID", "VENDORID")));

        }
        //Load Main
        private void ItembarcodeDetail_Order_Load(object sender, EventArgs e)
        {
            ClearData();
            SetFindEnter();
        }
        //SetFontInRadGridview
        #region SetFontInRadGridview
        private void RadGridView_Show_ConditionalFormattingFormShown(object sender, EventArgs e)
        {
            DatagridClass.SetDGV_ConditionalFormattingFormShown(sender, e);
        }
        // Set Rows+Font
        private void RadGridView_Show_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            DatagridClass.SetDGV_ViewCellFormatting(e);
        }

        private void RadGridView_Show_FilterPopupInitialized(object sender, FilterPopupInitializedEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupInitialized(e);
        }

        private void RadGridView_Show_FilterPopupRequired(object sender, FilterPopupRequiredEventArgs e)
        {
            DatagridClass.SetDGV_FilterPopupRequired(e);
        }
        #endregion

        //ClearData
        void ClearData()
        {
            if (RadGridView_ItemBarcode.Rows.Count > 0) RadGridView_ItemBarcode.Rows.Clear();

            radLabel_PO.Text = "";
            radLabel_Sale.Text = "";
        }
        void SetFindEnter()
        {
            this.Cursor = Cursors.WaitCursor;

            sID = _dtItem.Rows[0]["ITEMID"].ToString();
            sDim = _dtItem.Rows[0]["INVENTDIMID"].ToString();
            sPurchase = _dtItem.Rows[0]["NUM"].ToString() + "-" + _dtItem.Rows[0]["DESCRIPTION"].ToString();

            if (RadGridView_ItemBarcode.Rows.Count > 0) RadGridView_ItemBarcode.Rows.Clear();

            RadGridView_ItemBarcode.DataSource = _dtItem;
            _dtItem.AcceptChanges();

            SetSaleLast60OrderLast6();

            this.Cursor = Cursors.Default;
        }

        //เช็ค ยอดการขายและการสั่ง
        void SetSaleLast60OrderLast6()
        {
            //ค้นหาประวัติการสั่ง
            //if (_pCheckPO_6 == "1") SetData_Shop(POClass.GetINVENTTRANSFERJOURLINE_ByDim(_pBchID, sID, sDim, 6), "3", radLabel_PO);
            //else radLabel_PO.Text = "ไม่มีประวัติสั่งอ้างอิง [ผู้ใช้เลือกไม่ดึงข้อมูล]"; radLabel_PO.ForeColor = Color.Black;
            if (_pCheckPO_6 == "1") SetData_Shop(POClass.GetINVENTTRANSFERJOURLINE_ByDim(_pBchID, sID, sDim), "3", radLabel_PO);
            else radLabel_PO.Text = "ไม่มีประวัติสั่งอ้างอิง [ผู้ใช้เลือกไม่ดึงข้อมูล]"; radLabel_PO.ForeColor = Color.Black;

            //ค้นหาประวัติการขาย
            if (_pCheckSale_30 == "1") SetData_Shop(PosSaleClass.GetSaleSumInventQty_ByItemInvent(_dtItem.Rows[0]["ITEMBARCODE"].ToString(), _pBchID, 30), "4", radLabel_Sale);
            else radLabel_Sale.Text = "ไม่มีประวัติขายอ้างอิง [ผู้ใช้เลือกไม่ดึงข้อมูล]"; radLabel_Sale.ForeColor = Color.Black;
        }

        //set Data 
        void SetData_Shop(DataTable dtData, string pCase, RadLabel radLabelName)//pCase 1 =ค้นหาสินค้าห้ามสั่ง  2 =ค้นหาสินค้าห้ามขาย 3 =ค้นหาประวัติการสั่ง 4 =ค้นหาประวัติการขาย
        {
            if (dtData.Rows.Count == 0)
            {
                switch (pCase)
                {
                    case "1": radLabelName.Text = "สินค้าปกติ [สามารถสั่งได้]"; radLabelName.ForeColor = Color.Black; break;
                    case "2": radLabelName.Text = "สินค้าปกติ"; radLabelName.ForeColor = Color.Black; break;
                    case "3": radLabelName.Text = "ไม่มีรายการสั่งย้อนหลัง ภายใน 6 วัน ที่ผ่านมา"; radLabelName.ForeColor = Color.Blue; Sta_orderAddOn = "1"; break;
                    case "4": radLabelName.Text = "ไม่มียอดขายย้อนหลังภายใน 30 วัน"; radLabelName.ForeColor = Color.Red; break;
                    default: radLabelName.Text = ""; break;
                }
            }
            else
            {
                switch (pCase)
                {
                    case "1": radLabelName.Text = "สินค้าห้ามสั่ง จนถึง  " + dtData.Rows[0]["STA_DATELOCK"].ToString(); radLabelName.ForeColor = Color.Red; break;
                    case "2": radLabelName.Text = "ไม่สามารถสั่งได้ เนื่องจากสินค้าถูกระบุหยุดส่งสาขา"; radLabelName.ForeColor = Color.Red; break;
                    case "3":
                        radLabelName.Text = "รายการสั่งล่าสุด " + dtData.Rows[0]["TRANSDATE"].ToString() + " จำนวน " +
                            Convert.ToDouble(dtData.Rows[0]["QTY"].ToString()).ToString() + " " + dtData.Rows[0]["SPC_ITEMBARCODEUNIT"].ToString() +
                            " [" + dtData.Rows[0]["STA"].ToString() + "]";
                        radLabelName.ForeColor = Color.Red;
                        Sta_orderAddOn = dtData.Rows[0]["STAORDER_ADDON"].ToString();
                        break;
                    case "4":
                        double avg = Convert.ToDouble(dtData.Rows[0]["QTY"].ToString()) / 30;
                        string unitId = RadGridView_ItemBarcode.Rows[0].Cells["UNITID"].Value.ToString();
                        radLabelName.Text = "ยอดขายย้อนหลัง 30 วัน : " +
                            String.Format("{0:0.00}", Convert.ToDouble(dtData.Rows[0]["QTY"].ToString())) + " " + unitId +
                            " [ เฉลี่ย " + String.Format("{0:0.00}", avg) + " " + unitId + " /วัน ]";
                        radLabelName.ForeColor = Color.Blue;
                        break;
                    default: radLabelName.Text = ""; break;
                }
            }
        }


        //Cell Double Click
        private void RadGridView_ItemBarcode_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            ChooseForPO();
        }
        //cell OK Click
        private void RadGridView_ItemBarcode_CellClick(object sender, GridViewCellEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "OK":
                    ChooseForPO();
                    break;
                default:
                    break;
            }


        }
        //Choose For PO
        void ChooseForPO()
        {
            if ((RadGridView_ItemBarcode.Rows.Count == 0) || (RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString() == "") ||
                            (RadGridView_ItemBarcode.CurrentRow.Index == -1)) return;

            if (Sta_orderAddOn == "0")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"สินค้าอยู่ในระหว่างการจัดสินค้า ไม่สามารถสั่งได้.{Environment.NewLine}รอจัดสินค้าอีกครั้ง แล้วค่อยสั่งเพิ่มถ้ายังไม่ได้");
                return;
            }

            //ในกรณีที่เป็นสินค้าห้ามขาย ห้ามสั่ง ต้องผ่านไป
            if (RadGridView_ItemBarcode.CurrentRow.Cells["BlockSend"].Value.ToString() == "1")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Error($@"สินค้าอยู่ในสถานะหยุดส่งที่สาขา ไม่สามารถสั่งได้.{Environment.NewLine}[ติดต่อจัดซื้อสำหรับข้อมูลเพิ่มเติม]");
                return;
            }

            if (RadGridView_ItemBarcode.CurrentRow.Cells["STA_NOTOrder"].Value.ToString() == "1")
            {
                MsgBoxClass.MsgBoxShowButtonOk_Warning("สินค้าอยู่ในสถานะห้ามสั่ง ไม่สามารถสั่งได้.");
                return;
            }
            //ส่งค่าไปหน้าสั่งสินค้า

            sBarcode = RadGridView_ItemBarcode.CurrentRow.Cells["ITEMBARCODE"].Value.ToString();
            sSpc_Name = RadGridView_ItemBarcode.CurrentRow.Cells["SPC_ITEMNAME"].Value.ToString();
            sUnitID = RadGridView_ItemBarcode.CurrentRow.Cells["UNITID"].Value.ToString();
            sFactor = Convert.ToDouble(RadGridView_ItemBarcode.CurrentRow.Cells["QTY"].Value.ToString());

            sPOLast = radLabel_PO.Text;
            sPrice = Convert.ToDouble(RadGridView_ItemBarcode.CurrentRow.Cells["SPC_PRICEGROUP3"].Value.ToString());
            sPOINVENT = RadGridView_ItemBarcode.CurrentRow.Cells["SPC_PickingLocationId"].Value.ToString();

            sDimension = RadGridView_ItemBarcode.CurrentRow.Cells["NUM"].Value.ToString();
            sPathImage = RadGridView_ItemBarcode.CurrentRow.Cells["SPC_IMAGEPATH"].Value.ToString();
            sVenderID = RadGridView_ItemBarcode.CurrentRow.Cells["VENDORID"].Value.ToString();

            DialogResult = DialogResult.Yes;
            this.Close();
        }


    }
}
